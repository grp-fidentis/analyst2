package cz.fidentis.analyst.gui.app.tools.opencl;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;

import java.io.IOException;
import java.util.List;

/**
 * Benchmark comparing the speed of GPU-based and CPU-based Octree construction.
 *
 * @author Marek Horský
 */
public class OctreeBuilderBenchmark {
    private static final int ITERATIONS = 50;
    private static final int ITERATION_OFFSET = 5; // First n iterations are ignored to account for power management OS scheduling
    private static final boolean ALLOW_REBUILD = true; // Allows re-use of allocated octree memory for the next build
    private static final String PATH_MAIN = "C:\\Users\\marek\\Desktop\\FidentisFaces\\multi-scan\\average-boy-17-20\\average_boy_17-20.obj";

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        MeshModel model = BenchmarkUtils.getModel(PATH_MAIN);
        if (!OpenCLServices.isOpenCLAvailable()) {
            System.out.println("OpenCL could not find supported platform. Benchmark terminated");
            return;
        }
        BenchmarkUtils.printGPU();
        long timeGPU = buildGPU(model.getFacets(), ITERATIONS);
        long timeCPU = buildCPU(model.getFacets(), ITERATIONS);
        BenchmarkUtils.printComparison(timeCPU, timeGPU, ITERATIONS);
    }

    /**
     * @param facets
     * @param iterations
     * @return
     */
    public static long buildGPU(List<MeshFacet> facets, int iterations) {
        CLContext clContext = OpenCLServices.createContext();
        assert clContext != null;
        long timeGPU = 0;
        if (ALLOW_REBUILD) {
            OctreeOpenCL octree = OctreeOpenCL.create(clContext);
            for (int i = 0; i < iterations + ITERATION_OFFSET; i++) {
                long time = System.nanoTime();
                octree.build(facets);
                timeGPU += i < ITERATION_OFFSET ? 0 : System.nanoTime() - time;
            }
            octree.release();
        } else {
            for (int i = 0; i < iterations + ITERATION_OFFSET; i++) {
                long time = System.nanoTime();
                OctreeOpenCL octree = OctreeOpenCL.create(clContext);
                octree.build(facets);
                timeGPU += i < ITERATION_OFFSET ? 0 : System.nanoTime() - time;
                octree.release();
            }
        }
        OpenCLServices.release(clContext);
        return timeGPU;
    }

    /**
     * @param facets
     * @param iterations
     * @return
     */
    public static long buildCPU(List<MeshFacet> facets, int iterations) {
        long timeGPU = 0;
        for (int i = 0; i < iterations + ITERATION_OFFSET; i++) {
            long time = System.nanoTime();
            Octree.create(facets);
            timeGPU += i < ITERATION_OFFSET ? 0 : System.nanoTime() - time;
        }
        return timeGPU;
    }
}
