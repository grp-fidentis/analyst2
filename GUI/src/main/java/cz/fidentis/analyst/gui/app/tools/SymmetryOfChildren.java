package cz.fidentis.analyst.gui.app.tools;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceFactory;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.face.FaceDistanceServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.icp.IcpServices;
import cz.fidentis.analyst.engines.landmarks.LandmarkServices;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryServices;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This tool is used to measure symmetry of different age categories and genders.
 *
 * @author Radek Oslejsek
 */
public class SymmetryOfChildren {
    // Directory containing scans, scans in dir will be used in experiment
    private static final String DATA_DIR = "../analyst-data-antropologie/symetrie_data/Modely-all";
    // Limit for number of scans used for experiment
    private static final int MAX_SAMPLES = Integer.MAX_VALUE;

    private static final SymmetryConfig SYMMETRY_CONFIG_SEC = new SymmetryConfig(
            SymmetryConfig.Method.ROBUST_POINT_CLOUD,
            new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SPACE, 500), //200
            2000 //1000
    );

    private static final SymmetryConfig SYMMETRY_CONFIG_PRIM = new SymmetryConfig(
            SymmetryConfig.Method.ROBUST_MESH,
            new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SPACE, 500), //500
            2000 //2000
    );

    /**
     * Main method
     * @param args Input arguments
     * @throws IOException on IO error
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<Path> faces = Files.list(new File(DATA_DIR).toPath())
                .filter(f -> f.toString().endsWith(".obj"))
                .sorted()
                .limit(MAX_SAMPLES)
                .toList();

        StringBuilder output = new StringBuilder();
        //output.append("NAME|AGE|GENDER|G_ASYMMETRY|L_ASYMMETRY|PLANES_ANGLE|PLANES_SIMILARITY|G_PLANE|L_PLANE\n");
        output.append("NAME|AGE|GENDER|ASYMMETRY|PLANE\n");
        for (int i = 0; i < faces.size(); i++) {
            //if (i < 73) {
            //    continue;
            //}
            StringBuilder line = new StringBuilder();

            // Basic classification by age, gender, ...
            HumanFace face = HumanFaceFactory.create(-1L, faces.get(i).toFile());
            FaceStateServices.updateCurvature(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            System.out.println(i + ": " + face.getShortName());
            line.append(face.getShortName() + "|")
                    .append(face.getShortName().split("_", 4)[0] + "-" + face.getShortName().split("_", 4)[1] + "|")
                    .append(face.getShortName().split("_", 4)[2] + "|");

            // Geometry-based symmetry
            double dist = computeSymmetry(face, SYMMETRY_CONFIG_PRIM);
            Plane plane = face.getSymmetryPlane();
            if (dist > 1.5) {
                double dist2 = computeSymmetry(face, SYMMETRY_CONFIG_SEC);
                if (dist2 < dist) {
                    System.out.println("Robust point cloud IS BETTER: " + dist + "/" + dist2);
                    dist = dist2;
                    plane = face.getSymmetryPlane();
                }
            }

            // Landmarks-based symmetry
            double fpDist = computeSymmetry(face, null);
            Plane fpPlane = face.getSymmetryPlane();
            if (fpDist < dist) {
                System.out.println("Landmarks ARE BETTER: " + dist + "/" + fpDist);
                dist = fpDist;
                plane = fpPlane;
            }

            line.append(dist + "|");

            /*
            double angle = Math.abs(plane.getNormal().dot(fpPlane.getNormal()));
            if (angle < 0.5) {
                System.out.println("Wide angle: " + plane.getNormal() + " AND " + fpPlane.getNormal());
            }

            line.append(angle + "|");
            line.append(plane.similarity(fpPlane) + "|");
             */
            line.append(plane);
            line.append("\n");

            output.append(line);
        }

        System.out.println(output);
    }


    protected static double computeSymmetry(HumanFace face, SymmetryConfig config) {
        Plane plane = (config != null)
                ? SymmetryServices.estimate(face.getMeshModel(), config)
                : LandmarkServices.computeSymmetryPlane(face.getAllLandmarks().stream().filter(fp ->
                    fp.getCode().startsWith("EX_") || fp.getCode().startsWith("CH_") || fp.getCode().startsWith("T_")
                    ).toList()); // FILTROVAT !!!
        if (plane == null) {
            return Double.POSITIVE_INFINITY;
        }
        face.setSymmetryPlane(plane);
        SymmetryOfChildren.SymmetryStats symmetryStats = new SymmetryOfChildren.SymmetryStats(face);
        return symmetryStats.meanDistance;
    }

    /**
     * Class computing symmetry stats (Hausdorff distance, Mean distance and Chamfer distance) for given face.
     *
     * @author Patrik Brosz
     */
    private static class SymmetryStats {

        private final HumanFace face;
        private HumanFace mirrorFace;
        private boolean hasSymmetry;
        private double hausdorffDistance;
        private double meanDistance;
        private double chamferDistance;

        /**
         * Constructor
         *
         * @param face Human Face (or other type of 3D scan)
         */
        SymmetryStats(HumanFace face) {
            this.face = face;

            createMirrorFace();
            registerMirrorFace();
            calculateDistances();
        }

        public HumanFace getFace() {
            return face;
        }

        public boolean getHasSymmetry() {
            return hasSymmetry;
        }

        public double getHausdorffDistance() {
            return hausdorffDistance;
        }

        public double getChamferDistance() {
            return chamferDistance;
        }

        public double getMeanDistance() {
            return meanDistance;
        }

        private void setHausdorffDistance(double hausdorffDistance) {
            this.hausdorffDistance = hausdorffDistance;
        }

        private void setMeanDistance(double meanDistance) {
            this.meanDistance = meanDistance;
        }

        private void setChamferDistance(double chamferDistance) {
            this.chamferDistance = chamferDistance;
        }

        /**
         * Creates mirrored face, next used for symmetry precision computation
         */
        private void createMirrorFace() {
            if (!face.hasSymmetryPlane()) {
                System.out.println("ERROR: No symmetry plane");
                this.hasSymmetry = false;
                return;
            }

            this.hasSymmetry = true;

            MeshModel clone = MeshFactory.cloneMeshModel(face.getMeshModel());

            for (MeshFacet facet : clone.getFacets()) { // flip the mesh over the symmetry plane
                facet.getVertices().parallelStream()
                        .forEach(v -> v.getPosition().set(face.getSymmetryPlane().reflectPointOverPlane(v.getPosition())));
            }

            this.mirrorFace = HumanFaceFactory.create(clone, "tmpId", -1L, false);
        }

        private void registerMirrorFace() {
            IcpServices.transform(
                    mirrorFace.getMeshModel(),
                    new IcpConfig(
                            face.getMeshModel(),
                            100,
                            false,
                            0.01,
                            new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 500),
                            0)
            );
        }

        /**
         * Calculates Hausdorff, Mean and Chamfer symmetry distance
         */
        private void calculateDistances() {
            setHausdorffDistance(calculateDistance(FaceDistanceServices.DistanceAggregation.HAUSDORFF));
            setMeanDistance(calculateDistance(FaceDistanceServices.DistanceAggregation.MEAN));
            setChamferDistance(calculateDistance(FaceDistanceServices.DistanceAggregation.CHAMFER));
        }

        /**
         * Calculates distance of original and mirrored face
         */
        private double calculateDistance(FaceDistanceServices.DistanceAggregation aggregation) {
            return FaceDistanceServices.calculateBiDistance(
                    this.face,
                    this.mirrorFace,
                    false,
                    true,
                    MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS,
                    null,
                    aggregation);
        }
    }

}
