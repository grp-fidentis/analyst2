package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.project.Project;
import cz.fidentis.analyst.project.ProjectService;
import cz.fidentis.analyst.project.impl.ProjectImpl;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;

import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.WindowManager;

/**
 * A "save project" button in the application toolbox.
 * 
 * @author Radek Oslejsek
 */
@ActionID(
        category = "File",
        id = "cz.fidentis.analyst.project.toolbox.SaveProjectAction"
)
@ActionRegistration(
        iconBase = "saveAll.gif",
        displayName = "#CTL_SaveProjectAction"
)
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 1240),
    @ActionReference(path = "Toolbars/File", position = 230)
})
@Messages("CTL_SaveProjectAction=Save Project")
public final class SaveProjectAction implements ActionListener {
    
    @Override
    public void actionPerformed(ActionEvent e) {
        ProjectWindow projectWin = (ProjectWindow) WindowManager.getDefault().findTopComponent("ProjectWindow");
        if (projectWin == null) {
            return;
        }
        
        Project project = ProjectService.INSTANCE.getOpenedProject();
        
        if (!project.hasProjectDir()) { // new project
            File dir = selectDirectory(projectWin);
            if (dir != null) {
                if (!project.setProjectDir(dir)) {
                    throw new IllegalArgumentException("Failed to set project directory");
                }
            } else { // canceled
                return;
            }
        }
        
        projectWin.getProjectPanel().saveProject();
        
        WindowManager.getDefault().getMainWindow().setTitle("FIDENTIS Analyst 2: " + project.getName());
        if (project.changed()) { // something failed
            JOptionPane.showMessageDialog(projectWin, 
                    "<html><b>Failed to save project!</b></html>",
                    "ERROR",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }
    
    protected static File selectDirectory(ProjectWindow win) {
        while (true) {
            File dir = new FileChooserBuilder(ProjectImpl.class)
                    .setTitle("Create project's directory")
                    .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                    //.setFileFilter(new FileNameExtensionFilter("json files (*.json)", "json"))
                    //.setAcceptAllFileFilterUsed(true)
                    .setDirectoriesOnly(true)
                    .setFileHiding(true)
                    .showSaveDialog();
            if (dir!= null && !dir.exists() && dir.getParentFile().exists()) {
                dir = dir.getParentFile();
            }
            
            if (dir == null) { // canceled
                return dir;
            } else if (ProjectService.INSTANCE.getProjectFile(dir).exists()) { // already existing project
                Object[] options = {"No", "Yes"};
                int answer = JOptionPane.showOptionDialog(
                        win,
                        "Do you want to overwrite an existing FIDENTIS project?",
                        "WARNING",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null, options, options[0]);
                if (answer == 1) { 
                    return dir;
                }
            } else if (dir.list() != null && dir.list().length > 0) { // non-empty directory (without project file)
                Object[] options = {"No", "Yes"};
                int answer = JOptionPane.showOptionDialog(
                        win,
                        "The selected directory is not empty.\nDo you want to store the project inside?",
                        "WARNING",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null, options, options[0]);
                if (answer == 1) { 
                    return dir;
                }
            } else {
                return dir;
            }
        }
    }
}
