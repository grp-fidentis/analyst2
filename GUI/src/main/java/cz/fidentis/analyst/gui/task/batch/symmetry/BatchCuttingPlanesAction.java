/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.fidentis.analyst.gui.task.batch.symmetry;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.drawables.DrawableCuttingPlane;
import cz.fidentis.analyst.data.face.events.HumanFaceTransformedEvent;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.gui.elements.PlaneManipulationPanel;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.data.shapes.CrossSection2D;
import cz.fidentis.analyst.engines.face.CuttingPlanesUtils;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;
import org.json.simple.JSONObject;
import org.openide.filesystems.FileChooserBuilder;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.vecmath.Point2d;
import javax.vecmath.Vector3d;
import java.awt.event.ActionEvent;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.DoubleFunction;
import java.util.stream.Collectors;

import static cz.fidentis.analyst.engines.face.CuttingPlanesUtils.*;

/**
 * Action listener for manipulation with cutting planes.
 *
 * @author Dominik Racek
 * @author Radek Oslejsek
 * @author Samuel Smoleniak
 * @author Peter Conga
 */
public class BatchCuttingPlanesAction extends ControlPanelAction<BatchCuttingPlanesPanel> implements HumanFaceListener {
    
    public static final double FEATURE_POINTS_CLOSENESS = 1.5;
    
    /*
     * Constants for directions of cutting planes.
     */
    public static final Vector3d DIRECTION_VERTICAL = new Vector3d(1, 0, 0);
    public static final Vector3d DIRECTION_HORIZONTAL = new Vector3d(0, 1, 0);
    public static final Vector3d DIRECTION_FRONT_BACK = new Vector3d(0, 0, 1);
    
    private boolean humanFaceChanged = false;
    private int sampling = 100;
    
    /**
     * Scene slot used for the rendering the selected template face
     */
    private int faceSceneSlot = -1;
    private FaceReference lastAvgFace = null;
//    private boolean mirrorCuts = false;
    
    /**
     * Constructor.
     * A new {@code ProfilesPanel} is instantiated and added to the {@code topControlPane}
     *
     * @param canvas OpenGL canvas
     * @param task Faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public BatchCuttingPlanesAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);

        setControlPanel(new BatchCuttingPlanesPanel(this, task));
        FaceStateServices.updateBoundingBox(getPrimaryFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        getControlPanel().getCurveRenderingPanel().setFaceBoundingBox(getPrimaryFace().getBoundingBox());
        
        createDefaultPlanes();

        setShowHideCode(() -> { // SHOW
                    getControlPanel().updateDatasetMenu();
                    getCanvas().getScene().setDefaultColors();
                    if (humanFaceChanged) {
                        getScene().clearCuttingPlanes();
                        createDefaultPlanes();
                        getControlPanel().getCurveRenderingPanel().setFaceBoundingBox(getPrimaryFace().getBoundingBox());
                        humanFaceChanged = false;
                    }
                    FaceReference avgFace = TaskService.INSTANCE.getAvgFace(task);
                    if (avgFace != null && (lastAvgFace == null || !lastAvgFace.equals(avgFace))) {
                        lastAvgFace = avgFace;
                        createNewPlanes();
                    }

                    showCheckedPlanes();
                    showSelectedFace();
                    computeCrossSections(getCuttingPlanes(getControlPanel().getCurveRenderingPanel().getCurrentCuttingPlane()));
                },
                () -> { // HIDE
                    hideAllPlanes();
                    hideSelectedFace();
                }
        );

        // Be informed about changes in faces performed by other GUI elements
        getPrimaryDrawableFace().getHumanFace().registerListener(this);
        if (getSecondaryDrawableFace() != null) {
            getSecondaryDrawableFace().getHumanFace().registerListener(this);
        }

        hideAllPlanes();
        setDefaultPlane();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        
        switch (action) {
            case BatchCuttingPlanesPanel.ACTION_COMMAND_EXPORT:
                exportConfiguration();
                exportCurve(getControlPanel().getCurveRenderingPanel().get2DPrimaryCurves().get(0),
                        "Export primary face cross-section curve to file");
//                if (getControlPanel().isMirrorCutsChecked()) {
//                    exportProfile(this.primaryMirrorCurve, "Export primary face mirror profile to file");
//                }

                if (getSecondaryDrawableFace() != null) {
                    exportCurve(getControlPanel().getCurveRenderingPanel().get2DPrimaryCurves().get(0),
                            "Export secondary face cross-section curve to file");
//                    if (getControlPanel().isMirrorCutsChecked()) {
//                        exportProfile(this.secondaryMirrorCurve, "Export secondary face mirror profile to file");
//                    }
                }
                break;
                
            case BatchCuttingPlanesPanel.ACTION_VIEW_VERTICAL:
                getControlPanel().getCurveRenderingPanel().setCurrentCuttingPlane(DIRECTION_VERTICAL);
                computeCrossSections(getCuttingPlanes(DIRECTION_VERTICAL));
                break;
                
            case BatchCuttingPlanesPanel.ACTION_VIEW_HORIZONTAL:
                getControlPanel().getCurveRenderingPanel().setCurrentCuttingPlane(DIRECTION_HORIZONTAL);
                computeCrossSections(getCuttingPlanes(DIRECTION_HORIZONTAL));
                break;
                
            case BatchCuttingPlanesPanel.ACTION_VIEW_FRONT:
                getControlPanel().getCurveRenderingPanel().setCurrentCuttingPlane(DIRECTION_FRONT_BACK);
                computeCrossSections(getCuttingPlanes(DIRECTION_FRONT_BACK));
                break;
                
            case BatchCuttingPlanesPanel.ACTION_COMPUTE_HAUSDORFF:
                if (getSecondaryDrawableFace() == null) {
                    return;
                }
                //computeHausdorffDistances(getControlPanel().getCurveRenderingPanel().get2DAverageCurves(),
                //        getControlPanel().getCurveRenderingPanel().get2DAverageCurves());
                break;
            case BatchCuttingPlanesPanel.ACTION_INITIALIZE_MANUPULATION_PANEL:
                createPlane();
                break;
            case BatchCuttingPlanesPanel.ACTION_CHANGE_SAMPLING:
                sampling = getControlPanel().getSubsamplingStrength();
                break;
            case BatchCuttingPlanesPanel.ACTION_COMMAND_SHOW_SELECTED_FACE:
                showSelectedFace();
                computeCrossSections(getCuttingPlanes(getControlPanel().getCurveRenderingPanel().getCurrentCuttingPlane()));
                break;
            default:
                // do nothing
        }
        renderScene();
    }
    
    @Override
    public void acceptEvent(HumanFaceEvent event) {
        // If some human face is transformed, current cutting planes are deleted
        // and new default planes are created after focusing Cutting Planes Panel.
        
        if ((event instanceof HumanFaceTransformedEvent) && (((HumanFaceTransformedEvent) event).isFinished())) {
            humanFaceChanged = true;
        }
    }
    
    /**
     * Creates and returns a file of given type.
     * @param title title of file chooser window
     * @param extension file type
     * @return created file or null if not successful.
     */
    private File createFileType(String title, String extension) {
        File file = new FileChooserBuilder(BatchCuttingPlanesAction.class)
                .setTitle(title)
                .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                .setFilesOnly(true)
                .setFileFilter(new FileNameExtensionFilter(extension + " files",extension))
                .setAcceptAllFileFilterUsed(true)
                .showSaveDialog();

        if (file == null) {
            return null;
        }

        // If chosen file exists, use the exact file path
        // If chosen file does not exist and does not have an extension, add it
        if (!file.exists()) {
            if (!file.getAbsolutePath().endsWith("." + extension)) {
                file = new File(file.getAbsolutePath() + "." + extension);
            }
        }
        
        return file;
    }
    
    /**
     * Exports current configuration of planes from {@code CuttingPlanesPanel}
     * to .json file.
     */
    private void exportConfiguration() {
        File file = createFileType("Export Cutting Planes Panel configuration to file", "json");
        
        BatchCuttingPlanesPanel controlPanel = getControlPanel();

        List<CuttingPlaneConfig> cuttingPlaneConfigs = controlPanel.getPlaneManipulationPanels().stream()
                .map(p -> new CuttingPlaneConfig(p.getCurrentPlaneType(), p.getPlaneOrientation(), p.isVisible(),
                        p.getSlider().getValue()))
                .toList();
        
        JSONObject configuration = new JSONObject(getConfigExport(controlPanel.getSubsamplingStrength(),
                controlPanel.getCurveRenderingPanel().getCurrentCuttingPlane(), cuttingPlaneConfigs));

        try {
            file.createNewFile();
            try (PrintWriter writer = new PrintWriter(file.getAbsoluteFile(), "UTF-8")) {
                writer.print(configuration.toJSONString());
            }
        } catch (IOException ex) {
            System.out.println("ERROR writing to a file: " + ex);
        }
    }

    /**
     * Exports a curve to a new .csv file.
     * @param curve 2D curve
     * @param title title for file-choosing window
     */
    private void exportCurve(CrossSection2D curve, String title) {
        File file = createFileType(title, "csv");

        try {
            file.createNewFile();
            try (PrintWriter writer = new PrintWriter(file.getAbsoluteFile(), "UTF-8")) {
                List<Point2d> points = curve.getCurveSegments().stream()
                        .flatMap(Collection::stream)
                        .toList();
                writeCurveExport(points, writer);
            }
        } catch (IOException ex) {
            System.out.println("ERROR writing to a file: " + ex);
        }
    }
    
    private void createPlane() {
        List<PlaneManipulationPanel> planeManipulationPanels = getControlPanel().getPlaneManipulationPanels();
        planeManipulationPanels.get(planeManipulationPanels.size() - 1).setCanvasAndFace(getCanvas(), getControlPanel().getSelectedFace());
    }

    /**
     * Creates new cutting planes for all PlaneManipulationPanels
     */
    private void createNewPlanes() {
        List<PlaneManipulationPanel> planeManipulationPanels = getControlPanel().getPlaneManipulationPanels();
        for (PlaneManipulationPanel pmp : planeManipulationPanels) {
            pmp.setCanvasAndFace(getCanvas(), getControlPanel().getSelectedFace());
            pmp.createBboxCuttingPlane(pmp.getPlaneOrientation());
        }
    }
    
    /**
     * Initializes all cutting plane panels in the scene with default planes 
     * computed from bbox. Selects default plane.
     */
    private void createDefaultPlanes() {
        List<PlaneManipulationPanel> planeManipulationPanels = getControlPanel().getPlaneManipulationPanels();
        for (int i = 0; i < planeManipulationPanels.size() / 3; ++i) {
            planeManipulationPanels.get(i).setCanvasAndFace(getCanvas(), getControlPanel().getSelectedFace());
            planeManipulationPanels.get(i).createBboxCuttingPlane(DIRECTION_VERTICAL);
            
            planeManipulationPanels.get(i + 1).setCanvasAndFace(getCanvas(), getControlPanel().getSelectedFace());
            planeManipulationPanels.get(i + 1).createBboxCuttingPlane(DIRECTION_HORIZONTAL);
            
            planeManipulationPanels.get(i + 2).setCanvasAndFace(getCanvas(), getControlPanel().getSelectedFace());
            planeManipulationPanels.get(i + 2).createBboxCuttingPlane(DIRECTION_FRONT_BACK);
        }
    }
    
    /**
     * Sets first vertical plane as default. Selects its checkbox (hausdorff and 
     * cross-section is automatically computed), hides the plane because profiles
     * panel is not focused.
     */
    private void setDefaultPlane() {
        PlaneManipulationPanel defaultPlanePanel = getControlPanel().getPlaneManipulationPanels().get(0);
        defaultPlanePanel.selectCheckBox();
        defaultPlanePanel.getCurrentPlane().show(false);
    }

    /**
     * Computes cross-sections with all given visible planes, stores them in {@code CurveRenderingPanel}.
     * @param cuttingPlanes list of cutting planes to compute intersections with
     */
    private void computeCrossSections(List<DrawableCuttingPlane> cuttingPlanes) {
        List<CrossSectionPlane> crossSectionPlanes = cuttingPlanes.stream()
                .map(c -> new CrossSectionPlane(
                        c.isShown(),
                        new Vector3d(c.getNonShiftedPlane().getNormal()),
                        c.getPlane()))
                .toList();
        List<List<CrossSection2D>> curvesOfSelectedFace = CuttingPlanesUtils.computeCrossSections(crossSectionPlanes, getControlPanel().getSelectedFace(),
                null, FEATURE_POINTS_CLOSENESS);
        List<List<CrossSection2D>> curvesOfFaces = new ArrayList<>();
        List<FaceReference> faces = getControlPanel().getTask().getFaces();
        for (int i = 0; i < faces.size(); i++) {
            HumanFace face = FaceService.INSTANCE.getFaceByReference(faces.get(i));
            curvesOfFaces.add(CuttingPlanesUtils.computeCrossSections(crossSectionPlanes, face,
                    null, FEATURE_POINTS_CLOSENESS).get(0));
        }
        
        getControlPanel().getCurveRenderingPanel().setCurves(
                findNearestNeighbors(curvesOfSelectedFace.get(0), curvesOfFaces),
                List.of());
    }
    
    private List<CrossSection2D> findNearestNeighbors(List<CrossSection2D> avg, List<List<CrossSection2D>> faces) {
        List<CrossSection2D> reducedCurves = new ArrayList<>();
        for (int i = 0; i < avg.size(); i++) {
            if (avg.get(i) == null) {
                continue;
            }
            CrossSection2D avgCurve = avg.get(i).subSample(sampling);
            reducedCurves.add(avgCurve);
            List<CrossSection2D> curves = new ArrayList<>();
            for (List<CrossSection2D> curvesOfFace : faces) {
                curves.add(curvesOfFace.get(i));
            }

            AtomicInteger counter = new AtomicInteger(0);
            avgCurve.getCurveSegments().parallelStream().forEach(avgCurveSegment -> {
                avgCurveSegment.stream().forEach(avgPoint -> {
                    int curCounter = counter.getAndIncrement();
                    if (curCounter < avgCurveSegment.size() - 1) {
                        // Find nearest neighbors in parallel
                        List<Point2d> nearestNeighbors = curves.parallelStream()
                            .map(curve -> curve.getCurveSegments().stream()
                            .flatMap(Collection::stream)
                            .min(Comparator.comparingDouble(point -> Math.sqrt(Math.pow((point.x - avgPoint.x), 2) + Math.pow((point.y - avgPoint.y), 2))))
                            .orElse(null))
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList());
                        
                        Line2D normal = avgCurve.getNormals().get(curCounter);
                        
                        double maxDistance = Double.MIN_VALUE;
                        double minDistance = Double.MAX_VALUE;
                        for (Point2d p : nearestNeighbors) {
                            double dx = p.x - avgPoint.x;
                            double dy = p.y - avgPoint.y;
                            double relativeDistance = dx * normal.getX2() + dy * normal.getY2();
                            
                            maxDistance = relativeDistance > maxDistance ? relativeDistance : maxDistance;
                            minDistance = relativeDistance < minDistance ? relativeDistance : minDistance;
                        }
                        
                        normal.setLine(
                            normal.getX1() + (normal.getX2() * minDistance),
                            normal.getY1() + (normal.getY2() * minDistance),
                            normal.getX1() + (normal.getX2() * maxDistance),
                            normal.getY1() + (normal.getY2() * maxDistance)
                        );

                        avgCurve.setNormalAt(curCounter, normal);
                    }
                });
            });
        }
        return reducedCurves;
    }
    
    /**
     * Computes Hausdorff distances of all visible curves. 
     * @param primaryCurves cross sections with primary face
     * @param secondaryCurves cross sections with secondary face
     */
    protected void computeHausdorffDistances(List<CrossSection2D> primaryCurves, List<CrossSection2D> secondaryCurves) {
        if (secondaryCurves.isEmpty()) {
            return;
        }

        List<Double> hausdorffDistances = getHausdorffDistances(getControlPanel().getSubsamplingStrength(),
                primaryCurves, secondaryCurves);

        String hidden = "not visible";
        DoubleFunction<String> toRoundedValue = value -> String.format("%,.3f", value);
        String distance1 = !hausdorffDistances.isEmpty() ? toRoundedValue.apply(hausdorffDistances.get(0)) : hidden;
        String distance2 = hausdorffDistances.size() > 1 ? toRoundedValue.apply(hausdorffDistances.get(1)) : hidden;
        String distance3 = hausdorffDistances.size() > 2 ? toRoundedValue.apply(hausdorffDistances.get(2)) : hidden;

        //getControlPanel().setHausdorffDistances(distance1, distance2, distance3);
    }

    /**
     * Returns a list of cutting planes with given normal vector.
     * @param direction normal vector indicating direction of cutting plane
     * @return list of cutting planes in given direction
     */
    protected List<DrawableCuttingPlane> getCuttingPlanes(Vector3d direction) {
        List<DrawableCuttingPlane> currentCuttingPlanes = new ArrayList<>();
        List<PlaneManipulationPanel> planeManipulationPanels = getControlPanel().getPlaneManipulationPanels();
        for (PlaneManipulationPanel panel : planeManipulationPanels) {
            if (direction == null || direction.equals(panel.getPlaneOrientation())) {
                currentCuttingPlanes.add(panel.getCurrentPlane());
            }
        }
        return currentCuttingPlanes;
    }
    
    /**
     * Shows cutting planes marked as visible for user.
     */
    protected void showCheckedPlanes() {
        getControlPanel().getPlaneManipulationPanels().stream()
                .filter(panel -> panel.isCheckBoxSelected())
                .map(panel -> panel.getCurrentPlane())
                .forEach(plane -> plane.show(true));
    }
    
    protected void hideAllPlanes() {
        getControlPanel().getPlaneManipulationPanels().stream()
                .map(panel -> panel.getCurrentPlane())
                .forEach(plane -> plane.show(false));
    }
    
    protected void changeSampling(int sampling) {
        if (sampling < 1) {
            this.sampling = 1;
        } else if (sampling > 100) {
            this.sampling = 100;
        } else {
            this.sampling = sampling;
        }
    }
    
    private void showSelectedFace() {
        HumanFace face = getControlPanel().getSelectedFace();
        if (face != null) {
            if (faceSceneSlot == -1) {
                faceSceneSlot = getScene().getFreeSlotForFace();
            }
            getScene().setDrawableFace(faceSceneSlot, face);
            getScene().setFaceAsPrimary(faceSceneSlot);
            getScene().showDrawableFace(faceSceneSlot, true);
        }
    }

    private void hideSelectedFace() {
        if (faceSceneSlot >= 0) {
            getScene().showDrawableFace(faceSceneSlot, false);
        }
    }

}
