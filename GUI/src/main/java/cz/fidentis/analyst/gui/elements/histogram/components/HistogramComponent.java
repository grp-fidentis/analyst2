package cz.fidentis.analyst.gui.elements.histogram.components;

import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.gui.elements.histogram.scales.LinearScale;
import cz.fidentis.analyst.gui.elements.histogram.scales.Log1pScale;
import cz.fidentis.analyst.gui.elements.histogram.ActionEmitter;
import cz.fidentis.analyst.gui.elements.histogram.BucketHistogram;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Objects;
import javax.swing.*;

/**
 * High level interactive histogram bean-component.
 *
 * @author Jakub Nezval
 */
public class HistogramComponent extends JPanel {

    /*
     * Actions triggered by this panel and served by the associated action listener
     */
    public static final String ACTION_COMMAND_BOUNDS_MOVED = "histogram bounds moved";

    public static final String SEL_SCALE_LINEAR = "Linear";
    public static final String SEL_SCALE_LOGARITHMIC = "Logarithmic";
    
    private final ActionEmitter actionEmitter = new ActionEmitter();
    private final BucketHistogram bucketHistogram;
    private final ChartPainter chartPainter;
    private final LinearScale linearScale = new LinearScale();

    private Bound lowerBound, upperBound;
    
    /**
     * Creates new form {@link HistogramComponent}.
     */
    public HistogramComponent() {
        initComponents();
        this.bucketHistogram = new BucketHistogram(this.chart.getWidth() + 1);

        this.prepareBounds();

        this.chartPainter = new ChartPainter(this.chart, this.lowerBound.getValue());
        this.chartPainter.setGradientColors(
                ChartPainter.getGradientFrom(HeatMap3D.MIN_COLOR, HeatMap3D.MAX_COLOR, 7)
        );

        this.linearScale.setDomain(
                (double) (this.lowerBound.getValue() + 1),
                (double) (this.upperBound.getValue() - 1)
        );

        this.prepareBoundPairListener();
        this.prepareScaleComboBox();
    }

    private void prepareBounds() {
        this.lowerBound = new Bound(this.aHandle, this.aIndicator, this.aGhost);
        this.upperBound = new Bound(this.bHandle, this.bIndicator, this.bGhost);

        var minBoundValue = this.lowerBound.getValue();
        var maxBoundValue = this.upperBound.getValue();

        this.lowerBound.setBoundRange(minBoundValue, maxBoundValue);
        this.upperBound.setBoundRange(minBoundValue, maxBoundValue);
    }

    private void prepareBoundPairListener() {
        var boundPairListener = new BoundPairListener(
                this.lowerBound, this.upperBound,
                lb -> this.lowerBound = lb, ub -> this.upperBound = ub,
                this.chartPainter,
                e -> actionEmitter.triggerActionEvent(
                        new ActionEvent(
                                e.getSource(),
                                ActionEvent.ACTION_PERFORMED,
                                HistogramComponent.ACTION_COMMAND_BOUNDS_MOVED
                        )
                )
        );

        this.lowerBound.addActionListener(boundPairListener);
        this.upperBound.addActionListener(boundPairListener);
    }

    private void prepareScaleComboBox() {
        scaleComboBox.setModel(new DefaultComboBoxModel<>(new String[]{
                SEL_SCALE_LINEAR,
                SEL_SCALE_LOGARITHMIC
        }));

        scaleComboBox.addActionListener(
                e -> {
                    switch ((String) Objects.requireNonNull(scaleComboBox.getSelectedItem())) {
                        case SEL_SCALE_LINEAR:
                            this.chart.setScale(new LinearScale());
                            break;
                        case SEL_SCALE_LOGARITHMIC:
                            this.chart.setScale(new Log1pScale());
                            break;
                        default:
                            break;
                    }
                }
        );
    }

    /**
     * Set the values that the histogram will use.
     * Ignores values that aren't considered finite by {@link Double#isFinite(double)} predicate.
     *
     * @param values values
     */
    public void setValues(Collection<Double> values) {
        this.bucketHistogram.setValues(
                values
                        .stream()
                        .filter(Double::isFinite)
                        .toList()
        );

        this.chart.setValues(
                this.bucketHistogram.getBuckets()
                        .stream()
                        .mapToDouble(n -> (double) n)
                        .boxed()
                        .toList()
        );

        this.linearScale.setRange(this.bucketHistogram.getMinValue(), this.bucketHistogram.getMaxValue());
    }

    /**
     * @return lower value from the duo of bound-sliders
     * @see HistogramComponent#getUpperBoundValue()
     */
    public double getLowerBoundValue() {
        return this.linearScale.map((double) this.lowerBound.getValue());
    }

    /**
     * @return higher value from the duo of bound-sliders
     * @see HistogramComponent#getLowerBoundValue()
     */
    public double getUpperBoundValue() {
        return this.linearScale.map((double) this.upperBound.getValue());
    }

    /**
     * Moves bounds to their original position.
     */
    public void resetBounds() {
        this.lowerBound.reset();
        this.upperBound.reset();

        if (this.lowerBound.getValue() > this.upperBound.getValue()) {
            var tmp = lowerBound;
            lowerBound = upperBound;
            upperBound = tmp;
        }

        this.chartPainter.paintChart(this.lowerBound.getValue(), this.upperBound.getValue());
    }

    /**
     * Delegate of {@link ActionEmitter}.
     *
     * @param listener listener
     * @return {@code true} if {@code listener} was successfully added
     * @see ActionEmitter#addActionListener(ActionListener)
     */
    public boolean addActionListener(ActionListener listener) {
        return this.actionEmitter.addActionListener(listener);
    }

    /**
     * Delegate of {@link ActionEmitter}.
     *
     * @param listener listener
     * @return {@code true} if {@code listener} was successfully removed
     * @see ActionEmitter#removeActionListener(ActionListener)
     */
    public boolean removeActionListener(ActionListener listener) {
        return this.actionEmitter.removeActionListener(listener);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel titledPanel = new javax.swing.JPanel();
        javax.swing.JPanel nullLayout = new javax.swing.JPanel();
        aHandle = new javax.swing.JButton();
        bHandle = new javax.swing.JButton();
        aIndicator = new javax.swing.JPanel();
        bIndicator = new javax.swing.JPanel();
        aGhost = new javax.swing.JPanel();
        bGhost = new javax.swing.JPanel();
        chart = new cz.fidentis.analyst.gui.elements.histogram.components.HistogramChartPanel();
        javax.swing.JLabel scaleLabel = new javax.swing.JLabel();
        scaleComboBox = new javax.swing.JComboBox<>();

        titledPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(HistogramComponent.class, "HistogramComponent.titledPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Liberation Sans", 1, 15))); // NOI18N

        nullLayout.setLayout(null);

        org.openide.awt.Mnemonics.setLocalizedText(aHandle, org.openide.util.NbBundle.getMessage(HistogramComponent.class, "HistogramComponent.aHandle.text")); // NOI18N
        aHandle.setCursor(new java.awt.Cursor(java.awt.Cursor.W_RESIZE_CURSOR));
        nullLayout.add(aHandle);
        aHandle.setBounds(6, 112, 15, 45);

        org.openide.awt.Mnemonics.setLocalizedText(bHandle, org.openide.util.NbBundle.getMessage(HistogramComponent.class, "HistogramComponent.bHandle.text")); // NOI18N
        bHandle.setCursor(new java.awt.Cursor(java.awt.Cursor.W_RESIZE_CURSOR));
        nullLayout.add(bHandle);
        bHandle.setBounds(601, 243, 15, 45);

        aIndicator.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout aIndicatorLayout = new javax.swing.GroupLayout(aIndicator);
        aIndicator.setLayout(aIndicatorLayout);
        aIndicatorLayout.setHorizontalGroup(
            aIndicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        aIndicatorLayout.setVerticalGroup(
            aIndicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        nullLayout.add(aIndicator);
        aIndicator.setBounds(13, 0, 1, 400);

        bIndicator.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout bIndicatorLayout = new javax.swing.GroupLayout(bIndicator);
        bIndicator.setLayout(bIndicatorLayout);
        bIndicatorLayout.setHorizontalGroup(
            bIndicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        bIndicatorLayout.setVerticalGroup(
            bIndicatorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        nullLayout.add(bIndicator);
        bIndicator.setBounds(608, 0, 1, 400);

        aGhost.setBackground(new java.awt.Color(255, 255, 0));

        javax.swing.GroupLayout aGhostLayout = new javax.swing.GroupLayout(aGhost);
        aGhost.setLayout(aGhostLayout);
        aGhostLayout.setHorizontalGroup(
            aGhostLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        aGhostLayout.setVerticalGroup(
            aGhostLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        nullLayout.add(aGhost);
        aGhost.setBounds(13, 0, 1, 400);

        bGhost.setBackground(new java.awt.Color(255, 255, 0));

        javax.swing.GroupLayout bGhostLayout = new javax.swing.GroupLayout(bGhost);
        bGhost.setLayout(bGhostLayout);
        bGhostLayout.setHorizontalGroup(
            bGhostLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        bGhostLayout.setVerticalGroup(
            bGhostLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        nullLayout.add(bGhost);
        bGhost.setBounds(608, 0, 1, 400);

        javax.swing.GroupLayout chartLayout = new javax.swing.GroupLayout(chart);
        chart.setLayout(chartLayout);
        chartLayout.setHorizontalGroup(
            chartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 594, Short.MAX_VALUE)
        );
        chartLayout.setVerticalGroup(
            chartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 390, Short.MAX_VALUE)
        );

        nullLayout.add(chart);
        chart.setBounds(14, 5, 594, 390);

        org.openide.awt.Mnemonics.setLocalizedText(scaleLabel, org.openide.util.NbBundle.getMessage(HistogramComponent.class, "HistogramComponent.scaleLabel.text")); // NOI18N

        javax.swing.GroupLayout titledPanelLayout = new javax.swing.GroupLayout(titledPanel);
        titledPanel.setLayout(titledPanelLayout);
        titledPanelLayout.setHorizontalGroup(
            titledPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(titledPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scaleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scaleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(351, Short.MAX_VALUE))
            .addComponent(nullLayout, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        titledPanelLayout.setVerticalGroup(
            titledPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(titledPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nullLayout, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(titledPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(scaleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(scaleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titledPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titledPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel aGhost;
    private javax.swing.JButton aHandle;
    private javax.swing.JPanel aIndicator;
    private javax.swing.JPanel bGhost;
    private javax.swing.JButton bHandle;
    private javax.swing.JPanel bIndicator;
    private cz.fidentis.analyst.gui.elements.histogram.components.HistogramChartPanel chart;
    private javax.swing.JComboBox<String> scaleComboBox;
    // End of variables declaration//GEN-END:variables
}
