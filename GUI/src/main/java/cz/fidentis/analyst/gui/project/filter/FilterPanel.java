package cz.fidentis.analyst.gui.project.filter;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.gui.task.ControlPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Matej Kovar
 */
public class FilterPanel extends ControlPanel {
    
    public static final String ACTION_APPLY_FILTER = "Apply filter";
    public static final String ACTION_CLEAR_FILTERS = "Clear filters";
    public static final String ACTION_REVERT_FILTERING ="Revert filtering";
    
    private final List<Landmark> selectedFeaturePoints = new ArrayList<>();
    
    private final List<FilterSection> sections = new ArrayList<>();
    
    public static final String ICON = "filter_off28x28.png";
    //public static final String ICON_FILTER_ON = "filter_on28x28.png";
    public static final String NAME = "Filter";
    
    /**
     * Constructor.
     * @param action Action listener
     */
    public FilterPanel(ActionListener action) {
        super(action);
        
        setName(NAME);
        initComponents();
        setFilteringState(true);
        
        featurePointsFilterPanel1.initComponents();
        initSections();
        
        applyFilterButton.addActionListener((ActionEvent e) -> { 
            action.actionPerformed(new ActionEvent(
                        this,
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_APPLY_FILTER
                )
            );
        });
        
        clearFilteringButton.addActionListener((ActionEvent e) -> { 
            action.actionPerformed(new ActionEvent(
                        this,
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_CLEAR_FILTERS
                )
            );
        });
        
        revertLastFilteringButton.addActionListener((ActionEvent e) -> { 
            action.actionPerformed(new ActionEvent(
                        this,
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_REVERT_FILTERING
                )
            );
        });
    }
    
    @Override
    public ImageIcon getIcon() {
        return getStaticIcon();
    }
    
    /**
     * Static implementation of the {@link #getIcon()} method.
     * 
     * @return Control panel icon
     */
    public static ImageIcon getStaticIcon() {
        return new ImageIcon(FilterPanel.class.getClassLoader().getResource("/" + ICON));
    }
    
    /**
     * Determines whether the face name filter is on or off
     * @param negate negate the result
     * @return {@code true} if filter is on
     */
    public String getFaceNameFilter(boolean negate) {
        if (negate) { // has not the text filter
            return hasNotTextCheckbox.isSelected() ? hasNotTextTextfield.getText() : null;
        } else {
            return hasTextCheckbox.isSelected() ? hasTextTextfield.getText() : null;
        }
    }
    
    /**
     * Determines whether the "any FP" filter is turned on or off
     * @return {@code true} if the filter is selected.
     */
    public boolean isAnyFpFilterEnabled() {
        return this.hasFPCheckbox.isSelected();
    }
    
    /**
     * Determines whether the "non FP" filter is turned on or off
     * @return {@code true} if the filter is selected.
     */
    public boolean isNoneFpFilterEnabled() {
        return this.hasNotFPCheckbox.isSelected();
    }
    
    /**
     * Determines whether the "selected FP" filter is turned on or off
     * @return {@code true} if the filter is selected.
     */
    public boolean isSelectedFpFilterEnabled() {
        return this.hasSelectedFPCheckbox.isSelected();
    }
    
    /**
     * Sets the filtering state
     * @param emptyHistory determines whether the filtering history is empty
     */
    public final void setFilteringState(boolean emptyHistory) {
        clearFilteringButton.setEnabled(!emptyHistory);
        revertLastFilteringButton.setEnabled(!emptyHistory);
    }

    /**
     * Returns selected FPs
     * @return selected FPs
     */
    public List<Landmark> getSelectedFeaturePoints() {
        return Collections.unmodifiableList(selectedFeaturePoints);
    }
    
    private void initSections() {
        FilterSection featurePointsSection = new FilterSection();
        featurePointsSection.add(hasFPCheckbox, null);
        featurePointsSection.add(hasSelectedFPCheckbox, featurePointsFilterPanel1);
        featurePointsSection.add(hasNotFPCheckbox, null);
        sections.add(featurePointsSection);
        
        FilterSection nameSection = new FilterSection();
        nameSection.add(hasTextCheckbox, hasTextTextfield);
        nameSection.add(hasNotTextCheckbox, hasNotTextTextfield);
        sections.add(nameSection);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        hasTextCheckbox = new javax.swing.JCheckBox();
        hasTextTextfield = new javax.swing.JTextField();
        hasNotTextCheckbox = new javax.swing.JCheckBox();
        hasNotTextTextfield = new javax.swing.JTextField();
        applyFilterButton = new javax.swing.JButton();
        clearFilteringButton = new javax.swing.JButton();
        revertLastFilteringButton = new javax.swing.JButton();
        byFPLabel = new javax.swing.JLabel();
        hasFPCheckbox = new javax.swing.JCheckBox();
        hasSelectedFPCheckbox = new javax.swing.JCheckBox();
        hasNotFPCheckbox = new javax.swing.JCheckBox();
        featurePointsPanel = new javax.swing.JScrollPane();
        featurePointsFilterPanel1 = new cz.fidentis.analyst.gui.project.filter.FeaturePointsFilterPanel();
        byNameLabel = new javax.swing.JLabel();

        hasTextCheckbox.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(hasTextCheckbox, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.hasTextCheckbox.text")); // NOI18N

        hasTextTextfield.setText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.hasTextTextfield.text")); // NOI18N
        hasTextTextfield.setEnabled(false);

        hasNotTextCheckbox.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(hasNotTextCheckbox, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.hasNotTextCheckbox.text")); // NOI18N

        hasNotTextTextfield.setText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.hasNotTextTextfield.text")); // NOI18N
        hasNotTextTextfield.setEnabled(false);

        applyFilterButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(applyFilterButton, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.applyFilterButton.text_1")); // NOI18N

        clearFilteringButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(clearFilteringButton, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.clearFilteringButton.text_1")); // NOI18N
        clearFilteringButton.setEnabled(false);

        org.openide.awt.Mnemonics.setLocalizedText(revertLastFilteringButton, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.revertLastFilteringButton.text")); // NOI18N
        revertLastFilteringButton.setToolTipText(org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.revertLastFilteringButton.toolTipText")); // NOI18N
        revertLastFilteringButton.setEnabled(false);

        byFPLabel.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(byFPLabel, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.byFPLabel.text")); // NOI18N

        hasFPCheckbox.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(hasFPCheckbox, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.hasFPCheckbox.text")); // NOI18N

        hasSelectedFPCheckbox.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(hasSelectedFPCheckbox, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.hasSelectedFPCheckbox.text")); // NOI18N

        hasNotFPCheckbox.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(hasNotFPCheckbox, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.hasNotFPCheckbox.text")); // NOI18N

        featurePointsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.featurePointsPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        featurePointsPanel.setEnabled(false);

        featurePointsFilterPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12))); // NOI18N

        javax.swing.GroupLayout featurePointsFilterPanel1Layout = new javax.swing.GroupLayout(featurePointsFilterPanel1);
        featurePointsFilterPanel1.setLayout(featurePointsFilterPanel1Layout);
        featurePointsFilterPanel1Layout.setHorizontalGroup(
            featurePointsFilterPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 261, Short.MAX_VALUE)
        );
        featurePointsFilterPanel1Layout.setVerticalGroup(
            featurePointsFilterPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 401, Short.MAX_VALUE)
        );

        featurePointsPanel.setViewportView(featurePointsFilterPanel1);

        byNameLabel.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(byNameLabel, org.openide.util.NbBundle.getMessage(FilterPanel.class, "FilterPanel.byNameLabel.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(applyFilterButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(clearFilteringButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(revertLastFilteringButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(85, 85, 85))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(hasNotTextCheckbox, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(hasNotTextTextfield, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(byNameLabel)
                            .addComponent(byFPLabel)
                            .addComponent(hasFPCheckbox, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hasSelectedFPCheckbox, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hasNotFPCheckbox, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(hasTextCheckbox, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(hasTextTextfield, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(featurePointsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(applyFilterButton)
                    .addComponent(clearFilteringButton)
                    .addComponent(revertLastFilteringButton))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(byFPLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(hasFPCheckbox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(hasSelectedFPCheckbox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(hasNotFPCheckbox)
                        .addGap(18, 18, 18)
                        .addComponent(byNameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hasTextCheckbox)
                            .addComponent(hasTextTextfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hasNotTextCheckbox)
                            .addComponent(hasNotTextTextfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(featurePointsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
            
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyFilterButton;
    private javax.swing.JLabel byFPLabel;
    private javax.swing.JLabel byNameLabel;
    private javax.swing.JButton clearFilteringButton;
    private cz.fidentis.analyst.gui.project.filter.FeaturePointsFilterPanel featurePointsFilterPanel1;
    private javax.swing.JScrollPane featurePointsPanel;
    private javax.swing.JCheckBox hasFPCheckbox;
    private javax.swing.JCheckBox hasNotFPCheckbox;
    private javax.swing.JCheckBox hasNotTextCheckbox;
    private javax.swing.JTextField hasNotTextTextfield;
    private javax.swing.JCheckBox hasSelectedFPCheckbox;
    private javax.swing.JCheckBox hasTextCheckbox;
    private javax.swing.JTextField hasTextTextfield;
    private javax.swing.JButton revertLastFilteringButton;
    // End of variables declaration//GEN-END:variables

}
