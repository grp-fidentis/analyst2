package cz.fidentis.analyst.gui.app.tools.opencl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingVisitor;
import cz.fidentis.analyst.opencl.OpenCLServices;

import java.io.IOException;
import java.util.List;

/**
 * Benchmark comparing performance between CPU-based and GPU-based Poisson sub-sampling implementations
 *
 * @author Marek Horský
 */
public class PoissonReuseBenchmark {
    private static final int ITERATIONS = 50;
    private static final int ITERATION_OFFSET = 5; // First n iterations are ignored to account for power management OS scheduling
    private static final int SAMPLES_START = 1000;
    private static final int SAMPLES_STEP = 0;
    private static final String PATH_MAIN = "C:\\Users\\marek\\Desktop\\FidentisFaces\\problem-scan\\00297_01_ECA.obj";

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        MeshModel model = BenchmarkUtils.getModel(PATH_MAIN);
        if (!OpenCLServices.isOpenCLAvailable()) {
            System.out.println("OpenCL could not find supported platform. Benchmark terminated");
            return;
        }
        BenchmarkUtils.printGPU();
        long timeGPU = visitorBenchmark(model.getFacets(), getVisitor(PointSamplingConfig.Method.POISSON_OPENCL));
        //long timeCPU = visitorBenchmark(model.getFacets(), getVisitor(PointSamplingConfig.Method.POISSON));
        BenchmarkUtils.printComparison(0, timeGPU, ITERATIONS);
    }

    private static long visitorBenchmark(List<MeshFacet> facets, PointSamplingVisitor visitor) {
        int samples = SAMPLES_START;
        long resultTime = 0;
        facets.forEach(visitor::visitMeshFacet);

        for (int i = 0; i < ITERATIONS + ITERATION_OFFSET; i++) {
            visitor.setRequiredSamples(samples);
            long time = System.nanoTime();
            visitor.getSamples();
            time = System.nanoTime() - time;

            if (i >= ITERATION_OFFSET) {
                resultTime += time;
                samples += SAMPLES_STEP;
            }
        }
        visitor.dispose();
        return resultTime;
    }

    private static PointSamplingVisitor getVisitor(PointSamplingConfig.Method method) {
        return new PointSamplingConfig(method, SAMPLES_START).getVisitor();
    }
}
