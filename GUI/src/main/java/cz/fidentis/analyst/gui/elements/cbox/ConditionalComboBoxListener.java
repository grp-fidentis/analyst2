package cz.fidentis.analyst.gui.elements.cbox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A listener responsible for selecting an enabled combo box item 
 * when a disabled item is selected.
 * 
 * @author Radek Oslejsek
 */
public class ConditionalComboBoxListener implements ActionListener {

    private final SelectableComboBox combobox;
    private Object oldItem;

    /**
     * Constructor.
     * 
     * @param combobox A selectable combo box.
     */
    public ConditionalComboBoxListener(SelectableComboBox combobox) {
        this.combobox = combobox;
        combobox.setSelectedIndex(combobox.firstTrueItem());
        oldItem = combobox.getSelectedItem();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object selectedItem = combobox.getSelectedItem();
        if (!((ConditionalItem) selectedItem).isEnabled()) {
            combobox.setSelectedItem(oldItem);
        } else {
            oldItem = selectedItem;
        }
    }

}
