package cz.fidentis.analyst.gui.project.faceinfo;

import java.util.List;

/**
 *
 * @author Matej Kovar
 */
public class FaceGeometryInfo {
    
    private String verticesInfo;
    private String facetsInfo;
    private List<String> featurePointsInfo;

    /**
     * Face geometry info of face
     * @param verticesInfo String number of vertices
     * @param facetsInfo String number of facets
     * @param featurePointsInfo List of feature points
     */
    public FaceGeometryInfo(String verticesInfo, String facetsInfo, List<String> featurePointsInfo) {
        this.verticesInfo = verticesInfo;
        this.facetsInfo = facetsInfo;
        this.featurePointsInfo = featurePointsInfo;
    }

    public String getVerticesInfo() {
        return verticesInfo;
    }

    public void setVerticesInfo(String verticesInfo) {
        this.verticesInfo = verticesInfo;
    }

    public String getFecetsInfo() {
        return facetsInfo;
    }

    public void setFecetsInfo(String fecetsInfo) {
        this.facetsInfo = fecetsInfo;
    }

    public List<String> getFeaturePointsInfo() {
        return featurePointsInfo;
    }

    public void setFeaturePointsInfo(List<String> featurePointsInfo) {
        this.featurePointsInfo = featurePointsInfo;
    }
   
    
}
