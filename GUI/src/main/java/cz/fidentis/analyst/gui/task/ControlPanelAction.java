package cz.fidentis.analyst.gui.task;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.drawables.DrawableInteractiveMask;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;
import cz.fidentis.analyst.rendering.Scene;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;

/**
 * Default action listener (controller) used to connect specific control panel 
 * with the canvas.
 * 
 * @author Radek Oslejsek
 * @param <T> the type of control panel
 */
public abstract class ControlPanelAction<T extends ControlPanel> extends AbstractAction {
    
    /*
     * Top component - the space delimited for placing control panels
     */
    private final JTabbedPane topControlPane;
    
    /**
     * Control panel inside the topControlPane
     */
    private T controlPanel;
    
    /**
     * Canvas component
     */
    private final Canvas canvas;
    
    /**
     * Faces processed by current analytical task
     */
    private final Task task;
    
    /**
     * Constructor. 
     * 
     * @param canvas OpenGL canvas
     * @param task Task with faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     * @throws IllegalArgumentException if some param is missing
     */
    public ControlPanelAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        if (canvas == null) {
            throw new IllegalArgumentException("canvas");
        }
        if (task == null) {
            throw new IllegalArgumentException("task");
        }
        if (topControlPane == null) {
            throw new IllegalArgumentException("topControlPane");
        }
        this.task = task;
        this.canvas = canvas;
        this.topControlPane = topControlPane;
    }
    
    @Override
    public abstract void actionPerformed(ActionEvent ae);
    
    /**
     * Popups (shows) this control panel.
     */
    public void popup() {
        topControlPane.setSelectedComponent(getControlPanel()); 
    }
    
    protected Canvas getCanvas() {
        return canvas;
    }
    
    /**
     * Sets (replaces) the control panel.
     * 
     * @param controlPanel a control panel
     */
    protected void setControlPanel(T controlPanel) {
        if (controlPanel == null) {
            throw new IllegalArgumentException("controlPanel");
        }
        this.controlPanel = controlPanel;
        topControlPane.addTab(getControlPanel().getName(), getControlPanel().getIcon(), getControlPanel());
    }
    
    protected T getControlPanel() {
        return this.controlPanel;
    }
    
    /**
     * Provide a code that is run when the control panel is focused (popped up, shown) 
     * and unfocused (closed or hidden by another control panel)
     * 
     * @param visible A code to be run when the control panel is focused
     * @param hidden A code to be run when the control panel hides
     */
    protected void setShowHideCode(Runnable visible, Runnable hidden) {
        topControlPane.addChangeListener(e -> {
            if (((JTabbedPane) e.getSource()).getSelectedComponent().equals(controlPanel)) { 
                visible.run(); // If the panel is focused
            } else { 
                hidden.run(); // If another panel is focused
            }
        });
    }
    
    protected Task getTask() {
        return this.task;
    }
    
    /**
     * Return the primary face
     * @return the primary face 
     */
    protected HumanFace getPrimaryFace() {
        FaceReference primaryFace = TaskService.INSTANCE.getPrimaryFace(task);
        return FaceService.INSTANCE.getFaceByReference(primaryFace);
    }
    
    /**
     * Return the secondary face
     * @return the secondary face 
     */
    protected HumanFace getSecondaryFace() {
        FaceReference secondaryFace = TaskService.INSTANCE.getSecondaryFace(task);
        return FaceService.INSTANCE.getFaceByReference(secondaryFace);

    }
    
    /**
     * The generic code for showing/hiding the control panel
     * 
     * @param ae Action event
     */
    protected void hideShowPanelActionPerformed(ActionEvent ae, ControlPanel controlPanel) {
        if (((JToggleButton) ae.getSource()).isSelected()) {
            topControlPane.addTab(controlPanel.getName(), controlPanel.getIcon(), controlPanel);
            topControlPane.setSelectedComponent(controlPanel); // focus
        } else {
            topControlPane.remove(controlPanel);
        }
    }
    
    protected Scene getScene() {
        return canvas.getScene();
    }
    
    protected DrawableFace getPrimaryDrawableFace() {
        return (canvas.getScene() != null) 
                ? canvas.getScene().getDrawableFace(canvas.getScene().getPrimaryFaceSlot()) 
                : null;
    }
    
    protected DrawableFace getSecondaryDrawableFace() {
        return (canvas.getScene() != null) 
                ? canvas.getScene().getDrawableFace(canvas.getScene().getSecondaryFaceSlot()) 
                : null;
    }
    
    protected DrawableFeaturePoints getPrimaryFeaturePoints() {
        return (canvas.getScene() != null) 
                ? canvas.getScene().getDrawableFeaturePoints(canvas.getScene().getPrimaryFaceSlot()) 
                : null;
    }
    
    protected DrawableFeaturePoints getSecondaryFeaturePoints() {
        return (canvas.getScene() != null) 
                ? canvas.getScene().getDrawableFeaturePoints(canvas.getScene().getSecondaryFaceSlot()) 
                : null;
    }
    
    protected DrawableInteractiveMask getPrimaryInteractiveMask() {
        return (canvas.getScene() != null) 
                ? canvas.getScene().getDrawableInteractiveMask(canvas.getScene().getPrimaryFaceSlot()) 
                : null;
    }
    
    protected DrawableInteractiveMask getSecondaryInteractiveMask() {
        return (canvas.getScene() != null) 
                ? canvas.getScene().getDrawableInteractiveMask(canvas.getScene().getSecondaryFaceSlot()) 
                : null;
    }

    protected void renderScene() {
        canvas.renderScene();
    }
    
}
