package cz.fidentis.analyst.gui.elements.cbox;

/**
 * A combo box item with "is enabled" state.
 * 
 * @author Radek Oslejsek
 */
public class ConditionalItem {

    private Object object;
    private boolean isEnabled;

    /**
     * Constructor.
     * 
     * @param object Item content
     * @param isEnabled The state
     */
    public ConditionalItem(Object object, boolean isEnabled) {
        this.object = object;
        this.isEnabled = isEnabled;
    }

    /**
     * Constructor.
     * 
     * @param object Item content
     */
    public ConditionalItem(Object object) {
        this(object, true);
    }

    /**
     * Returns the state.
     * 
     * @return the state
     */
    public boolean isEnabled() {
        return isEnabled;
    }

    /**
     * Sets the state.
     * 
     * @param isEnabled Desired state.
     */
    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    @Override
    public String toString() {
        return object.toString();
    }
}
