package cz.fidentis.analyst.gui.project.table;

import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * Content of column with the list of tasks where the face is open.
 * 
 * @author Matej Kovar
 */
public class TaskCellEditor extends AbstractCellEditor implements TableCellEditor {

    private JComboBox comboBox;
    
    /**
     * Constructor.
     */
    public TaskCellEditor() {
        comboBox = new JComboBox();
        //comboBox.setOpaque(true);
        //comboBox.setBackground(Color.white);
    }
    
    @Override
    public Object getCellEditorValue() {
        return comboBox;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        comboBox = ((JComboBox)value);
        return comboBox;
    }
    
}
