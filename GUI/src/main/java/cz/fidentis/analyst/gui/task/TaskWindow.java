package cz.fidentis.analyst.gui.task;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.gui.task.batch.distance.BatchDistanceAction;
import cz.fidentis.analyst.gui.task.batch.registration.BatchRegistrationAction;
import cz.fidentis.analyst.gui.task.curvature.CurvatureAction;
import cz.fidentis.analyst.gui.task.distance.DistanceAction;
import cz.fidentis.analyst.gui.task.faceinfo.FaceOverviewAction;
import cz.fidentis.analyst.gui.task.featurepoints.FeaturePointsAction;
import cz.fidentis.analyst.gui.task.interactivemask.InteractiveMaskAction;
import cz.fidentis.analyst.gui.task.registration.RegistrationAction;
import cz.fidentis.analyst.gui.task.symmetry.CuttingPlanesAction;
import cz.fidentis.analyst.gui.task.symmetry.SymmetryAction;
import cz.fidentis.analyst.project.*;
import cz.fidentis.analyst.rendering.Camera;
import cz.fidentis.analyst.rendering.Scene;
import cz.fidentis.analyst.toolbar.BatchToolbar;
import cz.fidentis.analyst.toolbar.DoubleFaceToolbar;
import cz.fidentis.analyst.toolbar.SingleFaceToolbar;
import org.openide.windows.TopComponent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Objects;

/**
 * The non-singleton window/tab for the analysis of one, two or many to many faces
 *
 * @author Matej Kovar
 */
@TopComponent.Description(
        preferredID = "TaskWindow",
        iconBase = "analysis.png",
        persistenceType = TopComponent.PERSISTENCE_NEVER
)
public class TaskWindow extends TopComponent {

    private final Canvas canvas;
    private final TaskControlPane controlPanel;
    private final JScrollPane scrollPane;

    private final transient Task task;

    private final transient ActionListener endTaskListener;

    private boolean askWhenClosing = true;

    /**
     * Constructor.
     *
     * @param task           faces of analytical task
     * @param endTaskListener action endTaskListener
     */
    public TaskWindow(Task task, ActionListener endTaskListener) {
        this.task = task;
        this.canvas = new Canvas();
        this.endTaskListener = endTaskListener;

        initCanvas();

        controlPanel = new TaskControlPane();
        scrollPane = new JScrollPane(controlPanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(30);

        setName(task.getName());
        initComponents();

        // change the height so that it corresponds to the height of the OpenGL window
        canvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                scrollPane.setSize(ControlPanel.CONTROL_PANEL_WIDTH, canvas.getHeight());
            }
        });

        controlPanel.addChangeListener(e -> getCanvas().renderScene());

        initTabs();
    }

    /**
     * Determines whether a dialogue window is opened when the window is closing.
     *
     * @param ask If {@code true}, then dialogue window pups up
     */
    public void setAskWhenClosing(boolean ask) {
        this.askWhenClosing = ask;
    }

    @Override
    public boolean canClose() {
        if (!askWhenClosing) {
            return true;
        }
        int answer = JOptionPane.showConfirmDialog(
                this,
                "Do you really want to end this analytical task?\nChanges will be discarded!",
                "",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE
        );

        if (answer == JOptionPane.YES_OPTION && super.canClose()) {
            // notify project panel about closing me
            endTaskListener.actionPerformed(new ActionEvent(
                    this,
                    ActionEvent.ACTION_PERFORMED,
                    ""
            ));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_NEVER;
    }

    /**
     * Checks whether this tab contains a face with given name
     *
     * @param face String name of face
     * @return true if face with this name is in this tab
     */
    public boolean hasFace(FaceReference face) {
        for (int i = 0; i < task.getFaces().size(); i++) {
            if (task.getFaces().get(i).equals(face)) {
                return true;
            }
        }
        return false;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public Task getTask() {
        return this.task;
    }

    /**
     * Sets camera to canvas
     *
     * @param camera Camera
     */
    public void setCamera(Camera camera) {
        this.canvas.setCamera(camera);
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TaskWindow other = (TaskWindow) obj;

        return Objects.equals(this.getName(), other.getName());
    }

    private void initCanvas() {
        // todo
        if (task.getType() == TaskType.SINGLE_FACE_ANALYSIS || task.getType() == TaskType.PAIR_COMPARISON) { // initialize primary face
            Scene scene = canvas.getScene();
            int index = scene.getFreeSlotForFace();
            HumanFace primaryFace = FaceService.INSTANCE.getFaceByReference(TaskService.INSTANCE.getPrimaryFace(task));
            scene.setHumanFace(index, primaryFace);
            scene.setFaceAsPrimary(index);
            primaryFace.registerListener(canvas);
        }

        if (task.getType() == TaskType.PAIR_COMPARISON) { // initialize secondary face
            Scene scene = canvas.getScene();
            int index = scene.getFreeSlotForFace();
            HumanFace secondaryFace = FaceService.INSTANCE.getFaceByReference(TaskService.INSTANCE.getSecondaryFace(task));
            scene.setHumanFace(index, secondaryFace);
            scene.setFaceAsSecondary(index);
            secondaryFace.registerListener(canvas);
        }

        canvas.getScene().setDefaultColors();

        switch (task.getType()) {
            case SINGLE_FACE_ANALYSIS:
                canvas.addToolBox(new SingleFaceToolbar(canvas, task));
                break;
            case PAIR_COMPARISON:
                canvas.addToolBox(new DoubleFaceToolbar(canvas, task));
                break;
            case BATCH_PROCESSING:
                canvas.addToolBox(new BatchToolbar(canvas, task));
                break;
            default:
                throw new IllegalArgumentException("Unsupported task type: " + task.getType());
        }

        canvas.getCamera().zoomToFit(canvas.getScene());
    }

    private void initComponents() {
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(canvas, GroupLayout.DEFAULT_SIZE, 651, Short.MAX_VALUE)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
//                                .addComponent(renderingToolBar, GroupLayout.PREFERRED_SIZE, RenderingToolBar.WIDTH, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(
                                                scrollPane,
                                                ControlPanel.CONTROL_PANEL_WIDTH,
                                                ControlPanel.CONTROL_PANEL_WIDTH,
                                                ControlPanel.CONTROL_PANEL_WIDTH
                                        )
                        )
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createBaselineGroup(true, true)
                                                .addComponent(canvas)
//                                        .addComponent(renderingToolBar)
                                                .addComponent(scrollPane)
                                ))
        );
    }

    private void initTabs() {
        switch (task.getType()) {
            case SINGLE_FACE_ANALYSIS:
                new SymmetryAction(getCanvas(), task, controlPanel).popup();
                new CuttingPlanesAction(getCanvas(), task, controlPanel);
                new FeaturePointsAction(getCanvas(), task, controlPanel);
                new CurvatureAction(getCanvas(), task, controlPanel);
                new FaceOverviewAction(getCanvas(), task, controlPanel, 0);
                new InteractiveMaskAction(getCanvas(), task, controlPanel);
                break;
            case PAIR_COMPARISON:
                new RegistrationAction(canvas, task, controlPanel).popup();
                new DistanceAction(canvas, task, controlPanel);
                new CuttingPlanesAction(canvas, task, controlPanel);
                new FaceOverviewAction(getCanvas(), task, controlPanel, 0);
                new FaceOverviewAction(getCanvas(), task, controlPanel, 1);
                //new InteractiveMaskAction(getCanvas(), faces, controlPanel);
                break;
            case BATCH_PROCESSING: // batch mode
                new BatchRegistrationAction(canvas, task, controlPanel);
                new BatchDistanceAction(canvas, task, controlPanel);
                //new BatchCuttingPlanesAction(canvas, batchFacesProxy, controlPanel);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + task.getType());
        }
    }

}
