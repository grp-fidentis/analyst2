package cz.fidentis.analyst.gui.elements.cbox;

import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

/**
 * A combobox adapted to show enabled/disabled items.
 * 
 * @author Radek Oslejsek
 */
public class SelectableComboBox extends JComboBox<Object> {

    private ActionListener listener;

    /**
     * Constructor.
     */
    public SelectableComboBox() {
        this.setRenderer(new ConditionalComboBoxRenderer());
        listener = new ConditionalComboBoxListener(this);
        this.addActionListener(listener);
    }

    /**
     * Adds an enabled item into the combo box
     * 
     * @param str Text of the item
     */
    public void addItem(String str) {
        addItem(new ConditionalItem(str, true));
    }

    /**
     * Adds an item into the combobox in either enabled or disabled state,
     * 
     * @param str Text of the item
     * @param enabled The state 
     */
    public void addItem(String str, boolean enabled) {
        addItem(new ConditionalItem(str, enabled));
    }

    /**
     * Add an existing item into the combobox.
     * 
     * @param ci An existing item
     */
    public void addItem(Component ci) {
        this.add(ci);
        this.setRenderer(new ConditionalComboBoxRenderer());
        this.addActionListener(new ConditionalComboBoxListener(this));
    }

    /**
     * If the combobox contains {@code str}, sets its state to [@code bool}.
     * If it's not yet an item, ignores it. The method also re-sets the selected item to the first
     * one shown in the list as "true", and disables the listeners in this
     * process to avoid firing an action when reorganizing the list.
     * 
     * @param str Text of the existing item
     * @param enabled New state
     */
    public void setItem(String str, boolean enabled) {
        int n = this.getItemCount();
        for (int i = 0; i < n; i++) {
            if (this.getItemAt(i).toString().equals(str)) {

                this.removeActionListener(listener);
                this.removeItemAt(i);
                this.insertItemAt(new ConditionalItem(str, enabled), i);
                int k = this.firstTrueItem();
                if (k < 0) {
                    k = 0; // default index 0 if no true item is shown as true
                }
                this.setSelectedIndex(k);
                this.addActionListener(listener);

                return;
            }
        }
        System.err.println("Warning: item " + str + " is not a member of this combobox: ignoring it...");
    }

    protected Object[] getItems() {
        int n = this.getItemCount();
        Object[] obj = new Object[n];
        for (int i = 0; i < n; i++) {
            obj[i] = this.getItemAt(i);
        }
        return obj;
    }

    /**
     * @return -1 if no item is true
     */
    protected int firstTrueItem() {
        int i = 0;
        for (Object obj : this.getItems()) {
            if (((ConditionalItem) obj).isEnabled()) {
                return i;
            }
            i++;
        }
        return -1;
    }
}
