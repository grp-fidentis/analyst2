package cz.fidentis.analyst.gui.task.batch.registration;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.*;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistration;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationConfig;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices;
import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;
import cz.fidentis.analyst.project.TaskService;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * A concurrent task that registers multiple faces to selected template face. Simultaneously, it computes
 * average face. The exact computation parameters are taken from the
 * {@link BatchRegistrationPanel}.
 *
 * @author Radek Oslejsek
 */
public class BatchRegistrationTask extends SwingWorker<MeshModel, HumanFace> {

    private final ProgressDialog<MeshModel,HumanFace> progressDialog;
    private final BatchRegistrationPanel controlPanel;
    private final Canvas canvas;

    private int faceSceneSlot = -1; // scene slot to show the transformed face

    private final Stopwatch totalTime = new Stopwatch("Total computation time:\t");
    private final Stopwatch registrationTime = new Stopwatch("Registration time:\t");
    private final Stopwatch loadTime = new Stopwatch("Disk access time:\t");
    private final Stopwatch kdTreeConstructionTime = new Stopwatch("KD trees construction time:\t");
    private final Stopwatch octreeConstructionTime = new Stopwatch("Octree construction time:\t");

    /**
     * Constructor.
     *
     * @param progressDialog A window that show the progress of the computation. Must not be {@code null}
     * @param controlPanel A control panel with computation parameters. Must not be {@code null}
     * @param canvas Canvas with a 3D scene.
     */
    public BatchRegistrationTask(ProgressDialog<MeshModel,HumanFace> progressDialog, BatchRegistrationPanel controlPanel, Canvas canvas) {
        this.progressDialog = progressDialog;
        this.controlPanel = controlPanel;
        this.canvas = canvas;
    }

    @Override
    protected MeshModel doInBackground() throws Exception {
        setHumanFaceFactoryParams();

        totalTime.start();

        // Get initial (template) face. Either the average face is chosen if exists (we want to improve previous iteration),
        // or the selected face is chosen (the first iteration).
        // We don't need to reload the initFace periodically for two reasons:
        //   - It is never dumped from memory to disk because we use MRU
        //   - Even if dumped, the face keeps in the memory until we hold the pointer to it
        HumanFace initFace = getTemplateFace();

        // Iterate through all faces to register them and/or compute new average face.
        // Process all faces, including the selected one, because the template can vary.
        BatchFaceRegistrationConfig config = new BatchFaceRegistrationConfig(
                controlPanel.getRegistrationStrategy(),
                controlPanel.getAvgFaceStrategy(),
                controlPanel.scaleIcp(),
                controlPanel.getIcpUndersampling(),
                0.05,
                100,
                1,
                canvas.getGLCanvas().getContext());
        BatchFaceRegistration batchFaceRegistration = BatchFaceRegistrationServices.initRegistration(initFace, config);

        List<FaceReference> faceReferences = controlPanel.getTask().getFaces();
        for (int i = 0; i < faceReferences.size(); i++) {
            FaceReference faceReference = faceReferences.get(i);
            if (isCancelled()) {
                return null;
            }

            loadTime.start();
            HumanFace superimposedFace = FaceService.INSTANCE.getFaceByReference(faceReference); // Load face to be processed and possibly transformed
            loadTime.stop();

            registrationTime.start();
            batchFaceRegistration.register(superimposedFace);
            registrationTime.stop();

            publish(superimposedFace);       // update progress bar and possibly render the transformed face
            progressDialog.setValue((int) Math.round(100.0 * (i + 1.0) / faceReferences.size())); // set progress value
        }

        HumanFace avgFace = null;
        if (config.avgFaceStrategy() != BatchFaceRegistrationServices.AverageFaceStrategy.NONE) { // create new human face from the average face geometry
            avgFace = createAvgFace(batchFaceRegistration.getAverageMesh());
        }

        totalTime.stop();
        printTimeStats();

        return (avgFace == null) ? null : avgFace.getMeshModel();
    }

    @Override
    protected void done() {
        progressDialog.dispose(); // close progress bar
        if (isCancelled()) {
            TaskService.INSTANCE.replaceAverageFace(controlPanel.getTask(), null);
        }
    }

    @Override
    protected void process(List<HumanFace> chunks) {
        chunks.forEach(f -> {
            if (isCancelled()) {
                return;
            }
            if (faceSceneSlot == -1) {
                faceSceneSlot = canvas.getScene().getFreeSlotForFace();
            }
            canvas.getScene().setDrawableFace(faceSceneSlot, f);
            canvas.getScene().getDrawableFace(faceSceneSlot).setTransparency(0.5f);
            canvas.getScene().getDrawableFace(faceSceneSlot).setColor(DrawableFace.SKIN_COLOR_SECONDARY);
            canvas.renderScene();
        });
    }
    protected void printTimeStats() {
        Logger.print(registrationTime.toString());
        Logger.print(loadTime.toString());
        Logger.print(kdTreeConstructionTime.toString());
        Logger.print(octreeConstructionTime.toString());
        Logger.print(totalTime.toString());
    }
    
    protected HumanFace createAvgFace(MeshModel avgMesh) throws IOException {
        File tempFile = File.createTempFile(this.getClass().getSimpleName(), ".obj");
        tempFile.deleteOnExit();
        FaceReference faceReference = FaceService.INSTANCE.createFaceFromMeshModel(avgMesh, tempFile, true);
        HumanFace averageFace = FaceService.INSTANCE.getFaceByReference(faceReference);
        MeshIO.exportMeshModel(averageFace.getMeshModel(), tempFile);

        TaskService.INSTANCE.replaceAverageFace(controlPanel.getTask(), faceReference);
        return averageFace;
    }

    protected void setHumanFaceFactoryParams() {
        FaceService.INSTANCE.setDumpStrategy(HumanFaceMemoryManager.Strategy.MRU);
    }

    protected HumanFace getTemplateFace() {
        loadTime.start();
        HumanFace ret = controlPanel.getTask().getAverageFace() != null
                ? FaceService.INSTANCE.getFaceByReference(controlPanel.getTask().getAverageFace())
                : controlPanel.getSelectedFace();
        loadTime.stop();
        return ret;
    }
}
