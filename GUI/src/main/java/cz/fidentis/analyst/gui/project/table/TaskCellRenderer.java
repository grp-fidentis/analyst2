package cz.fidentis.analyst.gui.project.table;

import java.awt.Component;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * A renderer for columns the list of face's open tasks
 * 
 * @author Matej Kovar
 */
public class TaskCellRenderer extends JComboBox implements TableCellRenderer {
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        //setOpaque(true);
        setSelectedItem(value);
        //this.setForeground(this.getBackground());
        //setBackground(Color.white);
        return this;
    }

}
