/**
 * Control panel for the manipulation and management of feature points 
 * and other landmarks.
 */
package cz.fidentis.analyst.gui.task.featurepoints;
