package cz.fidentis.analyst.gui.task.batch.distance;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceMemoryManager;
import cz.fidentis.analyst.engines.face.batch.distance.BatchFaceDistanceServices;
import cz.fidentis.analyst.engines.face.batch.distance.BatchIndirectDistance;
import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;

import java.util.List;

/**
 * A task that computes distance-based similarity of the set of faces.
 * 
 * @author Radek Oslejsek
 */
public class BatchIndirectDistanceTask extends BatchDistanceTask {
    
    private final Stopwatch totalTime = new Stopwatch("Total computation time:\t\t");
    private final Stopwatch distComputationTime = new Stopwatch("Distance computation time:\t");
    private final Stopwatch loadTime = new Stopwatch("Disk access time:\t\t");
    private final Stopwatch finalDistComputationTime = new Stopwatch("Distance finalization time:\t");

    private final BatchFaceDistanceServices.DistanceStrategy distanceStrategy;
    private final GLContext context;

    /**
     * Constructor.
     * 
     * @param progressDialog A window that show the progress of the computation. Must not be {@code null}
     * @param controlPanel A control panel with computation parameters. Must not be {@code null}
     * The average face is skipped from the similarity measurement (i.e., we do not measure its distance to other distances)
     * @param distanceStrategy A strategy of measuring indirect distance
     * @param context Active OpenGL context on which makeCurrent() can be called.
     *                If this parameter is not {@code null}, then GPU can be used for acceleration.
     *                Otherwise, CPU is used instead.
     */
    public BatchIndirectDistanceTask(
            ProgressDialog<Void, Integer> progressDialog,
            BatchDistancePanel controlPanel,
            BatchFaceDistanceServices.DistanceStrategy distanceStrategy,
            GLContext context) {

        super(progressDialog, controlPanel);
        this.distanceStrategy = distanceStrategy;
        this.context = context;
    }

    @Override
    protected Void doInBackground() throws Exception {

        FaceService.INSTANCE.setDumpStrategy(HumanFaceMemoryManager.Strategy.MRU);
        List<FaceReference> faceReferences = getControlPanel().getTask().getFaces();

        totalTime.start();

        // We don't need to reload the selected "gauge" face periodically for two reasons:
        //   - It is never dumped from memory to disk because we use MRU
        //   - Even if dumped, the face keeps in the memory until we hold the pointer to it
        loadTime.start();
        setBatchDistance(BatchFaceDistanceServices.initIndirectMeasurement(distanceStrategy, getSelectedFace(), faceReferences.size(), context));
        loadTime.stop();

        for (int i = 0; i < faceReferences.size(); i++) {
            if (isCancelled()) { // the user canceled the process
                return null;
            }

            loadTime.start();
            HumanFace face = FaceService.INSTANCE.getFaceByReference(faceReferences.get(i));
            loadTime.stop();

            distComputationTime.start();
            ((BatchIndirectDistance) getBatchDistance()).addToMeasurement(face);
            distComputationTime.stop();

            // update progress bar
            getProgressDialog().setValue((int) Math.round(100.0 * (i+1) / faceReferences.size()));
        }

        finalDistComputationTime.start();
        ((BatchIndirectDistance) getBatchDistance()).measure();
        finalDistComputationTime.stop();

        totalTime.stop();

        printTimeStats();

        return null;
    }

    protected HumanFace getSelectedFace() {
        return getControlPanel().getSelectedFace();
    }

    protected void printTimeStats() {
        Logger.print(distComputationTime.toString());
        Logger.print(finalDistComputationTime.toString());
        Logger.print(loadTime.toString());
        Logger.print(totalTime.toString());
    }
}
