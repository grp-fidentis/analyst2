package cz.fidentis.analyst.gui.task.distance;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.drawables.DrawableFpWeights;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.engines.face.FaceDistanceServices;
import cz.fidentis.analyst.data.face.events.HumanFaceTransformedEvent;
import cz.fidentis.analyst.data.face.events.MeshDistanceComputed;
import cz.fidentis.analyst.gui.elements.FpListWeightsPanel;
import cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.gui.task.LoadedActionEvent;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.rendering.RenderingMode;
import org.openide.filesystems.FileChooserBuilder;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Action listener for computation of the Hausdorff distance.
 * <p>
 * Besides the UX logic, this object stores the parameters and results of 
 * Hausdorff distance and provide them to other parts of GUI. 
 * <p>
 * This object also serves as {@code HumanFaceListener}.
 * It means that it is invoked whenever one of the faces are changed and then can 
 * react to these changes, e.g., the distance automatically recomputed whenever 
 * the faces are transformed.
 * <p>
 * Changes made by these objects are announced to other listeners.
 * Following events are triggered:
 * <ul>
 * <li>{@code MeshDistanceComputed}</li>
 * </ul>
 * 
 * @author Radek Oslejsek
 * @author Daniel Schramm
 * @author Jakub Nezval
 */
public class DistanceAction extends ControlPanelAction<DistancePanel> implements HumanFaceListener {
    
    /*
     * Attributes handling the state
     */
    private MeshDistances meshDistance = null;

    private final Map<Landmark, Double> selectedFeaturePoints;

    private String strategy = DistancePanel.SEL_STRATEGY_POINT_TO_TRIANGLE;
    private boolean relativeDist = false;
    private String heatmapDisplayed = DistancePanel.SEL_SHOW_STD_DISTANCE;
    private String weightedFPsShow = DistancePanel.SEL_SHOW_NO_SPHERES;
    private boolean crop = true;
    
    private Landmark hoveredFeaturePoint = null;
    
    private final DrawableFpWeights fpSpheres;
    
    private static final Color  FEATURE_POINT_HIGHLIGHT_COLOR = Color.MAGENTA;
    private static final Color  FEATURE_POINT_HOVER_COLOR = Color.CYAN;
    
    /**
     * Constructor.
     * A new {@code DistancePanel} is instantiated and added to the {@code topControlPane}
     * 
     * @param canvas OpenGL canvas
     * @param faces Faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public DistanceAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);

        // Select and pre-set all feature points with default weight (sphere size)
        selectedFeaturePoints = getSecondaryFeaturePoints().getFeaturePoints().stream()
                .collect(Collectors.toMap(
                        featurePoint -> featurePoint,
                        featurePoint -> DrawableFpWeights.FPW_DEFAULT_SIZE));

        // Create the panel with initially computed values
        setControlPanel(new DistancePanel(this, getSecondaryFeaturePoints(), selectedFeaturePoints.keySet()));
        
        // Add spheres to the scene
        fpSpheres = new DrawableFpWeights(getSecondaryFeaturePoints().getFeaturePoints());
        getCanvas().getScene().setOtherDrawable(getCanvas().getScene().getFreeSlotForOtherDrawables(), fpSpheres);
        
        setShowHideCode(
                () -> { // SHOW
                    // ... display heatmap and feature points relevant to the Hausdorff distance
                    getCanvas().getScene().setDefaultColors();
                    setVisibleSpheres();
                    computeAndUpdateDistance(false);
                    updateHeatmap();
                },
                () -> { // HIDE
                    getSecondaryDrawableFace().setHeatMap(null);
                    fpSpheres.show(false);
                }
        );
        
        // Init the panel, i.e., compute weighted HD with significant points and update GUI (hide results until the tab is open):
        actionPerformed(new ActionEvent(this, 0, DistancePanel.ACTION_COMMAND_FEATURE_POINT_SELECT_AUTO));
        fpSpheres.show(false);
        getSecondaryDrawableFace().setHeatMap(null);
        
        // Be informed about changes in faces performed by other GUI elements
        getPrimaryDrawableFace().getHumanFace().registerListener(this);
        getSecondaryDrawableFace().getHumanFace().registerListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        final String action = ae.getActionCommand();
        
        List<Landmark> featurePoints = getSecondaryFeaturePoints().getFeaturePoints();
        
        switch (action) {
            case DistancePanel.ACTION_COMMAND_SET_DISPLAYED_HEATMAP:
                heatmapDisplayed = (String) ((JComboBox<?>) ae.getSource()).getSelectedItem();
                updateHeatmap();
                break;
            case DistancePanel.ACTION_COMMAND_SHOW_HIDE_WEIGHTED_FPOINTS:
                weightedFPsShow = (String) ((JComboBox<?>) ae.getSource()).getSelectedItem();
                setVisibleSpheres();
                break;
            case DistancePanel.ACTION_COMMAND_SET_DISTANCE_STRATEGY:
                strategy = (String) ((JComboBox<?>) ae.getSource()).getSelectedItem();
                computeAndUpdateDistance(true);
                updateHeatmap();
                break;
            case DistancePanel.ACTION_COMMAND_RELATIVE_ABSOLUTE_DIST:
                this.relativeDist = Objects.equals(
                        ((JComboBox<?>) ae.getSource()).getSelectedItem(),
                        DistancePanel.SEL_RELATIVE_DISTANCE
                );
                computeAndUpdateDistance(true);
                updateHeatmap();
                break;
            case FpListWeightsPanel.ACTION_COMMAND_FEATURE_POINT_SELECT:
                selectFeaturePoint((LoadedActionEvent) ae);
                setVisibleSpheres();
                highlightSelectedSpheres();
                computeAndUpdateDistance(false);
                updateHeatmap();
                break;
            case DistancePanel.ACTION_COMMAND_FEATURE_POINT_SELECT_ALL:
                for (int i = 0; i < featurePoints.size(); i++) {
                    selectedFeaturePoints.put(featurePoints.get(i), fpSpheres.getSize(i));
                }
                getControlPanel().getFeaturePointsListPanel().selectAllFeaturePoints(true);
                setVisibleSpheres();
                highlightSelectedSpheres();
                computeAndUpdateDistance(false);
                updateHeatmap();
                break;
            case DistancePanel.ACTION_COMMAND_FEATURE_POINT_SELECT_NONE:
                selectedFeaturePoints.clear();
                getControlPanel().getFeaturePointsListPanel().selectAllFeaturePoints(false);
                setVisibleSpheres();
                highlightSelectedSpheres();
                computeAndUpdateDistance(false);
                updateHeatmap();
                break;
            case DistancePanel.ACTION_COMMAND_FEATURE_POINT_SELECT_AUTO:
                for (int i = 0; i < featurePoints.size(); i++) { // select all
                    selectedFeaturePoints.put(featurePoints.get(i), fpSpheres.getSize(i));
                }
                computeAndUpdateDistance(false);
                selectSignificantFPs();
                for (int i = 0; i < featurePoints.size(); i++) { // update the checklist
                    getControlPanel().getFeaturePointsListPanel().selectFeaturePoint(
                            i,
                            selectedFeaturePoints.containsKey(featurePoints.get(i))
                    );
                }
                setVisibleSpheres();
                highlightSelectedSpheres();
                computeAndUpdateDistance(false);
                updateHeatmap();
                break;
            case DistancePanel.ACTION_COMMAND_HD_AUTO_CROP:
                this.crop = ((JCheckBox) ae.getSource()).isSelected();
                computeAndUpdateDistance(true);
                updateHeatmap();
                break;
            case FpListWeightsPanel.ACTION_COMMAND_FEATURE_POINT_HOVER_IN:
                hoverFeaturePoint((LoadedActionEvent) ae, true);
                break;
            case FpListWeightsPanel.ACTION_COMMAND_FEATURE_POINT_HOVER_OUT:
                hoverFeaturePoint((LoadedActionEvent) ae, false);
                break;
            case FpListWeightsPanel.ACTION_COMMAND_FEATURE_POINT_RESIZE:
                resizeFeaturePoint((LoadedActionEvent) ae);
                updateHeatmap(); // show changes in the heat map during resizing
                break;
            case FpListWeightsPanel.ACTION_COMMAND_DISTANCE_RECOMPUTE:
                computeAndUpdateDistance(false);
                updateHeatmap();
                break;
            case DistancePanel.ACTION_COMMAND_EXPORT_DISTANCE:
                if (meshDistance != null) {
                    exportDistances(false);
                }
                break;
            case DistancePanel.ACTION_COMMAND_EXPORT_WEIGHTED_DISTANCE:
                if (meshDistance != null) {
                    exportDistances(true);
                }
                break;
            case HistogramComponent.ACTION_COMMAND_BOUNDS_MOVED:
                updateHeatmap();
                break;
            default:
                // to nothing
        }
        renderScene();
    }

    @Override
    public void acceptEvent(HumanFaceEvent event) {
        if (event instanceof HumanFaceTransformedEvent ftEvent && event.getIssuer() != this) { // recompute (W)HD
            if (ftEvent.isFinished()) {
                meshDistance = null;
                computeAndUpdateDistance(true);
                // Relocate weight spheres:
                final List<Landmark> secondaryFPs = getSecondaryFeaturePoints().getFeaturePoints();
                final List<Landmark> weightedFPs = fpSpheres.getFeaturePoints();
                if (secondaryFPs.size() != weightedFPs.size()) {
                    Logger.print("ERROR: Landmarks and weights mismatch");
                    return;
                }
                for (int i = 0; i < secondaryFPs.size(); i++) {
                    weightedFPs.get(i).getPosition().set(secondaryFPs.get(i).getPosition());
                }
            }
        } else if (event instanceof MeshDistanceComputed hdEvent) { // update stats
            getControlPanel().updateHausdorffDistanceStats(
                    hdEvent.getMeshDistances().getDistanceStats(),
                    hdEvent.getMeshDistances().getWeightedStats());
        }
    }
    
    /**
     * Recalculates the distance and updates all GUI elements of {@link DistancePanel}.
     *
     * @param recomputeDist If {@code false}, then previously measured distance is used to update GUI elements.
     *                      Use it when only dealing with distance layers, e.g., enabling/disabling priority spheres.
     *                      If {@code true}, then new distances are calculated. Use it, when the distances have changed,
     *                      i.e., after registration.
     *
     */
    private void computeAndUpdateDistance(boolean recomputeDist) {
        // Recompute distances:
        if (recomputeDist || meshDistance == null) {
            calculateDistance(); // replaces the meshDistance

            // Update weights of priority spheres (i.e., feature points) based on new distances.
            FaceDistanceServices.setPrioritySphereMask(meshDistance, selectedFeaturePoints);

            getControlPanel().getHistogramComponent().resetBounds();
        }

        // Update weight of feature points (numbers of the right-hand side of the feature points list):
        getControlPanel().getFeaturePointsListPanel().updateFeaturePointWeights(
                FaceDistanceServices.getFeaturePointWeights(meshDistance, selectedFeaturePoints));

        // Announce that the new distances from the secondary to the primary face have been updated
        // (i.e., either completely recomputed or changed by layers):
        getSecondaryDrawableFace().getHumanFace().announceEvent(new MeshDistanceComputed(
                getSecondaryDrawableFace().getHumanFace(),
                getPrimaryDrawableFace().getHumanFace(),
                meshDistance,
                getSecondaryDrawableFace().getHumanFace().getShortName(),
                this));
    }
    
    /**
     * (Re)calculates the Hausdorff distance and updates the heat map of the secondary face
     * as well as values of all appropriate GUI elements of {@link DistancePanel}.
     */
    private void updateHeatmap() {
        FaceDistanceServices.setPrioritySphereMask(meshDistance, selectedFeaturePoints);

        var meshMap = meshDistance.distancesAsMap();
        var hist = getControlPanel().getHistogramComponent();
        var minValue = hist.getLowerBoundValue();
        var maxValue = hist.getUpperBoundValue();

        HashMap<MeshFacet, List<Double>> saturation;

        switch (heatmapDisplayed) {
            case DistancePanel.SEL_SHOW_STD_DISTANCE:
                saturation = new HashMap<>(meshMap);
                saturation.replaceAll(
                        (k ,v) -> v.stream()
                                .map(n -> minValue <= n && n <= maxValue ? 1d : 0d)
                                .toList()
                );
                hist.setVisible(true);
                getSecondaryDrawableFace().setHeatMap(new HeatMap3D(meshMap, saturation, minValue, maxValue));
                break;
            case DistancePanel.SEL_SHOW_WEIGHTED_DISTANCE:
                saturation = new HashMap<>(meshDistance.weightsAsMap());
                saturation.replaceAll(
                        (k ,v) -> v.stream()
                                .map(n -> minValue <= n && n <= maxValue ? n : 0)
                                .toList()
                );
                hist.setVisible(true);
                getSecondaryDrawableFace().setHeatMap(new HeatMap3D(meshMap, saturation, minValue, maxValue));
                break;
            case DistancePanel.SEL_SHOW_NO_HEATMAP:
                hist.setVisible(false);
                getSecondaryDrawableFace().setHeatMap(null);
                break;
            default:
                throw new UnsupportedOperationException(heatmapDisplayed);
        }
    }

    /**
     * Recalculates distances (creates new distance visitor). Logs computational time
     */
    private void calculateDistance() {
        HumanFace primaryFace = getPrimaryDrawableFace().getHumanFace();
        HumanFace secondaryFace = getSecondaryDrawableFace().getHumanFace();

        final MeshDistanceConfig.Method useStrategy = switch (strategy) {
            case DistancePanel.SEL_STRATEGY_POINT_TO_POINT -> MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS;
            case DistancePanel.SEL_STRATEGY_POINT_TO_TRIANGLE -> MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS;
            case DistancePanel.SEL_STRATEGY_RAY_CASTING -> MeshDistanceConfig.Method.RAY_CASTING;
            case DistancePanel.SEL_STRATEGY_RAY_CASTING_GPU -> MeshDistanceConfig.Method.RAY_CASTING_GPU;
            default -> throw new UnsupportedOperationException(strategy);
        };

        Logger out = Logger.measureTime();

        MeshDistanceConfig.GPUData gpuData = new MeshDistanceConfig.GPUData(primaryFace.getMeshModel().getFacets(), getCanvas().getGLCanvas().getContext());
        meshDistance = FaceDistanceServices.calculateDistance(primaryFace, secondaryFace, relativeDist, crop, useStrategy, gpuData);

        out.printDuration("Distance for models with "
                + primaryFace.getMeshModel().getNumVertices()
                + "/"
                + secondaryFace.getMeshModel().getNumVertices()
                + " vertices");

        // propagate new values to histogram
        getControlPanel().getHistogramComponent().setValues(
                meshDistance.distancesAsMap()
                        .values()
                        .stream()
                        .flatMap(Collection::stream)
                        .toList()
        );
   }

    private void selectFeaturePoint(LoadedActionEvent actionEvent) {
        final int index = (int) actionEvent.getData();
        final Landmark fp = getFeaturePoint(index);
        
        if (((JToggleButton) actionEvent.getSource()).isSelected()) {
            selectedFeaturePoints.put(fp, fpSpheres.getSize(index));
        } else {
            selectedFeaturePoints.remove(fp);
        }
    }
    
    /**
     * Changes the color of the secondary face's feature point at the given index
     * and of its weighted representation when the cursor hovers over the feature point's name.
     * The index is received as the data payload of {@code actionEvent}.
     * 
     * @param actionEvent Action event with the index of the feature point as its payload data
     * @param entered {@code true} if the cursor entered the feature point,
     *                {@code false} if the cursor left the feature point
     */
    private void hoverFeaturePoint(LoadedActionEvent actionEvent, boolean entered) {
        final int index = (int) actionEvent.getData();
        
        if (entered) { // entering a feature point
            fpSpheres.setColor(index, FEATURE_POINT_HOVER_COLOR);
            getSecondaryFeaturePoints().setColor(index, FEATURE_POINT_HOVER_COLOR);
            hoveredFeaturePoint = getFeaturePoint(index);
        } else if (selectedFeaturePoints.containsKey(hoveredFeaturePoint)) { // leaving highlighted FP
            fpSpheres.setColor(index, FEATURE_POINT_HIGHLIGHT_COLOR);
            getSecondaryFeaturePoints().resetColorToDefault(index);
        } else { // leaving ordinary FP
            fpSpheres.resetColorToDefault(index);
            getSecondaryFeaturePoints().resetColorToDefault(index);
        }
    }

    /**
     * Changes the size of the secondary face's feature point at the given index.
     * The index is received as the data payload of {@code actionEvent}.
     * 
     * @param actionEvent Action event with the index of the feature point as its payload data
     */
    private void resizeFeaturePoint(LoadedActionEvent actionEvent) {
        final int index = (int) actionEvent.getData();
        final double size = getControlPanel().getFeaturePointsListPanel().getSphereSize(index);
        
        fpSpheres.setSize(index, size);
        selectedFeaturePoints.replace(getFeaturePoint(index), size);
    }
    
    /**
     * Returns type of the feature point at the given index in the secondary face.
     * 
     * @param index Index of the feature point
     * @return Type of the feature point or {@code null}
     */
    private Landmark getFeaturePoint(int index) {
        final List<Landmark> featurePoints = getSecondaryFeaturePoints().getFeaturePoints();
        if (index < 0 || index >= featurePoints.size()) {
            return null;
        }
        return featurePoints.get(index);
    }
    
    private void highlightSelectedSpheres() {
        final List<Landmark> secondaryFPs = getSecondaryFeaturePoints().getFeaturePoints();
        if (secondaryFPs == null) {
            return;
        }
        for (int i = 0; i < secondaryFPs.size(); i++) {
            if (selectedFeaturePoints.containsKey(secondaryFPs.get(i))) {
                fpSpheres.setColor(i, FEATURE_POINT_HIGHLIGHT_COLOR);
            } else {
                fpSpheres.resetColorToDefault(i);
            }
        }
    }
    
    private void selectSignificantFPs() {
        // Compute a map of FPs with non-NaN weight
        Map<Landmark, Double> wMap = FaceDistanceServices.getFeaturePointWeights(meshDistance, selectedFeaturePoints)
                .entrySet()
                .stream()
                .filter(fpWeight -> !Double.isNaN(fpWeight.getValue())) // Filter out feature points with Double.NaN weight
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        fpType -> DrawableFpWeights.FPW_DEFAULT_SIZE));
        
        List<Landmark> featurePoints = getSecondaryFeaturePoints().getFeaturePoints();
        selectedFeaturePoints.clear();
        for (int i = 0; i < featurePoints.size(); i++) {
            Landmark fpt = featurePoints.get(i);
            if (wMap.containsKey(fpt)) {
                selectedFeaturePoints.put(fpt, fpSpheres.getSize(i));
            }
        }
    }
    
    private void setVisibleSpheres() {
        List<Landmark> featurePoints = getSecondaryFeaturePoints().getFeaturePoints();
        switch (this.weightedFPsShow) {
            case DistancePanel.SEL_SHOW_ALL_SPHERES:
                for (int i = 0; i < featurePoints.size(); i++) {
                    fpSpheres.resetAllRenderModesToDefault();
                }
                fpSpheres.show(true);
                break;
            case DistancePanel.SEL_SHOW_NO_SPHERES:
                fpSpheres.show(false);
                break;
            case DistancePanel.SEL_SHOW_SELECTED_SPHERES:
                for (int i = 0; i < featurePoints.size(); i++) {
                    if (selectedFeaturePoints.containsKey(featurePoints.get(i))) {
                        fpSpheres.setRenderMode(i, DrawableFpWeights.FPW_DEFAULT_RENDERING_MODE);
                    } else {
                        fpSpheres.setRenderMode(i, RenderingMode.HIDE);
                    }
                }
                fpSpheres.show(true);
                break;
            default:
        }
    }

    protected void exportDistances(boolean getWeightedDistances) {
        List<Double> values = getWeightedDistances
                ? meshDistance.streamOfWeightedDistances().boxed().toList()
                : meshDistance.streamOfRawDistances().boxed().toList();

        File file = new FileChooserBuilder(DistancePanel.class)
                .setTitle("Specify CSV file to save")
                .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                .setFileFilter(new FileNameExtensionFilter("CSV files (*.csv)", "csv"))
                .showSaveDialog();
        if (file == null) {
            return;
        }

        if (!file.getAbsolutePath().toLowerCase().endsWith(".csv")) {
            file = new File(file.getAbsolutePath() + ".csv");
        }

        try (BufferedWriter w = new BufferedWriter(new FileWriter(file))) {
            for (double d: values) {
                w.write(String.valueOf(d));
                w.newLine();
            }
        } catch (IOException|RuntimeException ex) {
            Logger.print(ex.toString());
        }
    }
}
