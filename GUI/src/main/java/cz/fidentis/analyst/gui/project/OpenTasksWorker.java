package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.gui.elements.ProgressDialog;
import java.util.List;
import java.util.function.Consumer;
import javax.swing.SwingWorker;

/**
 * A worker for opening multiple task tabs for a single-face analysis
 * 
 * @author Matej Kovar
 */
public class OpenTasksWorker extends SwingWorker<Boolean, Integer> {
    
    private final List<FaceReference> faces;
    private final ProgressDialog progressDialog;
    private final Consumer<FaceReference> consumer;
    
    /**
     * Constructor.
     * 
     * @param faces Files pf faces to be open in separate tabs
     * @param progressDialog A progress dialog window
     * @param consumer A method that creates an analytical tab from loaded camera and faces
     * @throws IllegalArgumentException if the project has not a folder assigned
     */
    public OpenTasksWorker(List<FaceReference> faces, ProgressDialog progressDialog, Consumer<FaceReference> consumer) {
        this.faces = faces;
        this.progressDialog = progressDialog;
        this.consumer = consumer;
    }

    @Override
    protected Boolean doInBackground() throws Exception {
        int counter = 0;
        for (FaceReference face: faces) {
            if (isCancelled()) {
                return false;
            }
            
            this.consumer.accept(face);
            
            int progress = (int)Math.round(100.0 * ++counter / faces.size());
            progressDialog.setValue(progress);
        }

        return true;
    }
    
    @Override
    protected void done() {
        progressDialog.dispose();
    }
}
