package cz.fidentis.analyst.gui.elements;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.drawables.DrawableFpWeights;
import cz.fidentis.analyst.gui.task.LoadedActionEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A list of feature points with weights. It is used for the computation of weighted Hausdorff distance.
 *
 * @author Radek Oslejsek
 * @author Daniel Schramm
 */
public class FpListWeightsPanel extends FpListAbstractPanel<FpListWeightsPanel.Row> {

    /**
     * A single raw of the list with a slider and numerical value in addition to the basic row.
     *
     * @author Radek Oslejsek
     */
    public class Row extends FpListAbstractPanel<Row>.Row {
        private final SpinSlider slider;
        private final JLabel value;

        /**
         * Constructor.
         *
         * @param featurePoint feature point
         * @param slider slider
         * @param value computed value
         */
        public Row(Landmark featurePoint, SpinSlider slider, JLabel value) {
            super(featurePoint);
            this.slider = slider;
            this.value = value;
        }

        @Override
        public void addToPanel(JPanel panel, GridBagConstraints c) {
            super.addToPanel(panel, c);
            c.anchor = GridBagConstraints.CENTER;
            add(slider, c);
            c.gridx++;
            add(value, c);
            c.gridx++;
        }
    }

    public static final String ACTION_COMMAND_FEATURE_POINT_RESIZE = "set size of feature point";
    public static final String ACTION_COMMAND_DISTANCE_RECOMPUTE = "recompute the Hausdorff distance";

    /**
     * Initiates the list of feature points. This method must be called before any other method.
     *
     * @param action Action listener that handles events appeared during the interaction with the list
     * @param drFeaturePoints Feature points to be put on the list
     * @param selectedFPs Feature points that are checked by default. Can be empty
     */
    public void initComponents(ActionListener action, DrawableFeaturePoints drFeaturePoints, Set<Landmark> selectedFPs) {
        // init GUI
        List<Row> rows = drFeaturePoints.getFeaturePoints().stream()
                .map(fp -> new Row(fp, new SpinSlider(), new JLabel()))
                .toList();

        rows.forEach(row -> {
            row.slider.initDouble(DrawableFpWeights.FPW_DEFAULT_SIZE, 0, 100, 1);
            row.slider.setContinuousSync(false);
            row.getCheckbox().setSelected(selectedFPs.contains(row.getFeaturePoint()));
        });

        super.initComponents(action, rows);

        setSliderActions();
    }

    /**
     * Updates GUI elements that display the weights of feature points
     * used to calculate the weighted Hausdorff distance.
     *
     * @param featurePointWeights Map of feature point types and their weights
     */
    public void updateFeaturePointWeights(Map<Landmark, Double> featurePointWeights) {
        getRows().forEach(row -> {
            final Double fpWeight = featurePointWeights.get(row.getFeaturePoint());
            row.value.setText(fpWeight == null ? "" : String.format("%.3f", fpWeight));
            row.value.setFont(new Font("Arial", 1, 12));
        });
    }

    /**
     * (De)selects all feature points for the computation of the weighted Hausdorff distance.
     *
     * @param selected {@code true} if all feature point checkboxes are to be selected,
     *                 {@code false} otherwise
     */
    public void selectAllFeaturePoints(boolean selected) {
        getRows().forEach(row -> row.getCheckbox().setSelected(selected));
    }

    /**
     * (De)selects given feature point.
     *
     * @param index Index of the feature point
     * @param selected {@code true} if all feature point checkboxes are to be selected,
     *                 {@code false} otherwise
     */
    public void selectFeaturePoint(int index, boolean selected) {
        if (index >= 0 && index < getRows().size()) {
            getRows().get(index).getCheckbox().setSelected(selected);
        }
    }

    /**
     * Returns current size of the sphere of index-th feature point
     * @param index Index of the feature point
     * @return Sphere size, {@code NaN} on error.
     */
    public double getSphereSize(int index) {
        return (index < 0 || index >= getRows().size())
                ? Double.NaN
                : (Double) getRows().get(index).slider.getValue();
    }

    /**
     * If the slider or its spinner are changed, then {@code ACTION_COMMAND_SPINSLIDER_SPINNER} or
     * {@code ACTION_COMMAND_SPINSLIDER_SLIDER} are triggered in the action listener.
     */
    protected void setSliderActions() {
        for (int i = 0; i < getRows().size(); i++) {
            int index = i;

            getRows().get(i).slider.addSliderListener(e ->
                getActionListener().actionPerformed(new LoadedActionEvent(
                        e.getSource(),
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_FEATURE_POINT_RESIZE,
                        index))
            );

            getRows().get(i).slider.addSpinnerListener(e -> {
                if (getRows().get(index).getCheckbox().isSelected()) { // Recompute only if the feature point is selected
                    getActionListener().actionPerformed(new ActionEvent(
                            e.getSource(),
                            ActionEvent.ACTION_PERFORMED,
                            ACTION_COMMAND_DISTANCE_RECOMPUTE
                    ));
                }
            });
        }
    }

}
