/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fidentis.analyst.gui.task.batch.symmetry;

import cz.fidentis.analyst.gui.elements.CurveRenderingPanel;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.gui.task.batch.SelectBoxService;
import cz.fidentis.analyst.gui.task.symmetry.*;
import cz.fidentis.analyst.gui.elements.PlaneManipulationPanel;
import cz.fidentis.analyst.gui.task.ControlPanel;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;

import java.awt.Component;

import javax.swing.*;
import javax.vecmath.Vector3d;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Control panel for analysis via cutting planes.
 *
 * @author Dominik Racek
 * @author Radek Oslejsek
 * @author Samuel Smoleniak
 * @author Peter Conga
 */
public class BatchCuttingPlanesPanel extends ControlPanel {

    /*
     * Actions triggered by this panel and served by the associated action listener 
     */
    public static final String ACTION_COMMAND_EXPORT = "export";
    public static final String ACTION_VIEW_VERTICAL = "view cross-sections from side view";
    public static final String ACTION_VIEW_HORIZONTAL = "view cross-sections from top view";
    public static final String ACTION_VIEW_FRONT = "view cross-sections from front view";
    public static final String ACTION_COMPUTE_HAUSDORFF = "compute hausdorff distance";
    public static final String ACTION_INITIALIZE_MANUPULATION_PANEL = "init planeManipulationPanels";
    public static final String ACTION_CHANGE_SAMPLING = "change sampling";
    public static final String ACTION_COMMAND_SHOW_SELECTED_FACE = "Show selected face";

    /*
     * List of panels for manipulating with the cutting planes.
     */
    private final List<PlaneManipulationPanel> planeManipulationPanels = new ArrayList<>();
    
    private final transient Task task;

    /*
     * Mandatory design elements
     */
    public static final String ICON = "profiles28x28.png";
    public static final String NAME = "Cutting Planes";
    
    

    private PropertyChangeListener planeManipulationListener;
    /**
     * Constructor
     *
     * @param action Action listener
     */
    public BatchCuttingPlanesPanel(ActionListener action, Task task) {
        super(action);
        setName(NAME);
        initComponents();
        //setSamplingSlider();
        setVertexReductionSlider();
        
        Objects.requireNonNull(task);
        this.task = task;

        SelectBoxService.syncSelectionMenu(task, false, jComboBox1);
        
        jComboBox1.addActionListener(createListener(action, ACTION_COMMAND_SHOW_SELECTED_FACE));

        jButton1.addActionListener(createListener(action, ACTION_COMMAND_EXPORT));
        jRadioButton11.addActionListener(createListener(action, ACTION_VIEW_VERTICAL));
        jRadioButton12.addActionListener(createListener(action, ACTION_VIEW_HORIZONTAL));
        jRadioButton13.addActionListener(createListener(action, ACTION_VIEW_FRONT));
        jRadioButton11.setSelected(true);
        //jButtonSamplingInfo.addActionListener((ActionEvent e) -> this.showSamplingHelp());

        planeManipulationPanels.addAll(Arrays.asList(cuttingPlanePanel1, cuttingPlanePanel2, cuttingPlanePanel3));

        // trigger curve view change when a plane is shifted or shown
        planeManipulationListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                // if plane was shifted, change view accordingly and repaint curve rendering panel
                if (evt.getPropertyName().equals("shift")) {
                    Vector3d normal = (Vector3d) evt.getNewValue();
                    if (normal.equals(CuttingPlanesAction.DIRECTION_VERTICAL)) {
                        jRadioButton11.setSelected(true);
                        getActionListener().actionPerformed(new ActionEvent(jRadioButton11, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_VERTICAL));
                    } else if (normal.equals(CuttingPlanesAction.DIRECTION_HORIZONTAL)) {
                        jRadioButton12.setSelected(true);
                        getActionListener().actionPerformed(new ActionEvent(jRadioButton12, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_HORIZONTAL));
                    } else {
                        jRadioButton13.setSelected(true);
                        getActionListener().actionPerformed(new ActionEvent(jRadioButton13, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_FRONT));
                    }
                }

                // if plane in current view changed visibility, repaint curve rendering panel
                if (evt.getPropertyName().equals("visibility")) {
                    Vector3d normal = (Vector3d) evt.getNewValue();
                    if (normal.equals(CuttingPlanesAction.DIRECTION_VERTICAL) && jRadioButton11.isSelected()) {
                        getActionListener().actionPerformed(new ActionEvent(jRadioButton11, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_VERTICAL));
                    } else if (normal.equals(CuttingPlanesAction.DIRECTION_HORIZONTAL) && jRadioButton12.isSelected()) {
                        getActionListener().actionPerformed(new ActionEvent(jRadioButton12, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_HORIZONTAL));
                    } else if (normal.equals(CuttingPlanesAction.DIRECTION_FRONT_BACK) && jRadioButton13.isSelected()) {
                        getActionListener().actionPerformed(new ActionEvent(jRadioButton13, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_FRONT));
                    }
                }

                // if mouse was released or a plane in current view changed visibility,
                // trigger hausdorff distance computation
                if (evt.getPropertyName().equals("mouse released") || evt.getPropertyName().equals("visibility")) {
                    getActionListener().actionPerformed(new ActionEvent(jRadioButton11, ActionEvent.ACTION_PERFORMED, ACTION_COMPUTE_HAUSDORFF));
                }
                
                if (evt.getPropertyName().equals("highlight")) {
                    PlaneManipulationPanel pmp = (PlaneManipulationPanel) evt.getNewValue();
                    if ((jRadioButton11.isSelected() && pmp.getPlaneOrientation().x != 1.0) || (jRadioButton12.isSelected() && pmp.getPlaneOrientation().y != 1.0) || (jRadioButton13.isSelected() && pmp.getPlaneOrientation().z != 1.0)) {
                        return;
                    }
                    int index = planeManipulationPanels.indexOf(pmp);
                    int counter = 0;
                    for (int i = 0; i < index; i++) {
                        if (planeManipulationPanels.get(i).getPlaneOrientation().x != pmp.getPlaneOrientation().x || planeManipulationPanels.get(i).getPlaneOrientation().y != pmp.getPlaneOrientation().y || planeManipulationPanels.get(i).getPlaneOrientation().z != pmp.getPlaneOrientation().z) {
                            counter ++;
                        }
                    }
                    polylinePanel1.setHighlightIndex(index - counter);
                    repaint();
                }
                if (evt.getPropertyName().equals("highlight off")) {
                    polylinePanel1.setHighlightIndex(-1);
                    repaint();
                }
            }
        };

        for (PlaneManipulationPanel panel : planeManipulationPanels) {
            panel.addPropertyChangeListener(planeManipulationListener);
        }

        // jCheckBox11.addActionListener(createListener(action, ACTION_MIRROR_CUTS));
        // restrict the slider range into the half if mirror cuts are turned on
        /*
        jCheckBox1.addActionListener((ActionEvent e) -> {
            double val = comboSliderDouble1.getValue();
            if (((JCheckBox) e.getSource()).isSelected()) {
                comboSliderDouble1.setRange(0.5, 1, 2);
                comboSliderDouble1.setValue((val < 0.5) ? 1.0 - val : val);
            } else {
                comboSliderDouble1.setRange(0, 1, 2);
                comboSliderDouble1.setValue(val);
            }
        });
         */
    }

    @Override
    public ImageIcon getIcon() {
        return getStaticIcon();
    }
    
    /**
     * Updates drop-down menu to select currently shown face
     */
    public void updateDatasetMenu() {
        SelectBoxService.syncSelectionMenu(task, true, jComboBox1);
    }

    /**
     * Returns the face selected in a menu.
     *
     * @return The face selected in a menu. This is either some face from the dataset, or the average face.
     */
    public HumanFace getSelectedFace() {
        FaceReference faceReference = SelectBoxService.getSelectedFace(task, jComboBox1);
        return FaceService.INSTANCE.getFaceByReference(faceReference);
    }

    /**
     * Returns {@code true} if the item selected in the menu is the average face
     *
     * @return {@code true} if the item selected in the menu is the average face
     */
    public boolean isAvgFaceSelected() {
        FaceReference faceReference = SelectBoxService.getSelectedFace(task, jComboBox1);
        return faceReference.isAverageFace();
    }

    /**
     * Select the average face in the manu, if exists.
     */
    public void selectAvgFace() {
        if (TaskService.INSTANCE.getAvgFace(task) != null) {
            jComboBox1.setSelectedIndex(0);
        }
    }

    /**
     * The average face is added, replaced, or removed (if {@code null}) and
     * the action event is automatically triggered by the jBomboBox1 with the first item set as selected.
     * Also, the export button is switched on/off.
     *
     * @param face new AVG face
     */
    public void addAndSelectAvgFace(FaceReference face) {
        SelectBoxService.syncSelectionMenu(task, face, true, jComboBox1);
    }

    /**
     * Static implementation of the {@link #getIcon()} method.
     *
     * @return Control panel icon
     */
    public static ImageIcon getStaticIcon() {
        return new ImageIcon(BatchCuttingPlanesPanel.class.getClassLoader().getResource("/" + ICON));
    }

    public Task getTask() {
        return task;
    }
    
    private void setVertexReductionSlider() {
        this.vertexReductionSpinSlider.setSliderSize(120);
        this.vertexReductionSpinSlider.initPercentage(100);
        vertexReductionSpinSlider.addSpinnerListener((ActionEvent e) -> {
            getActionListener().actionPerformed(new ActionEvent(vertexReductionSpinSlider, ActionEvent.ACTION_PERFORMED, ACTION_CHANGE_SAMPLING));
            getActionListener().actionPerformed(new ActionEvent(vertexReductionSpinSlider, ActionEvent.ACTION_PERFORMED, jRadioButton11.isSelected() ? ACTION_VIEW_VERTICAL : jRadioButton12.isSelected() ? ACTION_VIEW_HORIZONTAL : ACTION_VIEW_FRONT));
            repaint();
        });
    }

    /**
     * Returns embedded {@link CurveRenderingPanel}
     *
     * @return embedded {@link CurveRenderingPanel}
     */
    public CurveRenderingPanel getCurveRenderingPanel() {
        return this.polylinePanel1;
    }

    /**
     * Returns a list of Booleans that contains true for visible and false for
     * hidden planes.
     *
     * @return List of Booleans indicating visibility of planes
     */
    public List<Boolean> getCheckedPlanes() {
        return planeManipulationPanels.stream()
                .map(PlaneManipulationPanel::isCheckBoxSelected)
                .collect(Collectors.toList());
    }

    public List<PlaneManipulationPanel> getPlaneManipulationPanels() {
        return planeManipulationPanels;
    }

    /**
     * Set computed Hausdorff distance
     *
     * @param distance1 computed distance 1
     * @param distance2 computed distance 2
     * @param distance3 computed distance 3
     */
    /*
    public void setHausdorffDistances(String distance1, String distance2, String distance3) {
        this.hausdorffDistLabel1.setText(distance1);
        this.hausdorffDistLabel2.setText(distance2);
        this.hausdorffDistLabel3.setText(distance3);
    }
    */
    /**
     * Hide Hausdorff Distance label if only one face is present.
     */
    /*
    public void hideHausdorffDistance() {
        this.hausdorffDistancePanel.hide();
        this.vertexReductionSpinSlider.setVisible(true);
        this.jLabel9.setVisible(true);
        
    }
*/
    public int getSubsamplingStrength() {
        return (int) this.vertexReductionSpinSlider.getValue();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        viewButtons = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jRadioButton11 = new javax.swing.JRadioButton();
        jRadioButton12 = new javax.swing.JRadioButton();
        jRadioButton13 = new javax.swing.JRadioButton();
        showDeviationCheckBox = new javax.swing.JCheckBox();
        vectorSizeLabel = new javax.swing.JLabel();
        addVerticalPMPButton = new javax.swing.JButton();
        removeVerticalPMPButton = new javax.swing.JButton();
        removeHorizontalPMPButton = new javax.swing.JButton();
        addHorizontalPMPButton = new javax.swing.JButton();
        addFrontBackPMPButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        cuttingPlanePanel1 = new cz.fidentis.analyst.gui.elements.PlaneManipulationPanel();
        jPanel3 = new javax.swing.JPanel();
        cuttingPlanePanel2 = new cz.fidentis.analyst.gui.elements.PlaneManipulationPanel();
        jPanel4 = new javax.swing.JPanel();
        cuttingPlanePanel3 = new cz.fidentis.analyst.gui.elements.PlaneManipulationPanel();
        removeFronBackPMPButton = new javax.swing.JButton();
        vertexReductionSpinSlider = new cz.fidentis.analyst.gui.elements.SpinSlider();
        jLabel9 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        polylinePanel1 = new cz.fidentis.analyst.gui.elements.CurveRenderingPanel();

        setMaximumSize(new java.awt.Dimension(1500, 32767));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new java.awt.GridBagLayout());

        org.openide.awt.Mnemonics.setLocalizedText(jButton1, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.jButton1.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 8, 5, 8);
        jPanel1.add(jButton1, gridBagConstraints);

        viewButtons.add(jRadioButton11);
        org.openide.awt.Mnemonics.setLocalizedText(jRadioButton11, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.jRadioButton11.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 5, 0, 0);
        jPanel1.add(jRadioButton11, gridBagConstraints);

        viewButtons.add(jRadioButton12);
        org.openide.awt.Mnemonics.setLocalizedText(jRadioButton12, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.jRadioButton12.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 5, 0, 0);
        jPanel1.add(jRadioButton12, gridBagConstraints);

        viewButtons.add(jRadioButton13);
        org.openide.awt.Mnemonics.setLocalizedText(jRadioButton13, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.jRadioButton13.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 5, 0, 0);
        jPanel1.add(jRadioButton13, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(showDeviationCheckBox, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.showDeviationCheckBox.text")); // NOI18N
        showDeviationCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showDeviationCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 20, 0, 0);
        jPanel1.add(showDeviationCheckBox, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(vectorSizeLabel, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.vectorSizeLabel.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 18, 0, 0);
        jPanel1.add(vectorSizeLabel, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(addVerticalPMPButton, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.addVerticalPMPButton.text")); // NOI18N
        addVerticalPMPButton.setMargin(new java.awt.Insets(2, 6, 2, 6));
        addVerticalPMPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addVerticalPMPButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 2, 0);
        jPanel1.add(addVerticalPMPButton, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(removeVerticalPMPButton, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.removeVerticalPMPButton.text")); // NOI18N
        removeVerticalPMPButton.setMargin(new java.awt.Insets(2, 8, 2, 8));
        removeVerticalPMPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeVerticalPMPButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 6, 0);
        jPanel1.add(removeVerticalPMPButton, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(removeHorizontalPMPButton, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.removeHorizontalPMPButton.text")); // NOI18N
        removeHorizontalPMPButton.setMargin(new java.awt.Insets(2, 8, 2, 8));
        removeHorizontalPMPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeHorizontalPMPButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 6, 0);
        jPanel1.add(removeHorizontalPMPButton, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(addHorizontalPMPButton, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.addHorizontalPMPButton.text")); // NOI18N
        addHorizontalPMPButton.setMargin(new java.awt.Insets(2, 6, 2, 6));
        addHorizontalPMPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addHorizontalPMPButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 2, 0);
        jPanel1.add(addHorizontalPMPButton, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(addFrontBackPMPButton, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.addFrontBackPMPButton.text")); // NOI18N
        addFrontBackPMPButton.setMargin(new java.awt.Insets(2, 6, 2, 6));
        addFrontBackPMPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addFrontBackPMPButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 2, 0);
        jPanel1.add(addFrontBackPMPButton, gridBagConstraints);

        jPanel2.add(cuttingPlanePanel1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jPanel2, gridBagConstraints);

        jPanel3.add(cuttingPlanePanel2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jPanel3, gridBagConstraints);

        jPanel4.add(cuttingPlanePanel3);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jPanel4, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(removeFronBackPMPButton, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.removeFronBackPMPButton.text")); // NOI18N
        removeFronBackPMPButton.setMargin(new java.awt.Insets(2, 8, 2, 8));
        removeFronBackPMPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeFronBackPMPButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 6, 0);
        jPanel1.add(removeFronBackPMPButton, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel9, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.jLabel9.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(BatchCuttingPlanesPanel.class, "BatchCuttingPlanesPanel.jLabel1.text")); // NOI18N

        polylinePanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout polylinePanel1Layout = new javax.swing.GroupLayout(polylinePanel1);
        polylinePanel1.setLayout(polylinePanel1Layout);
        polylinePanel1Layout.setHorizontalGroup(
            polylinePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 462, Short.MAX_VALUE)
        );
        polylinePanel1Layout.setVerticalGroup(
            polylinePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(polylinePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(vertexReductionSpinSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vertexReductionSpinSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(polylinePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addVerticalPMPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addVerticalPMPButtonActionPerformed
        PlaneManipulationPanel newPMP = new PlaneManipulationPanel();
        newPMP.addPropertyChangeListener(this.planeManipulationListener);
        planeManipulationPanels.add(newPMP);
        getActionListener().actionPerformed(new ActionEvent(addVerticalPMPButton, ActionEvent.ACTION_PERFORMED, ACTION_INITIALIZE_MANUPULATION_PANEL));
        newPMP.createBboxCuttingPlane(CuttingPlanesAction.DIRECTION_VERTICAL);
        jPanel2.add(newPMP);
        jPanel2.revalidate();
        jPanel2.repaint();
    }//GEN-LAST:event_addVerticalPMPButtonActionPerformed

    private void addHorizontalPMPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addHorizontalPMPButtonActionPerformed
        PlaneManipulationPanel newPMP = new PlaneManipulationPanel();
        newPMP.addPropertyChangeListener(this.planeManipulationListener);
        planeManipulationPanels.add(newPMP);
        getActionListener().actionPerformed(new ActionEvent(addVerticalPMPButton, ActionEvent.ACTION_PERFORMED, ACTION_INITIALIZE_MANUPULATION_PANEL));
        newPMP.createBboxCuttingPlane(CuttingPlanesAction.DIRECTION_HORIZONTAL);
        jPanel3.add(newPMP);
        jPanel3.revalidate();
        jPanel3.repaint();
    }//GEN-LAST:event_addHorizontalPMPButtonActionPerformed

    private void addFrontBackPMPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addFrontBackPMPButtonActionPerformed
        PlaneManipulationPanel newPMP = new PlaneManipulationPanel();
        newPMP.addPropertyChangeListener(this.planeManipulationListener);
        planeManipulationPanels.add(newPMP);
        getActionListener().actionPerformed(new ActionEvent(addVerticalPMPButton, ActionEvent.ACTION_PERFORMED, ACTION_INITIALIZE_MANUPULATION_PANEL));
        newPMP.createBboxCuttingPlane(CuttingPlanesAction.DIRECTION_FRONT_BACK);
        jPanel4.add(newPMP);
        jPanel4.revalidate();
        jPanel4.repaint();
    }//GEN-LAST:event_addFrontBackPMPButtonActionPerformed

    private void removeVerticalPMPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeVerticalPMPButtonActionPerformed
        if (jPanel2.getComponentCount() > 0) {
            // Get the last component added
            Component lastComponent = jPanel2.getComponent(jPanel2.getComponentCount() - 1);

            if (lastComponent instanceof PlaneManipulationPanel) {
                jPanel2.remove(lastComponent);
                PlaneManipulationPanel pmp = (PlaneManipulationPanel) lastComponent;
                planeManipulationPanels.remove(pmp);
                pmp.deleteBboxCuttingPlane();
            }
            
            jPanel2.revalidate();
            jPanel2.repaint();
            // Fore a reload of cross cuts - repaint() does not work
            getActionListener().actionPerformed(new ActionEvent(jRadioButton11, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_VERTICAL));
        }
    }//GEN-LAST:event_removeVerticalPMPButtonActionPerformed

    private void removeHorizontalPMPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeHorizontalPMPButtonActionPerformed
        if (jPanel3.getComponentCount() > 0) {
            // Get the last component added
            Component lastComponent = jPanel3.getComponent(jPanel3.getComponentCount() - 1);

            if (lastComponent instanceof PlaneManipulationPanel) {
                jPanel3.remove(lastComponent);
                PlaneManipulationPanel pmp = (PlaneManipulationPanel) lastComponent;
                planeManipulationPanels.remove(pmp);
                pmp.deleteBboxCuttingPlane();
            }
            
            jPanel3.revalidate();
            jPanel3.repaint();
            // Fore a reload of cross cuts - repaint() does not work
            getActionListener().actionPerformed(new ActionEvent(jRadioButton12, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_HORIZONTAL));
        }
    }//GEN-LAST:event_removeHorizontalPMPButtonActionPerformed

    private void removeFronBackPMPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeFronBackPMPButtonActionPerformed
        if (jPanel4.getComponentCount() > 0) {
            // Get the last component added
            Component lastComponent = jPanel4.getComponent(jPanel4.getComponentCount() - 1);

            if (lastComponent instanceof PlaneManipulationPanel) {
                jPanel4.remove(lastComponent);
                PlaneManipulationPanel pmp = (PlaneManipulationPanel) lastComponent;
                planeManipulationPanels.remove(pmp);
                pmp.deleteBboxCuttingPlane();
            }
            
            jPanel4.revalidate();
            jPanel4.repaint();
            // Fore a reload of cross cuts - repaint() does not work
            getActionListener().actionPerformed(new ActionEvent(jRadioButton13, ActionEvent.ACTION_PERFORMED, ACTION_VIEW_FRONT));
        }
    }//GEN-LAST:event_removeFronBackPMPButtonActionPerformed

    private void showDeviationCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showDeviationCheckBoxActionPerformed
        polylinePanel1.setShowNormals(showDeviationCheckBox.isSelected());
        repaint();
    }//GEN-LAST:event_showDeviationCheckBoxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addFrontBackPMPButton;
    private javax.swing.JButton addHorizontalPMPButton;
    private javax.swing.JButton addVerticalPMPButton;
    private cz.fidentis.analyst.gui.elements.PlaneManipulationPanel cuttingPlanePanel1;
    private cz.fidentis.analyst.gui.elements.PlaneManipulationPanel cuttingPlanePanel2;
    private cz.fidentis.analyst.gui.elements.PlaneManipulationPanel cuttingPlanePanel3;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JRadioButton jRadioButton11;
    private javax.swing.JRadioButton jRadioButton12;
    private javax.swing.JRadioButton jRadioButton13;
    private cz.fidentis.analyst.gui.elements.CurveRenderingPanel polylinePanel1;
    private javax.swing.JButton removeFronBackPMPButton;
    private javax.swing.JButton removeHorizontalPMPButton;
    private javax.swing.JButton removeVerticalPMPButton;
    private javax.swing.JCheckBox showDeviationCheckBox;
    private javax.swing.JLabel vectorSizeLabel;
    private cz.fidentis.analyst.gui.elements.SpinSlider vertexReductionSpinSlider;
    private javax.swing.ButtonGroup viewButtons;
    // End of variables declaration//GEN-END:variables
}
