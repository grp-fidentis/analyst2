package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.gui.project.table.ProjectTable;
import cz.fidentis.analyst.gui.task.TaskWindow;
import cz.fidentis.analyst.project.*;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.windows.WindowManager;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The left-hand part of the project dashboard consisting of a project toolbar and 
 * a table of faces. This panel also handles the list of tasks (analytical tabs).
 * 
 * @author Matej Kovar
 * @author Radek Oslejsek
 */
public class ProjectPanel extends javax.swing.JPanel {
    
    /*
     * External actions
     */
    public static final String ACTION_FACES_SELECTED = "Faces selected";
    
    /**
     * If more that {@code MAX_OPEN_FACES} are selected for the single face
     * analysis, a warning dialogue appears informing that multiple 
     * tabs are to be opened.
     */
    public static final int MAX_OPEN_FACES = 5;

    /**
     * List of open task windows (tabs)
     */
    private final List<TaskWindow> taskTabs = new ArrayList<>();
    

    /**
     * Lis of controllers (action listeners) that mediate communication
     * between this project panel and control panels (right-hand side tabs)
     */
    private final List<ProjectPanelAction> actionListeners = new ArrayList<>();
    
    /**
     * Constructor.
     */
    public ProjectPanel() {
        initComponents(); // init GUI
        createNewProject();
        //projectTable1.setSize(jScrollPane1.getWidth(), jScrollPane1.getHeight());
        //jScrollPane1.setViewportView(projectTable1);
        //this.projectTable1.repaint();
        setEnabledTaskButtons(); // enable/distable analytical buttons
        initListeners(); // set communication logic
    }
    
    /**
     * Setting a {@code ProjectPanelAction} is mandatory for the communication
     * between this panel and project's control panels.
     * 
     * @param action Project panel action listener with the interaction code
     */
    public void addActionListener(ProjectPanelAction action) {
        actionListeners.add(action);
    }


    /**
     * Returns internal project table with the list of faces
     * @return internal project table with the list of faces
     */
    public ProjectTable getProjectTable() {
        return this.projectTable1;
    }
    
    /**
     * Creates new empty project.
     * @return Previous project or {@code null}
     */
    public final Project createNewProject() {
        Project old = ProjectService.INSTANCE.getOpenedProject();
        
        taskTabs.stream().forEach(taskWin -> {
            taskWin.setAskWhenClosing(false);
            taskWin.close(); 
        });
        taskTabs.clear();
        getProjectTable().removeAllRows();
        
        ProjectService.INSTANCE.createNewProject();
        repaint();
        return old;
    }
    
    /**
     * Opens an exiting project from disk
     * @param dir Project's folder
     * @return Previous project or {@code null}
     */
    public Project openProject(File dir) {
        Project old = ProjectService.INSTANCE.getOpenedProject();

        ProgressDialog progressDialog = new ProgressDialog(this, "Opening project with analytical tasks");
        Project project;
        // load faces into the project
        try {
            project = ProjectService.INSTANCE.loadProject(dir, progress -> {
                progressDialog.setValue(progress);
                return null;
            });
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        
        // close current tasks
        taskTabs.stream().forEach(taskWin -> {
            taskWin.setAskWhenClosing(false);
            taskWin.close(); 
        });
        taskTabs.clear();
        
        // fill the table with new faces
        getProjectTable().removeAllRows();
        for (int i = 0; i < project.getFaces().size(); i++) {
            addFaceToTable(project.getFaces().get(i));
        }
        
        // load analytical tasks (tabs) from the disk and open them
        RecoverTasksWorker recoverTasksWorker = new RecoverTasksWorker( // prepare worker
                project, 
                progressDialog,
                (camera, task) -> {
                    openTaskTab(task);
                }
        );
        progressDialog.runTask(recoverTasksWorker); // run the worker
        repaint();
        return old;
    }
    
    /**
     * Saves project to disk. Sets the state of the project to "unchanged".
     * Therefore, call {@code !project.changed()} to check whether the project
     * was saved successfully.
     */
    public void saveProject() {
        ProgressDialog progressDialog = new ProgressDialog(this, "Saving analytical tasks");
        SaveProjectWorker task = new SaveProjectWorker(progressDialog);
        progressDialog.runTask(task);
    }
    
    /**
     * Creates and/or opens an analytical tab. Its type is selected automatically.
     * 
     * @param task Face references for the task
     * @return new task window, {@code null} if the task already exists.
     */
    protected TaskWindow openTaskTab(Task task) {
        TaskWindow newTab = new TaskWindow(
                task,
                (ActionEvent e) -> { // closing action
                    closeTaskTab((TaskWindow) e.getSource());
                });

        if (!taskTabs.contains(newTab)) { // new tab
            projectTable1.linkFacesWithTask(task);
            taskTabs.add(newTab);            
            newTab.open();
            newTab.requestActive();
            return newTab;
        } else {
            focusTaskTab(newTab.getName());
            return null;
        }
    }
    
    /**
     * Closes task tab.
     * @param tab task window to be closed
     */
    protected void closeTaskTab(TaskWindow tab) {
        ProjectService.INSTANCE.removeTask(tab.getTask());
        projectTable1.unlinkFacesFromTask(tab.getTask());
        taskTabs.remove(tab);
    }
    
    /**
     * Finds selected rows in the table, closes task tabs in which corresponding
     * faces appear and removes rows fro the table.
     */
    protected void removeSelectedFaces() {
        List<Integer> selectedRows = Arrays.stream(projectTable1.getSelectedRows())
                .boxed()
                .sorted((i1, i2) -> i2.compareTo(i1)) // descending order
                .collect(Collectors.toList());

        // close tabs (task windows) and remove faces from the project instance
        List<Boolean> toRemoveRows = new ArrayList<>();
        selectedRows.stream().forEach(row -> {
            FaceReference face = projectTable1.getFaceAt(row);
            List<TaskWindow> tabsToClose = taskTabs.stream()
                    .filter(t -> (t.hasFace(face)))
                    .collect(Collectors.toList());
            
            toRemoveRows.add(Boolean.TRUE);
            for (TaskWindow tab: tabsToClose) {
                if (!tab.hasFace(face)) {
                    continue;
                }
                tab.requestActive();
                if (tab.close()) { // the use agreed to close the task
                    closeTaskTab(tab);
                } else { // closing of some tab was canceled by the user
                    toRemoveRows.set(toRemoveRows.size()-1, Boolean.FALSE);
                }
            }
            FaceService.INSTANCE.removeFace(face);
            ProjectService.INSTANCE.removeFaceFromProject(face);
        });
        
        // removes rows from the table
        for (int i = 0; i < selectedRows.size(); i++) {
            if (toRemoveRows.get(i)) {
                projectTable1.getProjectTableModel().removeRow(selectedRows.get(i));
            }
        }
        
        ((ProjectWindow) WindowManager.getDefault().findTopComponent("ProjectWindow")).requestActive();
    }
    
    /**
     * Pops up a file selection dialogue and then adds selected faces into the project.
     * The project state is set to "changed".
     */
    protected void addFacesToProject() {
        File[] files = new FileChooserBuilder(ProjectWindow.class)
                .setTitle("Open human face(s)")
                .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                //.setApproveText("Add")
                .setFileFilter(new FileNameExtensionFilter("obj files (*.obj)", "obj"))
                .setAcceptAllFileFilterUsed(true)
                .showMultiOpenDialog();
        
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                try {
                    FaceReference faceReference = FaceService.INSTANCE.loadFace(files[i]);
                    ProjectService.INSTANCE.addFace(faceReference);
                    if (faceReference != null) { // face has been added
                        addFaceToTable(faceReference);
                    }
                } catch (IOException ex) {}
            }
        }
    }
    
    /**
     * Adds a single face into the project's table.
     * The face has to be already included in the {@code project.faces} proxy.
     * The project state is set to "changed".
     * 
     * @param faceReference Index of the face from pro project instance to be added into the table
     */
    protected void addFaceToTable(FaceReference faceReference) {
        projectTable1.addRow(
                faceReference,
                (ActionEvent e) -> {
                    JComboBox comboBox = (JComboBox) e.getSource();
                    String taskName = String.valueOf(comboBox.getSelectedItem());
                    focusTaskTab(taskName);
                }
        );
    }
    
    /**
     * Sets active (pops up) a task tab with the given name
     * 
     * @param taskName Task name
     */
    protected void focusTaskTab(String taskName) {
        taskTabs.stream()
                .filter(tab -> (tab.getName().equals(taskName)))
                .findAny()
                .ifPresent(TaskWindow::requestActive);
    }
    
    private void setEnabledTaskButtons() {
        int sel = projectTable1.getSelectedRowCount();
        
        Font fontOn = new Font(
                singleAnalysisButton.getFont().getName(),
                Font.BOLD,
                singleAnalysisButton.getFont().getSize()
        );
        
        Font fontOff = new Font(
                singleAnalysisButton.getFont().getName(),
                Font.PLAIN,
                singleAnalysisButton.getFont().getSize()
        );
        
        if (sel == 0) {
            singleAnalysisButton.setEnabled(false);
            pairAnalysisButton.setEnabled(false);
            batchAnalysisButton.setEnabled(false);
        } else if (sel == 1) {
            singleAnalysisButton.setEnabled(true);
            pairAnalysisButton.setEnabled(false);
            batchAnalysisButton.setEnabled(false);
        } else if (sel == 2) {
            singleAnalysisButton.setEnabled(true);
            pairAnalysisButton.setEnabled(true);
            batchAnalysisButton.setEnabled(false);
        } else if (sel > 2) {
            singleAnalysisButton.setEnabled(true);
            pairAnalysisButton.setEnabled(false);
            batchAnalysisButton.setEnabled(true);            
        }
        
        singleAnalysisButton.setFont(singleAnalysisButton.isEnabled() ? fontOn : fontOff);
        pairAnalysisButton.setFont(pairAnalysisButton.isEnabled() ? fontOn : fontOff);
        batchAnalysisButton.setFont(batchAnalysisButton.isEnabled() ? fontOn : fontOff);
    }
    
    protected List<FaceReference> getSelectedFaces() {
        return Arrays.stream(projectTable1.getSelectedRows())
                .boxed()
                .map(row -> projectTable1.getFaceAt(row)) // selected faces
                .collect(Collectors.toList());
    }
    
    private void initListeners() {
        addButton.addActionListener((ActionEvent e) -> { 
            addFacesToProject();
        });
        
        selectAllButton.addActionListener((ActionEvent e) -> {
            projectTable1.selectAll();
        });
        
        deselectAllButton.addActionListener((ActionEvent e) -> {
            projectTable1.clearSelection();
        });
        
        removeButton.addActionListener((ActionEvent e) -> {
            if (projectTable1.getSelectedRowCount() <= 0) {
                JOptionPane.showMessageDialog(this, "No face chosen");
                return;
            }
            removeSelectedFaces();
        });
        
        singleAnalysisButton.addActionListener((ActionEvent e) -> {
            ProgressDialog progressDialog = new ProgressDialog(this, "Opening analytical tasks");
            OpenTasksWorker openTask = new OpenTasksWorker( // prepare worker
                    getSelectedFaces(),
                    progressDialog,
                    (face ) -> {
                        Task task = TaskService.INSTANCE.createTask(List.of(face));
                        ProjectService.INSTANCE.addTask(task);
                        this.openTaskTab(task);
                    }
            );
            progressDialog.runTask(openTask); // run the worker
            //repaint();
        });
        
        pairAnalysisButton.addActionListener((ActionEvent e) -> {
            FaceReference[] faceOptions = {
                projectTable1.getFaceAt(projectTable1.getSelectedRows()[0]), // name 1
                projectTable1.getFaceAt(projectTable1.getSelectedRows()[1])  // name 2
            };
            String[] options = Arrays.stream(faceOptions).map(it -> it.getName()).toArray(String[]::new);
            int choice = JOptionPane.showOptionDialog(this,
                "Select primary face: ",
                "Select primary face",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
            
            if (choice == 0) {
                Task task = TaskService.INSTANCE.createTask(List.of(faceOptions[0], faceOptions[1]));
                ProjectService.INSTANCE.addTask(task);
                openTaskTab(task);
            } else {
                Task task = TaskService.INSTANCE.createTask(List.of(faceOptions[1], faceOptions[0]));
                ProjectService.INSTANCE.addTask(task);
                openTaskTab(task);
            }
        });
        
        batchAnalysisButton.addActionListener((ActionEvent e) -> {
            Task task = TaskService.INSTANCE.createTask(getSelectedFaces());
            openTaskTab(task);
        });
        
        
        //table.addTableModelListener((TableModelEvent e) -> {
        //});
        
        projectTable1.getSelectionModel().addListSelectionListener((ListSelectionEvent e) -> {
            if (!e.getValueIsAdjusting()) {
                actionListeners.stream().forEach(action -> action.actionPerformed(new ActionEvent(
                        projectTable1,
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_FACES_SELECTED
                )));
                setEnabledTaskButtons();
            }
        });
        
        projectTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                //if (projectTable1.getSelectedRowCount() == 0) {
                //    checkFaceState(null, false);
                //} else 
                if (evt.getClickCount() == 2) {
                    if (!projectTable1.getProjectTableModel().isCellEditable(projectTable1.getSelectedRow(), 0)) { // this row is filtered out 
                        return;
                    }
                    projectTable1.getProjectTableModel().setValueAt(true, projectTable1.getSelectedRow(), 0); // check this face
                    FaceReference face = projectTable1.getFaceAt(projectTable1.getSelectedRow());
                    Task task = TaskService.INSTANCE.createTask(List.of(face));
                    ProjectService.INSTANCE.addTask(task);
                    openTaskTab(task);
                }
            }
        });
    }
        
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        projectTable1 = new cz.fidentis.analyst.gui.project.table.ProjectTable();
        jToolBar1 = new javax.swing.JToolBar();
        addButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        selectAllButton = new javax.swing.JButton();
        deselectAllButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        singleAnalysisButton = new javax.swing.JButton();
        pairAnalysisButton = new javax.swing.JButton();
        batchAnalysisButton = new javax.swing.JButton();

        jScrollPane1.setViewportView(projectTable1);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        addButton.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/head-black-plus24.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(addButton, org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.addButton.text")); // NOI18N
        addButton.setToolTipText(org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.addButton.toolTipText")); // NOI18N
        addButton.setFocusPainted(false);
        jToolBar1.add(addButton);

        removeButton.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        removeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/head-black-minus24.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(removeButton, org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.removeButton.text")); // NOI18N
        removeButton.setToolTipText(org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.removeButton.toolTipText")); // NOI18N
        removeButton.setFocusPainted(false);
        jToolBar1.add(removeButton);
        jToolBar1.add(jSeparator2);

        selectAllButton.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        selectAllButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/head-black-check24.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(selectAllButton, org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.selectAllButton.text")); // NOI18N
        selectAllButton.setToolTipText(org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.selectAllButton.toolTipText")); // NOI18N
        selectAllButton.setFocusPainted(false);
        jToolBar1.add(selectAllButton);

        deselectAllButton.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        deselectAllButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/head-black-cross24.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(deselectAllButton, org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.deselectAllButton.text")); // NOI18N
        deselectAllButton.setToolTipText(org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.deselectAllButton.toolTipText")); // NOI18N
        deselectAllButton.setFocusPainted(false);
        jToolBar1.add(deselectAllButton);

        jSeparator1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jToolBar1.add(jSeparator1);

        singleAnalysisButton.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        singleAnalysisButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/analysis24.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(singleAnalysisButton, org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.singleAnalysisButton.text")); // NOI18N
        singleAnalysisButton.setToolTipText(org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.singleAnalysisButton.toolTipText")); // NOI18N
        singleAnalysisButton.setFocusable(false);
        singleAnalysisButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jToolBar1.add(singleAnalysisButton);

        pairAnalysisButton.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        pairAnalysisButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/analysis24.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(pairAnalysisButton, org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.pairAnalysisButton.text")); // NOI18N
        pairAnalysisButton.setToolTipText(org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.pairAnalysisButton.toolTipText")); // NOI18N
        pairAnalysisButton.setFocusable(false);
        jToolBar1.add(pairAnalysisButton);

        batchAnalysisButton.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        batchAnalysisButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/analysis24.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(batchAnalysisButton, org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.batchAnalysisButton.text")); // NOI18N
        batchAnalysisButton.setToolTipText(org.openide.util.NbBundle.getMessage(ProjectPanel.class, "ProjectPanel.batchAnalysisButton.toolTipText")); // NOI18N
        batchAnalysisButton.setFocusable(false);
        batchAnalysisButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batchAnalysisButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(batchAnalysisButton);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void batchAnalysisButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batchAnalysisButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_batchAnalysisButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton batchAnalysisButton;
    private javax.swing.JButton deselectAllButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton pairAnalysisButton;
    private cz.fidentis.analyst.gui.project.table.ProjectTable projectTable1;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton selectAllButton;
    private javax.swing.JButton singleAnalysisButton;
    // End of variables declaration//GEN-END:variables

}
