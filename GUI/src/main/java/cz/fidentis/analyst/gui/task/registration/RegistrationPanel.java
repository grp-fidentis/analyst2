package cz.fidentis.analyst.gui.task.registration;

import cz.fidentis.analyst.gui.task.ControlPanel;
import cz.fidentis.analyst.rendering.Animatable;
import cz.fidentis.analyst.rendering.AnimationDirection;
import cz.fidentis.analyst.rendering.RotationAnimator;
import cz.fidentis.analyst.rendering.Scene;
import cz.fidentis.analyst.data.shapes.Box;

import javax.swing.*;
import javax.vecmath.Vector3d;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;

/**
 * Panel used to interactively visualize two face and adjust their registration.
 *
 * @author Richard Pajersky
 * @author Radek Oslejsek
 */
public class RegistrationPanel extends ControlPanel implements Animatable {

    /*
     * Mandatory design elements
     */
    public static final String ICON = "registration28x28.png";
    public static final String NAME = "Registration";

    /*
     * Actions triggered by this panel and served by the associated action listener 
     */
    public static final String ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED = "manual transformation";
    public static final String ACTION_COMMAND_MANUAL_TRANSFORMATION_IN_PROGRESS = "manual transformation in progress";
    public static final String ACTION_COMMAND_FP_CLOSENESS_THRESHOLD = "fp closeness threshold";
    public static final String ACTION_COMMAND_POINT_SAMPLING_STRATEGY = "Point sampling strategy";
    public static final String ACTION_COMMAND_POINT_SAMPLING_STRENGTH = "Point sampling strength";
    public static final String ACTION_COMMAND_REGISTER = "Register";
    public static final String ACTION_COMMAND_UNDO_TRANSFORMATION = "Undo last transformations";

    /*
     * Configuration of panel-specific GUI elements
     */
    public static final String STRATEGY_POINT_TO_POINT = "Point to point";
    public static final String STRATEGY_POINT_TO_TRIANGLE = "Point to triangle";
    
    public static final String HELP_URL = "https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/registration";

    public static final String[] POINT_SAMPLING_STRATEGIES = new String[] {
            "None",
            "Random Sampling",
            "Poisson disk sub sampling",
            "Gaussian Curvature",
            "Mean Curvature",
            "Uniform Space Sampling",
            "Uniform Mesh Sampling",
            "Poisson disk sub sampling (OpenCL)"
    };

    public static final String[] AUTO_ALIGNMENT_ALGORITHM = new String[] {
        "mesh-based (ICP)",
        "feature points (Procrustes)",
        "symmetry-based",
        "ICP with pre-alignment"
    };

    /**
     * Animator which animates transformations
     */
    private final RotationAnimator animator = new RotationAnimator();


    /**
     * Redefined rotation angle
     */
    private static final double ROTATION_ANGLE = 0.5 / 180.0;

    /**
     * ICP required samples for sub-sampling
     */
    private int undersamplingStrength = 500;

    private Vector3d manualRotation = new Vector3d();
    private Vector3d manualTranslation = new Vector3d();
    private double manualScale = 1.0;

    private static final double DEFAULT_TRANSLATION_FACTOR = 0.2;
    private double translationFactor = DEFAULT_TRANSLATION_FACTOR;


    /**
     * Constructor.
     *
     * @param action Action listener
     */
    public RegistrationPanel(ActionListener action) {
        super(action);
        this.setName(NAME);
        initComponents();
        
        infoLinkButton1.setUri(URI.create(HELP_URL));

        jButton1.addActionListener(createListener(action, ACTION_COMMAND_REGISTER));
        jButton2.addActionListener(createListener(action, ACTION_COMMAND_UNDO_TRANSFORMATION));

        thersholdFTF.setValue(5.0);
        thersholdFTF.addActionListener(createListener(action, ACTION_COMMAND_FP_CLOSENESS_THRESHOLD));

        spinSlider1.initInteger(undersamplingStrength, 50, 5000, 10);
        spinSlider1.setContinuousSync(false);
        spinSlider1.addSpinnerListener((ActionEvent e) -> { // update slider when the input field changed
                    this.undersamplingStrength = spinSlider1.getValue().intValue();
                    action.actionPerformed(new ActionEvent(
                            spinSlider1,
                            ActionEvent.ACTION_PERFORMED,
                            ACTION_COMMAND_POINT_SAMPLING_STRENGTH)
                    );
        });

        jFormattedTextField1.setValue(Double.valueOf(0.05));
        jFormattedTextField2.setValue(Integer.valueOf(100));

        Arrays.stream(POINT_SAMPLING_STRATEGIES).forEach(v -> selectableComboBox2.addItem(v));
        selectableComboBox2.setSelectedIndex(0);
        selectableComboBox2.addActionListener(createListener(action, ACTION_COMMAND_POINT_SAMPLING_STRATEGY));

        selectableComboBox1.addItem(AUTO_ALIGNMENT_ALGORITHM[0], true);
        selectableComboBox1.addItem(AUTO_ALIGNMENT_ALGORITHM[1], true);
        selectableComboBox1.addItem(AUTO_ALIGNMENT_ALGORITHM[2], true);
        selectableComboBox1.addItem(AUTO_ALIGNMENT_ALGORITHM[3], true);
        //selectableComboBox1.setSelectedIndex(0);
    }

    /**
     * Returns the manual rotation info and resents it
     *
     * @return rotation info
     */
    public Vector3d getAndClearManualRotation() {
        Vector3d ret = this.manualRotation;
        this.manualRotation = new Vector3d(0, 0, 0);
        return ret;
    }

    /**
     * Returns the manual translation info and resents it
     *
     * @return translation info
     */
    public Vector3d getAndClearManualTranslation() {
        Vector3d ret = this.manualTranslation;
        this.manualTranslation = new Vector3d(0, 0, 0);
        return ret;
    }

    ;
    /**
     * Returns the manual scale info and resents it
     * @return scale info
     */
    public double getAndClearManualScale() {
        double ret = this.manualScale;
        this.manualScale = 1.0;
        return ret;
    }

    /**
     * Computes and sets translation step based on the scene size
     * @param scene A scene. Must not be {@code null}
     */
    public void setTranslationFactor(Scene scene) {
        Box box = scene.getBoundingBox();
        this.translationFactor = (box != null) ? box.diagonalLength() * 0.002 : DEFAULT_TRANSLATION_FACTOR;
    }

    /**
     * Returns ICP undersampling parameter
     *
     * @return ICP undersampling parameter
     */
    public int getIcpUndersamplingStrength() {
        return undersamplingStrength;
    }

    /**
     * Returns actually selected algorithm
     *
     * @return actually selected algorithm
     */
    public String getRegistrationAlgorihm() {
        return this.selectableComboBox1.getSelectedItem().toString();
    }

    /**
     * Return selected point sampling strategy
     *
     * @return selected point sampling strategy
     */
    public String getIcpUdersamplingStrategy() {
        return POINT_SAMPLING_STRATEGIES[selectableComboBox2.getSelectedIndex()];
    }

    /**
     * Turns on or off the plane alignment
     *
     * @param on on-off value
     */
    public void setEnabledPlanes(boolean on) {
        selectableComboBox1.setItem(AUTO_ALIGNMENT_ALGORITHM[2], on);
    }

    /**
     * Turns on or off the plane alignment
     *
     * @param on on-off value
     */
    public void setEnabledProcrustes(boolean on) {
        selectableComboBox1.setItem(AUTO_ALIGNMENT_ALGORITHM[1], on);
    }

    /**
     * Turns on or off the OpenCL dependant sampling strategies
     *
     * @param on on-off value
     */
    public void setEnabledOpenCL(boolean on) {
        selectableComboBox2.setItem(POINT_SAMPLING_STRATEGIES[7], on);
    }

    /**
     * Updates GUI elements that display statistical data about the calculated
     * Hausdorff distance.
     *
     * @param hd Statistical data of the ordinary Hausdorff distance
     * @param whd Statistical data of the weighted Hausdorff distance
     */
    public void updateDistanceStats(DoubleSummaryStatistics hd, DoubleSummaryStatistics whd) {
        jTextField1.setText(String.format("%.3f", hd.getAverage()));
        jTextField2.setText(String.format("%.3f", whd.getAverage()));
    }

    public boolean getScaleParam() {
        return jCheckBox1.isSelected();
    }

    public boolean getIcpAutoCropParam() {
        return jCheckBox2.isSelected();
    }

    public int getMaxIcpIterParam() {
        return ((Number) jFormattedTextField2.getValue()).intValue();
    }

    public double getMinIcpErrorParam() {
        return ((Number) jFormattedTextField1.getValue()).doubleValue();
    }

    @Override
    public void transform(AnimationDirection dir) {
        switch (dir) {
            case TRANSLATE_LEFT:
                this.manualTranslation.x -= translationFactor;
                break;
            case TRANSLATE_RIGHT:
                this.manualTranslation.x += translationFactor;
                break;
            case TRANSLATE_UP:
                this.manualTranslation.y -= translationFactor;
                break;
            case TRANSLATE_DOWN:
                this.manualTranslation.y += translationFactor;
                break;
            case TRANSLATE_IN:
                this.manualTranslation.z -= translationFactor;
                break;
            case TRANSLATE_OUT:
                this.manualTranslation.z += translationFactor;
                break;
            case ROTATE_LEFT:
                this.manualRotation.x -= RegistrationPanel.ROTATION_ANGLE;
                break;
            case ROTATE_RIGHT:
                this.manualRotation.x += RegistrationPanel.ROTATION_ANGLE;
                break;
            case ROTATE_UP:
                this.manualRotation.y -= RegistrationPanel.ROTATION_ANGLE;
                break;
            case ROTATE_DOWN:
                this.manualRotation.y += RegistrationPanel.ROTATION_ANGLE;
                break;
            case ROTATE_IN:
                this.manualRotation.z += RegistrationPanel.ROTATION_ANGLE;
                break;
            case ROTATE_OUT:
                this.manualRotation.z -= RegistrationPanel.ROTATION_ANGLE;
                break;
            case ZOOM_OUT:
                this.manualScale /= 1.005;
                break;
            case ZOOM_IN:
                this.manualScale *= 1.005;
                break;
            default:
                return;
        }

        getActionListener().actionPerformed(new ActionEvent(
                this,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_IN_PROGRESS)
        );
    }

    @Override
    public ImageIcon getIcon() {
        return getStaticIcon();
    }

    /**
     * Static implementation of the {@link #getIcon()} method.
     *
     * @return Control panel icon
     */
    public static ImageIcon getStaticIcon() {
        return new ImageIcon(RegistrationPanel.class.getClassLoader().getResource("/" + ICON));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        primaryRenderModeGroup = new javax.swing.ButtonGroup();
        secondaryRenerModeGroup = new javax.swing.ButtonGroup();
        precisionGroup = new javax.swing.ButtonGroup();
        jSeparator11 = new javax.swing.JSeparator();
        transformationPanel = new javax.swing.JPanel();
        translationPanel = new javax.swing.JPanel();
        rightTranslationXButton = new javax.swing.JButton();
        leftTranslationYButton = new javax.swing.JButton();
        rightTranslationYButton = new javax.swing.JButton();
        translXLabel = new javax.swing.JLabel();
        translZLabel = new javax.swing.JLabel();
        translYLabel = new javax.swing.JLabel();
        rightTranslationZButton = new javax.swing.JButton();
        leftTranslationXButton = new javax.swing.JButton();
        leftTranslationZButton = new javax.swing.JButton();
        rotationPanel = new javax.swing.JPanel();
        leftRotationYButton = new javax.swing.JButton();
        leftRotationXButton = new javax.swing.JButton();
        rotatZLabel = new javax.swing.JLabel();
        rotatYLabel = new javax.swing.JLabel();
        rotatXLabel = new javax.swing.JLabel();
        rightRotationZButton = new javax.swing.JButton();
        rightRotationYButton = new javax.swing.JButton();
        leftRotationZButton = new javax.swing.JButton();
        rightRotationXButton = new javax.swing.JButton();
        scalePanel = new javax.swing.JPanel();
        scalePlusButton = new javax.swing.JButton();
        scaleMinusButton = new javax.swing.JButton();
        shiftPanel = new javax.swing.JPanel();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        selectableComboBox1 = new cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox();
        jSeparator7 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        featurePointsLabel = new javax.swing.JLabel();
        thersholdFTF = new javax.swing.JFormattedTextField();
        thersholdUpButton = new javax.swing.JButton();
        thresholdDownButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jCheckBox2 = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        jFormattedTextField1 = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jFormattedTextField2 = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        spinSlider1 = new cz.fidentis.analyst.gui.elements.SpinSlider();
        jLabel9 = new javax.swing.JLabel();
        selectableComboBox2 = new cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox();
        jPanel6 = new javax.swing.JPanel();
        infoLinkButton1 = new cz.fidentis.analyst.gui.elements.InfoLinkButton();
        jButton2 = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(600, 600));
        setPreferredSize(new java.awt.Dimension(600, 600));

        transformationPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.transformationPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N
        transformationPanel.setPreferredSize(new java.awt.Dimension(634, 400));

        translationPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.translationPanel.border.title"))); // NOI18N

        rightTranslationXButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-right-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rightTranslationXButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rightTranslationXButton.text")); // NOI18N
        rightTranslationXButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightTranslationXButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        rightTranslationXButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        rightTranslationXButton.setMaximumSize(new java.awt.Dimension(24, 24));
        rightTranslationXButton.setMinimumSize(new java.awt.Dimension(24, 24));
        rightTranslationXButton.setPreferredSize(new java.awt.Dimension(24, 24));
        rightTranslationXButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rightTranslationXButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rightTranslationXButtonMouseReleased(evt);
            }
        });

        leftTranslationYButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-left-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(leftTranslationYButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.leftTranslationYButton.text")); // NOI18N
        leftTranslationYButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftTranslationYButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        leftTranslationYButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        leftTranslationYButton.setPreferredSize(new java.awt.Dimension(24, 24));
        leftTranslationYButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                leftTranslationYButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                leftTranslationYButtonMouseReleased(evt);
            }
        });

        rightTranslationYButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-right-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rightTranslationYButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rightTranslationYButton.text")); // NOI18N
        rightTranslationYButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightTranslationYButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        rightTranslationYButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        rightTranslationYButton.setPreferredSize(new java.awt.Dimension(24, 24));
        rightTranslationYButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rightTranslationYButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rightTranslationYButtonMouseReleased(evt);
            }
        });

        translXLabel.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(translXLabel, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.translXLabel.text")); // NOI18N

        translZLabel.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(translZLabel, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.translZLabel.text")); // NOI18N

        translYLabel.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(translYLabel, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.translYLabel.text")); // NOI18N

        rightTranslationZButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-right-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rightTranslationZButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rightTranslationZButton.text")); // NOI18N
        rightTranslationZButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightTranslationZButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        rightTranslationZButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        rightTranslationZButton.setPreferredSize(new java.awt.Dimension(24, 24));
        rightTranslationZButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rightTranslationZButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rightTranslationZButtonMouseReleased(evt);
            }
        });

        leftTranslationXButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-left-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(leftTranslationXButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.leftTranslationXButton.text")); // NOI18N
        leftTranslationXButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftTranslationXButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        leftTranslationXButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        leftTranslationXButton.setMaximumSize(new java.awt.Dimension(24, 24));
        leftTranslationXButton.setMinimumSize(new java.awt.Dimension(24, 24));
        leftTranslationXButton.setPreferredSize(new java.awt.Dimension(24, 24));
        leftTranslationXButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                leftTranslationXButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                leftTranslationXButtonMouseReleased(evt);
            }
        });

        leftTranslationZButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-left-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(leftTranslationZButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.leftTranslationZButton.text")); // NOI18N
        leftTranslationZButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftTranslationZButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        leftTranslationZButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        leftTranslationZButton.setPreferredSize(new java.awt.Dimension(24, 24));
        leftTranslationZButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                leftTranslationZButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                leftTranslationZButtonMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout translationPanelLayout = new javax.swing.GroupLayout(translationPanel);
        translationPanel.setLayout(translationPanelLayout);
        translationPanelLayout.setHorizontalGroup(
            translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(translationPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(translXLabel)
                    .addGroup(translationPanelLayout.createSequentialGroup()
                        .addComponent(leftTranslationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rightTranslationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(translationPanelLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(leftTranslationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rightTranslationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(translationPanelLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(translYLabel)))
                .addGap(5, 5, 5)
                .addGroup(translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(translationPanelLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(leftTranslationZButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rightTranslationZButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2))
                    .addComponent(translZLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        translationPanelLayout.setVerticalGroup(
            translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(translationPanelLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(translXLabel)
                    .addComponent(translYLabel)
                    .addComponent(translZLabel))
                .addGap(9, 9, 9)
                .addGroup(translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(translationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(rightTranslationZButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(leftTranslationZButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(rightTranslationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(leftTranslationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rightTranslationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(leftTranslationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        rotationPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rotationPanel.border.title"))); // NOI18N

        leftRotationYButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-left-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(leftRotationYButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.leftRotationYButton.text")); // NOI18N
        leftRotationYButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftRotationYButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        leftRotationYButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        leftRotationYButton.setMaximumSize(new java.awt.Dimension(24, 24));
        leftRotationYButton.setMinimumSize(new java.awt.Dimension(24, 24));
        leftRotationYButton.setPreferredSize(new java.awt.Dimension(24, 24));
        leftRotationYButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                leftRotationYButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                leftRotationYButtonMouseReleased(evt);
            }
        });

        leftRotationXButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-left-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(leftRotationXButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.leftRotationXButton.text")); // NOI18N
        leftRotationXButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftRotationXButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        leftRotationXButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        leftRotationXButton.setPreferredSize(new java.awt.Dimension(24, 24));
        leftRotationXButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                leftRotationXButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                leftRotationXButtonMouseReleased(evt);
            }
        });

        rotatZLabel.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rotatZLabel, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rotatZLabel.text")); // NOI18N

        rotatYLabel.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rotatYLabel, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rotatYLabel.text")); // NOI18N

        rotatXLabel.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rotatXLabel, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rotatXLabel.text")); // NOI18N

        rightRotationZButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-right-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rightRotationZButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rightRotationZButton.text")); // NOI18N
        rightRotationZButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightRotationZButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        rightRotationZButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        rightRotationZButton.setMaximumSize(new java.awt.Dimension(24, 24));
        rightRotationZButton.setMinimumSize(new java.awt.Dimension(24, 24));
        rightRotationZButton.setPreferredSize(new java.awt.Dimension(24, 24));
        rightRotationZButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rightRotationZButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rightRotationZButtonMouseReleased(evt);
            }
        });

        rightRotationYButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-right-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rightRotationYButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rightRotationYButton.text")); // NOI18N
        rightRotationYButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightRotationYButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        rightRotationYButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        rightRotationYButton.setMaximumSize(new java.awt.Dimension(24, 24));
        rightRotationYButton.setMinimumSize(new java.awt.Dimension(24, 24));
        rightRotationYButton.setPreferredSize(new java.awt.Dimension(24, 24));
        rightRotationYButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rightRotationYButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rightRotationYButtonMouseReleased(evt);
            }
        });

        leftRotationZButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-left-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(leftRotationZButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.leftRotationZButton.text")); // NOI18N
        leftRotationZButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        leftRotationZButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        leftRotationZButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        leftRotationZButton.setMaximumSize(new java.awt.Dimension(24, 24));
        leftRotationZButton.setMinimumSize(new java.awt.Dimension(24, 24));
        leftRotationZButton.setPreferredSize(new java.awt.Dimension(24, 24));
        leftRotationZButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                leftRotationZButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                leftRotationZButtonMouseReleased(evt);
            }
        });

        rightRotationXButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/arrow-right-s-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rightRotationXButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.rightRotationXButton.text")); // NOI18N
        rightRotationXButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        rightRotationXButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        rightRotationXButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        rightRotationXButton.setMaximumSize(new java.awt.Dimension(24, 24));
        rightRotationXButton.setMinimumSize(new java.awt.Dimension(24, 24));
        rightRotationXButton.setPreferredSize(new java.awt.Dimension(24, 24));
        rightRotationXButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                rightRotationXButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rightRotationXButtonMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout rotationPanelLayout = new javax.swing.GroupLayout(rotationPanel);
        rotationPanel.setLayout(rotationPanelLayout);
        rotationPanelLayout.setHorizontalGroup(
            rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rotationPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(rotationPanelLayout.createSequentialGroup()
                        .addComponent(leftRotationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rightRotationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(rotationPanelLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(rotatXLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(rotationPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(leftRotationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rightRotationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(rotationPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(rotatYLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(rotationPanelLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(rotatZLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(rotationPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(leftRotationZButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rightRotationZButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        rotationPanelLayout.setVerticalGroup(
            rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rotationPanelLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(rotationPanelLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rotatZLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(rotatXLabel, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addComponent(rotatYLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(rotationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rightRotationZButton, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(leftRotationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rightRotationXButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(leftRotationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rightRotationYButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(leftRotationZButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        scalePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.scalePanel.border.title"))); // NOI18N

        scalePlusButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/add-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(scalePlusButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.scalePlusButton.text")); // NOI18N
        scalePlusButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        scalePlusButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        scalePlusButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        scalePlusButton.setMaximumSize(new java.awt.Dimension(24, 24));
        scalePlusButton.setMinimumSize(new java.awt.Dimension(24, 24));
        scalePlusButton.setPreferredSize(new java.awt.Dimension(24, 24));
        scalePlusButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                scalePlusButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                scalePlusButtonMouseReleased(evt);
            }
        });

        scaleMinusButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/subtract-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(scaleMinusButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.scaleMinusButton.text")); // NOI18N
        scaleMinusButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        scaleMinusButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        scaleMinusButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        scaleMinusButton.setMaximumSize(new java.awt.Dimension(24, 24));
        scaleMinusButton.setMinimumSize(new java.awt.Dimension(24, 24));
        scaleMinusButton.setPreferredSize(new java.awt.Dimension(24, 24));
        scaleMinusButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                scaleMinusButtonMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                scaleMinusButtonMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout scalePanelLayout = new javax.swing.GroupLayout(scalePanel);
        scalePanel.setLayout(scalePanelLayout);
        scalePanelLayout.setHorizontalGroup(
            scalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scalePlusButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scaleMinusButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );
        scalePanelLayout.setVerticalGroup(
            scalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scalePanelLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(scalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scalePlusButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(scaleMinusButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout shiftPanelLayout = new javax.swing.GroupLayout(shiftPanel);
        shiftPanel.setLayout(shiftPanelLayout);
        shiftPanelLayout.setHorizontalGroup(
            shiftPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 21, Short.MAX_VALUE)
        );
        shiftPanelLayout.setVerticalGroup(
            shiftPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 84, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout transformationPanelLayout = new javax.swing.GroupLayout(transformationPanel);
        transformationPanel.setLayout(transformationPanelLayout);
        transformationPanelLayout.setHorizontalGroup(
            transformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transformationPanelLayout.createSequentialGroup()
                .addComponent(jSeparator5)
                .addGap(474, 474, 474))
            .addGroup(transformationPanelLayout.createSequentialGroup()
                .addGroup(transformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(transformationPanelLayout.createSequentialGroup()
                        .addGap(580, 580, 580)
                        .addComponent(shiftPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(transformationPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(translationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rotationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scalePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        transformationPanelLayout.setVerticalGroup(
            transformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transformationPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transformationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(scalePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rotationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(translationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(shiftPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jPanel1.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N
        jPanel1.setMaximumSize(new java.awt.Dimension(600, 32767));

        jButton1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jButton1, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jButton1.text")); // NOI18N
        jButton1.setToolTipText(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jButton1.toolTipText")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        selectableComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectableComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selectableComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(0, 269, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectableComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(0, 6, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jPanel2.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel1.text")); // NOI18N

        jTextField1.setText(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jTextField1.text")); // NOI18N
        jTextField1.setEnabled(false);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel2.text")); // NOI18N

        jTextField2.setText(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jTextField2.text")); // NOI18N
        jTextField2.setEnabled(false);

        featurePointsLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(featurePointsLabel, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.featurePointsLabel.text")); // NOI18N

        thersholdFTF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        thersholdUpButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/add-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(thersholdUpButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.thersholdUpButton.text")); // NOI18N
        thersholdUpButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        thersholdUpButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        thersholdUpButton.setMaximumSize(new java.awt.Dimension(24, 24));
        thersholdUpButton.setMinimumSize(new java.awt.Dimension(24, 24));
        thersholdUpButton.setPreferredSize(new java.awt.Dimension(24, 24));
        thersholdUpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                thersholdUpButtonActionPerformed(evt);
            }
        });

        thresholdDownButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/subtract-line.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(thresholdDownButton, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.thresholdDownButton.text")); // NOI18N
        thresholdDownButton.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        thresholdDownButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        thresholdDownButton.setMaximumSize(new java.awt.Dimension(24, 24));
        thresholdDownButton.setMinimumSize(new java.awt.Dimension(24, 24));
        thresholdDownButton.setPreferredSize(new java.awt.Dimension(24, 24));
        thresholdDownButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                thresholdDownButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(featurePointsLabel)
                        .addGap(4, 4, 4)
                        .addComponent(thersholdFTF, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(thersholdUpButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(thresholdDownButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(featurePointsLabel)
                        .addComponent(thersholdFTF, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(thersholdUpButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(thresholdDownButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jPanel3.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel3.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jCheckBox1, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jCheckBox1.text")); // NOI18N
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jPanel5.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel4.text")); // NOI18N

        jCheckBox2.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(jCheckBox2, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jCheckBox2.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel5.text")); // NOI18N

        jFormattedTextField1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        jFormattedTextField1.setText(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jFormattedTextField1.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel6.text")); // NOI18N

        jFormattedTextField2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getIntegerInstance())));
        jFormattedTextField2.setText(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jFormattedTextField2.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel8, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel8.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel9, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jLabel9.text")); // NOI18N

        selectableComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectableComboBox2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(selectableComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel4))
                            .addComponent(jLabel6)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(spinSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jFormattedTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBox2))))
                .addContainerGap(98, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jCheckBox2))
                .addGap(8, 8, 8)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jFormattedTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spinSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(selectableComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jCheckBox1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jPanel6.border.title"))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(infoLinkButton1, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.infoLinkButton1.text")); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(infoLinkButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(infoLinkButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
        );

        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(RegistrationPanel.class, "RegistrationPanel.jButton2.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(805, 805, 805)
                .addComponent(jSeparator11))
            .addGroup(layout.createSequentialGroup()
                .addGap(791, 791, 791)
                .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 1, Short.MAX_VALUE)
                .addGap(450, 450, 450))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(transformationPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 536, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(transformationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addGap(661, 661, 661)
                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(221, 221, 221)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void thresholdDownButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_thresholdDownButtonActionPerformed
        double newValue = ((Number)thersholdFTF.getValue()).doubleValue() - 0.1;
        thersholdFTF.setValue(newValue);
        thersholdFTF.postActionEvent();
    }//GEN-LAST:event_thresholdDownButtonActionPerformed

    private void thersholdUpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_thersholdUpButtonActionPerformed
        double newValue = ((Number)thersholdFTF.getValue()).doubleValue() + 0.1;
        thersholdFTF.setValue(newValue);
        thersholdFTF.postActionEvent();
    }//GEN-LAST:event_thersholdUpButtonActionPerformed

    private void scaleMinusButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scaleMinusButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_scaleMinusButtonMouseReleased

    private void scaleMinusButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scaleMinusButtonMousePressed
        animator.startAnimation(AnimationDirection.ZOOM_OUT, this);
    }//GEN-LAST:event_scaleMinusButtonMousePressed

    private void scalePlusButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scalePlusButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_scalePlusButtonMouseReleased

    private void scalePlusButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scalePlusButtonMousePressed
        animator.startAnimation(AnimationDirection.ZOOM_IN, this);
    }//GEN-LAST:event_scalePlusButtonMousePressed

    private void rightRotationXButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightRotationXButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_rightRotationXButtonMouseReleased

    private void rightRotationXButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightRotationXButtonMousePressed
        animator.startAnimation(AnimationDirection.ROTATE_RIGHT, this);
    }//GEN-LAST:event_rightRotationXButtonMousePressed

    private void leftRotationZButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftRotationZButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_leftRotationZButtonMouseReleased

    private void leftRotationZButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftRotationZButtonMousePressed
        animator.startAnimation(AnimationDirection.ROTATE_IN, this);
    }//GEN-LAST:event_leftRotationZButtonMousePressed

    private void rightRotationYButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightRotationYButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_rightRotationYButtonMouseReleased

    private void rightRotationYButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightRotationYButtonMousePressed
        animator.startAnimation(AnimationDirection.ROTATE_DOWN, this);
    }//GEN-LAST:event_rightRotationYButtonMousePressed

    private void rightRotationZButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightRotationZButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_rightRotationZButtonMouseReleased

    private void rightRotationZButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightRotationZButtonMousePressed
        animator.startAnimation(AnimationDirection.ROTATE_OUT, this);
    }//GEN-LAST:event_rightRotationZButtonMousePressed

    private void leftRotationXButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftRotationXButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_leftRotationXButtonMouseReleased

    private void leftRotationXButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftRotationXButtonMousePressed
        animator.startAnimation(AnimationDirection.ROTATE_LEFT, this);
    }//GEN-LAST:event_leftRotationXButtonMousePressed

    private void leftRotationYButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftRotationYButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_leftRotationYButtonMouseReleased

    private void leftRotationYButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftRotationYButtonMousePressed
        animator.startAnimation(AnimationDirection.ROTATE_UP, this);
    }//GEN-LAST:event_leftRotationYButtonMousePressed

    private void leftTranslationZButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftTranslationZButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_leftTranslationZButtonMouseReleased

    private void leftTranslationZButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftTranslationZButtonMousePressed
        animator.startAnimation(AnimationDirection.TRANSLATE_IN, this);
    }//GEN-LAST:event_leftTranslationZButtonMousePressed

    private void leftTranslationXButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftTranslationXButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_leftTranslationXButtonMouseReleased

    private void leftTranslationXButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftTranslationXButtonMousePressed
        animator.startAnimation(AnimationDirection.TRANSLATE_LEFT, this);
    }//GEN-LAST:event_leftTranslationXButtonMousePressed

    private void rightTranslationZButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightTranslationZButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_rightTranslationZButtonMouseReleased

    private void rightTranslationZButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightTranslationZButtonMousePressed
        animator.startAnimation(AnimationDirection.TRANSLATE_OUT, this);
    }//GEN-LAST:event_rightTranslationZButtonMousePressed

    private void rightTranslationYButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightTranslationYButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_rightTranslationYButtonMouseReleased

    private void rightTranslationYButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightTranslationYButtonMousePressed
        animator.startAnimation(AnimationDirection.TRANSLATE_DOWN, this);
    }//GEN-LAST:event_rightTranslationYButtonMousePressed

    private void leftTranslationYButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftTranslationYButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_leftTranslationYButtonMouseReleased

    private void leftTranslationYButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftTranslationYButtonMousePressed
        animator.startAnimation(AnimationDirection.TRANSLATE_UP, this);
    }//GEN-LAST:event_leftTranslationYButtonMousePressed

    private void rightTranslationXButtonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightTranslationXButtonMouseReleased
        animator.stopAnimation();
        getActionListener().actionPerformed(new ActionEvent(
                evt,
                ActionEvent.ACTION_PERFORMED,
                ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED)
        );
    }//GEN-LAST:event_rightTranslationXButtonMouseReleased

    private void rightTranslationXButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightTranslationXButtonMousePressed
        animator.startAnimation(AnimationDirection.TRANSLATE_RIGHT, this);
    }//GEN-LAST:event_rightTranslationXButtonMousePressed

    private void selectableComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectableComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_selectableComboBox2ActionPerformed

    private void selectableComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectableComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_selectableComboBox1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel featurePointsLabel;
    private cz.fidentis.analyst.gui.elements.InfoLinkButton infoLinkButton1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JFormattedTextField jFormattedTextField1;
    private javax.swing.JFormattedTextField jFormattedTextField2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JButton leftRotationXButton;
    private javax.swing.JButton leftRotationYButton;
    private javax.swing.JButton leftRotationZButton;
    private javax.swing.JButton leftTranslationXButton;
    private javax.swing.JButton leftTranslationYButton;
    private javax.swing.JButton leftTranslationZButton;
    private javax.swing.ButtonGroup precisionGroup;
    private javax.swing.ButtonGroup primaryRenderModeGroup;
    private javax.swing.JButton rightRotationXButton;
    private javax.swing.JButton rightRotationYButton;
    private javax.swing.JButton rightRotationZButton;
    private javax.swing.JButton rightTranslationXButton;
    private javax.swing.JButton rightTranslationYButton;
    private javax.swing.JButton rightTranslationZButton;
    private javax.swing.JLabel rotatXLabel;
    private javax.swing.JLabel rotatYLabel;
    private javax.swing.JLabel rotatZLabel;
    private javax.swing.JPanel rotationPanel;
    private javax.swing.JButton scaleMinusButton;
    private javax.swing.JPanel scalePanel;
    private javax.swing.JButton scalePlusButton;
    private javax.swing.ButtonGroup secondaryRenerModeGroup;
    private cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox selectableComboBox1;
    private cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox selectableComboBox2;
    private javax.swing.JPanel shiftPanel;
    private cz.fidentis.analyst.gui.elements.SpinSlider spinSlider1;
    private javax.swing.JFormattedTextField thersholdFTF;
    private javax.swing.JButton thersholdUpButton;
    private javax.swing.JButton thresholdDownButton;
    private javax.swing.JPanel transformationPanel;
    private javax.swing.JLabel translXLabel;
    private javax.swing.JLabel translYLabel;
    private javax.swing.JLabel translZLabel;
    private javax.swing.JPanel translationPanel;
    // End of variables declaration//GEN-END:variables
}
