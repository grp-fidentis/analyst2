/**
 * A control panel which displays info about a selected face of the project
 */
package cz.fidentis.analyst.gui.project.faceinfo;
