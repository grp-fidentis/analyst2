package cz.fidentis.analyst.gui.app.tools;

import cz.fidentis.analyst.data.face.*;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshConfig;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshVisitor;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A class for testing the efficiency of batch Ray tracing algorithm. Algorithm
 * is used on multiple faces.
 *
 * @author Enkh-Undral EnkhBayar
 */
public class BatchOctreeTestsMultipleFaces {

    private static final int LIMIT = 500;
    private static final String DATA_DIR = "/home/uenkhbayar/git/fidentis/analyst-data/New folder";
    private static final String PATH_AVG_TIME = "/home/uenkhbayar/git/fidentis/avgFace.obj";

    private static final Stopwatch TOTAL_TIME = new Stopwatch("Total computation time:\t");
    private static final Stopwatch AVG_FACE_COMPUTATION_TIME = new Stopwatch("AVG face computation time:\t");
    private static final Stopwatch ICP_COMPUTATION_TIME = new Stopwatch("ICP registration time:\t");
    private static final Stopwatch LOAD_TIME = new Stopwatch("File (re-)loading time:\t");
    private static final Stopwatch KD_TREE_CONSTRUCTION_TIME = new Stopwatch("KD trees construction time:\t");
    private static final Stopwatch OCTREE_CONSTRUCTION_TIME = new Stopwatch("Octree construction time:\t");

    /**
     * 
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        List<Path> faces = Files.list(new File(DATA_DIR).toPath())
                .filter(f -> f.toString().endsWith(".obj"))
                .sorted()
                .limit(LIMIT)
                .collect(Collectors.toList());

        FaceService factory = FaceService.INSTANCE;

        int initialFaceIndex = 0;
        HumanFace avgFace;

        int undersampling = 100;
        boolean computeICP = true;
        boolean computeAvgFace = true;

        FaceService.INSTANCE.setDumpStrategy(HumanFaceMemoryManager.Strategy.MRU);

        TOTAL_TIME.start();

        LOAD_TIME.start();
        FaceReference faceReference = factory.loadFace(faces.get(initialFaceIndex).toFile());
        HumanFace initFace = factory.getFaceByReference(faceReference);
        LOAD_TIME.stop();

        AvgMeshVisitor avgFaceConstructorOctree = null;

        for (int i = 0; i < faces.size(); i++) {

            // Compute AVG template face. Use each tranfromed face only once. Skip the original face
            if (i != initialFaceIndex && (computeICP || computeAvgFace)) {

                LOAD_TIME.start();
                FaceReference reference = factory.loadFace(faces.get(i).toFile());
                HumanFace face = factory.getFaceByReference(reference);
                LOAD_TIME.stop();

                if (computeICP) {// ICP registration:
                    ICP_COMPUTATION_TIME.start();
                    FaceRegistrationServices.alignMeshes(
                            face, // is transformed
                            new IcpConfig(
                                    initFace.getMeshModel(),
                                    100,
                                    false,
                                    0.3,
                                    new PointSamplingConfig(PointSamplingConfig.Method.RANDOM, undersampling),
                                    1)
                    );
                    ICP_COMPUTATION_TIME.stop();
                }

                if (computeAvgFace) { // AVG template face
                    OCTREE_CONSTRUCTION_TIME.start();
                    FaceStateServices.updateOctree(face, FaceStateServices.Mode.COMPUTE_ALWAYS);
                    OCTREE_CONSTRUCTION_TIME.stop();

                    AVG_FACE_COMPUTATION_TIME.start();
                    if (avgFaceConstructorOctree == null) {
                        avgFaceConstructorOctree = (new AvgMeshConfig(initFace.getMeshModel(), null)).getRayCastingVisitor();
                    }
                    face.getOctree().accept(avgFaceConstructorOctree);
                    AVG_FACE_COMPUTATION_TIME.stop();

                    FaceStateServices.updateOctree(face, FaceStateServices.Mode.DELETE);
                }
            }
        }

        if (computeAvgFace) {
            File tempFile = new File(PATH_AVG_TIME);
            FaceReference avgFaceReference = factory.createFaceFromMeshModel(avgFaceConstructorOctree.getAveragedMeshModel(), tempFile, true);
            avgFace = factory.getFaceByReference(avgFaceReference);
            try {
                MeshIO.exportMeshModel(avgFace.getMeshModel(), tempFile);
            } catch (IOException ex) {
                System.err.println(ex.toString());
            }
        }

        TOTAL_TIME.stop();

        printTimeStats();
    }

    protected static void printTimeStats() {
        System.out.println(AVG_FACE_COMPUTATION_TIME.toString());
        System.out.println(ICP_COMPUTATION_TIME.toString());
        System.out.println(LOAD_TIME.toString());
        System.out.println(OCTREE_CONSTRUCTION_TIME.toString());
        System.out.println(KD_TREE_CONSTRUCTION_TIME.toString());
        System.out.println(TOTAL_TIME.toString());
    }

}
