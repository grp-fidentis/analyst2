package cz.fidentis.analyst.gui.elements.histogram.scales;

import java.util.List;

/**
 * Interface for classes that perform scaling/transformation of values from one range to another one.
 * Source range is called "domain" and target range is called "range".
 *
 * @author Jakub Nezval
 * @param <T> domain type
 * @param <U> range type
 */
public interface Scale<T, U> {
    /**
     * Scale {@code value} from domain to range.
     *
     * @param value value
     * @return scaled value
     */
    U map(T value);

    /**
     * Set domain using two points.
     *
     * @param start start
     * @param end end
     */
    void setDomain(T start, T end);

    /**
     * Set domain using list of values.
     *
     * @param values values
     */
    void setDomain(List<T> values);

    /**
     * Set range using two points.
     *
     * @param start start
     * @param end end
     */
    void setRange(U start, U end);

    /**
     * Set range using list of values.
     *
     * @param values values
     */
    void setRange(List<U> values);
}
