/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.fidentis.analyst.gui.elements;

import cz.fidentis.analyst.data.surfacemask.*;
import cz.fidentis.analyst.gui.elements.surfacemaskdrawables.DrawableSurfaceMask2D;
import cz.fidentis.analyst.gui.elements.surfacemaskdrawables.DrawableSurfaceMaskEllipse;
import cz.fidentis.analyst.gui.elements.surfacemaskdrawables.DrawableSurfaceMaskLine;
import cz.fidentis.analyst.gui.elements.surfacemaskdrawables.DrawableSurfaceMaskRectangle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/**
 * A panel to draw an interacitve mask on.
 * @author Mario Chromik
 */
public class SurfaceMaskPanel extends CurveRenderingPanel {
    private final SurfaceMaskLayers layerManager;
    private DrawableSurfaceMask2D drawableMask;
    private ActionListener panelListenerProject;
    private ActionListener panelListenerSelected;
    private boolean needsLayerUpdate = true;

    /**
     * Constructor setting mouse listeners and drawing an initial mask.
     */
    public SurfaceMaskPanel() {
        layerManager = new SurfaceMaskLayers();
        setBackground(Color.WHITE);
        Adapter ml = new Adapter();
        addMouseListener(ml);
        addMouseMotionListener(new MotionAdapter());
    }

    public SurfaceMaskLayers getLayerManager() {
        return layerManager;
    }

    /**
     * Updates the drawable mask based on the current surface mask.
     * @param mask the surface mask to update from
     */
    private void updateDrawableMask(SurfaceMask2D mask) {
        if (!needsLayerUpdate) return;

        if (mask instanceof SurfaceMaskEllipse) {
            drawableMask = new DrawableSurfaceMaskEllipse((SurfaceMaskEllipse) mask);
        } else if (mask instanceof SurfaceMaskRectangle) {
            drawableMask = new DrawableSurfaceMaskRectangle((SurfaceMaskRectangle) mask);
        } else if (mask instanceof SurfaceMaskLine) {
            drawableMask = new DrawableSurfaceMaskLine((SurfaceMaskLine) mask);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (layerManager.getLayers().isEmpty()) {
            return;
        }

        g.setColor(Color.BLACK);
        Layer layer = layerManager.getCurrentLayer();
        if (layer != null && layer.isVisible()) {
            for (SurfaceMask2D mask : layer.getAllMasks()) {
                updateDrawableMask(mask);
                drawableMask.draw(g);
            }
        }

        needsLayerUpdate = false;
    }

    /**
     * Gets panel width
     * @return integer panel width
     */
    public int getPanelWidth() {
        return (int)this.getSize().getWidth();
    }

    /**
     * Gets panel height
     * @return integer panel height
     */
    public int getPanelHeight() {
        return (int)this.getSize().getHeight();
    }

    /**
     * Adds action listener for projection of mask
     * @param listener action listener
     */
    public void addActionListenerProject(ActionListener listener) {
        this.panelListenerProject = listener;
    }

    /**
     * Adds action listener for selection of mask
     * @param listener action listener
     */
    public void addActionListenerSelected(ActionListener listener) {
        this.panelListenerSelected = listener;
    }

    /**
     * Sets state to param
     * @param needsLayerUpdate state
     */
    public void setNeedsLayerUpdate(boolean needsLayerUpdate) {
        this.needsLayerUpdate = needsLayerUpdate;
    }

    /**
     * Custom mouse listener to handle drawing a mask
     * @author Mario Chromik
     */
    class Adapter extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            if (layerManager.getCurrentLayer() == null || layerManager.getCurrentLayer().getCurrentMask() == null) {
                return;
            }

            if (SwingUtilities.isLeftMouseButton(e)) {
                layerManager.getCurrentLayer().getCurrentMask().addNewPoint(e.getPoint());
            } else if (SwingUtilities.isRightMouseButton(e)) {
                if (layerManager.getCurrentLayer().getCurrentMask() instanceof SurfaceMaskLine) {
                   ((SurfaceMaskLine) layerManager.getCurrentLayer().getCurrentMask()).deletePoint(e.getPoint());
               }
            }

            if (e.isShiftDown()) {
                layerManager.getCurrentLayer().getCurrentMask().containsPoint(e.getPoint());
            }
            needsLayerUpdate = true;
            repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (layerManager.getCurrentLayer() == null || layerManager.getCurrentLayer().getCurrentMask() == null) {
                return;
            }

            layerManager.getCurrentLayer().getCurrentMask().setSelectedPoint(null);
            layerManager.getCurrentLayer().getCurrentMask().setShiftPoint(null);
            panelListenerProject.actionPerformed( new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Mask updated"));

            needsLayerUpdate = true;
        }
    }

    /**
     * Custom mouse motion listener to hande dragging a point
     * @author Mario Chromik
     */
    class MotionAdapter extends MouseMotionAdapter {
        @Override
        public void mouseDragged(MouseEvent e) {
            if (layerManager.getCurrentLayer() == null || layerManager.getCurrentLayer().getCurrentMask() == null) {
                return;
            }

            if (e.getPoint().x < 0 || e.getPoint().x > getPanelWidth()
                    || e.getPoint().y < 0 || e.getPoint().y > getPanelHeight()) {
                //if point is outside drawing panel unset selected and shifted point
                layerManager.getCurrentLayer().getCurrentMask().setSelectedPoint(null);
                layerManager.getCurrentLayer().getCurrentMask().setShiftPoint(null);
            }
            if (e.isShiftDown() && layerManager.getCurrentLayer().getCurrentMask().getShiftPoint() != null) {
                int dx = e.getPoint().x - layerManager.getCurrentLayer().getCurrentMask().getShiftPoint().x;
                int dy = e.getPoint().y - layerManager.getCurrentLayer().getCurrentMask().getShiftPoint().y;
                layerManager.getCurrentLayer().getCurrentMask().shiftMask(dx, dy);
                layerManager.getCurrentLayer().getCurrentMask().setShiftPoint(e.getPoint());
                repaint();
            }
            if (layerManager.getCurrentLayer().getCurrentMask().getSelectedPoint() != null) {
                layerManager.getCurrentLayer().getCurrentMask().updateSelectedPoint(e.getPoint());
                repaint();
            }
            needsLayerUpdate = true;
        }
        @Override
        public void mouseMoved(MouseEvent e) {
            if (layerManager.getCurrentLayer() == null || layerManager.getCurrentLayer().getCurrentMask() == null) {
                return;
            }

            if (layerManager.getCurrentLayer().getCurrentMask().selectPoint(e.getPoint())) {
                setCursor(new Cursor(Cursor.HAND_CURSOR));
                repaint();
                panelListenerSelected.actionPerformed( new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Point selected"));
            } else if (layerManager.getCurrentLayer().getCurrentMask().getSelectedPoint() != null) {
                layerManager.getCurrentLayer().getCurrentMask().setSelectedPoint(null);
                repaint();
                setCursor(Cursor.getDefaultCursor());
                panelListenerSelected.actionPerformed( new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Point selected"));
            } else {
                repaint();
                setCursor(Cursor.getDefaultCursor());
            }
            needsLayerUpdate = true;
        }
    }
}

