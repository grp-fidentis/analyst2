package cz.fidentis.analyst.gui.project.filter;

import cz.fidentis.analyst.data.landmarks.FeaturePointsTable;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.LandmarksFactory;

import javax.swing.*;
import javax.vecmath.Point3d;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Matej Kovar
 */
public class FeaturePointsFilterPanel extends JPanel {
 
    private List<JCheckBox> featurePointCheckBoxes;
    private List<Landmark> selectedFeaturePoints = new ArrayList<>();
    
    /**
     * Constructor
     */
    public void initComponents() {
        setLayout(new GridBagLayout());

        featurePointCheckBoxes = new ArrayList<>();

        // Load all types of feature points
        int i = 0;
        Set<Integer> featurePoints = FeaturePointsTable.getInstance().getIDs();
        for (Integer fpID: FeaturePointsTable.getInstance().getIDs()) {
            addRow(i++, fpID);
        }
    }

    protected void addRow(int row, int fpID) {
        
        GridBagConstraints c1 = new GridBagConstraints();  
        c1.insets = new Insets(0, 0, 0, 0);
        c1.gridwidth = 1;
        c1.gridx = 0;
        c1.gridy = row;
        c1.anchor = GridBagConstraints.WEST;
        c1.fill = GridBagConstraints.NONE;

        Landmark fp = LandmarksFactory.createFeaturePoint(fpID, new Point3d());

        JCheckBox checkBox = new JCheckBox();
        checkBox.setSelected(false);
        checkBox.setEnabled(false);
        checkBox.setText(fp.getName());
        add(checkBox, c1);
        featurePointCheckBoxes.add(checkBox);
        
        checkBox.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selectedFeaturePoints.remove(fp)) {
                    selectedFeaturePoints.add(fp);
                }
            }
        });
    }
    
    /**
     * Deselects and disables(enables) all check boxes with each feature point type
     * @param enable enables or disables check boxes
     */
    public void disableCheckBoxes(boolean enable) {
        featurePointCheckBoxes.forEach(ch -> {
            
            if (!enable) {
                ch.setSelected(false);
            }
            ch.setEnabled(enable);
        });
    }

    public List<Landmark> getSelectedFeaturePoints() {
        return selectedFeaturePoints;
    }
    
    @Override
    public void setEnabled(boolean enable) {
        this.getParent().setEnabled(enable);
        disableCheckBoxes(enable);
    }
    
}
