package cz.fidentis.analyst.gui.task.curvature;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.project.Task;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.*;

/**
 * Action listener for the curvature computation.
 * 
 * @author Radek Oslejsek
 * @author Jakub Nezval
 */
public class CurvatureAction extends ControlPanelAction<CurvaturePanel> {
    
    /*
     * Attributes handling the state
     */
    private String curvatureType = CurvaturePanel.CURVATURE_GAUSSIAN;
    private Map<MeshFacet, List<Double>> heatmap;

    /**
     * Constructor.
     * A new {@code CurvaturePanel} is instantiated and added to the {@code topControlPane}
     * 
     * @param canvas OpenGL canvas
     * @param faces Faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public CurvatureAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);
        setControlPanel(new CurvaturePanel(this));
        
        setShowHideCode(
                () -> { // SHOW
                    // provide a code that runs when the panel is focused (selected)
                    setHeatmap();
                },
                () -> { // HIDE
                    // provide a code that runs when the panel is closed (hidden)
                    getPrimaryDrawableFace().setHeatMap(null);
                }
        );
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        
        switch (action) {
            case CurvaturePanel.ACTION_COMMAND_SET_CURVATURE_TYPE:
                var newCurvature = (String) ((JComboBox<?>) ae.getSource()).getSelectedItem();
                if (Objects.equals(newCurvature, curvatureType)) {
                    break;
                }

                curvatureType = newCurvature;
                setHeatmap();
                break;
            case CurvaturePanel.ACTION_COMMAND_HISTOGRAM_BOUNDS_MOVED:
                updateVisibleHeatmap();
                break;
            default:
                // to nothing
        }
        
        renderScene();
    }

    protected void setHeatmap() {
        var hist = getControlPanel().getHistogramComponent();
        this.heatmap = calcHeatmap();

        hist.setValues(
                this.heatmap.values()
                        .stream()
                        .flatMap(Collection::stream)
                        .toList()
        );

        hist.resetBounds();
        updateVisibleHeatmap();
    }

    protected void updateVisibleHeatmap() {
        var hist = this.getControlPanel().getHistogramComponent();

        double minValue = hist.getLowerBoundValue();
        double maxValue = hist.getUpperBoundValue();

        var saturation = new HashMap<>(this.heatmap);
        saturation.replaceAll(
                (k, v) -> v.stream()
                        .map(n -> minValue <= n && n <= maxValue ? 1d : 0d)
                        .toList()
        );

        var heatmap = new HeatMap3D(this.heatmap, saturation, minValue, maxValue);
        getPrimaryDrawableFace().setHeatMap(heatmap);
    }

    protected Map<MeshFacet, List<Double>> calcHeatmap() {
        FaceStateServices.updateCurvature(getPrimaryDrawableFace().getHumanFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        Map<MeshFacet, List<Double>> heatmapValues = new HashMap<>();

        for (MeshFacet facet: getPrimaryDrawableFace().getModel().getFacets()) {
            List<Double> values = facet.getVertices().stream()
                    .map(mp -> switch (this.curvatureType) {
                        case CurvaturePanel.CURVATURE_GAUSSIAN -> mp.getCurvature().gaussian();
                        case CurvaturePanel.CURVATURE_MEAN -> mp.getCurvature().mean();
                        case CurvaturePanel.CURVATURE_MIN -> mp.getCurvature().minPrincipal();
                        case CurvaturePanel.CURVATURE_MAX -> mp.getCurvature().maxPrincipal();
                        default -> throw new UnsupportedOperationException(curvatureType);
                    })
                    .toList();
            heatmapValues.put(facet, values);
        }

        return heatmapValues;
    }
}
