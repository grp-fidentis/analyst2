package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.project.*;
import cz.fidentis.analyst.rendering.Camera;

import javax.swing.*;
import java.util.function.BiConsumer;

/**
 * A worker for loading multiple task tabs (faces and cameras) of a project from dumped files.
 * 
 * @author Matej Kovar
 */
public class RecoverTasksWorker extends SwingWorker<Boolean, Integer> {
    
    private final Project project;
    private final ProgressDialog<Boolean, Integer> progressDialog;
    private final BiConsumer<Camera, Task> consumer;
    
    /**
     * Constructor.
     * 
     * @param project Project with info about project folder containing stored tasks
     * @param progressDialog A progress dialog window
     * @param consumer A method that creates an analytical tab from loaded camera and faces
     * @throws IllegalArgumentException if the project has not a folder assigned
     */
    public RecoverTasksWorker(Project project, ProgressDialog<Boolean, Integer> progressDialog, BiConsumer<Camera, Task> consumer) {
        if (!project.hasProjectDir()) {
            throw new IllegalArgumentException("project");
        }
        this.project = project;
        this.progressDialog = progressDialog;
        this.consumer = consumer;
    }

    @Override
    protected Boolean doInBackground() throws Exception {
        for (Task task : project.getTasks()) {
            this.consumer.accept(null, task);
        }
        return true;
    }
    
    @Override
    protected void done() {
        progressDialog.dispose();
    }
}
