package cz.fidentis.analyst.gui.task.batch;

import java.time.Duration;

/**
 * Stopwatch.
 * 
 * @author Radek Oslejsek
 */
public class Stopwatch {

    private final String label;
    private long totalTime = 0;
    private long spanStartTime = 0;
    
    /**
     * Constructor.
     * @param label label used for printing the time. Can be {@code null}
     */
    public Stopwatch(String label) {
        this.label = label;
    }
    
    /**
     * Starts measuring a time span.
     */
    public void start() {
        spanStartTime = System.currentTimeMillis();
    }
    
    /**
     * Stops the time span. Span is added to total measured time and also returned.
     * 
     * @return span duration in milliseconds
     */
    public long stop() {
        long span = System.currentTimeMillis() - spanStartTime;
        totalTime += span;
        return span;
    }
    
    /**
     * Returns the sum of time spans measured so far. 
     * Call the {@link #stop()} first to include the last span as well.
     * 
     * @return total measured time in milliseconds
     */
    public long getTotalTime() {
        return totalTime;
    }
    
    /**
     * The same as {@link #getTotalTime()}, but the time is returned as {@code Duration}
     * @return total measured time as {@code Duration}
     */
    public Duration getDuration() {
        return Duration.ofMillis(totalTime);
    }
    
    /**
     * Rests the stopwatch, i.e., clears the all previous time measurements.
     */
    public void reset() {
        totalTime = 0;
        spanStartTime = 0;
    }
    
    @Override
    public String toString() {
        Duration duration = getDuration();
        StringBuilder builder = new StringBuilder();
        builder.append((label == null) ? "" : label+": ").append(
                String.format("%02d:%02d:%02d,%03d", 
                        duration.toHoursPart(), 
                        duration.toMinutesPart(), 
                        duration.toSecondsPart(), 
                        duration.toMillisPart())
        );
        return builder.toString();
    }
}
