package cz.fidentis.analyst.gui.app.tools;

import com.opencsv.CSVWriter;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceFactory;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.face.FaceDistanceServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryServices;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;

import javax.vecmath.Vector3d;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Class for testing symmetry precision and computation time of selected computation settings (symmetry method,
 * sub-sampling strategy, search density, pruning density). Statistics of symmetry computation are stored in csv file.
 *
 * @author Patrik Brosz
 */
public class SymmetryComputationStats {
    // Directory containing scans, scans in dir will be used in experiment
    private static final String SCANS_DIR = "../data_scans";
    // Limit for number of scans used for experiment
    private static final int MAX_SCANS = 100;

    // Path for output directory and name of files for storing symmetry stats and average planes data
    private static final String OUTPUT_DIR_PATH = "../measurements";
    private static final String OUTPUT_FILENAME_SYMMETRY_STATS = "symmetry_stats.csv";
    private static final String OUTPUT_FILENAME_AVERAGE_PLANES = "average_planes.csv";

    private static final Stopwatch COMPUTATION_TIME_SW = new Stopwatch(null);

    // Experiment settings
    private static final int CALCULATION_REPS = 40; // number of experiment repetitions
    private static final SymmetryConfig.Method SYMMETRY_METHOD = SymmetryConfig.Method.ROBUST_POINT_CLOUD; // symmetry method used in experiments
    private static final List<Integer> SEARCH_DENSITY_VALUES = new ArrayList<>(List.of( // search density values used in experiments
            100, 200, 300, 500, 800));
    private static final List<Integer> PRUNING_DENSITY_VALUES = new ArrayList<>(List.of( // pruning density values used in experiments
            400, 800, 1600, 3200));
    private static final List<PointSamplingConfig.Method> SUBSAMPLING_STRATEGIES = List.of( // sub-sampling strategies used in experiments
            PointSamplingConfig.Method.RANDOM,
            PointSamplingConfig.Method.UNIFORM_SPACE,
            PointSamplingConfig.Method.UNIFORM_SURFACE,
            PointSamplingConfig.Method.POISSON_OPENCL,
            PointSamplingConfig.Method.CURVATURE_GAUSSIAN
    );

    private static CSVWriterService symmetryStatsWriter;
    private static CSVWriterService avgPlanesWriter;

    /**
     * Main method - runs symmetry plane computation for given parameters and stores statistics about runs and
     * average planes to csv files
     *
     * @param args Input arguments
     * @throws IOException on IO error
     */
    public static void main(String[] args) throws IOException {
        List<Path> faces = FaceLoader.loadFaces(SCANS_DIR, MAX_SCANS);

        symmetryStatsWriter = new CSVWriterService(OUTPUT_DIR_PATH, OUTPUT_FILENAME_SYMMETRY_STATS);
        avgPlanesWriter = new CSVWriterService(OUTPUT_DIR_PATH, OUTPUT_FILENAME_AVERAGE_PLANES);

        symmetryStatsWriter.writeHeaders(new String[]{"Face", "Method", "Search dens", "Pruning dens", "Hausdorff distance",
                "Chamfer distance", "Mean distance", "Calculation time"});
        avgPlanesWriter.writeHeaders(new String[]{"Face", "Method", "Search dens", "Pruning dens", "Distance", "Vector X", "Vector Y", "Vector Z"});

        for (int i = 0; i < faces.size(); i++) {
            System.out.println(i + ": " + faces.get(i));

            HumanFace face = HumanFaceFactory.create(-1L, faces.get(i).toFile());
            FaceStateServices.updateCurvature(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            for (int densitySearchValue : SEARCH_DENSITY_VALUES) {
                for (int densityPruningValue : PRUNING_DENSITY_VALUES) {
                    calculateSymmetryStats(densitySearchValue, densityPruningValue, face);
                }
            }
        }
        symmetryStatsWriter.close();
        avgPlanesWriter.close();
    }

    /**
     * Run of experiment - calculates the symmetry plane in the defined number of runs with the specified settings and saves
     * the statistics to a csv file average planes to csv files
     *
     * @param densitySearchValue  Input arguments
     * @param densityPruningValue Input arguments
     * @param face                Human Face (or other type of 3D scan)
     */
    private static void calculateSymmetryStats(int densitySearchValue, int densityPruningValue, HumanFace face) {
        for (PointSamplingConfig.Method subsamplingStrategy : SUBSAMPLING_STRATEGIES) {
            List<Plane> planesForAvg = new ArrayList<>();
            for (int repNumber = 0; repNumber < CALCULATION_REPS; repNumber++) {
                System.out.printf("Run: %d| strategy: %s, search density: %d, pruning density: %d%n",
                        repNumber, subsamplingStrategy.name(), densitySearchValue, densityPruningValue);

                PointSamplingConfig samplingStrategy0 = new PointSamplingConfig(subsamplingStrategy, densitySearchValue);
                SymmetryConfig estimator0 = new SymmetryConfig(SYMMETRY_METHOD, samplingStrategy0, densityPruningValue);

                COMPUTATION_TIME_SW.start();
                // Computation of symmetry plane
                face.setSymmetryPlane(SymmetryServices.estimate(face.getMeshModel(), estimator0));
                COMPUTATION_TIME_SW.stop();

                // Computation of average plane
                Plane symmetryPlane = face.getSymmetryPlane();
                planesForAvg.add(symmetryPlane);

                SymmetryStats symmetryStats = new SymmetryStats(face);
                // Write symmetry statistics data to csv
                symmetryStatsWriter.writeSymmetryStats(symmetryStats, subsamplingStrategy,
                        densitySearchValue, densityPruningValue, COMPUTATION_TIME_SW.getTotalTime());
                COMPUTATION_TIME_SW.reset();

            }
            // Write average plane data to csv
            avgPlanesWriter.writeAvgPlaneData(new Plane(planesForAvg), face, subsamplingStrategy, densitySearchValue, densityPruningValue);
        }
    }

    /**
     * Class handling operations with csv files (create, open, write, close)
     *
     * @author Patrik Brosz
     */
    public static class CSVWriterService {
        private final CSVWriter csvWriter;

        /**
         * Constructor
         *
         * @param dirPath  Path to directory
         * @param fileName Name of file
         */
        public CSVWriterService(String dirPath, String fileName) throws IOException {
            Files.createDirectories(Paths.get(dirPath));
            FileWriter outputFile = new FileWriter(Paths.get(dirPath, fileName).toString());
            this.csvWriter = new CSVWriter(outputFile);
        }

        /**
         * Writes headers to csv file
         *
         * @param headers List of strings representing headers of csv
         */
        public void writeHeaders(String[] headers) {
            csvWriter.writeNext(headers);
        }

        /**
         * Writes symmetry stats to csv file
         *
         * @param stats               Symmetry statistics containing distances
         * @param subsamplingStrategy Sub-sampling strategy (e.g. Random)
         * @param densitySearchValue  Search density
         * @param densityPruningValue Pruning density
         * @param calculationTime     Symmetry plane calculation time (ms)
         */
        public void writeSymmetryStats(SymmetryStats stats, PointSamplingConfig.Method subsamplingStrategy,
                                       int densitySearchValue, int densityPruningValue, long calculationTime) {
            if (stats.getHasSymmetry()) {
                csvWriter.writeNext(new String[]{
                        stats.getFace().getShortName(),
                        subsamplingStrategy.name(),
                        String.valueOf(densitySearchValue),
                        String.valueOf(densityPruningValue),
                        String.valueOf(stats.getHausdorffDistance()),
                        String.valueOf(stats.getChamferDistance()),
                        String.valueOf(stats.getMeanDistance()),
                        String.valueOf(calculationTime)
                });
            } else {
                System.out.printf(
                        "No symmetry plane (Face: '%s', symmetry strategy: '%s', SD: %d, PD: %d%n",
                        stats.getFace().getShortName(), subsamplingStrategy.name(), densitySearchValue, densityPruningValue);
            }
        }

        /**
         * Writes average plane data to csv file
         *
         * @param avgPlane            Average plane
         * @param face                Human face (or other type of 3D scan)
         * @param subsamplingStrategy Sub-sampling strategy (e.g. Random)
         * @param densitySearchValue  Search density
         * @param densityPruningValue Pruning density
         */
        public void writeAvgPlaneData(Plane avgPlane, HumanFace face,
                                      PointSamplingConfig.Method subsamplingStrategy,
                                      int densitySearchValue, int densityPruningValue) {
            if (avgPlane != null) {
                Vector3d normalVector = avgPlane.getNormal();
                csvWriter.writeNext(new String[]{
                        face.getShortName(),
                        subsamplingStrategy.name(),
                        String.valueOf(densitySearchValue),
                        String.valueOf(densityPruningValue),
                        String.valueOf(avgPlane.getDistance()),
                        String.valueOf(normalVector.x),
                        String.valueOf(normalVector.y),
                        String.valueOf(normalVector.z)
                });
            } else {
                System.out.printf(
                        "No AVG plane (Face: '%s', symmetry strategy: '%s', SD: %d, PD: %d%n",
                        face.getShortName(), subsamplingStrategy.name(), densitySearchValue, densityPruningValue);
            }
        }

        /**
         * Closes the stream.
         */
        public void close() throws IOException {
            csvWriter.close();
        }
    }

    /**
     * Class computing symmetry stats (Hausdorff distance, Mean distance and Chamfer distance) for given face.
     *
     * @author Patrik Brosz
     */
    private static class SymmetryStats {

        private HumanFace face;
        private HumanFace mirrorFace;
        private boolean hasSymmetry;
        private double hausdorffDistance;
        private double meanDistance;
        private double chamferDistance;

        /**
         * Constructor
         *
         * @param face Human Face (or other type of 3D scan)
         */
        SymmetryStats(HumanFace face) {
            this.face = face;

            createMirrorFace();
            calculateDistances();
        }

        public HumanFace getFace() {
            return face;
        }

        public boolean getHasSymmetry() {
            return hasSymmetry;
        }

        public double getHausdorffDistance() {
            return hausdorffDistance;
        }

        public double getChamferDistance() {
            return chamferDistance;
        }

        public double getMeanDistance() {
            return meanDistance;
        }

        private void setHausdorffDistance(double hausdorffDistance) {
            this.hausdorffDistance = hausdorffDistance;
        }

        private void setMeanDistance(double meanDistance) {
            this.meanDistance = meanDistance;
        }

        private void setChamferDistance(double chamferDistance) {
            this.chamferDistance = chamferDistance;
        }

        /**
         * Creates mirrored face, next used for symmetry precision computation
         */
        private void createMirrorFace() {
            if (!face.hasSymmetryPlane()) {
                this.hasSymmetry = false;
                return;
            }

            this.hasSymmetry = true;

            MeshModel clone = MeshFactory.cloneMeshModel(face.getMeshModel());

            for (MeshFacet facet : clone.getFacets()) { // flip the mesh over the symmetry plane
                facet.getVertices().parallelStream()
                        .forEach(v -> v.getPosition().set(face.getSymmetryPlane().reflectPointOverPlane(v.getPosition())));
            }

            this.mirrorFace = HumanFaceFactory.create(clone, "tmpId", -1L, false);
        }

        /**
         * Calculates Hausdorff, Mean and Chamfer symmetry distance
         */
        private void calculateDistances() {
            setHausdorffDistance(calculateDistance(FaceDistanceServices.DistanceAggregation.HAUSDORFF));
            setMeanDistance(calculateDistance(FaceDistanceServices.DistanceAggregation.MEAN));
            setChamferDistance(calculateDistance(FaceDistanceServices.DistanceAggregation.CHAMFER));
        }

        /**
         * Calculates distance of original and mirrored face
         */
        private double calculateDistance(FaceDistanceServices.DistanceAggregation aggregation) {
            return FaceDistanceServices.calculateBiDistance(
                    this.face,
                    this.mirrorFace,
                    false,
                    true,
                    MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS,
                    null,
                    aggregation);
        }
    }

    /**
     * Loading faces from directory
     *
     * @author Patrik Brosz
     */
    public static class FaceLoader {
        /**
         * Faces are loaded from directory, max number of faces is given by {@code maxScans}
         *
         * @param dataDir  Path to directory with scans
         * @param maxScans Maximum number of scans to be loaded
         */
        public static List<Path> loadFaces(String dataDir, int maxScans) throws IOException {
            return Files.list(Paths.get(dataDir))
                    .filter(f -> f.toString().endsWith(".obj"))
                    .sorted()
                    .limit(maxScans)
                    .collect(Collectors.toList());
        }
    }
}

