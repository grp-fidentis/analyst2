package cz.fidentis.analyst.gui.project.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * History of filtering, holds information of each filtering (which faces were filtered)
 * 
 * @author Matej Kovar
 */
public class FilterHistory {
    
    // List of each filterings
    private List<List<String>> filteredFaces = new ArrayList<>();
    
    // Sum of all filtered faces
    private int totalFaces = 0;
    
    /**
     * Checks if none faces are filtered
     * @return true if none faces are filtered, false otherwise
     */
    public boolean isEmpty() {
        return filteredFaces.isEmpty();
    }
    
    /**
     * Gets amount of faces filtered
     * @return how many faces are currently filtered
     */
    public int getTotalFaces() {
        return totalFaces;
    }
    
    /**
     * Adds newly filtered faces to list
     * @param faces list of filtered faces
     */
    public void addFaces(List<String> faces) {
        if (!faces.isEmpty()) {
            filteredFaces.add(faces);
        }
        totalFaces += faces.size();
    }
    
    /**
     * Removes face which is currently filtered
     * @param faceName name of face
     */
    public void removeFilteredFace(String faceName) {
        filteredFaces.forEach(faces -> {
            faces.removeIf(face -> face.equals(faceName));
        });
        totalFaces -= 1;
    }
    
    /**
     * Reverts last filtering, i.e. removes lastly filtered faces from list
     * @return sum of lastly filtered faces
     */
    public int revertLastFiltering() {
        int sumFaces = filteredFaces.get(filteredFaces.size() - 1).size();
        filteredFaces.remove(filteredFaces.size() - 1);
        totalFaces -= sumFaces;
        return sumFaces;
    }
    
    /**
     * Removes all faces filtered
     * @return amount of filtered faces
     */
    public int clear() {
        filteredFaces.clear();
        int total = totalFaces;
        totalFaces = 0;
        return total;
    }

}
