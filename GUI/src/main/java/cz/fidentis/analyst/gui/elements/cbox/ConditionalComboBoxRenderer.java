package cz.fidentis.analyst.gui.elements.cbox;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

/**
 * A combo box renderer adapted for displaying disabled items in gray color.
 * 
 * @author Radek Oslejsek
 */
public class ConditionalComboBoxRenderer extends BasicComboBoxRenderer implements ListCellRenderer<Object> {
    
    private static final Color DISABLED_COLOR = Color.LIGHT_GRAY;

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        setBorder(new EmptyBorder(5,5,5,5));
        
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        try {
            if (value != null && !((ConditionalItem) value).isEnabled()) {
                setBackground(list.getBackground());
                setForeground(DISABLED_COLOR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            setFont(list.getFont());
            setText((value == null) ? "" : value.toString());
        }
        return this;
    }
}
