package cz.fidentis.analyst.gui.task.symmetry;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.drawables.DrawableCuttingPlane;
import cz.fidentis.analyst.data.face.events.HumanFaceTransformedEvent;
import cz.fidentis.analyst.engines.face.CuttingPlanesUtils;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.gui.elements.PlaneManipulationPanel;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.data.shapes.CrossSection2D;
import cz.fidentis.analyst.project.Task;
import org.json.simple.JSONObject;
import org.openide.filesystems.FileChooserBuilder;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.vecmath.Point2d;
import javax.vecmath.Vector3d;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.DoubleFunction;

import static cz.fidentis.analyst.engines.face.CuttingPlanesUtils.*;

/**
 * Action listener for manipulation with cutting planes.
 *
 * @author Dominik Racek
 * @author Radek Oslejsek
 * @author Samuel Smoleniak
 * @author Peter Conga
 */
public class CuttingPlanesAction extends ControlPanelAction<CuttingPlanesPanel> implements HumanFaceListener {
    
    public static final double FEATURE_POINTS_CLOSENESS = 1.5;
    
    /*
     * Constants for directions of cutting planes.
     */
    public static final Vector3d DIRECTION_VERTICAL = new Vector3d(1, 0, 0);
    public static final Vector3d DIRECTION_HORIZONTAL = new Vector3d(0, 1, 0);
    public static final Vector3d DIRECTION_FRONT_BACK = new Vector3d(0, 0, 1);
    
    private boolean humanFaceChanged = false;
//    private boolean mirrorCuts = false;
    
    /**
     * Constructor.
     * A new {@code ProfilesPanel} is instantiated and added to the {@code topControlPane}
     *
     * @param canvas OpenGL canvas
     * @param faces Faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public CuttingPlanesAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);

        setControlPanel(new CuttingPlanesPanel(this));
        if (getSecondaryDrawableFace() == null) {
            getControlPanel().hideHausdorffDistance();
        }
        FaceStateServices.updateBoundingBox(getPrimaryFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        getControlPanel().getCurveRenderingPanel().setFaceBoundingBox(getPrimaryFace().getBoundingBox());
        
        createDefaultPlanes();
        
        setShowHideCode(() -> { // SHOW
                    getCanvas().getScene().setDefaultColors();
                    if (humanFaceChanged) {
                        getScene().clearCuttingPlanes();
                        createDefaultPlanes();
                        getControlPanel().getCurveRenderingPanel().setFaceBoundingBox(getPrimaryFace().getBoundingBox());
                        computeCrossSections(getCuttingPlanes(getControlPanel().getCurveRenderingPanel().getCurrentCuttingPlane()));
                        computeHausdorffDistances(getControlPanel().getCurveRenderingPanel().get2DPrimaryCurves(),
                        getControlPanel().getCurveRenderingPanel().get2DSecondaryCurves());
                        humanFaceChanged = false;
                    }
                    showCheckedPlanes();
                },
                () -> { // HIDE
                    hideAllPlanes();
                }
        );
        
        // Be informed about changes in faces perfomed by other GUI elements
        getPrimaryDrawableFace().getHumanFace().registerListener(this);
        if (getSecondaryDrawableFace() != null) {
            getSecondaryDrawableFace().getHumanFace().registerListener(this);
        }
        
        hideAllPlanes();
        setDefaultPlane();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        
        switch (action) {
            case CuttingPlanesPanel.ACTION_COMMAND_EXPORT:
                exportConfiguration();
                exportCurve(getControlPanel().getCurveRenderingPanel().get2DPrimaryCurves().getFirst(),
                        "Export primary face cross-section curve (CSV)");
                if (getSecondaryDrawableFace() != null) {
                    exportCurve(getControlPanel().getCurveRenderingPanel().get2DSecondaryCurves().getFirst(),
                            "Export secondary face cross-section curve (CSV)");
                }
                break;
                
            case CuttingPlanesPanel.ACTION_VIEW_VERTICAL:
                getControlPanel().getCurveRenderingPanel().setCurrentCuttingPlane(DIRECTION_VERTICAL);
                computeCrossSections(getCuttingPlanes(DIRECTION_VERTICAL));
                break;
                
            case CuttingPlanesPanel.ACTION_VIEW_HORIZONTAL:
                getControlPanel().getCurveRenderingPanel().setCurrentCuttingPlane(DIRECTION_HORIZONTAL);
                computeCrossSections(getCuttingPlanes(DIRECTION_HORIZONTAL));
                break;
                
            case CuttingPlanesPanel.ACTION_VIEW_FRONT:
                getControlPanel().getCurveRenderingPanel().setCurrentCuttingPlane(DIRECTION_FRONT_BACK);
                computeCrossSections(getCuttingPlanes(DIRECTION_FRONT_BACK));
                break;
                
            case CuttingPlanesPanel.ACTION_COMPUTE_HAUSDORFF:
                if (getSecondaryDrawableFace() == null) {
                    return;
                }
                computeHausdorffDistances(getControlPanel().getCurveRenderingPanel().get2DPrimaryCurves(),
                        getControlPanel().getCurveRenderingPanel().get2DSecondaryCurves());
                break;
            case CuttingPlanesPanel.ACTION_INITIALIZE_MANUPULATION_PANEL:
                createPlane();
                break;
            default:
                // do nothing
        }
        renderScene();
    }
    
    @Override
    public void acceptEvent(HumanFaceEvent event) {
        // If some human face is transformed, current cutting planes are deleted
        // and new default planes are created after focusing Cutting Planes Panel.
        
        if ((event instanceof HumanFaceTransformedEvent) && (((HumanFaceTransformedEvent) event).isFinished())) {
            humanFaceChanged = true;
        }
    }
    
    /**
     * Creates and returns a file of given type.
     * @param title title of file chooser window
     * @param extension file type
     * @return created file or null if not successful.
     */
    private File createFileType(String title, String extension) {
        File file = new FileChooserBuilder(CuttingPlanesAction.class)
                .setTitle(title)
                .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                .setFilesOnly(true)
                .setFileFilter(new FileNameExtensionFilter(extension + " files",extension))
                .setAcceptAllFileFilterUsed(true)
                .showSaveDialog();

        if (file == null) {
            return null;
        }

        // If chosen file exists, use the exact file path
        // If chosen file does not exist and does not have an extension, add it
        if (!file.exists()) {
            if (!file.getAbsolutePath().endsWith("." + extension)) {
                file = new File(file.getAbsolutePath() + "." + extension);
            }
        }
        
        return file;
    }
    
    /**
     * Exports current configuration of planes from {@code CuttingPlanesPanel}
     * to .json file.
     */
    private void exportConfiguration() {
        File file = createFileType("Export configuration (JSON)", "json");
        if (file == null) { // canceled
            return;
        }
        
        CuttingPlanesPanel controlPanel = getControlPanel();

        List<CuttingPlaneConfig> cuttingPlaneConfigs = controlPanel.getPlaneManipulationPanels().stream()
                .map(p -> new CuttingPlaneConfig(p.getCurrentPlaneType(), p.getPlaneOrientation(), p.isVisible(),
                        p.getSlider().getValue()))
                .toList();

        JSONObject configuration = new JSONObject(getConfigExport(controlPanel.getSubsamplingStrength(),
                controlPanel.getCurveRenderingPanel().getCurrentCuttingPlane(), cuttingPlaneConfigs));

        try {
            file.createNewFile();
            try (PrintWriter writer = new PrintWriter(file.getAbsoluteFile(), StandardCharsets.UTF_8)) {
                writer.print(configuration.toJSONString());
            }
        } catch (IOException ex) {
            System.out.println("ERROR writing to a file: " + ex);
        }
    }

    /**
     * Exports a curve to a new .csv file.
     * @param curve 2D curve
     * @param title title for file-choosing window
     */
    private void exportCurve(CrossSection2D curve, String title) {
        File file = createFileType(title, "csv");

        try {
            file.createNewFile();
            try (PrintWriter writer = new PrintWriter(file.getAbsoluteFile(), StandardCharsets.UTF_8)) {
                List<Point2d> points = curve.getCurveSegments().stream()
                        .flatMap(Collection::stream)
                        .toList();
                writeCurveExport(points, writer);
            }
        } catch (IOException|NullPointerException ex) {
            System.out.println("ERROR writing to a file: " + ex);
        }
    }
    
    private void createPlane() {
        List<PlaneManipulationPanel> planeManipulationPanels = getControlPanel().getPlaneManipulationPanels();
        planeManipulationPanels.getLast().setCanvasAndFace(getCanvas(), getPrimaryFace());
    }

    /**
     * Initializes all cutting plane panels in the scene with default planes 
     * computed from bbox. Selects default plane.
     */
    private void createDefaultPlanes() {
        List<PlaneManipulationPanel> planeManipulationPanels = getControlPanel().getPlaneManipulationPanels();
        for (int i = 0; i < planeManipulationPanels.size() / 3; ++i) {
            planeManipulationPanels.get(i).setCanvasAndFace(getCanvas(), getPrimaryFace());
            planeManipulationPanels.get(i).createBboxCuttingPlane(DIRECTION_VERTICAL);
            
            planeManipulationPanels.get(i + 1).setCanvasAndFace(getCanvas(), getPrimaryFace());
            planeManipulationPanels.get(i + 1).createBboxCuttingPlane(DIRECTION_HORIZONTAL);
            
            planeManipulationPanels.get(i + 2).setCanvasAndFace(getCanvas(), getPrimaryFace());
            planeManipulationPanels.get(i + 2).createBboxCuttingPlane(DIRECTION_FRONT_BACK);
        }
    }
    
    /**
     * Sets first vertical plane as default. Selects its checkbox (hausdorff and 
     * cross-section is automatically computed), hides the plane because profiles
     * panel is not focused.
     */
    private void setDefaultPlane() {
        PlaneManipulationPanel defaultPlanePanel = getControlPanel().getPlaneManipulationPanels().getFirst();
        defaultPlanePanel.selectCheckBox();
        defaultPlanePanel.getCurrentPlane().show(false);
    }

    /**
     * Computes cross-sections with all given visible planes, stores them in {@code CurveRenderingPanel}.
     * @param cuttingPlanes list of cutting planes to compute intersections with
     */
    private void computeCrossSections(List<DrawableCuttingPlane> cuttingPlanes) {
        List<CrossSectionPlane> crossSectionPlanes = cuttingPlanes.stream()
                .map(c -> new CrossSectionPlane(
                        c.isShown(),
                        new Vector3d(c.getNonShiftedPlane().getNormal()),
                        c.getPlane()))
                .toList();
        List<List<CrossSection2D>> curvesOfFaces = CuttingPlanesUtils.computeCrossSections(
                crossSectionPlanes, getPrimaryFace(), getSecondaryFace(), FEATURE_POINTS_CLOSENESS);

        getControlPanel()
                .getCurveRenderingPanel()
                .setCurves(curvesOfFaces.get(0), curvesOfFaces.size() > 1 ? curvesOfFaces.get(1) : new ArrayList<>());
    }
    
    /**
     * Computes Hausdorff distances of all visible curves. 
     * @param primaryCurves cross-sections with primary face
     * @param secondaryCurves cross-sections with secondary face
     */
    protected void computeHausdorffDistances(List<CrossSection2D> primaryCurves, List<CrossSection2D> secondaryCurves) {
        if (secondaryCurves.isEmpty()) {
            return;
        }

        List<Double> hausdorffDistances = getHausdorffDistances(getControlPanel().getSubsamplingStrength(),
                primaryCurves, secondaryCurves);

        String hidden = "not visible";
        DoubleFunction<String> toRoundedValue = value -> String.format("%,.3f", value);
        String distance1 = !hausdorffDistances.isEmpty() && hausdorffDistances.get(0) != null ? toRoundedValue.apply(hausdorffDistances.get(0)) : hidden;
        String distance2 = hausdorffDistances.size() > 1 && hausdorffDistances.get(1) != null ? toRoundedValue.apply(hausdorffDistances.get(1)) : hidden;
        String distance3 = hausdorffDistances.size() > 2 && hausdorffDistances.get(2) != null ? toRoundedValue.apply(hausdorffDistances.get(2)) : hidden;

        getControlPanel().setHausdorffDistances(distance1, distance2, distance3);
    }

    /**
     * Returns a list of cutting planes with given normal vector.
     * @param direction normal vector indicating direction of cutting plane
     * @return list of cutting planes in given direction
     */
    protected List<DrawableCuttingPlane> getCuttingPlanes(Vector3d direction) {
        List<DrawableCuttingPlane> currentCuttingPlanes = new ArrayList<>();
        List<PlaneManipulationPanel> planeManipulationPanels = getControlPanel().getPlaneManipulationPanels();
        for (PlaneManipulationPanel panel : planeManipulationPanels) {
            if (direction == null || direction.equals(panel.getPlaneOrientation())) {
                currentCuttingPlanes.add(panel.getCurrentPlane());
            }
        }
        return currentCuttingPlanes;
    }
    
    /**
     * Shows cutting planes marked as visible for user.
     */
    protected void showCheckedPlanes() {
        getControlPanel().getPlaneManipulationPanels().stream()
                .filter(PlaneManipulationPanel::isCheckBoxSelected)
                .map(PlaneManipulationPanel::getCurrentPlane)
                .forEach(plane -> plane.show(true));
    }
    
    protected void hideAllPlanes() {
        getControlPanel().getPlaneManipulationPanels().stream()
                .map(PlaneManipulationPanel::getCurrentPlane)
                .forEach(plane -> plane.show(false));
    }

}
