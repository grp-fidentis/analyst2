package cz.fidentis.analyst.gui.elements;

import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.engines.face.CuttingPlanesUtils;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.CrossSection2D;

import javax.swing.*;
import javax.vecmath.Point2d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;


/**
 * Panel for drawing 2D cutting curves.
 *
 * @author Dominik Racek
 * @author Radek Oslejsek
 * @author Samuel Smoleniak
 * @author Peter Conga
 */
public class CurveRenderingPanel extends JPanel {

    private static final int SEGMENT_POINT_SIZE = 4;
    private static final int PREF_W = 400;
    private static final int PREF_H = 400;
    private static final Color PRIMARY_COLOR = DrawableFace.SKIN_COLOR_PRIMARY;
    private static final Color SECONDARY_COLOR = DrawableFace.SKIN_COLOR_SECONDARY.darker();
    private static final Stroke GRAPH_STROKE = new BasicStroke(1f);
    private static final Stroke HIGHLIGHT_STROKE = new BasicStroke(2f);
    
    private int normalVectorLength = 10;
    private int sampling = 100;
    private int highlightIndex = -1;

    public int getHighlightIndex() {
        return highlightIndex;
    }

    public void setHighlightIndex(int highlightIndex) {
        this.highlightIndex = highlightIndex;
    }

    public int getSampling() {
        return sampling;
    }

    /**
     * Sets sampling for rendering the cross-cut curve
     * @param sampling % of points to keep on the curve
     */
    public void setSampling(int sampling) {
        if (sampling < 1) {
            this.sampling = 1;
        } else if (sampling > 100) {
            this.sampling = 100;
        } else {
            this.sampling = sampling;
        }
    }

    public int getNormalVectorLength() {
        return normalVectorLength;
    }

    public void setNormalVectorLength(int normalVectorLength) {
        this.normalVectorLength = normalVectorLength;
    }
    
    private boolean showNormals = false;
    
    private List<CrossSection2D> primaryCrossSectionCurves;
    private List<CrossSection2D> secondaryCrossSectionCurves;

    private Vector3d currentCuttingPlaneNormal;
    protected Box faceBoundingBox;
    
//    private boolean mirrorCuts = false;
    
    /*
     * Parameters for adjusting curve drawing in panel.
     */
    private double scale = Double.POSITIVE_INFINITY;
    private double offsetX;
    private double offsetY;
    
    /**
     * Constructor.
     */
    public CurveRenderingPanel() {
        setPreferredSize(new Dimension(PREF_W, PREF_H));
    }

    /**
     * Sets computed cross-section curves for rendering.
     * @param primaryCrossSectionCurves cross-sections with primary face
     * @param secondaryCrossSectionCurves cross-sections with secondary face 
     */
    public void setCurves(List<CrossSection2D> primaryCrossSectionCurves, List<CrossSection2D> secondaryCrossSectionCurves) {
        this.primaryCrossSectionCurves = primaryCrossSectionCurves;
        this.secondaryCrossSectionCurves = secondaryCrossSectionCurves;
        repaint();
    }
    
    /**
     * Getter for primary curves.
     * @return list of visible 2D curves for primary face
     */
    public List<CrossSection2D> get2DPrimaryCurves() {
        return primaryCrossSectionCurves;
    }
    
    /**
     * Getter for secondary curves.
     * @return list of visible 2D curves for secondary face
     */
    public List<CrossSection2D> get2DSecondaryCurves() {
        return secondaryCrossSectionCurves;
    } 
    
    /**
     * Sets current cutting plane and recomputes scale and offset fields
     * for proper display of CrossSectionCurve.
     * @param currentPlaneNormal normal vector of current cutting plane
     */
    public void setCurrentCuttingPlane(Vector3d currentPlaneNormal) {
        this.currentCuttingPlaneNormal = currentPlaneNormal;

        if (faceBoundingBox != null && currentCuttingPlaneNormal != null) {
            CuttingPlanesUtils.BoundingBox2D box2D = CuttingPlanesUtils.getBoundingBox2D(PREF_W, PREF_H, faceBoundingBox,
                    currentCuttingPlaneNormal);
            scale = box2D.scale();
            offsetX = box2D.offsetX();
            offsetY = box2D.offsetY();
        }
    }
    
    public Vector3d getCurrentCuttingPlane() {
        return currentCuttingPlaneNormal;
    }

    /**
     * Sets bounding box used for computing scale and offset when
     * drawing CrossSectionCurve.
     * @param boundingBox bounding box of currently analyzed face
     */
    public void setFaceBoundingBox(Box boundingBox) {
        this.faceBoundingBox = boundingBox;
    }

    public void setShowNormals(boolean showNormals) {
        this.showNormals = showNormals;
    }
    
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(PREF_W, PREF_H);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        if (primaryCrossSectionCurves == null) {
            return;
        }
        
        for (CrossSection2D curve : primaryCrossSectionCurves) {
            paintCurve(g2, curve, PRIMARY_COLOR, primaryCrossSectionCurves.indexOf(curve) == highlightIndex);
        }
        for (CrossSection2D curve : secondaryCrossSectionCurves) {
            paintCurve(g2, curve, SECONDARY_COLOR, secondaryCrossSectionCurves.indexOf(curve) == highlightIndex);
        }
    }

    protected void paintCurve(Graphics2D g2, CrossSection2D curve, Color faceColor, boolean highlight) {
        if (curve == null) {
            return;
        }
        
        List<Color> asd = new ArrayList<>(List.of(Color.GREEN, Color.RED, Color.BLUE, Color.BLACK, Color.MAGENTA, Color.ORANGE));
        
        CrossSection2D subsampledCurve = curve.subSample(sampling);
        for (List<Point2d> segment : subsampledCurve.getCurveSegments()) {
            Point2d a = null;
            for (Point2d point : segment) {
                g2.setColor(faceColor);
                g2.setStroke(highlight ? HIGHLIGHT_STROKE : GRAPH_STROKE);

                if (a == null) {
                    a = point;
                    continue;
                }
                // If new curve segment, find the closes point on segment[0]

                Point2d b = point;

                g2.draw(new Line2D.Double(
                        a.x * scale + offsetX,
                        a.y * scale + offsetY,
                        b.x * scale + offsetX,
                        b.y * scale + offsetY));
                a = b;
            }
        }
        
        if (showNormals) {

            List<Line2D> normalVectors = curve.getNormals();

            g2.setColor(Color.RED);
            for (Line2D line : normalVectors) {
                double x1 = line.getX1() * scale + offsetX;
                double y1 = line.getY1() * scale + offsetY;
                // This calculation is required to keep the normal vector sizes
                double x2 = (line.getX1() + (line.getX2() * normalVectorLength)) * scale + offsetX;
                double y2 = (line.getY1() + (line.getY2() * normalVectorLength)) * scale + offsetY;
                g2.draw(new Line2D.Double(x1, y1, x2, y2));
            }
        }
        
        drawProjectedFeaturePoints(g2, curve, faceColor);
    }

    protected void paintCurveSegment(Graphics2D g2, List<Point2d> curveSegment, Color faceColor) {
        g2.setColor(faceColor);
        g2.setStroke(GRAPH_STROKE);
        
        // Determine which way the curve goes (top -> bottom || bottom -> top)
        boolean clockwiseNormal = curveSegment.get(0).y > curveSegment.get(curveSegment.size() - 1).y;
        
        
        for (int i = 0; i < curveSegment.size() - 1; i++) {
            
            Point2d a = curveSegment.get(i);
            Point2d b = curveSegment.get(i + 1);
            
            g2.draw(new Line2D.Double(
                    a.x * scale + offsetX,
                    a.y * scale + offsetY,
                    b.x * scale + offsetX,
                    b.y * scale + offsetY));
            
            if (showNormals && i % 10 == 0) {
                
                // New, already adjusted to scale, A and B points
                Point2d a1 = new Point2d(a.x * scale + offsetX, a.y * scale + offsetY);
                Point2d b1 = new Point2d(b.x * scale + offsetX, b.y * scale + offsetY);
                // Point in the middle of our adjusted points
                Point2d middle = new Point2d(((a1.x + b1.x) / 2), ((a1.y + b1.y) / 2));
                // Calculate normal vector and rotate to the right direction
                Point2d normal;
                if (clockwiseNormal) {
                    normal = new Point2d((b1.y - a1.y) * (-1), b1.x - a1.x);
                } else {
                    normal = new Point2d(b1.y - a1.y, (b1.x - a1.x) * (-1));
                }
                
                // Calculate the length of the normal vector
                double length = Math.sqrt(Math.pow(normal.x, 2) + Math.pow(normal.y, 2));
                // DNormalize vector
                normal.set(normal.x / length, normal.y / length);
                // Multiply it by our desired size
                normal.set(normal.x * normalVectorLength, normal.y * normalVectorLength);
                
                // Draw the line
                g2.setColor(Color.RED);
                g2.draw(new Line2D.Double(
                        middle.x, 
                        middle.y,
                        middle.x + normal.x,
                        middle.y + normal.y));
                g2.setColor(faceColor);
            }
        }
    }
    
    protected void drawProjectedFeaturePoints(Graphics2D g2, CrossSection2D curve, Color faceColor) {
        if (curve.getFeaturePoints() == null) {
            return;
        }
        
        g2.setColor(faceColor);
        for (Point2d point : curve.getFeaturePoints()) {
            Ellipse2D.Double featurePoint = new Ellipse2D.Double(
                    (point.x * scale + offsetX) - SEGMENT_POINT_SIZE / 2d,
                    (point.y * scale + offsetY) - SEGMENT_POINT_SIZE / 2d,
                    SEGMENT_POINT_SIZE,
                    SEGMENT_POINT_SIZE);
            g2.fill(featurePoint);
            g2.draw(featurePoint);
        }
    }
}
