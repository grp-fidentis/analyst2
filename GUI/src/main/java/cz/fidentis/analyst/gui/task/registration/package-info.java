/**
 * Control panel for the registration (superimposition) of human faces.
 */
package cz.fidentis.analyst.gui.task.registration;
