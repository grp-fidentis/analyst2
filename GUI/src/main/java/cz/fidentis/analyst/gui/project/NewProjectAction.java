package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.project.Project;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import cz.fidentis.analyst.project.ProjectService;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.WindowManager;

/**
 * A "new project" button in the application toolbox
 * 
 * @author Radek Oslejsek
 */
@ActionID(
        category = "File",
        id = "cz.fidentis.analyst.project.toolbox.NewProjectAction"
)
@ActionRegistration(
        iconBase = "newProject.png",
        displayName = "#CTL_NewProjectAction"
)
@ActionReferences({
    //@ActionReference(path = "Menu/File", position = 1300, separatorBefore = 1150, separatorAfter = 1250),
    @ActionReference(path = "Menu/File", position = 1220),
    @ActionReference(path = "Toolbars/File", position = 220)
})
@Messages("CTL_NewProjectAction=New Project...")
public final class NewProjectAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        ProjectWindow win = (ProjectWindow) WindowManager.getDefault().findTopComponent("ProjectWindow");
        if (win == null) {
            return;
        }
        
        Project project = ProjectService.INSTANCE.getOpenedProject();
        
        if (project.changed()) { 
            Object[] options = {"Save", "Discard changes", "Cancel"};
            int answer = JOptionPane.showOptionDialog(
                    win,
                    "<html><b>Save current project?</b></html>",
                    "Current project modified",
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE,
                    null, options, options[0]);
            
            if (answer == 2 || answer == -1) { // cancel
                return;
            } else if (answer == 0) { // save
                // trigger saving dialogue:
                new SaveProjectAction().actionPerformed(new ActionEvent( 
                        this,
                        ActionEvent.ACTION_PERFORMED,
                        ""));
            
                if (project.changed()) { // saving canceled
                    return; // skip new project opening
                }
            }
        }
        
        win.getProjectPanel().createNewProject();
        WindowManager.getDefault().getMainWindow().setTitle("FIDENTIS Analyst 2: " 
                + project.getName());
    }
}
