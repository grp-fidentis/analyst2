package cz.fidentis.analyst.gui.elements;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.util.stream.Collectors;

/**
 * A common component for showing information about a face.
 * 
 * @author Matej Kovar
 * @author Radek Oslejsek
 */
public class FaceInfoPanel extends JPanel {
    
    /**
     * A big head contour used when no real photo is available
     */
    public static final String ANONYMOUS_PHOTO = "face160x160.png";
    
    public static final String HELP_URL = "https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/Project-Management";

    private ImageIcon previewFace = null;
    
    /**
     * Constructor.
     */
    public FaceInfoPanel() {
        initComponents();
        infoLinkButton1.setUri(URI.create(HELP_URL));
    }
    
    /**
     * Show face info
     * @param reference faceReference of the face to be shown
     */
    public void showFaceState(FaceReference reference) {
        setPhoto(reference.getPreviewFile());

        faceFolderOutput.setText(reference.getFaceFile().getParent());
        
        showPath(reference.getFaceFile(), pathOutput);
        showPath(reference.getPreviewFile(), previewOutput);
        showPath(reference.getFeaturePointsFile(), textureOutput);
        showPath(reference.getFeaturePointsFile(), featurePointsOutput);
        
        setMeshFileSize(reference.getFaceFile());
        
        if (FaceService.INSTANCE.isInMemory(reference)) {
            HumanFace humanFace = FaceService.INSTANCE.getFaceByReference(reference);
            verticesOutput.setEnabled(true);
            verticesOutput.setText("" + humanFace.getMeshModel().getNumVertices());
            facetsOutput.setEnabled(true);
            facetsOutput.setText("" + humanFace.getMeshModel().getFacets().size());
            showFeaturePoints(humanFace);
        } else {
            verticesOutput.setEnabled(false);
            verticesOutput.setText("N/A");
            facetsOutput.setEnabled(false);
            facetsOutput.setText("N/A");
            showFeaturePoints(null);
        }
    }

    private boolean setPhoto(File photoFile) {
        if (photoFile == null || !photoFile.exists()) {
            photo.setToolTipText("");
            photo.setIcon(new ImageIcon(FaceInfoPanel.class.getClassLoader().getResource("/" + ANONYMOUS_PHOTO)));
            previewFace = null;
            return false;
        }
        
        //previewFace = null;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
  
        try {
            BufferedImage image = ImageIO.read(photoFile);
            BufferedImage zoomedImage = Scalr.resize(image, (int) screenSize.getWidth() / 2, (int) screenSize.getHeight() / 2);
            BufferedImage smallImage = Scalr.resize(image, 240, 160);

            previewFace = new ImageIcon(zoomedImage);
            photo.setToolTipText("Click to enlarge the image");
            photo.setIcon(new ImageIcon(smallImage));
        } catch (IOException ex) {
            return false;
        }

        return true;
    }
    
    private void showPath(File file, JLabel pathOutput) {
        if (file != null) {
            String path = file.getAbsolutePath();
            String fileName = path.substring(path.lastIndexOf(File.separator)+1);
            pathOutput.setEnabled(true);
            pathOutput.setText(fileName);
        } else {
            pathOutput.setEnabled(false);
            pathOutput.setText("N/A");
        }
    }
    
    private void setMeshFileSize(File meshPath) {
        if (meshPath != null) {
            try {
                long bytes = Files.size(meshPath.toPath());
                sizeOutput.setEnabled(true);
                sizeOutput.setText(String.format("%,d kB", bytes / 1024));
            } catch (IOException ex) {
                sizeOutput.setText("ERROR");
            }
        } else {
            sizeOutput.setEnabled(false);
            sizeOutput.setText("N/A");
        }
    }
    
    private void showFeaturePoints(HumanFace face) {
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();
        if (face != null && !face.getAllLandmarks().isEmpty()) {
            jComboBox1.setEnabled(true);
            jComboBox1.setVisible(true);
            model.addAll(face.getAllLandmarks().stream()
                    .map(Landmark::getName)
                    .collect(Collectors.toList())
            );
        } else {
            jComboBox1.setEnabled(false);
            jComboBox1.setVisible(false);
        }
        jComboBox1.setModel(model);
    }
       
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        geometryPanel = new javax.swing.JPanel();
        verticesLabel = new javax.swing.JLabel();
        verticesOutput = new javax.swing.JLabel();
        facetsLabel = new javax.swing.JLabel();
        facetsOutput = new javax.swing.JLabel();
        fpLabel = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        photoPanel = new javax.swing.JPanel();
        photo = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        filePanel = new javax.swing.JPanel();
        pathLabel = new javax.swing.JLabel();
        pathOutput = new javax.swing.JLabel();
        sizeLabel = new javax.swing.JLabel();
        sizeOutput = new javax.swing.JLabel();
        featurePointsLabel = new javax.swing.JLabel();
        featurePointsOutput = new javax.swing.JLabel();
        previewLabel = new javax.swing.JLabel();
        previewOutput = new javax.swing.JLabel();
        textureLabel = new javax.swing.JLabel();
        textureOutput = new javax.swing.JLabel();
        faceFolder = new javax.swing.JLabel();
        faceFolderOutput = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        infoLinkButton1 = new cz.fidentis.analyst.gui.elements.InfoLinkButton();

        geometryPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.geometryPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(verticesLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.verticesLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(verticesOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.verticesOutput.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(facetsLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.facetsLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(facetsOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.facetsOutput.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(fpLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.fpLabel.text")); // NOI18N

        javax.swing.GroupLayout geometryPanelLayout = new javax.swing.GroupLayout(geometryPanel);
        geometryPanel.setLayout(geometryPanelLayout);
        geometryPanelLayout.setHorizontalGroup(
            geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(geometryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(facetsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                        .addComponent(fpLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(verticesLabel))
                .addGap(9, 9, 9)
                .addGroup(geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(verticesOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(facetsOutput, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        geometryPanelLayout.setVerticalGroup(
            geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(geometryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(verticesLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(verticesOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(facetsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(facetsOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(geometryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fpLabel)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        photoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.photoPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        photo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/face160x160.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(photo, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.photo.text")); // NOI18N
        photo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                photoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                photoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                photoMouseExited(evt);
            }
        });

        javax.swing.GroupLayout photoPanelLayout = new javax.swing.GroupLayout(photoPanel);
        photoPanel.setLayout(photoPanelLayout);
        photoPanelLayout.setHorizontalGroup(
            photoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(photoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(photo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        photoPanelLayout.setVerticalGroup(
            photoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(photo)
        );

        filePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.filePanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N
        filePanel.setPreferredSize(new java.awt.Dimension(570, 180));

        pathLabel.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(pathLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.pathLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(pathOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.pathOutput.text")); // NOI18N

        sizeLabel.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(sizeLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.sizeLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(sizeOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.sizeOutput.text")); // NOI18N

        featurePointsLabel.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(featurePointsLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.featurePointsLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(featurePointsOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.featurePointsOutput.text")); // NOI18N

        previewLabel.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(previewLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.previewLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(previewOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.previewOutput.text")); // NOI18N

        textureLabel.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(textureLabel, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.textureLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(textureOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.textureOutput.text")); // NOI18N

        faceFolder.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(faceFolder, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.faceFolder.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(faceFolderOutput, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.faceFolderOutput.text")); // NOI18N

        javax.swing.GroupLayout filePanelLayout = new javax.swing.GroupLayout(filePanel);
        filePanel.setLayout(filePanelLayout);
        filePanelLayout.setHorizontalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(faceFolder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pathLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(featurePointsLabel)
                    .addComponent(textureLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(previewLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sizeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(previewOutput, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                    .addComponent(featurePointsOutput, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(faceFolderOutput, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pathOutput, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sizeOutput, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(textureOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        filePanelLayout.setVerticalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filePanelLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(faceFolder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(faceFolderOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pathLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pathOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(sizeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sizeOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(featurePointsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(featurePointsOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(previewLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(previewOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(textureLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(textureOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jScrollPane3.setViewportView(filePanel);
        filePanel.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.filePanel.AccessibleContext.accessibleName")); // NOI18N

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.jPanel4.border.title"))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(infoLinkButton1, org.openide.util.NbBundle.getMessage(FaceInfoPanel.class, "FaceInfoPanel.infoLinkButton1.text")); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(infoLinkButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(infoLinkButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(geometryPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 575, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(photoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(photoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(geometryPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    
    private void photoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseClicked
        if (previewFace != null) {
            JOptionPane.showMessageDialog(this.getParent(), previewFace, "Preview Image", JOptionPane.PLAIN_MESSAGE);
        }
    }//GEN-LAST:event_photoMouseClicked

    private void photoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseEntered
        if (previewFace != null) {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
        }
    }//GEN-LAST:event_photoMouseEntered

    private void photoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseExited
        if (previewFace != null) {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_photoMouseExited
            
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel faceFolder;
    private javax.swing.JLabel faceFolderOutput;
    private javax.swing.JLabel facetsLabel;
    private javax.swing.JLabel facetsOutput;
    private javax.swing.JLabel featurePointsLabel;
    private javax.swing.JLabel featurePointsOutput;
    private javax.swing.JPanel filePanel;
    private javax.swing.JLabel fpLabel;
    private javax.swing.JPanel geometryPanel;
    private cz.fidentis.analyst.gui.elements.InfoLinkButton infoLinkButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel pathLabel;
    private javax.swing.JLabel pathOutput;
    private javax.swing.JLabel photo;
    private javax.swing.JPanel photoPanel;
    private javax.swing.JLabel previewLabel;
    private javax.swing.JLabel previewOutput;
    private javax.swing.JLabel sizeLabel;
    private javax.swing.JLabel sizeOutput;
    private javax.swing.JLabel textureLabel;
    private javax.swing.JLabel textureOutput;
    private javax.swing.JLabel verticesLabel;
    private javax.swing.JLabel verticesOutput;
    // End of variables declaration//GEN-END:variables

}
