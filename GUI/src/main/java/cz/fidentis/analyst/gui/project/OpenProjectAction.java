package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.project.Project;
import cz.fidentis.analyst.project.ProjectService;
import cz.fidentis.analyst.project.impl.ProjectImpl;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.WindowManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Opens an existing project.
 * 
 * @author Radek Oslejsek
 */
@ActionID(
        category = "File",
        id = "cz.fidentis.analyst.project.toolbox.OpenProjectAction"
)
@ActionRegistration(
        iconBase = "openProject.png",
        displayName = "#CTL_OpenProjectAction"
)
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 1200),
    @ActionReference(path = "Toolbars/File", position = 200)
})
@Messages("CTL_OpenProjectAction=Open Project...")
public final class OpenProjectAction implements ActionListener {
    
    @Override
    public void actionPerformed(ActionEvent e) {
        ProjectWindow win = (ProjectWindow) WindowManager.getDefault().findTopComponent("ProjectWindow");
        if (win == null) {
            return;
        }
        
        Project project = ProjectService.INSTANCE.getOpenedProject();

        File dir = selectDirectory(win);
        if (dir == null) { // canceled
            return;
        }
        
        if (project.changed()) { 
            Object[] options = {"Save", "Discard changes", "Cancel"};
            int answer = JOptionPane.showOptionDialog(
                    win,
                    "<html><b>Save current project?</b></html>",
                    "Current project modified",
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE,
                    null, options, options[0]);

            if (answer == 2 || answer == -1) { // cancel
                return;
            } else if (answer == 0) { // save
                // trigger saving dialogue:
                new SaveProjectAction().actionPerformed(new ActionEvent( 
                        this,
                        ActionEvent.ACTION_PERFORMED,
                        ""));
            
                if (project.changed()) { // saving canceled
                    return; // skip project opening
                }
            }
        }
        
        win.getProjectPanel().openProject(dir);
        WindowManager.getDefault().getMainWindow().setTitle("FIDENTIS Analyst II: "
                + project.getName());
        win.requestActive();
    }

    private static File selectDirectory(ProjectWindow win) {
        File dir = new FileChooserBuilder(ProjectImpl.class)
                .setTitle("Select project's directory")
                .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                .setDirectoriesOnly(true)
                .setFileHiding(false)
                //.setFileFilter(new FileNameExtensionFilter("json files (*.json)", "json"))
                .showOpenDialog();

        if (dir!= null && !dir.exists() && dir.getParentFile().exists()) {
            dir = dir.getParentFile();
        }

        if (dir == null) { // canceled
            return dir;
        } else if (!ProjectService.INSTANCE.getProjectFile(dir).exists()) { // not a FIDENTIS project file
            JOptionPane.showMessageDialog(win, "Not a FIDENTIS project directory", "Project openign failed", JOptionPane.ERROR_MESSAGE);
            return null;
        } else {
            return dir;
        }
    }
}
