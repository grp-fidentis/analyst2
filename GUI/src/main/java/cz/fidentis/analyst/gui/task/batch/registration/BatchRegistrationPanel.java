package cz.fidentis.analyst.gui.task.batch.registration;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices;
import cz.fidentis.analyst.gui.elements.InfoLinkButton;
import cz.fidentis.analyst.gui.elements.SpinSlider;
import cz.fidentis.analyst.gui.project.ProjectWindow;
import cz.fidentis.analyst.gui.task.ControlPanel;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.gui.task.batch.SelectBoxService;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Objects;

import static cz.fidentis.analyst.gui.task.registration.RegistrationPanel.getStaticIcon;

/**
 * A control panel for bath (N:N) processing.
 * It uses its own {@code HumanFaceFactory} to handle processed faces.
 *
 * @author Radek Oslejsek
 */
public class BatchRegistrationPanel extends ControlPanel {

    public static final String AVG_FACE_NEAREST_NEIGHBOURS = "Nearest neighbours";
    public static final String AVG_FACE_RAY_CASTING = "Ray casting [experimental]";
    public static final String AVG_FACE_RAY_CASTING_GPU = "Ray casting GPU [experimental]";
    public static final String AVG_FACE_NONE = "Skip (use selected face for registration)";
    
    public static final String REGISTRATION_NONE = "Skip registration";
    public static final String REGISTRATION_GPA = "feature points (Procrustes)";
    public static final String REGISTRATION_ICP = "mesh-based (ICP)";
    
    /*
     * Actions triggered by this panel and served by the associated action listener 
     */
    public static final String ACTION_COMMAND_AVERAGE_AND_REGISTER = "Compute average face and/or register";
    public static final String ACTION_COMMAND_SHOW_SELECTED_FACE = "Show selected face";
    public static final String ACTION_COMMAND_EXPORT_AVG_FACE = "Export average face";

    public static final String HELP_URL = "https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/batch-registration";

    /*
     * Mandatory design elements
     */
    public static final String ICON = "registration28x28.png";
    public static final String NAME = "Registration";

    private final transient Task task;

    /**
     * Constructor.
     * @param action Action listener
     * @param task Shared faces across multiple batch tabs
     * @throws IllegalArgumentException if the {@code faces} argument is empty or missing
     */
    public BatchRegistrationPanel(ActionListener action, Task task) {
        super(action);
        this.setName(NAME);

        initComponents();
        
        infoLinkButton1.setUri(URI.create(HELP_URL));

        Objects.requireNonNull(task);
        this.task = task;

        SelectBoxService.syncSelectionMenu(task, null, false, jComboBox1); // Action event is triggered automatically by the jComboBox1 with the first item set as selected

        jButton1.addActionListener(createListener(action, ACTION_COMMAND_AVERAGE_AND_REGISTER));
        
        jComboBox4.addItem(AVG_FACE_NEAREST_NEIGHBOURS);
        jComboBox4.addItem(AVG_FACE_RAY_CASTING);
        jComboBox4.addItem(AVG_FACE_RAY_CASTING_GPU);
        jComboBox4.addItem(AVG_FACE_NONE);
        
        jComboBox3.addItem(REGISTRATION_ICP);
        jComboBox3.addItem(REGISTRATION_GPA);
        jComboBox3.addItem(REGISTRATION_NONE);

        spinSlider1.initInteger(1000, 0, 5000, 1); // downsampling strength
        spinSlider2.initInteger(3, 1, 5, 1); // max GPA iterations
        spinSlider3.initDouble(0.3, 0, 5, 3); // PGA similaroty threshold

        jButton5.addActionListener(createListener(action, ACTION_COMMAND_EXPORT_AVG_FACE));
        jComboBox1.addActionListener(createListener(action, ACTION_COMMAND_SHOW_SELECTED_FACE));
    }

    @Override
    public ImageIcon getIcon() {
        return getStaticIcon();
    }

    /**
     * Returns the face selected in a menu.
     *
     * @return The face selected in a menu. This is either some face from the dataset, or the average face.
     */
    public HumanFace getSelectedFace() {
        FaceReference faceReference = SelectBoxService.getSelectedFace(task, jComboBox1);
        return FaceService.INSTANCE.getFaceByReference(faceReference);
    }

    /**
     * Returns {@code true} if the item selected in the menu is the average face
     *
     * @return {@code true} if the item selected in the menu is the average face
     */
    public boolean isAvgFaceSelected() {
        return TaskService.INSTANCE.getAvgFace(task) != null;
    }

    /**
     * Select the average face in the manu, if exists.
     */
    public void selectAvgFace() {
        FaceReference averageFace = TaskService.INSTANCE.getAvgFace(task);
        if (averageFace != null) {
            int indexOfAverageFace = task.getFaces().indexOf(averageFace);
            jComboBox1.setSelectedIndex(indexOfAverageFace);
        }
    }

    /**
     * The average face is added, replaced, or removed (if {@code null}) and
     * the action event is automatically triggered by the jBomboBox1 with the first item set as selected.
     * Also, the export button is switched on/off.
     *
     * @param face new AVG face
     */
    public void addAndSelectAvgFace(FaceReference face) {
        SelectBoxService.syncSelectionMenu(task, face, true, jComboBox1);
        jButton5.setEnabled(TaskService.INSTANCE.getAvgFace(task) != null);
    }

    /**
     * Exports the average face, if exists.
     */
    public void exportAvgFace() {
        FaceReference averageFaceReference = TaskService.INSTANCE.getAvgFace(task);

        if (averageFaceReference == null) {
            return;
        }
        HumanFace avgFace = FaceService.INSTANCE.getFaceByReference(averageFaceReference);

        File file = new FileChooserBuilder(ProjectWindow.class)
                .setTitle("Specify a file to save")
                .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                .setFileFilter(new FileNameExtensionFilter("obj files (*.obj)", "obj"))
                .showSaveDialog();

        if (file != null) {
            try {
                MeshIO.exportMeshModel(avgFace.getMeshModel(), file);
            } catch (IOException ex) {
                Logger.print(ex.toString());
            }
        }
    }

    public Task getTask() {
        return task;
    }

    /**
     * Returns the number of desired points, 0 for no downsampling
     * @return the number of desired points, 0 for no downsampling
     */
    public int getIcpUndersampling() {
        return (int) spinSlider1.getValue();
    }
    
    /**
     * Returns the number of iterations of the Generalized Procrustes Registration
     * @return the number of iterations of the Generalized Procrustes Registration
     */
    public int getMaxRegistrationIterations() {
        return (int) spinSlider2.getValue();
    }
    
    /**
     * Returns a threshold for the Generalized Procrustes Registration
     * @return  a threshold for the Generalized Procrustes Registration
     */
    public double getAvgSimilarityThreshold() {
        return (double) spinSlider3.getValue();
    }   

    /**
     * Determines whether to scale during ICP registration.
     * @return if {@code true}, then ICP scales
     */
    public boolean scaleIcp() {
        return jCheckBox4.isSelected();
    }

    /**
     * Returns selected registration strategy
     * @return selected registration strategy
     */
    public BatchFaceRegistrationServices.RegistrationStrategy getRegistrationStrategy() {
        return switch (jComboBox3.getSelectedItem().toString()) {
            case BatchRegistrationPanel.REGISTRATION_GPA -> BatchFaceRegistrationServices.RegistrationStrategy.GPA;
            case BatchRegistrationPanel.REGISTRATION_ICP -> BatchFaceRegistrationServices.RegistrationStrategy.ICP;
            case BatchRegistrationPanel.REGISTRATION_NONE -> BatchFaceRegistrationServices.RegistrationStrategy.NONE;
            default -> throw new IllegalStateException("Unexpected value: " + jComboBox3.getSelectedItem().toString());
        };
    }
    
    /**
     * Returns selected strategy for the average face computation
     * @return selected strategy for the average face computation
     */
    public BatchFaceRegistrationServices.AverageFaceStrategy getAvgFaceStrategy() {
        return switch (jComboBox4.getSelectedItem().toString()) {
            case AVG_FACE_NEAREST_NEIGHBOURS -> BatchFaceRegistrationServices.AverageFaceStrategy.NEAREST_NEIGHBOURS;
            case AVG_FACE_RAY_CASTING -> BatchFaceRegistrationServices.AverageFaceStrategy.PROJECTION_CPU;
            case AVG_FACE_RAY_CASTING_GPU -> BatchFaceRegistrationServices.AverageFaceStrategy.PROJECTION_GPU;
            case AVG_FACE_NONE -> BatchFaceRegistrationServices.AverageFaceStrategy.NONE;
            default -> throw new IllegalStateException("Unexpected value: " + jComboBox4.getSelectedItem().toString());
        };
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new JPanel();
        spinSlider3 = new SpinSlider();
        jLabel5 = new JLabel();
        jComboBox4 = new JComboBox<>();
        jLabel4 = new JLabel();
        jLabel3 = new JLabel();
        spinSlider2 = new SpinSlider();
        jPanel2 = new JPanel();
        jLabel1 = new JLabel();
        jComboBox1 = new JComboBox<>();
        jPanel4 = new JPanel();
        infoLinkButton1 = new InfoLinkButton();
        jPanel3 = new JPanel();
        jLabel6 = new JLabel();
        jComboBox3 = new JComboBox<>();
        jLabel2 = new JLabel();
        spinSlider1 = new SpinSlider();
        jLabel7 = new JLabel();
        jCheckBox4 = new JCheckBox();
        jButton1 = new JButton();
        jButton5 = new JButton();

        setPreferredSize(new Dimension(600, 600));

        jPanel1.setBorder(BorderFactory.createTitledBorder(null, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jPanel1.border.title"), TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Ubuntu", 1, 15))); // NOI18N

        Mnemonics.setLocalizedText(jLabel5, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jLabel5.text")); // NOI18N

        Mnemonics.setLocalizedText(jLabel4, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jLabel4.text")); // NOI18N

        Mnemonics.setLocalizedText(jLabel3, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jLabel3.text")); // NOI18N

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(118, 118, 118)
                        .addComponent(jComboBox4, GroupLayout.PREFERRED_SIZE, 293, GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(spinSlider3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinSlider2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jComboBox4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinSlider2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(spinSlider3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
                .addGap(298, 298, 298))
        );

        jPanel2.setBorder(BorderFactory.createTitledBorder(null, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jPanel2.border.title_1"), TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Ubuntu", 0, 15))); // NOI18N

        jLabel1.setFont(new Font("Ubuntu", 1, 15)); // NOI18N
        Mnemonics.setLocalizedText(jLabel1, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jLabel1.text")); // NOI18N

        GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(BorderFactory.createTitledBorder(NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jPanel4.border.title"))); // NOI18N

        Mnemonics.setLocalizedText(infoLinkButton1, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.infoLinkButton1.text")); // NOI18N
        infoLinkButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                infoLinkButton1ActionPerformed(evt);
            }
        });

        GroupLayout jPanel4Layout = new GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(infoLinkButton1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(infoLinkButton1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(BorderFactory.createTitledBorder(null, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jPanel3.border.title"), TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Ubuntu", 1, 15))); // NOI18N

        Mnemonics.setLocalizedText(jLabel6, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jLabel6.text")); // NOI18N

        Mnemonics.setLocalizedText(jLabel2, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jLabel2.text")); // NOI18N

        Mnemonics.setLocalizedText(jLabel7, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jLabel7.text")); // NOI18N

        Mnemonics.setLocalizedText(jCheckBox4, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jCheckBox4.text")); // NOI18N
        jCheckBox4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jCheckBox4ActionPerformed(evt);
            }
        });

        GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7))
                .addGap(16, 16, 16)
                .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox4)
                    .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addComponent(jComboBox3, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(spinSlider1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(spinSlider1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox4)
                    .addComponent(jLabel7))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Mnemonics.setLocalizedText(jButton1, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jButton1.text")); // NOI18N

        Mnemonics.setLocalizedText(jButton5, NbBundle.getMessage(BatchRegistrationPanel.class, "BatchRegistrationPanel.jButton5.text")); // NOI18N
        jButton5.setEnabled(false);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jButton5)
                    .addComponent(jButton1))
                .addContainerGap(101, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBox4ActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jCheckBox4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox4ActionPerformed

    private void infoLinkButton1ActionPerformed(ActionEvent evt) {//GEN-FIRST:event_infoLinkButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_infoLinkButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private InfoLinkButton infoLinkButton1;
    private JButton jButton1;
    private JButton jButton5;
    private JCheckBox jCheckBox4;
    private JComboBox<String> jComboBox1;
    private JComboBox<String> jComboBox3;
    private JComboBox<String> jComboBox4;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private SpinSlider spinSlider1;
    private SpinSlider spinSlider2;
    private SpinSlider spinSlider3;
    // End of variables declaration//GEN-END:variables

}
