package cz.fidentis.analyst.gui.task.featurepoints;

import cz.fidentis.analyst.data.landmarks.FeaturePointsTable;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.gui.task.ControlPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Control panel for feature points.
 * 
 * @author Katerina Zarska
 */
public class FeaturePointsPanel extends ControlPanel {

    /*
     * Mandatory design elements
     */
    public static final String ICON = "fps28x28.png";
    public static final String NAME = "Feature Points";

    /*
     * Actions triggered by this panel and served by the associated action listener 
     */
    public static final String ACTION_COMMAND_NEW_FEATURE_POINT = "new feature point";
    public static final String ACTION_COMMAND_REMOVE_FEATURE_POINT = "remove feature point";
    public static final String ACTION_COMMAND_RELOCATE_FEATURE_POINT = "edit feature point";
    public static final String ACTION_CHANGE_DISTANCE_THRESHOLD = "change threshold for feature point";
    public static final String ACTION_PIN_SELECTED_FEATURE_POINTS = "pin highlighted feature points to mesh";
    public static final String ACTION_PIN_ALL_FEATURE_POINTS = "pin all feature points to mesh";
    public static final String ACTION_ADD_FEATURE_POINT = "add feature point";
    public static final String ACTION_CANCEL_ADD_FEATURE_POINT = "cancel adding feature point";
    public static final String ACTION_SAVE_CHANGES = "save changes to a feature point";
    public static final String ACTION_FILL_NAME_AND_DESCRIPTION = "fill in text fields";
    public static final String ACTION_SHOW_HIDE_SKELETON = "show/hide skeleton of feature points";
    public static final String ACTION_CHANGE_TYPE_COLOR = "changes color of a selected type of feature point";
    public static final String ACTION_AUTODETECT_FEATURE_POINT = "autodetect feature points";
    public static final String ACTION_SAVE_FEATURE_POINT = "save feature points";

    /**
     * Creates new form FeaturePointsPanel
     */
    public FeaturePointsPanel(ActionListener action, DrawableFeaturePoints featurePoints) {
        super(action);
        setName(NAME);
        initComponents();
        resetSlider(featurePoints.getMaxDistance(), featurePoints);
        loadAvailableFps(featurePoints);
        featurePointListPanel.initComponents(action, featurePoints);

        newPrimaryFpButton.addActionListener(createListener(action, ACTION_COMMAND_NEW_FEATURE_POINT));
        newSecondaryFpButton.addActionListener(createListener(action, ACTION_COMMAND_NEW_FEATURE_POINT));
        
        pinSelectedFpButton.addActionListener(createListener(action, ACTION_PIN_SELECTED_FEATURE_POINTS));
        pinAllFpButton.addActionListener(createListener(action, ACTION_PIN_ALL_FEATURE_POINTS));

        removeFpButton.addActionListener(createListener(action, ACTION_COMMAND_REMOVE_FEATURE_POINT));
        removeFpButton.setEnabled(false);
        relocateFpButton.addActionListener(createListener(action, ACTION_COMMAND_RELOCATE_FEATURE_POINT));
        relocateFpButton.setEnabled(false);
        
        savePositionButton.setVisible(false);
        savePositionButton.setEnabled(false);
        savePositionButton.addActionListener(createListener(action, ACTION_COMMAND_RELOCATE_FEATURE_POINT));
        
        addPrimaryFpButton.addActionListener(createListener(action, ACTION_ADD_FEATURE_POINT));
        addPrimaryFpButton.setEnabled(false);
        addPrimaryFpButton.setVisible(false);
        addSecondaryFpButton.addActionListener(createListener(action, ACTION_ADD_FEATURE_POINT));
        addSecondaryFpButton.setEnabled(false);
        addSecondaryFpButton.setVisible(false);

        cancelPrimaryFpButton.addActionListener(createListener(action, ACTION_CANCEL_ADD_FEATURE_POINT));
        cancelPrimaryFpButton.setVisible(false);
        cancelSecondaryFpButton.addActionListener(createListener(action, ACTION_CANCEL_ADD_FEATURE_POINT));
        cancelSecondaryFpButton.setVisible(false);

        jButton1.addActionListener(createListener(action, ACTION_AUTODETECT_FEATURE_POINT));

        jButton2.addActionListener(createListener(action, ACTION_SAVE_FEATURE_POINT));

        newPrimaryFpList.setEnabled(false);
        newPrimaryFpList.setVisible(false);
        newSecondaryFpName.setEnabled(false);
        newSecondaryFpName.setVisible(false);

        fpDescription.setLineWrap(true);
        fpDescription.setWrapStyleWord(true);
        fpDescription.setEditable(false);

        editFpButton.addActionListener(createListener(action, ACTION_FILL_NAME_AND_DESCRIPTION));
        editFpButton.setEnabled(false);
        nameLabel.setEnabled(false);
        descriptionLabel.setEnabled(false);
        nameField.setEnabled(false);
        descriptionField.setEnabled(false);
        saveFpChangesButton.addActionListener(createListener(action, ACTION_SAVE_CHANGES));
        saveFpChangesButton.setEnabled(false);
        cancelFpChangesButton.setEnabled(false);
        
        showSkeletonCheckBox.addActionListener(createListener(action, ACTION_SHOW_HIDE_SKELETON));
        
        colorOffMesh.setOpaque(true);
        colorOffMesh.setBorderPainted(false);
        colorOffMesh.setBackground(featurePoints.getOffTheMeshColor());
        colorOffMesh.addActionListener(createListener(action, ACTION_CHANGE_TYPE_COLOR));
        
        colorOnMesh.setOpaque(true);
        colorOnMesh.setBorderPainted(false);
        colorOnMesh.setBackground(featurePoints.getOnTheMeshColor());
        colorOnMesh.addActionListener(createListener(action, ACTION_CHANGE_TYPE_COLOR));
        
        colorCloseToMesh.setOpaque(true);
        colorCloseToMesh.setBorderPainted(false);
        colorCloseToMesh.setBackground(featurePoints.getCloseToMeshColor());
        colorCloseToMesh.addActionListener(createListener(action, ACTION_CHANGE_TYPE_COLOR));
        
        colorSecondary.setOpaque(true);
        colorSecondary.setBorderPainted(false);
        colorSecondary.setBackground(featurePoints.getCustomFpColor());
        colorSecondary.addActionListener(createListener(action, ACTION_CHANGE_TYPE_COLOR));
    }

    @Override
    public ImageIcon getIcon() {
        return getStaticIcon();
    }

    /**
     * Static implementation of the {@link #getIcon()} method.
     *
     * @return Control panel icon
     */
    public static ImageIcon getStaticIcon() {
        return new ImageIcon(FeaturePointsPanel.class.getClassLoader().getResource("/" + ICON));
    }

    /**
     * Gets the FeaturePointListPanel
     *
     * @return FeaturePointListPanel
     */
    public FeaturePointListPanel getFeaturePointListPanel() {
        return featurePointListPanel;
    }

    /**
     * Resets the slider into initial values
     */
    private void resetSlider(double distance, DrawableFeaturePoints featurePoints) {
        if (distance < 0) {
            distance = 1;
        }
        featurePoints.setDistanceThreshold(distance * 0.5);
        thresholdSpinSlider.initDouble(distance * 0.5, 0, distance * 1.1, 3);
        thresholdSpinSlider.addSpinnerListener(createListener(getActionListener(), ACTION_CHANGE_DISTANCE_THRESHOLD));
    }

    /**
     * Updates the list of not currently used STANDARD feature points 
     * 
     * @param fps available feature points
     */
    public final void loadAvailableFps(DrawableFeaturePoints fps) {
        newPrimaryFpList.removeAllItems();

        FeaturePointsTable fpTable = FeaturePointsTable.getInstance();
        Set<Integer> featurePoints = new HashSet<>(fpTable.getIDs());
        featurePoints.removeAll(fps.getFeaturePoints().stream().map(Landmark::getType).toList());
        featurePoints.forEach(id -> newPrimaryFpList.addItem(fpTable.getFpName(id)));
    }

    /**
     * Changes the state of certain buttons based on number of selected feature points
     * 
     * @param selected currently selected feature points
     */
    public void changeStateOfButtons(List<Landmark> selected) {
        if (selected.size() == 1 && selected.get(0).isUserDefinedLandmark()) {
            relocateFpButton.setEnabled(true);
            removeFpButton.setEnabled(true);
            editFpButton.setEnabled(true);
        } else if (selected.size() == 1) {
            relocateFpButton.setEnabled(true);
            removeFpButton.setEnabled(true);
            editFpButton.setEnabled(false);
        } else if (selected.size() > 1) {
            relocateFpButton.setEnabled(false);
            removeFpButton.setEnabled(true);
            editFpButton.setEnabled(false);
        } else {
            relocateFpButton.setEnabled(false);
            removeFpButton.setEnabled(false);
            editFpButton.setEnabled(false);
        }
    }

    /**
     * Updates the visible description of a feature point
     * 
     * @param fpt landmarks
     * @param visible visibility
     */
    public void updateFpDescription(Landmark fpt, boolean visible) {
        if (visible) {
            fpDescription.setText(fpt.getDescription());
        } else {
            fpDescription.setText("");
        }
    }
    
    /**
     * Sets the state of the showSkeletonCheckbox
     * 
     * @param show show or hide
     */
    public void setShowSkeletonCheckBox(boolean show) {
        showSkeletonCheckBox.setSelected(show);
    }

    /**
     * Sets the state of addFpButton
     * @param state state
     */
    public void setAddButton(boolean state) {
        addPrimaryFpButton.setEnabled(state);
        addSecondaryFpButton.setEnabled(state);
    }
    
    /**
     * Sets the state of SaveFpChangesButton
     * @param state state
     */
    public void setSaveFpChangesButton(boolean state) {
        saveFpChangesButton.setEnabled(state);
    }
    
    /**
     * Gets the newFpList (list of available STANDARD feature points)
     * @return JComboBox
     */
    public JComboBox getNewPrimaryFpList() {
        return newPrimaryFpList;
    }
    
    public JTextField getNewSecondaryFpName() {
        return newSecondaryFpName;
    }
    
    public JButton getAddPrimaryFpButton() {
        return addPrimaryFpButton;
    }
    
    public JButton getAddSecondaryFpButton() {
        return addSecondaryFpButton;
    }
    
    /**
     * Gets the text from the nameField
     * @return String
     */
    public String getNameText() {
        return nameField.getText();
    }
    
    /**
     * Sets the text in the nameField
     * @param name new name
     */
    public void setNameText(String name) {
        nameField.setText(name);
    }
    
    /**
     * Gets the text from descriptionField
     * @return description
     */
    public String getDescriptionText() {
        return descriptionField.getText();
    }
    
    /**
     * Sets the text in the descriptionField
     * @param description new description
     */
    public void setDescriptionText(String description) {
        descriptionField.setText(description);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jColorChooser1 = new javax.swing.JColorChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        featurePointListPanel = new cz.fidentis.analyst.gui.task.featurepoints.FeaturePointListPanel();
        removeFpButton = new javax.swing.JButton();
        thresholdSpinSlider = new cz.fidentis.analyst.gui.elements.SpinSlider();
        pinSelectedFpButton = new javax.swing.JButton();
        pinAllFpButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        newPrimaryFpList = new javax.swing.JComboBox<>();
        addPrimaryFpButton = new javax.swing.JButton();
        cancelPrimaryFpButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        fpDescription = new javax.swing.JTextArea();
        nameField = new javax.swing.JTextField();
        nameLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        descriptionField = new javax.swing.JTextArea();
        saveFpChangesButton = new javax.swing.JButton();
        editFpButton = new javax.swing.JButton();
        relocateFpButton = new javax.swing.JButton();
        cancelFpChangesButton = new javax.swing.JButton();
        newPrimaryFpButton = new javax.swing.JButton();
        addSecondaryFpButton = new javax.swing.JButton();
        cancelSecondaryFpButton = new javax.swing.JButton();
        newSecondaryFpButton = new javax.swing.JButton();
        newSecondaryFpName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        colorCloseToMesh = new javax.swing.JButton();
        colorOnMesh = new javax.swing.JButton();
        colorOffMesh = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        colorSecondary = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        showSkeletonCheckBox = new javax.swing.JCheckBox();
        savePositionButton = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        featurePointListPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.featurePointListPanel.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        javax.swing.GroupLayout featurePointListPanelLayout = new javax.swing.GroupLayout(featurePointListPanel);
        featurePointListPanel.setLayout(featurePointListPanelLayout);
        featurePointListPanelLayout.setHorizontalGroup(
            featurePointListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 532, Short.MAX_VALUE)
        );
        featurePointListPanelLayout.setVerticalGroup(
            featurePointListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 367, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(featurePointListPanel);
        featurePointListPanel.getAccessibleContext().setAccessibleDescription(org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.featurePointListPanel.AccessibleContext.accessibleDescription")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(removeFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.removeFpButton.text")); // NOI18N
        removeFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeFpButtonActionPerformed(evt);
            }
        });

        thresholdSpinSlider.setToolTipText(org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.thresholdSpinSlider.toolTipText")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(pinSelectedFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.pinSelectedFpButton.text_2")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(pinAllFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.pinAllFpButton.text_2")); // NOI18N
        pinAllFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pinAllFpButtonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel1.text")); // NOI18N

        newPrimaryFpList.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        org.openide.awt.Mnemonics.setLocalizedText(addPrimaryFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.addPrimaryFpButton.text_2")); // NOI18N
        addPrimaryFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPrimaryFpButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(cancelPrimaryFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.cancelPrimaryFpButton.text")); // NOI18N
        cancelPrimaryFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelPrimaryFpButtonActionPerformed(evt);
            }
        });

        fpDescription.setColumns(20);
        fpDescription.setRows(5);
        fpDescription.setPreferredSize(new java.awt.Dimension(542, 84));
        jScrollPane2.setViewportView(fpDescription);

        nameField.setText(org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.nameField.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(nameLabel, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.nameLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(descriptionLabel, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.descriptionLabel.text")); // NOI18N

        descriptionField.setColumns(20);
        descriptionField.setRows(5);
        jScrollPane3.setViewportView(descriptionField);

        org.openide.awt.Mnemonics.setLocalizedText(saveFpChangesButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.saveFpChangesButton.text_2")); // NOI18N
        saveFpChangesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveFpChangesButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(editFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.editFpButton.text_2")); // NOI18N
        editFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editFpButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(relocateFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.relocateFpButton.text_2")); // NOI18N
        relocateFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                relocateFpButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(cancelFpChangesButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.cancelFpChangesButton.text_2")); // NOI18N
        cancelFpChangesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelFpChangesButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(newPrimaryFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.newPrimaryFpButton.text_2")); // NOI18N
        newPrimaryFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newPrimaryFpButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(addSecondaryFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.addSecondaryFpButton.text")); // NOI18N
        addSecondaryFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSecondaryFpButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(cancelSecondaryFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.cancelSecondaryFpButton.text")); // NOI18N
        cancelSecondaryFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelSecondaryFpButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(newSecondaryFpButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.newSecondaryFpButton.text")); // NOI18N
        newSecondaryFpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newSecondaryFpButtonActionPerformed(evt);
            }
        });

        newSecondaryFpName.setText(org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.newSecondaryFpName.text")); // NOI18N

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel2.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(colorCloseToMesh, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.colorCloseToMesh.text_2")); // NOI18N
        colorCloseToMesh.setMaximumSize(new java.awt.Dimension(22, 22));
        colorCloseToMesh.setMinimumSize(new java.awt.Dimension(22, 22));
        colorCloseToMesh.setPreferredSize(new java.awt.Dimension(22, 22));

        org.openide.awt.Mnemonics.setLocalizedText(colorOnMesh, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.colorOnMesh.text")); // NOI18N
        colorOnMesh.setMaximumSize(new java.awt.Dimension(22, 22));
        colorOnMesh.setMinimumSize(new java.awt.Dimension(22, 22));
        colorOnMesh.setPreferredSize(new java.awt.Dimension(22, 22));

        org.openide.awt.Mnemonics.setLocalizedText(colorOffMesh, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.colorOffMesh.text")); // NOI18N
        colorOffMesh.setMaximumSize(new java.awt.Dimension(22, 22));
        colorOffMesh.setMinimumSize(new java.awt.Dimension(22, 22));
        colorOffMesh.setPreferredSize(new java.awt.Dimension(22, 22));

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel3.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel4.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(colorSecondary, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.colorSecondary.text")); // NOI18N
        colorSecondary.setMaximumSize(new java.awt.Dimension(22, 22));
        colorSecondary.setMinimumSize(new java.awt.Dimension(22, 22));
        colorSecondary.setPreferredSize(new java.awt.Dimension(22, 22));

        org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel5.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel6.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel7, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel7.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel8, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel8.text")); // NOI18N

        showSkeletonCheckBox.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(showSkeletonCheckBox, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.showSkeletonCheckBox.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(savePositionButton, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.savePositionButton.text_2")); // NOI18N
        savePositionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savePositionButtonActionPerformed(evt);
            }
        });

        jLabel9.setForeground(new java.awt.Color(255, 153, 0));
        org.openide.awt.Mnemonics.setLocalizedText(jLabel9, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel9.text")); // NOI18N
        jLabel9.setToolTipText(org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel9.toolTipText")); // NOI18N

        jLabel10.setForeground(new java.awt.Color(255, 153, 0));
        org.openide.awt.Mnemonics.setLocalizedText(jLabel10, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel10.text")); // NOI18N
        jLabel10.setToolTipText(org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jLabel10.toolTipText")); // NOI18N

        jButton1.setBackground(new java.awt.Color(255, 204, 153));
        org.openide.awt.Mnemonics.setLocalizedText(jButton1, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jButton1.text_2")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.jButton2.text")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showSkeletonCheckBox))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(descriptionLabel)
                                .addComponent(nameField)
                                .addComponent(jScrollPane3)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(cancelFpChangesButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(saveFpChangesButton))
                                .addComponent(editFpButton, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(nameLabel)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pinSelectedFpButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pinAllFpButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(thresholdSpinSlider, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(colorOffMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel7))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(colorOnMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel5))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(colorCloseToMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel6))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(colorSecondary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel8))))))
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(savePositionButton, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(removeFpButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(relocateFpButton, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE))
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(cancelSecondaryFpButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(addSecondaryFpButton))
                                .addComponent(newSecondaryFpName)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(cancelPrimaryFpButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(addPrimaryFpButton))
                                .addComponent(newPrimaryFpList, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(newPrimaryFpButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(newSecondaryFpButton, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)))))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(colorOnMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(colorCloseToMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(colorOffMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8)
                            .addComponent(colorSecondary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(thresholdSpinSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(pinSelectedFpButton)
                            .addComponent(pinAllFpButton)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(nameLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(editFpButton)
                                .addGap(34, 34, 34)
                                .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(descriptionLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(saveFpChangesButton)
                            .addComponent(cancelFpChangesButton)
                            .addComponent(showSkeletonCheckBox))))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(newPrimaryFpButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(newPrimaryFpList, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addPrimaryFpButton)
                            .addComponent(cancelPrimaryFpButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(newSecondaryFpButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(newSecondaryFpName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addSecondaryFpButton)
                            .addComponent(cancelSecondaryFpButton)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(removeFpButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(relocateFpButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(savePositionButton)
                        .addGap(32, 32, 32)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        thresholdSpinSlider.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(FeaturePointsPanel.class, "FeaturePointsPanel.thresholdSpinSlider.AccessibleContext.accessibleName")); // NOI18N
    }// </editor-fold>//GEN-END:initComponents


    private void addPrimaryFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addPrimaryFpButtonActionPerformed
        newPrimaryFpList.setVisible(false);
        addPrimaryFpButton.setVisible(false);
        cancelPrimaryFpButton.setVisible(false);

        newPrimaryFpList.setEnabled(false);
        addPrimaryFpButton.setEnabled(false);
        addSecondaryFpButton.setEnabled(false);
        newSecondaryFpButton.setEnabled(true);
    }//GEN-LAST:event_addPrimaryFpButtonActionPerformed

    private void pinAllFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pinAllFpButtonActionPerformed
        thresholdSpinSlider.setEnabled(false);
        pinSelectedFpButton.setEnabled(false);
        pinAllFpButton.setEnabled(false);
    }//GEN-LAST:event_pinAllFpButtonActionPerformed

    private void cancelPrimaryFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelPrimaryFpButtonActionPerformed
        newPrimaryFpList.setVisible(false);
        addPrimaryFpButton.setVisible(false);
        cancelPrimaryFpButton.setVisible(false);

        newPrimaryFpList.setEnabled(false);
        addPrimaryFpButton.setEnabled(false);
        addSecondaryFpButton.setEnabled(false);
        newSecondaryFpButton.setEnabled(true);
    }//GEN-LAST:event_cancelPrimaryFpButtonActionPerformed

    private void removeFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeFpButtonActionPerformed
        removeFpButton.setEnabled(false);
    }//GEN-LAST:event_removeFpButtonActionPerformed

    private void saveFpChangesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveFpChangesButtonActionPerformed
        if (!nameField.getText().isBlank()) {
            Component[] components = featurePointListPanel.getComponents();
            for (Component c : components) {
                c.setEnabled(true);
            }
            featurePointListPanel.setEnabled(true);
            editFpButton.setEnabled(true);
            nameLabel.setEnabled(false);
            nameField.setEnabled(false);
            descriptionLabel.setEnabled(false);
            descriptionField.setEnabled(false);
            saveFpChangesButton.setEnabled(false);
            cancelFpChangesButton.setEnabled(false);
        }
    }//GEN-LAST:event_saveFpChangesButtonActionPerformed

    private void editFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editFpButtonActionPerformed
        Component[] components = featurePointListPanel.getComponents();
        for (Component c : components) {
            if (c instanceof JCheckBox && ((JCheckBox) c).isSelected()) {
                c.setEnabled(false);
            } else {
                c.setEnabled(false);
            }
        }
        featurePointListPanel.setEnabled(false);
        editFpButton.setEnabled(false);
        nameLabel.setEnabled(true);
        nameField.setEnabled(true);
        descriptionLabel.setEnabled(true);
        descriptionField.setEnabled(true);
        saveFpChangesButton.setEnabled(true);
        cancelFpChangesButton.setEnabled(true);
    }//GEN-LAST:event_editFpButtonActionPerformed

    private void relocateFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_relocateFpButtonActionPerformed
        relocateFpButton.setEnabled(false);
        removeFpButton.setEnabled(false);
        newPrimaryFpButton.setEnabled(false);
        newSecondaryFpButton.setEnabled(false);
        editFpButton.setEnabled(false);
        savePositionButton.setVisible(true);
        savePositionButton.setEnabled(true);
    }//GEN-LAST:event_relocateFpButtonActionPerformed

    private void cancelFpChangesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelFpChangesButtonActionPerformed
        if (!nameField.getText().isBlank()) {
            Component[] components = featurePointListPanel.getComponents();
            for (Component c : components) {
                c.setEnabled(true);
            }
            featurePointListPanel.setEnabled(true);
            editFpButton.setEnabled(true);
            nameLabel.setEnabled(false);
            nameField.setEnabled(false);
            descriptionLabel.setEnabled(false);
            descriptionField.setEnabled(false);
            saveFpChangesButton.setEnabled(false);
            cancelFpChangesButton.setEnabled(false);
        }
    }//GEN-LAST:event_cancelFpChangesButtonActionPerformed

    private void newPrimaryFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newPrimaryFpButtonActionPerformed
        relocateFpButton.setEnabled(false);
        removeFpButton.setEnabled(false);
        newSecondaryFpButton.setEnabled(false);

        newPrimaryFpList.setEnabled(true);

        newPrimaryFpList.setVisible(true);
        addPrimaryFpButton.setVisible(true);
        cancelPrimaryFpButton.setVisible(true);
    }//GEN-LAST:event_newPrimaryFpButtonActionPerformed

    private void addSecondaryFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSecondaryFpButtonActionPerformed
        newSecondaryFpName.setVisible(false);
        addSecondaryFpButton.setVisible(false);
        cancelSecondaryFpButton.setVisible(false);

        newSecondaryFpName.setEnabled(false);
        addPrimaryFpButton.setEnabled(false);
        addSecondaryFpButton.setEnabled(false);
        newPrimaryFpButton.setEnabled(true);
    }//GEN-LAST:event_addSecondaryFpButtonActionPerformed

    private void cancelSecondaryFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelSecondaryFpButtonActionPerformed
        newSecondaryFpName.setVisible(false);
        addSecondaryFpButton.setVisible(false);
        cancelSecondaryFpButton.setVisible(false);

        newSecondaryFpName.setEnabled(false);
        addPrimaryFpButton.setEnabled(false);
        addSecondaryFpButton.setEnabled(false);
        newPrimaryFpButton.setEnabled(true);
    }//GEN-LAST:event_cancelSecondaryFpButtonActionPerformed

    private void newSecondaryFpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newSecondaryFpButtonActionPerformed
        relocateFpButton.setEnabled(false);
        removeFpButton.setEnabled(false);
        newPrimaryFpButton.setEnabled(false);

        newSecondaryFpName.setEnabled(true);
        newSecondaryFpName.setVisible(true);
        addSecondaryFpButton.setVisible(true);
        cancelSecondaryFpButton.setVisible(true);
    }//GEN-LAST:event_newSecondaryFpButtonActionPerformed

    private void savePositionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savePositionButtonActionPerformed
        newPrimaryFpButton.setEnabled(true);
        newSecondaryFpButton.setEnabled(true);
        savePositionButton.setEnabled(false);
        savePositionButton.setVisible(false);
    }//GEN-LAST:event_savePositionButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed


    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addPrimaryFpButton;
    private javax.swing.JButton addSecondaryFpButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton cancelFpChangesButton;
    private javax.swing.JButton cancelPrimaryFpButton;
    private javax.swing.JButton cancelSecondaryFpButton;
    private javax.swing.JButton colorCloseToMesh;
    private javax.swing.JButton colorOffMesh;
    private javax.swing.JButton colorOnMesh;
    private javax.swing.JButton colorSecondary;
    private javax.swing.JTextArea descriptionField;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JButton editFpButton;
    private cz.fidentis.analyst.gui.task.featurepoints.FeaturePointListPanel featurePointListPanel;
    private javax.swing.JTextArea fpDescription;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField nameField;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JButton newPrimaryFpButton;
    private javax.swing.JComboBox<String> newPrimaryFpList;
    private javax.swing.JButton newSecondaryFpButton;
    private javax.swing.JTextField newSecondaryFpName;
    private javax.swing.JButton pinAllFpButton;
    private javax.swing.JButton pinSelectedFpButton;
    private javax.swing.JButton relocateFpButton;
    private javax.swing.JButton removeFpButton;
    private javax.swing.JButton saveFpChangesButton;
    private javax.swing.JButton savePositionButton;
    private javax.swing.JCheckBox showSkeletonCheckBox;
    private cz.fidentis.analyst.gui.elements.SpinSlider thresholdSpinSlider;
    // End of variables declaration//GEN-END:variables
}
