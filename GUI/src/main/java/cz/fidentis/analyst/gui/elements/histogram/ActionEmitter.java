package cz.fidentis.analyst.gui.elements.histogram;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Convenience class for handling {@link ActionEvent}s for components that don't support it by default.
 *
 * @author Jakub Nezval
 */
public class ActionEmitter {
    private final Collection<ActionListener> listeners = new ArrayList<>();

    /**
     * @param listener listener
     * @return {@code true} if {@code listener} was successfully added to collection
     */
    public boolean addActionListener(ActionListener listener) {
        return this.listeners.add(listener);
    }

    /**
     * @param listener listener
     * @return {@code true} if {@code listener} was successfully removed from collection
     */
    public boolean removeActionListener(ActionListener listener) {
        return this.listeners.remove(listener);
    }

    /**
     * @param event event to be trigger on all registered listeners
     * @see ActionEmitter#addActionListener(ActionListener)
     */
    public void triggerActionEvent(ActionEvent event) {
        for (var listener : this.listeners) {
            listener.actionPerformed(event);
        }
    }
}
