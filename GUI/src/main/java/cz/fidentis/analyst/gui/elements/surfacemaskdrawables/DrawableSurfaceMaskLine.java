package cz.fidentis.analyst.gui.elements.surfacemaskdrawables;

import cz.fidentis.analyst.data.surfacemask.SurfaceMaskLine;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Class representing a 2D drawable surface mask line
 * @author Mario Chromik
 */
public class DrawableSurfaceMaskLine implements DrawableSurfaceMask2D{
    private SurfaceMaskLine mask;

    /**
     * Constructor with one paramter
     * @param mask to draw
     */
    public DrawableSurfaceMaskLine(SurfaceMaskLine mask) {
        this.mask = mask;
    }

    public SurfaceMaskLine getMask() {
        return mask;
    }

    public void setMask(SurfaceMaskLine mask) {
        this.mask = mask;
    }

    @Override
    public void draw(Graphics g) {
        SurfaceMaskLine.MaskPointList maskPointsList = mask.getMaskPointsList();
        if (maskPointsList.isEmpty()) {
            return;
        }

        SurfaceMaskLine.MaskPoint point = maskPointsList.getFirst();
        for (int i = 0; i < maskPointsList.getSize(); i++) {
            if (point.getPoint() == mask.getSelectedPoint()) {
                g.setColor(Color.GREEN);
            }
            g.drawOval(point.getPoint().x - SurfaceMaskLine.POINT_SIZE /2,
                    point.getPoint().y - SurfaceMaskLine.POINT_SIZE /2,
                    SurfaceMaskLine.POINT_SIZE, SurfaceMaskLine.POINT_SIZE);
            g.setColor(mask.getColor());
            if (point.getNext() == null) {
                break;
            }

            g.drawLine(point.getPoint().x, point.getPoint().y, point.getNext().getPoint().x,
                    point.getNext().getPoint().y);

            point = point.getNext();
        }
    }
}
