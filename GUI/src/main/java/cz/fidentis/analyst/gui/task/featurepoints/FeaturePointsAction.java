package cz.fidentis.analyst.gui.task.featurepoints;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.canvas.CanvasState;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.LandmarksFactory;
import cz.fidentis.analyst.data.landmarks.LandmarksIO;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.drawables.DrawableFpSkeleton;
import cz.fidentis.analyst.data.face.events.HumanFaceSelectedEvent;
import cz.fidentis.analyst.data.face.events.LandmarksChangedEvent;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.gui.elements.DoubleSpinner;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.gui.task.LoadedActionEvent;
import cz.fidentis.analyst.landmarks.LandmarksDetector;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static cz.fidentis.analyst.engines.sampling.PointSamplingConfig.Method.UNIFORM_SPACE;

/**
 * Action listener for the FeaturePointsPanel
 * 
 * @author Katerina Zarska
 */
public class FeaturePointsAction extends ControlPanelAction<FeaturePointsPanel> implements HumanFaceListener {
    
    private final List<Landmark> selectedFeaturePoints = new ArrayList<>();
    private Landmark workingFP;
    private String workingFpMode = DISABLE;
    
    private String featurePointsVisible = DISABLE;

    private final DrawableFpSkeleton skeleton;
    private boolean skeletonVisible = false;
    
    private static final Color  FEATURE_POINT_HIGHLIGHT_COLOR = Color.MAGENTA;
    private static final Color  FEATURE_POINT_HOVER_COLOR = Color.CYAN;
    
    private static final String ENABLE_CHANGE_FP_POSITION = "enable change position of a feature point";
    private static final String ENABLE_ADD_FP = "enable adding feature points";
    private static final String DISABLE = "disabled";
    private static final String ENABLE = "enabled";
    
    /**
     * Constructor.
     * A new {@code FeaturePointsPanel} is instantiated and added to the {@code topControlPane}
     * 
     * @param canvas OpenGLCanvas
     * @param task Task with faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public FeaturePointsAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);
        setControlPanel(new FeaturePointsPanel(this, getPrimaryFeaturePoints()));

        skeleton = new DrawableFpSkeleton(getPrimaryFeaturePoints().getFeaturePoints());
        getCanvas().getScene().setOtherDrawable(getCanvas().getScene().getFreeSlotForOtherDrawables(), skeleton);
        skeleton.show(false);

        setShowHideCode(
                () -> { // SHOW
                    getCanvas().getScene().setDefaultColors();
                    setColorsOfFeaturePoints();
                    showFeaturePoints(true);
                },
                () -> { // HIDE
                    getPrimaryFeaturePoints().resetAllColorsToDefault();
                    showFeaturePoints(false);
                    skeleton.show(false);
                    getControlPanel().setShowSkeletonCheckBox(false);
                }
        );

        // Be informed about changes in faces performed by other GUI elements
        getPrimaryDrawableFace().getHumanFace().registerListener(this);
        if (getSecondaryDrawableFace() != null) {
            getSecondaryDrawableFace().getHumanFace().registerListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        
        switch (action) {
            case FeaturePointsPanel.ACTION_COMMAND_NEW_FEATURE_POINT:
                workingFpMode = ENABLE_ADD_FP;
                getControlPanel().loadAvailableFps(getPrimaryFeaturePoints());
                break;
                
            case FeaturePointsPanel.ACTION_ADD_FEATURE_POINT:
                addFeaturePoint(ae);
                skeleton.loadSkeleton(getPrimaryFeaturePoints().getFeaturePoints());
                getControlPanel().changeStateOfButtons(selectedFeaturePoints);
                break;
                
            case FeaturePointsPanel.ACTION_CANCEL_ADD_FEATURE_POINT:
                workingFpMode = DISABLE;
                getPrimaryFeaturePoints().removeFeaturePoint(workingFP);
                workingFP = null;
                getControlPanel().changeStateOfButtons(selectedFeaturePoints);
                break;
            
            case FeaturePointsPanel.ACTION_FILL_NAME_AND_DESCRIPTION:
                getControlPanel().setNameText(selectedFeaturePoints.getFirst().getName());
                getControlPanel().setDescriptionText(selectedFeaturePoints.getFirst().getDescription());
                break;
                
            case FeaturePointsPanel.ACTION_SAVE_CHANGES:
                saveChanges();
                break;
                
            case FeaturePointsPanel.ACTION_COMMAND_REMOVE_FEATURE_POINT:
                workWithSelectedFp("remove");
                getControlPanel().getFeaturePointListPanel().refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
                setColorsOfFeaturePoints();
                skeleton.loadSkeleton(getPrimaryFeaturePoints().getFeaturePoints());
                getControlPanel().changeStateOfButtons(selectedFeaturePoints);
                break;
                
            case FeaturePointsPanel.ACTION_COMMAND_RELOCATE_FEATURE_POINT:
                if (((JButton) ae.getSource()).getText().equals("Change position of a feature point")) {
                    workingFpMode = ENABLE_CHANGE_FP_POSITION;
                } else if (((JButton) ae.getSource()).getText().equals("Save position")) {
                    workingFpMode = DISABLE;
                    getControlPanel().changeStateOfButtons(selectedFeaturePoints);
                }
                skeleton.loadSkeleton(getPrimaryFeaturePoints().getFeaturePoints());
                break;
                
            case FeaturePointListPanel.ACTION_COMMAND_FEATURE_POINT_SELECT:
                selectFeaturePoint((LoadedActionEvent) ae);
                setColorsOfFeaturePoints();
                getControlPanel().changeStateOfButtons(selectedFeaturePoints);
                break;
                
            case FeaturePointListPanel.ACTION_COMMAND_FEATURE_POINT_HOVER_IN:
                hoverFeaturePoint((LoadedActionEvent) ae, true);
                break;
                
            case FeaturePointListPanel.ACTION_COMMAND_FEATURE_POINT_HOVER_OUT:
                hoverFeaturePoint((LoadedActionEvent) ae, false);
                break;
                
            case FeaturePointsPanel.ACTION_CHANGE_DISTANCE_THRESHOLD:
                changeDistanceThreshold(ae);
                setColorsOfFeaturePoints();
                break; 
                
            case FeaturePointsPanel.ACTION_PIN_ALL_FEATURE_POINTS:
                for (Landmark fp : getPrimaryFeaturePoints().getFeaturePoints()) {
                    if (fp.getMeshVicinity().distance() != 0) {
                        pinFeaturePointToMesh(fp);
                    }
                }
                setColorsOfFeaturePoints();
                getControlPanel().getFeaturePointListPanel().refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
                break;
                
            case FeaturePointsPanel.ACTION_PIN_SELECTED_FEATURE_POINTS:
                workWithSelectedFp("pin");
                setColorsOfFeaturePoints();
                getControlPanel().getFeaturePointListPanel().refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
                break;
                
            case FeaturePointsPanel.ACTION_SHOW_HIDE_SKELETON:
                skeletonVisible = ((JCheckBox) ae.getSource()).isSelected();
                skeleton.show(skeletonVisible);
                break;
                
            case FeaturePointsPanel.ACTION_CHANGE_TYPE_COLOR:
                changeTypeColor(ae);
                break;

            case FeaturePointsPanel.ACTION_AUTODETECT_FEATURE_POINT:
                int faceSlot = getCanvas().getScene().getPrimaryFaceSlot();
                if (addRecognizedLandmarks(recognizeLandmarks(faceSlot), faceSlot)) {
                    getControlPanel().getFeaturePointListPanel().refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
                }
                break;

            case FeaturePointsPanel.ACTION_SAVE_FEATURE_POINT:
                saveLandmarks();
                break;
                
            default:
                // do nothing
        }
        renderScene();
    }

    @Override
    public void acceptEvent(HumanFaceEvent event) {
        if (event instanceof HumanFaceSelectedEvent && !workingFpMode.equals(DISABLE)) {
            RayIntersection closestIntersection = ((HumanFaceSelectedEvent) event).getIntersection();

            if (closestIntersection != null && workingFpMode.equals(ENABLE_ADD_FP)) {
                getPrimaryFeaturePoints().removeFeaturePoint(workingFP);
                workingFP = LandmarksFactory.createLandmark(-1, closestIntersection.getPosition(), "", "");
                workingFP.setMeshVicinity(new MeshVicinity(0, workingFP.getPosition()));
                getPrimaryFeaturePoints().addFeaturePoint(workingFP);
                getControlPanel().setAddButton(true);

            } else if (closestIntersection != null && workingFpMode.equals(ENABLE_CHANGE_FP_POSITION)) {
                for (Landmark fp : getPrimaryFeaturePoints().getFeaturePoints()) {
                    if (fp.equals(selectedFeaturePoints.getFirst())) {
                        fp.setPosition(closestIntersection.getPosition());
                        fp.setMeshVicinity(new MeshVicinity(0, closestIntersection.getPosition()));
                    }
                }
            }
            renderScene();
        }
    }

    /**
     * Changes the color of the face's feature point at the given index
     * and of its weighted representation when the cursor hovers over the feature point's name.
     * The index is received as the data payload of {@code actionEvent}.
     * 
     * @param actionEvent Action event with the index of the feature point as its payload data
     * @param entered {@code true} if the cursor entered the feature point,
     *                {@code false} if the cursor left the feature point
     */
    private void hoverFeaturePoint(LoadedActionEvent actionEvent, boolean entered) {
        final int index = (int) actionEvent.getData();
        
        Landmark fp = getPrimaryFeaturePoints().getFeaturePoints().get(index);
        
        if (entered) { // entering a feature point
            getPrimaryFeaturePoints().setColor(index, FEATURE_POINT_HOVER_COLOR);
            getControlPanel().updateFpDescription(fp, true);
        } else { // leaving ordinary FP
            getPrimaryFeaturePoints().setColor(index, getPrimaryFeaturePoints().getAssignedColor(index));
            setColorsOfFeaturePoints();
            if (selectedFeaturePoints.size() == 1) {
                getControlPanel().updateFpDescription(selectedFeaturePoints.getFirst(), true);
            } else {
                getControlPanel().updateFpDescription(fp, false);
            }
        }
    }
    
    /**
     * Returns type of the feature point at the given index.
     * 
     * @param index Index of the feature point
     * @return Type of the feature point or {@code null}
     */
    private Landmark getFeaturePointOfType(int index) {
        final List<Landmark> featurePoints = getPrimaryFeaturePoints().getFeaturePoints();
        if (index < 0 || index >= featurePoints.size()) {
            return null;
        }
        return featurePoints.get(index);
    }
    
    /**
     * Adds a point to selected feature points
     * 
     * @param actionEvent LoadedActionEvent
     */
    private void selectFeaturePoint(LoadedActionEvent actionEvent) {
        final int index = (int) actionEvent.getData();
        final Landmark fp = getPrimaryFeaturePoints().getFeaturePoints().get(index);
        boolean selected = ((JToggleButton) actionEvent.getSource()).isSelected();
        if (selected) {
            selectedFeaturePoints.add(fp);
        } else {
            selectedFeaturePoints.remove(fp);
        }
    }
    
    /**
     * Updates the correct colors of all feature points
     */
    private void setColorsOfFeaturePoints() {
        List<Landmark> fps = getPrimaryFeaturePoints().getFeaturePoints();
        for (int i = 0; i < fps.size(); i++) {
            Color color;
            if (selectedFeaturePoints.contains(getFeaturePointOfType(i))) {
                color = FEATURE_POINT_HIGHLIGHT_COLOR;
            } else {
                color = getPrimaryFeaturePoints().getAssignedColor(i);
            }
            getPrimaryFeaturePoints().setColor(i, color);
        }
    }
    
    /**
     * Removes or pins selected feature points
     * 
     * @param action action to be performed (remove, pin)
     */
    private void workWithSelectedFp(String action) {
        if (selectedFeaturePoints.isEmpty()) {
            return;
        }

        for (int i = getPrimaryFeaturePoints().getFeaturePoints().size()-1; i >= 0; i--) {
            if (selectedFeaturePoints.contains(getFeaturePointOfType(i))) {
                switch(action) {
                    case "remove":
                        selectedFeaturePoints.remove(getFeaturePointOfType(i));
                        getPrimaryFace().removeCustomLandmark(getPrimaryFeaturePoints().getFeaturePoints().get(i));
                        getPrimaryFeaturePoints().removeFeaturePoint(i);
                        break;
                    case "pin":
                        pinFeaturePointToMesh(getPrimaryFeaturePoints().getFeaturePoints().get(i));
                        break;
                    default:
                        throw new IllegalArgumentException("action " + action);
                }
            }
        }
    }
         
    /**
     * Changes the distance threshold for categorizing feature points into either ON_THE_MESH or CLOSE_TO_MESH
     * 
     * @param ae ActionEvent
     */
    private void changeDistanceThreshold(ActionEvent ae) {
        double sliderVal = (Double) ((DoubleSpinner) ae.getSource()).getValue();
        getPrimaryFeaturePoints().setDistanceThreshold(sliderVal);
        getControlPanel().getFeaturePointListPanel().refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
    }
      
    /**
     * Changes the feature point's position to the closest point on mesh
     * 
     * @param fp feature point to be changed
     */
    private void pinFeaturePointToMesh(Landmark fp) {
        MeshVicinity relation = fp.getMeshVicinity();
        if (relation.getPositionType(getPrimaryFeaturePoints().getDistanceThreshold()) != MeshVicinity.Location.OFF_THE_MESH) {
            fp.setPosition(relation.nearestPoint());
            fp.setMeshVicinity(new MeshVicinity(0, relation.nearestPoint()));
        }
    }  
    
    /**
     * Saves changes to a feature point type of CUSTOM feature point
     */
    private void saveChanges() {
        String newDesc = getControlPanel().getDescriptionText();
        String newName = getControlPanel().getNameText();
        for (Landmark fp : getPrimaryFeaturePoints().getFeaturePoints()) {
            if (fp.equals(selectedFeaturePoints.getFirst())) {
                fp.setName(newName);
                fp.setDescription(newDesc);
            }
        }
        getControlPanel().updateFpDescription(selectedFeaturePoints.getFirst(), true);
        getControlPanel().getFeaturePointListPanel().refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
    }
    
    /**
     * Adds a feature point to PrimaryFeaturePoints based on the selected position and feature point type
     */
    private void addFeaturePoint(ActionEvent ae) {
        if (ae.getSource() == getControlPanel().getAddPrimaryFpButton()) {
            String fpName = (String) getControlPanel().getNewPrimaryFpList().getSelectedItem();
            Landmark fp = LandmarksFactory.createFeaturePointByName(fpName, workingFP.getPosition());
            fp.setMeshVicinity(new MeshVicinity(0, workingFP.getPosition()));
            getPrimaryFeaturePoints().addFeaturePoint(fp);
            getPrimaryFace().addCustomLandmark(fp);
        } else if (ae.getSource() == getControlPanel().getAddSecondaryFpButton()) {
            String fptName = getControlPanel().getNewSecondaryFpName().getText();
            List<Landmark> customFps = getPrimaryFace().getCustomLandmarks();
            int index = (customFps.isEmpty()) ? 0 : customFps.getLast().getType() + 1;
            Landmark fp = LandmarksFactory.createLandmark(index, workingFP.getPosition(), fptName, "");
            fp.setMeshVicinity(new MeshVicinity(0, workingFP.getPosition()));
            getPrimaryFeaturePoints().addFeaturePoint(fp);
            getPrimaryFace().addCustomLandmark(fp);
        }
        workingFpMode = DISABLE;
        getPrimaryFeaturePoints().removeFeaturePoint(workingFP);
        workingFP = null;
        getControlPanel().getFeaturePointListPanel().refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
    }
    
    /**
     * Changes the colors of standard feature points (ON_THE_MESH_COLOR, CLOSE_TO_MESH_COLOR, customFpColor) and custom feature points
 Chosen colors are based on the selected color in the pop-up JColorChooser
     * 
     * @param ae  action event
     */
    private void changeTypeColor(ActionEvent ae) {
        Color oldColor = ((JButton) ae.getSource()).getBackground();
        Color newColor = JColorChooser.showDialog((JButton) ae.getSource(), "Choose new color", oldColor);
        
        List<Color> currentColors = new ArrayList<>();
        currentColors.add(getPrimaryFeaturePoints().getOnTheMeshColor());
        currentColors.add(getPrimaryFeaturePoints().getCloseToMeshColor());
        currentColors.add(getPrimaryFeaturePoints().getOffTheMeshColor());
        currentColors.add(getPrimaryFeaturePoints().getCustomFpColor());
        currentColors.add(FEATURE_POINT_HIGHLIGHT_COLOR);
        currentColors.add(FEATURE_POINT_HOVER_COLOR);

        if (newColor == null || currentColors.contains(newColor)) {
            newColor = oldColor;
        } else if (oldColor == getPrimaryFeaturePoints().getOnTheMeshColor()) {
            getPrimaryFeaturePoints().setOnTheMeshColor(newColor);
        } else if (oldColor == getPrimaryFeaturePoints().getCloseToMeshColor()) {
            getPrimaryFeaturePoints().setCloseToMeshColor(newColor);
        } else if (oldColor == getPrimaryFeaturePoints().getOffTheMeshColor()) {
            getPrimaryFeaturePoints().setOffTheMeshColor(newColor);
        } else {
            getPrimaryFeaturePoints().setCustomFpColor(newColor);
        }
        ((JButton) ae.getSource()).setBackground(newColor);
        setColorsOfFeaturePoints();
        getControlPanel()
                .getFeaturePointListPanel()
                .refreshPanel(this, getPrimaryFeaturePoints(), selectedFeaturePoints);
    }

    private void showFeaturePoints(boolean show) {
        if (show && featurePointsVisible.equals(DISABLE)) {
            getPrimaryFeaturePoints().show(true);
            featurePointsVisible = ENABLE;
        } else if (!show && featurePointsVisible.equals(ENABLE)) {
            getPrimaryFeaturePoints().show(false);
            featurePointsVisible = DISABLE;
        }
    }

    protected java.util.List<Landmark> recognizeLandmarks(int faceSlot) {
        if (getPrimaryFace().hasLandmarks()) {
            JOptionPane.showMessageDialog(
                    getCanvas(),
                    "Currently, this function is available only if there are no feature points defined yet.");
            return Collections.emptyList();
        }

        if (workingFpMode.equals(ENABLE_ADD_FP)) {
            JOptionPane.showMessageDialog(
                    getCanvas(),
                    "Disable adding a new feature points first");
            return Collections.emptyList();
        }

        int resp = JOptionPane.showConfirmDialog(
                getCanvas(),
                "Do you want to estimate primary feature points approximately?",
                "No landmarks defined",
                JOptionPane.YES_NO_OPTION);

        if (resp == JOptionPane.NO_OPTION) {
            return Collections.emptyList();
        }

        LandmarksDetector detector = LandmarksDetector.getDetector(getCanvas());
        CanvasState state = getCanvas().getState();
        java.util.List<Landmark> result = detector.recognizeFromMultipleAngles(
                faceSlot,
                new SymmetryConfig(
                        SymmetryConfig.Method.ROBUST_POINT_CLOUD,
                        new PointSamplingConfig(UNIFORM_SPACE, 200),
                        200),
                5,
                30);
        getCanvas().setState(state);

        if (result.isEmpty()) {
            JOptionPane.showMessageDialog(getCanvas(), "Detection was not successful");
        }

        return result;
    }

    protected boolean addRecognizedLandmarks(List<Landmark> landmarks, int faceSlot) {
        if (landmarks.isEmpty()) {
            return false;
        }

        DrawableFace face = getScene().getDrawableFace(faceSlot);
        DrawableFeaturePoints fps = getScene().getDrawableFeaturePoints(faceSlot);
        landmarks.forEach(landmark -> {
            face.getHumanFace().addCustomLandmark(landmark);
            fps.addFeaturePoint(landmark);
        });

        face.getHumanFace().announceEvent(new LandmarksChangedEvent(
                face.getHumanFace(),
                face.getHumanFace().getShortName(),
                this));

        return true;
    }

    protected void saveLandmarks() {
        HumanFace face = getPrimaryFace();

        if (TaskService.INSTANCE.getPrimaryFace(getTask()).getFeaturePointsFile() != null) {
            int resp = JOptionPane.showConfirmDialog(
                    getCanvas(),
                    "<html><b>WARNING: Existing landmarks file will be overwritten!</b><br/><br/>Do you want to continue?</html>",
                    "Landmark file exists",
                    JOptionPane.YES_NO_OPTION);

            if (resp == JOptionPane.NO_OPTION) {
                return;
            }
        }

        try {
            LandmarksIO.exportFeaturePoints(face.getAllLandmarks(), face.getDirectory(), face.getShortName(), "CSV");
            JOptionPane.showMessageDialog(
                    getCanvas(),
                    String.format("<html>Landmarks saved to a new file<br/><br/><code>%s</code></html>",
                            getTask().getFaces().get(0).getFeaturePointsFile()));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(
                    getCanvas(),
                    String.format("<html>Saving has failed:<br/><br/><code>%s</code></html>", ex.getMessage()),
                    "Saving has failed",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
