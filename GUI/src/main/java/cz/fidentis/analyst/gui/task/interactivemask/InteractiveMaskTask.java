package cz.fidentis.analyst.gui.task.interactivemask;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.data.face.events.HumanFaceSelectedEvent;
import org.openide.util.Pair;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Task to handle asynchronous projection of points from SurfaceMaskPanel onto HumanFace
 * @author Mario Chromik
 */
public class InteractiveMaskTask extends SwingWorker<HumanFace, Integer> {
    private Canvas canvas;

    private ArrayList<Point> pointsToProject;
    private int panelWidth;
    private int panelHeight;

    /**
     * Parameterless constructor
     */
    public InteractiveMaskTask() {

    }

    /**
     * Constructor with 4 parmeters
     * @param canvas input canvas
     * @param pointsToProject points to project
     * @param panelWidth width of the drawing panel
     * @param panelHeight height of the drawing panel
     */
    public InteractiveMaskTask(Canvas canvas,ArrayList<Point> pointsToProject, int panelWidth, int panelHeight) {
        this.canvas = canvas;
        this.pointsToProject = pointsToProject;
        this.panelWidth = panelWidth;
        this.panelHeight = panelHeight;
    }

    /**
     * Normalizes and fits a pint from drawing panel to be projected onto face and projects it
     * @param point point to normalize and project
     * @return face if intersect else null
     */
    private HumanFace projectPoint(Point point) {
        Point normalizedPoint = new Point((int) point.getX() - (panelWidth/2) ,(int) point.getY() - (panelHeight/2));

        Pair<HumanFace, RayIntersection> closestFace = canvas.castRayThroughPixel(
                normalizedPoint.x + canvas.getWidth() / 2,
                normalizedPoint.y + canvas.getHeight() / 2);

        if (closestFace != null) {
            HumanFace face = closestFace.first();
            face.announceEvent(new HumanFaceSelectedEvent(
                    face,
                    closestFace.second(), // distance
                    "Intersect with secondary",
                    this
            ));
            return closestFace.first();
        }

        return null;
    }

    @Override
    public HumanFace doInBackground() throws Exception {
        HumanFace face = null;
        for (Point point: pointsToProject) {
            if (isCancelled()) {
                return null;
            }
            face = projectPoint(point);
        }
        return face;
    }

    @Override
    protected void done() {
        try {
            HumanFace face = get();

            if (face == null) {
                return;
            }
            face.announceEvent(new HumanFaceSelectedEvent(
                    face,
                    null, // distance
                    "Render",
                    this
            ));
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (CancellationException | InterruptedException e) {
            //Suppressing CancellationException and InterruptException
        }
    }
}
