package cz.fidentis.analyst.gui.elements.histogram.scales;

import java.util.List;
import java.util.function.Function;

/**
 * Base class for scales that apply a mapping function to values.
 *
 * @author Jakub Nezval
 */
public abstract class FunctionScale implements Scale<Double, Double> {
    private Function<Double, Double> mapFunc;

    private double domainStart = 0d;
    private double domainEnd = 10d;
    private double rangeStart = 0d;
    private double rangeEnd = 100d;

    /**
     * Creates a map function based on two points - (domainStart, rangeStart) and (domainEnd, rangeEnd).
     *
     * @param domainStart domainStart
     * @param domainEnd domainEnd
     * @param rangeStart rangeStart
     * @param rangeEnd rangeEnd
     * @return mapping function
     */
    protected abstract Function<Double, Double> createMapFunction(
            double domainStart, double domainEnd, double rangeStart, double rangeEnd
    );

    private void refreshMapFunction() {
        this.mapFunc = this.createMapFunction(this.domainStart, this.domainEnd, this.rangeStart, this.rangeEnd);
    }

    private void validateArguments(Double start, Double end) {
        if (start == null || end == null) {
            throw new IllegalArgumentException("`start` and `end` mustn't be null");
        }
        if (start.equals(end)) {
            throw new IllegalArgumentException("`start` and `end` mustn't be equal");
        }
    }

    @Override
    public Double map(Double value) {
        return this.mapFunc.apply(value);
    }

    @Override
    public void setDomain(Double start, Double end) {
        this.validateArguments(start, end);

        this.domainStart = start;
        this.domainEnd = end;

        this.refreshMapFunction();
    }

    @Override
    public void setRange(Double start, Double end) {
        this.validateArguments(start, end);

        this.rangeStart = start;
        this.rangeEnd = end;

        this.refreshMapFunction();
    }

    /**
     * {@inheritDoc}
     * Not supported by {@link FunctionScale}.
     *
     * @param values values
     * @see Scale#setDomain(Object, Object)
     * @throws UnsupportedOperationException not supported by {@link FunctionScale}
     */
    @Override
    public void setDomain(List<Double> values) {
        throw new UnsupportedOperationException("FunctionScale does not support setting domain using list");
    }

    /**
     * {@inheritDoc}
     * Not supported by {@link FunctionScale}.
     *
     * @param values values
     * @see Scale#setRange(Object, Object)
     * @throws UnsupportedOperationException not supported by {@link FunctionScale}
     */
    @Override
    public void setRange(List<Double> values) {
        throw new UnsupportedOperationException("FunctionScale does not support setting domain using list");
    }
}
