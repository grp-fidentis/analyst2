package cz.fidentis.analyst.gui.elements.histogram.scales;

import java.util.function.Function;

/**
 * Class for performing logarithmic scaling of values.
 * Uses {@link Math#log1p(double)} for the underlying calculations.
 *
 * @author Jakub Nezval
 */
public class Log1pScale extends FunctionScale {
    @Override
    protected Function<Double, Double> createMapFunction(
            double domainStart, double domainEnd, double rangeStart, double rangeEnd
    ) {
        var n1 = Math.log1p(domainStart);
        var n2 = Math.log1p(domainEnd);

        var b = (rangeStart - rangeEnd) / (n1 - n2);
        var a = rangeStart - b * n1;

        return x -> a + b * Math.log1p(x);
    }
}
