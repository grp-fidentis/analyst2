package cz.fidentis.analyst.gui.project.table;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;

/**
 * Table model for the project's table.
 * 
 * @author Matej Kovar
 */
public class ProjectTableModel extends DefaultTableModel {
    
    /**
     * Names of columns and their order
     * @author Radek Oslejsek
     */
    public enum Column {
        CHECKBOX(0),
        FACE_NAME(1),
        TASKS(2),
        PREVIEW(3),
        FILTERED_OUT(4); // hidden
        
        public final int index;
    
        /**
         * Constructor.
         * @param index column index 
         */
        Column(int index) {
            this.index = index;
        }
    }
    
    private final List<Path> pathsToFilteredFaces = new ArrayList<>();
    private Integer filteredRows = 0;
    
    /** 
     * Types of columns
     */
    private static final Class[] COLUMN_TYPES = new Class [] {
        java.lang.Boolean.class, java.lang.String.class, JComboBox.class, ImageIcon.class, java.lang.Boolean.class
    };

    /**
     * Can edit value of columns
     */
    private static final boolean[] COLUMN_CAN_EDIT = new boolean [] {
        true, false, true, false, false
    };
    
    /**
     * Names of columns
     */
    private static final Object[] COLUMN_NAMES = new Object[]{
        "", "Face name", "Open in tasks", "Photo", "Filtered out"
    };

    
    /**
     * Constructor
     */
    public ProjectTableModel() {
        super(COLUMN_NAMES, 0);
        
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return COLUMN_TYPES [columnIndex];}

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (rowIndex >= getRowCount() - filteredRows) {
            return false;
        }
        return COLUMN_CAN_EDIT [columnIndex];
    }
    
    @Override
    public void setRowCount(int rowCount) {
        if (rowCount == 0) {
            pathsToFilteredFaces.clear();
            filteredRows = 0;
            
        }
        super.setRowCount(rowCount);
    }
    
    @Override
    public void removeRow(int row) {
        String faceName = this.getValueAt(row, 1).toString();
        pathsToFilteredFaces.removeIf(path -> {
            return path
                    .toString()
                    .substring(
                            path.toString().lastIndexOf(File.separatorChar) + 1, 
                            path.toString().lastIndexOf('.'))
                    .equals(faceName);
        });
        super.removeRow(row);
    }
    
    /**
     * Returns the number of rows filtered out
     * @return the number of rows filtered out
     */
    public int getNumFilteredRows() {
        return this.filteredRows;
    }

    /**
     * Moves row to the bottom of the list (to filtered rows)
     * @param row index of row which is about to be moved/filtered
     */
    public void filterRow(int row) {
        setValueAt(true, row, Column.FILTERED_OUT.index);
        moveRow(row, row, getRowCount() - 1);
        filteredRows++;
    }
    
    /**
     * Moves row to the top of the list (to filtered rows)
     * @param row index of row which is about to be moved/filtered
     */
    public void unfilterRow(int row) {
        setValueAt(false, row, Column.FILTERED_OUT.index);
        moveRow(row, row, 0);
        filteredRows--;
    }
}
