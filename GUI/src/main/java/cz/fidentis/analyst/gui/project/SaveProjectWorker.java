package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.project.Project;
import cz.fidentis.analyst.project.ProjectService;

import javax.swing.*;

/**
 * A worker for saving  the state of task tabs into the disk.
 * 
 * @author Radek Oslejsek
 */
public class SaveProjectWorker extends SwingWorker<Boolean, Integer> {
    
    private final ProgressDialog progressDialog;
    
    /**
     * Constructor.
     * 
     * @param progressDialog Dialog window
     * @throws IllegalArgumentException if the project has not a folder assigned
     */
    public SaveProjectWorker(ProgressDialog progressDialog) {
        Project project = ProjectService.INSTANCE.getOpenedProject();
        if (!project.hasProjectDir()) {
            throw new IllegalArgumentException("project");
        }
        this.progressDialog = progressDialog;
    }
    

    @Override
    protected Boolean doInBackground() throws Exception {
        ProjectService.INSTANCE.saveProject(progress -> {
            progressDialog.setValue(progress);
            return null;
        });
        return true;
    }
    
    @Override
    protected void done() {
        progressDialog.dispose();
    }
}
