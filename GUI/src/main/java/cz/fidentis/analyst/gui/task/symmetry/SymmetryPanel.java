package cz.fidentis.analyst.gui.task.symmetry;

import cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent;
import cz.fidentis.analyst.gui.task.ControlPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.List;

/**
 * Control panel for symmetry plane.
 *
 * @author Natalia Bebjakova
 * @author Radek Oslejsek
 */
public class SymmetryPanel extends ControlPanel {

    /*
     * Actions triggered by this panel and served by the associated action listener
     */
    public static final String ACTION_COMMAND_COMPUTE = "Compute symmetry plane";
    public static final String ACTION_COMMAND_SHOW_SAMPLES = "Show/hide samples";
    public static final String ACTION_COMMAND_POINT_SAMPLING_STRENGTH = "Point sampling strength";
    public static final String ACTION_COMMAND_POINT_SAMPLING_STRATEGY = "Point sampling strategy";
    public static final String ACTION_COMMAND_UPDATE_SYMMETRY = "Update symmetry";

    public static final List<String> PREDEFINED_PROFILES = List.of(
            "Face",
            "Body - standing pose",
            "Body - sitting pose",
            "Skull"
    );
    
    public static final List<String> POINT_SAMPLING_STRATEGIES = List.of(
            "Uniform Space Sampling",
            "Uniform Surface Sampling",
            "Random Sampling",
            "Mean Curvature",
            "Gaussian Curvature",
            "Poisson disk sub sampling",
            "Poisson disk sub sampling (OpenCL)"
    );

    public static final List<String> SYMMETRY_ALGORITHMS = List.of(
            "mesh surface",
            "point cloud",
            "feature points"
    );

    /*
     * Mandatory design elements
     */
    public static final String ICON = "symmetry28x28.png";
    public static final String NAME = "Symmetry";

    public static final String HELP_URL = "https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/symmetry";


    /**
     * Creates new form SymmetryPanelNew
     */
    public SymmetryPanel(ActionListener action) {
        super(action);
        this.setName(NAME);
        initComponents();

        infoLinkButton1.setUri(URI.create(HELP_URL));

        SYMMETRY_ALGORITHMS.forEach(v -> selectableComboBox1.addItem(v, true));
        POINT_SAMPLING_STRATEGIES.forEach(v -> selectableComboBox2.addItem(v));
        PREDEFINED_PROFILES.forEach(v -> selectableComboBox3.addItem(v, true));
        resetProfile(0);

        selectableComboBox2.addActionListener(createListener(action, ACTION_COMMAND_POINT_SAMPLING_STRATEGY));
        selectableComboBox3.addActionListener((ActionEvent e) -> setProfileValues(action));

        spinSlider1.initInteger(200, 50, 500, 1);
        spinSlider2.initInteger(1000, 200, 2000, 1);

        jCheckBox1.addActionListener((ActionEvent e) ->
                action.actionPerformed(new ActionEvent( // recompute
                        e.getSource(),
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_SHOW_SAMPLES)));

        jButton2.addActionListener((ActionEvent e) ->
                action.actionPerformed(new ActionEvent( // recompute
                        e.getSource(),
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_COMPUTE)));

        spinSlider1.addSpinnerListener((ActionEvent e) ->   // show samples in 3D whenever the top slider is changed
                action.actionPerformed(new ActionEvent(
                        spinSlider1,
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_POINT_SAMPLING_STRENGTH)));

        jCheckBox2.setSelected(true); // show heatmap
        jCheckBox3.setSelected(true); // register reflected face

        jCheckBox2.addActionListener((ActionEvent e) ->
                action.actionPerformed(new ActionEvent(
                        e.getSource(),
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_UPDATE_SYMMETRY)));

        jCheckBox3.addActionListener((ActionEvent e) ->
                action.actionPerformed(new ActionEvent(
                        e.getSource(),
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_UPDATE_SYMMETRY)));

        histogramComponent1.addActionListener(createListener(action, HistogramComponent.ACTION_COMMAND_BOUNDS_MOVED));
    }

    /**
     * Returns chosen symmetry algorithm
     *
     * @return chosen symmetry algorithm
     */
    public String getAlgorithm() {
        return SYMMETRY_ALGORITHMS.get(selectableComboBox1.getSelectedIndex());
    }
    
    @Override
    public ImageIcon getIcon() {
        return getStaticIcon();
    }

    /**
     * Return the number of point samples for the fist phase
     *
     * @return the number of point samples
     */
    public int getPointSamplingStrength1() {
        return (Integer) spinSlider1.getValue();
    }

    /**
     * Return the number of point samples for the second phase
     *
     * @return the number of point samples
     */
    public int getPointSamplingStrength2() {
        return (Integer) spinSlider2.getValue();
    }

    /**
     * Return selected point sampling strategy
     *
     * @return selected point sampling strategy
     */
    public String getPointSamplingStrategy() {
        return POINT_SAMPLING_STRATEGIES.get(selectableComboBox2.getSelectedIndex());
    }

    /**
     * Returns true if su-samples are to be shown
     *
     * @return true if su-samples are to be shown
     */
    public boolean getShowSamples() {
        return jCheckBox1.isEnabled() && jCheckBox1.isSelected();
    }

    /**
     * Selects show samples.
     *
     * @param on on/off
     */
    public void setShowSamples(boolean on) {
        jCheckBox1.setSelected(on);
    }

    public boolean showHeatmap() {
        return jCheckBox2.isSelected();
    }

    public boolean registerReflectedFace() {
        return jCheckBox3.isSelected();
    }

    public HistogramComponent getHistogramComponent() {
        return histogramComponent1;
    }

    /**
     * Enables show samples.
     *
     * @param enable enable/disable
     */
    public void setEnableShowSamples(boolean enable) {
        jCheckBox1.setEnabled(enable);
        jLabel4.setEnabled(enable);
    }

    /**
     * Static implementation of the {@link #getIcon()} method.
     *
     * @return Control panel icon
     */
    public static ImageIcon getStaticIcon() {
        return new ImageIcon(SymmetryPanel.class.getClassLoader().getResource("/" + ICON));
    }

    /**
     * Enables/disables the selection for the computation of the symmetry from feature points.
     *
     * @param on {@code true} = enable
     */
    public void setComputeFromFPs(boolean on) {
        selectableComboBox1.setItem(SYMMETRY_ALGORITHMS.get(2), on);
    }

    /**
     * Turns on or off the OpenCL dependant sampling strategies
     *
     * @param on on-off value
     */
    public void setEnabledOpenCL(boolean on) {
        selectableComboBox2.setItem(POINT_SAMPLING_STRATEGIES.get(6), on);
    }

    /**
     * Updates GUI elements that display precision of the symmetry planes
     *
     * @param precision Precision of the symmetry plane
     */
    public void updatePrecision(double precision) {
        jTextField1.setText(String.format("%.2f", precision));
    }

    private void resetProfile(int profileIndex) {
        selectableComboBox3.setSelectedIndex(profileIndex);
        switch (profileIndex) {
            case 0: // facial scan
                selectableComboBox1.setSelectedIndex(0); // mesh-based symmetry
                selectableComboBox2.setSelectedIndex(0); // uniform space sampling
                spinSlider1.setValue(200); // sub-sampling 1
                spinSlider2.setValue(1000); // sub-sampling 2
                break;
            case 1: // body scan - standing
                selectableComboBox1.setSelectedIndex(0); // mesh-based symmetry
                selectableComboBox2.setSelectedIndex(0); // uniform space sampling
                spinSlider1.setValue(50); // sub-sampling 1
                spinSlider2.setValue(200); // sub-sampling 2
                break;
            case 2: // body scan - sitting
                selectableComboBox1.setSelectedIndex(1); // point-cloud-based symmetry
                selectableComboBox2.setSelectedIndex(0); // uniform space sampling
                spinSlider1.setValue(300); // sub-sampling 1
                spinSlider2.setValue(200); // sub-sampling 2
                break;
            case 3: // skull
                selectableComboBox1.setSelectedIndex(1); // point-cloud-based symmetry
                selectableComboBox2.setSelectedIndex(4); // Gaussian curvature sampling
                spinSlider1.setValue(500); // sub-sampling 1
                spinSlider2.setValue(400); // sub-sampling 2
                break;
        }
    }

    /**
     * Sets optimal settings (method, sampling strategy and num of sampling points) for selected predefined profile.
     *
     */
    private void setProfileValues(ActionListener action) {
        resetProfile(selectableComboBox3.getSelectedIndex());
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        spinSlider1 = new cz.fidentis.analyst.gui.elements.SpinSlider();
        jLabel3 = new javax.swing.JLabel();
        spinSlider2 = new cz.fidentis.analyst.gui.elements.SpinSlider();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        selectableComboBox2 = new cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox();
        jLabel10 = new javax.swing.JLabel();
        selectableComboBox1 = new cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox();
        jPanel2 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        selectableComboBox3 = new cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox();
        jPanel4 = new javax.swing.JPanel();
        infoLinkButton1 = new cz.fidentis.analyst.gui.elements.InfoLinkButton();
        histogramComponent1 = new cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent();

        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 1, 15))); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jPanel1.border.title_1"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jLabel1.text_1")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel9, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jLabel9.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jLabel3.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jCheckBox1, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jCheckBox1.text")); // NOI18N
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jLabel4.text")); // NOI18N

        selectableComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectableComboBox2ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jLabel10, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jLabel10.text")); // NOI18N

        selectableComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectableComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(selectableComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1)
                    .addComponent(spinSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinSlider2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(selectableComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(selectableComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(selectableComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(spinSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel1)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spinSlider2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel3)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox1)
                    .addComponent(jLabel4))
                .addGap(17, 17, 17))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jPanel2.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12))); // NOI18N

        jTextField1.setText(org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jTextField1.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jLabel2.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jCheckBox2, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jCheckBox2.text")); // NOI18N
        jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jCheckBox3, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jCheckBox3.text")); // NOI18N
        jCheckBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jCheckBox2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox3)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jPanel3.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12))); // NOI18N

        jButton2.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jButton2.text")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        selectableComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectableComboBox3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selectableComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(selectableComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jPanel4.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 0, 15))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(infoLinkButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(infoLinkButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(histogramComponent1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(histogramComponent1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        jPanel3.getAccessibleContext().setAccessibleName(org.openide.util.NbBundle.getMessage(SymmetryPanel.class, "SymmetryPanel.jPanel3.AccessibleContext.accessibleName")); // NOI18N
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void selectableComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectableComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_selectableComboBox1ActionPerformed

    private void selectableComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectableComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_selectableComboBox2ActionPerformed

    private void selectableComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectableComboBox3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_selectableComboBox3ActionPerformed

    private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox2ActionPerformed

    private void jCheckBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox3ActionPerformed

    private boolean profileChanged;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent histogramComponent1;
    private cz.fidentis.analyst.gui.elements.InfoLinkButton infoLinkButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField jTextField1;
    private cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox selectableComboBox1;
    private cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox selectableComboBox2;
    private cz.fidentis.analyst.gui.elements.cbox.SelectableComboBox selectableComboBox3;
    private cz.fidentis.analyst.gui.elements.SpinSlider spinSlider1;
    private cz.fidentis.analyst.gui.elements.SpinSlider spinSlider2;
    // End of variables declaration//GEN-END:variables
}
