package cz.fidentis.analyst.gui.app.tools.opencl;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionServices;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.memory.BufferFactory;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLConfig;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLServices;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Benchmark comparing performance between ray-casting implementations on CPU and GPU.
 *
 * @author Marek Horský
 */
public class RayCastingBenchmark {
    private static final int COORDINATE_BOUND = 100;
    private static final int ITERATIONS = 100;
    private static final int RAY_COUNT = 100000;
    private static final int ITERATION_OFFSET = 5; // First n iterations are ignored to account for power management OS scheduling
    private static final String PATH_MAIN = "C:\\Users\\marek\\Desktop\\FidentisFaces\\problem-scan\\00297_01_ECA.obj";
    private static final Random RANDOM = new Random(0);
    private static final RayIntersectionOpenCLConfig CONFIG = new RayIntersectionOpenCLConfig(MeshTriangle.Smoothing.NONE, false);

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        MeshModel model = BenchmarkUtils.getModel(PATH_MAIN);
        if (!OpenCLServices.isOpenCLAvailable()) {
            System.out.println("OpenCL could not find supported platform. Benchmark terminated");
            return;
        }
        BenchmarkUtils.printGPU();
        long timeGPU = rayCastUsingOpenCL(model.getFacets(), ITERATIONS, RAY_COUNT);
        //long timeCPU = rayCastUsingCPU(model.getFacets(), ITERATIONS, RAY_COUNT);
        BenchmarkUtils.printComparison(0, timeGPU, ITERATIONS);
    }

    /**
     * @param facets
     * @param iterations
     * @param rayCount
     * @return
     */
    public static long rayCastUsingOpenCL(List<MeshFacet> facets, int iterations, int rayCount) {
        CLContext clContext = OpenCLServices.createContext();
        OctreeOpenCL octree = OctreeOpenCL.create(clContext);
        octree.build(facets);
        WriteBufferGPU<Point3d> rayOrigins = BufferFactory.getVoxelPointBuffer(clContext);
        WriteBufferGPU<Vector3d> rayDirections = BufferFactory.getVoxelVectorBuffer(clContext);
        RayIntersectionOpenCLServices rayIntersectionServices = new RayIntersectionOpenCLServices(clContext, CONFIG);
        long timeGPU = 0;
        for (int i = 0; i < iterations + ITERATION_OFFSET; i++) {
            rayOrigins.putAll(getRandomPoints3D(rayCount));
            rayDirections.putAll(List.of(getRandomVector3D()));
            long time = System.nanoTime();
            rayIntersectionServices.computeClosestPoints(octree, rayOrigins, rayDirections);
            timeGPU += i < ITERATION_OFFSET ? 0 : System.nanoTime() - time;
        }
        rayOrigins.release();
        rayDirections.release();
        rayIntersectionServices.release();
        octree.release();
        OpenCLServices.release(clContext);
        return timeGPU;
    }

    /**
     * @param facets
     * @param iterations
     * @param rayCount
     * @return
     */
    public static long rayCastUsingCPU(List<MeshFacet> facets, int iterations, int rayCount) {
        Octree octree = Octree.create(facets);
        long timeCPU = 0;
        for (int i = 0; i < iterations + ITERATION_OFFSET; i++) {
            List<Point3d> originPoints = getRandomPoints3D(rayCount);
            Vector3d direction = getRandomVector3D();
            long time = System.nanoTime();
            computeSortedOnCPU(originPoints, octree, direction, MeshTriangle.Smoothing.NONE);
            timeCPU += i < ITERATION_OFFSET ? 0 : System.nanoTime() - time;
        }
        return timeCPU;
    }

    private static Set<RayIntersection> computeSortedOnCPU(List<Point3d> originPoints, Octree octree, Vector3d direction, MeshTriangle.Smoothing smoothing) {
        return originPoints.parallelStream()
                .map(origin -> RayIntersectionServices.computeClosest(
                        List.of(octree),
                        new RayIntersectionConfig(new Ray(origin, direction), smoothing, false)))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    private static List<Point3d> getRandomPoints3D(int count) {
        return Stream.generate(RayCastingBenchmark::getRandomPoint3D)
                .limit(count)
                .toList();
    }

    private static Vector3d getRandomVector3D() {
        return new Vector3d(
                RANDOM.nextDouble(-COORDINATE_BOUND, COORDINATE_BOUND),
                RANDOM.nextDouble(-COORDINATE_BOUND, COORDINATE_BOUND),
                RANDOM.nextDouble(-COORDINATE_BOUND, COORDINATE_BOUND));
    }

    private static Point3d getRandomPoint3D() {
        return new Point3d(
                RANDOM.nextDouble(-COORDINATE_BOUND, COORDINATE_BOUND),
                RANDOM.nextDouble(-COORDINATE_BOUND, COORDINATE_BOUND),
                RANDOM.nextDouble(-COORDINATE_BOUND, COORDINATE_BOUND));
    }
}
