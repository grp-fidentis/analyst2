package cz.fidentis.analyst.gui.app.tools;

    import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Radek Oslejsek
 */
public class DatasetStats {

    private static final String DATA_DIR = "../analyst-data-antropologie/_ECA";
    private static final int MAX_SAMPLES = 1000;

    /**
     * Main method
     * @param args args
     * @throws IOException exception
     */
    public static void main(String[] args) throws IOException {
        List<Path> faces = Files.list(new File(DATA_DIR).toPath())
                .filter(f -> f.toString().endsWith(".obj"))
                .sorted()
                .limit(MAX_SAMPLES)
                .toList();

        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        long sum = 0;
        List<Long> list = new ArrayList<>();
        for (int i = 0; i < faces.size(); i++) {
            HumanFace face = FaceService.INSTANCE.loadTemporaryInMemoryFace(faces.get(i).toFile());
            long n = face.getMeshModel().getNumVertices();
            //if (n > 100000) {
                System.out.println(face.getShortName());
            //}
            min = Math.min(min, n);
            max = Math.max(max, n);
            sum += n;
            list.add(n);
        }

        list.sort(Long::compareTo);

        System.out.println("MIN: " + min);
        System.out.println("MAX: " + max);
        System.out.println("AVG: " + sum/faces.size());
        System.out.println("MED: " + list.get(list.size()/2));
    }
}
