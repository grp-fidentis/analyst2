package cz.fidentis.analyst.gui.elements;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.drawables.DrawableCuttingPlane;
import cz.fidentis.analyst.engines.face.FaceCuttingServices;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.gui.task.symmetry.CuttingPlanesAction;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.Plane;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.awt.event.*;

/**
 * A GUI element grouping three main elements for manipulation with a single cutting plane.
 *   - spinSlider for shifting the plane
 *   - checkBox for controlling visibility of plane
 *   - comboBox for switching between orthogonal and symmetry planes
 * Places listeners on the elements and performs operations with stored cutting
 * plane as per user's requests.
 * 
 * @author Samuel Smoleniak
 * @author Peter Conga
 */
public class PlaneManipulationPanel extends JPanel {
    
    private Canvas canvas;
    private HumanFace face;
    
    /**
     * Cutting planes
     */
    private DrawableCuttingPlane currentCuttingPlane;
    private DrawableCuttingPlane bboxCuttingPlane;
    private DrawableCuttingPlane symmetryCuttingPlane;
    
    /**
     * GUI elements
     */
    private SpinSlider spinSlider;
    private JCheckBox visibilityCheckBox;
    private JComboBox planeTypeComboBox;
    
    /**
     * ComboBox options
     */
    private static final String CUTTING_PLANE_FROM_SYMMETRY = "Symmetry plane (primary face)";
    private static final String CUTTING_PLANE_FROM_BBOX = "Plane from bounding box";
    
    /**
     * Constructor
     */
    public PlaneManipulationPanel() {
        initComponents();
        setBackground(Color.LIGHT_GRAY);
        setBorder(new EmptyBorder(5, 5, 5, 5));
        this.addMouseListener(mouseListener);
        spinSlider.getSpinner().setMinimumSize(new Dimension(45, 25));
        spinSlider.getSpinner().setPreferredSize(new Dimension(48, 25));
        spinSlider.getSpinner().setMaximumSize(new Dimension(50, 25));
    }
    
    public DrawableCuttingPlane getCurrentPlane() {
        return currentCuttingPlane;
    }
    
    public Vector3d getPlaneOrientation() {
        return bboxCuttingPlane.getNonShiftedPlane().getNormal();
    }
    
    /**
     * Returns string with information about currently used plane type
     * @return current plane type string
     */
    public String getCurrentPlaneType() {
        if (currentCuttingPlane == bboxCuttingPlane) {
            return "from bounding box";
        }
        return "from symmetry (primary face)";
    }
    
    /**
     * Setter for canvas and face
     * @param canvas canvas for storing and showing cutting planes
     * @param face human face for plane computation
     */
    public void setCanvasAndFace(Canvas canvas, HumanFace face) {
        this.canvas = canvas;
        this.face = face;
    }
    
    /**
     * Mouse listener for highlighting a cutting plane, when hovering over this panel.
     */
    private MouseListener mouseListener = new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
            setBackground(Color.ORANGE);
            if (currentCuttingPlane != null) {
                currentCuttingPlane.show(true);
                currentCuttingPlane.setColor(Color.ORANGE);
                canvas.renderScene();
                firePropertyChange("highlight", null, PlaneManipulationPanel.this);
            }
        }
        @Override
        public void mouseExited(MouseEvent e) {
            setBackground(Color.LIGHT_GRAY);
            if (currentCuttingPlane != null) {
                if (!isCheckBoxSelected()) {
                    currentCuttingPlane.show(false);
                }
                currentCuttingPlane.setColor(Color.LIGHT_GRAY);
                canvas.renderScene();
                firePropertyChange("highlight off", null, PlaneManipulationPanel.this);
            }
        }
        @Override
        public void mouseReleased(MouseEvent e) {
            firePropertyChange("mouse released", null, bboxCuttingPlane.getNonShiftedPlane().getNormal());
        }
    };
    
    /**
     * Slider listener for plane shifting.
     */
    private ChangeListener sliderChangeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            if (currentCuttingPlane != null) {
                currentCuttingPlane.shift((Double) spinSlider.getValue() * getMaxPlaneShift());
                visibilityCheckBox.setSelected(true);
                // update curve rendering panel
                firePropertyChange("shift", null, bboxCuttingPlane.getNonShiftedPlane().getNormal());
            }
        }
    };
    
    /**
     * Checkbox listener for showing/hiding plane.
     */
    private ItemListener checkBoxSelectionListener = new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
            if (currentCuttingPlane != null) {
                currentCuttingPlane.show(isCheckBoxSelected());
                // update curve rendering panel
                firePropertyChange("visibility", null, bboxCuttingPlane.getNonShiftedPlane().getNormal());
            }
        }
    };
    
    /**
     * ComboBox listener for switching between plane types.
     */
    private ActionListener comboBoxActionListener = new ActionListener () {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (planeTypeComboBox.getSelectedItem() == CUTTING_PLANE_FROM_SYMMETRY) {
                // show message if no symmetry plane computed
                if (!face.hasSymmetryPlane()) {
                    JOptionPane.showMessageDialog(
                        canvas.getParent(), 
                        "Compute symmetry planes first.",
                        "No symmetry plane", 
                        JOptionPane.INFORMATION_MESSAGE
                    );
                    planeTypeComboBox.setSelectedItem(CUTTING_PLANE_FROM_BBOX);
                    return;
                }
                // create DrawableCuttingPlane from computed symmetry plane if not created already
                if (symmetryCuttingPlane == null) {
                    createSymmetryCuttingPlane();
                }
                bboxCuttingPlane.show(false);
                currentCuttingPlane = symmetryCuttingPlane;
                
            } else { // CUTTING_PLANE_FROM_BBOX
                if (symmetryCuttingPlane != null) {
                    symmetryCuttingPlane.show(false);
                }
                currentCuttingPlane = bboxCuttingPlane;
            }
            
            currentCuttingPlane.show(isCheckBoxSelected());
            // set slider according to shift of current plane
            double shift = currentCuttingPlane.getShift();
            spinSlider.setValue(shift / getMaxPlaneShift());
            canvas.renderScene();
        }
    };
    
    /**
     * Creates DrawableCuttingPlane from BoundingBox of face with given orthogonal normal
     * @param direction normal vector of new DrawableCuttingPlane
     */
    public void createBboxCuttingPlane(Vector3d direction) {
        Plane plane = FaceCuttingServices.fromBoundingBox(face, direction);

        bboxCuttingPlane = new DrawableCuttingPlane(plane, face.getBoundingBox(), true);
        bboxCuttingPlane.setTransparency(0.5f);
        synchronized (this) {
            int slot = canvas.getScene().getFreeSlotForCuttingPlane();
            canvas.getScene().setDrawableCuttingPlane(slot, bboxCuttingPlane);
        }
        currentCuttingPlane = bboxCuttingPlane;
        
        // set initial values in case that human face was transformed and planes are being recomputed
        planeTypeComboBox.setSelectedItem(CUTTING_PLANE_FROM_BBOX);
        symmetryCuttingPlane = null;
    }
    
    /**
     * Removes cutting plane from scene
     */
    public void deleteBboxCuttingPlane() {
        int slot = -1;
        for(DrawableCuttingPlane dcp : canvas.getScene().getCuttingPlanes()){
            slot++;
            if (dcp.equals(this.bboxCuttingPlane)) {
                break;
            }
        }
        if (slot == -1) {
            return;
        }
        canvas.getScene().setDrawableCuttingPlane(slot, null);
    }
    
    /**
     * Compares this.face to parameter face
     * @param face face to compare this.face to
     * @return true when the two faces are equal, false otherwise
     */
    public boolean isFaceSame(HumanFace face) {
        return this.face.equals(face);
    }

    /**
     * Creates cutting plane from symmetry plane computed in symmetry panel.
     */
    protected void createSymmetryCuttingPlane() {
        Plane rotatedSymmetryPlane = FaceCuttingServices.fromSymmetryPlane(face, bboxCuttingPlane.getNonShiftedPlane().getNormal());

        // create symmetry cutting plane
        symmetryCuttingPlane = new DrawableCuttingPlane(rotatedSymmetryPlane, face.getBoundingBox(),false);
        symmetryCuttingPlane.setTransparency(0.5f);
        synchronized (this) {
            int symmetryPlaneSlot = canvas.getScene().getFreeSlotForCuttingPlane();
            canvas.getScene().setDrawableCuttingPlane(symmetryPlaneSlot, symmetryCuttingPlane);
        }
    }

    /**
     * Returns a number describing how far can cutting plane be shifted from its 
     * origin in direction of its normal, so that it does not exceed bounding box of face.
     * @return max shift value of plane
     */
    protected double getMaxPlaneShift() {
        Vector3d normal = bboxCuttingPlane.getNonShiftedPlane().getNormal();
        Box bbox = face.getBoundingBox();
        Point3d midPoint = bbox.midPoint();
        
        if (normal.equals(CuttingPlanesAction.DIRECTION_VERTICAL)) {
            return Math.abs(bbox.maxPoint().x - midPoint.x);
        } else if (normal.equals(CuttingPlanesAction.DIRECTION_HORIZONTAL)) {
            return Math.abs(bbox.maxPoint().y - midPoint.y);
        } else {
            return Math.abs(bbox.maxPoint().z - midPoint.z);
        }
    }
    
    protected final void initComponents() {
        setLayout(new BorderLayout());

        initSpinSlider();
        add(spinSlider, BorderLayout.EAST);
        initComboBox();
        add(planeTypeComboBox, BorderLayout.SOUTH);
        initCheckBox();
        add(visibilityCheckBox, BorderLayout.WEST);
    }
    
    private void initSpinSlider() {
        spinSlider = new SpinSlider();
        spinSlider.setSliderSize(100);
        spinSlider.initDouble(0.0, -1, 1, 2);
        spinSlider.getSpinner().addMouseListener(mouseListener);
        spinSlider.getSlider().addMouseListener(mouseListener);
        spinSlider.getSlider().addChangeListener(sliderChangeListener);
    }

    private void initCheckBox() {
        visibilityCheckBox = new JCheckBox();
        visibilityCheckBox.setText("Show in 3D");
        visibilityCheckBox.addMouseListener(mouseListener);
        visibilityCheckBox.addItemListener(checkBoxSelectionListener);
    }
    
    private void initComboBox() {
        planeTypeComboBox = new JComboBox();
        planeTypeComboBox.addItem(CUTTING_PLANE_FROM_BBOX);
        planeTypeComboBox.addItem(CUTTING_PLANE_FROM_SYMMETRY);
        planeTypeComboBox.setSelectedItem(CUTTING_PLANE_FROM_BBOX);
        planeTypeComboBox.addMouseListener(mouseListener);
        planeTypeComboBox.addActionListener(comboBoxActionListener);
    }
    
    public boolean isCheckBoxSelected() {
        return visibilityCheckBox.isSelected();
    }
    
    /**
     * Marks checkbox as selected.
     */
    public void selectCheckBox() {
        visibilityCheckBox.setSelected(true);
    }
    
    public JSlider getSlider() {
        return spinSlider.getSlider();
    }
}
