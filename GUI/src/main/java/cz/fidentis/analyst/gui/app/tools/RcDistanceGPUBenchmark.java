package cz.fidentis.analyst.gui.app.tools;

import com.jogamp.opengl.*;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.measurement.FacetDistances;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceVisitor;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for testing the performance of GPU ray-casting vs CPU ray-casting.
 *
 * @author Pavol Kycina
 */
public class RcDistanceGPUBenchmark {

    // directory with .obj files
    //private static final String DATA_DIR = "/home/astetyne/Desktop/bakalarka/analyst-data-antropologie-vyber-500/_ECA/";
    private static final String DATA_DIR = "../analyst-data-antropologie/_ECA/";

    private static final int LOAD_FACES = 10;

    private static final int CROP_ICP = 1;

    /** this tells how much the GPU calculated distance can be different from CPU distance (note GPU floats vs CPU doubles)*/
    private static final double ALLOWED_DISTANCE_ERROR = 0.01;

    private static final boolean SUMMARY_PRINTS = true;

    /**
     * Benchmark for testing acceleration of mesh distance computation on GPU vs CPU.
     * <p>
     * To run performance nvidia GPU on multi-GPU linux system, you can set these environmental variables:
     * __NV_PRIME_RENDER_OFFLOAD=1; __GLX_VENDOR_LIBRARY_NAME=nvidia
     * </p>
     * @param args paths to the .obj files, which will be used as secondary faces, first will be used as a main (primary)
     * @throws IOException when testing models cannot be loaded
     */
    public static void main(String[] args) throws IOException {
        if(SUMMARY_PRINTS) {
            System.out.println("*** Starting benchmark ***");
        }

        List<Path> models = loadFaces();

        GLAutoDrawable drawable = createDummyGLContext();
        pairDistancePerformanceTest(drawable.getContext(), models);
        drawable.destroy();
    }

    static void pairDistancePerformanceTest(GLContext context, List<Path> models) throws IOException {
        MeasuredTimes cpuTimes = new MeasuredTimes();
        MeasuredTimes gpuTimes = new MeasuredTimes();

        System.out.println("build-cpu;visit-cpu;build-gpu;visit-gpu");

        for (int i = 0; i < models.size(); i++) {
            Path faceFile1 = models.get(i);
            HumanFace face1 = FaceService.INSTANCE.loadTemporaryInMemoryFace(faceFile1.toFile());
            IcpConfig icpConfig = new IcpConfig(
                    face1.getMeshModel(),
                    100,
                    false,
                    0.05,
                    new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 0),
                    CROP_ICP);

            for (int j = i + 1; j < models.size(); j++) {
                Path faceFile2 = models.get(j);
                HumanFace face2 = FaceService.INSTANCE.loadTemporaryInMemoryFace(faceFile2.toFile());

                FaceRegistrationServices.alignMeshes(face2, icpConfig);

                // perform mutual distance measurement
                MeshDistances dist1CPU = registerAndMeasureDistance(MeshDistanceConfig.Method.RAY_CASTING, context, face1, face2, cpuTimes);
                MeshDistances dist1GPU = registerAndMeasureDistance(MeshDistanceConfig.Method.RAY_CASTING_GPU, context, face1, face2, gpuTimes);

                cpuTimes.printLastToCsv(false);
                gpuTimes.printLastToCsv(true);

                if(SUMMARY_PRINTS) {
                    compare(dist1CPU, dist1GPU, faceFile1, faceFile2);
                }

                MeshDistances dist2CPU = registerAndMeasureDistance(MeshDistanceConfig.Method.RAY_CASTING, context, face2, face1, cpuTimes);
                MeshDistances dist2GPU = registerAndMeasureDistance(MeshDistanceConfig.Method.RAY_CASTING_GPU, context, face2, face1, gpuTimes);

                cpuTimes.printLastToCsv(false);
                gpuTimes.printLastToCsv(true);

                if(SUMMARY_PRINTS) {
                    compare(dist2CPU, dist2GPU, faceFile2, faceFile1);
                }
            }
        }

        float buildSpeedup = cpuTimes.getTotalBuildTimeMs() / (float) gpuTimes.getTotalBuildTimeMs();
        float visitSpeedup = cpuTimes.getTotalVisitTimeMs() / (float) gpuTimes.getTotalVisitTimeMs();
        float totalSpeedup = cpuTimes.getTotalTime() / (float) gpuTimes.getTotalTime();

        if(SUMMARY_PRINTS) {
            System.out.println("\n**********************************************");
            System.out.println("Times: (accel structure build) (visit) (total)");
            System.out.println("CPU             " + cpuTimes.getTotalBuildTimeMs() + "      " + cpuTimes.getTotalVisitTimeMs() + "     " + cpuTimes.getTotalTime());
            System.out.println("GPU             " + gpuTimes.getTotalBuildTimeMs() + "      " + gpuTimes.getTotalVisitTimeMs() + "     " + gpuTimes.getTotalTime());
            System.out.println("speed-up        " + buildSpeedup + "x     " + visitSpeedup + "x     " + totalSpeedup + "x");
            System.out.println("**********************************************\n");
        }
    }

    static MeshDistances registerAndMeasureDistance(MeshDistanceConfig.Method method, GLContext context, HumanFace primaryFace, HumanFace secondaryFace, MeasuredTimes times) {
        long buildStart = System.nanoTime();
        MeshDistanceVisitor distVisitor = new MeshDistanceConfig(method, primaryFace.getMeshModel().getFacets(), context, true, true).getVisitor();
        times.buildTimes.add(System.nanoTime() - buildStart);

        long visitTimeStart = System.nanoTime();
        secondaryFace.getMeshModel().compute(distVisitor);
        times.visitTimes.add(System.nanoTime() - visitTimeStart);
        distVisitor.dispose();

        return distVisitor.getDistancesOfVisitedFacets();
    }

    static GLAutoDrawable createDummyGLContext() {
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLAutoDrawable drawable = GLDrawableFactory.getFactory(glProfile).createOffscreenAutoDrawable(null, glCapabilities, null, 1, 1);
        drawable.display(); // initialize context
        return drawable;
    }

    static List<Path> loadFaces() throws IOException {
        List<Path> faces = Files.list(new File(DATA_DIR).toPath())
                .filter(f -> f.toString().endsWith(".obj"))
                .sorted()
                .limit(LOAD_FACES)
                .toList();

        if(SUMMARY_PRINTS) {
            System.out.println("found " + faces.size() + " faces\n");
        }
        return faces;
    }

    static void compare(MeshDistances cpuDistances, MeshDistances gpuDistances, Path primaryFaceFile, Path secondaryFaceFile) {
        for(MeshFacet facet : gpuDistances.distancesAsMap().keySet()) {
            FacetDistances cpuFD = cpuDistances.getFacetMeasurement(facet);
            FacetDistances gpuFD = gpuDistances.getFacetMeasurement(facet);

            int matches = countMatches(cpuFD, gpuFD);
            int raysCount = cpuFD.size();
            float percent = (int)((1000f * matches) / raysCount) / 10f;

            System.out.println(secondaryFaceFile.getFileName() + "->" + primaryFaceFile.getFileName() +
                    " distances match: " + matches + " / " + raysCount + "  " + percent + "%");
        }
    }

    static int countMatches(FacetDistances distCPU, FacetDistances distGPU) {
        int matches = 0;

        for (int i = 0; i < distCPU.size(); i++) {
            double dGPU = distCPU.get(i).getDistance();
            double dCPU = distGPU.get(i).getDistance();

            // round the numbers because GPU uses floats
            if((Double.isInfinite(dGPU) && Double.isInfinite(dCPU)) || Math.abs(dGPU - dCPU) < ALLOWED_DISTANCE_ERROR) {
                matches++;
            }else {
                // uncomment for debugging
                //System.out.println("NOT MATCHING: (gpu) " + dGPU + " (cpu) " + dCPU + " i: " + i);
            }
        }
        return matches;
    }

    /**
     * @author Pavol Kycina
     */
    private static class MeasuredTimes {

        // these times are stored in nanosecond precision
        private List<Long> buildTimes = new ArrayList<>();
        private List<Long> visitTimes = new ArrayList<>();

        public long getTotalBuildTimeMs() {
            return buildTimes.stream().reduce(Long::sum).get() / 1000_000; // divide by 1000_000 to get a millis
        }

        public long getTotalVisitTimeMs() {
            return visitTimes.stream().reduce(Long::sum).get() / 1000_000; // divide by 1000_000 to get a millis
        }

        public long getTotalTime() {
            return getTotalBuildTimeMs() + getTotalVisitTimeMs();
        }

        /** Prints with microsecond precision */
        public void printLastToCsv(boolean ending) {
            System.out.print(buildTimes.get(buildTimes.size() - 1) / 1000 + ";" + visitTimes.get(visitTimes.size() - 1) / 1000 + (ending ? "\n" : ";"));
        }
    }
}
