package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.gui.task.ControlPanel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;

/**
 * Default action listener (controller) used to connect specific control panel 
 * of the project window with the project itself.
 * 
 * @author Radek Oslejsek
 * @param <T> the type of control panel
 */
public abstract class ProjectPanelAction<T extends ControlPanel> extends AbstractAction {
    
    /*
     * Top component - the space delimited for placing control panels
     */
    private final JTabbedPane topControlPane;
    
    /**
     * Control panel inside the topControlPane
     */
    private T controlPanel;
    
    /**
     * Project panel
     */
    private final ProjectPanel projectPanel;
    
    /**
     * Constructor. 
     * 
     * @param projectPanel Project panel
     * @param topControlPane A top component when a new control panel is placed
     * @throws IllegalArgumentException if some param is missing
     */
    public ProjectPanelAction(ProjectPanel projectPanel, JTabbedPane topControlPane) {
        if (projectPanel == null) {
            throw new IllegalArgumentException("projectPanel");
        }
        if (topControlPane == null) {
            throw new IllegalArgumentException("topControlPane");
        }
        this.projectPanel = projectPanel;
        this.topControlPane = topControlPane;
        projectPanel.addActionListener(this);
    }
    
    @Override
    public abstract void actionPerformed(ActionEvent ae);
    
    /**
     * Popups (shows) this control panel.
     */
    public void popup() {
        topControlPane.setSelectedComponent(getControlPanel()); 
    }
    
    /**
     * Returns the project panel, i.e., the left-hand side of the window.
     * 
     * @return the project panel
     */
    protected ProjectPanel getProjectPanel() {
        return this.projectPanel;
    }
    
    /**
     * Sets (replaces) the control panel.
     * 
     * @param controlPanel a control panel
     */
    protected void setControlPanel(T controlPanel) {
        if (controlPanel == null) {
            throw new IllegalArgumentException("controlPanel");
        }
        this.controlPanel = controlPanel;
        topControlPane.addTab(getControlPanel().getName(), getControlPanel().getIcon(), getControlPanel());
    }
    
    protected T getControlPanel() {
        return this.controlPanel;
    }
    
    /**
     * Provide a code that is run when the control panel is focused (popped up, shown) 
     * and unfocused (closed or hidden by another control panel)
     * 
     * @param visible A code to be run when the control panel is focused
     * @param hidden A code to be run when the control panel hides
     */
    protected void setShowHideCode(Runnable visible, Runnable hidden) {
        topControlPane.addChangeListener(e -> {
            if (((JTabbedPane) e.getSource()).getSelectedComponent().equals(controlPanel)) { 
                visible.run(); // If the panel is focused
            } else { 
                hidden.run(); // If another panel is focused
            }
        });
    }
    
    /**
     * The generic code for showing/hiding the control panel
     * 
     * @param ae Action event
     */
    protected void hideShowPanelActionPerformed(ActionEvent ae, ControlPanel controlPanel) {
        if (((JToggleButton) ae.getSource()).isSelected()) {
            topControlPane.addTab(controlPanel.getName(), controlPanel.getIcon(), controlPanel);
            topControlPane.setSelectedComponent(controlPanel); // focus
        } else {
            topControlPane.remove(controlPanel);
        }
    }
    
}
