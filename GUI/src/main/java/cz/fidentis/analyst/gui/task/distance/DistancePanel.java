package cz.fidentis.analyst.gui.task.distance;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.gui.elements.FpListWeightsPanel;
import cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent;
import cz.fidentis.analyst.gui.task.ControlPanel;
import cz.fidentis.analyst.gui.task.ControlPanelAction;

import javax.swing.*;
import java.net.URI;
import java.util.DoubleSummaryStatistics;
import java.util.Set;

/**
 * Control panel for Hausdorff distance.
 * 
 * @author Radek Oslejsek
 * @author Daniel Schramm
 * @author Jakub Nezval
 */
public class DistancePanel extends ControlPanel {
    
    /*
     * Mandatory design elements
     */
    public static final String ICON = "distance28x28.png";
    public static final String NAME = "Distance";
    
    /*
     * Configuration of panel-specific GUI elements
     */
    public static final String SEL_STRATEGY_POINT_TO_POINT = "Point to point";
    public static final String SEL_STRATEGY_POINT_TO_TRIANGLE = "Point to triangle";
    public static final String SEL_STRATEGY_RAY_CASTING = "Ray-casting";
    public static final String SEL_STRATEGY_RAY_CASTING_GPU = "Ray-casting GPU";
    
    public static final String SEL_SHOW_STD_DISTANCE = "Standard";
    public static final String SEL_SHOW_WEIGHTED_DISTANCE = "Weighted";
    public static final String SEL_SHOW_NO_HEATMAP = "None (hide)";
    
    public static final String SEL_RELATIVE_DISTANCE = "Relative distance";
    public static final String SEL_ABSOLUTE_DISTANCE = "Absolute distance";
    
    public static final String SEL_SHOW_SELECTED_SPHERES = "Show selected";
    public static final String SEL_SHOW_ALL_SPHERES = "Show all";
    public static final String SEL_SHOW_NO_SPHERES = "Hide all";
    
    /*
     * Actions triggered by this panel and served by the associated action listener 
     */
    public static final String ACTION_COMMAND_SET_DISTANCE_STRATEGY = "p2p-p2t vicinity";
    public static final String ACTION_COMMAND_RELATIVE_ABSOLUTE_DIST = "switch abosulte-relative distance";
    public static final String ACTION_COMMAND_SET_DISPLAYED_HEATMAP = "standard-weighted hausdorff distance";
    public static final String ACTION_COMMAND_SHOW_HIDE_WEIGHTED_FPOINTS = "show-hide weighted feature points";
    public static final String ACTION_COMMAND_FEATURE_POINT_SELECT_ALL = "select all feature points";
    public static final String ACTION_COMMAND_FEATURE_POINT_SELECT_NONE = "deselect all feature points";
    public static final String ACTION_COMMAND_FEATURE_POINT_SELECT_AUTO = "Select significant feature points";
    public static final String ACTION_COMMAND_HD_AUTO_CROP = "Crop";

    public static final String ACTION_COMMAND_EXPORT_DISTANCE = "Export distance";
    public static final String ACTION_COMMAND_EXPORT_WEIGHTED_DISTANCE = "Export weighted distance";
    
    public static final String HELP_URL = "https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis/distance";
    
    /**
     * Constructor.
     * 
     * @param action Action listener
     * @param drFeaturePoints Feature points that can be used to calculate the weighted Hausdorff distance
     * @param selectedFPs Set of feature point types which are initially selected to be used to calculate
     *                    the weighted Hausdorff distance
     */
    public DistancePanel(ControlPanelAction<DistancePanel> action, DrawableFeaturePoints drFeaturePoints, Set<Landmark> selectedFPs) {
        super(action);
        this.setName(NAME);
        initComponents();
        
        infoLinkButton1.setUri(URI.create(HELP_URL));
        
        fpListWeightsPanel1.initComponents(action,  drFeaturePoints, selectedFPs);

        jComboBox1.setModel(new DefaultComboBoxModel<>(new String[] { 
            SEL_SHOW_STD_DISTANCE, 
            SEL_SHOW_WEIGHTED_DISTANCE,
            SEL_SHOW_NO_HEATMAP
        }));
        jComboBox1.addActionListener(createListener(action, ACTION_COMMAND_SET_DISPLAYED_HEATMAP));
        
        jComboBox2.addActionListener(createListener(action, ACTION_COMMAND_RELATIVE_ABSOLUTE_DIST));
        jComboBox2.setModel(new DefaultComboBoxModel<>(new String[] { 
            SEL_ABSOLUTE_DISTANCE, 
            SEL_RELATIVE_DISTANCE 
        }));
        
        jComboBox3.addActionListener(createListener(action, ACTION_COMMAND_SET_DISTANCE_STRATEGY));
        jComboBox3.setModel(new DefaultComboBoxModel<>(new String[] { 
            SEL_STRATEGY_POINT_TO_TRIANGLE,
            SEL_STRATEGY_POINT_TO_POINT,
            SEL_STRATEGY_RAY_CASTING,
            SEL_STRATEGY_RAY_CASTING_GPU,
        }));
        
        jComboBox4.addActionListener(createListener(action, ACTION_COMMAND_SHOW_HIDE_WEIGHTED_FPOINTS));
        jComboBox4.setModel(new DefaultComboBoxModel<>(new String[] { 
            SEL_SHOW_NO_SPHERES,
            SEL_SHOW_SELECTED_SPHERES,
            SEL_SHOW_ALL_SPHERES
        }));
        
        jButton1.addActionListener(createListener(action, ACTION_COMMAND_FEATURE_POINT_SELECT_ALL));
        jButton2.addActionListener(createListener(action, ACTION_COMMAND_FEATURE_POINT_SELECT_NONE));
        jButton3.addActionListener(createListener(action, ACTION_COMMAND_FEATURE_POINT_SELECT_AUTO));
        jButton4.addActionListener(createListener(action, ACTION_COMMAND_EXPORT_DISTANCE));
        jButton5.addActionListener(createListener(action, ACTION_COMMAND_EXPORT_WEIGHTED_DISTANCE));

        jCheckBox1.addActionListener(createListener(action, ACTION_COMMAND_HD_AUTO_CROP));

        histogramComponent.addActionListener(createListener(action, HistogramComponent.ACTION_COMMAND_BOUNDS_MOVED));
    }
    
    /**
     * Returns the internal panel with the list of feature points
     * @return the internal panel with the list of feature points
     */
    public FpListWeightsPanel getFeaturePointsListPanel() {
        return fpListWeightsPanel1;
    }

    public HistogramComponent getHistogramComponent() {
        return histogramComponent;
    }

    @Override
    public ImageIcon getIcon() {
        return getStaticIcon();
    }
    
    /**
     * Static implementation of the {@link #getIcon()} method.
     * 
     * @return Control panel icon
     */
    public static ImageIcon getStaticIcon() {
        return new ImageIcon(DistancePanel.class.getClassLoader().getResource("/" + ICON));
    }
    
    /**
     * Updates GUI elements that display statistical data about the calculated Hausdorff distance.
     * 
     * @param hd Statistical data of the ordinary Hausdorff distance
     * @param whd Statistical data of the weighted Hausdorff distance
     */
    public void updateHausdorffDistanceStats(DoubleSummaryStatistics hd, DoubleSummaryStatistics whd) {
        jTextField1.setText(String.format("%.3f", hd.getAverage()));
        jTextField2.setText(String.format("%.3f", whd.getAverage()));
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jCheckBox1 = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        fpListWeightsPanel1 = new cz.fidentis.analyst.gui.elements.FpListWeightsPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jComboBox4 = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        infoLinkButton1 = new cz.fidentis.analyst.gui.elements.InfoLinkButton();
        histogramComponent = new cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jPanel1.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Absolute", "Relative" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Point to point", "Point to triangle" }));

        jCheckBox1.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(jCheckBox1, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jCheckBox1.text")); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jScrollPane1.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        javax.swing.GroupLayout fpListWeightsPanel1Layout = new javax.swing.GroupLayout(fpListWeightsPanel1);
        fpListWeightsPanel1.setLayout(fpListWeightsPanel1Layout);
        fpListWeightsPanel1Layout.setHorizontalGroup(
            fpListWeightsPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 626, Short.MAX_VALUE)
        );
        fpListWeightsPanel1Layout.setVerticalGroup(
            fpListWeightsPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 338, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(fpListWeightsPanel1);

        jScrollPane1.setViewportView(jScrollPane2);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jPanel2.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jLabel1.text")); // NOI18N

        jTextField1.setText(org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jTextField1.text")); // NOI18N
        jTextField1.setEnabled(false);

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jLabel2.text")); // NOI18N

        jTextField2.setText(org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jTextField2.text")); // NOI18N
        jTextField2.setEnabled(false);

        org.openide.awt.Mnemonics.setLocalizedText(jButton4, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jButton4.text")); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jButton5, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jButton5.text")); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4)
                    .addComponent(jButton5))
                .addContainerGap())
        );

        org.openide.awt.Mnemonics.setLocalizedText(jButton1, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jButton1.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jButton2, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jButton2.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jButton3, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jButton3.text")); // NOI18N

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jPanel3.border.title"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 1, 15))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 15)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jLabel4.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jLabel3.text")); // NOI18N

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.jPanel4.border.title"))); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(infoLinkButton1, org.openide.util.NbBundle.getMessage(DistancePanel.class, "DistancePanel.infoLinkButton1.text")); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(infoLinkButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(infoLinkButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3))
                    .addComponent(histogramComponent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 632, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addGap(15, 15, 15)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(histogramComponent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private cz.fidentis.analyst.gui.elements.FpListWeightsPanel fpListWeightsPanel1;
    private cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent histogramComponent;
    private cz.fidentis.analyst.gui.elements.InfoLinkButton infoLinkButton1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
