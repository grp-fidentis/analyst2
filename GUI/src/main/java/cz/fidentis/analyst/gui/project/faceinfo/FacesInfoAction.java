package cz.fidentis.analyst.gui.project.faceinfo;

import cz.fidentis.analyst.gui.project.ProjectPanel;
import cz.fidentis.analyst.gui.project.ProjectPanelAction;
import cz.fidentis.analyst.gui.project.table.ProjectTable;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;

import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;

/**
 * Action listener for panel showing face details.
 *
 * @author Radek Oslejsek
 */
public class FacesInfoAction extends ProjectPanelAction<FaceInfoControlPanel> {
    
    /**
     * Constructor for project window when project panel is provided
     *
     * @param projectPanel Project panel
     * @param topControlPane Top component for placing control panels
     */
    public FacesInfoAction(ProjectPanel projectPanel, JTabbedPane topControlPane) {
        super(projectPanel, topControlPane);
        
        setControlPanel(new FaceInfoControlPanel(this));

        setShowHideCode(
                () -> { // SHOW
                },
                () -> { // HIDE
                }
        );        
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        
        switch (action) {
            case FaceInfoControlPanel.ACTION_LOAD_GEOMETRY_INFO:
                FaceReference faceReference = getSelectedFace();
                if (faceReference != null && !FaceService.INSTANCE.isInMemory(faceReference)) {
                    FaceService.INSTANCE.getFaceByReference(faceReference); // load
                    getControlPanel().getInfoPanel().showFaceState(faceReference);
                }
                break;
            case ProjectPanel.ACTION_FACES_SELECTED:
                FaceReference selectedFace = getSelectedFace();
                if (selectedFace != null) {
                    getControlPanel().getInfoPanel().showFaceState(selectedFace);
                }
                break;
            default:
        }
    }
    
    private FaceReference getSelectedFace() {
        ProjectTable table =  getProjectPanel().getProjectTable();
        if (table.getSelectedRowCount() == 1) {
            int row = table.getSelectedRow();
            return table.getFaceAt(row);
        } else {
            return null;
        }
    }
}
