package cz.fidentis.analyst.gui.elements;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * A single line input field that lets the user show and select real number
 * from an ordered sequence. Mapping of real values into the corresponding 
 * integer scale is available as well.
 * 
 * @author Radek Oslejsek
 */
public class DoubleSpinner extends JSpinner {
    
    private int fractionDigits = 2;

    /**
     * Constructor.
     * 
     * @param val Initial value
     * @param min Minimum allowed value
     * @param max Maximum allowed value
     * @param fractionDigits precision of floating numbers, 
     * i.e., the number of digits allowed after the floating dot. 
     * Must be bigger than zero.
     */
    public DoubleSpinner(double val, double min, double max, int fractionDigits) {
        super(new SpinnerNumberModel(val, min, max, 0.1));
        
        if (fractionDigits <= 0) {
            throw new IllegalArgumentException("fractionDigits");
        } else {
            this.fractionDigits = fractionDigits;
        }
    }
    
    /**
     * Returns the minimum allowed value as a real number.
     * @return the minimum allowed value
     */
    public double getDoubleMinimum() {
        return (Double) ((SpinnerNumberModel) getModel()).getMinimum();
    }
    
    /**
     * Returns the maximum allowed value as a real number.
     * @return the maximum allowed value
     */
    public double getDoubleMaximum() {
        return (Double) ((SpinnerNumberModel) getModel()).getMaximum();
    }
    
    /**
     * Returns current value as a real number.
     * @return current value
     */
    public double getDoubleValue() {
        return (Double) ((SpinnerNumberModel) getModel()).getValue();
    }
    
    /**
     * Returns the minimum allowed value as an integer 
     * (the real value multiplied by the {@code getFractionDecimals()}.
     * 
     * @return the minimum allowed value
     */
    public int getIntegerMinimum() {
        return (int) (getDoubleMinimum() * getFractionDecimals());
    }
    
    /**
     * Returns the maximum allowed value as an integer 
     * (the real value multiplied by the {@code getFractionDecimals()}.
     * 
     * @return the maximum allowed value
     */
    public int getIntegerMaximum() {
        return (int) (getDoubleMaximum() * getFractionDecimals());
    }
    
    /**
     * Returns current value as an integer 
     * (the real value multiplied by the {@code getFractionDecimals()}.
     * 
     * @return current value
     */
    public int getIntegerValue() {
        return (int) (getDoubleValue() * getFractionDecimals());
    }
    
    /**
     * This method sets the double value, but round it into the number of 
     * decimal digits given by {@code fractionDigits} parameter provided to 
     * the constructor.
     * 
     * @param value double value to be set
     * @throws IllegalArgumentException if <code>value</code> isn't allowed
     */
    @Override
    public void setValue(Object value) {
        super.setValue(convertValue(value));
    }

    /**
     * This method converts the given value by rounding it into the number of
     * decimal digits given by {@code fractionDigits} parameter provided to
     * the constructor.
     *
     * @param value double value to be set
     * @throws IllegalArgumentException if <code>value</code> isn't allowed
     */
    public double convertValue(Object value) {
        if (value instanceof Double) {
            // round a fractio part to N decimal places
            int fraction = getFractionDecimals();
            return ((double) Math.round((Double)value * fraction)) / fraction;
        } else if (value instanceof Integer) {
            return (Integer) value / (double) getFractionDecimals();
        } else {
            throw new IllegalArgumentException("value");
        }
    }
    
    /**
     * Returns 1, 10, etc., based on the number of fraction digits 1, 2, ...
     * @return 1, 10, etc., based on the number of fraction digits 1, 2, ...
     */
    public int getFractionDecimals() {
        int scale = 1;
        for (int i = 0; i < fractionDigits; i++) {
            scale *= 10;
        }
        return scale;
    }
}