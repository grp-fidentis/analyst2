package cz.fidentis.analyst.gui.task;

import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;

/**
 * A tabbed pane that stores individual control panels.
 * 
 * @author Radek Oslejsek
 */
public class TaskControlPane extends JTabbedPane {
    
    /**
     * Constructor.
     */
    public TaskControlPane() {
        TitledBorder border = BorderFactory.createTitledBorder("Control Panels");
        border.setTitleFont(border.getTitleFont().deriveFont(Font.BOLD));
        //border.setTitleColor(ControlPanelBuilder.OPTION_TEXT_COLOR);
        //setBorder(border);
    }    
}
