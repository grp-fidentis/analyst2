package cz.fidentis.analyst.gui.app;

import cz.fidentis.analyst.Logger;
import java.awt.BorderLayout;
import java.io.IOException;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Wrapper of the default Java Netbeans Platform output window.
 * This singleton runs in the background, reads messages logged through 
 * the {@link cz.fidentis.analyst.Logger} and displays them in the output window.
 * 
 * This singleton is currently executed from 
 * the {@link cz.fidentis.analyst.gui.project.ProjectWindow}, which is always presented
 * in the FIDENTIS application.
 *
 * @author Radek Oslejsek
 */
public class OutputWindowThread extends Thread {
    
    private JTextArea textArea; 
    
    private static OutputWindowThread instance;
    
    protected OutputWindowThread() {
        TopComponent outputWin = WindowManager.getDefault().findTopComponent("output");
        textArea = new JTextArea();
        textArea.setEditable(false);
        JScrollPane sp = new JScrollPane(textArea);
        outputWin.add(sp, BorderLayout.CENTER);
    }
    
    /**
     * Starts the redirection of messages logged via the {@link cz.fidentis.analyst.Logger}
     * into the output window.
     * 
     * @return {@code false} if the singleton/thread is already executed, 
     * {@code true} otherwise.
     */
    public static boolean execute() {
        if (instance != null) {
            return false;
        }
        instance = new OutputWindowThread();
        instance.start(); // executes the run() method
        Logger.redirectToPipe();
        return true;
    }
    
    /**
     * Stops the redirection of messages logged via the {@link cz.fidentis.analyst.Logger}
     * into the output window.
     * 
     * @param err If {@code true} then the logged messages are redirected to the system error output.
     *            Otherwise, the system standard output is used.
     * @return {@code false} if the singleton/thread is already stopped, 
     *         {@code true} otherwise.
     */
    public static boolean stopExecution(boolean err) {
        if (instance == null) {
            return false;
        }
        if (err) {
            Logger.redirectToStdErr();
        } else {
            Logger.redirectToStdOut();
        }
        instance = null;
        return true;
    }
    
    /**
     * Stops the redirection of messages logged via the {@link cz.fidentis.analyst.Logger}
     * into the output window. The messages are redirected into the system standard output.
     * 
     * @return {@code false} if the singleton/thread is already stopped, 
     *         {@code true} otherwise.
     */
    public static boolean stopExecution() {
        return stopExecution(false);
    }
    
    @Override
    public void run() {
        int errorCounter = 0;
        
        while (true) {
            try {
                String line = Logger.read();
                if (line != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(textArea.getText())
                            .append(line)
                            .append(System.lineSeparator());
                    textArea.setText(sb.toString());
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    errorCounter = 0;
                } else {
                    //System.out.println("No datain the message pipe");
                    fallAsleep(2000);
                }
            } catch (IOException ex) {
                if (errorCounter < 8) {
                    errorCounter++;
                }
                System.out.println("IOException: " + ex);
                fallAsleep(errorCounter * 1000);                
            } 
        }
    }
    
    protected void fallAsleep(int mil) {
        try {
            sleep(mil);
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
