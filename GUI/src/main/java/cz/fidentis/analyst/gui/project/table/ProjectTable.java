package cz.fidentis.analyst.gui.project.table;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.project.*;
import cz.fidentis.analyst.gui.task.faceinfo.FaceOverviewPanel;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableColumnModel;

import org.imgscalr.Scalr;
import org.openide.util.Exceptions;

/**
 * A table-based view of the list of project's faces
 * 
 * @author Matej Kovar
 * @author Radek Oslejsek
 */
public class ProjectTable extends JTable {
    
    private final ImageIcon previewBasic = new ImageIcon(FaceOverviewPanel.class.getClassLoader().getResource("/" + "face32x32.png"));
    //private final ImageIcon previewBasicGray = new ImageIcon(FaceOverviewPanel.class.getClassLoader().getResource("/" + "face_gray32x32.png"));
    
    /**
     * Constructor.
     */
    public ProjectTable() {
        super(new ProjectTableModel());
        
        // hide the filtered out column but remains it accessible via getValueAt()
        TableColumnModel tcm = getColumnModel();
        tcm.removeColumn(tcm.getColumn(ProjectTableModel.Column.FILTERED_OUT.index));
        
        setDefaultRenderer(String.class, new DisableTableCellRenderer());
        setDefaultRenderer(JComboBox.class, new DisableTableCellRenderer());
        //setDefaultRenderer(ImageIcon.class, new DisableTableCellRenderer());
        
        initComponents();
        
        // sync check boxes with the selection of rows:
        getSelectionModel().addListSelectionListener((ListSelectionEvent event) -> {
            Set<Integer> selRows = Arrays.stream(getSelectedRows()).boxed().collect(Collectors.toSet());
            for (int i = 0; i < getProjectTableModel().getRowCount(); i++) {
                if (!isFilteredOut(i)) {
                    getProjectTableModel().setValueAt(selRows.contains(i), i, ProjectTableModel.Column.CHECKBOX.index);
                }
            }
        });
    }
    
    @Override
    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
        if (!isFilteredOut(rowIndex)) {
            super.changeSelection(rowIndex, columnIndex, toggle, extend);
        }
    }
    
    @Override
    public void selectAll() {
        if (getRowCount() > 0 && getColumnCount() > 0) {
            clearSelection();
            ListSelectionModel selModel = getSelectionModel();
            selModel.setValueIsAdjusting(true);
            setRowSelectionInterval(0, getRowCount()-getProjectTableModel().getNumFilteredRows()-1);
            selModel.setValueIsAdjusting(false);
        }
    }
    
    /**
     * Clears the table
     */
    public void removeAllRows() {
        for (int i = getRowCount()-1; i >= 0; i--) {
            this.getProjectTableModel().removeRow(i);
        }
    }

    /**
     * Returns face name at the given row
     * 
     * @param row row number
     * @return face name or {@code null} 
     */
    public String getFaceNameAt(int row) {
        return (String) getModel().getValueAt(row, ProjectTableModel.Column.FACE_NAME.index);
    }

    /**
     * Returns face at the given row
     *
     * @param row row number
     * @return face or {@code null}
     */
    public FaceReference getFaceAt(int row) {
        String faceName = (String) getModel().getValueAt(row, ProjectTableModel.Column.FACE_NAME.index);
        if (faceName == null) {
            return null;
        }
        getModel().getValueAt(row, ProjectTableModel.Column.FACE_NAME.index);
        return ProjectService.INSTANCE.getFaceReferenceByName(faceName);
    }

    /**
     * Returns a combo box with tasks in which the face is open
     * 
     * @param row row number
     * @return a combo box with tasks in which the face is open, or {@code null} 
     */
    public JComboBox getTasksAt(int row) {
        return (JComboBox) getModel().getValueAt(row, ProjectTableModel.Column.TASKS.index);
    }
    
    /**
     * Determines whether the row is filtered out 
     * 
     * @param row row number
     * @return {@code true} if the row is filtered out
     */
    public boolean isFilteredOut(int row) {
        Boolean val = (Boolean) getModel().getValueAt(row, ProjectTableModel.Column.FILTERED_OUT.index);
        return (val != null && val);
    }
    
    /**
     * Returns the underlying table model
     * @return the underlying table model
     */
    public ProjectTableModel getProjectTableModel() {
        return (ProjectTableModel) getModel();
    }

    /**
     * Find faces (rows) that are included in the given task and 
     * links the task with them by adding the task name into to combo-box column.
     * 
     * @param task Face references of a task
     */    
    public void linkFacesWithTask(Task task) {
        List<FaceReference> faceReferences = task.getFaces();
        for (int i = 0; i < faceReferences.size(); i++) {
            for (int row = 0; row < this.getRowCount(); row++) {
                String faceName = faceReferences.get(i).getName();
                if (getFaceNameAt(row) == null ? faceName == null : getFaceNameAt(row).equals(faceName)) {
                    getTasksAt(row).addItem(task.getName());
                    break;
                }
            }
        }
    }
    
    /**
     * Finds all faces (lines) thar are linked with the given task and 
     * removes the task from their combo-box column.
     * 
     * @param task Face references of the task that is to be removed
     */
    public void unlinkFacesFromTask(Task task) {
        List<FaceReference> faceReferences = task.getFaces();
        for (int i = 0; i < faceReferences.size(); i++) {
            for (int row = 0; row < this.getRowCount(); row++) {
                String faceName = faceReferences.get(i).getName();
                if (getFaceNameAt(row) == null ? faceName == null : getFaceNameAt(row).equals(faceName)) {
                    getTasksAt(row).removeItem(task.getName());
                    break;
                }
            }
        }
    }
    
    /**
     * Adds new row to the table.
     *
     * @param faceReference Face reference of the face to be added
     * @param taskSelected Action listener to be connected with the combo-box with the face open tasks
     */
    public void addRow(FaceReference faceReference, ActionListener taskSelected) {
        File preview = faceReference.getPreviewFile();
        //Logger.print("Face preview " + preview.getAbsolutePath());
        int index = this.getRowCount() - getProjectTableModel().getNumFilteredRows();
        
        JComboBox comboBox = new JComboBox();
        comboBox.addActionListener(taskSelected);
        comboBox.setFont(new java.awt.Font("Tahoma", 0, 14));
        
        if (preview == null || !preview.exists()) {
            getProjectTableModel().insertRow(index, new Object[]{false, faceReference.getName(), comboBox, previewBasic});
        } else {
            try {
                BufferedImage image = ImageIO.read(preview);

                // Scale image to fit into column
                BufferedImage scaledImage = Scalr.resize(image, 70, 55);
                         
                getProjectTableModel().insertRow(index, new Object[]{false, faceReference.getName(), comboBox, new ImageIcon(scaledImage)});
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }    
        }
    }

    /**
     * Filters out the given rows.
     * 
     * @param filteredRows Rows to be filtered out
     * @return Names of faces that was filtered out
     */
    public List<String> filterRows(List<Integer> filteredRows) {
        List<String> filteredFaces = new ArrayList<>();
        
        // Move selected (filtered) faces to the end of the list:
        filteredRows.stream()
                .sorted(Collections.reverseOrder())
                .forEach(row -> { 
                    String faceName = getFaceNameAt(row);
                    filteredFaces.add(faceName);
                    getProjectTableModel().filterRow(row);
                });
        
        return filteredFaces;
    }

    /**
     * Unfilters and moves up given number of last filtered rows.
     * 
     * @param num The number of last filtered rows to be unfiltered
     */
    public void unfilterRows(int num) {
        for (int i = 0; i < num; i++) {
            int row = this.getRowCount() - 1;
            if (row >= 0 && isFilteredOut(row)) {
                getProjectTableModel().unfilterRow(row);
            }
        }
    }
    
    private void initComponents() {
        setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        getTableHeader().setOpaque(false);
        getTableHeader().setBackground(new java.awt.Color(204,204,204));
        getTableHeader().setFont(new java.awt.Font("Tahoma", 0, 18));
        
        //setModel(getModel());
        
        getColumnModel().getColumn(0).setMaxWidth(50);
        getColumnModel().getColumn(1).setPreferredWidth(400);
        getColumnModel().getColumn(2).setMaxWidth(250);
        getColumnModel().getColumn(2).setPreferredWidth(250);
        getColumnModel().getColumn(3).setMaxWidth(125);
        getColumnModel().getColumn(3).setPreferredWidth(125);
        
        getTableHeader().getColumnModel().getColumn(0).setMaxWidth(50);
        getTableHeader().getColumnModel().getColumn(1).setPreferredWidth(400);
        getTableHeader().getColumnModel().getColumn(2).setMaxWidth(250);
        getTableHeader().getColumnModel().getColumn(2).setPreferredWidth(250);
        getTableHeader().getColumnModel().getColumn(3).setMaxWidth(125);
        getTableHeader().getColumnModel().getColumn(3).setPreferredWidth(125);
        
        setDragEnabled(true);
        setRowHeight(60);
        setSelectionBackground(new java.awt.Color(102, 204, 255));
        //setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        getTableHeader().setReorderingAllowed(false);
        
        getColumnModel().getColumn(2).setCellEditor(new TaskCellEditor());
        getColumnModel().getColumn(2).setCellRenderer(new TaskCellRenderer());        
    }
    
}
