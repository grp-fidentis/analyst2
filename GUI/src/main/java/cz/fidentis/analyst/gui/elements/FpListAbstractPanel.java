package cz.fidentis.analyst.gui.elements;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.gui.task.LoadedActionEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Generic class for a list of feature points.
 *
 * @param <T> content of a row
 * @author Radek Oslejsek
 */
public abstract class FpListAbstractPanel<T extends FpListAbstractPanel.Row> extends JPanel {

    /**
     * Actions triggered during the user interaction. They should be handled by the associated action listener.
     */
    public static final String ACTION_COMMAND_FEATURE_POINT_HOVER_IN = "highlight hovered FP";
    public static final String ACTION_COMMAND_FEATURE_POINT_HOVER_OUT = "set default color of hovered FP";
    public static final String ACTION_COMMAND_FEATURE_POINT_SELECT = "highlight feature point with color";

    /**
     * A single raw of the list with mandatory checkpoint and the feature point.
     * This class can be extended in subclasses to add additional data to each row.
     *
     * @author Radek Oslejsek
     */
    protected class Row {
        private final Landmark featurePoint;
        private final JCheckBox checkbox;

        /**
         * Constructor.
         *
         * @param featurePoint feature point
         */
        public Row(Landmark featurePoint) {
            this.featurePoint = featurePoint;
            this.checkbox =  new JCheckBox(featurePoint.getName());
        }

        public Landmark getFeaturePoint() {
            return featurePoint;
        }

        public JCheckBox getCheckbox() {
            return checkbox;
        }

        /**
         * This method adds a row into a given panel.
         *
         * @param panel a panel storing rows
         * @param c Grid Bag layout constraint
         */
        public void addToPanel(JPanel panel, GridBagConstraints c) {
            c.anchor = GridBagConstraints.LINE_START;
            panel.add(checkbox, c);
            c.gridx++;
        }
    }

    /**
     * Action listener handling user events performed with the list of feature points.
     */
    private transient ActionListener actionListener;

    private final List<T> rows = new ArrayList<>();

    protected List<T> getRows() {
        return Collections.unmodifiableList(rows);
    }

    protected ActionListener getActionListener() {
        return actionListener;
    }

    protected void initComponents(ActionListener action, List<T> rows) {
        Objects.requireNonNull(action);

        rows.forEach(row -> row.getCheckbox().setToolTipText("<html><body><p style='width: 250px;'>" +
                row.getFeaturePoint().getDescription() + "</p></body></html>"));

        this.actionListener = action;
        this.rows.clear();
        this.rows.addAll(rows);

        // build GUI
        removeAll();
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 2, 0, 2);
        c.gridx = 0;
        c.gridy = 0;
        getRows().forEach(row -> {
            row.addToPanel(this, c);
            c.gridx = 0;
            c.gridy++;
        });

        setCheckboxActions();
        setHoverActions();
    }

    /**
     * If the feature point is (un)checked, then {@code ACTION_COMMAND_FEATURE_POINT_SELECT}
     * is triggered in the action listener.
     */
    protected void setCheckboxActions() {
        for (int i = 0; i < rows.size(); i++) {
            int index = i;
            rows.get(i).getCheckbox().addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    actionListener.actionPerformed(new LoadedActionEvent(
                            e.getSource(),
                            ActionEvent.ACTION_PERFORMED,
                            ACTION_COMMAND_FEATURE_POINT_SELECT,
                            index
                    ));
                }
            });
        }
    }

    /**
     * If the mouse pointer hovers over a feature point, then {@code ACTION_COMMAND_FEATURE_POINT_HOVER_IN}
     * and {@code ACTION_COMMAND_FEATURE_POINT_HOVER_OUT} are triggered in the action listener.
     */
    protected void setHoverActions() {
        for (int i = 0; i < rows.size(); i++) {
            int index = i;
            rows.get(i).getCheckbox().addMouseListener(new MouseAdapter() { // highlight spheres on mouse hover
                @Override
                public void mouseEntered(MouseEvent e) {
                    actionListener.actionPerformed(new LoadedActionEvent(
                            e.getSource(),
                            ActionEvent.ACTION_PERFORMED,
                            ACTION_COMMAND_FEATURE_POINT_HOVER_IN,
                            index));
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    actionListener.actionPerformed(new LoadedActionEvent(
                            e.getSource(),
                            ActionEvent.ACTION_PERFORMED,
                            ACTION_COMMAND_FEATURE_POINT_HOVER_OUT,
                            index));
                }
            });
        }
    }

}
