package cz.fidentis.analyst.gui.task.batch.distance;

import cz.fidentis.analyst.engines.face.batch.distance.BatchDistance;
import cz.fidentis.analyst.gui.elements.ProgressDialog;

import javax.swing.*;

/**
 * A task that computes distance-based similarity of the set of faces.
 *
 * @author Radek Oslejsek
 */
public abstract class BatchDistanceTask extends SwingWorker<Void, Integer> {

    private final ProgressDialog<Void, Integer> progressDialog;
    private final BatchDistancePanel controlPanel;

    private BatchDistance batchDistance;

    /**
     * Constructor.
     *
     * @param progressDialog A window that show the progress of the computation. Must not be {@code null}
     * @param controlPanel A control panel with computation parameters. Must not be {@code null}
     **/
    public BatchDistanceTask(ProgressDialog<Void, Integer> progressDialog, BatchDistancePanel controlPanel) {
        this.progressDialog = progressDialog;
        this.controlPanel = controlPanel;
    }

    @Override
    protected void done() {
        getProgressDialog().dispose(); // close progress bar
        if (isCancelled()) {
            setBatchDistance(null);
        }
    }

    /**
     * Returns computed 2D matrix of distance similarities or {@code null}
     * @return computed 2D matrix of distance similarities or {@code null}
     */
    public double[][] getDistSimilarities() {
        return batchDistance != null ? batchDistance.getDistSimilarities() : null;
    }

    /**
     * Returns computed 2D matrix of sample standard deviations or {@code null}
     * @return computed 2D matrix of sample standard deviations or {@code null}
     */
    public double[][] getDistDeviations() {
        return batchDistance != null ? batchDistance.getDistDeviations() : null;
    }

    protected ProgressDialog<Void, Integer> getProgressDialog() {
        return progressDialog;
    }

    protected final BatchDistancePanel getControlPanel() {
        return controlPanel;
    }

    protected void setBatchDistance(BatchDistance batchDistance) {
        this.batchDistance = batchDistance;
    }

    protected BatchDistance getBatchDistance() {
        return batchDistance;
    }
}
