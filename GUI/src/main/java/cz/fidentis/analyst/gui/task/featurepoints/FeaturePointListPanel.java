package cz.fidentis.analyst.gui.task.featurepoints;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.gui.task.LoadedActionEvent;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Katerina Zarska
 */
public class FeaturePointListPanel extends JPanel {
    
    public static final String ACTION_COMMAND_FEATURE_POINT_HOVER_IN = "highlight hovered FP";
    public static final String ACTION_COMMAND_FEATURE_POINT_HOVER_OUT = "set default color of hovered FP";
    public static final String ACTION_COMMAND_FEATURE_POINT_SELECT = "highlight feature point with color";
    
    private List<JLabel> colorIndicators;
    private List<JCheckBox> featurePointCheckBoxes;
    
    /**
     * Constructor.
     * 
     * @param action Action listener
     * @param featurePoints List of all feature points 
     */
    public void initComponents(ActionListener action, DrawableFeaturePoints featurePoints) {
        setLayout(new GridBagLayout());
        
        featurePointCheckBoxes = new ArrayList<>(featurePoints.getFeaturePoints().size());
        colorIndicators = new ArrayList<>(featurePoints.getFeaturePoints().size());
        
        for (int i = 0; i < featurePoints.getFeaturePoints().size(); i++) {
            addRow(i, action, featurePoints);
        }
    }
    
    /**
     * (De)selects given feature point
     * 
     * @param index Index of the feature point
     * @param selected {@code true} if a feature point checkbox is to be selected,
     *                 {@code false} otherwise
     */
    public void selectFeaturePoint(int index, boolean selected) {
        if (index >= 0 && index < featurePointCheckBoxes.size()) {
            featurePointCheckBoxes.get(index).setSelected(selected);
        }
    }
    
    /**
     * Refreshes the panel
     * @param action
     * @param featurePoints 
     */
    public void refreshPanel(ActionListener action, DrawableFeaturePoints featurePoints, List<Landmark> selectedFeaturePoints) {
        removeAll();
        initComponents(action, featurePoints);
        revalidate();
        repaint();

        for (int i = 0; i < featurePoints.getFeaturePoints().size(); i++) {
            if (selectedFeaturePoints.contains(featurePoints.getFeaturePoints().get(i))) {
                featurePointCheckBoxes.get(i).setSelected(true);
            } else {
                featurePointCheckBoxes.get(i).setSelected(false);
            }
        }
    }
    
    /**
     * Creates and returns action listener that can be connected with a low-level 
     * GUI element (e.g., a button). Action event of triggered low-level element is
     * redirected to the given {@code action}.
     * The listener may also carry additional data as a payload.
     * 
     * @param action Action listener
     * @param command Control panel command
     * @param data Payload data of the action listener
     * @return Action listener
     */
    protected final ActionListener createListener(ActionListener action, String command, Object data) {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action.actionPerformed(new LoadedActionEvent(
                        e.getSource(), 
                        ActionEvent.ACTION_PERFORMED, 
                        command,
                        data)
                ); 
            }  
        };
    }
    
    protected void addRow(int row, ActionListener action, DrawableFeaturePoints featurePoints) {
        Landmark featurePoint = featurePoints.getFeaturePoints().get(row);
        
        Color color = featurePoints.getOffTheMeshColor();
        MeshVicinity.Location fpType = featurePoint.getMeshVicinity().getPositionType(featurePoints.getDistanceThreshold());
        if (featurePoint.isUserDefinedLandmark()) {
            color = featurePoints.getCustomFpColor();
        } else if (fpType == MeshVicinity.Location.ON_THE_MESH) {
            color = featurePoints.getOnTheMeshColor();
        } else if (fpType == MeshVicinity.Location.CLOSE_TO_MESH) {
            color = featurePoints.getCloseToMeshColor();
        }

        GridBagConstraints c = new GridBagConstraints();  
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = row;
        c.anchor = GridBagConstraints.LINE_START;
        c.fill = GridBagConstraints.NONE;
        
        GridBagConstraints c1 = new GridBagConstraints();
        c1.insets = new Insets(0, 10, 0, 10);
        c1.gridwidth = GridBagConstraints.REMAINDER;
        c1.gridx = 1;
        c1.gridy = row;
        c1.anchor = GridBagConstraints.LINE_END;
        c1.fill = GridBagConstraints.NONE; 
        
        //adds a color indicator to the end of the line to show the relation to mesh of the feature point
        JLabel colorIndicator = new JLabel("");
        Border border = BorderFactory.createLineBorder(Color.GRAY, 1);
        colorIndicator.setOpaque(true);
        colorIndicator.setBackground(color);
        colorIndicator.setForeground(Color.GRAY);
        colorIndicator.setBorder(border);
        Dimension d = new Dimension(10, 10);
        colorIndicator.setMinimumSize(d);
        colorIndicator.setMaximumSize(d);
        colorIndicator.setPreferredSize(d);
        add(colorIndicator, c1);
        colorIndicators.add(colorIndicator);
        
        //adds a checkbox with the name of the feature point
        JCheckBox checkBox = new JCheckBox();
        checkBox.setText(featurePoint.getName());
        add(checkBox, c);
        featurePointCheckBoxes.add(checkBox);
        
        checkBox.addActionListener(createListener(action, ACTION_COMMAND_FEATURE_POINT_SELECT, row));
        
        final int index = row;
        checkBox.addMouseListener(new MouseAdapter() { // highlight spheres on mouse hover
            @Override
            public void mouseEntered(MouseEvent e) {
                action.actionPerformed(new LoadedActionEvent(
                        e.getSource(),
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_FEATURE_POINT_HOVER_IN,
                        index));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                action.actionPerformed(new LoadedActionEvent(
                        e.getSource(),
                        ActionEvent.ACTION_PERFORMED,
                        ACTION_COMMAND_FEATURE_POINT_HOVER_OUT,
                        index));
            }
        });
    }
}
