package cz.fidentis.analyst.gui.task.symmetry;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.face.events.LandmarksChangedEvent;
import cz.fidentis.analyst.data.face.events.SymmetryPlaneChangedEvent;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.impl.MeshTriangleImpl;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.drawables.DrawablePointCloud;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingVisitor;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.gui.elements.SpinSlider;
import cz.fidentis.analyst.gui.elements.histogram.components.HistogramComponent;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Action listener for the manipulation with the symmetry plane.
 * 
 * @author Radek Oslejsek
 */
public class SymmetryAction extends ControlPanelAction<SymmetryPanel> implements HumanFaceListener {

    private int primCloudSlot = -1;

    /**
     * Working with the subsampling density slider triggers frequent re-computation of samples.
     * In this case, calling the sampling algorithm via {@code PointSamplingServices.sample()}
     * is inefficient because new and new object is created and often new and new internal
     * structures are initiated. Therefore, the stateful invocation is used instead, when
     * the visitor is obtained and reused (with different settings) until the strategy is changed.
     */
    private PointSamplingVisitor samplingVisitor;

    private MeshDistances meshDistance = null;

    /**
     * Constructor. 
     * A new {@code SymmetryPanel} is instantiated and added to the {@code topControlPane}
     * 
     * @param canvas OpenGL canvas
     * @param task Task with faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public SymmetryAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);
        setControlPanel(new SymmetryPanel(this));

        getControlPanel().setEnabledOpenCL(
                OpenCLServices.isOpenCLAvailable()
        );


        getControlPanel().setComputeFromFPs(
                (getPrimaryFeaturePoints() != null && !getPrimaryFeaturePoints().getFeaturePoints().isEmpty()) || 
                        (getSecondaryFeaturePoints() != null && !getSecondaryFeaturePoints().getFeaturePoints().isEmpty())
        );
        
        setShowHideCode(
                () -> { // SHOW
                    // shows point cloud:
                    showHidePrimaryPointSamples(getCanvas().getScene().getPrimaryFaceSlot(), getControlPanel().getPointSamplingStrength1());
                    getCanvas().getScene().showSymmetryPlane(getCanvas().getScene().getPrimaryFaceSlot(), true);
                    getCanvas().getScene().showSymmetryPlane(getCanvas().getScene().getSecondaryFaceSlot(), true);
                },
                () -> { // HIDE
                    // hides point cloud:
                    getScene().setOtherDrawable(primCloudSlot, null);
                    primCloudSlot = -1;
                    getCanvas().getScene().showSymmetryPlane(getCanvas().getScene().getPrimaryFaceSlot(), false);
                    getCanvas().getScene().showSymmetryPlane(getCanvas().getScene().getSecondaryFaceSlot(), false);
                }
        );
        
        getControlPanel().setEnableShowSamples(getSecondaryFace() == null);
        showHidePrimaryPointSamples(getCanvas().getScene().getPrimaryFaceSlot(), getControlPanel().getPointSamplingStrength1());

        // Be informed about changes in faces performed by other GUI elements
        getPrimaryDrawableFace().getHumanFace().registerListener(this);
        if (getSecondaryDrawableFace() != null) {
            getSecondaryDrawableFace().getHumanFace().registerListener(this);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();

        switch (action) {
            case SymmetryPanel.ACTION_COMMAND_COMPUTE:
                if (getControlPanel().getAlgorithm().equals(SymmetryPanel.SYMMETRY_ALGORITHMS.get(2))) {
                    recomputeFromFeaturePoints(getCanvas().getScene().getPrimaryFaceSlot());
                    recomputeFromFeaturePoints(getCanvas().getScene().getSecondaryFaceSlot());
                } else {
                    recomputeFromMesh(getCanvas().getScene().getPrimaryFaceSlot());
                    recomputeFromMesh(getCanvas().getScene().getSecondaryFaceSlot());
                }
                break;
            case SymmetryPanel.ACTION_COMMAND_POINT_SAMPLING_STRENGTH:
                int numSamples = (Integer) ((SpinSlider) ae.getSource()).getValue();
                showHidePrimaryPointSamples(getCanvas().getScene().getPrimaryFaceSlot(), numSamples);
                break;
            case SymmetryPanel.ACTION_COMMAND_POINT_SAMPLING_STRATEGY:
                if(samplingVisitor!=null){
                    samplingVisitor.dispose();
                }

                var strategy = getSamplingStrategy(getControlPanel().getPointSamplingStrength1());
                samplingVisitor = strategy == null ? null : strategy.getVisitor();
                if (samplingVisitor != null) {
                    getPrimaryFace().getMeshModel().getFacets().forEach(facet -> samplingVisitor.visitMeshFacet(facet));
                }

                showHidePrimaryPointSamples(getCanvas().getScene().getPrimaryFaceSlot(), getControlPanel().getPointSamplingStrength1());
                break;
            case SymmetryPanel.ACTION_COMMAND_SHOW_SAMPLES:
                showHidePrimaryPointSamples(getCanvas().getScene().getPrimaryFaceSlot(), getControlPanel().getPointSamplingStrength1());
                break;
            case SymmetryPanel.ACTION_COMMAND_UPDATE_SYMMETRY:
                recomputeSymmetry();
                break;
            case HistogramComponent.ACTION_COMMAND_BOUNDS_MOVED:
                updateHeatmap();
                break;
            default:
                // do nothing
        }
        renderScene();
    }
    
    @Override
    public void acceptEvent(HumanFaceEvent event) {
        if (event instanceof SymmetryPlaneChangedEvent) {
            recomputeSymmetry();
        }

        if (event instanceof LandmarksChangedEvent landmarksChangedEvent) {
            getControlPanel().setComputeFromFPs(landmarksChangedEvent.getFace().hasLandmarks());
        }
    }
    
    protected void recomputeFromMesh(int faceSlot) {
        HumanFace face = FaceService.INSTANCE.getFaceByReference(getTask().getFaces().get(faceSlot));
        if (face == null) {
            return;
        }
        
        FaceStateServices.updateCurvature(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

        Logger log = Logger.measureTime();
        getControlPanel().getParent().setCursor(new Cursor(Cursor.WAIT_CURSOR));
        
        var symmetryConfig = getSymmetryConfig();

        if (symmetryConfig.method() == SymmetryConfig.Method.ROBUST_MESH &&
                (symmetryConfig.priSampling().method() == PointSamplingConfig.Method.POISSON
                        || symmetryConfig.priSampling().method() == PointSamplingConfig.Method.POISSON_OPENCL)) {
            JOptionPane.showMessageDialog(
                    getControlPanel().getParent().getParent(),
                    """
                            Poisson disk sub sampling cannot be used
                            with the robust mesh surface due to\s
                            missing curvature values at samples""",
                    "Not supported",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        FaceStateServices.updateSymmetryPlane(face, FaceStateServices.Mode.COMPUTE_ALWAYS, symmetryConfig);
        getControlPanel().getParent().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        log.printDuration("Symmetry plane estimation for " + face.getShortName() +
                " using " + symmetryConfig.method()+"/"+symmetryConfig.priSampling().method() + " sampling ");

        setDrawablePlane(face, faceSlot);
        face.announceEvent(new SymmetryPlaneChangedEvent(face, "", this));
    }
    
    protected void recomputeFromFeaturePoints(int slot) {
        HumanFace face = FaceService.INSTANCE.getFaceByReference(getTask().getFaces().get(slot));
        if (face == null) {
            return;
        }

        FaceStateServices.updateSymmetryPlane(face, FaceStateServices.Mode.COMPUTE_ALWAYS, null);

        setDrawablePlane(face, slot);
        face.announceEvent(new SymmetryPlaneChangedEvent(face, "", this));
    }
    
    protected void setDrawablePlane(HumanFace face, int slot) {
        getCanvas().getScene().setDrawableSymmetryPlane(slot, face); // add or remove
        if (face.hasSymmetryPlane()) {
            getCanvas().getScene().getDrawableSymmetryPlane(slot).setTransparency(0.5f);
            getCanvas().getScene().getDrawableSymmetryPlane(slot).setColor(
                    (slot == 0) ? DrawableFace.SKIN_COLOR_PRIMARY.darker() : DrawableFace.SKIN_COLOR_SECONDARY.darker()
            );
        }
    }
    
    private void showHidePrimaryPointSamples(int faceSlot, int numSamples) {
        if (!getControlPanel().getShowSamples()) { // hide
            getScene().setOtherDrawable(this.primCloudSlot, null);
            return;
        }

        HumanFace face = FaceService.INSTANCE.getFaceByReference(getTask().getFaces().get(faceSlot));
        if (face == null) {
            return;
        }                
        
        PointSamplingConfig sampling = getSamplingStrategy(numSamples);

        if (sampling.method().equals(PointSamplingConfig.Method.NO_SAMPLING)) { // don't show
            if (this.primCloudSlot != -1) {
                getScene().setOtherDrawable(this.primCloudSlot, null);
            }
            return;
        }

        if (this.primCloudSlot == -1) {
            this.primCloudSlot = getCanvas().getScene().getFreeSlotForFace();
        }

        samplingVisitor.setRequiredSamples(numSamples);
        getScene().setOtherDrawable(
                this.primCloudSlot,
                new DrawablePointCloud(samplingVisitor.getSamples())
        );
    }
    
    private PointSamplingConfig getSamplingStrategy(int strength) {
        String st = getControlPanel().getPointSamplingStrategy();
        
        if (st.equals(SymmetryPanel.POINT_SAMPLING_STRATEGIES.get(2))) {
            return new PointSamplingConfig(PointSamplingConfig.Method.RANDOM, strength);
        } else if (st.equals(SymmetryPanel.POINT_SAMPLING_STRATEGIES.get(0))) {
            return new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SPACE, strength);
        } else if (st.equals(SymmetryPanel.POINT_SAMPLING_STRATEGIES.get(3))) {
            return new PointSamplingConfig(PointSamplingConfig.Method.CURVATURE_MEAN, strength);
        } else if (st.equals(SymmetryPanel.POINT_SAMPLING_STRATEGIES.get(4))) {
            return new PointSamplingConfig(PointSamplingConfig.Method.CURVATURE_GAUSSIAN, strength);
        } else if (st.equals(SymmetryPanel.POINT_SAMPLING_STRATEGIES.get(1))) {
            return new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SURFACE, strength);
        } else if (st.equals((SymmetryPanel.POINT_SAMPLING_STRATEGIES.get(5)))){
            return new PointSamplingConfig(PointSamplingConfig.Method.POISSON, strength, MeshTriangleImpl.Smoothing.NORMAL);
        } else if (st.equals((SymmetryPanel.POINT_SAMPLING_STRATEGIES.get(6)))){
            return new PointSamplingConfig(PointSamplingConfig.Method.POISSON_OPENCL, strength, MeshTriangleImpl.Smoothing.NORMAL);
        } else {
            return null;
        }
    }
    
    private SymmetryConfig getSymmetryConfig() {
        String alg = getControlPanel().getAlgorithm();
        if (alg.equals(SymmetryPanel.SYMMETRY_ALGORITHMS.get(0))) {
            return new SymmetryConfig(
                    SymmetryConfig.Method.ROBUST_MESH,
                    getSamplingStrategy(getControlPanel().getPointSamplingStrength1()),
                    getControlPanel().getPointSamplingStrength2());
        } else if (alg.equals(SymmetryPanel.SYMMETRY_ALGORITHMS.get(1))) {
            return new SymmetryConfig(
                    SymmetryConfig.Method.ROBUST_POINT_CLOUD,
                    getSamplingStrategy(getControlPanel().getPointSamplingStrength1()),
                    getControlPanel().getPointSamplingStrength2());
        } else {
            return null;
        }
    }

    private void recomputeSymmetry() {
        if (!getPrimaryFace().hasSymmetryPlane()) {
            return;
        }

        meshDistance = FaceStateServices.measureSymmetry(
                getPrimaryFace(),
                getControlPanel().registerReflectedFace() ? 100 : 0);

        // show measurement
        getControlPanel().updatePrecision(meshDistance.getDistanceStats().getAverage());

        // propagate new values to histogram
        getControlPanel().getHistogramComponent().setValues(
                meshDistance.distancesAsMap()
                        .values()
                        .stream()
                        .flatMap(Collection::stream)
                        .toList());

        updateHeatmap();
    }

    private void updateHeatmap() {
        if (!getControlPanel().showHeatmap()) {
            getPrimaryDrawableFace().setHeatMap(null);
            return;
        }

        // show heatmap
        var meshMap = meshDistance.distancesAsMap();
        var hist = getControlPanel().getHistogramComponent();
        var minValue = hist.getLowerBoundValue();
        var maxValue = hist.getUpperBoundValue();

        HashMap<MeshFacet, List<Double>> saturation = new HashMap<>(meshMap);
        saturation.replaceAll(
                (k ,v) -> v.stream()
                        .map(n -> minValue <= n && n <= maxValue ? 1d : 0d)
                        .toList()
        );
        getPrimaryDrawableFace().setHeatMap(new HeatMap3D(meshMap, saturation, minValue, maxValue));
    }
}
