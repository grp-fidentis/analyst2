package cz.fidentis.analyst.gui.task.interactivemask;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.CrossSection3D;
import cz.fidentis.analyst.data.surfacemask.Layer;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask2D;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.interactivemask.MaskProjector;
import cz.fidentis.analyst.engines.interactivemask.MaskProjectorConfig;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.rendering.Camera;

import javax.swing.*;
import javax.vecmath.Point2d;
import javax.vecmath.Vector3d;
import java.awt.event.ActionEvent;


/**
 * Action listener for interactive mask panel
 * @author Mario Chromik
 */
public class InteractiveMaskAction extends ControlPanelAction<InteractiveMaskPanel> {
    /**
     * Constructor
     * A new {@code InteractiveMaskPanel} is instantiated and added to the {@code topControlPane}
     *
     * @param canvas OpenGLCanvas
     * @param faces Faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public InteractiveMaskAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);
        InteractiveMaskPanel pl = new InteractiveMaskPanel(this);
        setControlPanel(pl);
        pl.getSurfaceMaskPanel1().setFaceBoundingBox(getPrimaryFace().getBoundingBox());
        setShowHideCode(
                () -> { // SHOW
                    getCanvas().getScene().showInteractiveMask(getCanvas().getScene().getPrimaryFaceSlot(), true);
                    getCanvas().getScene().showInteractiveMask(getCanvas().getScene().getSecondaryFaceSlot(), true);
                },
                () -> { // HIDE
                    getCanvas().getScene().showInteractiveMask(getCanvas().getScene().getPrimaryFaceSlot(), false);
                    getCanvas().getScene().showInteractiveMask(getCanvas().getScene().getSecondaryFaceSlot(), false);
                }
        );
    }

    /**
     * Gets the ratio between bounding box and the drawing panel
     * @return ratio
     */
    private double getBboxScale() {
        Box box = getPrimaryFace().getBoundingBox();
        Point2d min = CrossSection3D.flattenPoint(box.minPoint(), new Vector3d(0, 0, 1));
        Point2d max = CrossSection3D.flattenPoint(box.maxPoint(), new Vector3d(0, 0, 1));
        double scaleWidth = Math.abs((max.x - min.x) / getControlPanel().getSurfaceMaskPanel1().getPanelWidth());
        double scaleHeight = Math.abs((max.y - min.y) / getControlPanel().getSurfaceMaskPanel1().getPanelHeight());
        return Math.max(scaleWidth, scaleHeight);
    }

    /**
     * Projects a 2D represntation of the mask into 3D
     * @param mask mask to be projected
     */
    private void project(SurfaceMask2D mask) {
        MaskProjectorConfig config = new MaskProjectorConfig(getCanvas().getWidth(),
            getCanvas().getHeight(), getControlPanel().getSurfaceMaskPanel1().getPanelWidth(),
            getControlPanel().getSurfaceMaskPanel1().getPanelHeight(),Camera.FIELD_OF_VIEW,
            getCanvas().getCamera().getPosition(), getCanvas().getCamera().getCenter(),
            getCanvas().getCamera().getUpDirection(), getBboxScale(), getControlPanel().getZoomPrecentage());
        MaskProjector mp = new MaskProjector(config, mask.getPointsToProject());

        if (getControlPanel().isPrimaryFaceEnabled() && getPrimaryFace() != null) {
            FaceStateServices.updateOctree(getPrimaryFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            getPrimaryFace().getOctree().accept(mp);
            getScene().setDrawableInteractiveMask(getScene().getPrimaryFaceSlot(), mp.getResult());
        }

        if (getControlPanel().isSecondaryFaceEnabled() && getSecondaryFace() != null) {
            FaceStateServices.updateOctree(getSecondaryFace(), FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            getSecondaryFace().getOctree().accept(mp);
            getScene().setDrawableInteractiveMask(getScene().getSecondaryFaceSlot(), mp.getResult());
        }
    }


    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        Layer currentLayer;
        switch (action) {
            case InteractiveMaskPanel.ACTION_PROJECT:
                getScene().clearDrawableInteractiveMasks();
                currentLayer = getControlPanel().getSurfaceMaskPanel1().getLayerManager().getCurrentLayer();
                if (currentLayer != null) {
                    currentLayer.updateHistory();
                }

                for (Layer layer : getControlPanel().getSurfaceMaskPanel1().getLayerManager().getLayers()) {
                    if (layer.isVisible()) {
                        for (SurfaceMask2D mask : layer.getAllMasks()) {
                            project(mask);
                        }
                    }
                }

                renderScene();
                break;
            case InteractiveMaskPanel.ACTION_HISTORY_BACKWARD:
                currentLayer = getControlPanel().getSurfaceMaskPanel1().getLayerManager().getCurrentLayer();
                if (currentLayer == null) {
                    break;
                }

                currentLayer.goBack();
                getScene().clearDrawableInteractiveMasks();
                for (Layer layer : getControlPanel().getSurfaceMaskPanel1().getLayerManager().getLayers()) {
                    if (layer.isVisible()) {
                        for (SurfaceMask2D mask : layer.getAllMasks()) {
                            project(mask);
                        }
                    }
                }
                renderScene();
                break;

            case InteractiveMaskPanel.ACTION_HISTORY_FORWARD:
                currentLayer = getControlPanel().getSurfaceMaskPanel1().getLayerManager().getCurrentLayer();
                if (currentLayer == null) {
                    break;
                }

                getControlPanel().getSurfaceMaskPanel1().getLayerManager().getCurrentLayer().goForward();
                getScene().clearDrawableInteractiveMasks();
                for (Layer layer : getControlPanel().getSurfaceMaskPanel1().getLayerManager().getLayers()) {
                    if (layer.isVisible()) {
                        for (SurfaceMask2D mask : layer.getAllMasks()) {
                            project(mask);
                        }
                    }
                }
                renderScene();
                break;
            case InteractiveMaskPanel.ACTION_ZOOM:
                getScene().clearDrawableInteractiveMasks();
                for (Layer layer : getControlPanel().getSurfaceMaskPanel1().getLayerManager().getLayers()) {
                    if (layer.isVisible()) {
                        for (SurfaceMask2D mask : layer.getAllMasks()) {
                            project(mask);
                        }
                    }
                }
                renderScene();
                break;
            case InteractiveMaskPanel.ACTION_POINT_SELECTED:
                break;
            case InteractiveMaskPanel.ACTION_SAMPLING:
                var currentMask = getControlPanel().getSurfaceMaskPanel1().getLayerManager().getCurrentLayer().getCurrentMask();
                currentMask.setSamplingStrength(getControlPanel().getSamplingStrength());
                project(currentMask);
                renderScene();
                break;
            default:
        }
    }
}
