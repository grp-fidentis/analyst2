/**
 * FIDENTIS settings integrated into the Options window of the NetBean Platform.
 */
@OptionsPanelController.ContainerRegistration(id = "Fidentis", categoryName = "#OptionsCategory_Name_Fidentis", iconBase = "logo-28x32.png", keywords = "#OptionsCategory_Keywords_Fidentis", keywordsCategory = "Fidentis")
@NbBundle.Messages(value = {"OptionsCategory_Name_Fidentis=Fidentis", "OptionsCategory_Keywords_Fidentis=fidentis"})
package cz.fidentis.analyst.gui.app.options;

import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;
