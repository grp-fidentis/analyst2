/**
 * Control panel for showing and possibly managing information related
 * to a human face data, e.g., available data files, size of the model, etc.
 */
package cz.fidentis.analyst.gui.task.faceinfo;
