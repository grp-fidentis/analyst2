package cz.fidentis.analyst.gui.task.batch;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;

import javax.swing.*;

/**
 * In contrast to the {@code FacesProxy}, which deals with faces that are the subject of batch processing,
 * this class add handling of the (optionally computed) average face.
 * While the faces can be temporarily dumped, the average face is always in the memory.
 * It is assumed that the average face is added as the first item of a menu (combo box), whenever is computed.
 * This class provides methods for such menu (combo box) management and synchronization.
 *
 * @author Radek Oslejsek
 */
public class SelectBoxService {

    /**
     * Fill the menu (combo box) with names of faces, including the average face, if provided as input parameter.
     * The average face is stored.
     * An action event is triggered automatically by the {@code cbox} with with the originally selected item or
     * the newly added average face.
     *
     * @param avgFace The average face, can be {@code null}
     * @param selectNewAvgFace If {@code true} and a new average is added, then the average face is automatically selected.
     *                         If the {@code avgFace} is {@code null}, then this parameter has no effect.
     * @param cbox A combo box (menu)
     */
    public static void syncSelectionMenu(Task task, FaceReference avgFace, boolean selectNewAvgFace, JComboBox<String> cbox) {
        TaskService.INSTANCE.replaceAverageFace(task, avgFace);
        syncSelectionMenu(task, selectNewAvgFace, cbox);
    }

    /**
     * Fill the menu (combo box) with names of faces stored in the face proxy, plus the optional average face.
     * An action event is triggered automatically by the {@code cbox} with the originally selected item or
     * the newly added average face.
     *
     * @param selectNewAvgFace If {@code true} and a new average is added, then the average face is automatically selected.
     * @param cbox A combo box (menu)
     */
    public static void syncSelectionMenu(Task task, boolean selectNewAvgFace, JComboBox<String> cbox) {
        int expectedNumItems = task.getFaces().size();
        int realNumItems = cbox.getItemCount();
        int selectedItem = cbox.getSelectedIndex();

        cbox.removeAllItems();
        task.getFaces().forEach(f -> {
            if (f.isAverageFace()) {
                cbox.addItem("Average face");
            } else {
                cbox.addItem(f.getName());
            }
        });

        // re-select proper item:
        if (realNumItems < expectedNumItems) { // adding a new average face
            if (selectNewAvgFace) {
                FaceReference averageFaceReference = TaskService.INSTANCE.getAvgFace(task);
                cbox.setSelectedIndex(task.getFaces().indexOf(averageFaceReference));
            } else if (selectedItem >= 0) {
                cbox.setSelectedIndex(selectedItem+1);
            }
        } else if (realNumItems > expectedNumItems) { // removing the average face
            cbox.setSelectedIndex((--selectedItem >=0) ? selectedItem : 0);
        } else { // nothing has changed
            if (selectedItem >= 0) {
                cbox.setSelectedIndex(selectedItem);
            }
        }
    }

    /**
     * Returns the face selected in a menu.
     *
     * @param cbox A combo box (menu)
     * @return The face selected in a menu. This is either some face from the dataset, or the average face.
     */
    public static FaceReference getSelectedFace(Task task, JComboBox<String> cbox) {
        if (cbox.getItemCount() == 0) {
            return null;
        }
        int index = cbox.getSelectedIndex();
        if (index < 0 || index >=  task.getFaces().size()) {
            return null;
        }
        return task.getFaces().get(index);
    }

}
