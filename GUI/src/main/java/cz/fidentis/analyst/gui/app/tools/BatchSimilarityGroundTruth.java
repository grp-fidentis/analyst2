package cz.fidentis.analyst.gui.app.tools;

import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceServices;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A class for testing the efficiency of batch processing algorithms.
 * The goal of this tool is to create a "ground truth" measurements for other optimization techniques
 * <ul>
 * <li>All pairs of faces are taken one by one from the collection.</li>
 * <li>ICP registration is performed for each pair, but only in one direction (the second to the first)</li>
 * <li>Hausdorff distance for each pair is computed in both direction (A-B and B-A). HD uses POINT_TO_POINT strategy and absolute distances.</li>
 * </ul>
 * Stats for 100 faces WITH CROP:
 * <pre>
 * ICP computation time: 02:27:32,170
 * HD computation time: 02:08:34,001
 * Total computation time: 05:29:55,321
 * </pre>
 * Stats for 100 faces WITHOUT CROP:
 * <pre>
 * ICP computation time: 02:23:06,495
 * HD computation time: 01:59:55,520
 * Total computation time: 05:16:46,154
 * </pre>
 * 
 * @author Radek Oslejsek
 */
public class BatchSimilarityGroundTruth {
    
    private static final String DATA_DIR = "../../analyst-data-antropologie/_ECA";
    private static final String OUTPUT_FILE = "../../SIMILARITY_GROUND_TRUTH.csv";
    private static final int MAX_SAMPLES = 100;
    private static final boolean CROP_DIST_MEASUREMENT = true;
    private static final int CROP_ICP = 1;

    private static FaceService faceService = FaceService.INSTANCE;
    
    /**
     * Main method 
     * @param args Input arguments 
     * @throws IOException on IO error
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<Path> faces = Files.list(new File(DATA_DIR).toPath())
                .filter(f -> f.toString().endsWith(".obj"))
                .sorted()
                .limit(MAX_SAMPLES)
                .collect(Collectors.toList());
        
        double[][] distancesAB = new double[faces.size()][faces.size()];
        double[][] distancesBA = new double[faces.size()][faces.size()];
        String[] names = new String[faces.size()];
        
        long totalTime = System.currentTimeMillis();
        long icpComputationTime = 0;
        long hdComputationTime = 0;
        
        int counter = 0;
        for (int i = 0; i < faces.size(); i++) {
            for (int j = i; j < faces.size(); j++) { // starts with "i"!
                HumanFace priFace = FaceService.INSTANCE.loadTemporaryInMemoryFace(faces.get(i).toFile());
                if (i == j) {
                    names[i] = priFace.getShortName().replaceAll("_01_ECA", "");
                    continue;
                }
                FaceStateServices.updateKdTree(priFace, FaceStateServices.Mode.COMPUTE_ALWAYS);

                HumanFace secFace = FaceService.INSTANCE.loadTemporaryInMemoryFace(faces.get(j).toFile());
                System.out.println(++counter + ": " + priFace.getShortName() + " - " + secFace.getShortName());
                registerAndMeasure(i, j, priFace, secFace, distancesAB);
                
                secFace = FaceService.INSTANCE.loadTemporaryInMemoryFace(faces.get(j).toFile()); // recover the position of the secondary face
                FaceStateServices.updateKdTree(secFace, FaceStateServices.Mode.COMPUTE_ALWAYS);
                System.out.println(++counter + ": " + secFace.getShortName() + " - " + priFace.getShortName());
                registerAndMeasure(i, j, secFace, priFace, distancesBA);
            }
        }
        
        BufferedWriter w = new BufferedWriter(new FileWriter(OUTPUT_FILE));
        w.write("PRI FACE;SEC FACE;reg SEC->PRI dist PRI-SEC;reg SEC->PRI dist SEC-PRI;reg PRI->SEC dist PRI-SEC;reg PRI->SEC dist SEC-PRI");
        w.newLine();
        for (int i = 0; i < faces.size(); i++) {
            for (int j = i; j < faces.size(); j++) {
                if (i == j) {
                    continue;
                }
                w.write(names[i] + ";");
                w.write(names[j] + ";");
                w.write(String.format("%.8f", distancesAB[i][j]) + ";");
                w.write(String.format("%.8f", distancesAB[j][i]) + ";");
                w.write(String.format("%.8f", distancesBA[i][j]) + ";");
                w.write(String.format("%.8f", distancesBA[j][i]) + ";");
                /*
                if (distances[i][j] > distances[j][i]) {
                    w.write(String.format("%.8f", distances[i][j]) + ";");
                    w.write(String.format("%.8f", distances[j][i]) + "");
                } else {
                    w.write(String.format("%.8f", distances[j][i]) + ";");
                    w.write(String.format("%.8f", distances[i][j]) + "");
                }
                */
                w.newLine();
            }
        }
        w.close();
        
        System.out.println();
        Duration duration = Duration.ofMillis(icpComputationTime);
        System.out.println("ICP computation time: " + 
                String.format("%02d:%02d:%02d,%03d", duration.toHoursPart(), duration.toMinutesPart(), duration.toSecondsPart(), duration.toMillisPart()));
        duration = Duration.ofMillis(hdComputationTime);
        System.out.println("HD computation time: " + 
                String.format("%02d:%02d:%02d,%03d", duration.toHoursPart(), duration.toMinutesPart(), duration.toSecondsPart(), duration.toMillisPart()));
        duration = Duration.ofMillis(System.currentTimeMillis() - totalTime);
        System.out.println("Total computation time: " + 
                String.format("%02d:%02d:%02d,%03d", duration.toHoursPart(), duration.toMinutesPart(), duration.toSecondsPart(), duration.toMillisPart()));
        
    }

    protected static void registerAndMeasure(int i, int j, HumanFace priFace, HumanFace trFace, double [][] distances) {
        if (i == j) {
            return;
        }
        
        // transform secondary face
        //long icpTime = System.currentTimeMillis();
        FaceRegistrationServices.alignMeshes(
                trFace, // is transformed
                new IcpConfig(
                        trFace.getMeshModel(),
                        100,
                        false,
                        0.05,
                        new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 0),
                        CROP_ICP));
        //icpComputationTime += System.currentTimeMillis() - icpTime;

        FaceStateServices.updateKdTree(trFace, FaceStateServices.Mode.COMPUTE_ALWAYS);

        //long hdTime = System.currentTimeMillis();
        // compute HD from secondary to primary:
        distances[j][i] = MeshDistanceServices.measure(
                trFace.getMeshModel(),
                new MeshDistanceConfig(
                        MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS,
                        priFace.getKdTree(),
                        false,
                        CROP_DIST_MEASUREMENT)).getDistanceStats().getAverage();

        // compute HD from primary to secondary:
        distances[i][j] = MeshDistanceServices.measure(
                priFace.getMeshModel(),
                new MeshDistanceConfig(
                        MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS,
                        trFace.getKdTree(),
                        false,
                        CROP_DIST_MEASUREMENT)).getDistanceStats().getAverage();

        //hdComputationTime += System.currentTimeMillis() - hdTime;
    }
}
