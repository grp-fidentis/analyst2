package cz.fidentis.analyst.gui.elements.surfacemaskdrawables;

import cz.fidentis.analyst.data.surfacemask.SurfaceMaskRectangle;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Class representing a 2D drawable surface mask line
 * @author Mario Chromik
 */
public class DrawableSurfaceMaskRectangle implements DrawableSurfaceMask2D{
    private SurfaceMaskRectangle mask;

    /**
     * Constructor with one parameter
     * @param mask to draw
     */
    public DrawableSurfaceMaskRectangle(SurfaceMaskRectangle mask) {
        this.mask = mask;
    }

    public SurfaceMaskRectangle getMask() {
        return mask;
    }

    public void setMask(SurfaceMaskRectangle mask) {
        this.mask = mask;
    }

    @Override
    public void draw(Graphics g) {
        if (mask.getOrigin() == null) {
            return;
        }
        if (mask.getSelectedPoint() != null) {
            g.setColor(Color.GREEN);
        } else {
            g.setColor(Color.BLACK);
        }
        g.drawRect(mask.getResize().x - SurfaceMaskRectangle.POINT_SIZE,
                mask.getResize().y - SurfaceMaskRectangle.POINT_SIZE,
                SurfaceMaskRectangle.POINT_SIZE, SurfaceMaskRectangle.POINT_SIZE);
        g.setColor(mask.getColor());
        g.drawRect(Math.min(mask.getOrigin().x, mask.getResize().x), Math.min(mask.getOrigin().y, mask.getResize().y),
                mask.getWidth(), mask.getHeight());
    }
}
