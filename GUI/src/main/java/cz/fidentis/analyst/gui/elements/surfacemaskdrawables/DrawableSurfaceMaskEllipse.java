package cz.fidentis.analyst.gui.elements.surfacemaskdrawables;

import cz.fidentis.analyst.data.surfacemask.SurfaceMaskEllipse;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

/**
 * Class representing a 2D drawable surface mask ellipse
 * @author Mario Chromik
 */
public class DrawableSurfaceMaskEllipse implements DrawableSurfaceMask2D {
    private SurfaceMaskEllipse mask;

    /**
     * Constructor with one parameter
     * @param mask to draw
     */
    public DrawableSurfaceMaskEllipse(SurfaceMaskEllipse mask) {
        this.mask = mask;
    }

    public SurfaceMaskEllipse getMask() {
        return mask;
    }

    public void setMask(SurfaceMaskEllipse mask) {
        this.mask = mask;
    }

    @Override
    public void draw(Graphics g) {
        if (mask.getOrigin() == null) {
            return;
        }
        if (mask.getSelectedPoint() != null) {
            g.setColor(Color.GREEN);
        } else {
            g.setColor(Color.BLACK);
        }
        g.drawRect(mask.getResize().x - mask.POINT_SIZE, mask.getResize().y - mask.POINT_SIZE, mask.POINT_SIZE, mask.POINT_SIZE);
        g.setColor(mask.getColor());
        Graphics2D g2d = (Graphics2D) g.create();
        float[] dashPattern = {20, 20};
        Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL,
                0, dashPattern , 0);
        g2d.setStroke(dashed);
        g2d.drawRect(Math.min(mask.getOrigin().x, mask.getResize().x), Math.min(mask.getOrigin().y, mask.getResize().y), mask.getWidth(), mask.getHeight());
        g.drawOval(Math.min(mask.getOrigin().x, mask.getResize().x), Math.min(mask.getOrigin().y, mask.getResize().y), mask.getWidth(), mask.getHeight());
    }
}
