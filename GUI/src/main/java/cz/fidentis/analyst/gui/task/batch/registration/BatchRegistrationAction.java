package cz.fidentis.analyst.gui.task.batch.registration;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceServices;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.data.mesh.measurement.DistanceRecord;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;
import cz.fidentis.analyst.rendering.RenderingMode;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.List;

/**
 * Action listener for batch registration phase.
 *
 * @author Radek Oslejsek
 */
public class BatchRegistrationAction extends ControlPanelAction<BatchRegistrationPanel> implements HumanFaceListener {

    /**
     * Scene slot used for the rendering the selected template face
     */
    private int faceSceneSlot = -1;

    /**
     * Constructor.
     * A new {@code BatchPanel} is instantiated and added to the {@code topControlPane}
     *
     * @param canvas OpenGL canvas
     * @param task Shared faces across multiple batch tabs.
     *                        The Registration tab recomputes the shared average face stored inside the decorator.
     * @param topControlPane A top component when a new control panel is placed
     */
    public BatchRegistrationAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);

        setControlPanel(new BatchRegistrationPanel(this, task));
        setShowHideCode(
                () -> { // SHOW
                    // provide a code that runs when the panel is focused (selected)
                    showSelectedFace();
                },
                () -> { // HIDE
                    // provide a code that runs when the panel is closed (hidden)
                    hideSelectedFace();
                }
        );
        showSelectedFace();
        getCanvas().getCamera().zoomToFit(getScene());
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        switch (action) {
            case BatchRegistrationPanel.ACTION_COMMAND_AVERAGE_AND_REGISTER:
                averageAndRegister();
                getCanvas().getCamera().zoomToFit(getScene());
                break;
            case BatchRegistrationPanel.ACTION_COMMAND_SHOW_SELECTED_FACE:
                showSelectedFace();
                break;
            case BatchRegistrationPanel.ACTION_COMMAND_EXPORT_AVG_FACE:
                getControlPanel().exportAvgFace();
                break;
            default:
        }
        renderScene();
    }

    @Override
    public void acceptEvent(HumanFaceEvent event) {
        // NOTHING TO DO
    }

    private void averageAndRegister() {
        HumanFace selectedFace = getControlPanel().getSelectedFace();
        if (selectedFace == null) {
            JOptionPane.showMessageDialog(
                    getControlPanel(),
                    "No faces in the data set",
                    null,
                    JOptionPane.WARNING_MESSAGE);
            return;
        }

        if (!getControlPanel().isAvgFaceSelected()) {
            getControlPanel().addAndSelectAvgFace(null); // remove previous average face to compute it from scratch
        }

        double prevPSM = 0;
        int iter = 0;
        while (true) {
            HumanFace newAvgFace = averageAndRegisterIteration(iter++);
            if (newAvgFace == null) { // canceled computation
                return;
            }

            if (iter >= getControlPanel().getMaxRegistrationIterations()) {
                break;
            }

            double newPSM = procrustesSurfaceMetric(selectedFace, newAvgFace);
            double psmDiff = Math.abs(prevPSM - newPSM);

            Logger.print("Iter. " + iter + " (PSM, |diff|): " + newPSM + ", " + psmDiff);

            if (psmDiff < getControlPanel().getAvgSimilarityThreshold()) {
                break;
            }

            selectedFace = newAvgFace;
            prevPSM = newPSM;
        }

        getControlPanel().selectAvgFace(); // select and show final average face
    }

    /**
     *
     * @param iteration iteration number
     * @return computed avg face, null if the process has been canceled
     */
    private HumanFace averageAndRegisterIteration(int iteration) {
        ProgressDialog<MeshModel,HumanFace> progressBar = new ProgressDialog<>(
                getControlPanel(),
                "Registration and average face computation - iteration " + (iteration+1)
        );

        BatchRegistrationTask task = new BatchRegistrationTask(progressBar, getControlPanel(), getCanvas());

        // The following code will be executed AFTER the task is done:
        task.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if ("state".equals(evt.getPropertyName()) && (SwingWorker.StateValue.DONE.equals(evt.getNewValue()))) {
                FaceReference avgFace = TaskService.INSTANCE.getAvgFace(getTask());
                getScene().clearScene();
                faceSceneSlot = -1;
                getCanvas().getCamera().zoomToFit(getScene());
                getControlPanel().addAndSelectAvgFace(avgFace); // show selected face (either the average ot other)
            }
        });

        // Prepare the scene - show the selected face towards which other faces are transformed:
        HumanFace selectedFace = getControlPanel().getSelectedFace();
        if (selectedFace != null) {
            getScene().clearScene(); // scene is recovered automatically - see the event handler above
            faceSceneSlot = getScene().getFreeSlotForFace();

            // Add initial face to the scene:
            getScene().setDrawableFace(faceSceneSlot, selectedFace);
            getScene().getDrawableFace(faceSceneSlot).setTransparency(0.7f);
            getScene().getDrawableFace(faceSceneSlot).setRenderMode(RenderingMode.POINT_CLOUD);
            getScene().getDrawableFace(faceSceneSlot).setColor(DrawableFace.SKIN_COLOR_PRIMARY);

            // locate the camera to the best angle:
            getCanvas().getCamera().zoomToFit(getScene());
            getCanvas().getCamera().rotate(10, -80);
            getCanvas().getCamera().move(40, 20);
        }        

        progressBar.runTask(task);
        FaceReference avgFaceReference = TaskService.INSTANCE.getAvgFace(getTask());
        return FaceService.INSTANCE.getFaceByReference(avgFaceReference);
    }

    /**
     * Implements Equation (3) from the
     * <a href="https://doi.org/10.1371/journal.pone.0150368">A Landmark-Free Method for Three-Dimensional Shape Analysis</a> paper.
     * @param f1 first face
     * @param f2 second face
     * @return equation result
     */
    private static double procrustesSurfaceMetric(HumanFace f1, HumanFace f2) {
        FaceStateServices.updateKdTree(f1, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

        MeshDistances distances1 = MeshDistanceServices.measure(
                f2.getMeshModel(),
                new MeshDistanceConfig(
                        MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS,
                        f1.getKdTree(),
                        false,
                        true));

        List<Double> distancesF2toF1 = distances1.stream()
                .mapToDouble(DistanceRecord::getDistance)
                .filter(Double::isFinite)
                .boxed()
                .toList();
        double d1 = distancesF2toF1.stream().mapToDouble(d -> d*d).sum();
        d1 *= 1.0 / (2.0 * distancesF2toF1.size());

        FaceStateServices.updateKdTree(f2, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        MeshDistances distances2 = MeshDistanceServices.measure(
                f1.getMeshModel(),
                new MeshDistanceConfig(
                        MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS,
                        f2.getKdTree(),
                        false,
                        true));

        List<Double> distancesF1toF2 = distances2.stream()
                .mapToDouble(DistanceRecord::getDistance)
                .filter(Double::isFinite)
                .boxed()
                .toList();
        double d2 = distancesF1toF2.stream().mapToDouble(d -> d*d).sum();
        d2 *= 1.0 / (2.0 * distancesF1toF2.size());

        return Math.sqrt(d1 + d2);
    }

    private void showSelectedFace() {
        HumanFace face = getControlPanel().getSelectedFace();
        if (face != null) {
            if (faceSceneSlot == -1) {
                faceSceneSlot = getScene().getFreeSlotForFace();
            }
            getScene().setDrawableFace(faceSceneSlot, face);
            getScene().setFaceAsPrimary(faceSceneSlot);
            getScene().showDrawableFace(faceSceneSlot, true);
        }
    }

    private void hideSelectedFace() {
        if (faceSceneSlot >= 0) {
            getScene().showDrawableFace(faceSceneSlot, false);
        }
    }
}
