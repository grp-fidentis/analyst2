package cz.fidentis.analyst.gui.elements.histogram.components;

import cz.fidentis.analyst.gui.elements.histogram.ActionEmitter;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

/**
 * Class for grouping {@code handle}, {@code indicator} and {@code ghost} components together.
 * It makes them draggable along the X axis and provides related convenient methods.
 *
 * @author Jakub Nezval
 */
public class Bound {

    /*
     * Actions triggered by this component
     */
    public static final String ACTION_COMMAND_BOUND_MOVED = "bound moved";

    private final ActionEmitter actionEmitter = new ActionEmitter();

    private final JButton handle;
    private final JPanel indicator;
    private final JPanel ghost;

    private final int handleOffset;
    private final int originalValue;

    private int xMinCoordinate = Integer.MIN_VALUE;
    private int xMaxCoordinate = Integer.MAX_VALUE;

    /**
     * Creates a Bound from given {@code handle}, {@code indicator} and {@code ghost}.
     * Dragging {@code handle} moves {@code handle} and {@code indicator} - it leaves {@code ghost} behind,
     * releasing mouse moves {@code ghost}.
     * By default, range of a Bound is ⟨{@link Integer#MIN_VALUE}, {@link Integer#MAX_VALUE}⟩.
     *
     * @param handle handle button
     * @param indicator indicator component
     * @param ghost ghost component (preferably the same size as {@code indicator})
     * @see Bound#setBoundRange(int, int)
     */
    public Bound(JButton handle, JPanel indicator, JPanel ghost) {
        this.handle = handle;
        this.indicator = indicator;
        this.ghost = ghost;

        this.handleOffset = indicator.getX() - handle.getX();
        this.originalValue = indicator.getX();

        var dragging = new MouseInputAdapter() {
            private int xOffset;

            @Override
            public void mousePressed(MouseEvent e) {
                this.xOffset = e.getX();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                var xNew = fitToRange(e.getX(), this.xOffset);

                handle.setLocation(xNew - handleOffset, handle.getY());
                indicator.setLocation(xNew, indicator.getY());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                setValue(fitToRange(e.getX(), this.xOffset));
                actionEmitter.triggerActionEvent(
                        new ActionEvent(
                                e.getSource(),
                                ActionEvent.ACTION_PERFORMED,
                                Bound.ACTION_COMMAND_BOUND_MOVED
                        )
                );
            }
        };

        this.handle.addMouseListener(dragging);
        this.handle.addMouseMotionListener(dragging);
    }

    /**
     * Ensures {@code rawValue} is within the {@link Bound} range.
     * If {@code rawValue} exceeds the range, the nearest boundary value is returned.
     *
     * @param rawValue raw value
     * @param offset offset
     * @return value within {@link Bound} range
     * @see Bound#setBoundRange(int, int)
     */
    private int fitToRange(int rawValue, int offset) {
        var value = this.handle.getX() + rawValue - offset + handleOffset;

        if (value < this.xMinCoordinate) {
            return this.xMinCoordinate;
        }
        if (value > this.xMaxCoordinate) {
            return this.xMaxCoordinate;
        }
        return value;
    }

    /**
     * Set constraints on movement along the X axis.
     *
     * @param min minimal value bound can have
     * @param max maximal value bound can have
     * @throws IllegalArgumentException if current value is outside of ⟨{@code min}, {@code max}⟩ range
     * @see Bound#getValue()
     */
    public void setBoundRange(int min, int max) throws IllegalArgumentException {
        if (this.getValue() < min || this.getValue() > max) {
            throw new IllegalArgumentException("Bound range doesn't include current value");
        }

        this.xMinCoordinate = min;
        this.xMaxCoordinate = max;
    }

    /**
     * Obtain value of a Bound. Equal to {@code indicator.getX()}.
     *
     * @return value from bound range
     * @see Bound#Bound(JButton, JPanel, JPanel)
     * @see Bound#setBoundRange(int, int)
     */
    public int getValue() {
        return this.indicator.getX();
    }

    /**
     * Set Bound value. This will also position a Bound accordingly.
     *
     * @param value value
     * @throws IllegalArgumentException if {@code value} is outside the Bound range
     */
    public void setValue(int value) throws IllegalArgumentException {
        if (value < this.xMinCoordinate || this.xMaxCoordinate < value) {
            throw new IllegalArgumentException("value is outside the Bound range");
        }

        this.handle.setLocation(value - this.handleOffset, handle.getY());
        this.indicator.setLocation(value, indicator.getY());
        this.ghost.setLocation(value, ghost.getY());
    }

    /**
     * Set Bound value to its initial value at the time of creation.
     */
    public void reset() {
        this.setValue(this.originalValue);
    }

    /**
     * Delegate of {@link ActionEmitter}.
     *
     * @param listener listener
     * @return {@code true} if {@code listener} was successfully added
     * @see ActionEmitter#addActionListener(ActionListener)
     */
    public boolean addActionListener(ActionListener listener) {
        return this.actionEmitter.addActionListener(listener);
    }

    /**
     * Delegate of {@link ActionEmitter}.
     *
     * @param listener listener
     * @return {@code true} if {@code listener} was successfully removed
     * @see ActionEmitter#removeActionListener(ActionListener)
     */
    public boolean removeActionListener(ActionListener listener) {
        return this.actionEmitter.removeActionListener(listener);
    }
}
