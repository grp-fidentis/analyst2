package cz.fidentis.analyst.gui.task;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * An abstract class for control panels that can be docked in the {@link TaskControlPane}.
 * 
 * @author Radek Oslejsek
 */
public abstract class ControlPanel extends JPanel {

    /**
     * Horizontal space for right-hand side control panels of the project and analytical windows.
     * The wider, the less space is allocated (dynamically) to 3D canvas.
     */
    public static final int CONTROL_PANEL_WIDTH = 900;
    
    /**
     * Associated action listener (controller)
     */
    private final ActionListener action;
    
    /**
     * Returns panel's icon.
     * 
     * @return panel's icon
     */
    public abstract ImageIcon getIcon();
    
    /**
     * Constructor.
     * 
     * @param action action listener (controller) which is invoked when an extrinsic state 
     * of the control panel is changed.
     * @throws IllegalArgumentException if the {@code action} is missing
     */
    public ControlPanel(ActionListener action) {
        if (action == null) {
            throw new IllegalArgumentException("action");
        }
        this.action = action;
    }
    
    public ActionListener getActionListener() {
        return action;
    }
    
    /**
     * Creates and returns action listener that can be connected with a low-level 
     * GUI element (e.g., a button). Action event of the low-level element is then
     * re-directed to the given {@code ControlPanelAction} as given command.
     * 
     * @param action An instance of the {@link ControlPanelAction}
     * @param command Control panel command
     * @return Action listener
     */
    protected final ActionListener createListener(ActionListener action, String command) {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action.actionPerformed(new ActionEvent(
                        e.getSource(), 
                        ActionEvent.ACTION_PERFORMED, 
                        command)
                ); 
            }  
        };
    }
    
    /**
     * Creates and returns action listener that can be connected with a low-level 
     * GUI element (e.g., a button). Action event of the low-level element is then
     * re-directed to the given {@code ControlPanelAction} as given command.
     * The listener may also carry additional data as a payload.
     * 
     * @param action An instance of the {@link ControlPanelAction}
     * @param command Control panel command
     * @param data Payload data of the action listener
     * @return Action listener
     */
    protected final ActionListener createListener(ActionListener action, String command, Object data) {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action.actionPerformed(new LoadedActionEvent(
                        e.getSource(), 
                        ActionEvent.ACTION_PERFORMED, 
                        command,
                        data)
                ); 
            }  
        };
    }
}
