package cz.fidentis.analyst.gui.elements;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * SpinSlider implements a combination of horizontal slider and input text field (Spinner).
 * The slider and the spinner are synchronized automatically.
 * 
 * @author Radek Oslejsek
 */
public class SpinSlider extends JPanel {
    
    /**
     * SpinSlider type
     * 
     * @author Radek Oslejsek
     */
    public enum ValueType {
        INTEGER,
        DOUBLE,
        PERCENTAGE
    };
    
    private final JSlider slider = new JSlider();
    private JSpinner spinner;
    private ValueType type;
    
    private boolean continuousSync;
    
    /**
     * Listener for continuous synchronization of the slider and the spinner.
     * The spinner value is updated whenever the slider is moved.
     */
    private ChangeListener changeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            spinner.setValue(slider.getValue());
        }
    };
    
    /**
     * Listener for postponed synchronization of the slider and the spinner.
     * The spinner value remains unchanged until the mouse button is released.
     */
    private MouseListener mouseListener = new MouseAdapter() {
        @Override
        public void mouseReleased(MouseEvent e) {
            spinner.setValue(slider.getValue());
        }
    };
    
    
    /**
     * Constructor that creates percentage spin-slider.
     * Call {@link #initDouble(double, double, double, int)} 
     * or {@link #initInteger(int, int, int, int)} to change it.
     */
    public SpinSlider() {
        initPercentage(100);
        initComponents();
        setContinuousSync(true); // update input field on slider's change and inform external listeners
        slider.setFont(new java.awt.Font("Dialog", 1, 0)); // hide numbers
        
        setBackground(Color.LIGHT_GRAY);
    }
    
    /**
     * Returns current value reflecting the slider's position.
     * Even if the postponed synchronization is turned on (see {@link #setContinuousSync(boolean)}),
     * the correct value is computed and returned anyway.
     * <p>
     * Based on the type, the return value is
     * <ul>
     * <li>{@code INTEGER}: An {@code Integer} from set range.</li>
     * <li>{@code DOUBLE}: An {@code Double} from set range.</li>
     * <li>{@code PERCENTAGE}: An {@code Integer} between 0 and 100.</li>
     * </ul>
     * <p>
     * Usage:
     * <ul>
     * <li>After {@code initDouble()}: {@code double var = (Double) spinSlider.getValue()}.</li>
     * <li>After {@code initInteger()}: {@code double var = (Integer) spinSlider.getValue()}.</li>
     * <li>After {@code initPercentage()}: {@code double var = (Integer) spinSlider.getValue()}.</li>
     * </ul>
     * 
     * @return Current value
     */
    public Number getValue() {
        if (isContinuousSync()) { // get prices value from the spinner
            return (this.type == ValueType.DOUBLE)
                    ? ((DoubleSpinner) spinner).getDoubleValue()
                    : (Number) spinner.getValue();
        } else { // the spinner hold old value while the slider is moving => get slider's value
            return (this.type == ValueType.DOUBLE)
                    ? ((DoubleSpinner) spinner).convertValue(slider.getValue())
                    : slider.getValue();
        }
    }
    
    /**
     * Sets the value. 
     * 
     * @param value A new value
     */
    public void setValue(Number value) {
        spinner.setValue(value);
    }
    
    /**
     * Initializes this spin-slider to integers.
     * <b>Removed all listeners!</b>
     * 
     * @param value Initial value
     * @param min Minimum value
     * @param max Maximum value
     * @param stepSize Spin step size
     */
    public void initInteger(int value, int min, int max, int stepSize) {
        remove(slider);
        if (spinner != null) {
            remove(spinner);
        }
        
        this.type = ValueType.INTEGER;
        spinner = new JSpinner();
        spinner.setModel(new SpinnerNumberModel(value, min, max, stepSize));
        spinner.setEditor(new JSpinner.NumberEditor(spinner));
        slider.setMinimum(min);
        slider.setMaximum(max);
        slider.setValue(value);
        
        initComponents();
        setValue(value);
    }

    /**
     * Initializes this spin-slider to doubles.
     * <b>Removes all listeners!</b>
     * 
     * @param value Initial value
     * @param min Minimum value
     * @param max Maximum value
     * @param fractionDigits precision of floating numbers, i.e., 
     *          the number of digits allowed after the floating dot. 
     *          Must be bigger than zero.
     */
    public void initDouble(double value, double min, double max, int fractionDigits) {
        remove(slider);
        if (spinner != null) {
            remove(spinner);
        }
        
        this.type = ValueType.DOUBLE;
        DoubleSpinner ds = new DoubleSpinner(value, min, max, fractionDigits);
        spinner = ds;
        slider.setMinimum(ds.getIntegerMinimum());
        slider.setMaximum(ds.getIntegerMaximum());
        slider.setValue(ds.getIntegerValue());
        
        initComponents();
        setValue(value);
    }
    
    /**
     * Initializes this spin-slider to percents.
     * 
     * @param value Initial value between 0 and 100.
     */
    public void initPercentage(int value) {
        initPercentage(value, 0, 100);
    }
    
    /**
     * Initializes this spin-slider to percents in given range.
     * 
     * @param value Initial value between 0 and 100.
     * @param min min value in the range 0..100
     * @param max max value in the range 0..100
     */
    public void initPercentage(int value, int min, int max) {
        if (min > max || min < 0 || max > 100) {
            throw new IllegalArgumentException("min/max: " + min + "/" + max);
        }
        
        remove(slider);
        if (spinner != null) {
            remove(spinner);
        }
        
        this.type = ValueType.PERCENTAGE;
        spinner = new JSpinner();
        spinner.setModel(new SpinnerNumberModel(value, 0, 100, 1));
        spinner.setEditor(new JSpinner.NumberEditor(spinner, "0'%'"));
        slider.setMinimum(min);
        slider.setMaximum(max);
        slider.setValue(value);
        
        initComponents();
        setValue(value);
    }
    
    /**
     * The slider is on the left, followed by ti input field, by default.
     * This method switches the order.
     */
    //public void setSliderEast() {
    //    remove(slider);
    //    add(slider);
    //}
    
    /**
     * If {@code true}, then the spinner is updated continuously during the slider move.
     * Otherwise, the spinner remains unchanged until the mouse key is released.
     * 
     * @param continuousSync Whether to update the spinner continuously.
     */
    public final void setContinuousSync(boolean continuousSync) {
        this.continuousSync = continuousSync;
        if (this.continuousSync) {
            slider.removeMouseListener(mouseListener);
            slider.addChangeListener(changeListener);
        } else {
            slider.addMouseListener(mouseListener);
            slider.removeChangeListener(changeListener);
        }
    }
    
    /**
     * If {@code true}, then the spinner is updated continuously during the slider move.
     * Otherwise, the spinner remains unchanged until the mouse key releases.
     * 
     * @return {@code true} if the slider and the spinner are synchronized continuously. 
     */
    public boolean isContinuousSync() {
        return continuousSync;
    }
    
    /**
     * Be informed when the spinner's value changes.
     * See also {@link #setContinuousSync(boolean)}.
     * Event's source is set to {@code JSpinner}. 
     * For the double type of the spin-slider, it is {@code DoubleSpinner}
     *
     * @param listener the action listener to be added
     */
    public synchronized void addSpinnerListener(ActionListener listener) {
        spinner.addChangeListener((ChangeEvent e) -> {
            listener.actionPerformed(new ActionEvent(spinner, ActionEvent.ACTION_PERFORMED, null));
        });
    }
    
    /**
     * Be informed when the slider changes.
     * See also {@link #setContinuousSync(boolean)}.
     * Event's source is set to {@code JSlider}
     *
     * @param listener the action listener to be added
     */
    public synchronized void addSliderListener(ActionListener listener) {
        slider.addChangeListener((ChangeEvent e) -> {
            listener.actionPerformed(new ActionEvent(slider, ActionEvent.ACTION_PERFORMED, null));
        });
    }
    
    @Override
    public void setEnabled(boolean enabled) {
        slider.setEnabled(enabled);
        spinner.setEnabled(enabled);
    }
    
    public JSpinner getSpinner() {
        return spinner;
    }
    
    public JSlider getSlider() {
        return slider;
    }
    
    protected final void initComponents() {
        //this.setLayout(new FlowLayout());
        this.setLayout(new BorderLayout());
        
        add(slider, BorderLayout.LINE_START);
        add(spinner, BorderLayout.LINE_END);
        
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSpinner s = (JSpinner) e.getSource();
                if (s.getClass() == DoubleSpinner.class) {
                    slider.setValue(((DoubleSpinner) s).getIntegerValue());
                } else {
                    slider.setValue((Integer) s.getValue());
                }
            }
        });
        
        slider.setBorder(new EmptyBorder(0,0,0,0));
        spinner.setBorder(new EmptyBorder(0,0,0,0));
        this.setBorder(new EmptyBorder(0,0,0,0));
    } 
    
    /**
     * Sets the width of slider.
     * @param size slider width
     */
    public void setSliderSize(int size) {
        slider.setPreferredSize(new Dimension(size, 20));
    }
}
