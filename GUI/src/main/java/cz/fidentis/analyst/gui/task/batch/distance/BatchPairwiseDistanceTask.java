package cz.fidentis.analyst.gui.task.batch.distance;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceMemoryManager;
import cz.fidentis.analyst.engines.face.batch.distance.BatchFaceDistanceServices;
import cz.fidentis.analyst.engines.face.batch.distance.BatchPairwiseDistance;
import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;

import java.util.List;

/**
 * A task that computes similarity of a set of faces by applying two-way (from A to B and vice versa)
 * distance measurement to all pairs A and B if the set.
 * The average face, if exists, is skipped from the similarity measurement
 * (i.e., we do not measure its distance to other faces).
 * The exact computation parameters are taken from the {@code BatchDistancePanel}.
 * 
 * @author Radek Oslejsek
 */
public class BatchPairwiseDistanceTask extends BatchDistanceTask {

    private final Stopwatch totalTime = new Stopwatch("Total computation time:\t");
    private final Stopwatch distComputationTime = new Stopwatch("Distance measurement time:\t");
    private final Stopwatch loadTime = new Stopwatch("Disk access time:\t");

    private final boolean crop;
    
    /**
     * Constructor.
     * 
     * @param progressDialog A window that show the progress of the computation. Must not be {@code null}
     * @param controlPanel A control panel with computation parameters. Must not be {@code null}
     * @param crop If {@code true}, then the measured faces are auto-cropped to the surface of the average face.
     *        This option slows down the computation even more, but enables us to compare 
     *        the results of this pairwise comparison other methods based of indirect measurement
     *        (i.e., the methods that use the average face as a "gauge").
     *        This option is ignored if there is average face computed.
     **/
    public BatchPairwiseDistanceTask(ProgressDialog<Void, Integer> progressDialog, BatchDistancePanel controlPanel, boolean crop) {
        super(progressDialog, controlPanel);
        this.crop = crop && controlPanel.getTask().getAverageFace() != null;
    }

    @Override
    protected Void doInBackground() throws Exception {

        FaceService.INSTANCE.setDumpStrategy(HumanFaceMemoryManager.Strategy.MRU);

        Task task = getControlPanel().getTask();

        List<FaceReference> faceReferences = task.getFaces();

        totalTime.start();

        setBatchDistance(BatchFaceDistanceServices.initDirectMeasurementWithCrop(
                crop ? FaceService.INSTANCE.getFaceByReference(task.getAverageFace()) : null,
                faceReferences.size()));

        int counter = 0;
        for (int i = 0; i < faceReferences.size(); i++) {
            for (int j = i; j < faceReferences.size(); j++) { // starts with "i"!
                
                if (isCancelled()) { // the user canceled the process
                    return null;
                }

                // (Re-)load the primary face (it could be dumped in the meantime) and load the second face
                loadTime.start();
                HumanFace priFace = FaceService.INSTANCE.getFaceByReference(faceReferences.get(i));
                HumanFace secFace = FaceService.INSTANCE.getFaceByReference(faceReferences.get(j));
                loadTime.stop();

                distComputationTime.start();
                ((BatchPairwiseDistance) getBatchDistance()).measure(priFace, secFace, i, j);
                distComputationTime.stop();
                
                // update progress bar
                getProgressDialog().setValue((int) Math.round(100.0 * ++counter / numCycles(faceReferences.size())));
            }
        }
        
        totalTime.stop();

        printTimeStats();

        return null;
    }

    protected void printTimeStats() {
        Logger.print(distComputationTime.toString());
        Logger.print(loadTime.toString());
        Logger.print(totalTime.toString());
    }
    
    protected double numCycles(int nFaces) {
        return 0.5 * nFaces * (nFaces + 1);
    }
}
