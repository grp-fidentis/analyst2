package cz.fidentis.analyst.gui.app.tools;

import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceServices;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

/**
 * For 500 faces:
 * <pre>
 * Old algorithm - random sampling   : 00:02:40,033 5.863821807746459
 * Old algorithm - curvature sampling: 00:02:13,161 7.078118071583159
 * Old algorithm - uniform sampling  : 00:04:06,655 4.40765867453703
 * New algorithm - without weights   : 00:05:14,265 4.3880481069874095
 * New algorithm - with weights      : 00:08:55,655 5.290415040372108
 * </pre>
 * 
 * For 100 faces:
 * <pre>
 * Old algorithm - random sampling   : 00:00:28,159 6.195494004642175
 * Old algorithm - curvature sampling: 00:00:27,188 7.1732047116681334
 * Old algorithm - uniform sampling  : 00:00:46,303 4.807517763293901
 * New algorithm - without weights   : 00:01:01,388 4.825086789583008
 * New algorithm - with weights      : 00:01:41,233 5.007362521377766
 * </pre>
 * 
 * @author Radek Oslejsek
 */
public class TestSymmetryAlgorithms {
    private static final String DATA_DIR = "../analyst-data-antropologie/_ECA";
    private static final int MAX_SAMPLES = 500;
    
    /**
     * Main method 
     * @param args Input arguments 
     * @throws IOException on IO error
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<Path> faces = Files.list(new File(DATA_DIR).toPath())
                .filter(f -> f.toString().endsWith(".obj"))
                .sorted()
                .limit(MAX_SAMPLES)
                .collect(Collectors.toList());
        
        Stopwatch swOld0 = new Stopwatch("Old algorithm - random sampling   ");
        Stopwatch swOld1 = new Stopwatch("Old algorithm - curvature sampling");
        Stopwatch swOld2 = new Stopwatch("Old algorithm - uniform sampling  ");
        Stopwatch swNew1 = new Stopwatch("New algorithm - without weights   ");
        Stopwatch swNew2 = new Stopwatch("New algorithm - with weights      ");

        double[] statsOld0 = new double[]{0, 0, 0, 0};
        double[] statsOld1 = new double[]{0, 0, 0, 0};
        double[] statsOld2 = new double[]{0, 0, 0, 0};
        double[] statsNew1 = new double[]{0, 0, 0, 0};
        double[] statsNew2 = new double[]{0, 0, 0, 0};
        
        for (int i = 0; i < faces.size(); i++) {
            System.out.println(i + ": " + faces.get(i));
            
            HumanFace face = FaceService.INSTANCE.loadTemporaryInMemoryFace(faces.get(i).toFile());
            FaceStateServices.updateCurvature(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

            DoubleSummaryStatistics stats;
            
            //////////////////////////////////////////////////////////////
            SymmetryConfig estimator2 = new SymmetryConfig(
                    SymmetryConfig.Method.ROBUST_POINT_CLOUD,
                    new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SPACE, 100),
                    1000);
            swNew1.start();
            face.setSymmetryPlane(SymmetryServices.estimate(face.getMeshModel(), estimator2));
            swNew1.stop();
            
            stats = checkPecision(face);
            if (stats != null) {
                statsNew1[0] += stats.getMin();
                statsNew1[1] += stats.getMax();
                statsNew1[2] += stats.getAverage();
                statsNew1[3] += stats.getSum();
            }
            
            //////////////////////////////////////////////////////////////
            SymmetryConfig estimator3 = new SymmetryConfig(
                    SymmetryConfig.Method.ROBUST_MESH,
                    new PointSamplingConfig(PointSamplingConfig.Method.UNIFORM_SPACE, 200),
                    200);
            swNew2.start();
            face.setSymmetryPlane(SymmetryServices.estimate(face.getMeshModel(), estimator3));
            swNew2.stop();
            
            stats = checkPecision(face);
            if (stats != null) {
                statsNew2[0] += stats.getMin();
                statsNew2[1] += stats.getMax();
                statsNew2[2] += stats.getAverage();
                statsNew2[3] += stats.getSum();
            }
        }
        
        System.out.println();
        System.out.println(swOld0 + " " + (statsOld0[1] / faces.size()));
        System.out.println(swOld1 + " " + (statsOld1[1] / faces.size()));
        System.out.println(swOld2 + " " + (statsOld2[1] / faces.size()));
        System.out.println(swNew1 + " " + (statsNew1[1] / faces.size()));
        System.out.println(swNew2 + " " + (statsNew2[1] / faces.size()));
    }

    protected static DoubleSummaryStatistics checkPecision(HumanFace face) {
        MeshModel clone = MeshFactory.cloneMeshModel(face.getMeshModel());
        
        if (face.getSymmetryPlane() == null) {
            System.out.println("No plane: " + face.getShortName());
            return null;
        }
        
        for (MeshFacet facet: clone.getFacets()) { // invert mesh
            facet.getVertices().parallelStream().forEach(v -> {
                v.getPosition().set(face.getSymmetryPlane().reflectPointOverPlane(v.getPosition()));
            });
        }

        return MeshDistanceServices.measure(
                clone,
                new MeshDistanceConfig(
                        MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS,
                        face.getKdTree(),
                        false,
                        true)).getDistanceStats();
    }
}
