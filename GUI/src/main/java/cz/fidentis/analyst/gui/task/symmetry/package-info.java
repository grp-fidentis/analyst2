/**
 * Control panel for the symmetry plane computation and fine tuning.
 */
package cz.fidentis.analyst.gui.task.symmetry;
