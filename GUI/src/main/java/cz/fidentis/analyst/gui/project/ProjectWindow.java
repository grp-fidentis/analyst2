package cz.fidentis.analyst.gui.project;

import cz.fidentis.analyst.gui.task.ControlPanel;
import cz.fidentis.analyst.gui.app.OutputWindowThread;
import cz.fidentis.analyst.gui.task.TaskControlPane;
import cz.fidentis.analyst.gui.project.faceinfo.FacesInfoAction;
import cz.fidentis.analyst.gui.project.filter.ProjectFilteringAction;
import cz.fidentis.analyst.project.ProjectService;
import java.io.File;
import javax.swing.GroupLayout;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

/**
 * The main panel enabling analysts to select the primary and secondary faces,
 * and to perform basic batch processing. This panel also serves as an entry
 * point for detailed face analysis and face-to-face comparison.
 *
 * @author Matej Kovar
 * @author Radek Oslejsek
 */
@ConvertAsProperties(
        dtd = "-//cz.fidentis.analyst.gui//Dashboard//EN",
        autostore = false
)
@TopComponent.Description(
        preferredID = "ProjectWindow",
        iconBase="checker.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "editor", openAtStartup = true)
@ActionID(category = "Window", id = "cz.fidentis.analyst.gui.ProjectWindow")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_ProjectWindowAction",
        preferredID = "ProjectWindow"
)
@Messages({
    "CTL_ProjectWindowAction=Project",
    "CTL_ProjectWindowTopComponent=Project",
    "HINT_ProjectWindowTopComponent=This is a Project window"
})
public final class ProjectWindow extends TopComponent {
    
    private final ProjectPanel projectPanel;
    private final TaskControlPane controlPanel;
    private final JScrollPane controlPanelScrollPane;
    
    public static final int CONTROL_PANEL_TAB_POSITION_FILTER_PANEL = 1;
    
    /**
     * Project Top Component
     */
    public ProjectWindow() {
        setName(Bundle.CTL_ProjectWindowTopComponent());
        setToolTipText(Bundle.HINT_ProjectWindowTopComponent());
        putClientProperty(TopComponent.PROP_CLOSING_DISABLED, Boolean.TRUE);
        putClientProperty(TopComponent.PROP_DRAGGING_DISABLED, Boolean.TRUE);
        putClientProperty(TopComponent.PROP_UNDOCKING_DISABLED, Boolean.TRUE);
        
        projectPanel = new ProjectPanel();
        controlPanel = new TaskControlPane();
        controlPanelScrollPane = new JScrollPane(controlPanel);
        
        initComponents();
        
        OutputWindowThread.execute();
        
        // initiate and add control panels:
        new FacesInfoAction(this.projectPanel, this.controlPanel); 
        new ProjectFilteringAction(this.projectPanel, this.controlPanel); 
        
        this.openAtTabPosition(0);
        this.toFront();
        this.requestActive();
        
        // Re-open recent project
        
    }
    
    /**
     * Re-opens the most recent project
     */
    @Override
    protected void componentOpened() {
        File recentProjectDir = ProjectService.INSTANCE.getRecentProjectDir();
        if (recentProjectDir != null) {
            projectPanel.openProject(recentProjectDir);
        }
    }
    
    @Override
    protected void componentClosed() {
        // TODO add custom code on component closing
    }
    
    /**
     * Returns the project panel, i.e., the left-hand side of the window.
     * 
     * @return the project panel
     */
    public ProjectPanel getProjectPanel() {
        return projectPanel;
    }
    
    private void initComponents() {
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(projectPanel, GroupLayout.DEFAULT_SIZE, 651, Short.MAX_VALUE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
//                                .addComponent(renderingToolBar, GroupLayout.PREFERRED_SIZE, RenderingToolBar.WIDTH, GroupLayout.PREFERRED_SIZE)
                                .addComponent(
                                        controlPanelScrollPane,
                                        ControlPanel.CONTROL_PANEL_WIDTH, 
                                        ControlPanel.CONTROL_PANEL_WIDTH, 
                                        ControlPanel.CONTROL_PANEL_WIDTH
                                )
                        )
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createBaselineGroup(true, true)
                                        .addComponent(projectPanel)
//                                        .addComponent(renderingToolBar)
                                        .addComponent(controlPanelScrollPane)
                                ))
        );
    }
    
    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }
}
