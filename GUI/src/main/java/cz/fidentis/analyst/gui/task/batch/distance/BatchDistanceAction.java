package cz.fidentis.analyst.gui.task.batch.distance;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.engines.face.batch.distance.BatchFaceDistanceServices;
import cz.fidentis.analyst.gui.elements.ProgressDialog;
import cz.fidentis.analyst.gui.project.ProjectWindow;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.project.Task;
import org.openide.filesystems.FileChooserBuilder;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Action listener for batch registration phase.
 * 
 * @author Radek Oslejsek
 */
public class BatchDistanceAction extends ControlPanelAction<BatchDistancePanel> implements HumanFaceListener {
    
    private int faceSceneSlot = -1;
    
    private double[][] distances;
    private double[][] deviations;
    
    /**
     * Constructor.
     * A new {@code BatchDistancePanel} is instantiated and added to the {@code topControlPane}
     *
     * @param canvas OpenGL canvas
     * @param task Shared faces across multiple batch tabs.
     *                        The Distance tab checks for changes in the average face stored in the decorator.
     * @param topControlPane A top component when a new control panel is placed
     */
    public BatchDistanceAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);
        setControlPanel(new BatchDistancePanel(this, task));
        setShowHideCode(
                () -> { // SHOW
                    // provide a code that runs when the panel is focused (selected)

                    // The average face might have changed. The showSelectedFace() is invoked automatically
                    getControlPanel().updateDatasetMenu();
                },
                () -> { // HIDE
                    // provide a code that runs when the panel is closed (hidden)
                    hideSelectedFace();
                }
        );
        showSelectedFace();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
         switch (action) {
            case BatchDistancePanel.ACTION_COMMAND_COMPUTE_SIMILARITY:
                computeSimilarity();
                break;
            case BatchDistancePanel.ACTION_COMMAND_SHOW_SELECTED_FACE:
                showSelectedFace();
                break;
            case BatchDistancePanel.ACTION_COMMAND_EXPORT_SIMILARITY:
                exportDistanceResults();
                break;
            default:
         }
        renderScene();
    }

    @Override
    public void acceptEvent(HumanFaceEvent event) {
        // NOTHING TO DO
    }
    
    private void computeSimilarity() {
        ProgressDialog<Void, Integer> progressBar = new ProgressDialog<>(getControlPanel(), "Similarity computation");
        final BatchDistanceTask task;
        
        switch (getControlPanel().getSimilarityStrategy()) {
            case BatchDistancePanel.SIMILARITY_PAIRWISE:
                task = new BatchPairwiseDistanceTask(progressBar, getControlPanel(),false);
                break;
            case BatchDistancePanel.SIMILARITY_PAIRWISE_CROP:
                task = new BatchPairwiseDistanceTask(progressBar, getControlPanel(), true);
                break;
            case BatchDistancePanel.SIMILARITY_AVG_REL_DIST:
                task = new BatchIndirectDistanceTask(progressBar, getControlPanel(), BatchFaceDistanceServices.DistanceStrategy.NEAREST_NEIGHBORS_RELATIVE_DISTANCE, null);
                break;
            case BatchDistancePanel.SIMILARITY_AVG_VEC:
                task = new BatchIndirectDistanceTask(progressBar, getControlPanel(), BatchFaceDistanceServices.DistanceStrategy.NEAREST_NEIGHBORS_DIRECT_DISTANCE, null);
                break;
            case BatchDistancePanel.SIMILARITY_AVG_COMBO:
                task = new BatchIndirectDistanceTask(progressBar, getControlPanel(), BatchFaceDistanceServices.DistanceStrategy.NEAREST_NEIGHBORS_COMBINED_DISTANCE, null);
                break;
            case BatchDistancePanel.SIMILARITY_AVG_RAY_CASTING:
                task = new BatchIndirectDistanceTask(progressBar, getControlPanel(), BatchFaceDistanceServices.DistanceStrategy.PROJECTION, null);
                break;
            case BatchDistancePanel.SIMILARITY_AVG_RAY_CASTING_GPU:
                task = new BatchIndirectDistanceTask(progressBar, getControlPanel(), BatchFaceDistanceServices.DistanceStrategy.PROJECTION, getCanvas().getGLCanvas().getContext());
                break;
            default:
                return;
        }
        
        task.addPropertyChangeListener((PropertyChangeEvent evt) -> { // when done ...
            if ("state".equals(evt.getPropertyName()) && (SwingWorker.StateValue.DONE.equals(evt.getNewValue()))) {
                double[][] result = task.getDistSimilarities();
                if (result != null) {
                    this.distances = result;
                    this.deviations = task.getDistDeviations();
                }
                getControlPanel().enableSimilarityExport(this.distances != null);
           }
        });
        
        progressBar.runTask(task);
    } 

    private void showSelectedFace() {
        HumanFace face = getControlPanel().getSelectedFace();
        if (face != null) {
            if (faceSceneSlot == -1) {
                faceSceneSlot = getScene().getFreeSlotForFace();
            }
            getScene().setDrawableFace(faceSceneSlot, face); 
            getScene().setFaceAsPrimary(faceSceneSlot);
            getScene().showDrawableFace(faceSceneSlot, true);
        }
    }

    private void hideSelectedFace() {
        if (faceSceneSlot >= 0) {
            getScene().showDrawableFace(faceSceneSlot, false);
        }
    }

    private void exportDistanceResults() {
        if (this.distances == null) {
            return;
        }
        
        File file = new FileChooserBuilder(ProjectWindow.class)
                .setTitle("Specify a file to save")
                .setDefaultWorkingDirectory(new File(System.getProperty("user.home")))
                .setFileFilter(new FileNameExtensionFilter("CSV files (*.csv)", "csv"))
                .showSaveDialog();
        
        if (file != null) {
            try (BufferedWriter w = new BufferedWriter(new FileWriter(file))) {
                w.write("PRI FACE;SEC FACE;AVG dist PRI-SEC;Std. deviation PRI-SEC;AVG dist SEC-PRI;Std. deviation SEC-PRI");
                w.newLine();
                
                for (int i = 0; i < distances.length; i++) {
                    for (int j = i; j < distances.length; j++) {
                        if (i == j) {
                            continue;
                        }
                        w.write(getShortName(getControlPanel().getTask().getFaces().get(i).getFaceFile()) + ";");
                        w.write(getShortName(getControlPanel().getTask().getFaces().get(j).getFaceFile()) + ";");
                        w.write(String.format("%.8f", distances[i][j]) + ";");
                        w.write(String.format("%.8f", deviations[i][j]) + ";");
                        w.write(String.format("%.8f", distances[j][i]) + ";");
                        w.write(String.format("%.8f", deviations[j][i]) + ";");
                        w.newLine();
                    }
                }
            } catch (IOException ex) {
                Logger.print(ex.toString());
            }
        }
    }
    
    private static String getShortName(File file) {
        String name = file.toString();
        name = name.substring(name.lastIndexOf(File.separatorChar) + 1);
        return name;
    }

    
}
