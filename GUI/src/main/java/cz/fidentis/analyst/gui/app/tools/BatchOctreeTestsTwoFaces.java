package cz.fidentis.analyst.gui.app.tools;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceMemoryManager;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshConfig;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshVisitor;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;

import java.io.File;
import java.io.IOException;

/**
 * A class for testing the correctness of batch Ray tracing algorithm. Algorithm
 * is used on two faces.
 *
 * @author Enkh-Undral EnkhBayar
 */
public class BatchOctreeTestsTwoFaces {

    private static final String PATH_MAIN = "/home/uenkhbayar/git/fidentis/mainFaceScaled.obj";
    private static final String PATH_SECONDARY = "/home/uenkhbayar/git/fidentis/secondFaceScaled.obj";
//    private static final String pathMain = "/home/uenkhbayar/git/fidentis/analyst-data/basic/01.obj";
//    private static final String pathsecondary = "/home/uenkhbayar/git/fidentis/analyst-data/basic/02.obj";

    private static final String PATH_MAIN_ALIGNED = "/home/uenkhbayar/git/fidentis/mainFace.obj";
    private static final String PATH_SECONDARY_ALIGNED = "/home/uenkhbayar/git/fidentis/secondFaceScaled.obj";

    private static final String PATH_AVG_FACE = "/home/uenkhbayar/git/fidentis/avgFace.obj";

    private static final Stopwatch TOTAL_TIME = new Stopwatch("Total computation time:\t");
    private static final Stopwatch AVG_FACE_COMPUTATION_TIME = new Stopwatch("AVG face computation time:\t");
    private static final Stopwatch ICP_COMPUTATION_TIME = new Stopwatch("ICP registration time:\t");
    private static final Stopwatch LOAD_TIME = new Stopwatch("File (re-)loading time:\t");
    private static final Stopwatch OCTREE_CONSTRUCTION_TIME = new Stopwatch("Octree construction time:\t");

    /**
     * 
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        FaceService factory = FaceService.INSTANCE;

        File mainFile = new File(PATH_MAIN);
        File secondFile = new File(PATH_SECONDARY);

        FaceService.INSTANCE.setDumpStrategy(HumanFaceMemoryManager.Strategy.MRU);

        TOTAL_TIME.start();

        LOAD_TIME.start();
        FaceReference mainFaceReference = factory.loadFace(mainFile);
        HumanFace mainFace = factory.getFaceByReference(mainFaceReference);
        LOAD_TIME.stop();

        LOAD_TIME.start();
        FaceReference secondFaceReference = factory.loadFace(secondFile);
        HumanFace secondFace = factory.getFaceByReference(secondFaceReference);
        LOAD_TIME.stop();

        OCTREE_CONSTRUCTION_TIME.start();
        FaceStateServices.updateOctree(secondFace, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        OCTREE_CONSTRUCTION_TIME.stop();

        AVG_FACE_COMPUTATION_TIME.start();
        AvgMeshVisitor avgFaceConstructorOctree = (new AvgMeshConfig(mainFace.getMeshModel(), null)).getRayCastingVisitor();
        avgFaceConstructorOctree.visitOctree(secondFace.getOctree());
        AVG_FACE_COMPUTATION_TIME.stop();

        writeToFile(avgFaceConstructorOctree.getAveragedMeshModel(), PATH_AVG_FACE);

        TOTAL_TIME.stop();

//        printTimeStats();
    }

    protected static void printVerticesCount(HumanFace face) {
        int count = 0;
        for (MeshFacet facet : face.getMeshModel().getFacets()) {
            count += facet.getNumberOfVertices();
        }
        System.out.println("count of vertices: " + count);
    }

    protected static void writeToFile(MeshModel model, String path) {
        File file = new File(path);
        try {
            MeshIO.exportMeshModel(model, file);
        } catch (IOException ex) {
            System.err.println(ex.toString());
        }
    }

    protected static void printTimeStats() {
        System.out.println(AVG_FACE_COMPUTATION_TIME.toString());
        System.out.println(ICP_COMPUTATION_TIME.toString());
        System.out.println(LOAD_TIME.toString());
        System.out.println(OCTREE_CONSTRUCTION_TIME.toString());
        System.out.println(TOTAL_TIME.toString());
    }

}
