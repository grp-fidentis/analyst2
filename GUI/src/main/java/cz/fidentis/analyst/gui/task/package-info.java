/**
 * Control panels and other GUI elements of a task.
 * <p>
 * Individual control panels are defined in their separate sub-packages.
 * Each control panel focuses on a specific goal, e.g., registration of faces or 
 * their distance analysis. 
 * It provides graphical control elements and application logic (controllers) for these purposes.
 * </p>
 */
package cz.fidentis.analyst.gui.task;
