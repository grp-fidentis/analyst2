package cz.fidentis.analyst.gui.task.faceinfo;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceFactory;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.face.events.HumanFaceTransformedEvent;
import cz.fidentis.analyst.data.face.events.LandmarksChangedEvent;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.project.Task;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

/**
 * Action listener for face info.
 * 
 * @author Radek Oslejsek
 */
public class FaceOverviewAction extends ControlPanelAction<FaceOverviewPanel> implements HumanFaceListener {

    private final HumanFace face;
    
    /**
     * Constructor.
     * A new {@code DistancePanel} is instantiated and added to the {@code topControlPane}
     * 
     * @param canvas OpenGL canvas
     * @param faces Faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     * @param faceIndex face index from the {@code faces}
     */
    public FaceOverviewAction(Canvas canvas, Task task, JTabbedPane topControlPane, int faceIndex) {
        super(canvas, task, topControlPane);
        setControlPanel(new FaceOverviewPanel(this, task, faceIndex));
        setShowHideCode(
                () -> { // SHOW
                },
                () -> { // HIDE
                }
        );
        // Be informed about changes in faces introduced by other GUI elements
        face = canvas.getScene().getDrawableFace(faceIndex).getHumanFace();
        face.registerListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        final String action = ae.getActionCommand();
        switch (action) {
            case FaceOverviewPanel.ACTION_SAVE_AS:
                try {
                    if (save()) {
                        getControlPanel().setSaveAsEnabled(false);
                    }
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(
                            getControlPanel(),
                            "<html><b>Saving has failed:</b><br/><br/>" + ex.getMessage() + "</html>");
                }
                break;
            default:
                // to nothing
        }
        renderScene();
    }

    @Override
    public void acceptEvent(HumanFaceEvent event) {
        if (event instanceof HumanFaceTransformedEvent || event instanceof LandmarksChangedEvent) {
            getControlPanel().setSaveAsEnabled(true);
        }
    }

    private boolean save() throws IOException {
        Object[] options = {"Overwrite", "Save as", "Cancel"};
        int answer = JOptionPane.showOptionDialog(
                getControlPanel(),
                "<html><b>Save modified model?</b></html>",
                "Current model modified",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, options, options[1]);

        if (answer == 2 || answer == -1) { // cancel
            return false;
        } else if (answer == 0) { // overwrite
            int resp = JOptionPane.showConfirmDialog(
                    getControlPanel(),
                    "<html><b>Existing files will be overwritten!</b><br/><br/>Do you want to continue?</html>",
                    "Overwrite warning",
                    JOptionPane.YES_NO_OPTION);
            if (resp == JOptionPane.NO_OPTION) {
                return false;
            } else {
                HumanFaceFactory.save(face);
            }
        } else { // save as
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Specify a file to save");
            int userSelection = fileChooser.showSaveDialog(getControlPanel());
            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                File dir = new File(fileToSave.getParent());
                File filename = new File(fileToSave.getName());
                HumanFaceFactory.saveAs(face, dir, filename);

                /*
                TaskWindow taskWindow = (TaskWindow) getControlPanel()
                        .getParent()  // TaskControlPane
                        .getParent()  // JViewport
                        .getParent()  // JScrollPane
                        .getParent(); // TaskWindow
                taskWindow.setName(filename.toString());
                 */
            } else {
                return false;
            }
        }

        return true;
    }
}
