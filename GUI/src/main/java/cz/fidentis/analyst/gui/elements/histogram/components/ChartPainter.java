package cz.fidentis.analyst.gui.elements.histogram.components;

import java.awt.*;

/**
 * Class for managing recoloring of {@link HistogramChartPanel}.
 *
 * @author Jakub Nezval
 */
public class ChartPainter {
    private final HistogramChartPanel chart;
    private final int offset;

    private Color[] colors = new Color[]{ Color.RED, Color.BLUE };
    private float[] fractions = new float[]{ 0f, 1f };

    /**
     * Creates a {@link ChartPainter} that recolors {@link HistogramChartPanel}.
     *
     * @param chart histogram chart to be affected
     * @param offset offset to 0
     *               ({@code offset} is subtracted from both arguments of {@link ChartPainter#paintChart(int, int)})
     */
    public ChartPainter(HistogramChartPanel chart, int offset) {
        this.chart = chart;
        this.offset = offset;
    }

    /**
     * Creates a gradient from two colors.
     *
     * @param a color A
     * @param b color B
     * @param stepsCount how many colors will be added between color {@code a} and {@code b}
     *                   (must be non-negative)
     * @return gradient, first element is color {@code a} and last element is color {@code b}
     */
    public static Color[] getGradientFrom(Color a, Color b, int stepsCount) {
        if (stepsCount < 0) {
            throw new IllegalArgumentException("stepsCount must be greater or equal to 0");
        }

        var aHsb = Color.RGBtoHSB(a.getRed(), a.getGreen(), a.getBlue(), null);
        var bHsb = Color.RGBtoHSB(b.getRed(), b.getGreen(), b.getBlue(), null);

        var hueStep = (aHsb[0] - bHsb[0]) / (stepsCount + 1);
        var saturationStep = (aHsb[1] - bHsb[1]) / (stepsCount + 1);
        var brightnessStep = (aHsb[2] - bHsb[2]) / (stepsCount + 1);

        var colors = new Color[stepsCount + 2];
        colors[0] = a;
        colors[colors.length - 1] = b;

        for (int i = 1; i <= stepsCount; i++) {
            colors[i] = Color.getHSBColor(
                    aHsb[0] - i * hueStep,
                    aHsb[1] - i * saturationStep,
                    aHsb[2] - i * brightnessStep
            );
        }

        return colors;
    }

    /**
     * Creates a new {@link Color} with the same properties as the original,
     * but with a modified alpha value.
     *
     * @param color source color
     * @param alpha desired alpha value
     * @return {@code color} with desired alpha value
     */
    public static Color getColorWithAlpha(Color color, int alpha) {
        return new Color(
                color.getRed(),
                color.getGreen(),
                color.getBlue(),
                alpha
        );
    }

    /**
     * Creates a new {@link LinearGradientPaint} identical to the original,
     * except for modified alpha values of the source colors.
     *
     * @param gradient source gradient
     * @param alpha desired alpha value
     * @return {@code gradient} with desired alpha value
     */
    public static LinearGradientPaint getGradientWithAlpha(LinearGradientPaint gradient, int alpha) {
        var colors = gradient.getColors();

        for (var i = 0; i < colors.length; i++) {
            colors[i] = getColorWithAlpha(colors[i], alpha);
        }

        return new LinearGradientPaint(
                gradient.getStartPoint(),
                gradient.getEndPoint(),
                gradient.getFractions(),
                colors
        );
    }

    /**
     * Creates a new {@link Color} with the same properties as the original,
     * but with a modified saturation value.
     *
     * @param color source color
     * @param saturation desired saturation
     * @return {@code color} with desired saturation
     */
    public static Color getColorWithSaturation(Color color, float saturation) {
        var hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        return Color.getHSBColor(hsb[0], saturation, hsb[2]);
    }

    /**
     * Set colors that will be used to create a gradient between bound values.
     *
     * @param colors gradient source colors
     * @see ChartPainter#paintChart(int, int)
     */
    public void setGradientColors(Color[] colors) {
        if (colors == null || colors.length < 2) {
            throw new IllegalArgumentException("`colors` must contain at least two elements");
        }

        this.colors = new Color[colors.length + 2];
        System.arraycopy(colors, 0, this.colors, 1, colors.length);
        this.colors[0] = ChartPainter.getColorWithSaturation(colors[0], 0);
        this.colors[this.colors.length - 1] = ChartPainter.getColorWithSaturation(colors[colors.length - 1], 0);

        this.fractions = new float[this.colors.length];

        for (int i = 0; i < colors.length; i++) {
            fractions[i + 1] = (float) i / (float) (colors.length - 1);
        }

        this.fractions[0] = 0f;
        this.fractions[1] = Math.nextUp(0f);
        this.fractions[this.fractions.length - 2] = Math.nextDown(1f);
        this.fractions[this.fractions.length - 1] = 1f;
    }

    /**
     * Creates a {@link LinearGradientPaint} between {@code lowerBoundValue} and {@code upperBoundValue}
     * and uses it to paint {@link HistogramChartPanel}.
     *
     * @param lowerBoundValue lower bound value
     * @param upperBoundValue upper bound value
     * @see ChartPainter#ChartPainter(HistogramChartPanel, int)  ChartPainter
     * @see ChartPainter#setGradientColors(Color[])
     * @see HistogramChartPanel#setPaint(Paint, Paint)
     */
    public void paintChart(int lowerBoundValue, int upperBoundValue) {
        var a = lowerBoundValue - this.offset;
        var b = upperBoundValue - this.offset - 1;

        if (a == b) {
            a--;
        }

        var gradient = new LinearGradientPaint(
                Math.min(a, b), 0,
                Math.max(a, b), 0,
                this.fractions,
                this.colors
        );

        this.chart.setPaint(gradient, ChartPainter.getGradientWithAlpha(gradient, 126));
    }
}
