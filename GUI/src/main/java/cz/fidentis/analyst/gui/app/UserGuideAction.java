package cz.fidentis.analyst.gui.app;

import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;

/**
 * Show a GitLab wiki page with user guide.
 * 
 * @author Radek Oslejsek
 */

@ActionID(
        category = "Help",
        id = "cz.fidentis.analyst.gui.app.UserGuideAction"
)
@ActionRegistration(
        displayName = "#CTL_SomeAction"
)
@ActionReference(path = "Menu/Help", position = 100)
@Messages("CTL_SomeAction=User Guide")
public final class UserGuideAction implements ActionListener {
    
    public static final URI LINK = URI.create("https://gitlab.fi.muni.cz/grp-fidentis/analyst2/-/wikis");

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Desktop.isDesktopSupported()) {
                try {
                    Desktop.getDesktop().browse(LINK);
                    return;
                } catch (Exception ex) {
                    showError();
                }
            } else {
                showError();
            }
    }
    
    /**
     * Show an error window if the redirection to the URI fails.
     */
    public void showError() {
        JOptionPane.showMessageDialog(null,
                "Can't navigate to " + LINK + System.lineSeparator() + "Type this address to your web browser manually.",
                "Error",
                JOptionPane.WARNING_MESSAGE);
    }
}
