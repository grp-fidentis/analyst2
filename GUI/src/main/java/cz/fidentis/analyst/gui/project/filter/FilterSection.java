package cz.fidentis.analyst.gui.project.filter;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author Matej Kovar
 */
public class FilterSection {

    // key is checkbox, value is list of all additional 
    // components checkbox has
    private Map<JCheckBox, JComponent> options = new HashMap<>();

    /**
     * Adds new pair of checkbox and corresponding additional JComponent to this checkbox
     * @param checkbox JCheckBox
     * @param additional JComponent, null if checkbox doesn't need another component
     */
    public void add(JCheckBox checkbox, JComponent additional) {
        
        options.put(checkbox, additional);
        checkbox.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkboxSelected(checkbox, checkbox.isSelected());
            }

        });
    }
    
    /**
     * Enables/disables JComponent which is associated with checkbox
     * @param checkbox JCheckbox
     * @param enable Boolean - true if checkbox is selected, false otherwise
     */
    private void checkboxSelected(JCheckBox checkbox, boolean enable) {
        
        JComponent c = options.get(checkbox);
        if (c != null) {
            c.setEnabled(enable);
        }
        
        if (c instanceof JTextField) {
                ((JTextField) c).setText("");
        }
        
        disableOtherComponents(checkbox);
    }
    
    /**
     * Disable other components from this section, i.e. disables other checkboxes
     * and their associated JComponents
     * @param checkbox JCheckbox
     */
    private void disableOtherComponents(JCheckBox checkbox) {
        
        options.keySet()
                .stream()
                .filter(key -> (key != checkbox))
                .map(key -> {
                    key.setSelected(false);
                    return key;
        })
                .forEachOrdered(key -> {
                    JComponent c = options.get(key);
                    if (c != null) {
                        c.setEnabled(false);
                    }
                    if (c instanceof JTextField) {
                        ((JTextField) c).setText("");
                    }
        });
    }
    
    /**
     * Disables all checkboxes and components in this section
     */
    public void disableAllComponents() {
        options.keySet()
                .stream()
                .map(key -> {
                    key.setSelected(false);
                    return key;
        })
                .forEachOrdered(key -> {
                    JComponent c = options.get(key);
                    if (c != null) {
                        c.setEnabled(false);
                    }
                    if (c instanceof JTextField) {
                        ((JTextField) c).setText("");
                    }
        });
    }
}
