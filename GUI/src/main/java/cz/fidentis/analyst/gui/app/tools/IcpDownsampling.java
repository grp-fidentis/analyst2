package cz.fidentis.analyst.gui.app.tools;

import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.face.FaceDistanceServices;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.gui.task.batch.Stopwatch;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * This class evaluates efficiency (time) and precision of the ICP registration if different
 * sub-sampling algorithms with different strength are used.
 * <p/>
 * The application reads N faces from a dataset and evaluates all pairs, but only in one direction
 * (i.e., if the face X is registered to the face Y, then the opposite registration Y -> X is skipped).
 * <strong>Only the secondary (transformed) face is sub-sampled</strong>
 *
 * @author Radek Oslejsek
 */
public class IcpDownsampling {

    /**
     * Sub-sampling strategy
     */
    private static final PointSamplingConfig.Method STRATEGY = PointSamplingConfig.Method.POISSON_OPENCL;

    /**
     * Surface smoothing strategy for the Poisson disk sampling
     */
    private static final MeshTriangle.Smoothing SMOOTHING = MeshTriangle.Smoothing.NONE;

    /**
     * Sub-sampling strength, i.e., number of samples of the second (transformed) face.
     * The real number of samples may differ as some strategy only approximates the desire number.
     * Zero means no sub-sampling (i.e., the registration in full resolution)
     */
    private static final int[] SUB_SAMPLE_TO = new int[] {
            //1000,5000,10000,30000
            //20,30,40,50,100,200,300,400,500//,1000,5000,10000,30000
            //50, 100, 200, 300, 400, 500, 1000, 2000, 3000
            50, 100, 200, 300, 400, 500, 1000, 2000, 3000
    };

    /**
     * Folder with faces
     */
    private static final String DATA_DIR = "../analyst-data-antropologie/_ECA";

    /**
     * Number of faces to be loaded from the folder.
     */
    private static final int LOAD_FACES = 500;

    /**
     * Process every Xth face, i.e., the overall number of processed faces is {@code LOAD_FACES / PROCESS_XTH_FACE}
     */
    private static final int PROCESS_XTH_FACE = 5;

    private static final double REPORT_FACES_DISSIMILAR_MORE_THAN = 3.5;

    private static final int REGISTRATION_MAX_ITERATION = 100;
    private static final double REGISTRATION_MIN_ERROR = 0.05;
    private static final boolean REGISTRATION_SCALE = false;
    private static final int REGISTRATION_IGNORE_PARTIALLY_OVERLAID_PARTS = 1;

    private static final MeshDistanceConfig.Method SIMILARITY_STRATEGY = MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS;
    private static final boolean SIMILARITY_RELATIVE_DISTANCE = false;
    private static final boolean SIMILARITY_AUTO_CROP = true;

    private static String veryDifferentFaces = "";
    
    /**
     * Main method 
     * @param args Input arguments 
     * @throws IOException on IO error
     */
    public static void main(String[] args) throws IOException {
        List<Path> faces = Files.list(new File(DATA_DIR).toPath())
                .filter(f -> f.toString().endsWith(".obj"))
                .sorted()
                .limit(LOAD_FACES)
                .toList();
        
        SortedMap<Integer, Stopwatch> efficiency = new TreeMap<>(Collections.reverseOrder());
        SortedMap<Integer, List<Double>> precision = new TreeMap<>(Collections.reverseOrder());
        
        int counter = 1;
        int numFaces = faces.size() / PROCESS_XTH_FACE;
        for (int i = 0; i < faces.size(); i += PROCESS_XTH_FACE) {
            for (int j = i; j < faces.size(); j += PROCESS_XTH_FACE) { // starts with "i"!
                if (i != j) { // register only different faces
                    System.out.println();
                    System.out.println("Comparison No: " + counter + "/" + ((numFaces*(numFaces-1))/2));
                    compareFaces(faces.get(i), faces.get(j), efficiency, precision);
                    printResults(efficiency, precision, counter);
                    counter++;
                }
            }
        }

        System.out.println();
        System.out.println("SIGNIFICANTLY DIFFERENT FACES:");
        System.out.println(veryDifferentFaces);
    }
    
    protected static void printResults(SortedMap<Integer, Stopwatch> efficiency, SortedMap<Integer, List<Double>> precision, double counter) {
        StringBuilder sb = new StringBuilder();
        for (int numSamples: precision.keySet()) {
            double avg = precision.get(numSamples).stream()
                    .mapToDouble(val -> val)
                    .summaryStatistics()
                    .getAverage();
            double variance = precision.get(numSamples).stream()
                    .mapToDouble(val -> Math.pow(Math.abs(val - avg), 2))
                    .average()
                    .orElse(-1);
            double time = efficiency.get(numSamples).getTotalTime() / counter;
            //double coefOfVariation = (Math.sqrt(variance) / avg) * 100; // as percentage

            if (numSamples == 0) {
                sb.insert(0, "MAX;" + avg + ";" + variance + ";" + time + System.lineSeparator());
            } else {
                sb.append(numSamples + ";" + avg + ";" + variance + ";" + time + System.lineSeparator());
            }
        }
        sb.insert(0, "# samples;dissimilarity;variance;time (ms)" + System.lineSeparator());
        System.out.println(sb);
    }

    protected static void compareFaces(
            Path priFacePath,
            Path secFacePath,
            Map<Integer, Stopwatch> efficiency,
            Map<Integer, List<Double>> precision) throws IOException {

        HumanFace priFace = FaceService.INSTANCE.loadTemporaryInMemoryFace(priFacePath.toFile());
        FaceStateServices.updateKdTree(priFace, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        HumanFace secFaceFromFile = FaceService.INSTANCE.loadTemporaryInMemoryFace(secFacePath.toFile());

        double bestDist = Double.POSITIVE_INFINITY;
        System.out.println("Faces: " + secFaceFromFile.getShortName() + " -> " + priFace.getShortName());
        for (int numSamples: SUB_SAMPLE_TO) {
            HumanFace secFace = FaceService.INSTANCE.createTemporaryInMemoryFace(
                    MeshFactory.cloneMeshModel(secFaceFromFile.getMeshModel()));

            PointSamplingConfig secSampling = (numSamples == 0) // skip sub-sampling, use full resolution
                    ? new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 0)
                    : new PointSamplingConfig(STRATEGY, numSamples, SMOOTHING);

            efficiency.computeIfAbsent(numSamples, k-> new Stopwatch("")).start();
            FaceRegistrationServices.alignMeshes(
                    secFace, // is transformed
                    new IcpConfig(
                            priFace.getMeshModel(),
                            REGISTRATION_MAX_ITERATION,
                            REGISTRATION_SCALE,
                            REGISTRATION_MIN_ERROR,
                            secSampling,
                            REGISTRATION_IGNORE_PARTIALLY_OVERLAID_PARTS));
            efficiency.get(numSamples).stop();

            System.out.println("Second face: " + secSampling);
            FaceStateServices.updateKdTree(secFace, FaceStateServices.Mode.COMPUTE_ALWAYS);
            double dist = FaceDistanceServices.calculateBiDistance(
                    priFace,
                    secFace,
                    SIMILARITY_RELATIVE_DISTANCE,
                    SIMILARITY_AUTO_CROP,
                    SIMILARITY_STRATEGY,
                    null,
                    FaceDistanceServices.DistanceAggregation.MODIFIED_HAUSDORFF);
            precision.computeIfAbsent(numSamples, k -> new ArrayList<>()).add(dist);

            if (numSamples > 0) { // best achieved similarity with sub-sampling in action
                bestDist = Math.min(bestDist, dist);
            }
        }

        if (Double.isFinite(bestDist)  && bestDist > REPORT_FACES_DISSIMILAR_MORE_THAN) {
            System.out.println("WARNING: SIGNIFICANTLY DISSIMILAR FACES with the best distance of " + bestDist);
            veryDifferentFaces += secFaceFromFile.getShortName() + " -> " + priFace.getShortName() + ": " + bestDist + System.lineSeparator();
        }
        System.out.println();
    }


    /*
     * OLD version used for the paper:
     *
    protected static void compareFacesOld(
            Path priFacePath,
            Path secFacePath,
            Map<Integer, Stopwatch> efficiency,
            Map<Integer, List<Double>> precision) throws IOException {
        
        HumanFace priFace = HumanFaceFactory.create(priFacePath.toFile());
        FaceStateServices.updateKdTree(priFace, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        HumanFace secFaceFromFile = HumanFaceFactory.create(secFacePath.toFile());

        double bestDist = Double.POSITIVE_INFINITY;
        System.out.println("Faces: " + secFaceFromFile.getShortName() + " -> " + priFace.getShortName());
        for (int numSamples: SUB_SAMPLE_TO) {
            HumanFace secFace = HumanFaceFactory.create(
                    MeshFactory.cloneMeshModel(secFaceFromFile.getMeshModel()),
                    secFaceFromFile.getId());
            
            PointSamplingConfig secSampling = (numSamples == 0) // skip sub-sampling, use full resolution
                    ? new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 0)
                    : new PointSamplingConfig(STRATEGY, numSamples, SMOOTHING);

            efficiency.computeIfAbsent(numSamples, k-> new Stopwatch("")).start();
            FaceRegistrationServices.alignMeshes(
                    secFace, // is transformed
                    new IcpConfig(
                        priFace.getMeshModel(),
                        REGISTRATION_MAX_ITERATION,
                        REGISTRATION_SCALE,
                        REGISTRATION_MIN_ERROR,
                        secSampling,
                        REGISTRATION_IGNORE_PARTIALLY_OVERLAID_PARTS));
            efficiency.get(numSamples).stop();
            
            System.out.println("Second face: " + secSampling);
            double distAB = MeshDistanceServices.measure(
                    secFace.getMeshModel(),
                    new MeshDistanceConfig(
                            SIMILARITY_STRATEGY,
                            priFace.getKdTree(),
                            SIMILARITY_RELATIVE_DISTANCE,
                            SIMILARITY_AUTO_CROP
                    )).getDistanceStats().getAverage();

            FaceStateServices.updateKdTree(secFace, FaceStateServices.Mode.COMPUTE_ALWAYS);
            double distBA = MeshDistanceServices.measure(
                    priFace.getMeshModel(),
                    new MeshDistanceConfig(
                            SIMILARITY_STRATEGY,
                            secFace.getKdTree(),
                            SIMILARITY_RELATIVE_DISTANCE,
                            SIMILARITY_AUTO_CROP
                    )).getDistanceStats().getAverage();

            double dist = Math.max(distAB, distBA); //(distAB + distBA) * 0.5;
            precision.computeIfAbsent(numSamples, k -> new ArrayList<>()).add(dist);

            if (numSamples > 0) { // best achieved similarity with sub-sampling in action
                bestDist = Math.min(bestDist, dist);
            }
        }

        if (Double.isFinite(bestDist)  && bestDist > REPORT_FACES_DISSIMILAR_MORE_THAN) {
            System.out.println("WARNING: SIGNIFICANTLY DISSIMILAR FACES with the best distance of " + bestDist);
            veryDifferentFaces += secFaceFromFile.getShortName() + " -> " + priFace.getShortName() + ": " + bestDist + System.lineSeparator();
        }
        System.out.println();
    }
     */

}
