package cz.fidentis.analyst.gui.app;

import cz.fidentis.analyst.gui.project.ProjectWindow;
import cz.fidentis.analyst.gui.project.SaveProjectAction;
import cz.fidentis.analyst.project.Project;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import cz.fidentis.analyst.project.ProjectService;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

/**
 * GUI initialization class.
 * 
 * @author Radek Oslejsek
 */
public class Installer extends ModuleInstall {
    
    @Override
    public void restored() {
        /*
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                //System.out.println(info.getName());
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Installer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        */
        java.awt.EventQueue.invokeLater(() -> {
            //old JFrame, disabled
            /*frameMain = new UserInterface();
            frameMain.setBackground(new Color(49,165,154));
            frameMain.pack();
            frameMain.setVisible(true);*/          
            //enables to use design of operating system
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException ex) {
            }
        });
    }
    
    @Override
    public boolean closing() {
        ProjectWindow win = (ProjectWindow) WindowManager.getDefault().findTopComponent("ProjectWindow");
        Project project = ProjectService.INSTANCE.getOpenedProject();
        
        // If no changes are to be saved, then close silently.
        // Otherwise, ask for saving the project first.
        if (project.changed()) { 
            Object[] options = {"Save and close", "Discard changes and close", "Cancel"};
            int answer = JOptionPane.showOptionDialog(
                    win,
                    "<html><b>Save current project?</b></html>",
                    "Current project modified",
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE,
                    null, options, options[0]);
            
            if (answer == 2 || answer == -1) { // cancel
                return false;
            } else if (answer == 0) { // save
                // trigger saving dialogue:
                new SaveProjectAction().actionPerformed(new ActionEvent( 
                        this,
                        ActionEvent.ACTION_PERFORMED,
                        ""));
            
                if (project.changed()) { // saving canceled
                    return false; // skip new project opening
                }
            }
        }
        
        return true;
        
        /*
        int answer = JOptionPane.showConfirmDialog(null,
                "Do you really want to close the application?", "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        
        if (answer == JOptionPane.YES_OPTION) {
            ProjectWindow win = (ProjectWindow) WindowManager.getDefault().findTopComponent("ProjectWindow");
            if (win != null) {
                win.serializeTasks();
            }
        }
        
        return answer == JOptionPane.YES_OPTION;
        */
    }

    
}
