package cz.fidentis.analyst.gui.task.interactivemask;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.canvas.ScreenPointToRay;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.drawables.Drawable;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.data.face.events.HumanFaceSelectedEvent;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionServices;
import org.openide.util.Pair;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

/**
 * Implementation of InteractiveMaskTask without extending SwingWorker and parallelism
 * @author Mario Chromik
 */
public class InteractiveMaskTaskNoParallel {

    private final Canvas canvas;

    private final ArrayList<Point> pointsToProject;
    private final int panelWidth;
    private final int panelHeight;


    /**
     * Constructor with 4 parmeters
     * @param canvas input canvas
     * @param pointsToProject points to project
     * @param panelWidth width of the drawing panel
     * @param panelHeight height of the drawing panel
     */
    public InteractiveMaskTaskNoParallel(Canvas canvas,ArrayList<Point> pointsToProject, int panelWidth, int panelHeight) {
        this.canvas = canvas;
        this.pointsToProject = pointsToProject;
        this.panelWidth = panelWidth;
        this.panelHeight = panelHeight;
    }

    /**
     * Calculates an octree, casts a ray and if intersecting returns said intersection
     * @param face face to check for intersection
     * @param ray ray to project
     * @return pair of face, intersection if intersect else null
     */
    private Pair<HumanFace, RayIntersection> getRayIntersection(HumanFace face, Ray ray) {
        FaceStateServices.updateOctree(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        RayIntersection ri = RayIntersectionServices.computeClosest(
                face.getOctree(),
                new RayIntersectionConfig(ray, MeshTriangle.Smoothing.NORMAL, false));
        return (ri == null) ? null : Pair.of(face, ri);
    }

    /**
     * Normalizes and fits a pint from drawing panel to be projected onto face and projects it
     * @param point point to normalize and project
     * @return face if intersect else null
     */
    private HumanFace projectPoint(Point point) {
        Point normalizedPoint = new Point((int) point.getX() - (panelWidth/2) ,(int) point.getY() - (panelHeight/2));

        //Ray ray = canvas.getSceneRenderer().castRayThroughPixel( normalizedPoint.x + canvas.getWidth() / 2,
             //   normalizedPoint.y + canvas.getHeight() / 2,
             //   canvas.getCamera());

        Ray ray = ScreenPointToRay.convert(normalizedPoint.x + canvas.getWidth() / 2, normalizedPoint.y + canvas.getHeight() / 2, canvas.getWidth(), canvas.getHeight(),   canvas.getCamera());
        Pair<HumanFace, RayIntersection> closestFace = canvas.getScene()
                .getDrawableFaces()
                .stream()
                .filter(Drawable::isShown)
                .map(DrawableFace::getHumanFace)
                .map(face -> getRayIntersection(face, ray))
                .filter(Objects::nonNull)
                .sorted(Comparator.comparingDouble(o -> o.second().getDistance()))
                .findFirst()
                .orElse(null);

        if (closestFace != null) {
            HumanFace face = closestFace.first();
            face.announceEvent(new HumanFaceSelectedEvent(
                    face,
                    closestFace.second(), // distance
                    "Intersect with secondary",
                    this
            ));
            return closestFace.first();
        }

        return null;
    }

    /**
     * Projects points from a given list onto face
     */
    public void project(){
        HumanFace face = null;

        for (Point point: pointsToProject) {
            face = projectPoint(point);
        }

        face.announceEvent(new HumanFaceSelectedEvent(
                face,
                null, // distance
                "Render",
                this
        ));
    }
}
