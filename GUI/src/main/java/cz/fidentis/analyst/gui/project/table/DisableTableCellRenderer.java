package cz.fidentis.analyst.gui.project.table;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * This renderer disables/disables cells depending on the state of 
 * the {@code ProjectTableModel.Column.FILTERED_OUT} cell.
 * Also, sets the corresponding background.
 * 
 * @author Radek Oslejsek
 */
public class DisableTableCellRenderer extends DefaultTableCellRenderer {
     
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        Boolean filteredOut = (Boolean) table.getModel().getValueAt(row, ProjectTableModel.Column.FILTERED_OUT.index);
        if (filteredOut != null && filteredOut) {
            c.setEnabled(false);
            setBackground(Color.LIGHT_GRAY);
        } else {
            c.setEnabled(true);
            if (isSelected) {
                setBackground(table.getSelectionBackground());
            } else {
                setBackground(table.getBackground());
            }
        }
        return c;
    }
}