package cz.fidentis.analyst.gui.elements;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

/**
 * A dialog window that shows the progress of a long-running task.
 * <p>
 * Before using this dialog, a {@code SwingWorker} task class has to be implemented
 * that stores the reference to this {@code ProgressDialog}, e.g.,
 * <pre>
 * public class Task extends SwingWorker&lt;Void, Voide&gt; {
 *    private final ProgressDialog progressDialog;
 *    
 *    public Task(ProgressDialog progressDialog) {
 *        this.progressDialog = progressDialog;
 *    }
 * 
 *    // other code
 * }
 * </pre>
 * Either the {@code doInBackground()} or {@code process(...)} method of the {@code Task}
 * has to call {@code progressDialog.setValue(val)} to update the progress bar accordingly.
 * <p>
 * Once the task and progress dialog are instantiated, it is necessary to attach listener that reacts to 
 * the end of the task computation, e.g.,
 * <pre>
 * ProgressDialog progressDialog = new ProgressDialog(null);
 * Task task = new Task(progressDialog);
 *        
 * task.addPropertyChangeListener((PropertyChangeEvent evt) -&gt; {
 *    if ("state".equals(evt.getPropertyName()) &amp;&amp; (SwingWorker.StateValue.DONE.equals(evt.getNewValue()))) {
 *        // code invoked when the task is done
 *    }
 * });
 * </pre>
 * Other states than the task end can be handled in similar way.
 * <p>
 * Finally, the task is executed in separate thread:
 * <pre>
 * progressDialog.runTask(task);
 * </pre>
 * 
 * @author Radek Oslejsek
 * 
 * @param <T> the result type returned by {@code SwingWorker's} {@code doInBackground} and {@code get} methods
 * @param <V> the type used for carrying out intermediate results by {@code SwingWorker's} {@code publish} and {@code process} methods
 */
public class ProgressDialog<T,V> extends JDialog {

    private JProgressBar progressBar;
    private JButton cancelButton;
    private SwingWorker<T,V> task;

    /**
     * Constructor.
     * @param comp the component in relation to which the dialog window location is determined
     * @param label label of the dialog window
     */
    public ProgressDialog(Component comp, String label) {
        super((Frame) null, label, true);
        initProgressBar();
        this.setLocationRelativeTo(comp);
    }
    
    /**
     * Runs the task in the separate thread and displays the progress dialog.
     * 
     * @param task Task to be run
     */
    public void runTask(SwingWorker<T,V> task) {
        this.task = task;
        task.execute();
        setVisible(true);
        setAlwaysOnTop(true);
    }
    
    /**
     * Updates progress bar. Should be executed from the task
     * 
     * @param progress Value in the range 0 .. 100
     */
    public void setValue(int progress) {
        progressBar.setValue(progress);
    }
    
    private void initProgressBar() {
        JPanel panel = new JPanel();
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        panel.add(progressBar);

        cancelButton = new JButton("Cancel");
        panel.add(cancelButton);

        add(panel);
        setSize(450, 100);
        
        cancelButton.addActionListener((ActionEvent e) -> {
            task.cancel(true);
            dispose();
        });
    }    
}
