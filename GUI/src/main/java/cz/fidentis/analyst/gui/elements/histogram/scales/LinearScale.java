package cz.fidentis.analyst.gui.elements.histogram.scales;

import java.util.function.Function;

/**
 * Class for performing linear scaling of values.
 *
 * @author Jakub Nezval
 */
public class LinearScale extends FunctionScale {
    @Override
    protected Function<Double, Double> createMapFunction(
            double domainStart, double domainEnd, double rangeStart, double rangeEnd
    ) {
        var b = (rangeStart - rangeEnd) / (domainStart - domainEnd);
        var a = rangeStart - b * domainStart;

        return x -> a + b * x;
    }
}
