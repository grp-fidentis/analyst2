package cz.fidentis.analyst.gui.app.tools.opencl;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.opencl.OpenCLServices;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Convenience methods for loading {@link MeshModel} files
 *
 * @author Marek Horský
 */
public class BenchmarkUtils {
    private static final Integer DIR_MODELS_LIMIT = 10;

    /**
     * @param path
     * @return
     */
    public static MeshModel getModel(String path) {
        MeshModel model;
        try {
            model = MeshIO.readMeshModel(new File(path));
        } catch (IOException e) {
            System.err.println("No mesh model found at the location " + path);
            return MeshFactory.createEmptyMeshModel();
        }
        return model;
    }

    /**
     * @param dir
     * @return
     */
    public static List<MeshModel> getModels(String dir) {
        try {
            return Files.list(new File(dir).toPath())
                    .filter(f -> f.toString().endsWith(".obj"))
                    .limit(DIR_MODELS_LIMIT)
                    .map(file -> getModel(file.toAbsolutePath().toString()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.err.println("No directory found at the location " + dir);
            return List.of();
        }
    }

    /**
     * Prints name of OpenCL device
     */
    public static void printGPU() {
        CLContext clContext = OpenCLServices.createContext();
        assert clContext != null;
        System.out.println("Benchmark evaluation of: " + clContext.getMaxFlopsDevice().getName());
        clContext.release();
    }

    /**
     * Prints benchmark results to stdout
     *
     * @param timeCPU
     * @param timeGPU
     * @param iterations
     */
    public static void printComparison(long timeCPU, long timeGPU, int iterations) {
        System.out.println();
        System.out.println("Results: ");
        System.out.println("GPU AVERAGE TIME: " + timeGPU / iterations + "ns - " + (timeGPU / iterations) / 1000000 + "ms");
        System.out.println("CPU AVERAGE TIME: " + timeCPU / iterations + "ns - " + (timeCPU / iterations) / 1000000 + "ms");
        System.out.println("GPU implementation is " + (timeCPU / ((float) timeGPU)) + " times faster");
    }
}
