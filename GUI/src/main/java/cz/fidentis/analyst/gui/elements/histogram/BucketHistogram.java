package cz.fidentis.analyst.gui.elements.histogram;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Class that implements bucket histogram.
 *
 * @author Jakub Nezval
 */
public class BucketHistogram {
    private final int bucketCount;
    
    private List<Integer> buckets;
    
    private double minValue;
    private double maxValue;

    /**
     * Create a new BucketHistogram that will use {@code bucketCount} buckets.
     * Number of buckets cannot be changed later.
     *
     * @param bucketCount number of buckets to be used
     */
    public BucketHistogram(int bucketCount) {
        this.bucketCount = bucketCount;
    }

    /**
     * Set values from which the bucket histogram will be calculated.
     *
     * @param values values
     * @see BucketHistogram#getBuckets()
     */
    public void setValues(Collection<Double> values) {
        this.buckets = new ArrayList<>(Collections.nCopies(this.bucketCount, 0));

        this.minValue = Double.POSITIVE_INFINITY;
        this.maxValue = Double.NEGATIVE_INFINITY;
        for (var value: values) {
            if (value < this.minValue) {
                this.minValue = value;
            }
            if (value > this.maxValue) {
                this.maxValue = value;
            }
        }

        double bucketSpan = (this.maxValue - this.minValue) / this.bucketCount;
        int bucketIndex, safeBucketIndex;
        for (var value: values) {
            bucketIndex = (int) ((value - this.minValue) / bucketSpan);
            safeBucketIndex = bucketIndex < this.bucketCount ? bucketIndex : this.bucketCount - 1;
            buckets.set(safeBucketIndex, buckets.get(safeBucketIndex) + 1);
        }
    }

    /**
     * Return buckets.
     * Integer at index {@code i} is equal to number of values that belong to that bucket range.
     *
     * @return list of buckets
     * @see BucketHistogram#setValues(Collection)
     */
    public List<Integer> getBuckets() {
        return Collections.unmodifiableList(this.buckets);
    }

    /**
     * @return minimum value from {@code values}
     * used in the last {@link BucketHistogram#setValues(Collection)} call
     */
    public double getMinValue() {
        return this.minValue;
    }

    /**
     * @return maximum value from {@code values}
     * used in the last {@link BucketHistogram#setValues(Collection)} call
     */
    public double getMaxValue() {
        return this.maxValue;
    }
}
