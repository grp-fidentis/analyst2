package cz.fidentis.analyst.gui.elements;

import cz.fidentis.analyst.data.surfacemask.Layer;
import cz.fidentis.analyst.data.surfacemask.SurfaceMaskLayers;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

import static cz.fidentis.analyst.gui.task.interactivemask.InteractiveMaskPanel.ACTION_PROJECT;

/**
 * Represents a UI panel for managing individual layers in the surface mask editor.
 *
 * <p>This panel provides controls for toggling the visibility of a layer, selecting it,
 * and removing it from the {@link SurfaceMaskLayers} manager. The panel includes:
 *  - A visual representation of the layer.
 *  - A button to show/hide the layer.
 *  - A button to delete the layer.
 *
 * @author Martin Bjaloň
 */
public class LayerPanel extends JPanel {

    private static final String SHOW = "eye.png";
    private static final String HIDE = "hidden.png";

    private final JLabel layerIcon;
    private final JButton showHideButton;
    private final Layer layer;
    private final SurfaceMaskLayers layerManager;
    private final JPanel container;
    private final SurfaceMaskPanel surfaceMaskPanel;
    private final ActionListener actionListener;

    /**
     * Creates new form of LayerPanel
     *
     * @param container         The parent container that holds this panel.
     * @param layer             The {@link Layer} associated with this panel.
     * @param layerManager      The {@link SurfaceMaskLayers} manager handling multiple layers.
     * @param surfaceMaskPanel  The panel responsible for rendering the surface masks.
     * @param actionListener    The listener for handling actions related to layers.
     *
     */
    public LayerPanel(JPanel container, Layer layer, SurfaceMaskLayers layerManager, SurfaceMaskPanel surfaceMaskPanel, ActionListener actionListener) {
        this.layer = layer;
        this.layerManager = layerManager;
        this.container = container;
        this.surfaceMaskPanel = surfaceMaskPanel;
        this.actionListener = actionListener;

        setUpPanel();
        layerIcon = createLayerIcon();
        showHideButton = createShowHideButton();
        JButton deleteButton = createDeleteButton(container);

        add(layerIcon);
        add(showHideButton);
        add(Box.createHorizontalGlue());
        add(deleteButton);

        selectLayer();
        addClickListener();
    }

    /**
     * Sets up the layout and styling for the panel.
     */
    private void setUpPanel() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    }

    /**
     * Creates the layer icon for the panel.
     *
     * @return A JLabel representing the layer icon.
     */
    private JLabel createLayerIcon() {
        JLabel icon = new JLabel();
        icon.setOpaque(true);
        icon.setBackground(Color.GRAY);
        icon.setPreferredSize(new Dimension(60, 15));
        icon.setMaximumSize(new Dimension(60, 15));
        icon.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 15));
        return icon;
    }

    /**
     * Creates the show/hide button with its action listener.
     *
     * @return A JButton for toggling layer visibility.
     */
    private JButton createShowHideButton() {
        JButton button = new JButton(loadIcon(SHOW));
        button.setFocusPainted(false);
        button.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        button.addActionListener(evt -> toggleVisibility());
        return button;
    }

    /**
     * Creates the delete button and its action listener.
     *
     * @param container The parent container holding this panel.
     * @return A JButton for deleting this layer panel.
     */
    private JButton createDeleteButton(JPanel container) {
        JButton button = new JButton("✖");
        button.setFocusPainted(false);
        button.setFont(new Font("SansSerif", Font.PLAIN, 12));
        button.addActionListener(evt -> removeLayer(container));
        return button;
    }

    /**
     * Toggles the visibility of the associated layer.
     */
    private void toggleVisibility() {
        if (layerIcon.getBackground().equals(Color.GRAY)) {
            layerIcon.setBackground(Color.WHITE);
            showHideButton.setIcon(loadIcon(HIDE));
            layer.setVisibility(false);
        } else {
            layerIcon.setBackground(Color.GRAY);
            showHideButton.setIcon(loadIcon(SHOW));
            layer.setVisibility(true);
        }

        surfaceMaskPanel.setNeedsLayerUpdate(true);
        surfaceMaskPanel.repaint();
        actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ACTION_PROJECT));
    }

    /**
     * Removes the layer from the container and updates the manager.
     *
     * @param container The parent container holding this panel.
     */
    private void removeLayer(JPanel container) {
        container.remove(this);
        container.revalidate();
        container.repaint();
        if (layerManager.getCurrentLayer() == layer) {
            layerManager.setCurrentLayer(null);
        }
        layerManager.removeLayer(layer);

        surfaceMaskPanel.setNeedsLayerUpdate(true);
        surfaceMaskPanel.repaint();
        actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ACTION_PROJECT));
    }

    private void addClickListener() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectLayer();
            }
        });
    }

    /**
     * Handles the selection of the layer panel.
     */
    private void selectLayer() {
        for (Component component : container.getComponents()) {
            if (component instanceof LayerPanel) {
                component.setBackground(Color.WHITE);
            }
        }

        setBackground(Color.GREEN);
        layerManager.setCurrentLayer(layer);

        surfaceMaskPanel.setNeedsLayerUpdate(true);
        surfaceMaskPanel.repaint();
        actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ACTION_PROJECT));
    }

    /**
     * Loads an ImageIcon from the given resource path.
     *
     * @param icon The resource path to the icon file.
     * @return The ImageIcon, or null if the file could not be loaded.
     */
    private ImageIcon loadIcon(String icon) {
        URL iconUrl = LayerPanel.class.getClassLoader().getResource("/" + icon);
        if (iconUrl != null) {
            ImageIcon originalIcon = new ImageIcon(iconUrl);
            Image scaledImage = originalIcon.getImage().getScaledInstance(20, 20, Image.SCALE_SMOOTH);
            return new ImageIcon(scaledImage);
        } else {
            System.err.println("Icon not found: " + icon);
            return null;
        }
    }
}

