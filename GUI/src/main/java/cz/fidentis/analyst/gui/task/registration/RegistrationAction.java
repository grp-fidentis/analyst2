package cz.fidentis.analyst.gui.task.registration;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.canvas.CanvasState;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.face.HumanFaceState;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.impl.MeshTriangleImpl;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.drawables.DrawablePointCloud;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.data.face.events.HumanFaceTransformedEvent;
import cz.fidentis.analyst.data.face.events.LandmarksChangedEvent;
import cz.fidentis.analyst.data.face.events.MeshDistanceComputed;
import cz.fidentis.analyst.data.face.events.SymmetryPlaneChangedEvent;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.landmarks.PrTransformation;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingVisitor;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.gui.elements.SpinSlider;
import cz.fidentis.analyst.gui.task.ControlPanelAction;
import cz.fidentis.analyst.landmarks.LandmarksDetector;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;

import javax.swing.*;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static cz.fidentis.analyst.engines.sampling.PointSamplingConfig.Method.*;

/**
 * Action listener for the ICP and Procrustes registration.
 * <p>
 * Besides the UX logic, this object stores the parameters and results of
 * manual, ICP, or Procrustes registration (alignment of human faces)
 * <p>
 * This object also serves as {@code HumanFaceListener}. It means that it is
 * invoked whenever one of the faces are changed and then can react to these
 * changes.
 * <p>
 * Changes made by these objects are announced to other listeners. Following events are triggered:
 * <ul>
 * <li>{@code MeshChangedEvent}</li>
 * <li></li>
 * </ul>
 *
 * @author Richard Pajersky
 * @author Radek Oslejsek
 * @author Daniel Schramm
 * @author Jan Popelas
 */
public class RegistrationAction extends ControlPanelAction<RegistrationPanel> implements HumanFaceListener {

    public static final int MINIMAL_SIGNIFICANT_POINTS = 5;
    private static final double X_ANGLE_ROTATION_INCREMENT = 30;

    /**
     * Point cloud of sub-sampled secondary face
     */
    private int pointCloudSlot = -1;

    /*
     * Coloring threshold and statistical values of feature point distances:
     */
    private double fpThreshold = 5.0;

    /**
     * The old state of transformed secondary face for possible fall back
     */
    private HumanFaceState secFaceState = null;

    /**
     * Working with the sub-sampling density slider triggers frequent re-computation of samples.
     * In this case, calling the sampling algorithm via {@code PointSamplingServices.sample()}
     * is inefficient because new and new object is created and often new and new internal
     * structures are initiated. Therefore, the stateful invocation is used instead, when
     * the visitor is obtained and reused (with different settings) until the strategy is changed.
     */
    private PointSamplingVisitor samplingVisitor;

    /**
     * Constructor.
     * A new {@code RegistrationPanel} is instantiated and added to the {@code topControlPane}
     *
     * @param canvas         OpenGL canvas
     * @param task          Faces processed by current analytical task
     * @param topControlPane A top component when a new control panel is placed
     */
    public RegistrationAction(Canvas canvas, Task task, JTabbedPane topControlPane) {
        super(canvas, task, topControlPane);
        setControlPanel(new RegistrationPanel(this));

        setShowHideCode(
                () -> { // SHOW
                    highlightCloseFeaturePoints();
                    pointCloudSlot = drawPointSamples(
                            getCanvas().getScene().getSecondaryFaceSlot(),
                            pointCloudSlot,
                            getControlPanel().getIcpUndersamplingStrength());
                },
                () -> { // HIDE
                    secFaceState = null; // clear cached state
                    getScene().setOtherDrawable(pointCloudSlot, null);
                    pointCloudSlot = -1;
                }
        );

        highlightCloseFeaturePoints();

        // Be informed about changes in faces performed by other GUI elements
        getPrimaryDrawableFace().getHumanFace().registerListener(this);
        getSecondaryDrawableFace().getHumanFace().registerListener(this);

        getControlPanel().setEnabledOpenCL(
                OpenCLServices.isOpenCLAvailable()
        );

        getControlPanel().setEnabledPlanes(
                getPrimaryFace().hasSymmetryPlane()
                        && getSecondaryFace().hasSymmetryPlane()
        );

        getControlPanel().setEnabledProcrustes(
                getPrimaryFace().hasLandmarks()
                        && getSecondaryFace().hasLandmarks()
        );
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();

        switch (action) {
            case RegistrationPanel.ACTION_COMMAND_REGISTER:
                String alg = getControlPanel().getRegistrationAlgorihm();
                this.secFaceState = getSecondaryFace().getState(); // cache original state for possible undo
                if (RegistrationPanel.AUTO_ALIGNMENT_ALGORITHM[0].equals(alg)) { // ICP
                    applyICP();
                } else if (RegistrationPanel.AUTO_ALIGNMENT_ALGORITHM[1].equals(alg)) { // Procrustes
                    applyProcrustes();
                } else if (RegistrationPanel.AUTO_ALIGNMENT_ALGORITHM[2].equals(alg)) { // Symmetry
                    alignSymmetryPlanes();
                } else if (RegistrationPanel.AUTO_ALIGNMENT_ALGORITHM[3].equals(alg)) { // Detection based
                    alignDetection();
                }
                highlightCloseFeaturePoints();
                if (RegistrationPanel.AUTO_ALIGNMENT_ALGORITHM[1].equals(alg)) { // Procrustes also transforms the primary face
                    getPrimaryFace().announceEvent(new HumanFaceTransformedEvent(getPrimaryFace(), "", this));
                }
                getSecondaryFace().announceEvent(new HumanFaceTransformedEvent(getSecondaryFace(), "", this));
                pointCloudSlot = drawPointSamples(getCanvas().getScene().getSecondaryFaceSlot(),
                        pointCloudSlot, getControlPanel().getIcpUndersamplingStrength());
                break;
            case RegistrationPanel.ACTION_COMMAND_MANUAL_TRANSFORMATION_IN_PROGRESS:
                getControlPanel().setTranslationFactor(getScene()); // adapt movement speed to the size of the scene
                if (this.secFaceState == null) {
                    this.secFaceState = getSecondaryFace().getState(); // cache original state for possible undo
                }
                FaceRegistrationServices.transformFace(
                        getSecondaryDrawableFace().getHumanFace(),
                        getControlPanel().getAndClearManualRotation(),
                        getControlPanel().getAndClearManualTranslation(),
                        getControlPanel().getAndClearManualScale()
                );
                highlightCloseFeaturePoints();
                getSecondaryFace().announceEvent(new HumanFaceTransformedEvent(
                        getSecondaryFace(), "", this, false) // transformation under progress
                );
                break;
            case RegistrationPanel.ACTION_COMMAND_MANUAL_TRANSFORMATION_FINISHED:
                getSecondaryFace().announceEvent(new HumanFaceTransformedEvent(
                        getSecondaryFace(), "", this, true) // finished transformation
                );
                pointCloudSlot = drawPointSamples(
                        getCanvas().getScene().getSecondaryFaceSlot(),
                        pointCloudSlot,
                        getControlPanel().getIcpUndersamplingStrength()
                );
                break;
            case RegistrationPanel.ACTION_COMMAND_UNDO_TRANSFORMATION:
                if (this.secFaceState != null) {
                    getSecondaryFace().setState(this.secFaceState);
                    getScene().setHumanFace(getScene().getSecondaryFaceSlot(), getSecondaryFace()); // update drawable
                    highlightCloseFeaturePoints();
                    pointCloudSlot = drawPointSamples(getCanvas().getScene().getSecondaryFaceSlot(),
                            pointCloudSlot, getControlPanel().getIcpUndersamplingStrength());
                    getSecondaryFace().announceEvent(
                            new HumanFaceTransformedEvent(getSecondaryFace(), "", this, true)
                    );
                    this.secFaceState = null;
                }
                break;
            case RegistrationPanel.ACTION_COMMAND_FP_CLOSENESS_THRESHOLD:
                fpThreshold = ((Number) (((JFormattedTextField) ae.getSource()).getValue())).doubleValue();
                highlightCloseFeaturePoints();
                break;
            case RegistrationPanel.ACTION_COMMAND_POINT_SAMPLING_STRENGTH:
                int numSamples = ((SpinSlider) ae.getSource()).getValue().intValue();
                pointCloudSlot = drawPointSamples(
                        getCanvas().getScene().getSecondaryFaceSlot(),
                        pointCloudSlot, numSamples);
                break;
            case RegistrationPanel.ACTION_COMMAND_POINT_SAMPLING_STRATEGY:
                var strategy = getSamplingStrategy(getControlPanel().getIcpUndersamplingStrength());
                if (samplingVisitor != null) {
                    samplingVisitor.dispose();
                }
                samplingVisitor = strategy == null ? null : strategy.getVisitor();
                if (samplingVisitor != null) {
                    getPrimaryFace().getMeshModel().getFacets().forEach(facet -> samplingVisitor.visitMeshFacet(facet));
                }
                pointCloudSlot = drawPointSamples(
                        getCanvas().getScene().getSecondaryFaceSlot(),
                        pointCloudSlot,
                        getControlPanel().getIcpUndersamplingStrength());
                break;
            default:
                // do nothing
        }

        renderScene();
    }

    @Override
    public void acceptEvent(HumanFaceEvent event) {
        if (event.getIssuer() == this) {
            return;
        }

        if (event instanceof MeshDistanceComputed hdEvent) { // update stats
            getControlPanel().updateDistanceStats(
                    hdEvent.getMeshDistances().getDistanceStats(),
                    hdEvent.getMeshDistances().getWeightedStats());
        }

        if (event instanceof SymmetryPlaneChangedEvent) {
            getControlPanel().setEnabledPlanes(
                    getPrimaryFace().hasSymmetryPlane()
                            && getSecondaryFace().hasSymmetryPlane());
        }

        if (event instanceof LandmarksChangedEvent) {
            getControlPanel().setEnabledProcrustes(getPrimaryFace().hasLandmarks() && getSecondaryFace().hasLandmarks());
        }

    }

    /**
     * Calculates Procrustes analysis and transforms secondary face.
     */
    protected void applyProcrustes() {
        Logger out = Logger.measureTime();
        PrTransformation tr = FaceRegistrationServices.alignFeaturePoints(
                getPrimaryFace(),
                getSecondaryFace(),
                getControlPanel().getScaleParam());

        out.printDuration("Procrustes for models with "
                + getPrimaryFace().getMeshModel().getNumVertices()
                + "/"
                + getSecondaryFace().getMeshModel().getNumVertices()
                + " vertices and "
                + getPrimaryFace().getAllLandmarks().size()
                + " feature points"
        );

        if (tr == null) {
            JOptionPane.showMessageDialog(new JFrame("Procrustes algorithm has failed"),
                    "Not enough feature points of the same type among both faces");
        }
    }

    protected void applyICP() {
        Logger out = Logger.measureTime();
        PointSamplingConfig samplingStrategy = getSamplingStrategy();

        FaceRegistrationServices.alignMeshes(
                getSecondaryFace(), // is transformed
                new IcpConfig(
                        getPrimaryFace().getMeshModel(),
                        getControlPanel().getMaxIcpIterParam(),
                        getControlPanel().getScaleParam(),
                        getControlPanel().getMinIcpErrorParam(),
                        samplingStrategy,
                        getControlPanel().getIcpAutoCropParam() ? 0 : -1));

        out.printDuration("ICP for models with "
                + getPrimaryFace().getMeshModel().getNumVertices()
                + "/"
                + getSecondaryFace().getMeshModel().getNumVertices()
                + " vertices. Sub-sampling of the secondary face: "
                + samplingStrategy
        );
    }

    /**
     * Calculates feature points which are too far away and changes their color
     * to green otherwise set color to default
     */
    private void highlightCloseFeaturePoints() {
        if (getPrimaryDrawableFace() == null) { // scene not yet initiated
            return;
        }

        if (getPrimaryFeaturePoints() == null
                || getSecondaryFeaturePoints() == null
                || getPrimaryFeaturePoints().getFeaturePoints().size() != getSecondaryFeaturePoints().getFeaturePoints().size()) {
            return;
        }

        double fpMaxDist = Double.NEGATIVE_INFINITY;
        double fpMinDist = Double.POSITIVE_INFINITY;
        double distSum = 0.0;
        for (int i = 0; i < getPrimaryFeaturePoints().getFeaturePoints().size(); i++) {
            Vector3d v = new Vector3d(getPrimaryFeaturePoints().getFeaturePoints().get(i).getPosition());
            v.sub(getSecondaryFeaturePoints().getFeaturePoints().get(i).getPosition());
            double distance = v.length();
            if (distance > fpThreshold) {
                getPrimaryFeaturePoints().resetColorToDefault(i);
                getSecondaryFeaturePoints().resetColorToDefault(i);
            } else {
                getPrimaryFeaturePoints().setColor(i, Color.GREEN);
                getSecondaryFeaturePoints().setColor(i, Color.GREEN);
            }
            fpMaxDist = Math.max(fpMaxDist, distance);
            fpMinDist = Math.min(fpMinDist, distance);
            distSum += distance;
        }
        double fpAvgDist = distSum / getPrimaryFeaturePoints().getFeaturePoints().size();
        // to do: show ACF dist
    }

    private void alignSymmetryPlanes() {
        FaceRegistrationServices.alignSymmetryPlanes(
                getPrimaryDrawableFace().getHumanFace(),
                getSecondaryDrawableFace().getHumanFace(), // is transformed
                true // forbid rotation around normal
        );
    }

    private void alignDetection() {
        List<Landmark> priModelLandmarks = new ArrayList<>();
        boolean priModelPredefinedFPs = getOrDetectLandmarks(
                priModelLandmarks,
                getCanvas().getScene().getPrimaryFaceSlot());

        List<Landmark> secModelLandmarks = new ArrayList<>();
        boolean secModelPredefinedFPs = getOrDetectLandmarks(
                secModelLandmarks,
                getCanvas().getScene().getSecondaryFaceSlot());

        if (priModelLandmarks.isEmpty() || secModelLandmarks.isEmpty()) {
            JOptionPane.showMessageDialog(new JFrame("Landmarks detection failed"),
                    "Not enough feature points detected or predefined on both models");
            return;
        }

        // Add auto-detected feature points into faces temporarily
        if (!priModelPredefinedFPs) {
            priModelLandmarks.forEach(landmark -> getPrimaryFace().addCustomLandmark(landmark));
        }
        if (!secModelPredefinedFPs) {
            secModelLandmarks.forEach(landmark -> getSecondaryFace().addCustomLandmark(landmark));
        }

        // Align using acquired feature points
        FaceRegistrationServices.alignFeaturePoints(
                getPrimaryFace(),
                getSecondaryFace(),
                getControlPanel().getScaleParam()
        );

        // Remove auto-detected feature points from faces
        if (!priModelPredefinedFPs) {
            priModelLandmarks.forEach(landmark -> getPrimaryFace().removeCustomLandmark(landmark));
        }
        if (!secModelPredefinedFPs) {
            secModelLandmarks.forEach(landmark -> getSecondaryFace().removeCustomLandmark(landmark));
        }

        // Align using ICP
        HumanFace body = getPrimaryFace();
        HumanFace face = getSecondaryFace();
        FaceStateServices.updateBoundingBox(body, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        FaceStateServices.updateBoundingBox(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        if (body.getBoundingBox().diagonalLength() < face.getBoundingBox().diagonalLength()) {
            body = getSecondaryFace();
            face = getPrimaryFace();
        }

        PointSamplingConfig samplingStrategy = getSamplingStrategy();
        FaceRegistrationServices.alignMeshes(
                face, // is transformed
                new IcpConfig(
                        body.getMeshModel(),
                        getControlPanel().getMaxIcpIterParam(),
                        getControlPanel().getScaleParam(),
                        getControlPanel().getMinIcpErrorParam(),
                        samplingStrategy,
                        getControlPanel().getIcpAutoCropParam() ? 0 : -1));

    }

    private PointSamplingConfig getSamplingStrategy() {
        return getSamplingStrategy(getControlPanel().getIcpUndersamplingStrength());
    }

    private PointSamplingConfig getSamplingStrategy(int strength) {
        String st = getControlPanel().getIcpUdersamplingStrategy();

        if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[0])) {
            //return new PointSamplingConfig(NO_SAMPLING, strength);
            return null;
        } else if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[1])) {
            return new PointSamplingConfig(RANDOM, strength);
        } else if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[2])) {
            return new PointSamplingConfig(POISSON, strength, MeshTriangleImpl.Smoothing.SHAPE);
        } else if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[3])) {
            return new PointSamplingConfig(CURVATURE_GAUSSIAN, strength);
        } else if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[4])) {
            return new PointSamplingConfig(CURVATURE_MEAN, strength);
        } else if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[5])) {
            return new PointSamplingConfig(UNIFORM_SPACE, strength);
        } else if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[6])) {
            return new PointSamplingConfig(UNIFORM_SURFACE, strength);
        } else if (st.equals(RegistrationPanel.POINT_SAMPLING_STRATEGIES[7])) {
            return new PointSamplingConfig(POISSON_OPENCL, strength, MeshTriangleImpl.Smoothing.SHAPE);
        } else {
            return null;
        }
    }

    private int drawPointSamples(int faceSlot, int cloudSlot, int numSamples) {
        HumanFace face = FaceService.INSTANCE.getFaceByReference(getTask().getFaces().get(faceSlot));
        if (face == null) {
            return -1;
        }

        if (samplingVisitor == null) { // don't show
            if (cloudSlot != -1) {
                getScene().setOtherDrawable(cloudSlot, null); // hide existing points
            }
            return -1;
        } else {
            samplingVisitor.setRequiredSamples(numSamples);
        }

        synchronized (this) {
            if (cloudSlot == -1) {
                cloudSlot = getCanvas().getScene().getFreeSlotForOtherDrawables();
            }
            getScene().setOtherDrawable(
                    cloudSlot,
                    new DrawablePointCloud(samplingVisitor.getSamples()));
        }
        return cloudSlot;
    }

    /**
     * Returns either existing or auto-detected feature points.
     *
     * @param fpList output list
     * @param faceSlot face slot
     * @return {@code true} if pre-defined landmarks are used (put into the output list)
     */
    protected boolean getOrDetectLandmarks(List<Landmark> fpList, int faceSlot) {
        String modelName = getScene().getDrawableFace(faceSlot).getHumanFace().getShortName();

        DrawableFeaturePoints fps = getScene().getDrawableFeaturePoints(faceSlot);
        if (fps != null) {
            fpList.addAll(fps.getFeaturePoints().stream()
                    .filter(Landmark::isStandardFeaturePoint)
                    .toList());
        }
        if (!fpList.isEmpty()) { // return existing feature points
            Logger.print(modelName + ": Using existing feature points for pre-alignment.");
            return true;
        }

        // AUTODETECT:
        SymmetryConfig symmetryConfig = new SymmetryConfig(
                SymmetryConfig.Method.ROBUST_POINT_CLOUD,
                new PointSamplingConfig(UNIFORM_SPACE, 200),
                200);
        LandmarksDetector service = LandmarksDetector.getDetector(getCanvas());
        Logger out = Logger.measureTime();

        // save scene state
        CanvasState state = getCanvas().getState();

        // compute (the scene is changed)
        fpList.addAll(service.recognizeFromMultipleAngles(
                faceSlot,
                symmetryConfig, MINIMAL_SIGNIFICANT_POINTS, X_ANGLE_ROTATION_INCREMENT));

        // restore scene state
        getCanvas().setState(state);

        if (fpList.isEmpty()) {
            out.printDuration(modelName + ": Auto-detection failed");
        } else {
            out.printDuration(modelName + ": Using auto-detected feature points");
        }

        return false;
    }
}
