/**
 * A control panel for filtering faces in the project.
 */
package cz.fidentis.analyst.gui.project.filter;
