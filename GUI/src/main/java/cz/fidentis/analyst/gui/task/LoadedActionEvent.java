package cz.fidentis.analyst.gui.task;

import java.awt.event.ActionEvent;

/**
 * A subclass of {@link ActionEvent} extended to carry additional data as payload.
 * 
 * @author Daniel Schramm
 */
public class LoadedActionEvent extends ActionEvent {
    
    private final Object data;

    /**
     * Constructor.
     *
     * @param source The object that originated the event
     * @param id An integer that identifies the event.
     *           For information on allowable values, see the class description
     *           for {@link ActionEvent}
     * @param command A string that may specify a command (possibly one
     *                of several) associated with the event
     * @param data Payload data of the event
     * @throws IllegalArgumentException if {@code source} is null
     */
    public LoadedActionEvent(Object source, int id, String command, Object data) {
        super(source, id, command);
        this.data = data;
    }

    /**
     * Constructor.
     *
     * @param source The object that originated the event
     * @param id An integer that identifies the event.
     *           For information on allowable values, see the class description
     *           for {@link ActionEvent}
     * @param command A string that may specify a command (possibly one
     *                of several) associated with the event
     * @param modifiers The modifier keys down during event (shift, ctrl, alt, meta).
     *                  Passing negative parameter is not recommended.
     *                  Zero value means that no modifiers were passed
     * @param data Payload data of the event
     * @throws IllegalArgumentException if {@code source} is null
     */
    public LoadedActionEvent(Object source, int id, String command, int modifiers, Object data) {
        super(source, id, command, modifiers);
        this.data = data;
    }

    /**
     * Constructor.
     *
     * @param source The object that originated the event
     * @param id An integer that identifies the event.
     *           For information on allowable values, see the class description
     *           for {@link ActionEvent}
     * @param command A string that may specify a command (possibly one
     *                of several) associated with the event
     * @param modifiers The modifier keys down during event (shift, ctrl, alt, meta).
     *                  Passing negative parameter is not recommended.
     *                  Zero value means that no modifiers were passed
     * @param when A long that gives the time the event occurred.
     *             Passing negative or zero value is not recommended
     * @param data Payload data of the event
     * @throws IllegalArgumentException if {@code source} is null
     */
    public LoadedActionEvent(Object source, int id, String command, long when, int modifiers, Object data) {
        super(source, id, command, when, modifiers);
        this.data = data;
    }

    /**
     * Returns payload data of the action event.
     * 
     * @return Payload data of the action event
     */
    public Object getData() {
        return data;
    }
}
