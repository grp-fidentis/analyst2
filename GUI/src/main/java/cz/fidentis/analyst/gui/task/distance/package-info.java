/**
 * Control panel for distance computation and visualization, e.g. Hausdorff distance.
 */
package cz.fidentis.analyst.gui.task.distance;
