package cz.fidentis.analyst.gui.project.filter;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.gui.project.ProjectPanel;
import cz.fidentis.analyst.gui.project.ProjectPanelAction;
import cz.fidentis.analyst.gui.project.table.ProjectTable;
import cz.fidentis.analyst.gui.project.table.ProjectTableModel;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Action listener for panel used to filter faces in the project window.
 *
 * @author Radek Oslejsek
 */
public class ProjectFilteringAction extends ProjectPanelAction<FilterPanel> {
    
    private final FilterHistory filterHistory = new FilterHistory();

    /**
     * Constructor for project window when project panel is provided
     *
     * @param projectPanel Project panel
     * @param topControlPane Top component for placing control panels
     */
    public ProjectFilteringAction(ProjectPanel projectPanel, JTabbedPane topControlPane) {
        super(projectPanel, topControlPane);

        setControlPanel(new FilterPanel(this));

        setShowHideCode(
                () -> { // SHOW
                },
                () -> { // HIDE
                }
        );        
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        
        switch (action) {
            case FilterPanel.ACTION_APPLY_FILTER:
                applyFilter();
                break;
            case FilterPanel.ACTION_CLEAR_FILTERS:
                clearFiltering();
                break;
            case FilterPanel.ACTION_REVERT_FILTERING:
                revertFiltering();
                break;
            default:
        }
    }
 
    protected void applyFilter() {
        ProjectTable table = getProjectPanel().getProjectTable();
        ProjectTableModel model = table.getProjectTableModel();
        
        table.clearSelection();
        
        List<Integer> filteredRows = new ArrayList<>();
        for (int row = 0; row < model.getRowCount() - filterHistory.getTotalFaces(); row++) { 
            FaceReference face = table.getFaceAt(row);
            if (!fitsNameFilter(face.getName()) && !fitsFeaturePointsFilter(face)) {
                filteredRows.add(row);
            }
        }
        
        List<String> filteredFaces = table.filterRows(filteredRows);
        filterHistory.addFaces(filteredFaces);
        getControlPanel().setFilteringState(filterHistory.isEmpty());
    }
    
    protected void clearFiltering() {
        int num = filterHistory.clear();
        getProjectPanel().getProjectTable().unfilterRows(num);
        getProjectPanel().getProjectTable().clearSelection();
        getControlPanel().setFilteringState(filterHistory.isEmpty());
        getProjectPanel().repaint();
    }
    
    protected void revertFiltering() {
        int removedRows = filterHistory.revertLastFiltering();
        getProjectPanel().getProjectTable().unfilterRows(removedRows);
        getProjectPanel().getProjectTable().clearSelection();
        getControlPanel().setFilteringState(filterHistory.isEmpty());
        getProjectPanel().repaint();
    }
    
    /**
     * Checks whether face should be filtered based on name filter options
     * @param faceName String name of face
     * @return true if face should be filtered, false otherwise
     */
    public boolean fitsNameFilter(String faceName) {
        String faceNameFilter =  getControlPanel().getFaceNameFilter(false);
        if (faceNameFilter != null) {
            return faceName.contains(faceNameFilter);
        } else {
            faceNameFilter = getControlPanel().getFaceNameFilter(true);
            return faceNameFilter != null && !faceName.contains(faceNameFilter);
        }
    }    

    /**
     * Checks whether face should be filtered based on feature points filter options
     * @param faceReference Face
     * @return true if face should be filtered, false otherwise
     */
    public boolean fitsFeaturePointsFilter(FaceReference faceReference) {
        File fpFile = faceReference.getFeaturePointsFile();
        
        if (getControlPanel().isAnyFpFilterEnabled() && fpFile != null) { // there is a FP file
            return true;
        }
        
        if (getControlPanel().isNoneFpFilterEnabled() && fpFile == null) { // there is no FP file
            return true;
        }
        
        // Has selected feature points (from feature points panel)
        List<Landmark> fps = getControlPanel().getSelectedFeaturePoints();
        if (getControlPanel().isSelectedFpFilterEnabled() && !fps.isEmpty()) {
            HumanFace face = FaceService.INSTANCE.getFaceByReference(faceReference);
            if (face != null && face.getAllLandmarks().stream()
                    .map(Landmark::getType)
                    .collect(Collectors.toList()).containsAll(fps)) {
                return true;
            } 
        }
        
        return false;
    }
}
