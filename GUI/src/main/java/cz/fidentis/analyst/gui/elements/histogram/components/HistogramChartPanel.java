package cz.fidentis.analyst.gui.elements.histogram.components;

import cz.fidentis.analyst.gui.elements.histogram.scales.LinearScale;
import cz.fidentis.analyst.gui.elements.histogram.scales.Scale;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Panel for drawing a histogram chart.
 *
 * @author Jakub Nezval
 */
public class HistogramChartPanel extends JPanel {

    /*
     * Used colors and strokes.
     */
    private static final Stroke STROKE = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    private static final Color GRID_LINE_COLOR = new Color(128, 128, 128, 51);

    /*
     * Grid configuration.
     */
    private static final int VERTICAL_LINES = 9;
    private static final int HORIZONTAL_LINES = 9;

    private Scale<Double, Double> scale;

    private Paint strokePaint;
    private Paint fillPaint;

    private double[] yRawValues;
    private double yMaxValue;

    private int[] xValues;
    private int[] yValues;

    /**
     * Creates a new form {@link HistogramChartPanel}.
     * By default, {@link LinearScale} is used.
     */
    public HistogramChartPanel() {
        initComponents();

        this.scale = new LinearScale();

        // defaults
        this.strokePaint = new Color(8, 110, 103);
        this.fillPaint = new Color(8, 110, 103, 128);
    }

    /**
     * Set Y values. Index of a Y value is used as corresponding X value.
     * Method creates an inner copy of {@code yValues}.
     * Calling this method triggers {@link Component#repaint()}.
     *
     * @param yValues Y values
     */
    public void setValues(List<Double> yValues) {
        if (yValues == null) {
            this.yValues = null;
            this.yMaxValue = 10;
            this.updateScaleDomainRange();
            this.repaint();
            return;
        }

        this.yRawValues = new double[yValues.size()];
        this.yMaxValue = Double.NEGATIVE_INFINITY;

        for (int i = 0; i < yValues.size(); i++) {
            this.yRawValues[i] = yValues.get(i);

            if (this.yRawValues[i] > this.yMaxValue) {
                this.yMaxValue = this.yRawValues[i];
            }
        }
        this.yMaxValue *= 1.075;

        this.xValues = IntStream.rangeClosed(0, yValues.size()).toArray();

        this.updateScaleDomainRange();
        this.scaleRawYValues();
        this.repaint();
    }

    /**
     * Set scale for Y axis.
     * Calling this method triggers {@link Component#repaint()}.
     *
     * @param scale scale
     */
    public void setScale(Scale<Double, Double> scale) {
        this.scale = scale;

        this.updateScaleDomainRange();
        if (this.yValues != null) {
            this.scaleRawYValues();
        }
        this.repaint();
    }

    /**
     * Set {@link Paint} for chart stroke and fill.
     * Calling this method triggers {@link Component#repaint()}.
     *
     * @param stroke stroke
     * @param fill fill
     */
    public void setPaint(Paint stroke, Paint fill) {
        this.strokePaint = stroke;
        this.fillPaint = fill;

        this.repaint();
    }

    private void scaleRawYValues() {
        this.yValues = Arrays.stream(this.yRawValues)
                .mapToInt(y -> this.getHeight() - (int) Math.round(this.scale.map(y)))
                .toArray();
    }

    private void updateScaleDomainRange() {
        this.scale.setDomain(0d, this.yMaxValue);
        this.scale.setRange(0d, (double) this.getHeight());
    }

    private void drawGrid(Graphics2D g) {
        g.setColor(GRID_LINE_COLOR);

        var xDelta = this.getWidth() / (HORIZONTAL_LINES - 1);
        for (var xSegments = 0; xSegments < (HORIZONTAL_LINES - 1); xSegments++) {
            g.drawLine(xSegments * xDelta, 0, xSegments * xDelta, this.getHeight());
        }
        g.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1, this.getHeight());

        var verticalValueDelta = this.yMaxValue / (VERTICAL_LINES - 1);
        for (var yRaw = 0d; yRaw < this.yMaxValue; yRaw += verticalValueDelta) {
            var yCoordinate = this.getHeight() - 1 - ((int) (double) this.scale.map(yRaw));
            g.drawLine(0, yCoordinate, this.getWidth(), yCoordinate);
        }
        g.drawLine(0, 0, this.getWidth(), 0);
    }

    private void drawPolygon(Graphics2D g) {
        var polygon = new Polygon(this.xValues, this.yValues, this.yValues.length);
        polygon.addPoint(this.yValues.length, this.getHeight());
        polygon.addPoint(0, this.getHeight());

        g.setPaint(this.fillPaint);
        g.fillPolygon(polygon);
    }

    private void drawPolyline(Graphics2D g) {
        g.setPaint(this.strokePaint);
        g.setStroke(HistogramChartPanel.STROKE);
        g.drawPolyline(this.xValues, this.yValues, this.yValues.length);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        var g2d = (Graphics2D) g;
        this.drawGrid(g2d);

        if (this.yValues == null) {
            return;
        }

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        this.drawPolygon(g2d);
        this.drawPolyline(g2d);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 594, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 390, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
