package cz.fidentis.analyst.gui.elements.histogram.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Consumer;

/**
 * Listener for a pair of {@link Bound}s that encapsulates all {@link Bound}-pair related logic.
 *
 * @author Jakub Nezval
 */
public class BoundPairListener implements ActionListener {
    private final Consumer<Bound> lowerBoundSetter;
    private final Consumer<Bound> upperBoundSetter;
    private final ChartPainter chartPainter;
    private final Consumer<ActionEvent> emitterTrigger;

    private Bound lowerBound;
    private Bound upperBound;

    /**
     * Creates a {@link BoundPairListener}.
     * Consumers {@code lowerBoundSetter} and {@code upperBoundSetter}
     * are guaranteed to be called before {@code emitterTrigger} consumer.
     *
     * @param aBound first bound
     * @param bBound second bound
     * @param lowerBoundSetter consumer for setting a bound with a lower value
     * @param upperBoundSetter consumer for setting a bound with a greater value
     * @param chartPainter middleman for repainting {@link HistogramChartPanel}
     * @param emitterTrigger consumer for propagating ActionEvents further
     */
    public BoundPairListener(
            Bound aBound, Bound bBound,
            Consumer<Bound> lowerBoundSetter, Consumer<Bound> upperBoundSetter,
            ChartPainter chartPainter,
            Consumer<ActionEvent> emitterTrigger
    ) {
        this.lowerBound = aBound;
        this.upperBound = bBound;

        this.lowerBoundSetter = lowerBoundSetter;
        this.upperBoundSetter = upperBoundSetter;
        this.chartPainter = chartPainter;
        this.emitterTrigger = emitterTrigger;

        this.orderBounds();
        this.setChartPaints();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case Bound.ACTION_COMMAND_BOUND_MOVED:
                this.orderBounds();
                this.lowerBoundSetter.accept(this.lowerBound);
                this.upperBoundSetter.accept(this.upperBound);
                this.setChartPaints();
                this.emitterTrigger.accept(e);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + e.getActionCommand());
        }
    }

    private void setChartPaints() {
        this.chartPainter.paintChart(this.lowerBound.getValue(), this.upperBound.getValue());
    }

    private void orderBounds() {
        if (this.lowerBound.getValue() > this.upperBound.getValue()) {
            var tmp = this.lowerBound;
            this.lowerBound = this.upperBound;
            this.upperBound = tmp;
        }
    }
}
