package cz.fidentis.analyst.gui.app.tools.opencl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingVisitor;
import cz.fidentis.analyst.opencl.OpenCLServices;

import java.io.IOException;
import java.util.List;

/**
 * Benchmark comparing performance between CPU-based and GPU-based Poisson sub-sampling implementations.
 * The visitors are recreated for each face
 *
 * @author Marek Horský
 */
public class PoissonBenchmark {
    private static final int SAMPLES_START = 5;
    private static final int SAMPLES_STEP = 0;
    private static final String PATH_DIR = "C:\\Users\\marek\\Desktop\\FidentisFaces\\multiple-scans";

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        List<MeshModel> models = BenchmarkUtils.getModels(PATH_DIR);
        if (!OpenCLServices.isOpenCLAvailable()) {
            System.out.println("OpenCL could not find supported platform. Benchmark terminated");
            return;
        }
        BenchmarkUtils.printGPU();
        PointSamplingConfig configGPU = new PointSamplingConfig(PointSamplingConfig.Method.POISSON_OPENCL, SAMPLES_START);
        PointSamplingConfig configCPU = new PointSamplingConfig(PointSamplingConfig.Method.POISSON, SAMPLES_START);
        visitorBenchmark(models.get(0).getFacets(), configGPU.getVisitor(), SAMPLES_START); // Compilation

        long timeGPU = benchmarkOnModels(configGPU, models);
        long timeCPU = benchmarkOnModels(configCPU, models);
        BenchmarkUtils.printComparison(timeCPU, timeGPU, models.size());
    }

    private static long benchmarkOnModels(PointSamplingConfig config, List<MeshModel> models) {
        int samples = SAMPLES_START;
        long time = 0;
        for (MeshModel model : models) {
            time += visitorBenchmark(model.getFacets(), config.getVisitor(), samples);
            samples += SAMPLES_STEP;
        }
        return time;
    }

    private static long visitorBenchmark(List<MeshFacet> facets, PointSamplingVisitor visitor, int samples) {
        facets.forEach(visitor::visitMeshFacet);
        visitor.setRequiredSamples(samples);
        long time = System.nanoTime();
        visitor.getSamples();
        time = System.nanoTime() - time;
        visitor.dispose();
        return time;
    }
}
