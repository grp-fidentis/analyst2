package cz.fidentis.analyst.gui.elements.surfacemaskdrawables;

import java.awt.Graphics;

/**
 * Interface for implementing the drawables of SurfaceMask2D
 * @author Mario Chromik
 */
public interface DrawableSurfaceMask2D {

    /**
     * The mask drawing method, overriden in each shape
     * @param g graphics
     */
    void draw(Graphics g);
}
