package cz.fidentis.analyst.gui.elements;

import javax.swing.*;
import java.awt.*;
import java.net.URI;

/**
 * An info button/icon providing a link into some web page.
 * 
 * @author Radek Oslejsek
 */
public class InfoLinkButton extends JButton {

    /**
     * Link
     */
    private URI uri;

    /**
     * Constructor.
     */
    public InfoLinkButton() {
        initComponents();
    }

    /**
     * Constructor.
     * @param uri Link to the web page
     */
    public InfoLinkButton(URI uri) {
        this();
        setUri(uri);
    }
    
    public void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Show an error window if the redirection to the URI fails.
     */
    public void showError() {
        JOptionPane.showMessageDialog(this,
                "Can't navigate to " + uri + System.lineSeparator() + "Type this address to your web browser manually.", 
                "Error",
                JOptionPane.WARNING_MESSAGE);
    }

    private void initComponents() {
        setText("");
        setIcon(new ImageIcon(getClass().getResource("/info.png"))); // NOI18N
        setBorderPainted(false);

        addActionListener(e -> {
            if (Desktop.isDesktopSupported()) {
                try {
                    Desktop.getDesktop().browse(uri);
                    return;
                } catch (Exception ex) {
                    showError();
                }
            } else {
                showError();
            }
        });
    }

}
