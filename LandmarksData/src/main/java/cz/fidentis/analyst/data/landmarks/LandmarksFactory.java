package cz.fidentis.analyst.data.landmarks;

import cz.fidentis.analyst.data.landmarks.impl.FeaturePointImpl;
import cz.fidentis.analyst.data.landmarks.impl.LandmarkImpl;

import javax.vecmath.Point3d;

/**
 * A factory that instantiates either user defined landmarks or standard anthropometric feature points.
 *
 * @author Radek Oslejsek
 * @author Jakub Kolman
 */
public interface LandmarksFactory {

    /**
     * Creates an instance of user-defined landmark.
     *
     * @param id A <b>unique</b> ID
     * @param position 3D position
     * @param name optional short name
     * @param description optional description
     * @return a user-defined landmark.
     */
    static Landmark createLandmark(int id, Point3d position, String name, String description) {
        return new LandmarkImpl(id, position, name, description);
    }

    /**
     * Creates an instance of standard anthropometric feature point.
     *
     * @param id A <b>unique</b> ID
     * @param position 3D position
     */
    static Landmark createFeaturePoint(int id, Point3d position) {
        return new FeaturePointImpl(id, position);
    }

    /**
     * Creates an instance of standard anthropometric feature point.
     *
     * @param code Code of the feature point
     * @param position 3D position
     */
    static Landmark createFeaturePointByCode(String code, Point3d position) {
        return new FeaturePointImpl(FeaturePointsTable.getInstance().getIdFromCode(code), position);
    }

    /**
     * Creates an instance of standard anthropometric feature point.
     *
     * @param name Name of the feature point
     * @param position 3D position
     */
    static Landmark createFeaturePointByName(String name, Point3d position) {
        return new FeaturePointImpl(FeaturePointsTable.getInstance().getIdFromName(name), position);
    }
}
