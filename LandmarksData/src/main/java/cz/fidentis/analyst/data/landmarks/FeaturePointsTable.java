package cz.fidentis.analyst.data.landmarks;

import cz.fidentis.analyst.data.landmarks.impl.FeaturePointsTableImpl;

import java.util.Set;

/**
 * A singleton providing information about standard anthropometric feature points,
 * i.e., their name, description, or code. Use {@link LandmarksFactory} to deal with specific
 * instances of landmarks or feature points located at a specific 3D positions.
 *
 * @author Jakub Kolman
 * @author Radek Oslejsek
 */
public interface FeaturePointsTable {

    /**
     * Returns singleton's instance.
     * @return object's instance
     */
    static FeaturePointsTable getInstance() {
        return FeaturePointsTableImpl.getInstance();
    }

    /**
     * Returns IDs of all known feature points
     *
     * @return IDs of all known feature points
     */
    Set<Integer> getIDs();

    /**
     * Returns name of the feature point.
     *
     * @param id ID aka type
     * @return the name of the feature point, {@code null} if the FP does not exist
     */
    String getFpName(int id);

    /**
     * Returns description of the feature point.
     *
     * @param id ID aka type
     * @return description of the feature point, {@code null} if the FP does not exist
     */
    String getFpDescription(int id);

    /**
     * Returns standard code of the feature point.
     *
     * @param id ID aka type
     * @return standard code of the feature point, {@code null} if the FP does not exist
     */
    String getFpCode(int id);

    /**
     * Return ID of the feature point based on a known FP code.
     *
     * @param code FP code
     * @return ID of the feature point, {@code -1} if FP with such code does not exist
     */
    int getIdFromCode(String code);

    /**
     * Return ID of the feature point based on a known FP code.
     *
     * @param name FP name
     * @return ID of the feature point, {@code -1} if FP with such code does not exist
     */
    int getIdFromName(String name);
}
