package cz.fidentis.analyst.data.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.FeaturePointsTable;
import cz.fidentis.analyst.data.landmarks.Landmark;

import javax.vecmath.Point3d;

/**
 * A standard anthropometric feature point with predefined (i.e., fixed) name, code, and description
 * shared by all instances.
 *
 * @author Radek Oslejsek
 * @author Jakub Kolman
 * @author Katerina Zarska
 */
public class FeaturePointImpl extends LandmarkAbstract {

    /**
     * Constructor.
     *
     * @param id A <b>unique</b> ID
     * @param position 3D position
     */
    public FeaturePointImpl(int id, Point3d position) {
        super(id, position);
    }

    @Override
    public Landmark clone() {
        return new FeaturePointImpl(getType(), getPosition());
    }

    @Override
    public String getName() {
        return FeaturePointsTable.getInstance().getFpName(getType());
    }

    @Override
    public void setName(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getDescription() {
        return FeaturePointsTable.getInstance().getFpDescription(getType());
    }

    @Override
    public void setDescription(String description) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getCode() {
        return FeaturePointsTable.getInstance().getFpCode(getType());
    }

    @Override
    public boolean isStandardFeaturePoint() {
        return true;
    }

}
