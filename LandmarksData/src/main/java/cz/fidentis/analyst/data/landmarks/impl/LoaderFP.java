package cz.fidentis.analyst.data.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.LandmarksFactory;

import javax.vecmath.Point3d;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used to import feature points from file of fp format
 * 
 * @author Jakub Kolman
 */
public class LoaderFP {

    private static final String GREATER_THAN_DELIMITER = ">";
    private static final String LESS_THAN_DELIMITER = "<";

    /**
     * Loads feature points form given file 
     * 
     * @param path File path
     * @param fileName File name
     * @return List of FeaturePoint
     * @throws IOException on error
     */
    public static List<Landmark> loadFeaturePoints(String path, String fileName) throws IOException {
        File file = new File(path, fileName);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            List<String> linesList = reader.lines().toList();

            if (linesList.isEmpty()) {
                throw new IOException(String.format("Feature point import file '%s' has wrong format", fileName));
            }

            List<Landmark> points = new ArrayList<>();
            for (int i = 1; i <= linesList.size() - 6; i += 5) {
                points.add(LandmarksFactory.createFeaturePoint(
                        Integer.parseInt(linesList.get(i).replaceAll("[^0-9]", "")),
                        new Point3d(
                                Double.parseDouble(linesList.get(i + 1).substring(
                                        linesList.get(i + 1).indexOf(GREATER_THAN_DELIMITER) + 1,
                                        linesList.get(i + 1).indexOf(LESS_THAN_DELIMITER, linesList.get(i + 1).indexOf(GREATER_THAN_DELIMITER)))),
                                Double.parseDouble(linesList.get(i + 2).substring(
                                        linesList.get(i + 2).indexOf(GREATER_THAN_DELIMITER) + 1,
                                        linesList.get(i + 2).indexOf(LESS_THAN_DELIMITER, linesList.get(i + 2).indexOf(GREATER_THAN_DELIMITER)))),
                                Double.parseDouble(linesList.get(i + 3).substring(
                                        linesList.get(i + 3).indexOf(GREATER_THAN_DELIMITER) + 1,
                                        linesList.get(i + 3).indexOf(LESS_THAN_DELIMITER, linesList.get(i + 3).indexOf(GREATER_THAN_DELIMITER)))))
                ));
            }
            return points;
        } catch (NumberFormatException e1) {
            throw new IOException(e1);
        }
    }
}
