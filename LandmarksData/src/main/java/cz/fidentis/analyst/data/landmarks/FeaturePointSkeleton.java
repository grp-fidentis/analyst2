package cz.fidentis.analyst.data.landmarks;

import java.util.*;

/**
 * Utility class for holding information about the correct pairing of points in the feature point skeleton.
 * 
 * @author Katerina Zarska
 */
public final class FeaturePointSkeleton {
    //List of feature points that are currently in the definition of the skeleton.
    public static final List<String> SKELETON_FEATURE_POINTS = 
        Arrays.asList("Exocanthion R", "Exocanthion L", "Endocanthion R", "Endocanthion L",
                 "Glabella", "Nasion", "Pronasale", "Subnasale", "Stomion", "Cheilion R",
                 "Cheilion L", "Labrale superius", "Labrale inferius", "Sublabiale");
    
    public static final Map<String, List<String>> SKELETON_MAP;
    
    //Map of correct pairings. Each line in the map is unique.
    static {
        SKELETON_MAP = new HashMap<>();
        SKELETON_MAP.put("Exocanthion R", new ArrayList<>(Arrays.asList("Endocanthion L")));
        SKELETON_MAP.put("Exocanthion L", new ArrayList<>(Arrays.asList("Endocanthion R")));
        SKELETON_MAP.put("Nasion", new ArrayList<>(Arrays.asList("Glabella", "Pronasale")));
        SKELETON_MAP.put("Pronasale", new ArrayList<>(Arrays.asList("Subnasale")));
        SKELETON_MAP.put("Labrale superius", new ArrayList<>(Arrays.asList("Subnasale", "Stomion","Cheilion R", "Cheilion L")));
        SKELETON_MAP.put("Stomion", new ArrayList<>(Arrays.asList("Cheilion R", "Cheilion L")));
        SKELETON_MAP.put("Labrale inferius", new ArrayList<>(Arrays.asList("Stomion","Cheilion R", "Cheilion L", "Sublabiale")));
    }
}
