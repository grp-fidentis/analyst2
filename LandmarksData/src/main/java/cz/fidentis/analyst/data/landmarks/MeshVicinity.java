package cz.fidentis.analyst.data.landmarks;

import javax.vecmath.Point3d;
import java.io.Serial;
import java.io.Serializable;

/**
 * Relationship between the landmark and the close vicinity of its mesh.
 *
 * @author Katerina Zarska
 * @author Radek Oslejsek
 * @param distance Distance to the related mesh
 * @param nearestPoint The closet point on the related mesh
 */
public record MeshVicinity(double distance, Point3d nearestPoint) implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * @author Katerina Zarska
     */
    public enum Location {
        /**
         * Outside the boundaries of unclosed mesh (the landmark cannot be projected onto the mesh)
         */
        OFF_THE_MESH,

        /**
         * Over the mash surface, but not exactly on the mesh surface (farther than the DISTANCE_THRESHOLD)
         */
        CLOSE_TO_MESH,

        /**
         * On the mesh (closer than the DISTANCE_THRESHOLD)
         */
        ON_THE_MESH
    }

    /**
     * Gets the position type based on distance threshold.
     *
     * @param distanceThreshold
     * @return position type
     */
    public Location getPositionType(double distanceThreshold) {
        if (distance == Double.POSITIVE_INFINITY) {
            return Location.OFF_THE_MESH;
        } else if (distance <= distanceThreshold) {
            return Location.ON_THE_MESH;
        } else {
            return  Location.CLOSE_TO_MESH;
        }
    }

    /**
     * Gets the position type same as getPositionType(double distanceThreshold) with distance threshold set to 0
     *
     * @return position type
     */
    public Location getPositionType() {
        return getPositionType(0);
    }
}
