package cz.fidentis.analyst.data.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.Landmark;

import javax.vecmath.Point3d;
import java.io.Serial;
import java.util.concurrent.ThreadLocalRandom;

/**
 * User-defined landmark with variable name, description, etc.
 *
 * @author Radek Oslejsek
 * @author Jakub Kolman
 * @author Katerina Zarska
 */
public class LandmarkImpl extends LandmarkAbstract {

    @Serial
    private static final long serialVersionUID = 1L;

    private String name;

    private String description;

        /**
         * Constructor.
         *
         * @param id A <b>unique</b> ID
         * @param position 3D position
         * @param name An optional <b>unique</b> short name. If blank or {@code null} then a random name is used.
         * @param description optional description
         */
    public LandmarkImpl(int id, Point3d position, String name, String description) {
        super(id, position);
        if (name == null || name.isBlank()) {
            this.name = "Landmark " + ThreadLocalRandom.current().nextInt(1, 999);
        } else {
            this.name = name;
        }
        this.description = (description == null) ? "" : description;
    }

    @Override
    public Landmark clone() {
        return new LandmarkImpl(getType(), getPosition(), getName(), getDescription());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getCode() {
        return "";
    }

    @Override
    public boolean isStandardFeaturePoint() {
        return false;
    }

}
