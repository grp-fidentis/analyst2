package cz.fidentis.analyst.data.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.Landmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Class used to export feature points to file of fp format
 * 
 * @author Jakub Kolman
 */
public class ExporterFP {
    
    /**
     * Exports feature points to file format fp at default location
     * 
     * @param featurePointList List of feature points
     * @param path Path to the directory with the face data
     * @param fileName Face name
     * @throws IOException on error
     */
    public static void exportToFP(List<Landmark> featurePointList, String path, String fileName) throws IOException {
        File fpOutputFile = new File(path, String.format("%s_landmarks.fp", fileName));

        // CSV is a normal text file, need a writer
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fpOutputFile))) {
            bw.write(String.format("<!--Saved by software Fidentis Analyst--><facialPoints model=\"%s\">", fileName));
            for (var featurePoint: featurePointList) {
                bw.newLine();
                bw.write(String.format("<facialPoint type=\"%s\">", featurePoint.getType()));
                bw.newLine();
                bw.write(String.format("<x>%f</x>", featurePoint.getX()));
                bw.newLine();
                bw.write(String.format("<y>%f</y>", featurePoint.getY()));
                bw.newLine();
                bw.write(String.format("<z>%f</z>", featurePoint.getZ()));
            }
            bw.newLine();
            bw.write("</facialPoints>");
        }
    }
        
}
