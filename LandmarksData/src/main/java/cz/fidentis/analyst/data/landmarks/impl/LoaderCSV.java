package cz.fidentis.analyst.data.landmarks.impl;


import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.LandmarksFactory;

import javax.vecmath.Point3d;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Class used to import feature points from file of CSV format
 * 
 * @author Jakub Kolman
 */
public class LoaderCSV {
    
    private static final String PRIMARY_COLUMN_DELIMITER = ";";
    private static final String SECONDARY_COLUMN_DELIMITER = ",";
    private static final String CODE_PREFIX_DELIMITER = " ";

    /**
     * Loads feature points from file of csv format.
     *
     * @param path File path
     * @param fileName File name
     * @return A list of feature points
     * @throws IOException on error
     */
    public static List<Landmark> loadFeaturePoints(String path, String fileName) throws IOException {
        File file = new File(path, fileName);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            String headerLine = reader.readLine();
            final String delimiter;
            if (headerLine.contains(PRIMARY_COLUMN_DELIMITER)) {
                delimiter = PRIMARY_COLUMN_DELIMITER;
            } else if (headerLine.contains(SECONDARY_COLUMN_DELIMITER)) {
                delimiter = SECONDARY_COLUMN_DELIMITER;
            } else {
                throw new IOException(String.format("Feature point import file '%s' has wrong format - unknown delimiter", fileName));
            }

            Stream<String> lines = reader.lines();
            List<List<String>> linesList = new ArrayList<>();

            linesList.add(Arrays.asList(headerLine.split(delimiter, -1)));
            lines.forEach(line -> linesList.add(Arrays.asList(line.split(delimiter, -1))));
            
            //if (linesList.stream().anyMatch(list -> list.size() != linesList.get(0).size())) {
            //    throw new IOException(String.format("Feature point import file '%s' has wrong format", fileName));
            //}

            // TODO: In real data sets, there can be multiple FP collections (lines) in a single file,
            // e.g., FPs before and after the face warping. Currently, we ignore them.
            List<Landmark> points = new ArrayList<>();
            for (int i = 1; i < linesList.get(0).size(); i += 3) {
                if (i >= linesList.get(1).size()) {
                    continue; // missing values (and separators) for corresponding columns defined in the header
                }
                if (linesList.get(1).get(i).isBlank()
                        || linesList.get(1).get(i+1).isBlank() 
                        || linesList.get(1).get(i+2).isBlank()) { // skip missing points
                    continue;
                }
                points.add(LandmarksFactory.createFeaturePointByCode(
                        getCode(linesList.get(0).get(i)),
                        new Point3d(Double.parseDouble(linesList.get(1).get(i)),
                                Double.parseDouble(linesList.get(1).get(i + 1)),
                                Double.parseDouble(linesList.get(1).get(i + 2)))));
            }
            return points;
        } catch (NumberFormatException e1) {
            throw new IOException(e1);
        }
    }

    private static String getCode(String str) {
        return str.substring(0, str.indexOf(CODE_PREFIX_DELIMITER));
    }
}
