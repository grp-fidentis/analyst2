package cz.fidentis.analyst.data.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.Landmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Class used to export feature points to file of csv format
 *
 * @author Jakub Kolman
 */
public class ExporterCSV {

    /**
     * Exports a file to set location in csv format <br>
     * File is located and named as {@code fileName}_landmarks.csv
     *
     * @param featurePointList List of feature points
     * @param path Path to the directory with the face data
     * @param fileName Face name
     * @throws IOException on error
     */
    public static void exportToCSV(List<Landmark> featurePointList, String path, String fileName) throws IOException {
        File csvOutputFile = new File(path, String.format("%s_landmarks.csv", fileName));

        // CSV is a normal text file, need a writer
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(csvOutputFile))) {
            bw.write("Scan name");
            for (var featurePoint: featurePointList) {
                bw.write(String.format(",%s x", featurePoint.getCode()));
                bw.write(String.format(",%s y", featurePoint.getCode()));
                bw.write(String.format(",%s z", featurePoint.getCode()));
            }
            bw.newLine();
            bw.write(String.format("%s", fileName));
            for (var featurePoint: featurePointList) {
                bw.write(",");
                bw.write(Double.toString(featurePoint.getX()));
                bw.write(",");
                bw.write(Double.toString(featurePoint.getY()));
                bw.write(",");
                bw.write(Double.toString(featurePoint.getZ()));
            }
        }
    }
}