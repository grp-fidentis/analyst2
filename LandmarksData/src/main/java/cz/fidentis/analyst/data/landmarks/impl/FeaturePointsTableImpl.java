package cz.fidentis.analyst.data.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.FeaturePointsTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A singleton data access object providing information about standard anthropometric feature points.
 *
 * @author Radek Oslejsek
 */
public final class FeaturePointsTableImpl implements FeaturePointsTable {

    private static final String FILE_NAME = "fp_text_default.csv";
    private static final String DELIMITER = ";";

    private final Map<Integer, FpInfo> fpsById = new HashMap<>();

    /**
     * Private constructor that read info from the CSV file.
     *
     * @throws IOException on error
     */
    private FeaturePointsTableImpl() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(FILE_NAME), StandardCharsets.UTF_8))) {
            reader.readLine(); // skip header line
            String line = reader.readLine();
            while (line != null) {
                String[] ll = line.split(DELIMITER);
                if (ll.length != 4) {
                    throw new IOException("Wrong number of columns");
                }
                int id = Integer.parseInt(ll[0]);
                FpInfo fpInfo = new FpInfo(id, ll[1], ll[2], ll[3]);
                fpsById.put(id, fpInfo);
                line = reader.readLine();
            }
        } catch (NumberFormatException ex) {
            throw new IOException(ex);
        }
    }

    /**
     * Returns singleton's instance.
     * @return object's instance
     */
    public static FeaturePointsTableImpl getInstance() {
        return FeaturePointsTableImpl.InstanceHolder.INSTANCE;
    }

    @Override
    public Set<Integer> getIDs() {
        return Collections.unmodifiableSet(fpsById.keySet());
    }

    @Override
    public String getFpName(int id) {
        FpInfo val = fpsById.getOrDefault(id, null);
        return (val == null) ? null : val.name();
    }

    @Override
    public String getFpDescription(int id) {
        FpInfo val = fpsById.getOrDefault(id, null);
        return (val == null) ? null : val.info();
    }

    @Override
    public String getFpCode(int id) {
        FpInfo val = fpsById.getOrDefault(id, null);
        return (val == null) ? null : val.code();
    }

    @Override
    public int getIdFromCode(String code) {
        return fpsById.entrySet().stream()
                .filter(e -> e.getValue().code().equalsIgnoreCase(code))
                .mapToInt(Map.Entry::getKey)
                .findAny()
                .orElse(-1);
    }

    @Override
    public int getIdFromName(String name) {
        return fpsById.entrySet().stream()
                .filter(e -> e.getValue().name().equalsIgnoreCase(name))
                .mapToInt(Map.Entry::getKey)
                .findAny()
                .orElse(-1);
    }


    /**
     * Provide thread safe singleton.
     *
     * @author Jakub Kolman
     */
    private static class InstanceHolder {
        public static final FeaturePointsTableImpl INSTANCE;

        static {
            try {
                INSTANCE = new FeaturePointsTableImpl();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Common info related to a single standard feature point.
     *
     * @author Radek Oslejsek
     * @param type type aka id
     * @param name short name
     * @param info description
     * @param code code
     */
    private record FpInfo(int type, String name, String info, String code)  {
    }
}
