package cz.fidentis.analyst.data.landmarks;

import cz.fidentis.analyst.data.mesh.IPosition;

import javax.vecmath.Point3d;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Either user-defined or standard anthropometric feature point.
 *
 * @author Radek Oslejsek
 */
public interface Landmark extends IPosition, Serializable {

    /**
     * Returns <b>unique</b> ID aka type of the feature point or user-defined landmark.
     * @return unique ID
     */
    int getType();

    @Override
    Point3d getPosition();

    /**
     * Update 3D position
     *
     * @param position New 3D position. Must not be {@code null}
     */
    void setPosition(Point3d position);

    /**
     * Clones the landmark
     * @return a clone of the landmark
     */
    Landmark clone();

    /**
     * Returns the closest mesh vicinity
     * @return the closest mesh vicinity
     */
    MeshVicinity getMeshVicinity();

    /**
     * Updates the closest mesh vicinity
     * @param meshVicinity new closest mesh vicinity
     */
    void setMeshVicinity(MeshVicinity meshVicinity);

    /**
     * Returns {@code true}, if the vicinity is set
     * @return {@code true}, if the vicinity is set
     */
    boolean hasMeshVicinity();

    /**
     * Returns a short name of the landmark, either user-defined or standard name of anthropometric feature point.
     * @return a short name
     */
    String getName();

    /**
     * Sets new short name.
     * @param name new name
     */
    void setName(String name);

    /**
     * Returns description of the landmark.
     * @return description of the landmark
     */
    String getDescription();

    /**
     * Updates description.
     * @param description new description
     */
    void setDescription(String description);

    /**
     * Returns a short code
     * @return a short code
     */
    String getCode();

    /**
     * Determines whether the landmark is standard anthropometric feature point or user-defined landmark.
     *
     * @return {@code true} if the landmark is a standard anthropometric feature point
     */
    boolean isStandardFeaturePoint();

    /**
     * Determines whether the landmark is standard anthropometric feature point or user-defined landmark.
     *
     * @return {@code true} if the landmark is a user-defined landmark
     */
    default boolean isUserDefinedLandmark() {
        return !isStandardFeaturePoint();
    }

    /**
     * Find landmarks of the same type (id). Landmarks of ID -1 are skipped.
     *
     * @param col1 First collection. Must not be {@code null}
     * @param col2 Second collection. Must not be {@code null}
     * @return Landmarks of the same type
     */
    static Collection<Landmark> landmarksOfTheSameType(Collection<Landmark> col1, Collection<Landmark> col2) {
        Map<Integer, Landmark> m1 = col1.stream()
                .filter(fp -> fp.getType() >= 0)
                .collect(Collectors.toMap(fp->fp.getType(), fp->fp));
        Map<Integer, Landmark> m2 = col2.stream()
                .filter(fp -> fp.getType() >= 0)
                .collect(Collectors.toMap(fp->fp.getType(), fp->fp));
        m1.keySet().retainAll(m2.keySet());
        return m1.values();
    }
}
