package cz.fidentis.analyst.data.landmarks;

import cz.fidentis.analyst.data.landmarks.impl.ExporterCSV;
import cz.fidentis.analyst.data.landmarks.impl.ExporterFP;
import cz.fidentis.analyst.data.landmarks.impl.LoaderCSV;
import cz.fidentis.analyst.data.landmarks.impl.LoaderFP;

import java.io.IOException;
import java.util.List;

/**
 * IO services for landmarks and feature points.
 *
 * @author Radek Oslejsek
 * @author Jakub Kolman
 */
public interface LandmarksIO {

    /**
     * Load feature points associated with a face from either CSV or FP file.
     *
     * @param path Path to the file
     * @param fileName CSV or FP file with feature points of the face
     * @return List of feature points
     * @throws IOException on error
     */
    static List<Landmark> importFeaturePoints(String path, String fileName) throws IOException {
        String format = fileName.substring(fileName.lastIndexOf(".") + 1);
        return switch (format.toUpperCase()) {
            case ("CSV") -> LoaderCSV.loadFeaturePoints(path, fileName);
            case ("FP") -> LoaderFP.loadFeaturePoints(path, fileName);
            default -> null;
        };
    }

    /**
     * Method calls either @see FeaturePointCsvExporter or @see
     * FeaturePointFpExporter based on the format given as parameter
     *
     * @param featurePointList List of feature points
     * @param path Path to the directory with the face data
     * @param fileName Name of the face
     * @param format Either "CSV" or "FP"
     * @throws IOException on error
     */
    static void exportFeaturePoints(List<Landmark> featurePointList, String path, String fileName, String format) throws IOException {
        switch (format) {
            case "CSV":
                ExporterCSV.exportToCSV(featurePointList, path, fileName);
                break;

            case "FP":
                ExporterFP.exportToFP(featurePointList, path, fileName);
                break;

            default:
                break;
        }
    }
}
