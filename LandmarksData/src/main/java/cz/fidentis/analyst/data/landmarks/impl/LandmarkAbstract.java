package cz.fidentis.analyst.data.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;

import javax.vecmath.Point3d;
import java.util.Objects;

/**
 * Generic class for landmarks and feature points.
 *
 * @author Radek Oslejsek
 */
public abstract class LandmarkAbstract implements Landmark {

    private final int id;

    private Point3d position;

    private MeshVicinity meshVicinity = null;

    /**
     * Constructor.
     *
     * @param id A <b>unique</b> id
     * @param position 3D position
     */
    public LandmarkAbstract(int id, Point3d position) {
        this.id = id;
        this.position = new Point3d(position);
    }

    @Override
    public int getType() {
        return id;
    }

    @Override
    public Point3d getPosition() {
        return position;
    }

    @Override
    public void setPosition(Point3d position) {
        this.position = new Point3d(Objects.requireNonNull(position));
    }

    @Override
    public abstract Landmark clone();

    @Override
    public MeshVicinity getMeshVicinity() {
        return meshVicinity;
    }

    @Override
    public void setMeshVicinity(MeshVicinity meshVicinity) {
        this.meshVicinity = meshVicinity;
    }

    @Override
    public boolean hasMeshVicinity() {
        return meshVicinity != null;
    }

    @Override
    public abstract String getName();

    @Override
    public abstract void setName(String name);

    @Override
    public abstract String getDescription();

    @Override
    public abstract void setDescription(String description);

    @Override
    public abstract String getCode();

    @Override
    public abstract boolean isStandardFeaturePoint();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LandmarkAbstract that)) {
            return false;
        }

        return getType() == that.getType();
    }

    @Override
    public int hashCode() {
        return getType();
    }

    @Override
    public String toString() {
        return "Landmark: id=" + getType() + " name=" + getName() + " position=" + position;
    }
}
