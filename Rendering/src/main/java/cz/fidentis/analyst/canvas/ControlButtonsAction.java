package cz.fidentis.analyst.canvas;

import cz.fidentis.analyst.rendering.AnimationDirection;
import cz.fidentis.analyst.rendering.RotationAnimator;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action listener for {@link ControlButtons}.
 * 
 * @author Radek Oslejsek
 */
public class ControlButtonsAction extends AbstractAction {
    
    public static final String ACTION_COMMAND_LEFT_PRESSED = "rotate left pressed";
    public static final String ACTION_COMMAND_LEFT_RELEASED = "rotate left released";
    public static final String ACTION_COMMAND_RIGHT_PRESSED = "rotate right pressed";
    public static final String ACTION_COMMAND_RIGHT_RELEASED = "rotate right released";
    public static final String ACTION_COMMAND_UP_PRESSED = "rotate up pressed";
    public static final String ACTION_COMMAND_UP_RELEASED = "rotate up released";
    public static final String ACTION_COMMAND_DOWN_PRESSED = "rotate down pressed";
    public static final String ACTION_COMMAND_DOWN_RELEASED = "rotate down released";
    public static final String ACTION_COMMAND_PLUS_PRESSED = "zoom in pressed";
    public static final String ACTION_COMMAND_PLUS_RELEASED = "zoom in released";
    public static final String ACTION_COMMAND_MINUS_PRESSED = "zoom out pressed";
    public static final String ACTION_COMMAND_MINUS_RELEASED = "zoom out released";
    public static final String ACTION_COMMAND_RESET = "reset";
    public static final String ACTION_COMMAND_LEFT_DOUBLECLICK = "rotate left doubleclick";
    public static final String ACTION_COMMAND_RIGHT_DOUBLECLICK = "rotate right doubleclick";
    public static final String ACTION_COMMAND_UP_DOUBLECLICK = "rotate up doubleclick";
    public static final String ACTION_COMMAND_DOWN_DOUBLECLICK = "rotate down doubleclick";
    
    private final transient RotationAnimator animator;
    private final Canvas canvas;

    /**
     * Constructor. 
     * 
     * @param canvas OpenGL canvas
     * @throws IllegalArgumentException if some param is missing
     */
    public ControlButtonsAction(Canvas canvas) {
        if (canvas == null) {
            throw new IllegalArgumentException("canvas");
        }
        this.canvas = canvas;
        this.animator = new RotationAnimator(canvas.getGLCanvas());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        
        switch (command) {
            case ACTION_COMMAND_LEFT_PRESSED:
                animator.startAnimation(AnimationDirection.ROTATE_LEFT, canvas.getCamera());
                break;
            case ACTION_COMMAND_RIGHT_PRESSED:
                animator.startAnimation(AnimationDirection.ROTATE_RIGHT, canvas.getCamera());
                break;
            case ACTION_COMMAND_UP_PRESSED:
                animator.startAnimation(AnimationDirection.ROTATE_UP, canvas.getCamera());
                break;
            case ACTION_COMMAND_DOWN_PRESSED:
                animator.startAnimation(AnimationDirection.ROTATE_DOWN, canvas.getCamera());
                break;
            case ACTION_COMMAND_RESET:
                canvas.getCamera().zoomToFit(canvas.getScene());
                canvas.renderScene();
                break;
            case ACTION_COMMAND_PLUS_PRESSED:
                animator.startAnimation(AnimationDirection.ZOOM_IN, canvas.getCamera());
                break;
            case ACTION_COMMAND_MINUS_PRESSED:
                animator.startAnimation(AnimationDirection.ZOOM_OUT, canvas.getCamera());
                break;
            case ACTION_COMMAND_LEFT_RELEASED,
                    ACTION_COMMAND_RIGHT_RELEASED,
                    ACTION_COMMAND_UP_RELEASED,
                    ACTION_COMMAND_DOWN_RELEASED,
                    ACTION_COMMAND_PLUS_RELEASED,
                    ACTION_COMMAND_MINUS_RELEASED:
                animator.stopAnimation();
                break;
             case ACTION_COMMAND_LEFT_DOUBLECLICK:
                canvas.getCamera().zoomToFit(canvas.getScene());
                canvas.getCamera().rotate(0, 90);
                canvas.renderScene();
                break;
             case ACTION_COMMAND_RIGHT_DOUBLECLICK:
                canvas.getCamera().zoomToFit(canvas.getScene());
                canvas.getCamera().rotate(0, -90);
                canvas.renderScene();
                break;
             case ACTION_COMMAND_UP_DOUBLECLICK:
                canvas.getCamera().zoomToFit(canvas.getScene());
                canvas.getCamera().rotate(-90, 0);
                canvas.renderScene();
                break;
             case ACTION_COMMAND_DOWN_DOUBLECLICK:
                canvas.getCamera().zoomToFit(canvas.getScene());
                canvas.getCamera().rotate(90, 0);
                canvas.renderScene();
                break;
            default:
                // do nothing
        }
    }
    
}
