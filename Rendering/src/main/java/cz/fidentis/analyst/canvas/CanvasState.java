package cz.fidentis.analyst.canvas;

import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.rendering.Camera;
import cz.fidentis.analyst.rendering.RenderingMode;

import java.awt.*;

/**
 * A copy of the internal state of the {@link Canvas} instance.
 *
 * @author Jan Popelas
 */
public class CanvasState {

    private final Camera camera;
    private final Color backgroundColor;
    private final float primaryFaceTransparency;
    private final float secondaryFaceTransparency;
    private final RenderingMode primaryFaceRenderingMode;
    private final RenderingMode secondaryFaceRenderingMode;
    private final boolean primaryFaceVisible;
    private final boolean secondaryFaceVisible;

    /**
     * Constructor from Canvas
     *
     * @param canvas the Canvas object for which we want to construct the state
     */
    public CanvasState(Canvas canvas) {
        DrawableFace priFace = canvas.getScene().getDrawableFace(canvas.getScene().getPrimaryFaceSlot());
        DrawableFace secFace = canvas.getScene().getDrawableFace(canvas.getScene().getSecondaryFaceSlot());

        this.camera = canvas.getCamera().copy();
        this.backgroundColor = canvas.getGLCanvas().getBackground();

        this.primaryFaceTransparency = (priFace != null) ? priFace.getTransparency() : 0.0f;
        this.primaryFaceRenderingMode = (priFace != null) ? priFace.getRenderMode() : null;
        this.primaryFaceVisible = (priFace != null) ? priFace.isShown() : false;

        this.secondaryFaceTransparency = (secFace != null) ? secFace.getTransparency() : 0.0f;
        this.secondaryFaceRenderingMode = (secFace != null) ? secFace.getRenderMode() : null;
        this.secondaryFaceVisible = (secFace != null) ? secFace.isShown() : false;
    }

    public Camera getCamera() {
        return camera;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public float getPrimaryFaceTransparency() {
        return primaryFaceTransparency;
    }

    public float getSecondaryFaceTransparency() {
        return secondaryFaceTransparency;
    }

    public RenderingMode getPrimaryFaceRenderingMode() {
        return primaryFaceRenderingMode;
    }

    public RenderingMode getSecondaryFaceRenderingMode() {
        return secondaryFaceRenderingMode;
    }

    public boolean isPrimaryFaceVisible() {
        return primaryFaceVisible;
    }

    public boolean isSecondaryFaceVisible() {
        return secondaryFaceVisible;
    }
}
