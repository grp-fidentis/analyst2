package cz.fidentis.analyst.canvas;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

/**
 * Listener invoked when the OpenGL canvas is initiated, shown, or resized.
 * 
 * @author Radek Oslejsek
 * @author Jan Popelas
 */
public class CanvasListener implements GLEventListener {
    
    private Canvas canvas;
    private boolean isRecording = false;
    private BufferedImage capturedImage = null;
    
    /**
     * Constructor.
     * 
     * @param canvas Canvas
     * @throws IllegalArgumentException if the canvas is {@code null}
     */
    public CanvasListener(Canvas canvas) {
        if (canvas == null) {
            throw new IllegalArgumentException("canvas is null");
        }
        this.canvas = canvas;
    }
    
    @Override
    public void init(GLAutoDrawable glad) {
        //System.out.println("CanvasListener.init");
        canvas.getSceneRenderer().initGLContext(glad.getContext());
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        //System.out.println("CanvasListener.dispose");
    }

    @Override
    public void display(GLAutoDrawable glad) {
        if (canvas.getScene() != null) { // non-empty scene
            canvas.getSceneRenderer().renderScene(canvas.getCamera(), canvas.getScene().getAllDrawables());
        }
        if (isRecording) {
            storeImage();
        }
        //OutputWindow.print("3D canvas has been rendered, window size " 
        //        + canvas.getWidth() + "x" + canvas.getHeight()
        //        +", GL canvas size " + canvas.getGLCanvas().getWidth() + "x" + canvas.getGLCanvas().getHeight()
        //);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
        canvas.getSceneRenderer().setViewport(x, y, width, height);
    }

    public void setRecording(boolean recording) {
        isRecording = recording;
    }

    public BufferedImage getCapturedImage() {
        return capturedImage;
    }

    private void storeImage() {
        GL2 gl = canvas.getGLCanvas().getGL().getGL2();
        int width = canvas.getGLCanvas().getSurfaceWidth();
        int height = canvas.getGLCanvas().getSurfaceHeight();

        ByteBuffer buffer = ByteBuffer.allocateDirect(width * height * 3);

        // Read pixels from the back buffer
        gl.glReadBuffer(GL.GL_FRONT_AND_BACK);
        gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1);
        gl.glReadPixels(0, 0, width, height, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, buffer);

        // Convert ByteBuffer to BufferedImage
        capturedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int i = (x + (width * y)) * 3;
                int r = buffer.get(i) & 0xFF;
                int g = buffer.get(i + 1) & 0xFF;
                int b = buffer.get(i + 2) & 0xFF;
                capturedImage.setRGB(x, height - y - 1, (r << 16) | (g << 8) | b);
            }
        }
    }
}
