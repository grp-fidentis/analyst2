package cz.fidentis.analyst.canvas;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.data.face.events.HumanFaceSelectedEvent;
import org.openide.util.Pair;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * A mouse adapter that handles clicking in canvas and then casts the ray into
 * the scene. Moreover, the listener can be switched on/off.
 * 
 * @author Mario Chromik
 */
public class MouseClickListener extends MouseAdapter {
    private final Canvas canvas;
    private boolean active = true;
    
    /**
     * constructor requiring one parameter
     * @param canvas OpenGL canvas 
     */
    public MouseClickListener(Canvas canvas) {
        if (canvas == null) {
            throw new IllegalArgumentException("canvas is null");
        }
        this.canvas = canvas;
    }
    
    /**
     * toggles activity of object
     * @return current status
     */
    public boolean toggleActivity() {
        active = !active;
        return active;
    }
         
    /**
     * when mouse is clicked calculate a ray into the scene
     * @param evt mouse event
     */
    @Override
    public void mouseClicked(MouseEvent evt) {
        if (!SwingUtilities.isLeftMouseButton(evt) || !active){
            return;
        }

        Pair<HumanFace, RayIntersection> closestFace = canvas.castRayThroughPixel(evt.getX(), evt.getY());

        if (closestFace != null) {
            HumanFace face = closestFace.first();
            face.announceEvent(new HumanFaceSelectedEvent(
                    face, 
                    closestFace.second(), // distance
                    "Intersect with secondary face", 
                    this
            ));
        }                
    }
    
}
