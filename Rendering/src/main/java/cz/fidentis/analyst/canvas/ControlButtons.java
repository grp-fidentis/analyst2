package cz.fidentis.analyst.canvas;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * The control panel inside the canvas.
 * 
 * @author Radek Oslejsek
 */
public class ControlButtons extends JPanel {
    
    public static final Dimension SIZE = new Dimension(90,120);
    
    /**
     * Constructor.
     * 
     * @param actionListener Action listener
     */
    public ControlButtons(ActionListener actionListener) {
        setLayout(new GridBagLayout());
        setPreferredSize(SIZE);
        setMaximumSize(SIZE);
        setMinimumSize(SIZE);
        
        setListner(
                addButton(1, 0, "upButton.png"), 
                actionListener, 
                ControlButtonsAction.ACTION_COMMAND_UP_PRESSED, 
                ControlButtonsAction.ACTION_COMMAND_UP_RELEASED,
                ControlButtonsAction.ACTION_COMMAND_UP_DOUBLECLICK
        );
        
        setListner(
                addButton(0, 1, "leftButton.png"), 
                actionListener, 
                ControlButtonsAction.ACTION_COMMAND_LEFT_PRESSED, 
                ControlButtonsAction.ACTION_COMMAND_LEFT_RELEASED,
                ControlButtonsAction.ACTION_COMMAND_LEFT_DOUBLECLICK
        );
        
        addButton(1, 1, "resetButton.png").addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                actionListener.actionPerformed(new ActionEvent(
                        evt.getSource(), 
                        ActionEvent.ACTION_PERFORMED, 
                        ControlButtonsAction.ACTION_COMMAND_RESET)
                );
            }
        });

        setListner(addButton(2, 1, "rightButton.png"), 
                actionListener, 
                ControlButtonsAction.ACTION_COMMAND_RIGHT_PRESSED, 
                ControlButtonsAction.ACTION_COMMAND_RIGHT_RELEASED,
                ControlButtonsAction.ACTION_COMMAND_RIGHT_DOUBLECLICK
        );
        
        setListner(
                addButton(1, 2, "downButton.png"), 
                actionListener, 
                ControlButtonsAction.ACTION_COMMAND_DOWN_PRESSED, 
                ControlButtonsAction.ACTION_COMMAND_DOWN_RELEASED,
                ControlButtonsAction.ACTION_COMMAND_DOWN_DOUBLECLICK
        );
        
        setListner(
                addButton(0, 3, "plus.png"), 
                actionListener, 
                ControlButtonsAction.ACTION_COMMAND_PLUS_PRESSED, 
                ControlButtonsAction.ACTION_COMMAND_PLUS_RELEASED,
                null
        );

        setListner(
                addButton(2, 3, "minus.png"), 
                actionListener, 
                ControlButtonsAction.ACTION_COMMAND_MINUS_PRESSED, 
                ControlButtonsAction.ACTION_COMMAND_MINUS_RELEASED,
                null
        );
        
        addBackgroundCross();
    }
    
    private void setListner(
            JButton button, 
            ActionListener actionListener, 
            String commandPressed, 
            String commandReleased, 
            String commandDoubleclick) {
        
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2 && !evt.isConsumed() && commandDoubleclick != null) {
                    actionListener.actionPerformed(new ActionEvent(
                            evt.getSource(), 
                            ActionEvent.ACTION_PERFORMED, 
                            commandDoubleclick)
                    );
                }
            }
            
            @Override
            public void mousePressed(MouseEvent evt) {
                actionListener.actionPerformed(new ActionEvent(
                        evt.getSource(), 
                        ActionEvent.ACTION_PERFORMED, 
                        commandPressed)
                );
            }
            @Override
            public void mouseReleased(MouseEvent evt) {
                actionListener.actionPerformed(new ActionEvent(
                        evt.getSource(), 
                        ActionEvent.ACTION_PERFORMED, 
                        commandReleased)
                );
            }                
        });

    }
    
    private void addBackgroundCross() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = 3;
        c.gridheight = 3;
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.NONE;
        
        JLabel label = new JLabel();
        label.setIcon(new ImageIcon(getClass().getResource("/navigBackground.png")));
        label.setBorder(BorderFactory.createEmptyBorder());
        add(label, c);
    }

    private JButton addButton(int x, int y, String icon) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = x;
        c.gridy = y;
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(0,0,0,0);
        
        JButton button = new JButton();
        button.setIcon(new ImageIcon(getClass().getResource("/" + icon)));
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
        
        add(button, c);
        return button;
    }
}
