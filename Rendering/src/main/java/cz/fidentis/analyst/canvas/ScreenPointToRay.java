package cz.fidentis.analyst.canvas;

import cz.fidentis.analyst.rendering.Camera;
import cz.fidentis.analyst.data.ray.Ray;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Utility class for converting screen position to ray
 * 
 * @author Mario Chromik
 *
 * @deprecated The implementation is not correct and was replaced by
 * the {@link cz.fidentis.analyst.rendering.SceneRenderer#castRayThroughPixel(int, int, Camera)}
 * method, which is mased on the {@code gluUnProject} reverse mapping of the projection and modelview
 * OpenGL matrices.
 */
@Deprecated(forRemoval = true)
public class ScreenPointToRay {

    /**
     * converts screen point to ray by first calculating position on near plane,
     * constructing a coordinate frame and calculates direction from camera 
     * position to point on near plane within this frame
     * @param screenX screen x coordinate
     * @param screenY screen y coordinate
     * @param width view port width
     * @param height view port height
     * @param camera camera
     * @return ray
     */
    public static Ray convert(int screenX, int screenY, int width, int height, Camera camera) {
        Point3d nearPlanePos = calculatePositionOnNearPlane(screenX, screenY, width, height);
        
        double fovY = Camera.FIELD_OF_VIEW * Math.PI / 180.0;
        double fovX = fovY * width/height;
        
        Vector3d camDir = calculateCameraDirection(camera);
        Vector3d camRight = calculateOrthogonalCameraRight(camera.getUpDirection(), camDir);
        Vector3d camUp = calculateOrthogonalCameraUp(camDir, camRight);
        
        double alpha = nearPlanePos.x * Math.tan(fovX / 2) * nearPlanePos.z;
        double beta = nearPlanePos.y * Math.tan(fovY / 2) * nearPlanePos.z;

        Vector3d upScaled = new Vector3d(beta * camUp.x, beta * camUp.y, beta * camUp.z);
        Vector3d rightScaled = new Vector3d(alpha * camRight.x, alpha * camRight.y, alpha * camRight.z);
        
        camDir.negate();
        upScaled.add(rightScaled);
        upScaled.add(camDir);
        
        Vector3d dir = new Vector3d();
        dir.normalize(upScaled);
        
        Point3d origin = new Point3d(camera.getPosition());
        
        return new Ray(origin, dir);
    }

    /**
     * Converts pixel coordinates to Normalized Device Coordinates
     *
     * @param screenX screen x coordinate
     * @param screenY screen y coordinate
     * @param width width of view port
     * @param height height of view port
     * @return Normalized Device Coordinates of the pixel's location in the range from -1 to 1 for both X and Y axes.
     */
    private static Point3d calculatePositionOnNearPlane(int screenX, int screenY, int width, int height) {
        double x = (2.0 * screenX) / width - 1.0;
        double y = 1.0 - (2.0 * screenY) / height;
        double z = 1.0;
        
        return new Point3d(x, y, z);
    }
    
    /**
     * calculates camera direction 
     * direction from camera position to center of scene
     * @param camera camera
     * @return camera direction
     */
    private static Vector3d calculateCameraDirection(Camera camera) {
        Vector3d camPos = camera.getPosition();
        Vector3d center = camera.getCenter();
        center.negate();
        camPos.add(center);
        Vector3d camDir = new Vector3d();
        camDir.normalize(camPos);
        camDir.scale(1);
        
        return camDir;
    }
    
    /**
     * calculates orthogonal camera right direction 
     * @param up camera's up direction
     * @param camDir direction to the center of the scene
     * @return camera right
     */
    private static Vector3d calculateOrthogonalCameraRight(Vector3d up, Vector3d camDir) {
        Vector3d right = new Vector3d();
        right.cross(up, camDir);
        right.normalize();
        
        return right;
    }
    
    /**
     * calculates orthogonal camera up direction
     * @param camDir
     * @param camRight
     * @return 
     */
    private static Vector3d calculateOrthogonalCameraUp(Vector3d camDir, Vector3d camRight) {
        Vector3d up = new Vector3d();
        up.cross(camDir, camRight);
        up.normalize();
        
        return up;
    }
}
