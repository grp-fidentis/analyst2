package cz.fidentis.analyst.canvas;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.drawables.Drawable;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.data.face.events.HumanFaceSelectedEvent;
import cz.fidentis.analyst.data.face.events.HumanFaceTransformedEvent;
import cz.fidentis.analyst.data.face.events.SymmetryPlaneChangedEvent;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionServices;
import cz.fidentis.analyst.rendering.Camera;
import cz.fidentis.analyst.rendering.Scene;
import cz.fidentis.analyst.rendering.SceneRenderer;
import org.openide.util.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * OpenGL canvas.
 * 
 * @author Natalia Bebjakova 
 * @author Radek Oslejsek
 * @author Jan Popelas
 */
public class Canvas extends JPanel implements HumanFaceListener {
    
    // Scene components:
    private final transient Scene scene = new Scene();
    private final transient SceneRenderer sceneRenderer;
    private Camera camera = new Camera();
    
    // Listeners:
    private final transient CanvasListener listener;
    private final transient MouseRotationListener manipulator;
    private final transient MouseClickListener clicker;
    
    // GUI elements:
    private JLayeredPane layeredPane;
    private JPanel canvasPanel;
    private ControlButtons controlButtonsPanel;
    private GLCanvas glCanvas;
    private JToolBar toolbar;

    /**
     * Constructor.
     */
    public Canvas() {
        sceneRenderer = new SceneRenderer();
        listener = new CanvasListener(this);
        manipulator = new MouseRotationListener(this);
        clicker = new MouseClickListener(this);
        initComponents();
    }
    
    /**
     * Returns the scene
     * @return the scene
     */
    public SceneRenderer getSceneRenderer() {
        return sceneRenderer;
    }
    
    /**
     * Renders the scene.
     */
    public void renderScene() {
        glCanvas.display();
    }

    public JPanel getCanvasPanel() {
        return canvasPanel;
    }

    public Scene getScene() {
        return scene;
    }

    /**
     * Creates the set of the canvas.
     * Primarily stores the camera, background color, transparency and rendering mode of the faces.
     *
     * @return the state of the canvas
     */
    public CanvasState getState() {
        return new CanvasState(this);
    }

    /**
     * Sets the state of the canvas.
     * Primarily adjusts the camera, background color, transparency and rendering mode of the faces.
     *
     * @param state the state to set
     */
    public void setState(CanvasState state) {
        DrawableFace priFace = scene.getDrawableFace(scene.getPrimaryFaceSlot());
        DrawableFace secFace = scene.getDrawableFace(scene.getSecondaryFaceSlot());

        camera = state.getCamera();
        setDarkBackground(state.getBackgroundColor() == SceneRenderer.DARK_BACKGROUND);

        if (priFace != null) {
            priFace.setTransparency(state.getPrimaryFaceTransparency());
            priFace.setRenderMode(state.getPrimaryFaceRenderingMode());
            priFace.show(state.isPrimaryFaceVisible());
        }

        if (secFace != null) {
            secFace.setTransparency(state.getSecondaryFaceTransparency());
            secFace.setRenderMode(state.getSecondaryFaceRenderingMode());
            secFace.show(state.isSecondaryFaceVisible());
        }
    }
    
    /**
     * Returns camera. 
     * 
     * @return camera
     */
    public Camera getCamera() {
        return camera;
    }
    
    public void setCamera(Camera camera) {
        this.camera = camera;
    }
    
    /**
     * Returns the underlying OpenGL canvas.
     * 
     * @return the underlying OpenGL canvas.
     */
    public GLCanvas getGLCanvas() {
        return this.glCanvas;
    }

    public CanvasListener getListener() {
        return listener;
    }

    /**
     * Sets background color
     * 
     * @param dark If {@code true}, then dark background is set
     */
    public void setDarkBackground(boolean dark) {
        if (dark) {
            canvasPanel.setBackground(SceneRenderer.DARK_BACKGROUND);
            sceneRenderer.setDarkBackground();
        } else {
            canvasPanel.setBackground(SceneRenderer.BRIGHT_BACKGROUND);
            sceneRenderer.setBrightBackground();
        }
    }
    
    /**
     * Adds a toolbox to the scene toolbar.
     * @param toolbox New toolbox
     */
    public void addToolBox(JPanel toolbox) {
        if (toolbox != null) {
            toolbar.add(toolbox);
        }
    }
    
    @Override
    public void acceptEvent(HumanFaceEvent event) {
        // The symmetry plane is replaced with new instance during the transformation
        // of a human face. Therefore, the drawable plane has to be updated.
        // It is not necessary for meshes and feature points because their vertices
        // are directly modified and then do not affect their drawable wrappers
        if (event instanceof HumanFaceTransformedEvent || event instanceof SymmetryPlaneChangedEvent) { 
            final HumanFace face = event.getFace();
            final List<DrawableFace> drFaces = getScene().getDrawableFaces();
            for (int i = 0; i < drFaces.size(); i++) {
                if (drFaces.get(i).getHumanFace().equals(face)) { // recomputes and replaces i-th symmetry plane
                    getScene().setDrawableSymmetryPlane(i, face);
                    break;
                }
            }
        }
        
        // A face was selected by the user:
        if (event instanceof HumanFaceSelectedEvent) {
            // write code here
            //HumanFaceSelectedEvent huEvent = (HumanFaceSelectedEvent) event;
            //Logger.print(huEvent.getFace().getShortName() 
            //        + " hit on position " + huEvent.getIntersection().getPosition()
            //        + " in distance " + huEvent.getIntersection().getDistance());
        }
    }
    


    private ControlButtons getButtonsPanel(Canvas canvas) {
        ControlButtonsAction controlButtonsListener = new ControlButtonsAction(canvas);
        ControlButtons ret = new ControlButtons(controlButtonsListener);
        ret.setOpaque(false);
        ret.setBounds(20, 20, ControlButtons.SIZE.width, ControlButtons.SIZE.height);
        return ret;
    }
    
    private void initComponents() {
        setLayout(new BorderLayout());

        initGLCanvas();
        canvasPanel = new JPanel();
        canvasPanel.setLayout(new BorderLayout());
        canvasPanel.add(glCanvas);
        
        controlButtonsPanel = getButtonsPanel(this);
        
        layeredPane = new JLayeredPane();
        layeredPane.add(canvasPanel, 1);
        layeredPane.add(controlButtonsPanel, 0);
        add(layeredPane, BorderLayout.CENTER);
        
        setDarkBackground(false);
        
        toolbar = new JToolBar();
        toolbar.setFloatable(false);
        add(toolbar, BorderLayout.SOUTH);
        
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                canvasPanel.setBounds(0, 0, layeredPane.getWidth(), layeredPane.getHeight());
                glCanvas.setBounds(layeredPane.getX(), layeredPane.getY(), layeredPane.getWidth(), layeredPane.getHeight());
            }
        });
    }

    private void initGLCanvas() {
        // gl version 2 is used
        GLCapabilities capabilities = new GLCapabilities(GLProfile.get(GLProfile.GL2));
        capabilities.setDoubleBuffered(true);
       
        // creates new glCanvas panel for displaying model
        glCanvas = new GLCanvas(capabilities);
        glCanvas.setVisible(true);     
        
        // enables glCanvas to react to events
        glCanvas.requestFocusInWindow();        
        //glCanvas.setSize(getWidth() - getInsets().left - getInsets().right, getHeight() - getInsets().top - getInsets().bottom);

        glCanvas.addGLEventListener(listener);
        glCanvas.addMouseListener(manipulator);
        glCanvas.addMouseListener(clicker);
        glCanvas.addMouseMotionListener(manipulator);
        glCanvas.addMouseWheelListener(manipulator);        
    }
    
    /**
     * Captures the canvas and returns it as a buffered image.
     * @return the captured canvas
     */
    public BufferedImage captureCanvas() {
        listener.setRecording(true);
        renderScene();
        BufferedImage image = listener.getCapturedImage();
        listener.setRecording(false);
        return image;
    }

    /**
     * Cast a ray through a pixel and find the intersection with <strong>visible faces</strong> only
     * (hidden faces and other graphical objects are omitted).
     *
     * @param pixelX The X coordinate of mouse pointer
     * @param pixelY The X coordinate of mouse pointer
     * @return The intersection with the closest visible face
     */
    public Pair<HumanFace, RayIntersection> castRayThroughPixel(int pixelX, int pixelY) {
       Ray ray = getSceneRenderer().castRayThroughPixel(pixelX, pixelY, getCamera());
       return getScene().getDrawableFaces().stream()
               .filter(Drawable::isShown)
               .map(DrawableFace::getHumanFace)
               .map(face -> getRayIntersection(face, ray))
               .filter(Objects::nonNull)
               .min(Comparator.comparingDouble(o -> o.second().getDistance()))
               .orElse(null);
    }

    /**
     * Calculates an octree, casts a ray and if intersecting returns said intersection
     * @param face face to check for intersection
     * @param ray ray to project
     * @return pair of face, intersection if intersect else null
     */
    private Pair<HumanFace, RayIntersection> getRayIntersection(HumanFace face, Ray ray) {
        FaceStateServices.updateOctree(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        RayIntersection ri = RayIntersectionServices.computeClosest(
                face.getOctree(),
                new RayIntersectionConfig(ray, MeshTriangle.Smoothing.NORMAL, false));
        return (ri == null) ? null : Pair.of(face, ri);
    }
}
