package cz.fidentis.analyst.toolbar;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.rendering.ShadersManager;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Canvas toolbox for the selection fog version.
 *
 * @author Ondrej Simecek
 */
public class FogToolbox extends Toolbox {

    private static final String OFF_BUTTON_ICON = "fog0_28x28.png";
    private static final String OUTER_BUTTON_ICON = "fog1_28x28.png";
    private static final String TRANSPARENCY_BUTTON_ICON = "fog2_28x28.png";
    private static final String INNER_BUTTON_ICON = "fog3_28x28.png";

    /**
     * Constructor.
     * @param canvas Rendering canvas
     */
    public FogToolbox(Canvas canvas) {
        super(canvas);
        initComponents();
    }
    
    private void initComponents() {
        JMenuItem menuItem1 = new JMenuItem(new AbstractAction() { // fill
            @Override
            public void actionPerformed(ActionEvent e) {
                getCanvas().getSceneRenderer().getShadersManager().setFogVersion(0);
                getCanvas().renderScene();
            }
        });

        JMenuItem menuItem2 = new JMenuItem(new AbstractAction() { // lines
            @Override
            public void actionPerformed(ActionEvent e) {
                getCanvas().getSceneRenderer().getShadersManager().setFogVersion(1);
                getCanvas().renderScene();
            }
        });

        JMenuItem menuItem3 = new JMenuItem(new AbstractAction() { // points
            @Override
            public void actionPerformed(ActionEvent e) {
                getCanvas().getSceneRenderer().getShadersManager().setFogVersion(2);
                getCanvas().renderScene();
            }
        });

        JMenuItem menuItem4 = new JMenuItem(new AbstractAction() { // texture
            @Override
            public void actionPerformed(ActionEvent e) {
                ShadersManager shaders = getCanvas().getSceneRenderer().getShadersManager();
                if (shaders != null) {
                    shaders.setFogVersion(3);
                    getCanvas().renderScene();
                }
            }
        });
        
        menuItem1.setIcon(new ImageIcon(getClass().getResource("/" + FogToolbox.OFF_BUTTON_ICON)));
        menuItem2.setIcon(new ImageIcon(getClass().getResource("/" + FogToolbox.OUTER_BUTTON_ICON)));
        menuItem3.setIcon(new ImageIcon(getClass().getResource("/" + FogToolbox.TRANSPARENCY_BUTTON_ICON)));
        menuItem4.setIcon(new ImageIcon(getClass().getResource("/" + FogToolbox.INNER_BUTTON_ICON)));

        menuItem1.setToolTipText(NbBundle.getMessage(FogToolbox.class, "FogToolbox.none.text"));
        menuItem2.setToolTipText(NbBundle.getMessage(FogToolbox.class, "FogToolbox.outerMapping.text"));
        menuItem3.setToolTipText(NbBundle.getMessage(FogToolbox.class, "FogToolbox.transparencyMapping.text"));
        menuItem4.setToolTipText(NbBundle.getMessage(FogToolbox.class, "FogToolbox.innerMapping.text"));

        add(menuItem1);
        add(menuItem2);
        add(menuItem3);
        add(menuItem4);
        
        setLayout(new GridLayout(1,0));
    }
}
