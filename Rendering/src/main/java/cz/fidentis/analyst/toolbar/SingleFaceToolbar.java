package cz.fidentis.analyst.toolbar;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;

/**
 * Canvas toolbar for scenes with a single faces.
 * 
 * @author Richard Pajersky
 * @author Radek Oslejsek
 */
public class SingleFaceToolbar extends Toolbar {

    /**
     * Constructor.
     * @param canvas Rendering canvas
     * @param faces Faces processed by current analytical task
     */
    public SingleFaceToolbar(Canvas canvas, Task task) {
        super(canvas);

        addLabel("Rendering:");
        addToolbarMenu(new RenderingToolbox(canvas), RENDERING_MODE_TOOLBOX_ICON, "Toolbar.renderingMode.tooltip");
        add(initBackgroundButton());
        add(initReflectionsButton());
        addLabel("Screenshot:");
        addScreenshotButton();
        addLabel("Subject:");
        addToolbarMenu(
                new FaceToolbox(canvas, canvas.getScene().getPrimaryFaceSlot(), FaceToolbox.ToolboxType.SINGLE_FACE),
                FaceToolbox.ToolboxType.SINGLE_FACE.getIcon(),
                "Toolbar.faceButton.tooltip"
        );

        // be informed if something changes in the faces -- see acceptEvent()
        FaceReference faceReference = TaskService.INSTANCE.getPrimaryFace(task);
        FaceService.INSTANCE.getFaceByReference(faceReference).registerListener(this);
    }
    
    @Override
    public void acceptEvent(HumanFaceEvent event) {
    }
}
