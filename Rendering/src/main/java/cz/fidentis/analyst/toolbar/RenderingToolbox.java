package cz.fidentis.analyst.toolbar;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.rendering.RenderingMode;
import org.openide.util.NbBundle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Canvas popup toolbox for the selection of rendering mode.
 * 
 * @author Radek Oslejsek
 * @author Katerina Zarska
 */
public class RenderingToolbox extends Toolbox {
    
    private static final String TEXTURE_BUTTON_ICON = "texture-tri28x28.png";
    private static final String SMOOTH_BUTTON_ICON = "smooth-tri28x28.png";
    private static final String WIREFRAME_BUTTON_ICON = "wireframe-tri28x28.png";
    private static final String POINTS_BUTTON_ICON = "points-tri28x28.png";
    
    /**
     * Constructor.
     * @param canvas Rendering canvas
     */
    public RenderingToolbox(Canvas canvas) {
        super(canvas);
        initComponents();
    }
    
    private void initComponents() {
        JMenuItem menuItem1 = new JMenuItem(new AbstractAction() { // fill
            @Override
            public void actionPerformed(ActionEvent e) {
                getScene().getDrawableFaces().forEach(f -> f.setRenderMode(RenderingMode.SMOOTH));
                getCanvas().renderScene();
            }
        });
        
        JMenuItem menuItem2 = new JMenuItem(new AbstractAction() { // lines
            @Override
            public void actionPerformed(ActionEvent e) {
                getScene().getDrawableFaces().forEach(f -> f.setRenderMode(RenderingMode.WIREFRAME));
                getCanvas().renderScene();
            }
        });
        
        JMenuItem menuItem3 = new JMenuItem(new AbstractAction() { // points
            @Override
            public void actionPerformed(ActionEvent e) {
                getScene().getDrawableFaces().forEach(f -> f.setRenderMode(RenderingMode.POINT_CLOUD));
                getCanvas().renderScene();
            }
        });
        
        JMenuItem menuItem4 = new JMenuItem(new AbstractAction() { // texture
            @Override
            public void actionPerformed(ActionEvent e) {
                getScene().getDrawableFaces().forEach(f -> f.setRenderMode(RenderingMode.TEXTURE));
                getCanvas().renderScene();
            }
        });
        
        menuItem1.setIcon(new ImageIcon(getClass().getResource("/" + RenderingToolbox.SMOOTH_BUTTON_ICON)));
        menuItem2.setIcon(new ImageIcon(getClass().getResource("/" + RenderingToolbox.WIREFRAME_BUTTON_ICON)));
        menuItem3.setIcon(new ImageIcon(getClass().getResource("/" + RenderingToolbox.POINTS_BUTTON_ICON)));
        menuItem4.setIcon(new ImageIcon(getClass().getResource("/" + RenderingToolbox.TEXTURE_BUTTON_ICON)));

        menuItem1.setToolTipText(NbBundle.getMessage(RenderingToolbox.class, "RenderingModeToolbox.smooth.text"));
        menuItem2.setToolTipText(NbBundle.getMessage(RenderingToolbox.class, "RenderingModeToolbox.wireframe.text"));
        menuItem3.setToolTipText(NbBundle.getMessage(RenderingToolbox.class, "RenderingModeToolbox.pointcloud.text"));
        menuItem4.setToolTipText(NbBundle.getMessage(RenderingToolbox.class, "RenderingModeToolbox.texture.text"));
        
        add(menuItem4);
        add(menuItem1);
        add(menuItem2);
        add(menuItem3);
        
        setLayout(new GridLayout(1,0));
    }
}
