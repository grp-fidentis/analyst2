package cz.fidentis.analyst.toolbar;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.canvas.CanvasListener;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.drawables.Drawable;
import cz.fidentis.analyst.rendering.Scene;
import cz.fidentis.analyst.rendering.SceneRenderer;
import org.openide.awt.DropDownButtonFactory;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * A common class for canvas toolbars located under the 3D scene canvas.
 *
 * @author Radek Oslejsek
 */
public abstract class Toolbar extends JPanel implements HumanFaceListener {

    public static final String SCREENSHOT_BUTTON_ICON = "screenshot28x28.png";
    public static final String RENDERING_MODE_TOOLBOX_ICON = "wireframe28x28.png";
    public static final String FOG_VERSION_TOOLBOX_ICON = "fog_toolbox28x28.png";
    public static final String CONTOURS_ICON = "intersection28x28.png";
    public static final String GLYPH_ICON = "glyph28x28.png";
    public static final String BACKGROUND_BUTTON_ICON = "background28x28.png";
    public static final String REFLECTIONS_BUTTON_ICON = "lightbulb28x28.png";
    public static final Color  REFLECTIONS_COLOR = Color.DARK_GRAY;

    public static final Border BUTTONS_BORDER = new EmptyBorder(5,5,5,5);

    private final Canvas canvas;

    /**
     * Constructor.
     * @param canvas Rendering canvas
     */
    public Toolbar(Canvas canvas) {
        this.canvas = Objects.requireNonNull(canvas);
    }

    protected Canvas getCanvas() {
        return canvas;
    }

    protected void renderScene() {
        canvas.renderScene();
    }

    protected Scene getScene() {
        return canvas.getScene();
    }

    protected final void addLabel(String label) {
        add(new JLabel(label));
    }

    protected final void addToolbarMenu(JPopupMenu menu, String icon, String tooltip) {
        // The button that will display the default action and the dropdown arrow
        JButton button = DropDownButtonFactory.createDropDownButton(
                new ImageIcon(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB)),
                menu);

        button.setBorder(new EmptyBorder(5,5,5,5));
        button.setIcon(new ImageIcon(getClass().getResource("/" + icon)));

        button.setToolTipText(NbBundle.getMessage(Toolbar.class, tooltip));
        button.setFocusable(false);

        add(button);
    }

    protected final void addScreenshotButton() {
        JButton screenshotButton = new JButton();
        screenshotButton.setBorder(BUTTONS_BORDER);
        screenshotButton.setIcon(new ImageIcon(getClass().getResource("/" + SCREENSHOT_BUTTON_ICON)));
        screenshotButton.setFocusable(false);
        screenshotButton.setToolTipText(NbBundle.getMessage(Toolbar.class, "Toolbar.screenshot.tooltip"));

        screenshotButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BufferedImage screenshot = captureCanvas();

                // TODO: Figure out what to actually do with the screenshot
                JFrame frame = new JFrame("Screenshot");

                ImageIcon icon = new ImageIcon(screenshot);
                JLabel label = new JLabel(icon);

                frame.add(label);
                frame.pack();

                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });

        add(screenshotButton);
    }

    /**
     * Captures the canvas and returns it as a buffered image.
     * @return the captured canvas
     */
    protected final BufferedImage captureCanvas() {
        CanvasListener canvasListener = canvas.getListener();
        canvasListener.setRecording(true);
        canvas.getGLCanvas().display();
        BufferedImage image = canvasListener.getCapturedImage();
        canvasListener.setRecording(false);
        return image;
    }

    protected final JToggleButton initBackgroundButton() {
        JToggleButton button = Toolbox.initToggleButton(BACKGROUND_BUTTON_ICON, "Toolbar.background.tooltip", false);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (((JToggleButton) e.getSource()).isSelected()) {
                    canvas.getCanvasPanel().setBackground(SceneRenderer.DARK_BACKGROUND);
                    canvas.getSceneRenderer().setDarkBackground();
                } else {
                    canvas.getCanvasPanel().setBackground(SceneRenderer.BRIGHT_BACKGROUND);
                    canvas.getSceneRenderer().setBrightBackground();
                }
                renderScene();
            }
        });

        return button;
    }

    protected final JToggleButton initReflectionsButton() {
        JToggleButton button = Toolbox.initToggleButton(REFLECTIONS_BUTTON_ICON, "Toolbar.reflections.tooltip", false);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Drawable dm: getScene().getAllDrawables()) {
                    if (((JToggleButton) e.getSource()).isSelected()) {
                        dm.setHighlights(REFLECTIONS_COLOR);
                    } else {
                        dm.setHighlights(Color.BLACK);
                    }
                }
                renderScene();
            }
        });

        return button;
    }

}
