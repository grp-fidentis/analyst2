package cz.fidentis.analyst.toolbar;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.rendering.Scene;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Objects;

/**
 * A generic toolbox panel that pops up when a user clicks on a button at the canvas toolbar
 * (located under the 3D scene).
 *
 * @author Radek Oslejsek
 */
public abstract class Toolbox extends JPopupMenu {

    /**
     * Toolbox border
     */
    public static final Border BUTTONS_BORDER = new EmptyBorder(5,5,5,5);

    private final Canvas canvas;
    private  final JPanel panel;

    /**
     * Constructor.
     * @param canvas Rendering canvas
     */
    public Toolbox(Canvas canvas) {
        this.canvas = Objects.requireNonNull(canvas);

        panel = new JPanel();
        panel.setLayout(new FlowLayout());
        add(panel);
    }

    /**
     * Helper method that creates a toggle button for toolbox or toolbar.
     * @param icon The name icon file
     * @param tooltip NbBundle tooltip identifier
     * @param isSelected Whether the button is selected by default
     * @return The button.
     */
    public static JToggleButton initToggleButton(String icon, String tooltip, boolean isSelected) {
        JToggleButton button = new JToggleButton();
        button.setBorder(BUTTONS_BORDER);
        button.setIcon(new ImageIcon(Toolbox.class.getResource("/" + icon)));
        button.setFocusable(false);
        button.setSelected(isSelected);
        button.setToolTipText(NbBundle.getMessage(Toolbox.class, tooltip));
        return button;
    }

    /**
     * Helper method that creates a toggle button for toolbox or toolbar.
     * @param icon The name icon file
     * @param tooltip NbBundle tooltip identifier
     * @param isSelected Whether the button is selected by default
     * @return The button.
     */
    public static JButton initButton(String icon, String tooltip, boolean isSelected) {
        JButton button = new JButton();
        button.setBorder(BUTTONS_BORDER);
        button.setIcon(new ImageIcon(Toolbox.class.getResource("/" + icon)));
        button.setFocusable(false);
        button.setSelected(isSelected);
        button.setToolTipText(NbBundle.getMessage(Toolbox.class, tooltip));
        return button;
    }

    protected Component addToPanel(Component component) {
        return panel.add(component);
    }

    protected Canvas getCanvas() {
        return canvas;
    }

    protected Scene getScene() {
        return canvas.getScene();
    }

}
