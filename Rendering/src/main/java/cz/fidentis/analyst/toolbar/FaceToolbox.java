package cz.fidentis.analyst.toolbar;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.drawables.DrawableFace;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

/**
 * A popup toolbox for per-face settings.
 *
 * @author Radek Oslejsek
 */
public class FaceToolbox extends Toolbox {

    public static final int DEFAULT_TRANSPARENCY_SLIDER_LENGTH = 150;
    public static final String LANDMARKS_ICON = "fps28x28.png";
    public static final String SYMMETRY_ICON = "symmetry28x28.png";

    /**
     * Types of menu faces
     * @author Radek Oslejsek
     */
    public enum ToolboxType {

        SINGLE_FACE("head28x28.png", DrawableFace.SKIN_COLOR_PRIMARY, 100),
        PRIMARY_FACE("primaryFace28x28.png", DrawableFace.SKIN_COLOR_PRIMARY, 75),
        SECONDARY_FACE("secondaryFace28x28.png", DrawableFace.SKIN_COLOR_SECONDARY, 75);

        private final String icon;
        private final Color color;
        private final int opacity;

        /**
         * Constructor.
         * @param icon Icon
         * @param color Color
         * @param opacity Opacity
         */
        ToolboxType(String icon, Color color, int opacity) {
            this.icon = icon;
            this.color = color;
            this.opacity = opacity;
        }

        public String getIcon() {
            return icon;
        }

        public Color getColor() {
            return color;
        }

        public int getOpacity() {
            return opacity;
        }
    }

    /**
     * Constructor.
     * @param canvas Rendering canvas
     * @param faceSlot Slot of the face in the scene
     * @param toolboxType Toolbox type
     */
    public FaceToolbox(Canvas canvas, int faceSlot, ToolboxType toolboxType) {
        super(canvas);

        if (toolboxType != ToolboxType.SINGLE_FACE) {
            addToPanel(initColorButton(16, 35, toolboxType));
        }
        //addToPanel(initSymmetryButton(faceSlot));
        addToPanel(initLandmarksButton(faceSlot));
        addToPanel(initFaceButton(faceSlot, toolboxType.getIcon()));
        addToPanel(initSlider(faceSlot, toolboxType));
    }

    private JSlider initSlider(int faceSlot, ToolboxType toolboxType) {
        JSlider slider = new JSlider();
        slider.setPreferredSize(new Dimension(DEFAULT_TRANSPARENCY_SLIDER_LENGTH, 50));
        slider.setMaximum(100);
        slider.setPaintTicks(false);
        slider.setToolTipText(org.openide.util.NbBundle.getMessage(FaceToolbox.class, "FaceToolbox.slider.tooltip"));
        slider.setValue(toolboxType.getOpacity());
        slider.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        slider.setBorder(new EmptyBorder(0,0,0,0));

        getScene().getDrawableFace(faceSlot).setTransparency(slider.getValue()/100f);

        slider.addChangeListener((ChangeEvent e) -> {
            int val = ((JSlider) e.getSource()).getValue();
            getScene().getDrawableFace(faceSlot).setTransparency(val/100f);
            getCanvas().renderScene();
        });

        return slider;
    }

    private JButton initLandmarksButton(int faceSlot) {
        JButton button = initButton(LANDMARKS_ICON, "FaceToolbox.landButton.tooltip", false);
        button.setSelected(false);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                var drawableFeaturePoints = getScene().getDrawableFeaturePoints(faceSlot);
                boolean show = !drawableFeaturePoints.isShown(); // switch by default
                /*
                if (!getCanvas().isAllowedLandmarksButton()) {
                    JOptionPane.showMessageDialog(
                            getCanvas(),
                            "This function is disabled by the Feature Point tab");
                    return;
                }
                 */
                if (!getScene().getDrawableFace(faceSlot).getHumanFace().hasLandmarks()) {
                    //show = addRecognizedLandmarks(recognizeLandmarks(faceSlot), faceSlot);
                    JOptionPane.showMessageDialog(
                            getCanvas(),
                            "No landmarks available.\nUse the Feature Points tab to compute it.");
                }
                drawableFeaturePoints.show(show);
                getCanvas().renderScene();
            }
        });

        return button;
    }

    private JButton initSymmetryButton(int faceSlot) {
        JButton button = initButton(SYMMETRY_ICON, "FaceToolbox.symmetryButton.tooltip", false);
        button.setSelected(false);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*
                if (!getCanvas().isAllowSymmetryButton()) {
                    JOptionPane.showMessageDialog(
                            getCanvas(),
                            "This function is disabled for the Symmetry and Cutting Planes tabs.");
                    return;
                }
                 */
                if (!getScene().getDrawableFace(faceSlot).getHumanFace().hasSymmetryPlane()) {
                    JOptionPane.showMessageDialog(
                            getCanvas(),
                            "No symmetry plane available.\nUse the Symmetry tab to compute it.");
                    return;
                }
                var drawableSymmetryPlane = getScene().getDrawableSymmetryPlane(faceSlot);
                drawableSymmetryPlane.show(!drawableSymmetryPlane.isShown());
                getCanvas().renderScene();
            }
        });

        return button;
    }

    private JToggleButton initFaceButton(int faceSlot, String icon) {
        JToggleButton button = initToggleButton(icon, "FaceToolbox.faceButton.tooltip", true);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean onOff = ((JToggleButton) e.getSource()).isSelected();
                getScene().showDrawableFace(faceSlot, onOff);
                getCanvas().renderScene();
            }
        });

        return button;
    }

    private JButton initColorButton(int width, int height, ToolboxType toolboxType) {
        JButton button = new JButton();

        button.setToolTipText(NbBundle.getMessage(FaceToolbox.class, "FaceToolbox.colorButton.tooltip"));
        button.setBorder(new EmptyBorder(0,0,0,0));
        BufferedImage image = new BufferedImage(width, height, java.awt.image.BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();
        graphics.setColor(toolboxType.getColor());
        graphics.fillRect(0, 0, width, height);
        graphics.setXORMode(Color.LIGHT_GRAY);
        graphics.drawRect(0, 0, width - 1, height - 1);
        image.flush();
        button.setIcon(new ImageIcon(image));
        button.repaint();

        return button;
    }

}
