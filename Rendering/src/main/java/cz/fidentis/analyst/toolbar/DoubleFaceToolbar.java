package cz.fidentis.analyst.toolbar;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;
import cz.fidentis.analyst.rendering.ShadersManager;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Canvas toolbox for scenes with two faces. 
 * It enables to show-hide faces in the scene, show-hide feature points,
 * and to change the transparency of face couple.
 * 
 * @author Richard Pajersky
 * @author Radek Oslejsek
 * @author Ondrej Simecek
 */
public class DoubleFaceToolbar extends Toolbar {

    /**
     * Constructor.
     *
     * @param canvas Rendering canvas
     * @param faces  Faces processed by current analytical task
     */
    public DoubleFaceToolbar(Canvas canvas, Task task) {
        super(canvas);

        addLabel("Rendering:");
        addToolbarMenu(new RenderingToolbox(canvas), RENDERING_MODE_TOOLBOX_ICON, "Toolbar.renderingMode.tooltip");
        add(initBackgroundButton());
        add(initReflectionsButton());
        addLabel("Differences:");
        add(initContoursButton());
        add(initGlyphsButton());
        addToolbarMenu(new FogToolbox(canvas), FOG_VERSION_TOOLBOX_ICON, "Toolbar.fog.tooltip");
        addLabel("Screenshot:");
        addScreenshotButton();
        addLabel("Subjects:");
        addToolbarMenu(
                new FaceToolbox(canvas, canvas.getScene().getPrimaryFaceSlot(), FaceToolbox.ToolboxType.PRIMARY_FACE),
                FaceToolbox.ToolboxType.PRIMARY_FACE.getIcon(),
                "Toolbar.primaryFaceButton.tooltip"
        );
        addToolbarMenu(
                new FaceToolbox(canvas, canvas.getScene().getSecondaryFaceSlot(), FaceToolbox.ToolboxType.SECONDARY_FACE),
                FaceToolbox.ToolboxType.SECONDARY_FACE.getIcon(),
                "Toolbar.secondaryFaceButton.tooltip"
        );

        // be informed if something changes in the faces -- see acceptEvent()
        FaceReference faceReference = TaskService.INSTANCE.getPrimaryFace(task);
        FaceService.INSTANCE.getFaceByReference(faceReference).registerListener(this);
        FaceReference secondaryFaceReference = TaskService.INSTANCE.getSecondaryFace(task);
        FaceService.INSTANCE.getFaceByReference(secondaryFaceReference).registerListener(this);
    }

    @Override
    public void acceptEvent(HumanFaceEvent event) {
    }

    protected final JToggleButton initContoursButton() {
        JToggleButton button = Toolbox.initToggleButton(CONTOURS_ICON, "Toolbar.contoursButton.tooltip", false);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean onOff = ((JToggleButton) e.getSource()).isSelected();
                if (getCanvas().getSceneRenderer().getShadersManager() != null) {
                    getCanvas().getSceneRenderer().getShadersManager().setUseContours(onOff);
                }
                getCanvas().renderScene();
            }
        });

        return  button;
    }

    protected final JToggleButton initGlyphsButton() {
        JToggleButton button = Toolbox.initToggleButton(GLYPH_ICON, "Toolbar.glyphsButton.tooltip", false);

        button.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean onOff = ((JToggleButton) e.getSource()).isSelected();
                ShadersManager shaders = getCanvas().getSceneRenderer().getShadersManager();
                if (shaders != null) {
                    shaders.setUseGlyphs(onOff);
                    getCanvas().renderScene();
                }
            }
        });

        return button;
    }

}
