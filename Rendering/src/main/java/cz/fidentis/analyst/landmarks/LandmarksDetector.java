package cz.fidentis.analyst.landmarks;

import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.canvas.CanvasState;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.landmarks.impl.LandmarksDetectorImpl;

import java.util.List;

/**
 * Services for detecting faces and their landmarks from 2D images captured from the canvas.
 * The canvas is modified! Therefore, should its state should be saved and then recovered.
 * Sequence to recognize landmarks from the scene (a single snapshot recognition):
 * <ul>
 *     <li>{@link Canvas#getState()}</li>
 *     <li>Set camera, zoom, show only required face, or use {@link #zoomAndRenderFace(DrawableFace)}</li>
 *     <li>{@link #recognizeFromImage(int)} </li>
 *     <li>{@link Canvas#setState(CanvasState)}</li>
 * </ul>
 *
 * Sequence to recognize a single face automatically (snapshots taken from different angles):
 * <ul>
 *     <li>{@link Canvas#getState()}</li>
 *     <li>{@link #recognizeFromMultipleAngles(int, SymmetryConfig, int, double)}</li>
 *     <li>{@link Canvas#setState(CanvasState)}</li>
 * </ul>
 *
 * @author Jan Popelas
 * @author Radek Oslejsek
 */
public interface LandmarksDetector {

    /**
     * Creates and returns detector. Each object instantiates its own face detection AI model automatically.
     *
     * @param canvas Canvas with the scene. Must not be {@code null}
     * @return detector
     */
    static LandmarksDetector getDetector(Canvas canvas) {
        return new LandmarksDetectorImpl(canvas);
    }

    /**
     * Prepares the scene for a single-face snapshot. The scene/canvas is changed!
     * Don't forget to save and recover its state.
     *
     * @param face Face to be rendered and later analyzed
     */
    void zoomAndRenderFace(DrawableFace face);

    /**
     * Detects landmarks from the canvas, usually prepared by the previous call of
     * the {@link #zoomAndRenderFace(DrawableFace)} method.
     * It is supposed that only one face is visible.
     *
     * @param minLandmarks Minimum required landmarks to be detected.
     * @return List of 3D landmarks that were detected from the camera view,
     * an empty list if the required number of landmarks was not found.
     */
    List<Landmark> recognizeFromImage(int minLandmarks);

    /**
     * Detects landmarks of a face. Camera is rotated around the selected face (using its symmetry plane) to
     * capture the face from different angles until the required number of landmarks is recognized.
     * The scene is prepared automatically, i.e., previous calls of {@link #zoomAndRenderFace(DrawableFace)} has no effect.
     *
     * @param faceSlot Scene slot of the face that should be recognized
     * @param symmetryConfig Symmetry configuration used to compute the symmetry plane temporarily if the
     *                       symmetry plane of the face is missing.
     * @param minLandmarks Minimum number of required landmarks
     * @param cameraRotationAngle Angle of camera rotation. If unsure, use 30
     * @return List of 3D landmarks that were detected from the camera view, an empty list if at least required
     * number of landmarks was not found.
     */
    List<Landmark> recognizeFromMultipleAngles(
            int faceSlot,
            SymmetryConfig symmetryConfig,
            int minLandmarks,
            double cameraRotationAngle
    );
}
