package cz.fidentis.analyst.landmarks.impl;

/**
 * Represents a significant point in an image.
 *
 * @param pointType the type of the significant point
 * @param x the x coordinate of the significant point
 * @param y the y coordinate of the significant point
 *
 * @author Jan Popelas
 */
public record SignificantPoint(SignificantPointType pointType, int x, int y) {

    /**
     * Returns landmark type
     * @return landmark type
     */
    public String landmarkName() {
        return switch (pointType) {
            case RIGHT_EYE -> "Exocanthion R";
            case LEFT_EYE -> "Exocanthion L";
            case NOSE -> "Pronasale";
            case RIGHT_MOUTH_CORNER -> "Cheilion R";
            case LEFT_MOUTH_CORNER -> "Cheilion L";
            default -> "Unknown";
        };
    }
}
