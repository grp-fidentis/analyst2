package cz.fidentis.analyst.landmarks.impl;

/**
 * Represents information about a face detected in an image.
 *
 * @param boundingRectangle the bounding box of the detected face
 * @param significantPoints the significant points of the detected face
 *
 * @author Jan Popelas
 */
public record FaceDetectionInformation(BoundingRectangle boundingRectangle, SignificantPoint[] significantPoints) {

    /**
     * Constructor.
     * @param boundingRectangle the bounding box of the detected face
     * @param significantPoints the significant points of the detected face
     */
    public FaceDetectionInformation {
        if (boundingRectangle == null) {
            throw new IllegalArgumentException("Bounding box cannot be null");
        }

        if (significantPoints == null) {
            significantPoints = new SignificantPoint[0];
        }
    }
}
