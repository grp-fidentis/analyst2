package cz.fidentis.analyst.landmarks.impl;

/**
 * Represents a 2D bounding box in an image.
 *
 * @param x X coordinate
 * @param y Y coordinate
 * @param width Width
 * @param height Height
 *
 * @author Jan Popelas
 */
public record BoundingRectangle(int x, int y, int width, int height) {

    /**
     * Returns the x coordinate of the opposite corner of the bounding box.
     * @return the x coordinate of the opposite corner of the bounding box
     */
    public int getOppositeX() {
        return x + width;
    }

    /**
     * Returns the y coordinate of the opposite corner of the bounding box.
     * @return the y coordinate of the opposite corner of the bounding box
     */
    public int getOppositeY() {
        return y + height;
    }
}
