package cz.fidentis.analyst.landmarks.impl;

import nu.pattern.OpenCV;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.FaceDetectorYN;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * An IA-based detector of faces and their landmarks from 2D images. It uses the YuNet model from
 * the <a href="https://github.com/opencv/opencv_zoo/tree/main/models/face_detection_yunet">OpenCV Zoo - YuNet</a>.
 *
 * <p>
 * The model is loaded from a resource file.
 * The detected faces are represented by 2D bounding boxes and significant 2D points.
 * The significant points are the right eye, left eye, nose, right mouth corner, and left mouth corner.
 * </p>
 *
 * @see FaceDetectorYN
 * @see FaceDetectionInformation
 *
 * @author Jan Popelas
 */
public class FaceDetector {

    private FaceDetectorYN faceDetectorModel;

    private static final String MODEL_NAME = "face_detection_yunet_2023mar.onnx";
    private static final Size MODEL_RECOGNITION_SIZE = new Size(320, 320);
    private static final String MODEL_CONFIG = "";
    private static final float MODEL_SCORE_THRESHOLD = 0.6f;
    private static final float MODEL_NMS_THRESHOLD = 0.5f;

    private static final SignificantPointType[] SIGNIFICANT_POINTS = new SignificantPointType[]{
            SignificantPointType.RIGHT_EYE,
            SignificantPointType.LEFT_EYE,
            SignificantPointType.NOSE,
            SignificantPointType.RIGHT_MOUTH_CORNER,
            SignificantPointType.LEFT_MOUTH_CORNER
    };

    /**
     * Constructor.
     * Loads the model from the resource file.
     */
    public FaceDetector() {
        OpenCV.loadLocally();
        loadModel();
    }

    /**
     * Detects faces in the given image.
     *
     * @param imageFile the image file to detect faces in
     * @return information about the detected faces - bounding box and significant points (if available)
     */
    public List<FaceDetectionInformation> detect(File imageFile) {
        return detect(Imgcodecs.imread(imageFile.getAbsolutePath()));
    }

    /**
     * Detects faces in the given image.
     *
     * @param image the image to detect faces in
     * @return information about the detected faces - bounding box and significant points (if available)
     */
    public List<FaceDetectionInformation> detect(BufferedImage image) {
        int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        byte[] data = new byte[image.getWidth() * image.getHeight() * 3];

        for (int i = 0; i < pixels.length; i++) {
            data[i * 3 + 2] = (byte) ((pixels[i] >> 16) & 0xFF);
            data[i * 3 + 1] = (byte) ((pixels[i] >> 8) & 0xFF);
            data[i * 3] = (byte) (pixels[i] & 0xFF);
        }

        Mat imageMatrix = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
        imageMatrix.put(0, 0, data);

        return detect(imageMatrix);
    }

    /**
     * Loads the model from the resource file.
     *
     * The reason why we load it into a temporary file is that the create method of FaceDetectorYN
     * has issues with loading path from resource and throws exception "(-5: Bad argument)"
     *
     * This circumvents the issue by creating a temporary file and loading the model from it.
     * Somehow, this works. Sue me.
     */
    private void loadModel() {
        InputStream is = getClass().getClassLoader().getResourceAsStream("/" + MODEL_NAME);
        File tempFile;
        try {
            tempFile = File.createTempFile("targetmodel", ".onnx");
            tempFile.deleteOnExit();
            try (FileOutputStream out = new FileOutputStream(tempFile)) {
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = is.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
            }
            String path = tempFile.getAbsolutePath();
            faceDetectorModel = FaceDetectorYN.create(
                    path,
                    MODEL_CONFIG,
                    MODEL_RECOGNITION_SIZE,
                    MODEL_SCORE_THRESHOLD,
                    MODEL_NMS_THRESHOLD
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param imageMatrix the image matrix to detect faces in
     * @return information about the detected faces - bounding box and significant points (if available)
     */
    private List<FaceDetectionInformation> detect(Mat imageMatrix) {
        Mat image = new Mat();
        Imgproc.cvtColor(imageMatrix, image, Imgproc.COLOR_RGBA2RGB);

        faceDetectorModel.setInputSize(new Size(image.width(), image.height()));
        Mat outputDetectionData = new Mat();

        faceDetectorModel.detect(image, outputDetectionData);

        List<FaceDetectionInformation> result = new ArrayList<>();
        for (int faceIndex = 0; faceIndex < outputDetectionData.rows(); faceIndex++) {
            result.add(getInformationFromDetectionRow(outputDetectionData.row(faceIndex)));
        }

        return result;
    }

    /**
     * @param detectionRow the row of the detection data
     * @return information about the detected face - bounding box and significant points (if available)
     */
    private static FaceDetectionInformation getInformationFromDetectionRow(Mat detectionRow) {
        double[] detection = new double[(int) (detectionRow.total() * detectionRow.channels())];
        for (int j = 0; j < detectionRow.width(); j++) {
            detection[j] = detectionRow.get(0, j)[0];
        }
        int x = (int) detection[0];
        int y = (int) detection[1];
        int width = (int) detection[2] - x;
        int height = (int) detection[3] - y;

        BoundingRectangle boundingRectangle = new BoundingRectangle(x, y, width, height);
        SignificantPoint[] significantPoints = new SignificantPoint[5];
        for (int j = 4; j < detection.length && j < 14; j += 2) {
            int pointIndex = (j - 4) / 2;
            SignificantPoint point = new SignificantPoint(SIGNIFICANT_POINTS[pointIndex], (int) detection[j], (int) detection[j + 1]);
            significantPoints[pointIndex] = point;
        }

        return new FaceDetectionInformation(boundingRectangle, significantPoints);
    }

}
