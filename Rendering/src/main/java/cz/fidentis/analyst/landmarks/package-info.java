/**
 * Detection of landmarks by making canvas snapshots, detecting landmarks in 2D, and then
 * projecting 2D landmarks back to the scene.
 */
package cz.fidentis.analyst.landmarks;
