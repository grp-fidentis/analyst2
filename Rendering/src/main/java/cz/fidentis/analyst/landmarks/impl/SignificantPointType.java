package cz.fidentis.analyst.landmarks.impl;

/**
 * Types of 2D landmarks that we are able to identify.
 *
 * @author Jan Popelas
 * @see SignificantPoint
 */
public enum SignificantPointType {
    RIGHT_EYE,
    LEFT_EYE,
    NOSE,
    RIGHT_MOUTH_CORNER,
    LEFT_MOUTH_CORNER
}
