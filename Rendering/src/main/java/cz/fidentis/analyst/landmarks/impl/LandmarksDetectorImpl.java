package cz.fidentis.analyst.landmarks.impl;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.canvas.Canvas;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.LandmarksFactory;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.rendering.Camera;
import cz.fidentis.analyst.landmarks.LandmarksDetector;
import cz.fidentis.analyst.rendering.RenderingMode;
import org.openide.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * The implementation of landmarks detector.
 *
 * @author Jan Popelas
 * @author Radek Oslejsek
 */
public class LandmarksDetectorImpl implements LandmarksDetector {

    private final Canvas canvas;
    private final FaceDetector faceDetector = new FaceDetector();

    /**
     * Constructor.
     *
     * @param canvas Canvas with the scene. Must not be {@code null}
     */
    public LandmarksDetectorImpl(Canvas canvas){
        this.canvas = Objects.requireNonNull(canvas);
    }

    @Override
    public void zoomAndRenderFace(DrawableFace face) {
        canvas.setDarkBackground(false);
        canvas.getScene().getDrawableFaces().forEach(dr -> dr.show(false));
        face.show(true);
        face.setTransparency(1.0f);
        face.setRenderMode(RenderingMode.TEXTURE);
        canvas.getCamera().zoomToFit(canvas.getScene());
        canvas.renderScene();
    }

    @Override
    public List<Landmark> recognizeFromImage(int minLandmarks) {
        List<Landmark> landmarks = new ArrayList<>();

        List<FaceDetectionInformation> detectionInf = faceDetector.detect(canvas.captureCanvas());
        if (detectionInf == null || detectionInf.isEmpty()) {
            return Collections.emptyList();
        }

        var di = detectionInf.get(0); // get (the only one) face results
        if (di == null || di.significantPoints() == null || di.significantPoints().length < minLandmarks) {
            return Collections.emptyList();
        }

        for (SignificantPoint significantPoint : di.significantPoints()) {
            Pair<HumanFace, RayIntersection> closestFace = canvas.castRayThroughPixel(significantPoint.x(), significantPoint.y());
            if (closestFace == null) {
                Logger.print("No face found for significant point " + significantPoint);
            } else {
                landmarks.add(constructLandmark(significantPoint, closestFace.second()));
            }
        }

        return landmarks.size() < minLandmarks ? Collections.emptyList() : landmarks;
    }

    @Override
    public List<Landmark> recognizeFromMultipleAngles(
            int faceSlot,
            SymmetryConfig symmetryConfig,
            int minLandmarks,
            double cameraRotationAngle) {

        DrawableFace face = canvas.getScene().getDrawableFace(faceSlot);

        // prepare the scene
        boolean removePlane = computeTemporarySymmetryPlane(face.getHumanFace(), symmetryConfig);
        zoomAndRenderFace(face);

        // compute
        Camera origCamera = canvas.getCamera().copy();
        List<Landmark> landmarks = null;
        for (Camera camera: getCameraPositions(face.getHumanFace(), cameraRotationAngle)) {
            canvas.setCamera(camera);
            canvas.renderScene();
            landmarks = recognizeFromImage(minLandmarks);
            if (!landmarks.isEmpty()) {
                break;
            }
        }
        canvas.setCamera(origCamera);

        // recover the scene
        if (removePlane) {
            face.getHumanFace().setSymmetryPlane(null);
        }

        return landmarks;
    }

    /**
     *
     * @param face Face
     * @param symmetryConfig Symmetry config
     * @return {@code true} is a temporary symmetry plane has been set (and should be removed)
     * @throws IllegalArgumentException on parameters mismatch
     */
    private static boolean computeTemporarySymmetryPlane(HumanFace face, SymmetryConfig symmetryConfig) {
        if (face.hasSymmetryPlane()) {
            return false;
        }
        if (symmetryConfig == null) {
            throw new IllegalArgumentException("symmetry plane not defined and symmetryConfig is missing");
        }
        FaceStateServices.updateSymmetryPlane(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT, symmetryConfig);
        return true;
    }

    /**
     * Constructs a landmark based on the significantPoint and the ray intersection.
     *
     * @param significantPoint
     * @param rayIntersection
     * @return the constructed landmark
     */
    private static Landmark constructLandmark(SignificantPoint significantPoint, RayIntersection rayIntersection) {
        Landmark landmark = LandmarksFactory.createFeaturePointByName(
                significantPoint.landmarkName(),
                rayIntersection.getPosition()
        );
        landmark.setMeshVicinity(new MeshVicinity(0, rayIntersection.getPosition()));
        return landmark;
    }

    private List<Camera> getCameraPositions(HumanFace face, double cameraRotAngle) {
        Camera camera = canvas.getCamera().copy();
        FaceStateServices.updateBoundingBox(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        camera.zoomToFitPerpendicular(face.getBoundingBox(), face.getSymmetryPlane().getNormal());

        List<Camera> ret = new ArrayList<>();
        for (double xAngle = 0; xAngle < 360; xAngle += cameraRotAngle) {
            Camera newCamera = camera.copy();
            newCamera.rotate(xAngle, 0);
            ret.add(newCamera);
        }
        return ret;
    }
}
