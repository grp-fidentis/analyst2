package cz.fidentis.analyst.data.face;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.Glyph;
import cz.fidentis.analyst.data.shapes.Plane;

import java.util.List;

/**
 * A copy of the internal state of the {@link HumanFace} instance.
 * Can be used to fall back changes made with the face.
 *
 * @author Radek Oslejsek
 *
 * @param meshModel Mesh model
 * @param symmetryPlane Symmetry plane
 * @param kdTree K-d tree
 * @param octree Octree
 * @param featurePoints Feature points
 * @param boundingBox Bounding box
 * @param glyphs Glyphs
 */
public record HumanFaceState(MeshModel meshModel,
                             Plane symmetryPlane,
                             KdTree kdTree,
                             Octree octree,
                             List<Landmark> featurePoints,
                             Box boundingBox,
                             List<Glyph> glyphs) {

    /**
     * Constructor.
     * @param face Original face.
     */
    public HumanFaceState(HumanFace face) {
        this(
                MeshFactory.cloneMeshModel(face.getMeshModel()),
                face.hasSymmetryPlane() ? new Plane(face.getSymmetryPlane()) : null,
                face.getKdTree(),
                face.getOctree(),
                face.hasLandmarks() ? face.getAllLandmarks().stream().map(Landmark::clone).toList() : null,
                face.getBoundingBox(),
                face.getGlyphs()
        );
    }
}
