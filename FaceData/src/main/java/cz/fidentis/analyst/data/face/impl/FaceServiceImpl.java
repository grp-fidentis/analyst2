package cz.fidentis.analyst.data.face.impl;

import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.data.face.*;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Service used for manipulation with faces
 *
 * @author Marek Seďa
 */
public class FaceServiceImpl implements FaceService {

    private final AtomicLong counter = new AtomicLong();

    /**
     * The factory instantiating human faces and automatically
     * swapping them onto the dist if the application runs out of memory.
     */
    private final HumanFaceMemoryManager factory = HumanFaceMemoryManager.create();

    @Override
    public HumanFace getFaceByReference(FaceReference reference) {
        if (reference == null) {
            return null;
        }
        return factory.getFace(reference.getId());
    }

    @Override
    public void replaceFaces(List<HumanFace> faces) throws IOException {
        Long maxId = 0L;
        factory.clearFaces();
        for (HumanFace face : faces) {
            Logger.print("Replacing face " + face.getId());
            factory.addFace(face);
            if (maxId < face.getId()) {
                maxId = face.getId();
            }
        }
        counter.set(maxId);
    }

    @Override
    public FaceReference createCopyOfFace(FaceReference reference) {
        Logger.print("Creating copy of face " + reference.getId());
        HumanFace oldFace = factory.getFace(reference.getId());
        Long newId = counter.incrementAndGet();
        try {
            HumanFace newFace = HumanFaceFactory.create(newId, new File(oldFace.getPath()));
            factory.addFace(newFace);
            return new FaceReferenceImpl(newId, reference.isAverageFace(), reference.getFaceFile());
        } catch (IOException e) {}
        return null;

    }

    @Override
    public boolean isInMemory(FaceReference reference) {
        return factory.isLoaded(reference.getId());
    }

    @Override
    public FaceReference loadFace(File file) throws IOException {
        Long newId = counter.incrementAndGet();
        Logger.print("Load face " + newId);
        HumanFace face = HumanFaceFactory.create(newId, file);
        factory.addFace(face);
        Logger.print("Loading face " + newId + " from file " + file.getCanonicalPath());
        return new FaceReferenceImpl(newId, false, file);
    }

    @Override
    public boolean removeFace(FaceReference faceReference) {
        return factory.removeFace(faceReference.getId());
    }

    @Override
    public FaceReference createFaceFromMeshModel(MeshModel meshModel, File file, boolean isAverageFace) throws IOException {
        Long newId = counter.incrementAndGet();
        HumanFace newHumanFace = HumanFaceFactory.create(MeshFactory.cloneMeshModel(meshModel), file.getCanonicalPath(), newId, isAverageFace);
        factory.addFace(newHumanFace);
        return new FaceReferenceImpl(newId, false, file);
    }

    @Override
    public HumanFace createTemporaryInMemoryFace(MeshModel meshModel) {
        return HumanFaceFactory.create(meshModel, "tempId", -1L, false);
    }

    @Override
    public HumanFace loadTemporaryInMemoryFace(File file) throws IOException {
        return HumanFaceFactory.create(-1L, file);
    }

    @Override
    public void setDumpStrategy(HumanFaceMemoryManager.Strategy strategy) {
        factory.setStrategy(strategy);
    }
}
