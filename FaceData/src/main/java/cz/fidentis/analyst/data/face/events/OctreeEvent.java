package cz.fidentis.analyst.data.face.events;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;

/**
 * The root type for events relate to the changes in octree.
 * 
 * @author Enkh-Undral EnkhBayar
 */
public class OctreeEvent extends HumanFaceEvent {
    
    /**
     * Constructor.
     * @param face Human face related to the event
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public OctreeEvent(HumanFace face, String name, Object issuer) {
        super(face, name, issuer);
    }

    /**
     * Returns {@code true} if the octree is calculated.
     * @return {@code true} if the octree is calculated.
     */
    public boolean isCalculated() {
        return false;
    }

}
