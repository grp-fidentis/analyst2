package cz.fidentis.analyst.data.face.events;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;

/**
 * New mesh distance metric has been computed.
 * 
 * @author Radek Oslejsek
 */
public class MeshDistanceComputed extends HumanFaceEvent {
    
    private final HumanFace targetFace;
    private final MeshDistances meshDistances;
    
    /**
     * Constructor.
     * @param sourceFace Human face from which the HD has been computed
     * @param targetFace Human face towards which the HD has been computed
     * @param meshDistances Mesh distance stats
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public MeshDistanceComputed(
            HumanFace sourceFace, 
            HumanFace targetFace, 
            MeshDistances meshDistances,
            String name,
            Object issuer) {
        
        super(sourceFace, name, issuer);
        this.targetFace = targetFace;
        this.meshDistances = meshDistances;
    }
    
    /**
     * Returns human face from which the HD has been computed. This method
     * is identical to {@link #getFace()}
     * 
     * @return human face from which the HD has been computed.
     */
    public HumanFace getSourceFace() {
        return getFace();
    }

    /**
     * Returns human face towards which the HD has been computed.
     * 
     * @return human face towards which the HD has been computed.
     */
    public HumanFace getTargetFace() {
        return targetFace;
    }

    /**
     * Returns distance values.
     * @return distance values.
     */
    public MeshDistances getMeshDistances() {
        return meshDistances;
    }
}
