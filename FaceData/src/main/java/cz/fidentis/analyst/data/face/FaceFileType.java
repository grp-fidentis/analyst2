package cz.fidentis.analyst.data.face;

/**
 * Types of files associated with a single human face
 * @author Radek Oslejsek
 */
public enum FaceFileType {
    MESH, // .obj
    FEATURE_POINTS, // _landmarks.csv
    PREVIEW_IMAGE, // _preview.jpg
    INFO, // _info.csv
    TEXTURE // .jpg
}
