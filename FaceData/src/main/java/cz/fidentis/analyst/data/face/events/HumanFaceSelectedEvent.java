package cz.fidentis.analyst.data.face.events;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.ray.RayIntersection;

/**
 * A human face has been selected
 * @author Mario Chromik
 */
public class HumanFaceSelectedEvent extends HumanFaceEvent {
    private final RayIntersection intersect;
    
    /**
     * Returns a ray intersection that selected the face 
     * (a ray thrown from the mouse location into the scene).
     * 
     * @return a ray intersection that selected the face
     */
    public RayIntersection getIntersection() {
        return intersect;
    }
    
    /**
     * Constructor
     * @param face Human face related to event
     * @param in Ray intersection
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public HumanFaceSelectedEvent(HumanFace face, RayIntersection in, String name, Object issuer) {
        super(face, name, issuer);
        this.intersect = in;
    }
    
}
