package cz.fidentis.analyst.data.face.events;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;

/**
 * A human face all its components (mesh, symmetry plane, feature points, etc.)
 * have been transformed in scape.
 * 
 * @author Radek Oslejsek
 */
public class HumanFaceTransformedEvent extends HumanFaceEvent {
    
    private final boolean isFinished;
    
    /**
     * Constructor of finished transformation.
     * 
     * @param face Human face related to the event
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public HumanFaceTransformedEvent(HumanFace face, String name, Object issuer) {
        this(face, name, issuer, true);
    }
    
    /**
     * Constructor.
     * @param face Human face related to the event
     * @param name Event name provided by issuer
     * @param issuer The issuer
     * @param isFinished if {@code true}, then it is supposed that the transformation will continue
     *        (is in the progress).
     */
    public HumanFaceTransformedEvent(HumanFace face, String name, Object issuer, boolean isFinished) {
        super(face, name, issuer);
        this.isFinished = isFinished;
    }
    
    /**
     * Returns {@code true} it the transformation is finished (is not under progress)
     * @return {@code true} it the transformation is finished 
     */
    public boolean isFinished() {
        return this.isFinished;
    }
}
