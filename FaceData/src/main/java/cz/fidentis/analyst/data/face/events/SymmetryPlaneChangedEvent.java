package cz.fidentis.analyst.data.face.events;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;

/**
 * A new symmetry plane has been added or the previous one has been recomputed.
 * If the plane is transformed separately (without the transformation of mesh or
 * other parts of the human face), use this event as well. 
 * On the contrary, if symmetry plane is transformed together with 
 * the mesh transformation, use the {@code HausdorffDistanceComputed} event instead.
 * 
 * @author Radek Oslejsek
 */
public class SymmetryPlaneChangedEvent extends HumanFaceEvent {
    
    /**
     * Constructor.
     * 
     * @param face Human face related to the event
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public SymmetryPlaneChangedEvent(HumanFace face, String name, Object issuer) {
        super(face, name, issuer);
    }
    
}
