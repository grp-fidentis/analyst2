package cz.fidentis.analyst.data.face;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.Glyph;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * A single human face consisting of a mesh, feature points, space partitioning structures, and other data structures.
 *
 * @author Radek Oslejsek
 * @author Matej Kovar
 * @author Katerina Zarska
 */
public interface HumanFace extends HumanFaceEventBus, Serializable {

    String LANDMARK_FILE_SUFFIX = "_landmarks.csv";

    /**
     *
     * @return unique ID of Face
     */
    Long getId();

    /**
     *
     * @return is average face
     */
    boolean isAverageFace();

    /**
     * Returns the triangular mesh model of the human face.
     *
     * @return the triangular mesh model of the human face
     */
    MeshModel getMeshModel();

    /**
     * Sets the symmetry plane. If the input argument is {@code null}, then removes the plane.
     *
     * @param plane The new symmetry plane
     */
    void setSymmetryPlane(Plane plane);

    /**
     *
     * @return The face's symmetry plane
     */
    Plane getSymmetryPlane();

    /**
     * Returns {@code true} if the face has the symmetry plane computed.
     * @return {@code true} if the face has the symmetry plane computed.
     */
    boolean hasSymmetryPlane();

    /**
     * Reads feature points from a file on the given path.
     *
     * @param path Directory where the file is located
     * @param fileName Name of the file
     * @throws IOException on I/O failure
     */
    void loadFeaturePoints(String path, String fileName) throws IOException;

    /**
     * Returns all feature points or empty list.
     * 
     * @return The face's feature points.
     */
    List<Landmark> getAllLandmarks();
    
    /**
     * @return list of standard feature points
     */
    List<Landmark> getStandardFeaturePoints();
    
    /**
     * @return list of custom feature points
     */
    List<Landmark> getCustomLandmarks();

    /**
     * Adds custom landmark to face
     */
    void addCustomLandmark(Landmark landmark);

    /**
     * Removes custom landmark from face
     * @param landmark Landmark to remove
     * @return true if the landmark was removed, false otherwise
     */
    boolean removeCustomLandmark(Landmark landmark);

    /**
     * Checks if HumanFace has feature points
     * @return true if yes and false if not
     */
    boolean hasLandmarks();

    /**
     * Returns canonical path to the face file
     * @return canonical path to the face file
     */
    String getPath();

    /**
     * Returns short name of the face without its path in the name. May not be unique.
     * @return short name of the face without its path in the name
     */
    String getShortName();

    /**
     * Returns canonical path to the folder in which the face file is located, i.e.,
     * {@link #getPath()} with {@link #getShortName()} suffix removed.
     *
     * @return Returns canonical path to the folder in which the face file is located
     */
    String getDirectory();

    /**
     * Returns already computed octree of the triangular mesh or {@code null}.
     * @return Already computed octree of the triangular mesh or {@code null}
     */
    Octree getOctree();

    /**
     * Checks if HumanFace has octree calculated
     * @return true if yes and false if not
     */
    boolean hasOctree();

    /**
     * Sets teh octree
     *
     * @param octree New octree. Can be {@code null}
     */
    void setOctree(Octree octree);

    /**
     * Returns already computed k-d tree of the triangular mesh or {@code null}.
     * @return Already computed k-d tree of the triangular mesh or {@code null}
     */
    KdTree getKdTree();

    /**
     * Sets teh octree
     *
     * @param kdTree New k-d tree. Can be {@code null}
     */
    void setKdTree(KdTree kdTree);

    /**
     * Checks if HumanFace has KdTree calculated
     * @return true if yes and false if not
     */
    boolean hasKdTree();

    /**
     * Returns Interactive mask. The mask can be empty;
     * 
     * @return the interactive mask
     */
    SurfaceMask getSurfaceMask();

    /**
     * Returns a deep copy of current state.
     * @return a deep copy of current state.
     */
    HumanFaceState getState();

    /**
     * Falls back to given state. No event is triggered - it up to the caller.
     * @param state Old state. Must not be {@code null}
     */
    void setState(HumanFaceState state);

    /**
     * Gets the glyphs of the face or empty list
     *
     * @return list of glyphs or empty list
     */
    List<Glyph> getGlyphs();

    /**
     * Sets the glyphs.
     *
     * @param glyphs Glyphs. Can be {@code null}
     */
    void setGlyphs(List<Glyph> glyphs);

    /**
     * Checks if the human face has assigned glyphs
     * @return {@code true}, if the glyphs exist
     */
    boolean hasGlyphs();

    /**
     * Returns bounding box or {@code null}
     * @return bounding box or {@code null}
     */
    Box getBoundingBox();

    /**
     * Set a bounding box.
     * @param boundingBox Bounding box or {@code null}
     */
    void setBoundingBox(Box boundingBox);

    /**
     * Checks if the human face has assigned a bounding box
     * @return {@code true}, if the bounding box exists
     */
    boolean hasBoundingBox();

    /**
     * Set a surface mask.
     * @param surfaceMask surfaceMask or {@code null}
     */
    void setSurfaceMask(SurfaceMask surfaceMask);

}
