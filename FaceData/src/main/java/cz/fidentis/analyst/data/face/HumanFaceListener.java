package cz.fidentis.analyst.data.face;

import com.google.common.eventbus.Subscribe;

/**
 * Objects implementing this interface can be registered with 
 * a {@link HumanFace} object
 * and then be informed when the human face (some of its internal data structures) changes.
 * 
 * @author Radek Oslejsek
 */
public interface HumanFaceListener {

    /**
     * Subscription method, which is invoked when an event appears.
     * 
     * @param event A fired event.
     */
    @Subscribe
    void acceptEvent(HumanFaceEvent event);
    
}
