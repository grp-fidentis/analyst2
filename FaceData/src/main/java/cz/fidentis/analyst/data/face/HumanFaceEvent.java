package cz.fidentis.analyst.data.face;

/**
 * The root type for events fired by the {@link HumanFace}.
 * 
 * @author Radek Oslejsek
 */
public class HumanFaceEvent {
    
    private final HumanFace face;
    private final String name;
    private final Object issuer;
    
    /**
     * Constructor.
     * @param face Human face related to the event
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public HumanFaceEvent(HumanFace face, String name, Object issuer) {
        this.face = face;
        this.name = name;
        this.issuer = issuer;
    }

    /**
     * Returns human face related to the event
     * @return human face related to the event
     */
    public HumanFace getFace() {
        return face;
    }

    /**
     * Returns event name provided by issuer.
     * @return event name provided by issuer.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the object that triggered the event.
     * @return the object that triggered the event.
     */
    public Object getIssuer() {
        return issuer;
    }
}
