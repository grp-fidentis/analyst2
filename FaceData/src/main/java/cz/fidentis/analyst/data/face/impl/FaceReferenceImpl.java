package cz.fidentis.analyst.data.face.impl;

import cz.fidentis.analyst.data.face.FaceReference;

import java.io.File;

/**
 * @author Marek Seďa
 */
public class FaceReferenceImpl implements FaceReference {

    private final Long id;
    private final boolean isAverageFace;
    private final File file;
    private final String name;

    /**
     * Constructor.
     * @param id ID
     * @param isAverageFace average face indicator
     * @param file face file
     */
    public FaceReferenceImpl(Long id, boolean isAverageFace, File file) {
        this.id = id;
        this.isAverageFace = isAverageFace;
        this.file = file;
        this.name = file.getName();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public boolean isAverageFace() {
        return isAverageFace;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public File getPreviewFile() {
        return getFileWithSuffixIfExists("_preview.jpg");
    }

    @Override
    public File getFaceFile() {
        return getFileIfExists(file);
    }

    @Override
    public File getFeaturePointsFile() {
        return getFileWithSuffixIfExists("_landmarks.csv");
    }

    @Override
    public File getTextureFile() {
        return getFileWithSuffixIfExists(".jpg");
    }

    @Override
    public File getInfoFile() {
        return getFileWithSuffixIfExists("_info.csv");
    }

    private File getFileIfExists(File file) {
        return file.exists() ? file : null;
    }

    private File getFileWithSuffixIfExists(String suffix) {
        String expectedFile = file.getName().substring(0, file.getName().lastIndexOf("."));
        expectedFile = expectedFile.concat(suffix);
        return getFileIfExists(new File(file.getParentFile(), expectedFile));
    }

}
