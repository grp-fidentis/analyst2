package cz.fidentis.analyst.data.face.events;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;

/**
 * Some landmarks were added, removed, or shifted.
 *
 * @author Radek Oslejsek
 */
public class LandmarksChangedEvent extends HumanFaceEvent {

    /**
     * Constructor.
     *
     * @param face Human face related to the event
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public LandmarksChangedEvent(HumanFace face, String name, Object issuer) {
        super(face, name, issuer);
    }
}
