package cz.fidentis.analyst.data.face.impl;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.util.DefaultInstantiatorStrategy;
import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceMemoryManager;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.analyst.data.landmarks.impl.FeaturePointImpl;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.Plane;
import org.objenesis.strategy.StdInstantiatorStrategy;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

/**
 * A flyweight factory that creates and caches human faces. Faces are
 * stored in the memory until there is enough space in the Java heap. Then they 
 * are cached on disk automatically. The dumping strategy can be switched at run-time.
 * <p>
 * Currently, listeners registered to {@code HumanFace} are neither dumped nor recovered!
 * </p>
 * 
 * @author Radek Oslejsek
 */
public class HumanFaceMemoryManagerImpl implements HumanFaceMemoryManager {

    private static Kryo kryo;

    /**
     * Human faces currently being stored on disk.
     * <ul>
     * <li>Key = Face ID<li>
     * <li>Value = Dump file<li>
     * </ul>
     */
    private final Map<Long, File> dumpedFaces = new HashMap<>();

    /**
     * Human faces currently being allocated in the memory.
     * <ul>
     * <li>Key = Last access time (milliseconds)<li>
     * <li>Value = Human face<li>
     * </ul>
     */
    private final SortedMap<Long, HumanFace> inMemoryFaces = new TreeMap<>();

    /**
     * The usage "table"
     * <ul>
     * <li>Key = Face ID<li>
     * <li>Value = Last access time (milliseconds)<li>
     * </ul>
     */
    private final Map<Long, Long> usage = new HashMap<>();

    /**
     * Dumping strategy.
     */
    private Strategy strategy = Strategy.LRU;

    /**
     * Constructor.
     */
    public HumanFaceMemoryManagerImpl() {
        synchronized (this) {
            if (HumanFaceMemoryManagerImpl.kryo == null) {
                HumanFaceMemoryManagerImpl.kryo = new Kryo();
                HumanFaceMemoryManagerImpl.kryo.setInstantiatorStrategy(new DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
                HumanFaceMemoryManagerImpl.kryo.register(HumanFace.class);
                HumanFaceMemoryManagerImpl.kryo.register(HumanFaceImpl.class);
                MeshFactory.registerClassesInKryo(kryo);
                HumanFaceMemoryManagerImpl.kryo.register(Box.class);
                HumanFaceMemoryManagerImpl.kryo.register(Plane.class);
                HumanFaceMemoryManagerImpl.kryo.register(Point3d.class);
                HumanFaceMemoryManagerImpl.kryo.register(Vector3d.class);
                HumanFaceMemoryManagerImpl.kryo.register(ArrayList.class);
                HumanFaceMemoryManagerImpl.kryo.register(HashMap.class);
                HumanFaceMemoryManagerImpl.kryo.register(String.class);
                HumanFaceMemoryManagerImpl.kryo.register(FeaturePointImpl.class);
                HumanFaceMemoryManagerImpl.kryo.register(MeshVicinity.class);
            }
        }
    }
    
    @Override
    public void setStrategy(Strategy strategy) {
        if (strategy == null) {
            throw new IllegalArgumentException("strategy");
        }
        this.strategy = strategy;
    }
    
    @Override
    public Strategy getStrategy() {
        return this.strategy;
    }

    @Override
    public Long loadFace(Long id, File file) {
        HumanFace face;
        try {
            face = new HumanFaceImpl(id, file); // read from file
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(HumanFaceMemoryManagerImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }    
        
        return addFace(face);
    }
    
    @Override
    public boolean isLoaded(Long faceId) {
        return usage.containsKey(faceId);
    }
    
    @Override
    public Long addFace(HumanFace face) {
        if (face == null) {
            return null;
        }
        
        Long faceId = face.getId();
        
        try {
            checkMemAndDump(); // free the memory, if necessary
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(HumanFaceMemoryManagerImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        // Update data structures, replace previous face:
        long time = getNanoTime();
        inMemoryFaces.put(time, face);
        usage.put(faceId, time);
        
        return faceId;
    }
    
    @Override
    public HumanFace getFace(Long faceId) {
        if (updateAccessTime(faceId)) { // in memory face
            return inMemoryFaces.get(usage.get(faceId));
        }
        
        File dumpFile = dumpedFaces.get(faceId);
        if (dumpFile == null) { // unknown face
            return null;
        }
         
        // Free memory and recover human face from dump file:
        HumanFace face;
        try {
            face = restoreFromFile(dumpFile); // recover face from disk
            checkMemAndDump(); // free the memory, if necessary
            //System.out.println(face.getShortName() + " recovered");
        } catch (IOException ex) {
            Logger.print("HumanFaceFactory ERROR: " + ex);
            //java.util.logging.Logger.getLogger(HumanFaceFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        // Update data structures:
        long time = getNanoTime();
        while (inMemoryFaces.containsKey(time)) { // wait until we get unique ms
            time = getNanoTime();
        }
        //dumpedFaces.remove(faceId);
        inMemoryFaces.put(time, face);
        usage.put(faceId, time);
        return face;
    }

    private long getNanoTime() {
        return System.currentTimeMillis() * 1000000 + System.nanoTime();
    }
    
    @Override
    public boolean removeFace(Long faceId) {
        if (usage.containsKey(faceId)) {
            inMemoryFaces.remove(usage.remove(faceId));
            dumpedFaces.remove(faceId);
            return true; // remove from memory;
        }
        
        return dumpedFaces.remove(faceId) != null;
    }
    
    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append(formatSize(presumableFreeMemory())).
                append(" Java heap memory available (out of ").
                append(formatSize(Runtime.getRuntime().maxMemory()));
        ret.append("). In memory: ").
                append(this.inMemoryFaces.size()).
                append(", dump files: ").
                append(this.dumpedFaces.size());
        return ret.toString();
    }
    
    /**
     * Updates last access time of the face allocated in the memory. 
     * Returns {@code false} if the face is not in the memory.
     * 
     * @param faceId Face to be updated
     * @return {@code false} if the face is not in the memory.
     */
    protected boolean updateAccessTime(Long faceId) {
        Long oldTime = usage.get(faceId);
        if (oldTime == null) {
            return false;
        }
        
        HumanFace face = inMemoryFaces.get(oldTime);
        
        long newTime = getNanoTime();
        while (inMemoryFaces.containsKey(newTime)) { // wait until we get unique ms
            newTime = getNanoTime();
        }
        inMemoryFaces.remove(oldTime);
        inMemoryFaces.put(newTime, face);
        usage.put(faceId, newTime);
        return true;
    }
    
    /**
     * Checks and releases the heap, if necessary.
     * 
     * @return true if some existing face has been dumped to free the memory.
     * @throws IOException on I/O error
     */
    protected int checkMemAndDump() throws IOException {
        int ret =  0;
        int counter = 0;
        while (counter++ < this.MAX_DUMP_FACES &&
                (double) presumableFreeMemory() / Runtime.getRuntime().maxMemory() <= MIN_FREE_MEMORY &&
                !inMemoryFaces.isEmpty()) {
            
            Long time = null;
            switch (strategy) {
                case MRU:
                    time = inMemoryFaces.lastKey();
                    break;
                default:
                case LRU:
                    time = inMemoryFaces.firstKey();
                    break;
            }
            
            if (time == null) { // no face in the memory
                break; 
            }
        
            HumanFace faceToDump = this.inMemoryFaces.remove(time);
            this.usage.remove(faceToDump.getId());
            dumpedFaces.put(faceToDump.getId(), dumpToFile(faceToDump));
            //System.out.println(faceToDump.getShortName() + " dumped");
        }
        
        return ret;        
    }
        
    protected static String formatSize(long v) {
        if (v < 1024) {
            return v + " B";
        }
        int z = (63 - Long.numberOfLeadingZeros(v)) / 10;
        return String.format("%.1f %sB", (double)v / (1L << (z*10)), " KMGTPE".charAt(z));
    }
    
    /**
     * https://stackoverflow.com/questions/12807797/java-get-available-memory
     * @return 
     */
    protected long presumableFreeMemory() {
        long allocatedMemory = (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory());
        return Runtime.getRuntime().maxMemory() - allocatedMemory;
    }
    
    protected File dumpToFile(HumanFace face) throws IOException {
        File tempFile = File.createTempFile(this.getClass().getSimpleName(), ".bin");
        tempFile.deleteOnExit();
        
        /*
        RandomAccessFile raf = new RandomAccessFile(tempFile, "rw");
        try (ObjectOutputStream fos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(raf.getFD())))) {
            fos.writeObject(this);
            fos.flush();
        }
        */
        
        try (Output out = new Output(new FileOutputStream(tempFile))) {
            kryo.writeObject(out, face);
        } catch(Exception ex) {
            throw new IOException(ex);
        }

        return tempFile;
    }
    
    protected static HumanFace restoreFromFile(File dumpFile) throws IOException {
        /*
        try (ObjectInputStream fos = new ObjectInputStream(new BufferedInputStream(new FileInputStream(dumpFile)))) {
            return (HumanFace) fos.readObject();
        }
        */
        
        try (Input in = new Input(new FileInputStream(dumpFile))) {
            return kryo.readObject(in, HumanFaceImpl.class);
        } catch(Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void clearFaces() {
        usage.clear();
        dumpedFaces.clear();
        inMemoryFaces.clear();
    }
}
