/**
 * This package contains interfaces and
 * classes necessary for publish-subscribe notification of changes in human faces.
 * The package includes the definition of specific events that are triggered 
 * when the human face is changed.
 */
package cz.fidentis.analyst.data.face.events;
