package cz.fidentis.analyst.data.face;

import java.io.File;
import java.io.Serializable;

/**
 * Class represents reference to HumanFace.
 * @author Marek Seďa
 */
public interface FaceReference extends Serializable {

    /**
     * Returns face ID
     * @return face ID
     */
    Long getId();

    /**
     * Returns face name
     * @return face name
     */
    String getName();

    /**
     * Returns preview file
     * @return preview file
     */
    File getPreviewFile();

    /**
     * Returns feature points file
     * @return feature points file
     */
    File getFeaturePointsFile();

    /**
     * Returns texture file
     * @return texture file
     */
    File getTextureFile();

    /**
     * Returns info file
     * @return info file
     */
    File getInfoFile();

    /**
     * Returns face OBJ file
     * @return face OBJ file
     */
    File getFaceFile();

    /**
     * Returns {@code true} if the face is an average face
     * @return {@code true} if the face is an average face
     */
    boolean isAverageFace();

}
