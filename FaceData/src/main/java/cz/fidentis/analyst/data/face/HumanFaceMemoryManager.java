package cz.fidentis.analyst.data.face;

import cz.fidentis.analyst.data.face.impl.HumanFaceMemoryManagerImpl;

import java.io.File;

/**
 * A flyweight factory that creates and caches human faces. Faces are
 * stored in the memory until there is enough space in the Java heap. Then they 
 * are cached on disk automatically. The dumping strategy can be switched at run-time.
 * <p>
 * Currently, listeners registered to {@code HumanFace} are neither dumped nor recovered!
 * </p>
 * 
 * @author Radek Oslejsek
 */
public interface HumanFaceMemoryManager {
    
    /**
     * Memory clean  up, i.e. dumping the file into disk and de-referencing it, is
     * very fast. Faster real objects removal by JVM. 
     * Therefore, the {@code checkMemAndDump()} method removes almost
     * {@code MAX_DUMP_FACES} at once.
     */
    int MAX_DUMP_FACES = 3;
    
    /**
     * Dumping strategies.
     * @author Radek Oslejsek
     */
    enum Strategy {
        LRU, // least recently used faces are dumped first
        MRU // most recently used faces are dumped first
    }
    
    /**
     * Keep at least this portion of the Java heap memory free
     */
    double MIN_FREE_MEMORY = 0.1; // 10%

    /**
     * Creates a new instance of the factory.
     * @return a new instance of the factory.
     */
    static HumanFaceMemoryManager create() {
        return new HumanFaceMemoryManagerImpl();
    }
    
    /**
     * Changes the dumping strategy
     * @param strategy Dumping strategy. Must not be {@code null}
     * @throws IllegalArgumentException if the strategy is missing
     */
    void setStrategy(Strategy strategy);
    
    /**
     * Returns current dumping strategy.
     * @return current dumping strategy
     */
    Strategy getStrategy();

    /**
     * Loads new face. If the face is already loaded, then the ID of existing instance
     * is returned. To access the human face instance, use {@link #getFace(String)}.
     * 
     * @param file OBJ file with human face.
     * @return ID of the human face or {@code null}
     */
    Long loadFace(Long id, File file);
    
    /**
     * Checks whether the given face is loaded in the factory
     * 
     * @param faceId OBJ file with human face.
     * @return {@code true} if the face exists in the factory, {@code false} otherwise.
     */
    boolean isLoaded(Long faceId);
    
    /**
     * Directly adds an existing face instance into the factory.
     * If a face with the same ID already exists, then it is replaced.
     * 
     * @param face Human face
     * @return ID of the human face or {@code null}
     */
    Long addFace(HumanFace face);
    
    /**
     * Returns a human face. Recovers the face from the disk if necessary.
     * 
     * @param faceId ID of the face
     * @return Human face or {@code null}
     */
    HumanFace getFace(Long faceId);
    
    /**
     * Removes the face from either memory or swap space. To re-initiate the face,
     * use {@link #loadFace(Long, java.io.File)} again.
     * 
     * @param faceId Face ID
     * @return true if the face existed and was removed.
     */
    boolean removeFace(Long faceId);

    /**
     * Removes all faces from memory and swap space
     */
    void clearFaces();
    
}
