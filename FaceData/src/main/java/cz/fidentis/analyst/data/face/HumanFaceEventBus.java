package cz.fidentis.analyst.data.face;

import cz.fidentis.analyst.data.face.impl.HumanFaceImpl;

/**
 * Changes in the human face and its data structures (e.g., mesh model, etc.)
 * can be monitored by listeners. Listeners have to implement the
 * {@link HumanFaceListener} interface and they have to be
 * registered using the {@link HumanFaceImpl#registerListener} method.
 * Then they are informed about changes in the human automatically via methods
 * prescribed by the interface.
 *
 * @author Radek Oslejsek
 * @author Matej Kovar
 * @author Katerina Zarska
 */
public interface HumanFaceEventBus {

    /**
     * Registers listeners (objects concerned in the human face changes) to receive events.
     * If listener is {@code null}, no exception is thrown and no action is taken.
     *
     * @param listener Listener concerned in the human face changes.
     */
    void registerListener(HumanFaceListener listener);

    /**
     * Unregisters listeners from receiving events.
     *
     * @param listener Registered listener
     */
    void unregisterListener(HumanFaceListener listener);

    /**
     * Broadcast event to registered listeners.
     *
     * @param evt Event to be triggered.
     */
    void announceEvent(HumanFaceEvent evt);

}
