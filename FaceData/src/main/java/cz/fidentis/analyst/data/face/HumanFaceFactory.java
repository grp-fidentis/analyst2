package cz.fidentis.analyst.data.face;

import cz.fidentis.analyst.data.face.impl.HumanFaceImpl;
import cz.fidentis.analyst.data.landmarks.LandmarksIO;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * This class provides services that instantiates a human face.
 *
 * @author Radek Oslejsek
 */
public interface HumanFaceFactory {

    /**
     * Reads a 3D human face from the given OBJ file.
     *
     * @param file OBJ file
     * @param loadLandmarks If {@code true}, then the constructor aims to load landmarks as well
     * @throws IOException on I/O failure
     */
    static HumanFace create(Long id, File file, boolean loadLandmarks) throws IOException {
        return new HumanFaceImpl(id, file, loadLandmarks);
    }

    /**
     * Reads a 3D human face from the given OBJ file.
     *
     * @param file OBJ file
     * @throws IOException on I/O failure
     */
    static HumanFace create(Long id, File file) throws IOException {
        return create(id, file, true);
    }

    /**
     * Recover human face from serialized dump file. As the dump file can contain additional
     * objects, e.g., camera, it is supposed that the given input stream is set to a human face object.
     *
     * @param in A dump file as object input stream
     * @return The instance of the human face
     * @throws IOException If reading fails
     * @throws ClassNotFoundException If there is no human face in the stream
     */
    static HumanFace create(ObjectInputStream in) throws IOException, ClassNotFoundException {
        return (HumanFace) in.readObject();
    }

    /**
     * Creates a human face from existing mesh model. The mesh model is stored directly (not copied).
     *
     * @param model Mesh model
     * @param id Canonical path to the OBJ file
     * @throws IllegalArgumentException if the {@code model} is {@code null}
     */
    static HumanFace create(MeshModel model, String fileId, Long id, boolean isAverageFace) {
        return new HumanFaceImpl(model, fileId, id, isAverageFace);
    }

    /**
     * Save face data by rewriting existing files!
     *
     * @param face Face
     * @throws IOException on IO error
     */
    static void save(HumanFace face) throws IOException {
        File dir = new File(face.getDirectory());
        File filename = new File(face.getShortName().split(".obj")[0]);
        saveAs(face, dir, filename);
    }

    /**
     * Save face by writing the data into a new files.
     *
     * @param face Face
     * @param dir Directory
     * @param filename Basic file name without suffixes
     * @throws IOException on IO error, especially if the files already exist
     */
    static void saveAs(HumanFace face, File dir, File filename) throws IOException {
        MeshIO.exportMeshModel(face.getMeshModel(), new File(dir, filename + ".obj"));
        if (face.hasLandmarks()) {
            LandmarksIO.exportFeaturePoints(
                    face.getAllLandmarks(),
                    dir.getPath(),
                    filename.toString(),
                    "CSV");
        }
    }
}
