package cz.fidentis.analyst.data.face.events;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;

/**
 * The root type for events relate to the changes in k-d tree.
 * 
 * @author Radek Oslejsek
 */
public class KdTreeEvent extends HumanFaceEvent {
    
    /**
     * Constructor.
     * @param face Human face related to the event
     * @param name Event name provided by issuer
     * @param issuer The issuer
     */
    public KdTreeEvent(HumanFace face, String name, Object issuer) {
        super(face, name, issuer);
    }

    /**
     * Returns {@code true} if the k-d tree is calculated.
     * @return {@code true} if the k-d tree is calculated.
     */
    public boolean isCalculated() {
        return false;
    }

}
