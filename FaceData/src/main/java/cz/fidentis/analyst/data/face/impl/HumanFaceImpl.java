package cz.fidentis.analyst.data.face.impl;

import com.google.common.eventbus.EventBus;
import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceEvent;
import cz.fidentis.analyst.data.face.HumanFaceListener;
import cz.fidentis.analyst.data.face.HumanFaceState;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.LandmarksIO;
import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.Glyph;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask;

import java.io.File;
import java.io.IOException;
import java.io.Serial;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A human face implementation.
 *
 * @author Radek Oslejsek
 * @author Matej Kovar
 * @author Katerina Zarska
 */
public class HumanFaceImpl implements HumanFace {

    @Serial
    private static final long serialVersionUID = 1L;

    private MeshModel meshModel;

    /**
     * {@code KdTree} is marked as transient because the Kryo library is not able
     * to handle the reference of {@code KdNode} to {@code MeshModel}s.
     */
    private transient KdTree kdTree;

    private transient Octree octree;

    private Box boundingBox;

    private Plane symmetryPlane;

    private List<Landmark> featurePoints;

    private final transient EventBus eventBus;

    private final Long id;

    private final String fileId;

    private final boolean isAverageFace;

    private transient SurfaceMask surfaceMask = new SurfaceMask();

    private List<Glyph> glyphs;

    /**
     * Reads a 3D human face from the given OBJ file.
     *
     * @param file OBJ file
     * @param loadLandmarks If {@code true}, then the constructor aims to load
     * landmarks along with the mesh. Use {@link #getAllLandmarks()} to check
     * whether the landmarks (feature points) has been loaded.
     * @throws IOException on I/O failure
     */
    public HumanFaceImpl(Long id, File file, boolean loadLandmarks) throws IOException {
        this.id = id;
        meshModel = MeshIO.readMeshModel(file);
        meshModel.simplifyModel();
        fileId = file.getCanonicalPath();
        isAverageFace = false;

        meshModel.estimateMissingVertexNormals();

        eventBus = new EventBus();

        if (loadLandmarks) {
            File landFile = this.findLandmarks();
            if (landFile != null) {
                try {
                    loadFeaturePoints(landFile.getAbsoluteFile().getParent(), landFile.getName());
                } catch(IOException ex) {
                    Logger.print(ex.toString());
                }
            }
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    /**
     * Reads a 3D human face from the given OBJ file.
     * Also loads landmarks (feature points) if appropriate file is found.
     *
     * @param file OBJ file
     * @throws IOException on I/O failure
     */
    public HumanFaceImpl(Long id, File file) throws IOException {
        this(id, file, true);
    }

    /**
     * Creates a human face from existing mesh model. The mesh model is
     * stored directly (not copied).
     *
     * @param model Mesh model
     * @param fileId Canonical path to the OBJ file
     * @param id Unique id
     * @throws IllegalArgumentException if the {@code model} is {@code null}
     */
    public HumanFaceImpl(MeshModel model, String fileId, Long id, boolean isAverageFace) {
        if (model == null) {
            throw new IllegalArgumentException("model");
        }
        if (id == null) {
            throw new IllegalArgumentException("id");
        }
        this.meshModel = model;
        this.id = id;
        this.fileId = fileId;
        this.isAverageFace = isAverageFace;
        eventBus = new EventBus();
    }

    /**
     * Non-public constructor necessary for the Kryo de-serialization
     */
    private HumanFaceImpl() {
        id = null;
        fileId = null;
        isAverageFace = false;
        eventBus = new EventBus();
    }

    /**
     * Returns the triangular mesh model of the human face.
     *
     * @return the triangular mesh model of the human face
     */
    public MeshModel getMeshModel() {
        return meshModel;
    }

    @Override
    public void setSymmetryPlane(Plane plane) {
        this.symmetryPlane = plane;
    }

    @Override
    public Plane getSymmetryPlane() {
        return symmetryPlane;
    }

    @Override
    public boolean hasSymmetryPlane() {
        return (symmetryPlane != null);
    }

    @Override
    public void loadFeaturePoints(String path, String fileName) throws IOException {
        featurePoints = LandmarksIO.importFeaturePoints(path, fileName);
    }

    @Override
    public List<Landmark> getAllLandmarks() {
        return featurePoints == null ? Collections.emptyList() : Collections.unmodifiableList(featurePoints);
    }
    
    @Override
    public List<Landmark> getStandardFeaturePoints() {
        return getAllLandmarks().stream()
                .filter(Landmark::isStandardFeaturePoint)
                .toList();
    }
    
    @Override
    public List<Landmark> getCustomLandmarks() {
        return getAllLandmarks().stream()
                .filter(Landmark::isUserDefinedLandmark)
                .toList();
    }

    @Override
    public void addCustomLandmark(Landmark landmark) {
        if (landmark == null) {
            return;
        }

        if (featurePoints == null) {
            featurePoints = new ArrayList<>();
        }

        featurePoints.add(landmark);
    }

    @Override
    public boolean removeCustomLandmark(Landmark landmark) {
        if (landmark == null || featurePoints == null) {
            return false;
        }

        return featurePoints.remove(landmark);
    }

    @Override
    public boolean hasLandmarks() {
        return featurePoints != null &&  !featurePoints.isEmpty();
    }

    @Override
    public String getPath() {
        return fileId;
    }

    @Override
    public String getShortName() {
        String name = fileId.substring(0, fileId.lastIndexOf('.')); // remove extension
        name = name.substring(name.lastIndexOf(File.separatorChar) + 1);
        return name;
    }

    @Override
    public String getDirectory() {
        return fileId.substring(0, getPath().indexOf(getShortName()));
    }

    @Override
    public Octree getOctree() {
        return this.octree;
    }

    @Override
    public boolean hasOctree() {
        return octree != null;
    }

    @Override
    public void setOctree(Octree octree) {
        this.octree = octree;
    }

    @Override
    public KdTree getKdTree() {
        return this.kdTree;
    }

    @Override
    public void setKdTree(KdTree kdTree) {
        this.kdTree = kdTree;
    }

    @Override
    public boolean hasKdTree() {
        return kdTree != null;
    }

    @Override
    public SurfaceMask getSurfaceMask() {
        return surfaceMask;
    }

    @Override
    public void registerListener(HumanFaceListener listener) {
        if (eventBus != null) { // eventBus is null when the class is deserialized!
            eventBus.register(listener);
        }
    }

    @Override
    public void unregisterListener(HumanFaceListener listener) {
        if (eventBus != null) { // eventBus is null when the class is deserialized!
            eventBus.unregister(listener);
        }
    }

    @Override
    public void announceEvent(HumanFaceEvent evt) {
        if (evt != null && eventBus != null) { // eventBus is null when the class is deserialized!
            eventBus.post(evt);
        }
    }

    @Override
    public HumanFaceState getState() {
        return new HumanFaceState(this);
    }

    @Override
    public void setState(HumanFaceState state) {
        this.meshModel = state.meshModel();
        this.symmetryPlane = state.symmetryPlane();
        this.kdTree = state.kdTree();
        this.octree = state.octree();
        if (this.featurePoints != null) {
            this.featurePoints.clear();
            this.featurePoints = state.featurePoints();
        }
        this.boundingBox = state.boundingBox();
        this.glyphs = state.glyphs();
    }

    @Override
    public List<Glyph> getGlyphs() {
        return glyphs == null ? null : Collections.unmodifiableList(glyphs);
    }

    @Override
    public void setGlyphs(List<Glyph> glyphs) {
        this.glyphs = glyphs;
    }

    @Override
    public boolean hasGlyphs() {
        return glyphs != null;
    }

    @Override
    public Box getBoundingBox() {
        return boundingBox;
    }

    @Override
    public void setBoundingBox(Box boundingBox) {
        this.boundingBox = boundingBox;
    }

    @Override
    public boolean hasBoundingBox() {
        return boundingBox != null;
    }

    @Override
    public void setSurfaceMask(SurfaceMask surfaceMask) {
        this.surfaceMask = surfaceMask;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HumanFaceImpl other = (HumanFaceImpl) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public boolean isAverageFace() {
        return isAverageFace;
    }

    /**
     * Tries to find a file with landmarks definition based on the name of the face's OBJ file.
     * @return The file with landmarks or {@code null}
     */
    protected File findLandmarks() {
        String filename = fileId.split(".obj")[0] + LANDMARK_FILE_SUFFIX;
        if ((new File(filename)).exists()) {
            return new File(filename);
        }

        filename = fileId.split("_ECA")[0] + LANDMARK_FILE_SUFFIX;
        if ((new File(filename)).exists()) {
            return new File(filename);
        }

        filename = fileId.split("_CA")[0] + LANDMARK_FILE_SUFFIX;
        if ((new File(filename)).exists()) {
            return new File(filename);
        }

        return null;
    }


}
