package cz.fidentis.analyst.data.face;


import cz.fidentis.analyst.data.face.impl.FaceServiceImpl;
import cz.fidentis.analyst.data.mesh.MeshModel;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Service used for manipulation with faces
 *
 * @author Marek Seďa
 */
public interface FaceService {

    FaceService INSTANCE = new FaceServiceImpl();

    /**
     * Returns face based on given reference
     * @param reference reference
     * @return face based on given reference
     */
    HumanFace getFaceByReference(FaceReference reference);

    /**
     *
     * @param meshModel from which is HumanFace created
     * @return new instance of HumanFace which is not saved in HumanFaceMemoryManager
     */
    HumanFace createTemporaryInMemoryFace(MeshModel meshModel);

    /**
     *
     * @param reference of HumanFace that is copied
     * @return FaceReference of new independent copy of HumanFace
     */
    FaceReference createCopyOfFace(FaceReference reference);

    /**
     * Creates face from mesh model.
     *
     * @param meshModel mesh model
     * @param file Face file
     * @param isAverageFace Average face indicator
     * @return face reference
     * @throws IOException on failure
     */
    FaceReference createFaceFromMeshModel(MeshModel meshModel, File file, boolean isAverageFace) throws IOException;

    /**
     * Loads face from file.
     *
     * @param file File
     * @return face reference
     * @throws IOException on failure
     */
    FaceReference loadFace(File file) throws IOException;

    /**
     * Replace all stored faces
     * @param faces to be loaded
     * @throws IOException
     */
    void replaceFaces(List<HumanFace> faces) throws IOException;

    /**
     * Determines if the face is in the memory.
     * @param reference Face reference
     * @return {@code true} if the face is in memory
     */
    boolean isInMemory(FaceReference reference);
    /**
     *
     * @param faceReference that is supposed to be deleted from HumanFaceMemoryManager
     * @return true when the HumanFace was successfully deleted from HumanFaceMemoryManager, false when FaceReference wasn't found etc.
     */
    boolean removeFace(FaceReference faceReference);

    /**
     * Load face into the memory
     * @param file Face file
     * @return Face
     * @throws IOException om failure
     */
    HumanFace loadTemporaryInMemoryFace(File file) throws IOException;

    /**
     * Sets dumping strategy
     * @param strategy Strategy
     */
    void setDumpStrategy(HumanFaceMemoryManager.Strategy strategy);

}
