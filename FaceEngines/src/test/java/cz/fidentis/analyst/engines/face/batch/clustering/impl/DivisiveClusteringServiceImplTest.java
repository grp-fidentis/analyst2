package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Sabrina Oralkova
 */
public class DivisiveClusteringServiceImplTest {

    @Test
    void fullDivisiveClusteringTestBasicData() {
        double[][] distances = new double[][]{{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<String> clusterNames = Arrays.asList("A", "B", "C", "D", "E");
        DivisiveClustererImpl clusteringService = new DivisiveClustererImpl(distances, clusterNames);

        ClusterNode rootNode = clusteringService.performClustering();

        Assertions.assertNotNull(rootNode);
        Assertions.assertNull(rootNode.getParent());
        Assertions.assertEquals(2, rootNode.getChildren().size());
        Assertions.assertNotNull(rootNode.getChildren().getFirst().getParent());
        Assertions.assertNotNull(rootNode.getChildren().get(1).getParent());
        Assertions.assertEquals(2, rootNode.getChildren().getFirst().getChildren().size());
        Assertions.assertEquals(2, rootNode.getChildren().getLast().getChildren().size());
        Assertions.assertEquals("D", rootNode.getChildren().getLast().getChildren().getFirst().getName());
        Assertions.assertEquals("B", rootNode.getChildren().getLast().getChildren().getLast().getName());
        Assertions.assertEquals("A", rootNode.getChildren().getFirst().getChildren().getLast().getName());
        Assertions.assertEquals("E", rootNode.getChildren().getFirst().getChildren().getFirst().getChildren().getFirst().getName());
        Assertions.assertEquals("C", rootNode.getChildren().getFirst().getChildren().getFirst().getChildren().getLast().getName());
        Assertions.assertEquals("cluster#0, cluster#1, cluster#3, E, C, A, cluster#2, D, B", rootNode.toString());
    }

    @Test
    void fullDivisiveClusteringTestForEmptyDistances() {
        double[][] distances = new double[][]{};
        List<String> clusterNames = Arrays.asList("A", "B", "C", "D", "E");
        DivisiveClustererImpl clusteringService = new DivisiveClustererImpl(distances, clusterNames);

        Assertions.assertThrows(IllegalArgumentException.class, clusteringService::performClustering);
    }

    @Test
    void fullDivisiveClusteringTestForEmptyDatasetAndNames() {
        double[][] distances = new double[][]{};
        List<String> clusterNames = new ArrayList<>();
        DivisiveClustererImpl clusteringService = new DivisiveClustererImpl(distances, clusterNames);

        clusteringService.performClustering();

        Assertions.assertEquals(0, clusterNames.size());
        Assertions.assertEquals(0, distances.length);
    }

    @Test
    void fullDivisiveClusteringTestForLargerDataset() {
        double[][] distances = new double[][]
                {
                        {0, 9, 3, 6, 11, 14, 5, 20, 6, 1, 9, 10},
                        {9, 0, 7, 5, 10, 13, 4, 9, 3, 8, 40, 1},
                        {3, 7, 0, 9, 2, 5, 6, 3, 7, 2, 7, 8},
                        {6, 5, 9, 0, 8, 11, 2, 6, 2, 7, 5, 6},
                        {11, 10, 2, 8, 0, 3, 8, 11, 7, 2, 10, 11},
                        {14, 13, 5, 11, 3, 4, 11, 14, 10, 5, 13, 14},
                        {5, 4, 6, 2, 8, 11, 0, 5, 1, 6, 4, 5},
                        {0, 9, 3, 6, 11, 14, 5, 0, 6, 1, 9, 10},
                        {6, 3, 7, 2, 7, 10, 1, 6, 0, 5, 3, 4},
                        {1, 8, 2, 7, 2, 5, 6, 1, 5, 0, 8, 9},
                        {9, 20, 7, 5, 10, 13, 4, 9, 3, 8, 0, 1},
                        {10, 1, 8, 6, 11, 14, 5, 10, 4, 9, 1, 10}
                };
        List<String> clusterNames = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
        DivisiveClustererImpl clusteringService = new DivisiveClustererImpl(distances, clusterNames);

        ClusterNode rootNode = clusteringService.performClustering();

        Assertions.assertEquals("cluster#0, cluster#1, cluster#3, cluster#5, cluster#7, I, G, D, cluster#6, " +
                "K, L, H, cluster#2, cluster#13, cluster#15, E, F, cluster#16, cluster#19, J, A, C, B", rootNode.toString());
    }


    @Test
    void fullDivisiveClusteringTestForDatasetWithNonZeroDiagonale() {
        double[][] distances = new double[][]
                {
                        {0, 9, 3, 6, 11, 14, 5, 5, 6, 1, 9, 10},
                        {9, 0, 7, 5, 10, 13, 4, 9, 3, 8, 1, 1},
                        {3, 7, 0, 9, 2, 5, 6, 3, 7, 2, 7, 8},
                        {6, 5, 9, 0, 8, 11, 2, 6, 2, 7, 5, 6},
                        {11, 10, 2, 8, 0, 3, 8, 11, 7, 2, 10, 11},
                        {14, 13, 5, 11, 3, 0, 11, 14, 10, 5, 13, 14},
                        {5, 4, 6, 2, 8, 11, 0, 5, 1, 6, 4, 5},
                        {5, 9, 3, 6, 11, 14, 5, 0, 6, 1, 9, 10},
                        {6, 3, 7, 2, 7, 10, 1, 6, 0, 5, 3, 4},
                        {1, 8, 2, 7, 2, 5, 6, 1, 5, 0, 8, 9},
                        {9, 1, 7, 5, 10, 13, 4, 9, 3, 8, 0, 1},
                        {10, 1, 8, 6, 11, 14, 5, 10, 4, 9, 1, 0}
                };
        List<String> clusterNames = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
        DivisiveClustererImpl clusteringService = new DivisiveClustererImpl(distances, clusterNames);

        ClusterNode rootNode = clusteringService.performClustering();

        Assertions.assertEquals("cluster#0, cluster#1, cluster#3, cluster#5, cluster#7, L, K, B, cluster#6, " +
                "cluster#11, I, G, D, cluster#4, H, A, cluster#2, cluster#17, cluster#19, J, C, E, F", rootNode.toString());
    }


    private static double[][] generateStaticMatrix(int size) {
        double[][] matrix = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = (i + j) % 100 + 1;
            }
        }
        return matrix;
    }

    private static List<String> generateClusterNames(int size) {
        return IntStream.range(0, size)
                .mapToObj(i -> "Cluster" + i)
                .collect(Collectors.toList());
    }

    @Test
    void fullDivisiveClusteringTestForLargeDataset() {

        double[][] distances = generateStaticMatrix(500);
        List<String> clusterNames = generateClusterNames(500);
        DivisiveClustererImpl clusteringService = new DivisiveClustererImpl(distances, clusterNames);

        ClusterNode rootNode = clusteringService.performClustering();

        Assertions.assertNotNull(rootNode);
        Assertions.assertEquals("cluster#0, cluster#1, cluster#3, cluster#5, cluster#7, cluster#9, cluster#11, " +
                "cluster#13, cluster#15, cluster#17, cluster#19, cluster#21, cluster#23, cluster#25, cluster#27, cluster#29, " +
                "cluster#31, cluster#33, cluster#35, cluster#37, cluster#39, cluster#41, cluster#43, cluster#45, cluster#47, " +
                "cluster#49, cluster#51, cluster#53, cluster#55, cluster#57, cluster#59, cluster#61, cluster#63, cluster#65, " +
                "cluster#67, cluster#69, cluster#71, cluster#73, cluster#75, cluster#77, cluster#79, cluster#81, cluster#83, " +
                "cluster#85, cluster#87, cluster#89, cluster#91, cluster#93, Cluster450, Cluster350, Cluster250, Cluster150, " +
                "Cluster50, Cluster451, Cluster351, Cluster251, Cluster151, Cluster51, Cluster460, Cluster360, Cluster461, " +
                "Cluster361, Cluster261, Cluster161, Cluster61, cluster#64, cluster#97, Cluster260, Cluster449, cluster#98, " +
                "cluster#101, Cluster160, Cluster349, cluster#102, cluster#105, Cluster60, Cluster249, cluster#106, cluster#109, " +
                "cluster#111, Cluster459, Cluster149, Cluster359, cluster#110, cluster#115, cluster#117, Cluster259, Cluster49, " +
                "Cluster159, cluster#116, cluster#121, cluster#123, Cluster458, Cluster448, Cluster358, cluster#122, cluster#127, " +
                "cluster#129, Cluster258, Cluster348, Cluster158, cluster#128, cluster#133, cluster#135, cluster#137, cluster#139, " +
                "cluster#141, Cluster457, Cluster248, Cluster357, Cluster257, cluster#138, cluster#145, cluster#147, cluster#149, " +
                "cluster#151, cluster#153, cluster#155, cluster#157, cluster#159, cluster#161, cluster#163, cluster#165, cluster#167, " +
                "cluster#169, cluster#171, cluster#173, cluster#175, cluster#177, cluster#179, cluster#181, cluster#183, cluster#185, " +
                "cluster#187, cluster#189, cluster#191, cluster#193, cluster#195, Cluster452, Cluster148, Cluster352, Cluster252, " +
                "Cluster152, Cluster52, Cluster453, Cluster353, Cluster253, Cluster153, Cluster53, Cluster454, Cluster354, Cluster254, " +
                "Cluster154, Cluster54, Cluster455, Cluster355, Cluster255, Cluster155, Cluster55, Cluster456, Cluster356, Cluster256, " +
                "Cluster156, Cluster56, Cluster157, Cluster57, Cluster58, Cluster59, Cluster482, Cluster382, Cluster282, Cluster182, " +
                "cluster#54, Cluster48, Cluster82, Cluster483, Cluster383, Cluster283, Cluster183, Cluster83, Cluster484, Cluster384, " +
                "Cluster284, Cluster184, Cluster84, Cluster485, Cluster385, Cluster285, Cluster185, Cluster85, Cluster486, Cluster386, " +
                "Cluster286, Cluster186, cluster#14, Cluster447, Cluster86, cluster#12, Cluster347, Cluster487, cluster#10, Cluster247, " +
                "Cluster387, cluster#8, Cluster147, Cluster287, cluster#6, Cluster47, Cluster187, cluster#4, cluster#211, Cluster481, " +
                "Cluster446, cluster#212, cluster#215, Cluster381, Cluster346, cluster#216, cluster#219, cluster#221, Cluster281, Cluster246, " +
                "cluster#222, cluster#225, Cluster181, Cluster146, cluster#226, cluster#229, Cluster480, Cluster46, cluster#230, cluster#233, " +
                "Cluster380, Cluster445, cluster#234, cluster#237, Cluster280, Cluster345, cluster#238, cluster#241, Cluster180, Cluster245, " +
                "cluster#242, cluster#245, Cluster80, Cluster145, cluster#246, cluster#249, cluster#251, Cluster479, Cluster45, Cluster379, " +
                "cluster#250, cluster#255, cluster#257, Cluster279, Cluster444, Cluster179, cluster#256, cluster#261, cluster#263, Cluster344, " +
                "Cluster478, Cluster79, cluster#262, cluster#267, cluster#269, Cluster378, Cluster244, Cluster278, cluster#268, cluster#273, " +
                "cluster#275, Cluster178, Cluster144, Cluster78, cluster#274, cluster#279, cluster#281, Cluster477, Cluster44, Cluster377, " +
                "cluster#280, cluster#285, cluster#287, Cluster277, Cluster443, Cluster177, cluster#286, cluster#291, cluster#293, Cluster476, " +
                "Cluster343, Cluster77, cluster#292, cluster#297, cluster#299, Cluster376, Cluster243, Cluster276, cluster#298, cluster#303, " +
                "cluster#305, Cluster176, Cluster143, Cluster76, cluster#304, cluster#309, cluster#311, Cluster475, Cluster43, Cluster375, " +
                "cluster#310, cluster#315, cluster#317, Cluster275, Cluster442, Cluster175, cluster#316, cluster#321, cluster#323, Cluster474, " +
                "Cluster342, Cluster75, cluster#322, cluster#327, cluster#329, Cluster374, Cluster242, Cluster274, cluster#328, cluster#333, " +
                "cluster#335, Cluster174, Cluster142, Cluster74, cluster#334, cluster#339, cluster#341, Cluster473, Cluster42, Cluster373, " +
                "cluster#340, cluster#345, cluster#347, cluster#349, Cluster273, Cluster441, Cluster173, cluster#348, cluster#353, cluster#355, " +
                "Cluster472, Cluster341, Cluster372, cluster#354, cluster#359, cluster#361, Cluster272, Cluster241, Cluster172, cluster#360, " +
                "cluster#365, cluster#367, Cluster471, Cluster141, Cluster72, cluster#366, cluster#371, cluster#373, Cluster371, Cluster41, " +
                "Cluster271, cluster#372, cluster#377, cluster#379, Cluster171, Cluster440, Cluster71, cluster#378, cluster#383, cluster#385, " +
                "cluster#387, Cluster470, Cluster340, Cluster370, Cluster270, cluster#384, cluster#391, cluster#393, cluster#395, Cluster240, " +
                "Cluster469, Cluster170, Cluster70, cluster#392, cluster#399, cluster#401, cluster#403, Cluster369, Cluster140, Cluster269, " +
                "Cluster169, cluster#400, cluster#407, cluster#409, cluster#411, Cluster468, Cluster40, Cluster368, Cluster69, cluster#408, " +
                "cluster#415, cluster#417, cluster#419, cluster#421, Cluster439, Cluster467, Cluster268, Cluster168, cluster#418, cluster#425, " +
                "cluster#427, Cluster367, Cluster339, Cluster267, Cluster68, cluster#416, cluster#431, cluster#433, cluster#435, cluster#437, " +
                "cluster#439, cluster#441, Cluster466, Cluster239, Cluster366, Cluster266, Cluster166, cluster#436, cluster#445, cluster#447, " +
                "Cluster465, Cluster139, Cluster66, Cluster167, cluster#434, cluster#451, cluster#453, cluster#455, cluster#457, cluster#459, " +
                "Cluster39, Cluster464, Cluster365, Cluster265, Cluster165, cluster#454, cluster#463, cluster#465, cluster#467, cluster#469, " +
                "Cluster364, Cluster438, Cluster264, Cluster164, Cluster64, Cluster65, cluster#452, cluster#473, cluster#475, cluster#477, " +
                "cluster#479, Cluster463, Cluster338, Cluster363, Cluster263, Cluster163, Cluster67, cluster#432, cluster#483, cluster#485, " +
                "cluster#487, cluster#489, Cluster462, Cluster238, Cluster362, Cluster262, Cluster63, Cluster73, cluster#346, cluster#493, " +
                "cluster#495, Cluster162, Cluster138, Cluster62, Cluster81, cluster#220, Cluster38, Cluster87, cluster#2, cluster#501, " +
                "cluster#503, cluster#505, cluster#507, cluster#509, cluster#511, cluster#513, cluster#515, cluster#517, cluster#519, " +
                "cluster#521, cluster#523, cluster#525, cluster#527, cluster#529, cluster#531, cluster#533, cluster#535, cluster#537, " +
                "cluster#539, cluster#541, cluster#543, cluster#545, cluster#547, cluster#549, cluster#551, cluster#553, cluster#555, " +
                "cluster#557, cluster#559, cluster#561, cluster#563, cluster#565, cluster#567, cluster#569, cluster#571, cluster#573, " +
                "cluster#575, cluster#577, cluster#579, cluster#581, cluster#583, cluster#585, cluster#587, cluster#589, cluster#591, " +
                "Cluster400, Cluster300, Cluster200, Cluster100, Cluster0, Cluster401, Cluster301, Cluster201, Cluster101, Cluster1, " +
                "Cluster410, Cluster310, Cluster411, Cluster311, Cluster211, Cluster111, Cluster11, cluster#562, cluster#595, Cluster210, " +
                "Cluster499, cluster#596, cluster#599, Cluster110, Cluster399, cluster#600, cluster#603, Cluster10, Cluster299, cluster#604, " +
                "cluster#607, cluster#609, Cluster409, Cluster199, Cluster309, cluster#608, cluster#613, cluster#615, Cluster209, Cluster99, " +
                "Cluster109, cluster#614, cluster#619, cluster#621, Cluster408, Cluster498, Cluster308, cluster#620, cluster#625, cluster#627, " +
                "Cluster208, Cluster398, Cluster108, cluster#626, cluster#631, cluster#633, cluster#635, cluster#637, cluster#639, Cluster407, " +
                "Cluster298, Cluster307, Cluster207, cluster#636, cluster#643, cluster#645, cluster#647, cluster#649, cluster#651, cluster#653, " +
                "cluster#655, cluster#657, cluster#659, cluster#661, cluster#663, cluster#665, cluster#667, cluster#669, cluster#671, cluster#673, " +
                "cluster#675, cluster#677, cluster#679, cluster#681, cluster#683, cluster#685, cluster#687, cluster#689, cluster#691, cluster#693, " +
                "Cluster402, Cluster198, Cluster302, Cluster202, Cluster102, Cluster2, Cluster403, Cluster303, Cluster203, Cluster103, Cluster3, Cluster404, " +
                "Cluster304, Cluster204, Cluster104, Cluster4, Cluster405, Cluster305, Cluster205, Cluster105, Cluster5, Cluster406, Cluster306, Cluster206, " +
                "Cluster106, Cluster6, Cluster107, Cluster7, Cluster8, Cluster9, Cluster432, Cluster332, Cluster232, Cluster132, cluster#552, Cluster98, Cluster32, " +
                "Cluster433, Cluster333, Cluster233, Cluster133, Cluster33, Cluster434, Cluster334, Cluster234, Cluster134, Cluster34, Cluster435, Cluster335, " +
                "Cluster235, Cluster135, Cluster35, Cluster436, Cluster336, Cluster236, Cluster136, cluster#512, Cluster497, Cluster36, cluster#510, Cluster397, " +
                "Cluster437, cluster#508, Cluster297, Cluster337, cluster#506, Cluster197, Cluster237, cluster#504, Cluster97, Cluster137, cluster#502, " +
                "cluster#709, Cluster431, Cluster496, cluster#710, cluster#713, Cluster331, Cluster396, cluster#714, cluster#717, cluster#719, Cluster231, " +
                "Cluster296, cluster#720, cluster#723, Cluster131, Cluster196, cluster#724, cluster#727, Cluster430, Cluster96, cluster#728, cluster#731, " +
                "Cluster330, Cluster495, cluster#732, cluster#735, Cluster230, Cluster395, cluster#736, cluster#739, Cluster130, Cluster295, cluster#740, " +
                "cluster#743, Cluster30, Cluster195, cluster#744, cluster#747, cluster#749, Cluster429, Cluster95, Cluster329, cluster#748, cluster#753, " +
                "cluster#755, Cluster229, Cluster494, Cluster129, cluster#754, cluster#759, cluster#761, Cluster394, Cluster428, Cluster29, cluster#760, " +
                "cluster#765, cluster#767, Cluster328, Cluster294, Cluster228, cluster#766, cluster#771, cluster#773, Cluster128, Cluster194, Cluster28, " +
                "cluster#772, cluster#777, cluster#779, Cluster427, Cluster94, Cluster327, cluster#778, cluster#783, cluster#785, Cluster227, Cluster493, " +
                "Cluster127, cluster#784, cluster#789, cluster#791, Cluster426, Cluster393, Cluster27, cluster#790, cluster#795, cluster#797, Cluster326, " +
                "Cluster293, Cluster226, cluster#796, cluster#801, cluster#803, Cluster126, Cluster193, Cluster26, cluster#802, cluster#807, cluster#809, " +
                "Cluster425, Cluster93, Cluster325, cluster#808, cluster#813, cluster#815, Cluster225, Cluster492, Cluster125, cluster#814, cluster#819, " +
                "cluster#821, Cluster424, Cluster392, Cluster25, cluster#820, cluster#825, cluster#827, Cluster324, Cluster292, Cluster224, cluster#826, " +
                "cluster#831, cluster#833, Cluster124, Cluster192, Cluster24, cluster#832, cluster#837, cluster#839, Cluster423, Cluster92, Cluster323, " +
                "cluster#838, cluster#843, cluster#845, cluster#847, Cluster223, Cluster491, Cluster123, cluster#846, cluster#851, cluster#853, Cluster422, " +
                "Cluster391, Cluster322, cluster#852, cluster#857, cluster#859, Cluster222, Cluster291, Cluster122, cluster#858, cluster#863, cluster#865, " +
                "Cluster421, Cluster191, Cluster22, cluster#864, cluster#869, cluster#871, Cluster321, Cluster91, Cluster221, cluster#870, cluster#875, " +
                "cluster#877, Cluster121, Cluster490, Cluster21, cluster#876, cluster#881, cluster#883, cluster#885, Cluster420, Cluster390, Cluster320, " +
                "Cluster220, cluster#882, cluster#889, cluster#891, cluster#893, Cluster290, Cluster419, Cluster120, Cluster20, cluster#890, cluster#897, " +
                "cluster#899, cluster#901, Cluster319, Cluster190, Cluster219, Cluster119, cluster#898, cluster#905, cluster#907, cluster#909, Cluster418, " +
                "Cluster90, Cluster318, Cluster19, cluster#906, cluster#913, cluster#915, cluster#917, cluster#919, Cluster489, Cluster417, Cluster218, " +
                "Cluster118, cluster#916, cluster#923, cluster#925, Cluster317, Cluster389, Cluster217, Cluster18, cluster#914, cluster#929, cluster#931, " +
                "cluster#933, cluster#935, cluster#937, cluster#939, Cluster416, Cluster289, Cluster316, Cluster216, Cluster116, cluster#934, cluster#943, " +
                "cluster#945, Cluster415, Cluster189, Cluster16, Cluster117, cluster#932, cluster#949, cluster#951, cluster#953, cluster#955, cluster#957, " +
                "Cluster89, Cluster414, Cluster315, Cluster215, Cluster115, cluster#952, cluster#961, cluster#963, cluster#965, cluster#967, Cluster314, " +
                "Cluster488, Cluster214, Cluster114, Cluster14, Cluster15, cluster#950, cluster#971, cluster#973, cluster#975, cluster#977, Cluster413, " +
                "Cluster388, Cluster313, Cluster213, Cluster113, Cluster17, cluster#930, cluster#981, cluster#983, cluster#985, cluster#987, Cluster412, " +
                "Cluster288, Cluster312, Cluster212, Cluster13, Cluster23, cluster#844, cluster#991, cluster#993, Cluster112, Cluster188, Cluster12, Cluster31, " +
                "cluster#718, Cluster88, Cluster37", rootNode.toString());
    }
}
