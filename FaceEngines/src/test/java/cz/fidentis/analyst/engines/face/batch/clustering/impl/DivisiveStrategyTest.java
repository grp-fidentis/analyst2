package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterDistanceMap;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterLink;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

/**
 * @author Sabrina Oralkova
 */
public class DivisiveStrategyTest {

    @Test
    void testIfClassCanBeInstantiated() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();

        Assertions.assertNotNull(divisiveStrategy);
    }

    @Test
    void testFurthestClusterNodesForNullLeftNodeNode() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap();
        clusterDistanceMap.add(new ClusterLink(new ClusterNode("leftNode"), new ClusterNode("rightNode"), 0.0));
        ClusterNode rightNode = new ClusterNode("rightNode");

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, null, rightNode);

        Assertions.assertNull(furthestNode);
    }

    @Test
    void testFurthestClusterNodesForNullRightNode() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap();
        ClusterNode leftNode = new ClusterNode("leftNode");
        clusterDistanceMap.add(new ClusterLink(leftNode, leftNode, 0.0));

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode, null);

        Assertions.assertNull(furthestNode);
    }


    @Test
    void testFurthestClusterNodesForSameNodes() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap();
        ClusterNode node = new ClusterNode("leftNode");
        clusterDistanceMap.add(new ClusterLink(node, node, 0.0));

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, node, node);

        Assertions.assertNull(furthestNode);
    }

    @Test
    void testFurthestClusterNodesForOneLink() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap();
        ClusterNode leftNode = new ClusterNode("leftNode");
        ClusterNode rightNode = new ClusterNode("rightNode");
        clusterDistanceMap.add(new ClusterLink(leftNode, rightNode, 0.0));

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode, rightNode);

        Assertions.assertNull(furthestNode);
    }

    @Test
    void testFurthestClusterNodesMapForSmallValues() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        leftNode.addChild(clusters.get(0));
        leftNode.addChild(clusters.get(1));
        rightNode.addChild(clusters.get(2));
        rightNode.addChild(clusters.get(3));
        rightNode.addChild(clusters.get(4));

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode,  rightNode);

        Assertions.assertEquals(clusters.get(0), furthestNode);
    }

    @Test
    void testFurthestClusterNodesMapForLargerValues() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        double[][] distances = new double[][] {{7, 79, 73, 67, 115}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        leftNode.addChild(clusters.get(0));
        leftNode.addChild(clusters.get(1));
        rightNode.addChild(clusters.get(2));
        rightNode.addChild(clusters.get(3));
        rightNode.addChild(clusters.get(4));

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode,  rightNode);

        Assertions.assertEquals(clusters.get(1), furthestNode);
    }

    @Test
    void testFurthestClusterNodesMapForZeroValues() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        double[][] distances = new double[][] {{0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        leftNode.addChild(clusters.getFirst());

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode,  rightNode);

        Assertions.assertNull(furthestNode);
    }

    @Test
    void testFurthestClusterNodesMapForZeroValuesAndLeftEmptyNode() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        double[][] distances = new double[][] {{0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        rightNode.addChild(clusters.getFirst());

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode,  rightNode);

        Assertions.assertNull(furthestNode);
    }

    @Test
    void testFurthestClusterNodesMapForMoreComplexNode() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        leftNode.addChild(clusters.get(0));
        leftNode.addChild(clusters.get(1));
        leftNode.addChild(clusters.get(2));
        leftNode.addChild(clusters.get(3));
        leftNode.addChild(clusters.get(4));

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode,  rightNode);

        Assertions.assertEquals(clusters.get(1), furthestNode);
    }

    @Test
    void testFurthestClusterNodesMapForNodeInBothNodes() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        leftNode.addChild(clusters.get(0));
        leftNode.addChild(clusters.get(2));
        leftNode.addChild(clusters.get(3));
        leftNode.addChild(clusters.get(4));
        rightNode.addChild(clusters.get(1));
        rightNode.addChild(clusters.get(3));

        ClusterNode furthestNode = divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode,  rightNode);

        Assertions.assertEquals(clusters.get(3), furthestNode);
    }

    @Test
    void testFurthestClusterNodesForEmptyMap() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap();
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        leftNode.addChild(clusters.get(0));
        rightNode.addChild(clusters.get(1));
        leftNode.addChild(clusters.get(2));
        leftNode.addChild(clusters.get(3));
        leftNode.addChild(clusters.get(4));

        Assertions.assertThrows(IllegalArgumentException.class, () -> divisiveStrategy.getFurthestClusterNode(clusterDistanceMap, leftNode,  rightNode));
    }

    @Test
    void testFurthestClusterNodesForNullMap() {
        DivisiveStrategy divisiveStrategy = new DivisiveStrategy();
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        var leftNode = new ClusterNode("leftNode");
        var rightNode = new ClusterNode("rightNode");
        leftNode.addChild(clusters.get(0));
        rightNode.addChild(clusters.get(1));
        leftNode.addChild(clusters.get(2));
        leftNode.addChild(clusters.get(3));
        leftNode.addChild(clusters.get(4));

        Assertions.assertThrows(IllegalArgumentException.class, () -> divisiveStrategy.getFurthestClusterNode(null, leftNode,  rightNode));
    }
}
