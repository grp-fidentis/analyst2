package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterDistanceMap;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

/**
 * @author Sabrina Oralkova
 */
public class DivisiveHierarchyBuilderTest {

    @Test
    void divideClustersForNull() {
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        DivisiveHierarchyBuilder divisiveHierarchyBuilder = new DivisiveHierarchyBuilder(clusterDistanceMap);

        Assertions.assertDoesNotThrow(() -> divisiveHierarchyBuilder.divideClusters(null));
    }

    @Test
    void divideClustersForEmptyNode() {
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        DivisiveHierarchyBuilder divisiveHierarchyBuilder = new DivisiveHierarchyBuilder(clusterDistanceMap);
        var node = new ClusterNode("A");

        divisiveHierarchyBuilder.divideClusters(node);
        Assertions.assertEquals(0, node.getChildren().size());
        Assertions.assertEquals("A", node.getName());
    }

    @Test
    void divideClustersForOneChild() {
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        DivisiveHierarchyBuilder divisiveHierarchyBuilder = new DivisiveHierarchyBuilder(clusterDistanceMap);
        var node = new ClusterNode("A");
        node.addChild(new ClusterNode("B"));

        Assertions.assertThrows(IllegalArgumentException.class, () -> divisiveHierarchyBuilder.divideClusters(node));
    }

    @Test
    void divideClustersForRootWithTwoChildren() {
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        DivisiveHierarchyBuilder divisiveHierarchyBuilder = new DivisiveHierarchyBuilder(clusterDistanceMap);
        var node = new ClusterNode("A");
        node.addChild(new ClusterNode("B"));
        node.addChild(new ClusterNode("C"));

        divisiveHierarchyBuilder.divideClusters(node);

        Assertions.assertEquals(2, node.getChildren().size());
        Assertions.assertEquals("A", node.getName());
        Assertions.assertEquals("C", node.getChildren().get(0).getName());
        Assertions.assertEquals("B", node.getChildren().get(1).getName());
        Assertions.assertEquals(0, node.getChildren().get(0).getChildren().size());
        Assertions.assertEquals(0, node.getChildren().get(1).getChildren().size());
        Assertions.assertEquals("A", node.getChildren().get(0).getParent().getName());
        Assertions.assertEquals("A", node.getChildren().get(1).getParent().getName());
    }

    @Test
    void divideClustersForComplexTree() {
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        DivisiveHierarchyBuilder divisiveHierarchyBuilder = new DivisiveHierarchyBuilder(clusterDistanceMap);
        var node = new ClusterNode("A");
        node.addChild(new ClusterNode("B"));
        node.addChild(new ClusterNode("C"));
        node.getChildren().get(0).addChild(new ClusterNode("D"));
        node.getChildren().get(0).addChild(new ClusterNode("E"));

        Assertions.assertThrows(IllegalArgumentException.class, () -> divisiveHierarchyBuilder.divideClusters(node));
    }

    @Test
    void divideClustersNodeWithManyChildren() {
        double[][] distances = new double[][] {{0, 9, 3, 6, 11}, {9, 0, 7, 5, 10}, {3, 7, 0, 9, 2}, {6, 5, 9, 0, 8}, {11, 10, 2, 8, 0}};
        List<ClusterNode> clusters = Stream.of("A", "B", "C", "D", "E")
                .map(ClusterNode::new)
                .toList();
        ClusterDistanceMap clusterDistanceMap = new ClusterDistanceMap(distances, clusters);
        DivisiveHierarchyBuilder divisiveHierarchyBuilder = new DivisiveHierarchyBuilder(clusterDistanceMap);
        var node = new ClusterNode("cluster#0");
        node.addChild(new ClusterNode("A"));
        node.addChild(new ClusterNode("B"));
        node.addChild(new ClusterNode("C"));
        node.addChild(new ClusterNode("D"));
        node.addChild(new ClusterNode("E"));

        divisiveHierarchyBuilder.divideClusters(node);

        Assertions.assertEquals(2, node.getChildren().size());
        Assertions.assertEquals("cluster#0", node.getName());
        Assertions.assertEquals("cluster#1", node.getChildren().get(0).getName());
        Assertions.assertEquals("cluster#2", node.getChildren().get(1).getName());
        Assertions.assertEquals("cluster#0", node.getChildren().get(0).getParent().getName());
        Assertions.assertEquals("cluster#0", node.getChildren().get(1).getParent().getName());
        Assertions.assertEquals(2, node.getChildren().get(0).getChildren().size());
        Assertions.assertEquals("cluster#3", node.getChildren().get(0).getChildren().get(0).getName());
        Assertions.assertEquals("A", node.getChildren().get(0).getChildren().get(1).getName());
        Assertions.assertEquals("cluster#1", node.getChildren().get(0).getChildren().get(0).getParent().getName());
        Assertions.assertEquals("cluster#1", node.getChildren().get(0).getChildren().get(1).getParent().getName());
        Assertions.assertEquals("E", node.getChildren().get(0).getChildren().get(0).getChildren().get(0).getName());
        Assertions.assertEquals("C", node.getChildren().get(0).getChildren().get(0).getChildren().get(1).getName());
        Assertions.assertEquals("cluster#3", node.getChildren().get(0).getChildren().get(0).getChildren().get(0).getParent().getName());
        Assertions.assertEquals("cluster#3", node.getChildren().get(0).getChildren().get(0).getChildren().get(1).getParent().getName());
        Assertions.assertEquals(2, node.getChildren().get(1).getChildren().size());
        Assertions.assertEquals("D", node.getChildren().get(1).getChildren().get(0).getName());
        Assertions.assertEquals("B", node.getChildren().get(1).getChildren().get(1).getName());
        Assertions.assertEquals("cluster#2", node.getChildren().get(1).getChildren().get(0).getParent().getName());
        Assertions.assertEquals("cluster#2", node.getChildren().get(1).getChildren().get(1).getParent().getName());
    }
}
