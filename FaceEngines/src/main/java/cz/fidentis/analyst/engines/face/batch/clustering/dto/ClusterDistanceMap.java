package cz.fidentis.analyst.engines.face.batch.clustering.dto;

import java.util.*;

/**
 * @author Patrik Tomov
 */
public class ClusterDistanceMap {

    private final Map<String, DistanceEntry> pairHash;
    private final PriorityQueue<DistanceEntry> priorityQueue;

    /**
     * Represents a pair of clusters and their distance.
     * @author Patrik Tomov
     */
    private class DistanceEntry implements Comparable<DistanceEntry> {
        final ClusterLink link;
        final String hash;
        private boolean removed = false;

        DistanceEntry(ClusterLink link) {
            this.link = link;
            this.hash = hashCodePair(link);
        }

        @Override
        public int compareTo(DistanceEntry other) {
            return link.compareTo(other.link);
        }
    }

    /**
     * Creates new instance of ClusterDistanceMap.
     */
    public ClusterDistanceMap() {
        this.priorityQueue = new PriorityQueue<>();
        this.pairHash = new HashMap<>();
    }

    /**
     * Creates new ClusterLinks for all clusters
     *
     * @param distances matrix of distances between cluster nodes
     * @param clusters cluster nodes
     */
    public ClusterDistanceMap(double[][] distances, List<ClusterNode> clusters) {
        this();
        if (distances.length != clusters.size() || Arrays
                .stream(distances)
                .map(x -> x.length)
                .anyMatch(x -> x != clusters.size())) {
            throw new IllegalArgumentException("Invalid distances matrix");
        }

        for (int i = 0; i < clusters.size(); i++) {
            for (int j = i + 1; j < clusters.size(); j++) {
                ClusterLink link = new ClusterLink(clusters.get(i), clusters.get(j), distances[i][j]);
                this.add(link);
            }
        }
    }

    public boolean isEmpty() {
        return priorityQueue.isEmpty();
    }

    /**
     * Returns list of cluster links.
     *
     * @return list of cluster links
     */
    public List<ClusterLink> list() {
        List<ClusterLink> links = new ArrayList<>(priorityQueue.size());
        for (DistanceEntry entry : priorityQueue) {
            links.add(entry.link);
        }
        return links;
    }

    /**
     * Returns cluster link between two clusters.
     *
     * @param cluster1 first cluster
     * @param cluster2 second cluster
     * @return cluster link
     */
    public ClusterLink findByClusters(ClusterNode cluster1, ClusterNode cluster2) {
        String code = hashCodePair(cluster1, cluster2);
        return pairHash.get(code).link;
    }

    /**
     * Removes the closest cluster link.
     *
     * @return closest cluster link
     */
    public ClusterLink removeClosest() {
        DistanceEntry closest = priorityQueue.poll();
        while (closest != null && closest.removed) {
            closest = priorityQueue.poll();
        }
        if (closest == null) {
            return null;
        }
        pairHash.remove(closest.hash);
        return closest.link;
    }

    /**
     * Removes cluster link.
     *
     * @param link cluster link
     */
    public void remove(ClusterLink link) {
        DistanceEntry entry = pairHash.remove(hashCodePair(link));
        if (entry == null) {
            return;
        }
        entry.removed = true;
    }

    /**
     * Adds cluster link.
     *
     * @param link cluster link
     * @return true if added, false otherwise
     */
    public boolean add(ClusterLink link) {
        DistanceEntry entry = new DistanceEntry(link);
        if (pairHash.containsKey(entry.hash)) {
            return false;
        }
        pairHash.put(entry.hash, entry);
        priorityQueue.add(entry);
        return true;
    }

    private String hashCodePair(ClusterLink link) {
        return hashCodePair(link.getLeftCluster(), link.getRightCluster());
    }

    private String hashCodePair(ClusterNode cluster1, ClusterNode cluster2) {
        String name1 = cluster1.getName();
        String name2 = cluster2.getName();
        return (name1.compareTo(name2) < 0) ? name1 + "~~~" + name2 : name2 + "~~~" + name1;
    }
}
