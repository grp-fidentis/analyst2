package cz.fidentis.analyst.engines.face.batch.clustering.dto;

/**
 * @author Patrik Tomov
 */
public class Distance implements Comparable<Distance> {

    private Double distance;

    /**
     * Creates new instance of Distance.
     */
    public Distance(Double setDistance) {
        distance = setDistance;
    }

    @Override
    public int compareTo(Distance otherDistance) {
        return distance == null ? 1 : distance.compareTo(otherDistance.getDistance());
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double setDistance) {
        distance = setDistance;
    }
}
