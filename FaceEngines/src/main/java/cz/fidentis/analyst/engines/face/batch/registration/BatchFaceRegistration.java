package cz.fidentis.analyst.engines.face.batch.registration;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshModel;

/**
 * N:N registration and/or the computation of the average face.
 *
 * @author Radek Oslejsek
 */
public interface BatchFaceRegistration {

    /**
     * Register and/or update (metamorphose) the template face.
     *
     * @param face Face to be registered and/or used to compute the average face.
     *             <b>Be aware that the registration changes the face's mesh geometry!</b>
     *             (position in space and possibly the size)
     */
    void register(HumanFace face);

    /**
     * Returns the average mesh obtained by the metamorphoses of the {@code template face} se in the constructor.
     * If the {@link #register} method does not compute the average face, then the original mesh of
     * the {@code template face} is returned.
     *
     * @return the average mesh obtained by the metamorphoses of the {@code template face} se in the constructor.
     */
    MeshModel getAverageMesh();
}
