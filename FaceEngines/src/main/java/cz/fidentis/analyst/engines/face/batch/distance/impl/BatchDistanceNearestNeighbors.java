package cz.fidentis.analyst.engines.face.batch.distance.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A class that computes similarity of a set of faces by computing
 * the distance of faces to an average (gauge) face and then combining these values
 * to get mutual similarity for all pairs. Euclidean distance to nearest neighbors
 * is used as the distance metric.
 * <p>
 * The computation is accelerated by using multiple CPU cores concurrently.
 * </p>
 *
 * @author Radek Oslejsek
 */
public abstract class BatchDistanceNearestNeighbors extends BatchIndirectDistanceImpl {

    /**
     * Constructor.
     *
     * @param gaugeFace A face serving as a "gauge". It is usually an average face of a set of faces.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     * @param distCache Distance cache
     */
    public BatchDistanceNearestNeighbors(HumanFace gaugeFace, int numFaces, DistCache<?> distCache) {
        super(gaugeFace, numFaces, distCache);
    }

    @Override
    public void addToMeasurement(HumanFace face) {
        FaceStateServices.updateKdTree(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

        MeshDistances dist = MeshDistanceServices.measure(
                getGaugeFace().getMeshModel(),
                new MeshDistanceConfig(
                        MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS,
                        face.getKdTree(),
                        true,
                        true)
        );
        getDistCache().addValuesOfFace(dist);

        // Memory is more valuable. And it's probable that we will not need
        // k-d trees anymore. If yes, then they are quickly re-computed.
        FaceStateServices.updateKdTree(face, FaceStateServices.Mode.DELETE);
    }

    /**
     * Cache of distances from the average face to other faces.
     *
     * @param <T> the type of cached value
     * @author Radek Oslejsek
     */
    public abstract static class DistCache<T> {
        private final List<List<T>> cache = new ArrayList<>();

        /**
         * Caches measured distances
         * @param meshDistances Measured distances
         */
        public abstract void addValuesOfFace(MeshDistances meshDistances);

        /**
         * Returns distance of two faces.
         *
         * @param faceIndex1 First face index
         * @param faceIndex2 Second face index
         * @param vertIndex Index of the vertex on the gauge face
         * @return distance
         */
        public abstract double getDistance(int faceIndex1, int faceIndex2, int vertIndex);

        /**
         * Returns the number of cached faces
         * @return the number of cached faces
         */
        public int getNumFaces() {
            return cache.size();
        }

        /**
         * Returns the number of cached vertices of the given faces
         * @return the number of cached vertices of the given faces
         */
        public int getNunFaceVertices(int faceIndex) {
            return cache.get(faceIndex).size();
        }

        protected List<List<T>> getCache() {
            return Collections.unmodifiableList(cache);
        }

        protected void add(List<T> list) {
            cache.add(list);
        }
    }

}
