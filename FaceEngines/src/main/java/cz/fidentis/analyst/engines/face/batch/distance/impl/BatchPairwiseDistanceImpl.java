package cz.fidentis.analyst.engines.face.batch.distance.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceVisitor;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.face.batch.distance.BatchPairwiseDistance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Pair-wise N:N distance measurement.
 *
 * @author Radek Oslejsek
 */
public class BatchPairwiseDistanceImpl implements BatchPairwiseDistance {

    private final HumanFace cropFace;

    private final double[][] distances; // AVG distance
    private final double[][] deviations; // sample standard deviation

    /**
     * Constructor.
     *
     * @param cropFace If not {@code null}, then only surfaces overlapping the {@code gaugeFace} are measured.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     */
    public BatchPairwiseDistanceImpl(HumanFace cropFace, int numFaces) {
        if (numFaces <= 0) {
            throw new IllegalArgumentException("numFaces");
        }
        distances = new double[numFaces][numFaces];
        deviations = new double[numFaces][numFaces];
        this.cropFace = cropFace;
    }

    @Override
    public void measure(HumanFace face1, HumanFace face2, int face1Index, int face2Index) {
        if (cropFace != null) {
            FaceStateServices.updateKdTree(cropFace, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        }
        FaceStateServices.updateKdTree(face1, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        FaceStateServices.updateKdTree(face2, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

        measureAndStoreOneDirection(face1, face2, face1Index, face2Index); // measure face2 to face1
        measureAndStoreOneDirection(face2, face1, face2Index, face1Index); // measure face1 to face2
    }

    @Override
    public double[][] getDistSimilarities() {
        return distances;
    }

    @Override
    public double[][] getDistDeviations() {
        return this.deviations;
    }

    protected void setDistSimilarity(int i, int j, double val) {
        this.distances[i][j] = val;
    }

    protected void setDistDeviation(int i, int j, double val) {
        this.deviations[i][j] = val;
    }

    /**
     * Measures {@code faceB} towards {@code faceA}. Omits that parts of the {@code faceB} that do not
     * overlap the crop face.
     *
     * @param faceA Face A
     * @param faceB Face B
     * @param indexA index of face A
     * @param indexB index of face B
     */
    protected void measureAndStoreOneDirection(HumanFace faceA, HumanFace faceB, int indexA, int indexB) {
        // Measure distance of faceB to the crop face to identify its non-overlapping parts with the crop face
        // (the distance is infinity)
        MeshDistanceVisitor cropDistVisitor = null;
        if (cropFace != null) { // crop
            cropDistVisitor = new MeshDistanceConfig(
                    MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS,
                    cropFace.getKdTree(),
                    false,
                    true).getVisitor();
            faceB.getMeshModel().compute(cropDistVisitor);
        }

        // Measure distance from faceB to faceA
        MeshDistanceVisitor dist = new MeshDistanceConfig(
                MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS,
                faceA.getKdTree(),
                false,
                true).getVisitor();
        faceB.getMeshModel().compute(dist);

        // Store a single-value (aggregated) measurement omitting non-overlaying parts of the faceB
        setDistValues(indexB, indexA, dist, cropDistVisitor);
    }

    /**
     * Sets an average distance and a sample standard deviation but only considering
     * vertices that are not copped either by the second face in the pairwise comparison
     * or the mean face (if defined).
     * @param i the first index
     * @param j the second index
     * @param pairwiseVisitor distance visitor
     * @param meanFaceVisitor mean face visitor
     */
    protected void setDistValues(int i, int j, MeshDistanceVisitor pairwiseVisitor, MeshDistanceVisitor meanFaceVisitor) {
        if (meanFaceVisitor == null) { // no cropping
            //setDistSimilarity(i, j, pairwiseVisitor.getStats().getMax());
            setDistSimilarity(i, j, pairwiseVisitor.getDistancesOfVisitedFacets().getDistanceStats().getAverage());
            setDistDeviation(i, j, pairwiseVisitor.getDistancesOfVisitedFacets().getSampleStandardDeviation());
            return;
        }

        // Auto crop to the average face:
        Map<MeshFacet, List<Double>> pairwiseMap = pairwiseVisitor.getDistancesOfVisitedFacets().distancesAsMap();
        Map<MeshFacet, List<Double>> meanFaceMap = meanFaceVisitor.getDistancesOfVisitedFacets().distancesAsMap();
        List<Double> values = new ArrayList<>();

        for (MeshFacet facet: pairwiseMap.keySet()) {
            for (int k = 0; k < facet.getNumberOfVertices(); k++) {
                if (Double.isFinite(meanFaceMap.get(facet).get(k)) &&
                        Double.isFinite(pairwiseMap.get(facet).get(k))) {
                    values.add(pairwiseMap.get(facet).get(k));
                }
            }
        }

        double avg = values.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);

        double sum = values.stream()
                .mapToDouble(Double::doubleValue)
                .map(dist -> (dist - avg) * (dist- avg))
                .sum();

        double dev = Math.sqrt((1.0 / (values.size()-1)) * sum);

        setDistSimilarity(i, j, avg);
        //setDistSimilarity(i, j, values.stream()
        //        .mapToDouble(Double::doubleValue)
        //        .max()
        //        .orElse(Double.POSITIVE_INFINITY)
        //);
        setDistDeviation(i, j, dev);
    }
}
