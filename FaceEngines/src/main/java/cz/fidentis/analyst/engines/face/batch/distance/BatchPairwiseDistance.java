package cz.fidentis.analyst.engines.face.batch.distance;

import cz.fidentis.analyst.data.face.HumanFace;

/**
 * The results of N:N similarity measured directly between all pairs.
 *
 * @author Radek Oslejsek
 */
public interface BatchPairwiseDistance extends BatchDistance {

    /**
     * Measure two faces in both directions.
     *
     * @param face1 First face
     * @param face2 Second face
     * @param face1Index The index under which is the result of {@code face1->face2} measurement stored
     * @param face2Index The index under which is the result of {@code face2->face1} measurement stored
     */
    void measure(HumanFace face1, HumanFace face2, int face1Index, int face2Index);
}
