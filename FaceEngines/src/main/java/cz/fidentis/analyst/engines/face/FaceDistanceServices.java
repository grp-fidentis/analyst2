package cz.fidentis.analyst.engines.face;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.engines.face.impl.FaceDistanceServicesImpl;

import java.util.Map;

/**
 * Services for the computation of distance (similarity) between faces.
 *
 * @author Radek Oslejsek
 * @author Ondřej Bazala
 */
public interface FaceDistanceServices {

    /**
     * Strategies to combine distances from face A to B and vice versa into
     * a single-value similarity measurement.
     * See <a href="https://doi.org/10.1109/ICPR.1994.576361">A Modified Hausdorff Distance for Object Matching</a>
     * for more details.
     *
     * @author Radek Oslejsek
     */
    enum DistanceAggregation {
        /**
         * Standard Hausdorff distance, i.e., max(max, max)
         */
        HAUSDORFF,

        /**
         * Mean distance, i.e., (avg + avg) / 2
         */
        MEAN,

        /**
         * Modified Hausdorff distance, i.e., max(avg, avg), that performs best for object's identification.
         */
        MODIFIED_HAUSDORFF, // max(avg, avg)
        /**
         * Chamfer distance, i.e., (avg + avg)
         */
        CHAMFER
    }

    /**
     * Calculates distance between faces. Required space partitioning structures are automatically
     * computed, if needed.
     *
     * @param primaryFace   A face towards the measurement is performed
     * @param secondaryFace A face from which the measurement is performed
     * @param relativeDist  Compute relative distances instead of absolute
     * @param crop          If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *                      taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *                      (their distance is set to {@code NaN}).
     *                      This feature makes the distance computation more symmetric.
     *                      This parameter is used only for the nearest neighbors strategies.
     * @param distConfig    The strategy of distance measurement. Must not be {@code null}
     * @param gpuData Wrapper with data for GPU computing, can be {@code null} if you do not want to use GPU-based method.
     * @return distance values
     */
    static MeshDistances calculateDistance(HumanFace primaryFace,
                                           HumanFace secondaryFace,
                                           boolean relativeDist,
                                           boolean crop,
                                           MeshDistanceConfig.Method distConfig,
                                           MeshDistanceConfig.GPUData gpuData) {
        return new FaceDistanceServicesImpl().calculateDistance(primaryFace, secondaryFace, relativeDist, crop, distConfig, gpuData);
    }

    /**
     * Calculates the distances of mesh surfaces in both directions, i.e., from object A to B and vice versa,
     * to address possible asymmetry. Then, it combines the measurement into a single-value similarity
     * indicator.
     * <p>
     *     This method is slower that the {@link #calculateDistance} method, but is more precise and
     *     return final single-value measurement instead of the distances of individual mesh vertices.
     * </p>
     *
     * @param face1 First object to be measured
     * @param face2 Second object to be measured
     * @param relativeDist Compute relative distances instead of absolute
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the distance computation more symmetric.
     *             This parameter is used only for the nearest neighbors strategies.
     * @param strategy The strategy of distance measurement. Must not be {@code null}
     * @param gpuData Wrapper with data for GPU computing, can be {@code null} if you do not want to use GPU-based method.
     * @param distanceAggregation The strategy of combining two measurements (directions) into a single value.
     * @return the distance of the objects.
     */
    static double calculateBiDistance(HumanFace face1,
                                      HumanFace face2,
                                      boolean relativeDist,
                                      boolean crop,
                                      MeshDistanceConfig.Method strategy,
                                      MeshDistanceConfig.GPUData gpuData,
                                      FaceDistanceServices.DistanceAggregation distanceAggregation) {
        return new FaceDistanceServicesImpl().calculateBiDistance(face1, face2, relativeDist, crop, strategy, gpuData, distanceAggregation);
    }

    /**
     * Extends the distance measurement (typically retrieved by
     * the {@link #calculateDistance(HumanFace, HumanFace, boolean, boolean, MeshDistanceConfig.Method, MeshDistanceConfig.GPUData)} method)
     * with priority sphere layers. Previous layers are deleted.
     *
     * @param meshDistances   Distance measurement of the face (towards another face). Must not be {@code null}
     * @param prioritySpheres Priority spheres of the measured face. Must not be {@code null}
     * @return the distance measurement (input parameter) equipped with priority sphere layers.
     */
    static MeshDistances setPrioritySphereMask(MeshDistances meshDistances, Map<Landmark, Double> prioritySpheres) {
        return new FaceDistanceServicesImpl().setPrioritySphereMask(meshDistances, prioritySpheres);
    }

    /**
     * Computes and returns weights of landmarks based on weighted distance measurement
     *
     * @param meshDistances   Distance measurement of the face (towards another face) with weighted values,
     *                        e.g., priority spheres set by the {@link #setPrioritySphereMask(MeshDistances, Map)}.
     *                        Must not be {@code null}
     * @param prioritySpheres Landmarks and their impact (radius) to be checked. Typically, this is the same
     *                        parameter used for the {@link #setPrioritySphereMask(MeshDistances, Map)}.
     *                        Must not be {@code null}.
     * @return weights of landmarks stored in the {@code prioritySpheres}
     */
    static Map<Landmark, Double> getFeaturePointWeights(MeshDistances meshDistances, Map<Landmark, Double> prioritySpheres) {
        return new FaceDistanceServicesImpl().getFeaturePointWeights(meshDistances, prioritySpheres);
    }

}
