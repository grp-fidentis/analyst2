package cz.fidentis.analyst.engines.face.batch.registration.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshConfig;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshVisitor;
import cz.fidentis.analyst.engines.face.FaceRegistrationServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistration;
import cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationConfig;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;

import java.util.Objects;

import static cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices.RegistrationStrategy.ICP;
import static cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices.RegistrationStrategy.NONE;

/**
 * N:N registration and/or the computation of the average face.
 *
 * @author Radek Oslejsek
 */
public class BatchFaceRegistrationImpl implements BatchFaceRegistration {

    private final BatchFaceRegistrationConfig config;
    private final IcpConfig icpConfig;
    private final HumanFace templateFace;

    private AvgMeshVisitor avgFaceVisitor = null;

    /**
     * Constructor.
     *
     * @param templateFace A template face towards which other faces are registered and/or from which
     *                     the average face is computed. Be aware that if the average face computation is requested,
     *                     then the template face geometry (mesh) metamorphoses (<b>changes its geometry</b>)!
     *                     Therefore, use a clone if you need to preserve the original face.
     * @param config Parameter object for this service
     */
    public BatchFaceRegistrationImpl(HumanFace templateFace, BatchFaceRegistrationConfig config) {
        this.templateFace = Objects.requireNonNull(templateFace);
        this.config = config;

        if (config.regStrategy() == ICP) {
            PointSamplingConfig sampling = (config.icpSubsampling() == 0)
                    ? new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, config.icpSubsampling())
                    : new PointSamplingConfig(PointSamplingConfig.Method.RANDOM, config.icpSubsampling());
            this.icpConfig = new IcpConfig(
                    templateFace.getMeshModel(),
                    config.icpIterations(),
                    config.scale(),
                    config.icpError(),
                    sampling,
                    config.icpAutoCropSince());
        } else {
            this.icpConfig = null;
        }
    }

    @Override
    public void register(HumanFace face) {
        switch (config.regStrategy()) {
            case NONE -> {}
            case ICP -> FaceRegistrationServices.alignMeshes(face, icpConfig);
            case GPA -> FaceRegistrationServices.alignFeaturePoints(templateFace, face, config.scale());
            default -> throw new IllegalStateException("Unexpected value: " + config.regStrategy());
        }

        avgFaceVisitor = switch (config.avgFaceStrategy()) {
            case NONE -> null;
            case NEAREST_NEIGHBOURS -> computeAvgFaceNN(templateFace, face, avgFaceVisitor);
            case PROJECTION_CPU -> computeAvgFaceRT(templateFace, face, avgFaceVisitor);
            case PROJECTION_GPU -> computeAvgFaceRTGPU(templateFace, face, avgFaceVisitor);
        };
    }

    @Override
    public MeshModel getAverageMesh() {
        return (avgFaceVisitor == null) ? templateFace.getMeshModel() : avgFaceVisitor.getAveragedMeshModel();
    }

    protected AvgMeshVisitor computeAvgFaceNN(HumanFace initFace, HumanFace superimposedFace, AvgMeshVisitor avgFaceVisitor) {
        // If the face was moved by registration, then the spatial ordering structure was removed => create it
        // If no transformation was made, then you can use old structure, if exists
        FaceStateServices.updateKdTree(
                superimposedFace,
                config.regStrategy() == NONE ? FaceStateServices.Mode.COMPUTE_IF_ABSENT : FaceStateServices.Mode.COMPUTE_ALWAYS
        );

        AvgMeshVisitor ret = (avgFaceVisitor == null)
                ? (new AvgMeshConfig(initFace.getMeshModel(), null)).getNearestNeighborsVisitor()
                : avgFaceVisitor;
        superimposedFace.getKdTree().accept(ret);

        return ret;
    }

    protected AvgMeshVisitor computeAvgFaceRT(HumanFace initFace, HumanFace superimposedFace, AvgMeshVisitor avgFaceVisitor) {
        // If the face was moved by registration, then the spatial ordering structure was removed => create it
        // If no transformation was made, then you can use old structure, if exists
        FaceStateServices.updateOctree(
                superimposedFace,
                config.regStrategy() == NONE ? FaceStateServices.Mode.COMPUTE_IF_ABSENT : FaceStateServices.Mode.COMPUTE_ALWAYS
        );

        AvgMeshVisitor ret = (avgFaceVisitor == null)
                ? (new AvgMeshConfig(initFace.getMeshModel(), null)).getRayCastingVisitor()
                : avgFaceVisitor;
        superimposedFace.getOctree().accept(ret);

        return ret;
    }

    protected AvgMeshVisitor computeAvgFaceRTGPU(HumanFace initFace, HumanFace superimposedFace, AvgMeshVisitor avgFaceVisitor) {
        AvgMeshVisitor ret = (avgFaceVisitor == null)
                ? (new AvgMeshConfig(initFace.getMeshModel(), config.glContext())).getRayCastingGpuVisitor()
                : avgFaceVisitor;

        superimposedFace.getMeshModel().compute(ret);

        return ret;
    }
}
