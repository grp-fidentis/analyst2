package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterDistanceMap;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;


/**
 * Class for calculating the furthest node from a bunch of nodes.
 *
 * @author Sabrina Oralkova
 */
public class DivisiveStrategy {

    /**
     * Gets the furthest cluster node from a bunch.
     *
     * @param distanceMap map of distances between clusters
     * @param leftNode left node of the parent,
     * @param rightNode right node of the parent
     * @return node that will be moved from leftNode to rightNode for further splitting
     */
    public ClusterNode getFurthestClusterNode(ClusterDistanceMap distanceMap, ClusterNode leftNode, ClusterNode rightNode) {
        if (distanceMap == null || distanceMap.isEmpty()) {
            throw new IllegalArgumentException("Distance map is empty.");
        }
        double max = 0;
        ClusterNode furthestNode = null;
        if (leftNode == null) {
            return furthestNode;
        }

        for (ClusterNode node : leftNode.getChildren()) {
            double calculatedDistance = nodeDistancesMean(distanceMap, leftNode, rightNode, node);
            if (max < calculatedDistance) {
                max = calculatedDistance;
                furthestNode = node;
            }
        }
        return furthestNode;
    }

    private double nodeDistancesMean(ClusterDistanceMap distanceMap, ClusterNode leftNode, ClusterNode rightNode, ClusterNode primaryNode) {
        double leftNodeSum = nodeDistancesSum(distanceMap, leftNode, primaryNode);
        double rightNodeSum = nodeDistancesSum(distanceMap, rightNode, primaryNode);
        // minus one to subtract the primary node itself
        int leftNodeSize = leftNode.getChildren().size() - 1;
        int rightNodeSize = rightNode.getChildren().size();

        // left node should not be empty
        if (leftNodeSize <= 0) {
            return Double.NEGATIVE_INFINITY;
        }

        // The distances of all nodes are summarised to then be divided by size of the cluster.
        if (rightNodeSize == 0) {
            return leftNodeSum / leftNodeSize;
        }
        return leftNodeSum / leftNodeSize - rightNodeSum / rightNodeSize;
    }

    private double nodeDistancesSum(ClusterDistanceMap distanceMap, ClusterNode node, ClusterNode primaryNode) {
        return node.getChildren()
                .stream()
                .filter(child -> !primaryNode.getName().equals(child.getName()))
                .mapToDouble(child -> distanceMap.findByClusters(primaryNode, child)
                        .getLinkageDistance())
                .sum();
    }
}
