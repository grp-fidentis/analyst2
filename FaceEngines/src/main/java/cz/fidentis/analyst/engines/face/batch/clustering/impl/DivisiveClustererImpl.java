package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.Clusterer;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterDistanceMap;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sabrina Oralkova
 */
public class DivisiveClustererImpl extends AbstractClusterer implements Clusterer {

    /**
     * Constructor.
     *
     * @param distances        positive distances between cluster nodes
     * @param clusterNames     names of cluster nodes
     */
    public DivisiveClustererImpl(double[][] distances, List<String> clusterNames) {
        super(distances, clusterNames);
    }

    @Override
    public ClusterNode performClustering() {
        ClusterNode rootCluster = createInitialCluster();
        List<ClusterNode> individualClusters = createSingleClusters(this.getClusterNames(), rootCluster);
        ClusterDistanceMap linkages = new ClusterDistanceMap(this.getDistances(), individualClusters);

        (new DivisiveHierarchyBuilder(linkages)).divideClusters(rootCluster);
        return rootCluster;
    }

    private ClusterNode createInitialCluster() {
        ClusterNode rootCluster = new ClusterNode("cluster#0");
        this.getClusterNames().forEach(rootCluster::addLeafName);
        return rootCluster;
    }

    /**
     * Creates single ClusterNodes for every single value in list of cluster names
     *
     * @param clusterNames names of the values
     * @param rootCluster root cluster of our cluster tree
     * @return list of ClusterNodes of the cluster names
     */
    private List<ClusterNode> createSingleClusters(List<String> clusterNames, ClusterNode rootCluster) {
        List<ClusterNode> singleClusters = new ArrayList<>();
        for (String name : clusterNames) {
            ClusterNode cluster = new ClusterNode(name);
            cluster.addLeafName(name);
            cluster.setParent(rootCluster);

            rootCluster.addChild(cluster);
            singleClusters.add(cluster);
        }
        return singleClusters;
    }
}
