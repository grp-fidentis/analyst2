package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterDistanceMap;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;

/**
 * DivisiveHierarchyBuilder is responsible for splitting cluster and creating tree-like hierarchy between the nodes.
 *
 * @author Sabrina Oralkova
 */
public class DivisiveHierarchyBuilder {
    private final ClusterDistanceMap distanceMap;
    private final DivisiveStrategy divisiveStrategy;

    private final String name = "cluster#";
    private int clusterIndexCounter = 1;

    /**
     * Creates new instance of DivisiveHierarchyBuilder.
     * @param distanceMap  map of distances between clusters
     */
    public DivisiveHierarchyBuilder(ClusterDistanceMap distanceMap) {
        this.distanceMap = distanceMap;
        this.divisiveStrategy = new DivisiveStrategy();
    }

    /**
     * Divides one cluster recursively to two child clusters based on how further are they from each other.
     */
    public void divideClusters(ClusterNode clusterNode) {
        if (clusterNode == null || clusterNode.getChildren().isEmpty()) {
            return;
        }

        for (ClusterNode child : clusterNode.getChildren()) {
            if (!child.getChildren().isEmpty()) {
                throw new IllegalArgumentException("Cannot divide cluster with children.");
            }
        }

        if (clusterNode.getChildren().size() == 1) {
            removeNodeFromTree(clusterNode);
            return;
        }

        ClusterNode leftCluster = new ClusterNode(getClusterName());
        ClusterNode rightCluster = new ClusterNode(getClusterName());

        leftCluster.appendLeafNames(clusterNode.getLeafNames());
        for (ClusterNode node : clusterNode.getChildren()) {
            leftCluster.addChild(node);
        }

        ClusterNode furthestNode = getFurthestNode(clusterNode, rightCluster);

        do {
            moveToRightCluster(rightCluster, leftCluster, furthestNode);
            furthestNode = getFurthestNode(leftCluster, rightCluster);
        } while (furthestNode != null && leftCluster.getChildren().size() > 1);

        fixRelations(clusterNode, leftCluster, rightCluster);
        divideClusters(leftCluster);
        divideClusters(rightCluster);
    }

    private void removeNodeFromTree(ClusterNode clusterNode) {
        if (clusterNode == null) {
            throw new IllegalArgumentException("Cannot remove null node from the tree.");
        }
        if (clusterNode.getParent() == null) {
            throw new IllegalArgumentException("Cannot remove root node from the tree.");
        }
        connectClusters(clusterNode.getParent(), clusterNode.getChildren().getFirst());
        clusterNode.getParent().removeChild(clusterNode);
        clusterNode.setParent(null);
        clusterNode.removeChildren();
    }

    private void fixRelations(ClusterNode clusterNode, ClusterNode leftCluster, ClusterNode rightCluster) {
        clusterNode.removeChildren();
        connectClusters(clusterNode, leftCluster);
        connectClusters(clusterNode, rightCluster);
    }

    private void connectClusters(ClusterNode parentNode, ClusterNode childNode) {
        parentNode.addChild(childNode);
        childNode.setParent(parentNode);
    }

    private void moveToRightCluster(ClusterNode rightCluster, ClusterNode leftCluster, ClusterNode furthestNode) {
        if (furthestNode == null) {
            throw new IllegalArgumentException("Cannot find furthest node.");
        }
        if (rightCluster == null) {
            throw new IllegalArgumentException("Cannot find right cluster.");
        }
        if (leftCluster == null) {
            throw new IllegalArgumentException("Cannot find left cluster.");
        }

        rightCluster.addChild(furthestNode);
        rightCluster.addLeafName(furthestNode.getName());

        leftCluster.removeChild(furthestNode);
        leftCluster.removeLeafName(furthestNode.getName());
    }

    private ClusterNode getFurthestNode(ClusterNode leftCluster, ClusterNode rightCluster) {
        return divisiveStrategy.getFurthestClusterNode(distanceMap, leftCluster, rightCluster);
    }

    public String getClusterName() {
        return name + clusterIndexCounter++;
    }
}
