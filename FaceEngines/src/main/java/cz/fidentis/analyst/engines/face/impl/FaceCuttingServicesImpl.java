package cz.fidentis.analyst.engines.face.impl;

import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.shapes.Plane;

import javax.vecmath.Vector3d;

/**
 * Implementation of cutting services.
 *
 * @author Ondřej Bazala
 * @author Peter Conga
 */
public class FaceCuttingServicesImpl {

    /**
     * Creates a cutting plane from the symmetry plane of the face
     *
     * @param face A face with symmetry plane. Must not be {@code null}
     * @param direction A normal vector of the cutting plane determining its basic orientation (x, y, or z)
     * @return a cutting plane, {@code null} if the face has no symmetry plane.
     */
    public Plane fromSymmetryPlane(HumanFace face, Vector3d direction) {
        if (!face.hasSymmetryPlane()) {
            return null;
        }

        Plane copyOfSymmetryPlane = new Plane(face.getSymmetryPlane());
        if (copyOfSymmetryPlane.getNormal().x < 0) { // orient to the right-hand direction
            copyOfSymmetryPlane = copyOfSymmetryPlane.flip();
        }

        // rotate and shift symmetry plane so that its orientation matches orientation of
        // cutting plane from bbox and is positioned in the middle of bbox
        Plane rotatedSymmetryPlane;
        if (direction.x == 1) {
            rotatedSymmetryPlane = new Plane(copyOfSymmetryPlane);
        } else {
            FaceStateServices.updateBoundingBox(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            if (direction.y == 1) {
                copyOfSymmetryPlane.rotateOverZ();
                rotatedSymmetryPlane = new Plane(copyOfSymmetryPlane.getNormal(), face.getBoundingBox().midPoint().y);
            } else {
                copyOfSymmetryPlane.rotateOverY();
                rotatedSymmetryPlane = new Plane(copyOfSymmetryPlane.getNormal(), face.getBoundingBox().midPoint().z);
            }
        }

        return rotatedSymmetryPlane;
    }

    /**
     * Creates cutting plane from bounding box of face
     *
     * @param face A face. Must not be {@code null}
     * @param direction A normal vector of the cutting plane determining its orientation
     * @return a cutting plane
     */
    public Plane fromBoundingBox(HumanFace face, Vector3d direction) {
        FaceStateServices.updateBoundingBox(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        // compute plane distance based on direction
        double distance;
        if (direction.x == 1) {
            distance = face.getBoundingBox().midPoint().x;
        } else if (direction.y == 1) {
            distance = face.getBoundingBox().midPoint().y;
        } else {
            distance = face.getBoundingBox().midPoint().z;
        }

        return new Plane(direction, distance);
    }


}
