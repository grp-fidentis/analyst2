package cz.fidentis.analyst.engines.face.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.measurement.*;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceVisitor;
import cz.fidentis.analyst.engines.face.FaceDistanceServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Implementation of distance services.
 *
 * @author Ondřej Bazala
 */
public class FaceDistanceServicesImpl {

    /**
     * Calculates distance between faces. Required space partitioning structures are automatically
     * computed, if needed.
     *
     * @param primaryFace A face towards the measurement is performed
     * @param secondaryFace A face from which the measurement is performed
     * @param relativeDist Compute relative distances instead of absolute
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the distance computation more symmetric.
     *             This parameter is used only for the nearest neighbors strategies.
     * @param strategy The strategy of distance measurement. Must not be {@code null}
     * @param gpuData Wrapper with data for GPU computing, can be {@code null} if you do not want to use GPU-based method.
     * @return distance values
     */
    public MeshDistances calculateDistance(HumanFace primaryFace,
                                           HumanFace secondaryFace,
                                           boolean relativeDist,
                                           boolean crop,
                                           MeshDistanceConfig.Method strategy,
                                           MeshDistanceConfig.GPUData gpuData) {

        switch (Objects.requireNonNull(strategy)) {
            case POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, POINT_TO_POINT_NEAREST_NEIGHBORS ->
                    FaceStateServices.updateKdTree(primaryFace, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            case RAY_CASTING ->
                    FaceStateServices.updateOctree(primaryFace, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            case RAY_CASTING_GPU -> {}
            default -> throw new IllegalStateException("Unexpected value: " + Objects.requireNonNull(strategy));
        }

        MeshDistanceVisitor distVisitor = new MeshDistanceConfig(
                strategy,
                primaryFace.getKdTree(),
                primaryFace.getOctree(),
                gpuData,
                relativeDist,
                crop).getVisitor();

        secondaryFace.getMeshModel().compute(distVisitor);

        distVisitor.dispose();

        return distVisitor.getDistancesOfVisitedFacets();
    }

    /**
     * Calculates the distances of mesh surfaces in both directions, i.e., from object A to B and vice versa,
     * to address possible asymmetry. Then, it combines the measurement into a single-value similarity
     * indicator.
     * <p>
     *     This method is slower that the {@link #calculateDistance} method, but is more precise and
     *     return final single-value measurement instead of the distances of individual mesh vertices.
     * </p>
     *
     * @param face1 First object to be measured
     * @param face2 Second object to be measured
     * @param relativeDist Compute relative distances instead of absolute
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the distance computation more symmetric.
     *             This parameter is used only for the nearest neighbors strategies.
     * @param strategy The strategy of distance measurement. Must not be {@code null}
     * @param gpuData Wrapper with data for GPU computing, can be {@code null} if you do not want to use GPU-based method.
     * @param distanceAggregation The strategy of combining two measurements (directions) into a single value.
     * @return the distance of the objects.
     */
    public double calculateBiDistance(HumanFace face1,
                                      HumanFace face2,
                                      boolean relativeDist,
                                      boolean crop,
                                      MeshDistanceConfig.Method strategy,
                                      MeshDistanceConfig.GPUData gpuData,
                                      FaceDistanceServices.DistanceAggregation distanceAggregation) {

        MeshDistances dist1 = calculateDistance(face1, face2, relativeDist, crop, strategy, gpuData);
        MeshDistances dist2 = calculateDistance(face2, face1, relativeDist, crop, strategy, gpuData);
        return switch (distanceAggregation) {
            case HAUSDORFF -> Math.max(dist1.getDistanceStats().getMax(), dist2.getDistanceStats().getMax());
            case MEAN -> (dist1.getDistanceStats().getAverage() + dist2.getDistanceStats().getAverage()) / 2.0;
            case MODIFIED_HAUSDORFF -> Math.max(dist1.getDistanceStats().getAverage(), dist2.getDistanceStats().getAverage());
            case CHAMFER -> (dist1.getDistanceStats().getAverage() + dist2.getDistanceStats().getAverage());
        };
    }

    /**
     * Extends the distance measurement (typically retrieved by
     * the {@link #calculateDistance(HumanFace, HumanFace, boolean, boolean, MeshDistanceConfig.Method, MeshDistanceConfig.GPUData)} method)
     * with priority sphere layers. Previous layers are deleted.
     *
     * @param meshDistances Distance measurement of the face (towards another face). Must not be {@code null}
     * @param prioritySpheres Priority spheres of the measured face. Must not be {@code null}
     * @return the distance measurement (input parameter) equipped with priority sphere layers.
     */
    public MeshDistances setPrioritySphereMask(MeshDistances meshDistances, Map<Landmark, Double> prioritySpheres) {
        meshDistances.clearLayers();
        meshDistances.addLayer(new ZeroWeightsDecorator());
        prioritySpheres.forEach((featurePoint, featurePointRadius) ->
                meshDistances.addLayer(new PrioritySphereDecorator(featurePoint.getPosition(), featurePointRadius)));
        return meshDistances;
    }

    /**
     * Computes and returns weights of landmarks based on weighted distance measurement
     *
     * @param meshDistances Distance measurement of the face (towards another face) with weighted values,
     *                      e.g., priority spheres set by the {@link #setPrioritySphereMask(MeshDistances, Map)}.
     *                      Must not be {@code null}
     * @param prioritySpheres Landmarks and their impact (radius) to be checked. Typically, this is the same
     *                        parameter used for the {@link #setPrioritySphereMask(MeshDistances, Map)}.
     *                        Must not be {@code null}.
     * @return weights of landmarks stored in the {@code prioritySpheres}
     */
    public Map<Landmark, Double> getFeaturePointWeights(
            MeshDistances meshDistances, Map<Landmark, Double> prioritySpheres) {

        final Map<Landmark, Double> weightedDistSum = new HashMap<>();
        final Map<Landmark, Double> weightSum = new HashMap<>();

        for (FacetDistances faceDistances: meshDistances) {
            for (DistanceRecord rec: faceDistances) {
                if (Double.isFinite(rec.getDistance())) {
                    prioritySpheres.forEach((featurePoint, radius) -> {
                        double dist = featurePoint.getPosition().distance(rec.getFacetPoint().getPosition());
                        if (dist < radius) {
                            double fpWeight = 1 - dist / radius;
                            weightedDistSum.putIfAbsent(featurePoint, 0.0);
                            weightedDistSum.put(featurePoint, weightedDistSum.get(featurePoint) + dist * fpWeight);
                            weightSum.putIfAbsent(featurePoint, 0.0);
                            weightSum.put(featurePoint, weightSum.get(featurePoint) + fpWeight);
                        }
                    });
                }
            }
        }

        weightedDistSum.forEach(((featurePoint, value) ->
                weightedDistSum.put(featurePoint, weightedDistSum.get(featurePoint) / weightSum.get(featurePoint))
        ));

        return weightedDistSum;
    }
}
