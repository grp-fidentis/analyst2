package cz.fidentis.analyst.engines.face.batch.registration;

import com.jogamp.opengl.GLContext;

import static cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices.AverageFaceStrategy.PROJECTION_GPU;
import static cz.fidentis.analyst.engines.face.batch.registration.BatchFaceRegistrationServices.RegistrationStrategy.GPA;

/**
 * Configuration object for the batch registration service.
 *
 * @author Radek Oslejsek
 *
 * @param regStrategy Registration strategy. If {@code NONE}, then only the average face is computed.
 *                    It can be useful if the faces are already registered.
 * @param avgFaceStrategy Strategy of the average face metamorphose. If {@code NONE},
 *                        then the {@code templateFace} is not transformed into the average face.
 * @param scale If {@code true}, then the registered faces can be scaled during the registration.
 *              Otherwise, they are only rotated and translated.
 * @param icpSubsampling Subsampling strength (the number of samples).
 *                       This parameter is used only for {@code ICP} registration.
 * @param icpError Minimal error for {@code ICP} registration.
 *                 This parameter is used only for {@code ICP} registration.
 * @param icpIterations Maximum number of ICP iterations.
 *                      This parameter is used only for {@code ICP} registration.
 * @param icpAutoCropSince Use auto-crop feature since the given ICP iteration. Use zero to use it always.
 *                         This parameter is used only for {@code ICP} registration.
 * @param glContext OpenGl context. Must not be {@code null} for the {@code PROJECTION_GPU} average face strategy.
 *                  Is ignored for other strategies.
 */
public record BatchFaceRegistrationConfig(
        BatchFaceRegistrationServices.RegistrationStrategy regStrategy,
        BatchFaceRegistrationServices.AverageFaceStrategy avgFaceStrategy,
        boolean scale,
        int icpSubsampling,
        double icpError,
        int icpIterations,
        int icpAutoCropSince,
        GLContext glContext) {

    /**
     * Constructor.
     *
     * @param regStrategy Registration strategy. If {@code NONE}, then only the average face is computed.
     *                    It can be useful if the faces are already registered.
     * @param avgFaceStrategy Strategy of the average face metamorphose. If {@code NONE},
     *                        then the {@code templateFace} is not transformed into the average face.
     * @param scale If {@code true}, then the registered faces can be scaled during the registration.
     *              Otherwise, they are only rotated and translated.
     * @param icpSubsampling Subsampling strength (the number of samples).
     *                       This parameter is used only for {@code ICP} registration.
     * @param icpError Minimal error for {@code ICP} registration.
     *                 This parameter is used only for {@code ICP} registration.
     * @param icpIterations Maximum number of ICP iterations.
     *                      This parameter is used only for {@code ICP} registration.
     * @param icpAutoCropSince Use auto-crop feature since the given ICP iteration. Use zero to use it always.
     *                         This parameter is used only for {@code ICP} registration.
     * @param glContext OpenGl context. Must not be {@code null} for the {@code PROJECTION_GPU} average face strategy.
     *                  Is ignored for other strategies.
     */
    public BatchFaceRegistrationConfig {
        if (avgFaceStrategy == PROJECTION_GPU && glContext == null) {
            throw new IllegalArgumentException("glContext must be set for the " + PROJECTION_GPU + " strategy");
        }
    }

    /**
     * Simplified constructor for the GPA registration strategy.
     *
     * @param avgFaceStrategy Strategy of the average face metamorphose. If {@code NONE},
     *                        then the {@code templateFace} is not transformed into the average face.
     * @param scale If {@code true}, then the registered faces can be scaled during the registration.
     *              Otherwise, they are only rotated and translated.
     * @param glContext OpenGl context. Must not be {@code null}
     */
    public BatchFaceRegistrationConfig(BatchFaceRegistrationServices.AverageFaceStrategy avgFaceStrategy, boolean scale, GLContext glContext) {
        this(GPA, avgFaceStrategy, scale, 0, 0, 0, 0, glContext);
    }

    /**
     * Simplified constructor for the ICP registration strategy.
     *
     * @param avgFaceStrategy Strategy of the average face metamorphose. If {@code NONE},
     *                        then the {@code templateFace} is not transformed into the average face.
     * @param scale If {@code true}, then the registered faces can be scaled during the registration.
     *              Otherwise, they are only rotated and translated.
     * @param icpSubsampling Subsampling strength (the number of samples).
     * @param icpError Minimal error for {@code ICP} registration.
     * @param icpIterations Maximum number of ICP iterations.
     * @param icpAutoCropSince Use auto-crop feature since the given ICP iteration. Use zero to use it always.
     */
    public BatchFaceRegistrationConfig(BatchFaceRegistrationServices.AverageFaceStrategy avgFaceStrategy, boolean scale, int icpSubsampling, double icpError, int icpIterations, int icpAutoCropSince) {
        this(null, avgFaceStrategy, scale, icpSubsampling, icpError, icpIterations, icpAutoCropSince, null);
    }

    /**
     * Simplified constructor for the ICP registration strategy with {@code icpError} set to 0.05,
     * {@code icpIterations} set to 100, and {@code icpAutoCropSince} set to 1.
     *
     * @param avgFaceStrategy Strategy of the average face metamorphose. If {@code NONE},
     *                        then the {@code templateFace} is not transformed into the average face.
     * @param scale If {@code true}, then the registered faces can be scaled during the registration.
     *              Otherwise, they are only rotated and translated.
     * @param icpSubsampling Subsampling strength (the number of samples).
     */
    public BatchFaceRegistrationConfig(BatchFaceRegistrationServices.AverageFaceStrategy avgFaceStrategy, boolean scale, int icpSubsampling) {
        this(null, avgFaceStrategy, scale, icpSubsampling, 0.05, 100, 1, null);
    }
}
