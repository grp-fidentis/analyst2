package cz.fidentis.analyst.engines.face.batch.clustering;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;

/**
 * @author Patrik Tomov
 * @since  14.10.2024
 */
public interface Clusterer {

    /**
     * Performs clustering on given distances.
     * @return root node of dendrogram
     */
    ClusterNode performClustering();
}
