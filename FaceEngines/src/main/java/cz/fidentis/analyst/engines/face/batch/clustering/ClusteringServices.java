package cz.fidentis.analyst.engines.face.batch.clustering;

import cz.fidentis.analyst.engines.face.batch.clustering.impl.DivisiveClustererImpl;
import cz.fidentis.analyst.engines.face.batch.clustering.impl.AgglomerativeClustererImpl;

import java.util.List;

/**
 * Services used to cluster results of N:N batch processing.
 *
 * @author Patrik Tomov
 * @since  29.10.2024
 */
public class ClusteringServices {
    /**
     * Initiates and returns an object implementing an agglomerative clustering service.
     *
     * @param distances        distances between clusters
     * @param clusterNames     names of clusters
     * @param linkageStrategy  strategy for linking clusters
     * @return Clustering service implementation
     */
    public static Clusterer initAgglomerativeClusteringService(double[][] distances, List<String> clusterNames, LinkageStrategy linkageStrategy) {
        return new AgglomerativeClustererImpl(distances, clusterNames, linkageStrategy);
    }

    /**
     * Initiates and returns an object implementing a divisive clustering service.
     *
     * @param distances        distances between clusters
     * @param clusterNames     names of clusters
     * @return Clustering service implementation
     */
    public static Clusterer initDivisiveClusteringService(double[][] distances, List<String> clusterNames) {
        return new DivisiveClustererImpl(distances, clusterNames);
    }
}
