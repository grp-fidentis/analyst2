package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import java.util.List;

/**
 * @author Sabrina Oralkova
 */
public class AbstractClusterer {
    private final double[][] distances;
    private final List<String> clusterNames;

    /**
     * Constructor.
     *
     * @param distances        positive distances between cluster nodes
     * @param clusterNames     names of cluster nodes
     */
    protected AbstractClusterer(double[][] distances, List<String> clusterNames) {
        this.distances = distances;
        this.clusterNames = clusterNames;
    }

    protected List<String> getClusterNames() {
        return clusterNames;
    }

    protected double[][] getDistances() {
        return distances;
    }
}
