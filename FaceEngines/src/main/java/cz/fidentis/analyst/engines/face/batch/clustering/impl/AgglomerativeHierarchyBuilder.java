package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.LinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterDistanceMap;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterLink;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.Distance;

import java.util.ArrayList;
import java.util.List;

/**
 * AgglomerativeHierarchyBuilder is responsible for merging clusters and updating distances between them.
 * @author Patrik Tomov
 * @since  14.10.2024
 */
public class AgglomerativeHierarchyBuilder {

    private final ClusterDistanceMap distanceMap;
    private final List<ClusterNode> clusterNodes;
    private int clusterIndexCounter = 0;

    /**
     * Creates new instance of AgglomerativeHierarchyBuilder.
     * @param clusterNodes list of clusters
     * @param distanceMap  map of distances between clusters
     */
    public AgglomerativeHierarchyBuilder(List<ClusterNode> clusterNodes, ClusterDistanceMap distanceMap) {
        this.clusterNodes = clusterNodes;
        this.distanceMap = distanceMap;
    }

    /**
     * Merges two closest clusters and updates distances between them.
     * @param linkageStrategy strategy for merging clusters
     */
    public void mergeClusters(LinkageStrategy linkageStrategy) {
        ClusterLink closestLink = distanceMap.removeClosest();
        if (closestLink != null) {
            clusterNodes.remove(closestLink.getRightCluster());
            clusterNodes.remove(closestLink.getLeftCluster());

            ClusterNode leftCluster = closestLink.getLeftCluster();
            ClusterNode rightCluster = closestLink.getRightCluster();
            ClusterNode mergedCluster = closestLink.merge(++clusterIndexCounter);

            updateDistances(linkageStrategy, leftCluster, rightCluster, mergedCluster);
            clusterNodes.add(mergedCluster);
        }
    }

    private void updateDistances(LinkageStrategy linkageStrategy, ClusterNode leftCluster, ClusterNode rightCluster, ClusterNode mergedCluster) {
        for (ClusterNode existingCluster : clusterNodes) {
            ClusterLink linkToLeft = findClusterLink(existingCluster, leftCluster);
            ClusterLink linkToRight = findClusterLink(existingCluster, rightCluster);
            ClusterLink newLink = new ClusterLink();
            newLink.setLeftCluster(existingCluster);
            newLink.setRightCluster(mergedCluster);

            List<Distance> distances = new ArrayList<>();

            if (linkToLeft != null) {
                distances.add(new Distance(linkToLeft.getLinkageDistance()));
                distanceMap.remove(linkToLeft);
            }
            if (linkToRight != null) {
                distances.add(new Distance(linkToRight.getLinkageDistance()));
                distanceMap.remove(linkToRight);
            }

            Distance newDistance = linkageStrategy.calculateDistance(distances);
            newLink.setLinkageDistance(newDistance.getDistance());
            distanceMap.add(newLink);
        }
    }

    private ClusterLink findClusterLink(ClusterNode cluster1, ClusterNode cluster2) {
        return distanceMap.findByClusters(cluster1, cluster2);
    }

    public boolean isTreeComplete() {
        return clusterNodes.size() == 1;
    }

    /**
     * Returns the root cluster of the hierarchy.
     * @return root cluster
     */
    public ClusterNode getRootCluster() {
        if (!isTreeComplete()) {
            throw new IllegalStateException("Tree is not yet complete, no root cluster available.");
        }
        return clusterNodes.getFirst();
    }
}
