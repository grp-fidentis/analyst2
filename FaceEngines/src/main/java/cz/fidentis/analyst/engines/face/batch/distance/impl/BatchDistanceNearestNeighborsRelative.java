package cz.fidentis.analyst.engines.face.batch.distance.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.measurement.DistanceRecord;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;

/**
 * A class that computes similarity of a set of faces by computing
 * the distance of faces to an average (gauge) face and then combining these values
 * to get mutual similarity for all pairs. Euclidean distance to nearest neighbors
 * is used as the distance metric.
 * <p>
 * The computation is accelerated by using multiple CPU cores concurrently.
 * <b>Relative distances</b> from the gauge vertices to other vertices are cached and
 * combined to estimate mutual similarity.
 * </p>
 *
 * @author Radek Oslejsek
 */
public class BatchDistanceNearestNeighborsRelative extends BatchDistanceNearestNeighbors {

    /**
     * Constructor.
     *
     * @param gaugeFace A face serving as a "gauge". It is usually an average face of a set of faces.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     */
    public BatchDistanceNearestNeighborsRelative(HumanFace gaugeFace, int numFaces) {
        super(gaugeFace, numFaces, new RelativeDistCache());
    }

    /**
     * Cache of relative distances from the average face to other faces.
     *
     * @author Radek Oslejsek
     */
    protected static class RelativeDistCache extends DistCache<Double> {

        @Override
        public void addValuesOfFace(MeshDistances meshDistances) {
            if (meshDistances == null) {
                add(null);
            } else { // Store relative distances of vertices to the cache
                add(meshDistances.stream()
                        .mapToDouble(DistanceRecord::getDistance)
                        .boxed()
                        .toList());
            }
        }

        @Override
        public double getDistance(int faceIndex1, int faceIndex2, int vertIndex) {
            double d1 = getCache().get(faceIndex1).get(vertIndex);
            double d2 = getCache().get(faceIndex2).get(vertIndex);
            return (Double.isFinite(d1) && Double.isFinite(d2))
                    ? Math.abs(d1 - d2)
                    : Double.NaN;
        }
    }
}
