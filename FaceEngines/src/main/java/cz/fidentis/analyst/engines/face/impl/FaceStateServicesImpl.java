package cz.fidentis.analyst.engines.face.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.face.HumanFaceFactory;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.bbox.BoundingBoxConfig;
import cz.fidentis.analyst.engines.bbox.BoundingBoxServices;
import cz.fidentis.analyst.engines.curvature.CurvatureConfig;
import cz.fidentis.analyst.engines.curvature.CurvatureServices;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.face.FaceDistanceServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.glyphs.GlyphServices;
import cz.fidentis.analyst.engines.glyphs.GlyphsConfig;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.icp.IcpServices;
import cz.fidentis.analyst.engines.landmarks.LandmarkServices;
import cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceConfig;
import cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceServices;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryServices;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Collection;

/**
 * Services for a single human face.
 *
 * @author Radek Oslejsek
 */
public class FaceStateServicesImpl implements FaceStateServices {

    /**
     * Manages a k-d tree storing the mesh of given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    public void updateKdTree(HumanFace face, Mode mode) {
        switch (mode) {
            case RECOMPUTE_IF_PRESENT -> {
                if (face.hasKdTree()) {
                    face.setKdTree(KdTree.create(face.getMeshModel()));
                }
            }
            case COMPUTE_IF_ABSENT -> {
                if (!face.hasKdTree()) {
                    face.setKdTree(KdTree.create(face.getMeshModel()));
                }
            }
            case COMPUTE_ALWAYS -> face.setKdTree(KdTree.create(face.getMeshModel()));
            case DELETE -> face.setKdTree(null);
            default -> throw new IllegalStateException("Unexpected value: " + mode);
        }
    }

    /**
     * Manages an octree storing the mesh of given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    public void updateOctree(HumanFace face, Mode mode) {
        switch (mode) {
            case RECOMPUTE_IF_PRESENT -> {
                if (face.hasOctree()) {
                    face.setOctree(Octree.create(face.getMeshModel()));
                }
            }
            case COMPUTE_IF_ABSENT -> {
                if (!face.hasOctree()) {
                    face.setOctree(Octree.create(face.getMeshModel()));
                }
            }
            case COMPUTE_ALWAYS -> face.setOctree(Octree.create(face.getMeshModel()));
            case DELETE -> face.setOctree(null);
            default -> throw new IllegalStateException("Unexpected value: " + mode);
        }
    }

    /**
     * Manages bounding box of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    public void updateBoundingBox(HumanFace face, Mode mode) {
        switch (mode) {
            case RECOMPUTE_IF_PRESENT -> {
                if (face.hasBoundingBox()) {
                    face.setBoundingBox(BoundingBoxServices.compute(face.getMeshModel(), new BoundingBoxConfig()));
                }
            }
            case COMPUTE_IF_ABSENT -> {
                if (!face.hasBoundingBox()) {
                    face.setBoundingBox(BoundingBoxServices.compute(face.getMeshModel(), new BoundingBoxConfig()));
                }
            }
            case COMPUTE_ALWAYS -> face.setBoundingBox(BoundingBoxServices.compute(face.getMeshModel(), new BoundingBoxConfig()));
            case DELETE -> face.setBoundingBox(null);
            default -> throw new IllegalStateException("Unexpected value: " + mode);
        }
    }

    /**
     * Manages glyphs of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     * @param method Sub-sampling method
     * @param maxSamples Maximum number of glyphs
     */
    public void updateGlyphs(HumanFace face, Mode mode, PointSamplingConfig.Method method, int maxSamples) {
        var config = new GlyphsConfig(new PointSamplingConfig(method, maxSamples));

        switch (mode) {
            case RECOMPUTE_IF_PRESENT -> {
                if (face.hasGlyphs()) {
                    face.setGlyphs(GlyphServices.calculateGlyphs(face.getMeshModel(), config));
                }
            }
            case COMPUTE_IF_ABSENT -> {
                if (!face.hasGlyphs()) {
                    face.setGlyphs(GlyphServices.calculateGlyphs(face.getMeshModel(), config));
                }
            }
            case COMPUTE_ALWAYS -> face.setGlyphs(GlyphServices.calculateGlyphs(face.getMeshModel(), config));
            case DELETE -> face.setGlyphs(null);
            default -> throw new IllegalStateException("Unexpected value: " + mode);
        }
    }

    /**
     * Manages curvature values of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    public void updateCurvature(HumanFace face, Mode mode) {
        switch (mode) {
            case RECOMPUTE_IF_PRESENT -> {
                if (face.getMeshModel().hasCurvature()) {
                    CurvatureServices.computeAndSet(face.getMeshModel(), new CurvatureConfig());
                }
            }
            case COMPUTE_IF_ABSENT -> {
                if (!face.getMeshModel().hasCurvature()) {
                    CurvatureServices.computeAndSet(face.getMeshModel(), new CurvatureConfig());
                }
            }
            case COMPUTE_ALWAYS -> CurvatureServices.computeAndSet(face.getMeshModel(), new CurvatureConfig());
            case DELETE -> throw new UnsupportedOperationException();
            default -> throw new IllegalStateException("Unexpected value: " + mode);
        }
    }

    /**
     * Computes vicinity of all landmarks of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    public void updateLandmarksVicinity(HumanFace face, Mode mode) {
        switch (mode) {
            case RECOMPUTE_IF_PRESENT -> {
                if (face.getMeshModel().hasCurvature()) {
                    computeVicinity(face);
                }
            }
            case COMPUTE_IF_ABSENT -> {
                if (!face.getMeshModel().hasCurvature()) {
                    computeVicinity(face);
                }
            }
            case COMPUTE_ALWAYS -> computeVicinity(face);
            case DELETE -> face.getAllLandmarks().forEach(landmark -> landmark.setMeshVicinity(null));
            default -> throw new IllegalStateException("Unexpected value: " + mode);
        }
    }

    /**
     * Computes symmetry plane from mesh or landmarks.
     *
     * @param face Human face
     * @param mode Operation to be performed
     * @param meshSymmetryConfig Configuration of the mesh symmetry calculation (when needed).
     *                           If {@code null}, then the symmetry plane is computed from landmarks.
     */
    public void updateSymmetryPlane(HumanFace face, Mode mode, SymmetryConfig meshSymmetryConfig) {
        switch (mode) {
            case RECOMPUTE_IF_PRESENT -> {
                if (face.hasSymmetryPlane()) {
                    if (meshSymmetryConfig != null) {
                        face.setSymmetryPlane(SymmetryServices.estimate(face.getMeshModel(), meshSymmetryConfig));
                    } else {
                        face.setSymmetryPlane(LandmarkServices.computeSymmetryPlane(face.getAllLandmarks()));
                    }
                }
            }
            case COMPUTE_IF_ABSENT -> {
                if (!face.hasSymmetryPlane()) {
                    if (meshSymmetryConfig != null) {
                        face.setSymmetryPlane(SymmetryServices.estimate(face.getMeshModel(), meshSymmetryConfig));
                    } else {
                        face.setSymmetryPlane(LandmarkServices.computeSymmetryPlane(face.getAllLandmarks()));
                    }
                }
            }
            case COMPUTE_ALWAYS -> {
                if (meshSymmetryConfig != null) {
                    face.setSymmetryPlane(SymmetryServices.estimate(face.getMeshModel(), meshSymmetryConfig));
                } else {
                    face.setSymmetryPlane(LandmarkServices.computeSymmetryPlane(face.getAllLandmarks()));
                }
            }
            case DELETE -> face.setSymmetryPlane(null);
            default -> throw new IllegalStateException("Unexpected value: " + mode);
        }
    }

    /**
     * Measures the symmetry by reflecting the mesh over the symmetry plane, (optionally) registering the reflected mesh
     * to the original surface, and then measuring distances of the original mesh towards the reflected mesh
     * (excluding non-overlapping areas).
     *
     * @param face Human face <b>with symmetry plane</b> already computed
     * @param precision A value between 0 and 100.
     *                  Zero means fast measurement when no registration is performed (only reflection of the mesh).
     *                  100 means slow but precise measurement when ICP registration is used without subsampling.
     *                  The values between use ICP registration with certain level of subsampling.*
     * @return Measured distances between the original mesh and its clone reflected over the symmetry plane and
     * optionally registered using ICP. Non-overlapping areas are excluded from the measurement.
     */
    public MeshDistances measureSymmetry(HumanFace face, int precision) {
        Plane symmetryPlane = face.getSymmetryPlane();
        if (symmetryPlane == null || precision < 0 || precision > 100) {
            return null;
        }

        // flip the mesh over the symmetry plane
        MeshModel clone = MeshFactory.cloneMeshModel(face.getMeshModel());
        clone.getFacets().forEach(facet -> facet.getVertices().parallelStream()
                .forEach(v -> v.getPosition().set(symmetryPlane.reflectPointOverPlane(v.getPosition()))));
        HumanFace mirrorFace = HumanFaceFactory.create(clone, "tmpId"+ RandomStringUtils.randomAlphanumeric(8).toUpperCase(), -1L, false);

        if (precision > 0) { // perform registration
            IcpServices.transform(
                    mirrorFace.getMeshModel(),
                    new IcpConfig(
                            face.getMeshModel(),
                            precision, // max iterations
                            false,
                            0.01,
                            precision == 100 ?
                                    new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 0):
                                    new PointSamplingConfig(PointSamplingConfig.Method.RANDOM, 10 * precision),
                            1)
            );
        }

        return FaceDistanceServices.calculateDistance(
                mirrorFace,
                face,
                false,
                true,
                MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS,
                null);
    }

    protected void computeVicinity(HumanFace face) {
        updateKdTree(face, Mode.COMPUTE_IF_ABSENT);
        for (var landmark: face.getAllLandmarks()) {
            var config = new PointToSurfaceDistanceConfig(
                    PointToSurfaceDistanceConfig.Method.POINT_TO_VERTICES,
                    landmark.getPosition(),
                    true);
            var distInfo = PointToSurfaceDistanceServices.measure(face.getKdTree(), config);
            var vicinity = new MeshVicinity(
                    distInfo.getLeft(),
                    distInfo.getRight().values().stream()
                            .flatMap(Collection::stream)
                            .map(MeshPoint::getPosition)
                            .findAny()
                            .orElse(null));
            landmark.setMeshVicinity(vicinity);
        }
    }

}
