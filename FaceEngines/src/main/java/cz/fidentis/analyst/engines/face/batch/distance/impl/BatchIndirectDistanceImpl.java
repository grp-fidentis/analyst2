package cz.fidentis.analyst.engines.face.batch.distance.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.face.batch.distance.BatchIndirectDistance;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;

/**
 * An object that computes distance-based similarity of the set of faces.
 *
 * @author Radek Oslejsek
 */
public abstract class BatchIndirectDistanceImpl implements BatchIndirectDistance {

    /**
     * Cache of distances of individual vertices
     * distCache.get(i) = i-th face
     * distCache.get(i).get(j) = distance of j-th vertex of i-th face
     */
    private final BatchDistanceNearestNeighbors.DistCache<?> distCache;

    private final HumanFace gaugeFace;

    private final double[][] distances; // AVG distance
    private final double[][] deviations; // sample standard deviation

    /**
     * Constructor.
     *
     * @param gaugeFace A face serving as a "gauge". It is usually an average face of a set of faces.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     * @param distCache Distance cache
     */
    public BatchIndirectDistanceImpl(HumanFace gaugeFace, int numFaces, BatchDistanceNearestNeighbors.DistCache<?> distCache) {
        if (numFaces <= 0) {
            throw new IllegalArgumentException("numFaces");
        }
        distances = new double[numFaces][numFaces];
        deviations = new double[numFaces][numFaces];
        this.distCache = Objects.requireNonNull(distCache);
        this.gaugeFace = Objects.requireNonNull(gaugeFace);
    }

    @Override
    public abstract void addToMeasurement(HumanFace face);

    @Override
    public void measure() {
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        final List<Future<Void>> results = new ArrayList<>();

        for (int i = 0; i < distCache.getNumFaces(); i++) {
            for (int j = i; j < distCache.getNumFaces(); j++) {
                //assert(distCache.get(i).size() == distCache.get(i).size()); // should never happen

                final int fi = i;
                final int fj = j;

                // Compute average HD for the pair of faces in a separate threat:
                results.add(executor.submit(() -> {
                    if (fi == fj) {
                        setDistSimilarity(fi, fj, 0.0);
                        setDistSimilarity(fj, fi, 0.0);
                        setDistDeviation(fi, fj, 0.0);
                        setDistDeviation(fj, fi, 0.0);
                        return null;
                    }

                    //double max = Double.NEGATIVE_INFINITY;
                    double sum = 0.0;
                    int counter = 0;
                    for (int k = 0; k < distCache.getNunFaceVertices(fi); k++) {
                        double d = distCache.getDistance(fi, fj, k);
                        if (Double.isFinite(d)) {
                            sum += d;
                            counter++;
                        }
                    }
                    double avg = sum / counter;
                    double dev = Math.sqrt((1.0 / (counter-1)) * sum);
                    setDistSimilarity(fi, fj, avg);
                    setDistSimilarity(fj, fi, avg);
                    //setDistSimilarity(fi, fj, max);
                    //setDistSimilarity(fj, fi, max);
                    setDistDeviation(fi, fj, dev);
                    setDistDeviation(fj, fi, dev);
                    return null;
                }));
            }
        }

        executor.shutdown();
        while (!executor.isTerminated()){}
        try {
            for (Future<Void> res: results) {
                res.get(); // waits until all computations are finished
            }
        } catch (final InterruptedException | ExecutionException ex) {
            java.util.logging.Logger.getLogger(BatchDistanceNearestNeighbors.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public double[][] getDistSimilarities() {
        return distances;
    }

    @Override
    public double[][] getDistDeviations() {
        return this.deviations;
    }

    protected void setDistSimilarity(int i, int j, double val) {
        this.distances[i][j] = val;
    }

    protected void setDistDeviation(int i, int j, double val) {
        this.deviations[i][j] = val;
    }

    protected BatchDistanceNearestNeighbors.DistCache<?> getDistCache() {
        return distCache;
    }

    protected HumanFace getGaugeFace() {
        return gaugeFace;
    }
}
