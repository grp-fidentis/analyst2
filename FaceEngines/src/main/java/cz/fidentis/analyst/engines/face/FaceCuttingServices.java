package cz.fidentis.analyst.engines.face;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.face.impl.FaceCuttingServicesImpl;
import cz.fidentis.analyst.data.shapes.Plane;

import javax.vecmath.Vector3d;

/**
 * Services related to cutting planes and cross-section curves of a human faces.
 *
 * @author Radek Oslejsek
 * @author Ondřej Bazala
 * @author Peter Conga
 */
public interface FaceCuttingServices {

    /**
     * Creates a cutting plane from the symmetry plane of the face
     *
     * @param face A face with symmetry plane. Must not be {@code null}
     * @param direction A normal vector of the cutting plane determining its basic orientation (x, y, or z)
     * @return a cutting plane, {@code null} if the face has no symmetry plane.
     */
    static Plane fromSymmetryPlane(HumanFace face, Vector3d direction) {
        return new FaceCuttingServicesImpl().fromSymmetryPlane(face, direction);
    }

    /**
     * Creates cutting plane from bounding box of face
     *
     * @param face A face. Must not be {@code null}
     * @param direction A normal vector of the cutting plane determining its orientation
     * @return a cutting plane
     */
    static Plane fromBoundingBox(HumanFace face, Vector3d direction) {
        return new FaceCuttingServicesImpl().fromBoundingBox(face, direction);
    }
}
