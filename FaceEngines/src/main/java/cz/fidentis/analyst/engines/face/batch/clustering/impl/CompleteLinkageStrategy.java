package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.LinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.Distance;

import java.util.List;

/**
 * @author Patrik Tomov
 */
public class CompleteLinkageStrategy implements LinkageStrategy {
    @Override
    public Distance calculateDistance(List<Distance> distances) {
        double maxDistance = distances.stream()
                .mapToDouble(Distance::getDistance)
                .max()
                .orElse(0.0);
        return new Distance(maxDistance);
    }
}
