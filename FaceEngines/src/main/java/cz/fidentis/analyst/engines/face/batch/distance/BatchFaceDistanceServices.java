package cz.fidentis.analyst.engines.face.batch.distance;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.face.batch.distance.impl.*;

/**
 * Services for the computation of N:N distance (similarity) between faces.
 *
 * @author Radek Oslejsek
 */
public interface BatchFaceDistanceServices {

    /**
     * Strategies of computation
     *
     * @author Radek Oslejsek
     */
    enum DistanceStrategy {
        /**
         * The closest points are find by looking for nearest neighbors.
         * The mutual similarity is estimated by caching and combining relative
         * distances of measured faces to the gauge face.
         */
        NEAREST_NEIGHBORS_RELATIVE_DISTANCE,

        /**
         * The closest points are find by looking for nearest neighbors.
         * The mutual similarity is estimated by caching and combining
         * the nearest neighbors and then computing their Euclidean distance.
         */
        NEAREST_NEIGHBORS_DIRECT_DISTANCE,

        /**
         * The closest points are find by looking for nearest neighbors.
         * The mutual similarity is estimated by combining
         * the {@code NEAREST_NEIGHBORS_DIRECT_DISTANCE} and
         * {@code NEAREST_NEIGHBORS_RELATIVE_DISTANCE} strategies.
         */
        NEAREST_NEIGHBORS_COMBINED_DISTANCE,

        /**
         * Uses rat-casting to get distance
         */
        PROJECTION
    }

    /**
     * Initiates and returns an object that can be used to measure N:N similarity
     * indirectly through a gauge face in nearly linear time.
     *
     * @param strategy Similarity measurement strategy
     * @param gaugeFace A face service as a gauge. Usually it is an average face from batch registration.
     * @param numFaces Number of faces to be measured (excluding the gauge face)
     * @param context Active OpenGL context on which makeCurrent() can be called.
     *               If this parameter is not {@code null}, then GPU can be used for acceleration.
     *               Otherwise, CPU is used instead.
     * @return an object that is used to measure N:N similarity of {@code numFaces} faces step-by-step.
     */
    static BatchIndirectDistance initIndirectMeasurement(DistanceStrategy strategy, HumanFace gaugeFace, int numFaces, GLContext context) {
        return switch (strategy) {
            case NEAREST_NEIGHBORS_RELATIVE_DISTANCE -> new BatchDistanceNearestNeighborsRelative(gaugeFace, numFaces);
            case NEAREST_NEIGHBORS_DIRECT_DISTANCE -> new BatchDistanceNearestNeighborsDirect(gaugeFace, numFaces);
            case NEAREST_NEIGHBORS_COMBINED_DISTANCE -> new BatchDistanceNearestNeighborsCombined(gaugeFace, numFaces);
            case PROJECTION -> new BatchDistanceRayCasting(gaugeFace, numFaces, context);
        };
    }

    /**
     * Initiates and returns an object that can be used to measure N:N similarity
     * directly (between all pairs).
     *
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     * @return an object that is used to measure N:N similarity of {@code numFaces} faces step-by-step.
     */
    static BatchPairwiseDistance initDirectMeasurement(int numFaces) {
        return initDirectMeasurementWithCrop(null, numFaces);
    }

    /**
     * Initiates and returns an object that can be used to measure N:N similarity
     * directly (between all pairs). Moreover, this method allows to measure only
     * surface parts that overlay the given {@code cropFace} surface.
     *
     * @param cropFace If not {@code null}, then only surfaces overlapping the {@code gaugeFace} are measured.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     * @return an object that is used to measure N:N similarity of {@code numFaces} faces step-by-step.
     */
    static BatchPairwiseDistance initDirectMeasurementWithCrop(HumanFace cropFace, int numFaces) {
        return new BatchPairwiseDistanceImpl(cropFace, numFaces);
    }
}
