package cz.fidentis.analyst.engines.face.batch.clustering;

import cz.fidentis.analyst.engines.face.batch.clustering.dto.Distance;

import java.util.List;

/**
 *
 * @author Patrik Tomov
 */
public interface LinkageStrategy {

    /**
     * Calculates average distance between clusters.
     *
     * @param distances distances between clusters
     * @return single value distance between clusters
     */
    Distance calculateDistance(List<Distance> distances);
}
