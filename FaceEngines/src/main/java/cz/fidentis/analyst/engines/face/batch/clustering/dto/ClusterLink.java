package cz.fidentis.analyst.engines.face.batch.clustering.dto;

/**
 * @author Patrik Tomov
 */
public class ClusterLink implements Comparable<ClusterLink> {

    private ClusterNode leftCluster;
    private ClusterNode rightCluster;
    private Double linkageDistance;

    /**
     * Creates new instance of ClusterLink.
     */
    public ClusterLink() {
    }

    /**
     * Creates new instance of ClusterLink.
     *
     * @param leftCluster      left cluster
     * @param rightCluster     right cluster
     * @param linkageDistance  linkage distance
     */
    public ClusterLink(ClusterNode leftCluster, ClusterNode rightCluster, Double linkageDistance) {
        this.leftCluster = leftCluster;
        this.rightCluster = rightCluster;
        this.linkageDistance = linkageDistance;
    }

    @Override
    public int compareTo(ClusterLink other) {
        int result;
        if (other == null || other.getLinkageDistance() == null) {
            result = -1;
        } else if (getLinkageDistance() == null) {
            result = 1;
        } else {
            result = this.linkageDistance.compareTo(other.getLinkageDistance());
        }

        return result;
    }

    /**
     * Merges two clusters into one.
     *
     * @param clusterIndex index of the cluster
     * @return merged cluster
     */
    public ClusterNode merge(int clusterIndex) {
        ClusterNode mergedCluster = new ClusterNode("cluster#" + clusterIndex);
        mergedCluster.setDistance(new Distance(linkageDistance));
        mergedCluster.appendLeafNames(leftCluster.getLeafNames());
        mergedCluster.appendLeafNames(rightCluster.getLeafNames());
        mergedCluster.addChild(leftCluster);
        mergedCluster.addChild(rightCluster);
        leftCluster.setParent(mergedCluster);
        rightCluster.setParent(mergedCluster);
        return mergedCluster;
    }

    public ClusterNode getRightCluster() {
        return rightCluster;
    }

    public void setRightCluster(ClusterNode rightCluster) {
        this.rightCluster = rightCluster;
    }

    public ClusterNode getLeftCluster() {
        return leftCluster;
    }

    public void setLeftCluster(ClusterNode leftCluster) {
        this.leftCluster = leftCluster;
    }

    public Double getLinkageDistance() {
        return linkageDistance;
    }

    public void setLinkageDistance(Double linkageDistance) {
        this.linkageDistance = linkageDistance;
    }
}
