package cz.fidentis.analyst.engines.face;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.SymmetryConfig;
import cz.fidentis.analyst.engines.face.impl.FaceStateServicesImpl;

/**
 * Services for managing a state (internal elements) of a single human face.
 *
 * @author Radek Oslejsek
 */
public interface FaceStateServices {

    /**
     * Update strategy.
     *
     * @author Radek Oslejsek
     */
    enum Mode {
        RECOMPUTE_IF_PRESENT,
        COMPUTE_IF_ABSENT,
        COMPUTE_ALWAYS,
        DELETE
    }

    /**
     * Manages a k-d tree storing the mesh of given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    static void updateKdTree(HumanFace face, Mode mode) {
        new FaceStateServicesImpl().updateKdTree(face, mode);
    }

    /**
     * Manages an octree storing the mesh of given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    static void updateOctree(HumanFace face, Mode mode) {
        new FaceStateServicesImpl().updateOctree(face, mode);
    }

    /**
     * Manages bounding box of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    static void updateBoundingBox(HumanFace face, Mode mode) {
        new FaceStateServicesImpl().updateBoundingBox(face, mode);
    }

    /**
     * Manages glyphs of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     * @param method Sub-sampling method. Used only if new glyphs are computed.
     * @param maxSamples Maximum number of glyphs. Used only if new glyphs are computed.
     */
    static void updateGlyphs(HumanFace face, Mode mode, PointSamplingConfig.Method method, int maxSamples) {
        new FaceStateServicesImpl().updateGlyphs(face, mode, method, maxSamples);
    }

    /**
     * Manages curvature values of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    static void updateCurvature(HumanFace face, Mode mode) {
        new FaceStateServicesImpl().updateCurvature(face, mode);
    }

    /**
     * Manages vicinity of all landmarks of the given human face.
     *
     * @param face Human face
     * @param mode Operation to be performed
     */
    static void updateLandmarksVicinity(HumanFace face, Mode mode) {
        new FaceStateServicesImpl().updateLandmarksVicinity(face, mode);
    }

    /**
     * Computes symmetry plane from mesh or landmarks.
     *
     * @param face Human face
     * @param mode Operation to be performed
     * @param meshSymmetryConfig Configuration of the mesh symmetry calculation (when needed).
     *                           If {@code null}, then the symmetry plane is computed from landmarks.
     */
    static void updateSymmetryPlane(HumanFace face, Mode mode, SymmetryConfig meshSymmetryConfig) {
        new FaceStateServicesImpl().updateSymmetryPlane(face, mode, meshSymmetryConfig);
    }

    /**
     * Measures the symmetry by reflecting the mesh over the symmetry plane, (optionally) registering the reflected mesh
     * to the original surface, and then measuring distances of the original mesh towards the reflected mesh
     * (excluding non-overlapping areas).
     *
     * @param face Human face <b>with symmetry plane</b> already computed
     * @param precision A value between 0 and 100.
     *                  Zero means fast measurement when no registration is performed (only reflection of the mesh).
     *                  100 means slow but precise measurement when ICP registration is used without subsampling.
     *                  The values between use ICP registration with certain level of subsampling.*
     * @return Measured distances between the original mesh and its clone reflected over the symmetry plane and
     * optionally registered using ICP. Non-overlapping areas are excluded from the measurement.
     */
    static MeshDistances measureSymmetry(HumanFace face, int precision) {
        return new FaceStateServicesImpl().measureSymmetry(face, precision);
    }
}
