package cz.fidentis.analyst.engines.face.batch.clustering.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Patrik Tomov
 */
public class ClusterNode {

    private String name;
    private ClusterNode parent;
    private final List<ClusterNode> children;
    private final List<String> leafNames;
    private Distance distance;

    /**
     * Creates new instance of ClusterNode.
     *
     * @param name name of the node
     */
    public ClusterNode(String name) {
        this.name = name;
        this.leafNames = new ArrayList<>();
        this.children = new ArrayList<>();
    }

    /**
     * Adds leaf name to the node.
     *
     * @param leafName name of the leaf
     */
    public void addLeafName(String leafName) {
        this.leafNames.add(leafName);
    }

    /**
     * Appends list of leaf names to the node.
     *
     * @param names list of leaf names
     */
    public void appendLeafNames(List<String> names) {
        this.leafNames.addAll(names);
    }

    /**
     * Adds child to the node.
     *
     * @param child child node
     */
    public void addChild(ClusterNode child) {
        this.children.add(child);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParent(ClusterNode parent) {
        this.parent = parent;
    }

    public List<String> getLeafNames() {
        return leafNames;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public ClusterNode getParent() {
        return parent;
    }

    public List<ClusterNode> getChildren() {
        return children;
    }

    /**
     * Removes child from list of children
     *
     * @param child child to be removed
     */
    public void removeChild(ClusterNode child) {
        children.remove(child);
    }

    /**
     * Removes leaf name from list of leaf names
     *
     * @param leafName leaf name to be removed
     */
    public void removeLeafName(String leafName) {
        leafNames.remove(leafName);
    }

    /**
     * Removes all children from list of children
     */
    public void removeChildren() {
        children.clear();
    }

    @Override
    public String toString() {
        List<String> names = new ArrayList<>();
        inorderTraversal(this, names);
        return String.join(", ", names);
    }

    private void inorderTraversal(ClusterNode node, List<String> names) {
        if (node == null) {
            return;
        }
        names.add(node.name);
        for (ClusterNode child : node.getChildren()) {
            inorderTraversal(child, names);
        }
    }
}
