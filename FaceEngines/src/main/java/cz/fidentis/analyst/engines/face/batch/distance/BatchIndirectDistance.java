package cz.fidentis.analyst.engines.face.batch.distance;

import cz.fidentis.analyst.data.face.HumanFace;

/**
 * An object for measuring N:N similarity step-by-step indirectly via a gauge surface.
 *
 * @author Radek Oslejsek
 */
public interface BatchIndirectDistance extends BatchDistance {

    /**
     * Adds the face to the N:N measurement.
     *
     * @param face Face to be measured
     */
    void addToMeasurement(HumanFace face);

    /**
     * Computes pair-wise distance values.
     */
    void measure();
}
