package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.LinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.Distance;

import java.util.List;

/**
 * @author Patrik Tomov
 */
public class SingleLinkageStrategy implements LinkageStrategy {

    @Override
    public Distance calculateDistance(List<Distance> distances) {
        double minDistance = distances.stream()
                .mapToDouble(Distance::getDistance)
                .min()
                .orElse(0.0);
        return new Distance(minDistance);
    }
}
