package cz.fidentis.analyst.engines.face.batch.distance;

/**
 * The results of N:N similarity measurement.
 *
 * @author Radek Oslejsek
 */
public interface BatchDistance {

    /**
     * Returns computed 2D matrix of distance similarities or {@code null}
     * @return computed 2D matrix of distance similarities or {@code null}
     */
    double[][] getDistSimilarities();

    /**
     * Returns computed 2D matrix of sample standard deviations or {@code null}
     * @return computed 2D matrix of sample standard deviations or {@code null}
     */
    double[][] getDistDeviations();
}
