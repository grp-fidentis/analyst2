package cz.fidentis.analyst.engines.face;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.face.impl.FaceRegistrationServicesImpl;
import cz.fidentis.analyst.engines.landmarks.PrTransformation;
import cz.fidentis.analyst.math.Quaternion;

import javax.vecmath.Vector3d;

/**
 * Services for registering (mutually align) faces.
 *
 * @author Radek Oslejsek
 */
public interface FaceRegistrationServices {

    /**
     * Superimpose given face to the face included in the ICP configuration object.
     *
     * @param transformedFace A face to be transformed.
     * @param icpConfig ICP configuration
     */
    static void alignMeshes(HumanFace transformedFace, IcpConfig icpConfig) {
        new FaceRegistrationServicesImpl().alignMeshes(transformedFace, icpConfig);
    }

    /**
     * Transform the face "manually".
     *
     * @param face Face to be transformed.
     * @param rotation Rotation vector denoting the rotation angle around axes X, Y, and Z.
     * @param translation Translation vector denoting the translation in the X, Y, and Z direction.
     * @param scale Scale factor (1 = no scale).
     */
    static void transformFace(HumanFace face, Vector3d rotation, Vector3d translation, double scale) {
        new FaceRegistrationServicesImpl().transformFace(face, rotation, translation, scale);
    }

    /**
     * Transforms one face so that its symmetry plane fits the symmetry plane of the other face.
     * Symmetry planes must be computed in advance!
     *
     * @param staticFace a human face that remains unchanged
     * @param transformedFace a human face that will be transformed
     * @param preserveUpDir If {@code false}, then the object can be rotated around the target's normal arbitrarily.
     * @throws NullPointerException if the symmetry planes are missing.
     */
    static void alignSymmetryPlanes(HumanFace staticFace, HumanFace transformedFace, boolean preserveUpDir) {
        new FaceRegistrationServicesImpl().alignSymmetryPlanes(staticFace, transformedFace, preserveUpDir);
    }

    /**
     * Transforms the second face so that its feature points best fit the position of corresponding
     * feature points of the first face.
     *
     * @param staticFace a human face that remains unchanged
     * @param transformedFace a human face that will be transformed
     * @param scale Whether to scale faces as well
     */
    static PrTransformation alignFeaturePoints(HumanFace staticFace, HumanFace transformedFace, boolean scale) {
        return new FaceRegistrationServicesImpl().alignFeaturePoints(staticFace, transformedFace, scale);
    }

    /**
     * Transforms the whole plane, i.e., its normal and position.
     * TO DO: Remove or move to appropriate interface.
     *
     * @param plane plane to be transformed
     * @param rot rotation
     * @param translation translation
     * @param scale scale
     * @return transformed plane
     */
    @Deprecated
    static Plane transformPlane(Plane plane, Quaternion rot, Vector3d translation, double scale) {
        return new FaceRegistrationServicesImpl().transformPlane(plane,rot,translation,scale);
    }
}
