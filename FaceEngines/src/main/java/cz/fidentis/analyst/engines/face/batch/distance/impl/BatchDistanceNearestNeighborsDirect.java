package cz.fidentis.analyst.engines.face.batch.distance.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.measurement.DistanceRecord;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;

import javax.vecmath.Point3d;

/**
 * A class that computes similarity of a set of faces by computing
 * the distance of faces to an average (gauge) face and then combining these values
 * to get mutual similarity for all pairs. Euclidean distance to nearest neighbors
 * is used as the distance metric.
 * <p>
 * The computation is accelerated by using multiple CPU cores concurrently.
 * <b>Nearest neighbors</b> from the gauge vertices to other vertices are cached and
 * then their Euclidean distance is computed to estimate mutual similarity.
 * </p>
 *
 * @author Radek Oslejsek
 */
public class BatchDistanceNearestNeighborsDirect extends BatchDistanceNearestNeighbors {

    /**
     * Constructor.
     *
     * @param gaugeFace A face serving as a "gauge". It is usually an average face of a set of faces.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     */
    public BatchDistanceNearestNeighborsDirect(HumanFace gaugeFace, int numFaces) {
        super(gaugeFace, numFaces, new DirectionCache());
    }

    /**
     * Cache of vectors to the nearest neighbors, from which the final distance is computed.
     *
     * @author Radek Oslejsek
     */
    protected static class DirectionCache extends DistCache<Point3d> {

        @Override
        public void addValuesOfFace(MeshDistances meshDistances) {
            if (meshDistances == null) {
                add(null);
            } else {
                add(meshDistances.stream()
                        .map(DistanceRecord::getNearestNeighbor)
                        .map(mp -> mp == null ? null : mp.getPosition())
                        .toList());
            }
        }

        @Override
        public double getDistance(int faceIndex1, int faceIndex2, int vertIndex) {
            Point3d p1 = getCache().get(faceIndex1).get(vertIndex);
            Point3d p2 = getCache().get(faceIndex2).get(vertIndex);
            return (p1 != null && p2 != null)
                    ? p1.distance(p2)
                    : Double.NaN;
        }
    }
}
