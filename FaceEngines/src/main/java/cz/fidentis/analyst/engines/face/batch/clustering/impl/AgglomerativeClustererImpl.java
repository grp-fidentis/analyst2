package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.Clusterer;
import cz.fidentis.analyst.engines.face.batch.clustering.LinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterDistanceMap;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.ClusterNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Patrik Tomov
 */
public class AgglomerativeClustererImpl extends AbstractClusterer implements Clusterer {
    private final LinkageStrategy linkageStrategy;

    /**
     * Constructor.
     *
     * @param distances        distances between clusters
     * @param clusterNames     names of clusters
     * @param linkageStrategy  strategy for linking clusters
     */
    public AgglomerativeClustererImpl(double[][] distances, List<String> clusterNames, LinkageStrategy linkageStrategy) {
        super(distances, clusterNames);
        this.linkageStrategy = linkageStrategy;
    }

    @Override
    public ClusterNode performClustering() {
        List<ClusterNode> clusters = createInitialClusters(this.getClusterNames());
        ClusterDistanceMap linkages = new ClusterDistanceMap(this.getDistances(), clusters);

        AgglomerativeHierarchyBuilder builder = new AgglomerativeHierarchyBuilder(clusters, linkages);
        while (!builder.isTreeComplete()) {
            builder.mergeClusters(linkageStrategy);
        }
        return builder.getRootCluster();
    }

    private List<ClusterNode> createInitialClusters(List<String> clusterNames) {
        List<ClusterNode> clusters = new ArrayList<>();
        for (String name : clusterNames) {
            ClusterNode cluster = new ClusterNode(name);
            cluster.addLeafName(name);
            clusters.add(cluster);
        }
        return clusters;
    }
}
