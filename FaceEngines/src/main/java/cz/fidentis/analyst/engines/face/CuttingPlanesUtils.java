package cz.fidentis.analyst.engines.face;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.CrossSection2D;
import cz.fidentis.analyst.data.shapes.CrossSection3D;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.cut.CrossSectionConfig;
import cz.fidentis.analyst.engines.cut.CrossSectionServices;

import javax.vecmath.Point2d;
import javax.vecmath.Vector3d;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Utility functions for cutting planes. Have to be replaced with a manager component in the future.
 *
 * @author Ondřej Bazala
 * @author Peter Conga
 */
@Deprecated
public final class CuttingPlanesUtils {

    private CuttingPlanesUtils() {}

    /**
     * Returns a Map object containing current settings of cutting planes and parameters
     *
     * @param samplingStrength sampling strength to be used in computations
     * @param vectorDirection normal vector determining direction of cutting plane
     * @param cuttingPlaneConfigs plane configs with needed parameters
     *
     * @return Map object with settings of cutting planes and parameters
     */
    public static Map<String, Object> getConfigExport(Integer samplingStrength, Vector3d vectorDirection,
                                                      List<CuttingPlaneConfig> cuttingPlaneConfigs) {
        Map<String, Object> config = new HashMap<>();
        config.put("Sampling", samplingStrength);
        config.put("View", String.valueOf(vectorDirection));
        List<Map<String, Object>> planeConfigs = cuttingPlaneConfigs.stream()
                .map(CuttingPlanesUtils::getPlaneConfigExport)
                .toList();
        config.put("Planes", planeConfigs);

        return config;
    }

    /**
     * Returns a Map object containing current setting of cutting plane panel.
     *
     * @param config plane config with needed parameters
     *
     * @return Map object with plane configuration
     */
    private static Map<String, Object> getPlaneConfigExport(CuttingPlaneConfig config) {
        Map<String, Object> planeConfig = new HashMap<>();
        planeConfig.put("Plane_Type", config.type());
        planeConfig.put("Plane_Orientation", String.valueOf(config.vectorDirection()));
        planeConfig.put("Visibility", String.valueOf(config.isVisible()));
        planeConfig.put("Shift", String.valueOf(config.sliderValue()));
        return planeConfig;
    }

    /**
     * Writes information from points to given writer
     *
     * @param points data with 2D points
     * @param writer Points data will be written to writer
     */
    public static void writeCurveExport(List<Point2d> points, PrintWriter writer) {
        writer.println("N,X-Coordinate,Y-Coordinate");
        DecimalFormat df = new DecimalFormat("###.#####");
        int pointCounter = 1;
        for (Point2d point : points) {
            writer.println(pointCounter + "," + df.format(point.x) + "," + df.format(point.y));
            ++pointCounter;
        }
    }

    /**
     * @author Ondřej Bazala
     *
     * @param type Symmetry or bounding box type
     * @param vectorDirection Normal vector determining direction of cutting plane
     * @param isVisible Cutting plane is shown
     * @param sliderValue shift of cutting plane at slider
     */
    public record CuttingPlaneConfig(String type, Vector3d vectorDirection, Boolean isVisible, Integer sliderValue) {}

    /**
     * Computes cross-sections with all given visible planes
     *
     * @param crossSectionPlanes list of selected cutting planes to compute intersections with
     * @param primaryFace of task
     * @param secondaryFace of task
     * @param featurePointCloseness threshold for distance of feature points
     *
     * @return for each face returns list of curves
     */
    public static List<List<CrossSection2D>> computeCrossSections(List<CrossSectionPlane> crossSectionPlanes,
                                                                  HumanFace primaryFace, HumanFace secondaryFace,
                                                                  Double featurePointCloseness) {
        List<Boolean> isPrimaryFaceList = secondaryFace == null ? List.of(true) : List.of(true, false);
        return isPrimaryFaceList.stream()
                .map(isPrimaryFace ->
                    crossSectionPlanes.stream()
                            .map(crossSectionPlane -> {
                                if (!crossSectionPlane.isVisible()) {
                                    // add null values for hidden planes to correctly assign computed hausdorff distances
                                    return null;
                                }
                                Plane cuttingPlane = crossSectionPlane.cuttingPlane();
                                HumanFace face = isPrimaryFace ? primaryFace : secondaryFace;
                                CrossSection3D curve3d = CrossSectionServices.compute(
                                        face.getMeshModel(),
                                        new CrossSectionConfig(cuttingPlane));
                                List<Point2d> featurePoints = getCloseFeaturePoints(
                                        cuttingPlane,
                                        face,
                                        featurePointCloseness);
                                return new CrossSection2D(
                                        curve3d.getFlattened(crossSectionPlane.normalVector),
                                        featurePoints);
                            })
                            .toList())
                .toList();
    }

    /**
     * @author Ondřej Bazala
     *
     * @param isVisible Cutting plane is shown
     * @param normalVector Normal vector determining direction of cutting plane
     * @param cuttingPlane Cutting plane to be used
     */
    public record CrossSectionPlane(Boolean isVisible, Vector3d normalVector, Plane cuttingPlane) {}

    /**
     * Returns list of 2D feature points of given face that are close to the cutting
     * plane and close to the mesh.
     *
     * @param cuttingPlane cutting plane for selecting feature points
     * @param face human face containing feature points
     * @param featurePointCloseness threshold for distance of feature points
     *
     * @return valid 2D feature points of face that are close to cutting plane
     */
    private static List<Point2d> getCloseFeaturePoints(Plane cuttingPlane, HumanFace face, Double featurePointCloseness) {
        if (!face.hasLandmarks()) {
            return null;
        }
        FaceStateServices.updateLandmarksVicinity(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);

        return face.getAllLandmarks()
                .stream()
                .filter(fp -> Math.abs(cuttingPlane.getPointDistance(fp.getPosition())) < featurePointCloseness)
                .filter(fp -> fp.getMeshVicinity().getPositionType() != MeshVicinity.Location.OFF_THE_MESH)
                .map(Landmark::getPosition)
                .map(fp -> CrossSection3D.flattenPoint(fp, cuttingPlane.getNormal()))
                .toList();
    }

    /**
     * Computes Hausdorff distances of all visible curves.
     *
     * @param samplingStrength sampling strength to be used in computations
     * @param primaryCurves cross-sections with primary face
     * @param secondaryCurves cross-sections with secondary face
     *
     * @return list with hausdorff distances or nulls
     */
    public static List<Double> getHausdorffDistances(Integer samplingStrength, List<CrossSection2D> primaryCurves, List<CrossSection2D> secondaryCurves) {
        return IntStream.range(0, primaryCurves.size())
                .mapToObj(i -> {
                    if (primaryCurves.get(i) == null) {
                        return null;
                    }
                    return primaryCurves.get(i).hausdorffDistance(secondaryCurves.get(i), samplingStrength);
                })
                .toList();
    }

    /**
     * Computes scale and offset based on bounding box of face and cutting plane direction
     * so that currently projected curve fits the size of canvas.
     *
     * @param canvasWidth width size of canvas
     * @param canvasHeight height size of canvas
     * @param faceBoundingBox 3D bounding box of face
     * @param currentCuttingPlaneNormal normal of cutting plane determining direction
     *
     * @return 2D bounding box with scale and offsets
     */
    public static BoundingBox2D getBoundingBox2D(Integer canvasWidth, Integer canvasHeight, Box faceBoundingBox,
                                                    Vector3d currentCuttingPlaneNormal) {
        // project bounding box into 2D
        Point2d bboxMin = CrossSection3D.flattenPoint(faceBoundingBox.minPoint(), currentCuttingPlaneNormal);
        Point2d bboxMax = CrossSection3D.flattenPoint(faceBoundingBox.maxPoint(), currentCuttingPlaneNormal);
        double minX = bboxMin.x;
        double maxX = bboxMax.x;
        double minY = bboxMin.y;
        double maxY = bboxMax.y;

        // scale bounding box and compute offset
        double scale = 0.8 * Math.abs(Math.min(canvasWidth / (maxX - minX), canvasHeight / (maxY - minY)));
        minX *= scale;
        maxX *= scale;
        minY *= scale;
        maxY *= scale;
        double offsetX = ((canvasWidth / 2.0) - (maxX - minX) / 2.0) - minX;
        double offsetY = ((canvasHeight / 2.0) - (maxY - minY) / 2.0) - minY;

        return new BoundingBox2D(scale, offsetX, offsetY);
    }

    /**
     * @author Ondřej Bazala
     *
     * @param scale of 2D projection
     * @param offsetX of drawings in 2D projection
     * @param offsetY of drawings in 2D projection
     */
    public record BoundingBox2D(double scale, double offsetX, double offsetY) {}
}
