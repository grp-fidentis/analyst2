package cz.fidentis.analyst.engines.face.batch.distance.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.measurement.DistanceRecord;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.vecmath.Point3d;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that computes similarity of a set of faces by computing
 * the distance of faces to an average (gauge) face and then combining these values
 * to get mutual similarity for all pairs. Euclidean distance to nearest neighbors
 * is used as the distance metric.
 * <p>
 * The computation is accelerated by using multiple CPU cores concurrently.
 * </p>
 *
 * @author Radek Oslejsek
 */
public class BatchDistanceNearestNeighborsCombined extends BatchDistanceNearestNeighbors {

    /**
     * Constructor.
     *
     * @param gaugeFace A face serving as a "gauge". It is usually an average face of a set of faces.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     */
    public BatchDistanceNearestNeighborsCombined(HumanFace gaugeFace, int numFaces) {
        super(gaugeFace, numFaces, new CombinedCache());
    }

    /**
     * TO DO
     *
     * @author Radek Oslejsek
     */
    protected static class CombinedCache extends DistCache<ImmutablePair<Point3d, Double>> {

        @Override
        public void addValuesOfFace(MeshDistances meshDistances) {
            if (meshDistances == null) {
                add(null);
            } else {
                List<Point3d> nearestPoints = meshDistances.stream()
                        .map(DistanceRecord::getNearestNeighbor)
                        .map(mp -> mp == null ? null : mp.getPosition())
                        .toList();

                List<Double> distances = meshDistances.stream()
                        .map(DistanceRecord::getDistance)
                        .toList();

                assert(nearestPoints.size() == distances.size());

                List<ImmutablePair<Point3d, Double>> list = new ArrayList<>(distances.size());
                for (int i = 0; i < distances.size(); i++) {
                    list.add(new ImmutablePair<>(nearestPoints.get(i), distances.get(i)));
                }
                add(list);
            }
        }

        @Override
        public double getDistance(int faceIndex1, int faceIndex2, int vertIndex) {
            double d1 = getCache().get(faceIndex1).get(vertIndex).getRight();
            double d2 = getCache().get(faceIndex2).get(vertIndex).getRight();

            if (!Double.isFinite(d1) || !Double.isFinite(d2)) {
                return Double.NaN;
            } else if (d1 >= 0 && d2 >= 0 || d1 <= 0 && d2 <= 0) {
                return Math.abs(d1 - d2);
            } else {
                Point3d p1 = getCache().get(faceIndex1).get(vertIndex).getLeft();
                Point3d p2 = getCache().get(faceIndex2).get(vertIndex).getLeft();
                return (p1 != null && p2 != null)
                        ? p1.distance(p2)
                        : Double.NaN;
            }
        }
    }

}
