package cz.fidentis.analyst.engines.face.impl;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.icp.IcpConfig;
import cz.fidentis.analyst.engines.icp.IcpServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.landmarks.LandmarkServices;
import cz.fidentis.analyst.engines.landmarks.PrTransformation;
import cz.fidentis.analyst.math.Quaternion;

import javax.vecmath.*;
import java.util.Collection;

/**
 * A utility functions for the registration of whole human faces.
 *
 * @author Radek Oslejsek
 */
public class FaceRegistrationServicesImpl {

    /**
     * Superimpose given face to the face included in the ICP configuration object.
     *
     * @param transformedFace A face to be transformed.
     * @param icpConfig ICP configuration
     */
    public void alignMeshes(HumanFace transformedFace, IcpConfig icpConfig) {

        // transform mesh:
        var trHistory = IcpServices.transform(transformedFace.getMeshModel(), icpConfig);

        FaceStateServices.updateKdTree(transformedFace, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateOctree(transformedFace, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateBoundingBox(transformedFace, FaceStateServices.Mode.RECOMPUTE_IF_PRESENT);

        // transform feature points:
        if (transformedFace.hasLandmarks()) {
            trHistory.values()
                    .forEach(trList -> trList.forEach(tr -> {
                        for (int i = 0; i < transformedFace.getAllLandmarks().size(); i++) {
                            Landmark fp = transformedFace.getAllLandmarks().get(i);
                            fp.setPosition(tr.transformPoint(fp.getPosition(), icpConfig.scale()));
                        }
                    }));
        }

        // transform symmetry plane:
        if (transformedFace.hasSymmetryPlane()) {
            trHistory.values()
                    .forEach(trList -> trList.forEach(tr -> transformedFace.setSymmetryPlane(
                            transformPlane(
                                    transformedFace.getSymmetryPlane(),
                                    tr.rotation(),
                                    tr.translation(),
                                    tr.scaleFactor()))
                            )
                    );
        }
    }

    /**
     * Transform the face "manually".
     *
     * @param face Face to be transformed.
     * @param rotation Rotation vector denoting the rotation angle around axes
     * X, Y, and Z.
     * @param translation Translation vector denoting the translation in the X,
     * Y, and Z direction.
     * @param scale Scale factor (1 = no scale).
     */
    public void transformFace(HumanFace face, Vector3d rotation, Vector3d translation, double scale) {
        Quaternion rot = new Quaternion(rotation.x, rotation.y, rotation.z, 1.0);

        // update mesh
        face.getMeshModel().getFacets().stream()
                .map(MeshFacet::getVertices)
                .flatMap(Collection::parallelStream)
                .forEach(meshPoint -> {
                    transformPoint(meshPoint.getPosition(), rot, translation, scale);
                    transformNormal(meshPoint.getNormal(), rot);
                });

        FaceStateServices.updateKdTree(face, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateOctree(face, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateBoundingBox(face, FaceStateServices.Mode.RECOMPUTE_IF_PRESENT);

        // transform feature points:
        if (face.hasLandmarks()) {
            face.getAllLandmarks().parallelStream()
                    .forEach(fp -> transformPoint(fp.getPosition(), rot, translation, scale));
        }

        // transform symmetry plane:
        if (face.hasSymmetryPlane()) {
            face.setSymmetryPlane(transformPlane(face.getSymmetryPlane(), rot, translation, scale));
        }
    }

    /**
     * Transforms the second face so that its symmetry plane fits the symmetry plane of the first face.
     * Symmetry planes must be computed in advance!
     *
     * @param staticFace a human face that remains unchanged
     * @param transformedFace a human face that will be transformed
     * @param preserveUpDir If {@code false}, then the object can be rotated around the target's normal arbitrarily.
     * @throws NullPointerException if the symmetry planes are missing.
     */
    public void alignSymmetryPlanes(HumanFace staticFace, HumanFace transformedFace, boolean preserveUpDir) {
        Plane statPlane = staticFace.getSymmetryPlane();
        Plane tranPlane = transformedFace.getSymmetryPlane();

        if (statPlane.getNormal().dot(tranPlane.getNormal()) < 0) {
            tranPlane = tranPlane.flip();
        }

        Matrix4d trMat = statPlane.getAlignmentMatrix(tranPlane, preserveUpDir);

        // Transform mesh vertices:
        Matrix3d rotMat = new Matrix3d();
        transformedFace.getMeshModel().getFacets()
                .forEach(f -> f.getVertices().forEach(p -> {
                    trMat.transform(p.getPosition());
                    if (p.getNormal() != null) {
                        trMat.getRotationScale(rotMat);
                        rotMat.transform(p.getNormal());
                        p.getNormal().normalize();
                    }
                }));

        FaceStateServices.updateKdTree(transformedFace, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateOctree(transformedFace, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateBoundingBox(transformedFace, FaceStateServices.Mode.RECOMPUTE_IF_PRESENT);

        // Transform feature points:
        if (transformedFace.hasLandmarks()) {
            transformedFace.getAllLandmarks().parallelStream()
                    .forEach(fp -> trMat.transform(fp.getPosition()));
        }

        // Transform the symmetry plane
        Point3d p = transformedFace.getSymmetryPlane().getPlanePoint();
        trMat.transform(p);
        transformedFace.setSymmetryPlane(new Plane(p));
    }

    /**
     * Transforms the second face so that its feature points best fit the position of corresponding
     * feature points of the first face.
     *
     * @param staticFace a human face that remains unchanged
     * @param transformedFace a human face that will be transformed
     * @param scale Whether to scale faces as well
     */
    public PrTransformation alignFeaturePoints(HumanFace staticFace, HumanFace transformedFace, boolean scale) {
        // compute Procrustes superimposition
        PrTransformation pt = LandmarkServices.getProcrustesTransformation(staticFace.getAllLandmarks(), transformedFace.getAllLandmarks(), scale);
        if (pt == null) {
            return null;
        }

        // transform mesh and landmarks
        transformedFace.getMeshModel().getFacets().forEach(facet ->
                LandmarkServices.transform(facet.getVertices(), pt));
        LandmarkServices.transform(transformedFace.getAllLandmarks(), pt);

        FaceStateServices.updateKdTree(transformedFace, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateOctree(transformedFace, FaceStateServices.Mode.DELETE);
        FaceStateServices.updateBoundingBox(transformedFace, FaceStateServices.Mode.RECOMPUTE_IF_PRESENT);

        // transform symmetry plane:
        if (transformedFace.hasSymmetryPlane()) {
            Vector3d adjustment = pt.getCentroidAdjustment();
            double transformationScaleValue = pt.getScale();
            Quaternion rotation = pt.getMatrixAsQuaternion(pt.getRotationMatrix());
            transformedFace.getAllLandmarks().parallelStream()
                    .forEach(fp -> transformedFace.setSymmetryPlane(transformPlane(
                            transformedFace.getSymmetryPlane(), rotation, adjustment, transformationScaleValue))
                    );
            transformedFace.getAllLandmarks().parallelStream()
                    .forEach(fp -> transformedFace.setSymmetryPlane(transformPlane(
                            transformedFace.getSymmetryPlane(),
                            pt.getMatrixAsQuaternion(pt.getIdentityMatrix()),
                            pt.getSuperImpositionAdjustment(), 1))
                    );
        }

        return pt;
    }

    /**
     * Transform a single 3d point.
     *
     * @param point point to be transformed
     * @param rotation rotation, can be {@code null}
     * @param translation translation
     * @param scale scale
     */
    private void transformPoint(Tuple3d point, Quaternion rotation, Vector3d translation, double scale) {
        Quaternion rotQuat = new Quaternion(point.x, point.y, point.z, 1);

        if (rotation != null) {
            Quaternion rotationCopy = Quaternion.multiply(rotQuat, rotation.getConjugate());
            rotQuat = Quaternion.multiply(rotation, rotationCopy);
        }

        point.set(
                rotQuat.x * scale + translation.x,
                rotQuat.y * scale + translation.y,
                rotQuat.z * scale + translation.z
        );
    }

    private void transformNormal(Tuple3d normal, Quaternion rotation) {
        if (normal != null) {
            transformPoint(normal, rotation, new Vector3d(0, 0, 0), 1.0); // rotate only
        }
    }

    /**
     * Transforms the whole plane, i.e., its normal and position.
     *
     * @param plane plane to be transformed
     * @param rot rotation
     * @param translation translation
     * @param scale scale
     * @return transformed plane
     */
    public Plane transformPlane(Plane plane, Quaternion rot, Vector3d translation, double scale) {
        Point3d point = new Point3d(plane.getNormal());
        transformNormal(point, rot);
        Plane retPlane = new Plane(point, plane.getDistance());

        // ... then translate and scale a point projected on the rotate plane:
        point.scale(retPlane.getDistance()); // point laying on the rotated plane
        transformPoint(point, null, translation, scale); // translate and scale only
        Vector3d normal = retPlane.getNormal();
        double dist = ((normal.x * point.x) + (normal.y * point.y) + (normal.z * point.z))
                / Math.sqrt(normal.dot(normal)); // distance of transformed surface point in the plane's normal direction

        return new Plane(retPlane.getNormal(), dist);
    }

}
