package cz.fidentis.analyst.engines.face.batch.registration;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.engines.face.batch.registration.impl.BatchFaceRegistrationImpl;

/**
 * Services for the computation of N:N registration and/or computing an average face.
 *
 * @author Radek Oslejsek
 */
public class BatchFaceRegistrationServices {

    /**
     * Strategies of registration
     *
     * @author Radek Oslejsek
     */
    public enum RegistrationStrategy {
        /**
         * Skip registration (i.e., compute the average face only for already registered faces)
         */
        NONE,

        /**
         * Use Generalized Procrustes Analysis (more precisely superimposition)
         * to mutually align the same landmarks. Landmarks are required.
         */
        GPA,

        /**
         * Use Iterative Closets Point algorithm to mutually align mesh vertices.
         * No landmarks are required.
         */
        ICP
    }

    /**
     * Strategy of the computation of the average face
     *
     * @author Radek Oslejsek
     */
    public enum AverageFaceStrategy {
        /**
         * Skip the computation of the average face, i.e., use and existing (average) face
         * as the template face for registration.
         */
        NONE,

        /**
         * The average face metamorphose to other faces by searching nearest neighbors,
         * i.e., the closest vertices of registered faces.
         */
        NEAREST_NEIGHBOURS,

        /**
         * The average face metamorphose to other faces by projecting its vertices
         * to registered faces using ray-casting computed on CPU
         */
        PROJECTION_CPU,

        /**
         * The average face metamorphose to other faces by projecting its vertices
         * to registered faces using ray-casting computed on GPU. If GPU is not
         * supported, switches to the {@code PROJECTION_CPU} automatically.
         */
        PROJECTION_GPU,
    }

    /**
     * Constructor.
     *
     * @param templateFace A template face towards which other faces are registered and/or from which
     *                     the average face is computed. Be aware that if the average face computation is requested,
     *                     then the template face geometry (mesh) metamorphoses (<b>changes its geometry</b>)!
     *                     Therefore, use a clone if you need to preserve the original face.
     * @param config Parameter object for this service
     */
    public static BatchFaceRegistration initRegistration(HumanFace templateFace, BatchFaceRegistrationConfig config) {
        return new BatchFaceRegistrationImpl(templateFace, config);
    }
}
