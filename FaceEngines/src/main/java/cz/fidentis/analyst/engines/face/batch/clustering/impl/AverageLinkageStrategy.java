package cz.fidentis.analyst.engines.face.batch.clustering.impl;

import cz.fidentis.analyst.engines.face.batch.clustering.LinkageStrategy;
import cz.fidentis.analyst.engines.face.batch.clustering.dto.Distance;

import java.util.List;

/**
 *
 * @author Patrik Tomov
 */
public class AverageLinkageStrategy implements LinkageStrategy {

    @Override
    public Distance calculateDistance(List<Distance> distances) {
        double sum = distances.stream().mapToDouble(Distance::getDistance).sum();
        double average = distances.isEmpty() ? 0.0 : sum / distances.size();
        return new Distance(average);
    }
}
