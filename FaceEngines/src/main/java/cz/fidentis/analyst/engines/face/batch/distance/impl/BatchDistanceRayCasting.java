package cz.fidentis.analyst.engines.face.batch.distance.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.mesh.measurement.DistanceRecord;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;

/**
 * A class that computes similarity of a set of faces by casting rays
 *  in the diction of template face normals.
 * <p>
 * The computation is accelerated by using multiple CPU or possibly GPU cores concurrently.
 * </p>
 *
 * @author Radek Oslejsek
 */
public class BatchDistanceRayCasting extends BatchIndirectDistanceImpl {

    private final GLContext context;

    /**
     * Constructor.
     *
     * @param gaugeFace A face serving as a "gauge". It is usually an average face of a set of faces.
     * @param numFaces The number of faces to be measured (excluding the gauge face)
     * @param context Active OpenGL context on which makeCurrent() can be called.
     *               If this parameter is not {@code null}, then GPU will be used.
     *               Otherwise, regular CPU implementation is used.
     */
    public BatchDistanceRayCasting(HumanFace gaugeFace, int numFaces, GLContext context) {
        super(gaugeFace, numFaces, new RayCastingDistCache());
        this.context = context;
    }

    @Override
    public void addToMeasurement(HumanFace face) {
        if (context == null) {
            FaceStateServices.updateOctree(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
        }

        MeshDistances dist = MeshDistanceServices.measure(
                getGaugeFace().getMeshModel(),
                context == null ? new MeshDistanceConfig(MeshDistanceConfig.Method.RAY_CASTING, face.getOctree(), true)
                        : new MeshDistanceConfig(MeshDistanceConfig.Method.RAY_CASTING_GPU, face.getMeshModel().getFacets(), context, true, false));
        getDistCache().addValuesOfFace(dist);

        // Memory is more valuable. And it's probable that we will not need
        // the octrees anymore. If yes, then they are quickly re-computed.
        FaceStateServices.updateOctree(face, FaceStateServices.Mode.DELETE);
    }

    /**
     * Cache of relative distances from the average face to other faces.
     *
     * @author Radek Oslejsek
     */
    protected static class RayCastingDistCache extends BatchDistanceNearestNeighbors.DistCache<Double> {

        @Override
        public void addValuesOfFace(MeshDistances meshDistances) {
            if (meshDistances == null) {
                add(null);
            } else { // Store relative distances of vertices to the cache
                add(meshDistances.stream()
                        .mapToDouble(DistanceRecord::getDistance)
                        .boxed()
                        .toList());
            }
        }

        @Override
        public double getDistance(int faceIndex1, int faceIndex2, int vertIndex) {
            double d1 = getCache().get(faceIndex1).get(vertIndex);
            double d2 = getCache().get(faceIndex2).get(vertIndex);
            return (Double.isFinite(d1) && Double.isFinite(d2))
                    ? Math.abs(d1 - d2)
                    : Double.NaN;
        }
    }
}
