package cz.fidentis.analyst.rendering;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.glu.GLU;
import cz.fidentis.analyst.data.shapes.Glyph;
import cz.fidentis.analyst.drawables.Drawable;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.drawables.renderers.RenderingService;
import cz.fidentis.analyst.engines.face.FaceStateServices;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.glsl.GlslServices;
import cz.fidentis.analyst.glsl.buffergroups.BufferGroupDef;
import cz.fidentis.analyst.glsl.buffergroups.GlslBufferGroup;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.code.GlslProgram;
import cz.fidentis.analyst.glsl.code.GlslProgramDef;

import javax.vecmath.Vector4f;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL2ES3.*;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_LIGHTING;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.*;

/**
 * Handles GLSL {@code Program}s for 3D scene.
 * This class links shaders into a program, initialises buffers and textures, and uses programs during
 * a rendering pass.
 *
 * @author Ondrej Simecek
 */
public class ShadersManager {
    private final GLContext glContext;
    private final GLU glu;

    private boolean useContours = false;

    private static final int MAX_FRAMEBUFFER_WIDTH = 2048;
    private static final int MAX_FRAMEBUFFER_HEIGHT = 2048;

    private static final float[] FOG_COLOR = {0f, 0f, 1.0f, 1.0f};

    private int fogVersion = 0;
    private static final int SHADOW_RATIO = 2;

    private boolean useGlyphs = false;

    private static final int MAX_GLYPHS = 40;

    private final float[] lightModelViewMatrix = new float[16];
    private final float[] lightProjectionMatrix = new float[16];

    /**
     * Shader storage buffer for glyphs and information about them: position, normal and principle curvature directions.
     */
    private final GlslBufferGroup buffers;

    /**
     * Setups buffers and textures. To be used in the initialisation phase of the OpenGl context.
     *
     * @param glContext  An OpenGL context. Must not be {@code null}, must be at least GL2
     * @param glu An OpenGL utilities. Must not be {@code null}
     * @throws GLException if the shaders' initiation fails
     */
    public ShadersManager(GLContext glContext, GLU glu) {
        if (!glContext.isGL2()) {
            throw new IllegalArgumentException("glContext");
        }
        this.glContext = glContext;
        this.glu = Objects.requireNonNull(glu);

        int totalPixels = MAX_FRAMEBUFFER_WIDTH * MAX_FRAMEBUFFER_HEIGHT;

        // Register buffers:
        this.buffers = GlslServices.registerBuffers(glContext, BufferGroupDef.VISUAL_EFFECTS);

        // Allocate buffers:
        buffers.getImageBuffer(BufferDef.VIS_DIFF_DEPTH_TEXTURE_BUFFER).allocate( // target = GL_TEXTURE_2D
                SHADOW_RATIO * MAX_FRAMEBUFFER_WIDTH,
                SHADOW_RATIO * MAX_FRAMEBUFFER_HEIGHT,
                GL_DEPTH_COMPONENT,
                GL_UNSIGNED_BYTE,
                GL_NEAREST);

        buffers.getFrameBuffer(BufferDef.VIS_DIFF_DEPTH_FRAME_BUFFER).allocate( // target = GL_FRAMEBUFFER
                buffers.getImageBuffer(BufferDef.VIS_DIFF_DEPTH_TEXTURE_BUFFER));

        buffers.getTextureAdapterBuffer(BufferDef.VIS_DIFF_FRAGMENT_LIST_TEXTURE_BUFFER).allocate(
                GL_NEAREST,
                buffers.getSSboBuffer(BufferDef.VIS_DIFF_FRAGMENT_LIST_SSBO_BUFFER).getGlName());

        buffers.getSSboBuffer(BufferDef.VIS_DIFF_FRAGMENT_LIST_SSBO_BUFFER).allocate(
                totalPixels,
                false);

        buffers.getImageBuffer(BufferDef.VIS_DIFF_HEAD_POINTER_TEXTURE_BUFFER).allocate( // target = GL_TEXTURE_2D
                MAX_FRAMEBUFFER_WIDTH,
                MAX_FRAMEBUFFER_HEIGHT,
                GL_RED_INTEGER,
                GL_UNSIGNED_INT,
                GL_NEAREST);

        buffers.getPixelUnpackBuffer(BufferDef.VIS_DIFF_HEAD_POINTER_INIT_PU_BUFFER).allocate(
                totalPixels,
                true);

        buffers.getAtomicCounterBuffer(BufferDef.VIS_DIFF_ATOMIC_COUNTER_SSBO_BUFFER).allocate();

        glContext.getGL().glBindFramebuffer(GL_FRAMEBUFFER, 0); // switch back to default frame buffer
        glContext.getGL().glDisable(GL_CULL_FACE);
        glContext.getGL().glBindTexture(GL_TEXTURE_2D, 0);
        glContext.getGL().glActiveTexture(GL_TEXTURE0);
    }

    /**
     * Puts drawable objects through pipeline which enables to render fog simulation and contours.
     *
     * @param drawables objects to be rendered
     * @param viewPortWidth current width of the viewport
     * @param viewPortHeight current height of the viewport
     * @param lightPosition the position of the scene's light in homogenous object coordinates.
     */
    public void renderObjects(Collection<Drawable> drawables, int viewPortWidth, int viewPortHeight, Vector4f lightPosition) {
        if (useGlyphs) {
            GlslServices.useProgram(glContext, GlslProgramDef.SHADOW_MAP_GLSL_PROGRAM);

            setupLightSpaceView(lightModelViewMatrix, lightProjectionMatrix, lightPosition);

            List<DrawableFace> faces = drawables.stream()
                    .filter(e -> e instanceof DrawableFace)
                    .map(e -> (DrawableFace) e).toList();
            for (DrawableFace face : faces) {
                updateGlyphDataSSBO(face);
                RenderingService.render(glContext.getGL().getGL2(), face);
            }

            resetView(viewPortWidth, viewPortHeight);
        }

        GlslProgram shadingProgram = GlslServices.useProgram(glContext, GlslProgramDef.SHADING_GLSL_PROGRAM);
        // ALLOCATION TEST
        buffers.bindBuffer(BufferDef.VIS_DIFF_HEAD_POINTER_TEXTURE_BUFFER);
        buffers.bindBuffer(BufferDef.VIS_DIFF_FRAGMENT_LIST_TEXTURE_BUFFER);
        buffers.bindBuffer(BufferDef.VIS_DIFF_ATOMIC_COUNTER_SSBO_BUFFER); // !!! z nejakho duvodu se musi znovu bindovat !!!
        //buffers.bindBuffer(BufferDef.VIS_DIFF_DEPTH_TEXTURE_BUFFER); // z nejakeho duvodu neni potreba

        ////// resetPointerTexture():
        buffers.getAtomicCounterBuffer(BufferDef.VIS_DIFF_ATOMIC_COUNTER_SSBO_BUFFER).reset(); // reset to zero
        buffers.getImageBuffer(BufferDef.VIS_DIFF_HEAD_POINTER_TEXTURE_BUFFER).clear(0); // reset to zeros

        /////// bindTextures():
        //buffers.bindBuffer(BufferDef.VIS_DIFF_HEAD_POINTER_TEXTURE_BUFFER); // target = GL_TEXTURE_2D, index = 1
        //buffers.bindBuffer(BufferDef.VIS_DIFF_FRAGMENT_LIST_TEXTURE_BUFFER); // target = GL_TEXTURE_BUFFER, index = 2
        //buffers.bindBuffer(BufferDef.VIS_DIFF_DEPTH_TEXTURE_BUFFER); // target = GL_TEXTURE_2D, index = -1

        // bind back default targets
        glContext.getGL().glBindTexture(GL_TEXTURE_2D, 0);
        glContext.getGL().glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        glContext.getGL().glActiveTexture(GL_TEXTURE3); // shadow_texture (binding index 3) from SHADING_GLSL_PROGRAM?

        renderDrawables(drawables, shadingProgram);

        useColorMixingProgram(viewPortWidth, viewPortHeight);

        if (useContours) {
            GlslServices.useProgram(glContext,GlslProgramDef.CONTOURS_GLSL_PROGRAM);
            glContext.getGL().glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }
    }

    /**
     * Get information about shadow-casting glyphs and update the storage buffer with them.
     *
     * @param face face the glyph information belongs to
     */
    private void updateGlyphDataSSBO(DrawableFace face) {
        FaceStateServices.updateGlyphs(
                face.getHumanFace(),
                FaceStateServices.Mode.COMPUTE_IF_ABSENT,
                PointSamplingConfig.Method.POISSON,
                MAX_GLYPHS);
        List<Glyph> samples = face.getHumanFace().getGlyphs();
        int glyphSampleCount = samples.size();

        buffers.getSSboBuffer(BufferDef.VIS_DIFF_GLYPH_LOCATIONS_SSBO_BUFFER).allocate(glyphSampleCount, 60, false);
        buffers.bindSsboBuffers(); // JE TO NEZBYTNE?

        buffers.getSSboBuffer(BufferDef.VIS_DIFF_GLYPH_LOCATIONS_SSBO_BUFFER).writeBytesTo(0, 4, byteBuffer ->
                byteBuffer.putInt(glyphSampleCount)
        );

        long bufferSize = glyphSampleCount * buffers.getSSboBuffer(BufferDef.VIS_DIFF_GLYPH_LOCATIONS_SSBO_BUFFER).getItemSize();
        buffers.getSSboBuffer(BufferDef.VIS_DIFF_GLYPH_LOCATIONS_SSBO_BUFFER).writeBytesTo(4, bufferSize, byteBuffer ->
                samples.stream()
                        .map(glyph -> List.of(glyph.location(), glyph.normal(), glyph.maxCurvatureDir(), glyph.minCurvatureDir()))
                        .flatMap(Collection::stream)
                        .map(tuple -> List.of(tuple.x, tuple.y, tuple.z))
                        .flatMap(Collection::stream)
                        .forEach(coord -> byteBuffer.putFloat(coord.floatValue())));
    }

    /**
     * Set up the graphical context for rendering the scene from the lights point of view; save the modelView and
     * projection matrices for later use.
     *
     * @param modelViewMatrix destination of the modelView matrix from the light point of view
     * @param projectionMatrix destination of the projection matrix used for shadow rendering
     * @param lightPosition position of the scene's light
     */
    private void setupLightSpaceView(float[] modelViewMatrix, float[] projectionMatrix, Vector4f lightPosition) {
        GL2 gl = glContext.getGL().getGL2();
        gl.glDepthFunc(GL_LESS);

        gl.glViewport(0, 0, SHADOW_RATIO * MAX_FRAMEBUFFER_HEIGHT, SHADOW_RATIO * MAX_FRAMEBUFFER_WIDTH);
        gl.glClear(GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        glu.gluLookAt(lightPosition.x, lightPosition.y, lightPosition.z, 0, 0, 0, 0, 1, 0);
        gl.glGetFloatv(GL_MODELVIEW_MATRIX, FloatBuffer.wrap(modelViewMatrix));

        gl.glMatrixMode(GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        glu.gluPerspective(Camera.FIELD_OF_VIEW, 1f, 100f, 1500f);
        gl.glGetFloatv(GL_PROJECTION_MATRIX, FloatBuffer.wrap(projectionMatrix));
    }

    /**
     * Reset the context after rendering with non-default transformation matrices, framebuffer and viewport
     *
     * @param viewPortWidth width in pixels
     * @param viewPortHeight height in pixels
     */
    private void resetView(int viewPortWidth, int viewPortHeight) {
        GL2 gl = glContext.getGL().getGL2();

        gl.glMatrixMode(GL_PROJECTION);
        gl.glPopMatrix();

        gl.glMatrixMode(GL_MODELVIEW);
        gl.glPopMatrix();

        gl.glBindFramebuffer(GL_FRAMEBUFFER, 0);

        gl.glDepthFunc(GL_ALWAYS);
        gl.glEnable(GL_LIGHTING);
        gl.glViewport(0, 0, viewPortWidth, viewPortHeight);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    /**
     * Set up uniforms for the {@code shadingProgram} and render all visible objects.
     *
     * @param drawables objects to be rendered via their render() function
     * @param program compiled GLSL shading program
     */
    private void renderDrawables(Collection<Drawable> drawables, GlslProgram program) {
        GL2 gl = glContext.getGL().getGL2();
        program.setMat4fVar("lightProjectionMatrix", lightProjectionMatrix);
        program.setMat4fVar("lightModelViewMatrix", lightModelViewMatrix);

        List<Drawable> objects = new ArrayList<>(drawables);
        for (int i = 0; i < drawables.size(); i++) {
            if (objects.get(i).isShown()) {
                program.setNumberVar("renderGlyphs", useGlyphs ? 1 : 0);
                program.setNumberVar("modelNumber", i);
                if (objects.get(i) instanceof DrawableFace face) {
                    program.setNumberVar("isFace", GL_TRUE);
                    if (useGlyphs) {
                        updateGlyphDataSSBO(face);
                    }
                } else {
                    program.setNumberVar("isFace", GL_FALSE);
                }
                if (objects.get(i) instanceof DrawableFace && ((DrawableFace) objects.get(i)).isHeatmapRendered()) {
                    program.setNumberVar("materialMode", 2);
                } else if (objects.get(i).getRenderMode() != RenderingMode.TEXTURE) {
                    program.setNumberVar("materialMode", 0);
                } else {
                    program.setNumberVar("materialMode", 1);
                }
                RenderingService.render(gl, objects.get(i));
            }
        }
    }

    /**
     * Set up uniforms for the {@code colorMixingProgram} and use it.
     *
     * @param viewPortWidth width in pixels
     * @param viewPortHeight height in pixels
     */
    private void useColorMixingProgram(int viewPortWidth, int viewPortHeight) {
        GL2 gl = glContext.getGL().getGL2();
        //find extreme z-buffer values
        gl.glMemoryBarrier((int) GL_ALL_BARRIER_BITS);
        FloatBuffer depth = Buffers.newDirectFloatBuffer(viewPortWidth * viewPortHeight);
        gl.glReadPixels(0, 0, viewPortWidth, viewPortHeight, GL_DEPTH_COMPONENT, GL_FLOAT, depth);
        float min = 1;
        float max = 0;
        for (int i = 0; i < viewPortHeight * viewPortWidth; i++) {
            if (depth.get(i) != 0 && depth.get(i) != 1) {
                min = Math.min(min, depth.get(i));
                max = Math.max(max, depth.get(i));
            }
        }

        GlslProgram program = GlslServices.useProgram(glContext,GlslProgramDef.COLOR_MIXING_GLSL_PROGRAM);
        buffers.bindBuffer(BufferDef.VIS_DIFF_HEAD_POINTER_INIT_PU_BUFFER);

        program.setNumberVar("minZ", min);
        program.setNumberVar("maxZ", max);
        program.setNumberVar("fogVersion", fogVersion);
        program.setVec4fVar("fogColor", FOG_COLOR[0], FOG_COLOR[1], FOG_COLOR[2], 1);
        program.setNumberVar("innerSurfaceSolid", (fogVersion != 3 && !useGlyphs) ? GL_FALSE : GL_TRUE);

        gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
        gl.glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

    /**
     * Setter for fog rendering type
     *
     * @param fogVersion Enumerated type of fog
     */
    public void setFogVersion(int fogVersion) {
        this.fogVersion = fogVersion;
    }

    /**
     * Setter for turning contours on and off
     *
     * @param useContours boolean to set the contours to
     */
    public void setUseContours(boolean useContours) {
        this.useContours = useContours;
    }

    /**
     * Setter for turning glyphs on and off
     *
     * @param useGlyphs boolean to set the glyphs to
     */
    public void setUseGlyphs(boolean useGlyphs) {
        this.useGlyphs = useGlyphs;
    }
}
