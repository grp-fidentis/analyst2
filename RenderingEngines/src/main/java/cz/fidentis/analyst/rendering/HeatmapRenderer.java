package cz.fidentis.analyst.rendering;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;

import static com.jogamp.opengl.GL.GL_FRONT_AND_BACK;
import static com.jogamp.opengl.GL.GL_TRIANGLES;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_DIFFUSE;

/**
 * Heatmap rendering.
 * 
 * @author Daniel Sokol
 * @author Daniel Schramm
 */
public class HeatmapRenderer {
    
    /**
     * Maps distances of mesh model vertices to colors and renders the taken heatmap.
     * @param gl OpenGL context
     * @param model Mesh model to be rendered
     * @param heatMap 3D heat map
     * @param transparency transparency
     */
    public void drawMeshModel(GL2 gl, MeshModel model, HeatMap3D heatMap, float transparency) {
        for (MeshFacet f: model.getFacets()) {
            if (heatMap.getFacets().contains(f)) {
                renderMeshFacet(gl, f, heatMap, transparency);
            }
        }
    }
    
    /**
     * Maps distances of mesh facet to colors and renders the taken heatmap.
     * @param gl OpenGL context
     * @param facet Mesh facet
     * @param heatMap 3D heat map
     */
    public void renderMeshFacet(GL2 gl, MeshFacet facet, HeatMap3D heatMap, float transparency) {
        gl.glBegin(GL_TRIANGLES); //vertices are rendered as triangles

        for (MeshTriangle tri: facet) {
            for (int i = 0; i < 3; i++) {
                MeshPoint mPoint = switch (i) {
                    case 0 -> tri.getPoint1();
                    case 1 -> tri.getPoint2();
                    case 2 -> tri.getPoint3();
                    default -> throw new IllegalStateException("Unexpected value: " + i);
                };

                // render the normals
                Vector3d norm = mPoint.getNormal();
                if (norm != null) {
                    gl.glNormal3d(norm.x, norm.y, norm.z);
                }

                // render the vertices
                Point3d vert = mPoint.getPosition();

                //get color of vertex
                int vertexIndex = switch (i) {
                    case 0 -> tri.getIndex1();
                    case 1 -> tri.getIndex2();
                    case 2 -> tri.getIndex3();
                    default -> throw new IllegalStateException("Unexpected value: " + i);
                };
                Color c = heatMap.getColor(facet, vertexIndex);
                float[] ca = c.getComponents(null);
                ca[3] = transparency;

                gl.glColor4f(ca[0], ca[1], ca[2], ca[3]); // needed if SceneRenderer uses shaders
                gl.glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, ca, 0); // needed if SceneRenderer doesn't use shaders

                gl.glVertex3d(vert.x, vert.y, vert.z);
            }
        }

        /*
        // get the normal and tex coord. indices for face i
        for (int v = 0; v < facet.getCornerTable().getSize(); v++) {
            int vertexIndex = facet.getCornerTable().getRow(v).getVertexIndex();
            
            // render the normals
            Vector3d norm = facet.getVertices().get(vertexIndex).getNormal();
            if (norm != null) {
                gl.glNormal3d(norm.x, norm.y, norm.z);
            }
            
            // render the vertices
            Point3d vert = facet.getVertices().get(vertexIndex).getPosition();
            
            //get color of vertex
            Color c = heatMap.getColor(facet, vertexIndex);
            float[] ca = c.getComponents(null);
            ca[3] = transparency;

            gl.glColor4f(ca[0], ca[1], ca[2], ca[3]); // needed if SceneRenderer uses shaders
            gl.glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, ca, 0); // needed if SceneRenderer doesn't use shaders

            gl.glVertex3d(vert.x, vert.y, vert.z);
        }
         */

        gl.glEnd();
        gl.glPopAttrib();
    }
}
