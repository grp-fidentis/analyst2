package cz.fidentis.analyst.rendering;

import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Animates rotation and zooming of objects, e.g., a camera in a GLCanvas 
 * or 3D shapes in the scene.
 * 
 * @author Natalia Bebjakova 
 * @author Richard Pajersky
 * @author Radek Oslejsek
 */
public class RotationAnimator {
    
    /**
     * Frequency of the rotation or zoom animations
     */
    private static final int FPS = 60; 
    
    /**
     * The delay between the mouse press and the start of the animation.
     */
    private static final int SENSITIVITY = 50;
    
    /**
     * Animator used when the rotation or zoom buttons are pressed and held
     */
    private final FPSAnimator animator;
    
    /*
     * Animation timer
     */
    private TimerTask task;
    private Timer timer;
    
    private AnimationDirection direction = AnimationDirection.NONE;
    
    /**
     * Constructor.
     * 
     * @param glCanvas OpenGL canvas
     * @throws IllegalArgumentException if some argument is missing
     */
    public RotationAnimator(GLCanvas glCanvas) {
        if (glCanvas == null) {
            throw new IllegalArgumentException("glCanvas is null");
        }
        this.animator = new FPSAnimator(glCanvas, FPS, true);
    }
    
    /**
     * Constructor.
     */
    public RotationAnimator() {
        this.animator = new FPSAnimator(FPS, true);
    }

    /**
     * Starts the animation.This method is invoked after the mouse key is pressed and hold
     * 
     * @param dir Animation direction
     * @param animObj Object to be animated
     * @throws UnsupportedOperationException if there is unfinished animation
     */
    public void startAnimation(AnimationDirection dir, Animatable animObj) {
        if (this.direction != AnimationDirection.NONE) {
            throw new UnsupportedOperationException(); // this should no happen
        }
        
        animator.start();
        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                animObj.transform(direction);
            }
        };
        timer.schedule(task, SENSITIVITY, 100);
        
        this.direction = dir;        
    }
    
    /**
     * Stops the animation.This method is invoked after the mouse key is clicked only.
     */
    public void stopAnimation() {
        timer.cancel();
        animator.stop();
        this.direction = AnimationDirection.NONE;
    }
}
