package cz.fidentis.analyst.rendering;

import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask;
import cz.fidentis.analyst.drawables.*;
import cz.fidentis.analyst.engines.bbox.BoundingBoxConfig;
import cz.fidentis.analyst.engines.bbox.BoundingBoxServices;
import cz.fidentis.analyst.engines.face.FaceStateServices;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple 3D scene. The structure is as follows:
 * Drawable components of a single human face (mesh, symmetry plane, feature points. etc.)
 * are always stored at the same index (slot). One face can be labeled as "primary", one as "secondary".
 * Other faces have no label.
 * Then, there is a special list of so-called other drawables. The size of this list can differ.
 * List drawableCuttingPlanes contains all cutting planes.
 * 
 * @author Radek Oslejsek
 * @author Dominik Racek
 * @author Samuel Smoleniak
 */
public class Scene {
    
    private final List<DrawableFace> drawableFaces = new ArrayList<>();
    private final List<DrawableFeaturePoints> drawableFeaturePoints = new ArrayList<>();
    private final List<DrawablePlane> drawableSymmetryPlanes = new ArrayList<>();
    private final List<DrawableCuttingPlane> drawableCuttingPlanes = new ArrayList<>();
    private final List<DrawableInteractiveMask> drawableInteractiveMasks = new ArrayList<>();
    private final List<Drawable> otherDrawables = new ArrayList<>();
    
    private int primaryFaceSlot = -1;
    private int secondaryFaceSlot = -1;
    
    public static final int MAX_FACES_IN_SCENE = 20;
    
    /**
     * Removes all objects.
     */
    public void clearScene() {
        this.drawableFaces.clear();
        this.drawableFeaturePoints.clear();
        this.drawableSymmetryPlanes.clear();
        this.drawableCuttingPlanes.clear();
        this.otherDrawables.clear();
        primaryFaceSlot = -1;
        secondaryFaceSlot = -1;
    }

    /**
     * Removes all cutting and symmetry planes from scene.
     * Prepares slots for new symmetry planes.
     */
    public void clearCuttingPlanes() {
        this.drawableCuttingPlanes.clear();
        this.drawableSymmetryPlanes.clear();
        this.drawableSymmetryPlanes.add(null);
        this.drawableSymmetryPlanes.add(null);
    }

    /**
     * Removes all drawable interactive masks from scene.
     */
    public void clearDrawableInteractiveMasks() {
        this.drawableInteractiveMasks.clear();
        this.drawableInteractiveMasks.add(null);
    }

    /**
     * Computes and returns bounding box of faces (only) in the scene.
     *
     * @return a bounding box of faces (only) in the scene or {@code null}
     */
    public Box getBoundingBox() {
        return BoundingBoxServices.compute(
                getDrawableFaces().stream()
                        .map(df -> df.getHumanFace().getMeshModel().getFacets())
                        .flatMap(List::stream)
                        .toList(),
                new BoundingBoxConfig());
    }
    
    /**
     * Finds and returns a first free slot <b>for human face and its feature points and symmetry plane</b>.
     * Use {@link #getFreeSlotForOtherDrawables()} to get free slot of other objects.
     * 
     * @return a first free slot for human face and its feature points and symmetry plane.
     */
    public int getFreeSlotForFace() {
        int slot = -1;
        for (int i = 0; i < getArraySize(); i++) {
            if (this.drawableFaces.get(i) == null) {
                slot = i;
                break;
            }
        }
        if (slot == -1) {
            slot = getArraySize();
        }
        return (slot < Scene.MAX_FACES_IN_SCENE) ? slot : -1;
    }
    
    /**
     * Finds and returns a first free slot for other drawable objects.
     * @return a first free slot for other drawable objects.
     */
    public synchronized int getFreeSlotForOtherDrawables() {
        int slot = -1;
        for (int i = 0; i < this.otherDrawables.size(); i++) {
            if (this.otherDrawables.get(i) == null) {
                slot = i;
                break;
            }
        }
        if (slot == -1) {
            slot = otherDrawables.size();
        }
        return slot;
    }
    
    /**
     * Finds and returns a first free slot for drawable cutting plane.
     * @return a first free slot for drawable cutting plane.
     */   
    public synchronized int getFreeSlotForCuttingPlane() {
        int slot = -1;
        for (int i = 0; i < this.drawableCuttingPlanes.size(); i++) {
            if (this.drawableCuttingPlanes.get(i) == null) {
                slot = i;
                break;
            }
        }
        if (slot == -1) {
            slot = drawableCuttingPlanes.size();
        }
        return slot;
    }
    
    /**
     * Returns slot of the primary face or -1
     * @return slot of the primary face or -1
     */
    public int getPrimaryFaceSlot() {
        return this.primaryFaceSlot;
    }
    
    /**
     * Returns slot of the secondary face or -1
     * @return slot of the secondary face or -1
     */
    public int getSecondaryFaceSlot() {
        return this.secondaryFaceSlot;
    }
    
    /**
     * Sets the given face as primary.
     * @param slot slot of the face that should be set as primary
     */
    public void setFaceAsPrimary(int slot) {
        if (slot >= 0 && slot < getArraySize()) {
            this.primaryFaceSlot = slot;
        }
    }
    
    /**
     * Sets the given face as secondary.
     * @param slot slot of the face that should be set as secondary
     */
    public void setFaceAsSecondary(int slot) {
        if (slot >= 0 && slot < getArraySize()) {
            this.secondaryFaceSlot = slot;
        }
    }
    
    /**
     * Returns all "occupied" slots, i.e., indexes where some human face is stored.
     * @return "occupied" slots, i.e., indexes where some human face is stored.
     */
    public List<Integer> getFaceSlots() {
        List<Integer> ret = new ArrayList<>();
        for (int i = 0; i < this.drawableFaces.size(); i++) {
            if (this.drawableFaces.get(i) != null) {
                ret.add(i);
            }
        }
        return ret;
    }
    
    /**
     * Returns drawable face.
     * 
     * @param slot Slot of the face
     * @return drawable face or {@code null}
     */
    public DrawableFace getDrawableFace(int slot) {
        return (slot < 0 || slot >= getArraySize()) ? null : drawableFaces.get(slot);
    }
    
    /**
     * Sets the face and all its existing drawable components.   
     * If the face is {@code null}, then the drawable face and all its components are removed.
     * 
     * @param slot Slot of the face
     * @param face New face or {@code null}
     * @return {@code true} if all drawable components (face, feature points, symmetry plane, etc.) 
     * have been set successfully.
     */
    public boolean setHumanFace(int slot, HumanFace face) {
        return setDrawableFace(slot, face) &&
                setDrawableFeaturePoints(slot, face) &&
                setDrawableSymmetryPlane(slot, face);
    }
    
    /**
     * Sets the drawable face (mesh). If the face is {@code null}, then the drawable face is removed.
     *
     * @param slot Slot of the face
     * @param face New face or {@code null}
     * @return {@code true} on success.
     */
    public boolean setDrawableFace(int slot, HumanFace face) {
        if (slot < 0 || slot >= Scene.MAX_FACES_IN_SCENE) {
            return false;
        } else if (face == null) { // remove
            if (slot >= getArraySize()){
                return false;
            } else {
                this.drawableFaces.set(slot, null);
                return true;
            }
        } else if (!prepareArrays(slot)) {
            return false;
        }        

        DrawableFace origDrFace = drawableFaces.get(slot);
        DrawableFace newDrFace = new DrawableFace(face);
        if (origDrFace != null) {
            newDrFace.setColor(origDrFace.getColor());
            newDrFace.setTransparency(origDrFace.getTransparency());
        }
        drawableFaces.set(slot, newDrFace);
        return true;
    }
    
    /**
     * Sets the drawable feature points of the face. 
     * If the face is {@code null}, then the drawable feature points are removed.
     *
     * @param slot Slot of the face
     * @param face New face or {@code null}
     * @return {@code true} on success.
     */
    public boolean setDrawableFeaturePoints(int slot, HumanFace face) {
        if (slot < 0 || slot >= Scene.MAX_FACES_IN_SCENE) {
            return false;
        } else if (face == null || face.getAllLandmarks() == null) { // remove
            if (slot >= getArraySize()){
                return false;
            } else {
                this.drawableFeaturePoints.set(slot, null);
                return true;
            }
        } else if (!prepareArrays(slot)) {
            return false;
        }        
        
        drawableFeaturePoints.set(slot, new DrawableFeaturePoints(face.getAllLandmarks()));
        return true;
    }
    
    /**
     * Sets the drawable symmetry plane of the face. 
     * If the face is {@code null}, then the drawable symmetry plane is removed.
     *
     * @param slot Slot of the face
     * @param face New face or {@code null}
     * @return {@code true} on success.
     */
    public boolean setDrawableSymmetryPlane(int slot, HumanFace face) {
        if (slot < 0 || slot >= Scene.MAX_FACES_IN_SCENE) {
            return false;
        } else if (face == null || face.getSymmetryPlane() == null) { // remove
            if (slot >= getArraySize()){
                return false;
            } else {
                this.drawableSymmetryPlanes.set(slot, null);
                return true;
            }
        } else if (!prepareArrays(slot)) {
            return false;
        }
        
        if (drawableSymmetryPlanes.get(slot) == null) { // add new
            FaceStateServices.updateBoundingBox(face, FaceStateServices.Mode.COMPUTE_IF_ABSENT);
            drawableSymmetryPlanes.set(slot, new DrawablePlane(face.getSymmetryPlane(), face.getBoundingBox(), false));
        } else { // replace existing
            drawableSymmetryPlanes.get(slot).setPlane(face.getSymmetryPlane());
        }
        
        return true;
    }
    
    /**
     * Sets drawable interactive mask.
     * If the face is {@code null} or has not a valid mask, then the drawable mask is removed.
     * 
     * @param slot Slot of the drawable cutting plane
     * @param mask New face mask or {@code null}
     * @return {@code true} on success.
     */
    public boolean setDrawableInteractiveMask(int slot, SurfaceMask mask) {
        if (slot < 0 || slot >= Scene.MAX_FACES_IN_SCENE) {
            return false;
        } else if (mask == null || mask.getPoints().isEmpty()) { // remove
            if (slot >= getArraySize()){
                return false;
            } else {
                drawableInteractiveMasks.set(slot, null);
                return true;
            }
        } 
     
        drawableInteractiveMasks.add(slot, new DrawableInteractiveMask(mask));
        return true;
    }
    
    /**
     * Sets other drawable. 
     * If the drawable is {@code null}, then the drawable is removed.
     *
     * @param slot Slot of the drawable
     * @param dr Drawable
     * @return {@code true} on success.
     */
    public boolean setOtherDrawable(int slot, Drawable dr) {
        if (slot < 0 || slot >= Scene.MAX_FACES_IN_SCENE) {
            return false;
        } else if (dr == null) { // remove
            if (slot >= otherDrawables.size()){
                return false;
            } else {
                otherDrawables.set(slot, null);
                return true;
    }
        } 

        for (int i = otherDrawables.size(); i <= slot; i++) {
            otherDrawables.add(null);
        }
        otherDrawables.set(slot, dr);
        return true;
    }

    /**
     * Sets drawable cutting plane. 
     * If the drawable cutting plane is {@code null}, then the cutting plane is removed.
     *
     * @param slot Slot of the drawable cutting plane
     * @param cuttingPlane Drawable cutting plane
     * @return {@code true} on success.
     */
    public boolean setDrawableCuttingPlane(int slot, DrawableCuttingPlane cuttingPlane) {
        if (slot < 0 || slot >= Scene.MAX_FACES_IN_SCENE) {
            return false;
        } else if (cuttingPlane == null) { // remove
            if (slot >= drawableCuttingPlanes.size()){
                return false;
            } else {
                drawableCuttingPlanes.set(slot, null);
                return true;
            }
        } 
        
        for (int i = drawableCuttingPlanes.size(); i <= slot; i++) {
            drawableCuttingPlanes.add(null);
        }
        drawableCuttingPlanes.set(slot, cuttingPlane);
        return true;
    }
    
    /**
     * Sets default colors of faces and feature points
     */
    public final void setDefaultColors() {
        for (int i = 0; i < getArraySize(); i++) {
            if (drawableFaces.get(i) != null) {
                if (i == this.primaryFaceSlot) {
                    drawableFaces.get(i).setColor(DrawableFace.SKIN_COLOR_PRIMARY);
                } else if (i == this.secondaryFaceSlot) {
                    drawableFaces.get(i).setColor(DrawableFace.SKIN_COLOR_SECONDARY);
                } else {
                    drawableFaces.get(i).setColor(DrawableFace.SKIN_COLOR_DEFAULT);
                }
            }
            if (drawableFeaturePoints.get(i) != null) {
                if (i == this.primaryFaceSlot) {
                    drawableFeaturePoints.get(i).setColor(getColorOfFeaturePoints(DrawableFace.SKIN_COLOR_PRIMARY));
                } else if (i == this.secondaryFaceSlot) {
                    drawableFeaturePoints.get(i).setColor(getColorOfFeaturePoints(DrawableFace.SKIN_COLOR_SECONDARY));
                } else {
                    drawableFeaturePoints.get(i).setColor(getColorOfFeaturePoints(DrawableFace.SKIN_COLOR_DEFAULT));
                }
            }
        }
    }

    /**
     * Returns drawable feature points.
     * 
     * @param slot Slot of the face
     * @return drawable face or {@code null}
     */
    public DrawableFeaturePoints getDrawableFeaturePoints(int slot) {
        return (slot >= 0 && slot < getArraySize()) ? drawableFeaturePoints.get(slot) : null;
    }
    
    /**
     * Returns drawable symmetry plane.
     * 
     * @param slot Slot of the face
     * @return drawable plane or {@code null}
     */
    public DrawablePlane getDrawableSymmetryPlane(int slot) {
        return (slot >= 0 && slot < getArraySize()) ? drawableSymmetryPlanes.get(slot) : null;
    }
    
    /**
     * Showing or hiding the face (its mesh)
     *
     * @param slot Slot of the face
     * @param show determines whether to hide or show the object
     */
    public void showDrawableFace(int slot, boolean show) {
        if (slot >= 0 && slot < getArraySize() && this.drawableFaces.get(slot) != null) {
            this.drawableFaces.get(slot).show(show);
        }
    }

    /**
     * Showing or hiding the feature points
     *
     * @param slot Slot of the face
     * @param show determines whether to hide or show the object
     */
    public void showFeaturePoints(int slot, boolean show) {
        if (slot >= 0 && slot < getArraySize() && this.drawableFeaturePoints.get(slot) != null) {
            this.drawableFeaturePoints.get(slot).show(show);
        }
    }
    
    /**
     * Showing or hiding the symmetry plane
     *
     * @param slot Slot of the face
     * @param show determines whether to hide or show the object
     */
    public void showSymmetryPlane(int slot, boolean show) {
        if (slot >= 0 && slot < getArraySize() && this.drawableSymmetryPlanes.get(slot) != null) {
            this.drawableSymmetryPlanes.get(slot).show(show);
        }
    }
    
    /**
     * showing or hiding the interactive mask
     * @param slot Slot of the face
     * @param show determines whether to hide or show object
     */
    public void showInteractiveMask(int slot, boolean show) {
        if (slot >= 0 && slot < getArraySize() && this.drawableInteractiveMasks.get(slot) != null) {
            this.drawableInteractiveMasks.get(slot).show(show);
        }
    }
    /**
     * Returns other drawable object.
     * 
     * @param slot Slot of the drawable object
     * @return drawable object or {@code null}
     */
    public Drawable getOtherDrawable(int slot) {
        return (slot >= 0 && slot < this.otherDrawables.size()) ? this.otherDrawables.get(slot) : null;
    }
    
    /**
     * Returns drawable cutting plane object.
     * 
     * @param slot Slot of the cutting plane
     * @return drawable cutting plane or {@code null}
     */
    public DrawableCuttingPlane getDrawableCuttingPlane(int slot) {
        return (slot >= 0 && slot < this.drawableCuttingPlanes.size()) ? this.drawableCuttingPlanes.get(slot) : null;
    }
    
    /**
     * returns drawable interactive mask
     * @param slot Slot of the interactive mask
     * @return drawable interactive mask
     */
    public DrawableInteractiveMask getDrawableInteractiveMask(int slot) {
         return (slot >= 0 && slot < this.drawableInteractiveMasks.size()) ? this.drawableInteractiveMasks.get(slot) : null;
    }
        
    /**
     * Returns all drawable objects.
     * 
     * @return all drawable objects.
     */
    public List<Drawable> getAllDrawables() {
        List<Drawable> ret = new ArrayList<>();
        ret.addAll(this.drawableFaces);
        ret.addAll(this.drawableFeaturePoints);
        ret.addAll(this.drawableSymmetryPlanes);
        ret.addAll(this.drawableCuttingPlanes);
        ret.addAll(this.drawableInteractiveMasks);
        ret.addAll(this.otherDrawables);
        while (ret.remove(null)) {}
        return ret;
    }
    
    public List<DrawableFace> getDrawableFaces() {
        return Collections.unmodifiableList(this.drawableFaces);
    }
    
    public List<DrawableCuttingPlane> getCuttingPlanes() {
        return Collections.unmodifiableList(this.drawableCuttingPlanes);
    }
    
    protected Color getColorOfFeaturePoints(Color origColor) {
        return new Color(
                Math.min(255, origColor.getRed() + 40),
                Math.min(255, origColor.getGreen() + 40),
                Math.min(255, origColor.getBlue() + 40)
        );
    }
    
    protected boolean prepareArrays(int slot) {
        if (slot >= Scene.MAX_FACES_IN_SCENE) {
            return false;
        }
        
        // extend the 
        for (int i = getArraySize(); i <= slot; i++) {
            this.drawableFaces.add(null);
            this.drawableFeaturePoints.add(null);
            this.drawableSymmetryPlanes.add(null);
            this.drawableCuttingPlanes.add(null);
            this.drawableInteractiveMasks.add(null);
        }
        
        return true;
    }

    protected int getArraySize() {
        return this.drawableFaces.size();
    }
    
    @Override
    public String toString() {
        return this.drawableFaces.toString();
    }
}
