package cz.fidentis.analyst.rendering;

import cz.fidentis.analyst.data.shapes.Box;

import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.io.Serial;
import java.io.Serializable;

/**
 * OpenGL code related to the camera.
 * 
 * @author Natalia Bebjakova 
 * @author Radek Oslejsek
 */
public class Camera implements Animatable, Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private static final double ANIMATION_ROTATION = 2.0;
    private static final double ANIMATION_ZOOM = 3.0;

    /**
     * FOVY in degrees
     */
    public static final int FIELD_OF_VIEW = 50;
    public static final float Z_NEAR = 0.1f;
    public static final float Z_FAR = 4500.0f;
    public static final Vector3f POSITION = new Vector3f(0.0f, 0.0f, -1400.0f);

    /**
     * When the proper distance of the camera is found based on scene bounding box,
     * the objects may be unnecessary small. This zooms them in.
     */
    public static final double SCALE_FACTOR = 1.2d;

    private final Vector3d center = new Vector3d(0, 0, 0);
    private final Vector3d currentPosition = new Vector3d(0, 0, 300);
    private final Vector3d upDirection = new Vector3d(0, 1, 0);

    /**
     * Zooms the camera to fit all human faces in the scene
     * @param scene a scene
     */
    public void zoomToFit(Scene scene) {
        Box bbox = scene.getBoundingBox();
        if (bbox != null) {
            zoomToFit(bbox);
        }
    }

    /**
     * Constructs a deep copy of the camera.
     *
     * @return deep copy of the camera
     */
    public Camera copy() {
        Camera camera = new Camera();
        camera.center.set(center);
        camera.currentPosition.set(currentPosition);
        camera.upDirection.set(upDirection);
        return camera;
    }

    /**
     * Zooms the camera to fit the given bounding box
     * @param bbox a bounding box
     */
    public void zoomToFit(Box bbox) {
        Vector3d sceneSize = new Vector3d(bbox.maxPoint());
        sceneSize.sub(bbox.minPoint());
        double maxSceneDim = Math.max(sceneSize.x, Math.max(sceneSize.y, sceneSize.z));

        // Calculate new camera distance based on the bounding box dimensions
        double radians = FIELD_OF_VIEW * Math.PI / 180;
        double cameraDistance = maxSceneDim / (SCALE_FACTOR * Math.tan(radians / 2.0d));

        this.upDirection.set(0, 1, 0);
        this.center.set(bbox.midPoint());
        this.currentPosition.set(center.x, center.y, center.z + cameraDistance);
    }

    /**
     * Zooms the camera to fit the given bounding box, perpendicular to the normal
     * @param bbox a bounding box
     * @param normal a normal
     */
    public void zoomToFitPerpendicular(Box bbox, Vector3d normal) {
        Vector3d sceneSize = new Vector3d(bbox.maxPoint());
        sceneSize.sub(bbox.minPoint());
        double maxSceneDim = Math.max(sceneSize.x, Math.max(sceneSize.y, sceneSize.z));

        // Calculate new camera distance based on the bounding box dimensions
        double radians = FIELD_OF_VIEW * Math.PI / 180;
        double cameraDistance = maxSceneDim / (SCALE_FACTOR * Math.tan(radians / 2.0d));

        this.center.set(bbox.midPoint());

        // Calculate up direction perpendicular to the normal
        Vector3d newUpDirection = new Vector3d();
        newUpDirection.cross(normal, new Vector3d(0, 0, 1));
        newUpDirection.normalize();
        if (newUpDirection.y < 0) {
            newUpDirection.negate();
        }

        this.upDirection.set(newUpDirection);

        // Calculate current position based on distance, so that it's perpendicular to the normal and up direction
        Vector3d currentPositionDirection = new Vector3d();
        currentPositionDirection.cross(normal, newUpDirection);
        currentPositionDirection.normalize();
        if (currentPositionDirection.z < 0) {
            currentPositionDirection.negate();
        }
        currentPositionDirection.scale(cameraDistance);

        this.currentPosition.set(center);
        this.currentPosition.add(currentPositionDirection);
    }

    /**
     * Returns center of the scene.
     * 
     * @return center of the scene.
     */
    public Vector3d getCenter() {
        return new Vector3d(center);
    }
    
    /**
     * Returns camera position.
     * 
     * @return camera position.
     */
    public Vector3d getPosition() {
        return new Vector3d(currentPosition);
    }
    
    /**
     * Returns camera orientation (the up direction).
     * 
     * @return camera orientation (the up direction).
     */
    public Vector3d getUpDirection() {
        return new Vector3d(upDirection);
    }

    /**
     * Rotates the camera up.
     * 
     * @param degree degree of rotation
     */
    public void rotateUp(double degree) {
        rotate(-degree, 0);
    }

    /**
     * Rotates the camera down.
     * 
     * @param degree degree of rotation
     */
    public void rotateDown(double degree) {
        rotate(degree, 0);
    }

    /**
     * Rotates the camera left.
     * 
     * @param degree degree of rotation
     */
    public void rotateLeft(double degree) {
        rotate(0, degree);
    }

    /**
     * Rotates the camera right.
     * @param degree degree of rotation
     */
    public void rotateRight(double degree) {
        rotate(0, -degree);
    }

    /**
     * Returns normalized direction of the X axis.
     * 
     * @return normalized direction of the X axis.
     */
    private Vector3d getXaxis() {
        Vector3d axis = new Vector3d(
                (currentPosition.y - center.y) * upDirection.z - (currentPosition.z - center.z) * upDirection.y,
                (currentPosition.z - center.z) * upDirection.x - (currentPosition.x - center.x) * upDirection.z,
                (currentPosition.x - center.x) * upDirection.y - (currentPosition.y - center.y) * upDirection.x);
        axis.normalize();
        return axis;
    }

    /**
     * Returns normalized direction of the Y axis.
     * 
     * @return normalized direction of the Y axis.
     */
    private Vector3d getYaxis() {
        return new Vector3d(upDirection.x, upDirection.y, upDirection.z);
    }

    /**
     * Rotates object around axes that appear as horizontal and vertical axes on
     * the screen (parallel to the screen edges), intersecting at the center of
     * the screen (i.e., head center).
     *
     * @param xAngle angle around vertical axis on the screen
     * @param yAngle angle around horizontal axis on the screen
     */
    public void rotate(double xAngle, double yAngle) {
        Vector3d xAxis = getXaxis();
        Vector3d yAxis = getYaxis();
        
        Vector3d newPos = rotateAroundAxis(currentPosition, xAxis, Math.toRadians(xAngle));
        newPos = rotateAroundAxis(newPos, yAxis, Math.toRadians(yAngle));
        this.currentPosition.set(newPos);
        
        Vector3d newUp = rotateAroundAxis(upDirection, xAxis, Math.toRadians(xAngle));
        newUp = rotateAroundAxis(newUp, yAxis, Math.toRadians(yAngle));
        newUp.normalize();
        this.upDirection.set(newUp);
    }

    /**
     * Moves the camera.
     * 
     * @param xShift xShift
     * @param yShift yShift
     */
    public void move(double xShift, double yShift) {
        Vector3d yAxis = getYaxis();
        yAxis.scale(yShift);
        
        Vector3d shift = getXaxis();
        shift.scale(xShift);
        shift.add(yAxis);
        
        Vector3d camera = new Vector3d(currentPosition);
        camera.add(shift);
        
        this.center.add(shift);
        this.currentPosition.set(camera);
    }

    /**
     * Calculate the new position f point from given angle and rotation axis.
     *
     * @param point original position
     * @param u vector of rotation axis
     * @param angle angle of rotation
     * @return new position
     */
    public Vector3d rotateAroundAxis(Tuple3d point, Vector3d u, double angle) {
        double x = ((Math.cos(angle) + u.x * u.x * (1 - Math.cos(angle))) * point.x
                + (u.x * u.y * (1 - Math.cos(angle)) - u.z * Math.sin(angle)) * point.y
                + (u.x * u.z * (1 - Math.cos(angle)) + u.y * Math.sin(angle)) * point.z);
        double y = ((u.x * u.y * (1 - Math.cos(angle)) + u.z * Math.sin(angle)) * point.x
                + (Math.cos(angle) + u.y * u.y * (1 - Math.cos(angle))) * point.y
                + (u.y * u.z * (1 - Math.cos(angle)) - u.x * Math.sin(angle)) * point.z);
        double z = ((u.x * u.z * (1 - Math.cos(angle)) - u.y * Math.sin(angle)) * point.x
                + (u.y * u.z * (1 - Math.cos(angle)) + u.x * Math.sin(angle)) * point.y
                + (Math.cos(angle) + u.z * u.z * (1 - Math.cos(angle))) * point.z);
        return new Vector3d(x, y, z);
    }

    /**
     * Zooms in.
     * 
     * @param distance Distance to be zoom in
     */
    public void zoomIn(double distance) {
        Vector3d v = new Vector3d(currentPosition);
        v.sub(center);
        double len = v.length();
        if (len > 0) {
            this.currentPosition.set(v);
            this.currentPosition.scale((len - distance) / len);
            this.currentPosition.add(center);
        }
    }

    /**
     * Zooms out.
     * 
     * @param distance Distance to be zoom out
     */
    public void zoomOut(double distance) {
        Vector3d v = new Vector3d(currentPosition);
        v.sub(center);
        double len = v.length();
        if (len == 0) {
            len = 1;
        }
        
        this.currentPosition.set(v);
        this.currentPosition.scale((len + distance) / len);
        this.currentPosition.add(center);
    }
    
    @Override
    public void transform(AnimationDirection dir) {
        switch (dir) {
            case ROTATE_LEFT -> rotateLeft(ANIMATION_ROTATION);
            case ROTATE_RIGHT -> rotateRight(ANIMATION_ROTATION);
            case ROTATE_UP -> rotateUp(ANIMATION_ROTATION);
            case ROTATE_DOWN -> rotateDown(ANIMATION_ROTATION);
            case ZOOM_IN -> zoomIn(ANIMATION_ZOOM);
            case ZOOM_OUT -> zoomOut(ANIMATION_ZOOM);
            default -> {
            }
        }
    }

    @Override
    public String toString() {
        return "Camera position: " + currentPosition + System.lineSeparator() +
                "Looks at: " + center + System.lineSeparator() +
                "Up direction: " + upDirection;
    }
}
