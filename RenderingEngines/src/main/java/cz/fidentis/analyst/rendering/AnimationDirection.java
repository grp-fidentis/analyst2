package cz.fidentis.analyst.rendering;

/**
 * Animation direction.
 * 
 * @author Richard Pajersky
 */
public enum AnimationDirection {
        ROTATE_LEFT,
        ROTATE_RIGHT,
        ROTATE_UP,
        ROTATE_DOWN,
        ROTATE_IN,
        ROTATE_OUT,
        ZOOM_IN,
        ZOOM_OUT,
        TRANSLATE_LEFT,
        TRANSLATE_RIGHT,
        TRANSLATE_UP,
        TRANSLATE_DOWN,
        TRANSLATE_IN,
        TRANSLATE_OUT,
        NONE
}
