package cz.fidentis.analyst.rendering;


import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.glu.GLU;
import cz.fidentis.analyst.Logger;
import cz.fidentis.analyst.drawables.Drawable;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.drawables.renderers.RenderingService;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4f;
import java.awt.*;
import java.nio.FloatBuffer;
import java.util.Collection;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL2.*;
import static com.jogamp.opengl.GL2ES1.GL_LIGHT_MODEL_TWO_SIDE;
import static com.jogamp.opengl.GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT;
import static com.jogamp.opengl.GL2ES2.GL_DEPTH_COMPONENT;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_LIGHTING;

/**
 * Handles {@code DrawableMesh}s - objects to be drawn, and (re-)renders them on demand. 
 * 
 * @author Natalia Bebjakova
 * @author Radek Oslejsek
 * @author Richard Pajersky
 * @author Ondrej Simecek
 */
public class SceneRenderer {

    public static final Color BRIGHT_BACKGROUND = new Color(0.9f, 0.9f, 0.9f);
    public static final Color DARK_BACKGROUND = new Color(0.25f, 0.25f, 0.25f);
    
    private final GLU glu = new GLU();
    private GL2 gl;
    private GLContext glContext;
    
    private boolean brightBackground = false;
    
    private ShadersManager shadersManager;

    /**
     * The position of the scene's light in homogenous object coordinates.
     */
    private static final float[] LIGHT_POSITION = {0, 0, 300, 1};

    // Data required for casting a ray through a pixel:
    private final float[] projectionMatrix = new float[16];
    private final float[] modelviewMatrix = new float[16];
    private final int[] viewport = new int[4];

    /**
     * Initialization method - must be called first.
     * 
     * @param glContext An OpenGL context. Must not be {@code null}, must be at least GL2
     * @throws IllegalArgumentException if the {@code gl} is null
     */
    public void initGLContext(GLContext glContext) {
        if (!glContext.isGL2()) {
            throw new IllegalArgumentException("glContext");
        }
        this.glContext = glContext;
        this.gl = glContext.getGL().getGL2();

        try {
            shadersManager = new ShadersManager(glContext, glu);
        } catch(GLException ex) {
            Logger.print("[WARNING] Initiation of shaders failed. Some visual effects will not be available.");
            Logger.print("[WARNING] " + ex);
        }
        
        gl.setSwapInterval(1);
        gl.glEnable(GL_LIGHTING);
        gl.glEnable(GL_LIGHT0);
        gl.glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION, 0);
        gl.glClearColor(0,0,0,0);     // background for GLCanvas
        gl.glClear(GL_COLOR_BUFFER_BIT);
        
        gl.glShadeModel(GL_SMOOTH);    // use smooth shading

        gl.glDepthFunc(hasShaders() ? GL_ALWAYS : GL_LESS);
        gl.glDepthRange(0.0, 1.0);
        gl.glEnable(GL_DEPTH_TEST);

        gl.glEnable(GL_NORMALIZE);
        gl.glDisable(GL_CULL_FACE);
        
        gl.glEnable(GL_BLEND);    // enable transparency
        gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        gl.glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE); //turn on two-sided lighting

        gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    }
    
    /**
     * Sets the scene background to bright.
     */
    public void setBrightBackground() {
        this.brightBackground = true;
    }
    
    /**
     * Sets the scene background to dark.
     */
    public void setDarkBackground() {
        this.brightBackground = false;
    }
    
    /**
     * Sets the view-port.
     * 
     * @param x X corner
     * @param y Y corner
     * @param width Width 
     * @param height Height
     */
    public void setViewport(int x, int y, int width, int height) {
        if (height == 0) {
            height = 1;    // to avoid division by 0 in aspect ratio below
        }
        gl.glViewport(x, y, width, height);  // size of drawing area

        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(Camera.FIELD_OF_VIEW, width / (float) height, Camera.Z_NEAR, Camera.Z_FAR);
        
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslatef(Camera.POSITION.x, Camera.POSITION.y, Camera.POSITION.z);
        
        gl.glGetIntegerv(GL_VIEWPORT, viewport, 0);
    }
    
    /**
     * Renders drawable objects.
     */
    public void renderScene(Camera camera, Collection<Drawable> drawables) {
        clearScene();

        setPosition(camera);

        gl.glEnable(GL_LIGHTING);

        if (hasShaders()) {
            gl.glMatrixMode(GL_TEXTURE);
            gl.glActiveTexture(GL_TEXTURE0);
            gl.glLoadIdentity();
            gl.glMatrixMode(GL_MODELVIEW);
            shadersManager.renderObjects(drawables, getViewportWidth(), getViewportHeight(), new Vector4f(LIGHT_POSITION));
        } else { // no shaders available
            drawables.stream()
                    .filter(Drawable::isShown)
                    .sorted(new Drawable.TransparencyComparator())
                    .forEach(drawable -> RenderingService.render(gl, drawable));
        }
        
        gl.glFlush();

        // Store data needed for casting a ray through a pixel:
        gl.glGetFloatv(GL_PROJECTION_MATRIX, projectionMatrix, 0);
        gl.glGetFloatv(GL_MODELVIEW_MATRIX, modelviewMatrix, 0);
    }
    
    /**
     * gets view port width in pixels
     * @return view port width in pixels
     */
    public int getViewportWidth() {
        return viewport[2];
    }
    
    /**
     * gets view port height in pixels
     * @return view port height in pixels
     */
    public int getViewportHeight() {
        return viewport[3];
    }

    /**
     * Sets model to proper position
     * 
     * @param camera Camera
     */
    private void setPosition(Camera camera) {
        Vector3d currentPosition = camera.getPosition();
        Vector3d center = camera.getCenter();
        Vector3d upDirection = camera.getUpDirection();
        glu.gluLookAt(currentPosition.x, currentPosition.y, currentPosition.z,  
                center.x, center.y, center.z,
                upDirection.x, upDirection.y, upDirection.z);
    }
    
    /**
     * Clears the scene and prepares it for the re-drawing.
     */
    protected void clearScene() { // join with the renderScene() ???
        if (brightBackground) {
            gl.glClearColor(
                    BRIGHT_BACKGROUND.getRed()   / 255f, 
                    BRIGHT_BACKGROUND.getGreen() / 255f, 
                    BRIGHT_BACKGROUND.getBlue()  / 255f, 
                    0.0f);
        } else {
            gl.glClearColor(
                    DARK_BACKGROUND.getRed()   / 255f, 
                    DARK_BACKGROUND.getGreen() / 255f, 
                    DARK_BACKGROUND.getBlue()  / 255f, 
                    0.0f);
        }
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();
    }
    
    /**
     * Gets the current manager of shaders
     * @return shaders manager
     */
    public ShadersManager getShadersManager() {
        return shadersManager;
    }

    /**
     * Returns {@code true}, if OpenGL shaders are available
     * @return {@code true}, if OpenGL shaders are available
     */
    public boolean hasShaders() {
        return shadersManager != null;
    }

    /**
     * Cast a ray through a pixel under the mouse pointer.
     *
     * @param pixelX The X coordinate of mouse pointer
     * @param pixelY The X coordinate of mouse pointer
     * @param camera Camera
     * @return A ray directing towards the scene
     */
    public Ray castRayThroughPixel(int pixelX, int pixelY, Camera camera) {
        float winY = viewport[3] - (float) pixelY;
        FloatBuffer winZ = FloatBuffer.allocate(1);
        gl.glReadPixels(pixelX, (int) winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, winZ);

        float[] worldCoords = new float[3];
        glu.gluUnProject(
                pixelX, winY, winZ.get(0),
                modelviewMatrix, 0,
                projectionMatrix, 0,
                viewport, 0,
                worldCoords, 0);

        Vector3d dir = new Vector3d(worldCoords[0], worldCoords[1], worldCoords[2]);
        dir.sub(camera.getPosition());
        dir.normalize();
        return new Ray(new Point3d(camera.getPosition()), dir);
    }
}
