package cz.fidentis.analyst.rendering;

/**
 * The ability to being used in animations and transitions.
 * 
 * @author Radek Oslejsek
 */
public interface Animatable {
    
    /**
     * Animation step.
     * 
     * @param dir Transformation direction
     */
    void transform(AnimationDirection dir);
}
