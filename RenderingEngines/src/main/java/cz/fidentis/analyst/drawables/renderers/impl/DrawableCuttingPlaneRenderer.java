package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.drawables.DrawableCuttingPlane;

/**
 * Renderer for DrawableCuttingPlane.
 *
 * @author Radek Oslejsek
 * @author Dominik Racek
 */
public class DrawableCuttingPlaneRenderer extends DrawablePlaneRenderer<DrawableCuttingPlane> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawableCuttingPlane.class;
    }

    @Override
    protected void renderObject(GL2 gl, DrawableCuttingPlane drawableCuttingPlane) {
        renderObject(gl, drawableCuttingPlane.getPlane().getMesh(drawableCuttingPlane.getBBox()), drawableCuttingPlane.isShownBBobx(), drawableCuttingPlane.getBBox());
        if (drawableCuttingPlane.isMirrorPlaneShown()) {
            renderObject(gl, drawableCuttingPlane.getMirrorPlane().getMesh(drawableCuttingPlane.getBBox()), drawableCuttingPlane.isShownBBobx(), drawableCuttingPlane.getBBox());
        }
    }
}
