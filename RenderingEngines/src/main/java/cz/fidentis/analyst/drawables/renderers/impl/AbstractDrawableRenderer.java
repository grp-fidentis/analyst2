package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import cz.fidentis.analyst.drawables.Drawable;

import java.awt.*;

/**
 * Abstract renderer for rendering generic Drawable
 *
 * @param <T> type that extends Drawable which should be rendered using implemented renderer
 * @author Radek Oslejsek
 * @author Richard Pajersky
 * @author Marek Seďa
 */
public abstract class AbstractDrawableRenderer<T extends Drawable> {

    protected static final GLU GLU_CONTEXT = new GLU();

    /**
     * Default constructor.
     */
    public AbstractDrawableRenderer() {

    }

    /**
     *
     * @return class that this instance of renderer can render
     */
    abstract Class<?> getDrawableClass();

    /**
     * Renders the scene.
     *
     * @param gl OpenGL context
     */
    public void render(GL2 gl, T drawable) {
        initRendering(gl, drawable);
        renderObject(gl, drawable);
        finishRendering(gl, drawable);
    }

    protected void initRendering(GL2 gl, T drawable) {
        setPolygonMode(gl, drawable);

        Color color = drawable.getColor();
        // set color
        float[] rgba = {color.getRed() / 255f, color.getGreen() / 255f,
                color.getBlue() / 255f, drawable.getTransparency()};
        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, rgba, 0);

    }

    protected abstract void renderObject(GL2 gl, T drawable);

    protected void finishRendering(GL2 gl, T drawable) {
    }

    protected void setPolygonMode(GL2 gl, T drawable) {
        gl.glShadeModel(GL2.GL_SMOOTH);
        switch (drawable.getRenderMode()) {
            case WIREFRAME:
                gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_LINE);
                break;
            case POINT_CLOUD:
                gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_POINT);
                break;
            default:
            case TEXTURE:
            case SMOOTH:
                gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
                break;
        }
    }
}
