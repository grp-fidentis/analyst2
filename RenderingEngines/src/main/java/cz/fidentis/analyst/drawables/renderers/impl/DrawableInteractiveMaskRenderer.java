package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.CrossSection3D;
import cz.fidentis.analyst.data.shapes.Cylinder;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask;
import cz.fidentis.analyst.drawables.DrawableInteractiveMask;
import cz.fidentis.analyst.engines.cut.CrossSectionConfig;
import cz.fidentis.analyst.engines.cut.CrossSectionServices;

import javax.vecmath.Point3d;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.max;
import static java.lang.Double.min;


/**
 * Renderer for DrawableInteractiveMask.
 *
 * @author Mario Chromik
 */
public class DrawableInteractiveMaskRenderer extends AbstractDrawableRenderer<DrawableInteractiveMask> {

    private final double defaultRadius = 1;

    private final double sampledRadius = 0.75;

    private final Color defaultColor = Color.WHITE;

    private final Color selectedColor = Color.GREEN;

    /**
     * legacy expression determining if point is within bounding box
     *
     * @param point point to check
     * @param box   box to check
     * @return true if outside else false
     */
    protected static boolean isPointOutsideBox(Point3d point, Box box) {
        return point.x < box.minPoint().x || point.y < box.minPoint().y || point.z < box.minPoint().z ||
                point.x > box.maxPoint().x || point.y > box.maxPoint().y || point.z > box.maxPoint().z;
    }

    @Override
    public Class<?> getDrawableClass() {
        return DrawableInteractiveMask.class;
    }

    @Override
    protected void renderObject(GL2 gl, DrawableInteractiveMask drawableInteractiveMask) {
        drawableInteractiveMask.getColor();
        float[] rgba = {
                defaultColor.getRed() / 255f,
                defaultColor.getGreen() / 255f,
                defaultColor.getBlue() / 255f,
                defaultColor.getTransparency()};

        float[] rgbaSelected = {
                selectedColor.getRed() / 255f,
                selectedColor.getGreen() / 255f,
                selectedColor.getBlue() / 255f,
                selectedColor.getTransparency()};
        gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, rgba, 0);

        // draw mask points
        for (SurfaceMask.MaskPoint3D maskPoint : drawableInteractiveMask.getMask().getPoints()) {
            if (maskPoint.getIsSelected()) {
                gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, rgbaSelected, 0);
            } else {
                gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, rgba, 0);
            }
            gl.glPushMatrix();
            gl.glTranslated(maskPoint.getPosition().x, maskPoint.getPosition().y, maskPoint.getPosition().z);
            GLUquadric center = GLU_CONTEXT.gluNewQuadric();
            GLU_CONTEXT.gluQuadricDrawStyle(center, GLU.GLU_FILL);
            GLU_CONTEXT.gluQuadricNormals(center, GLU.GLU_FLAT);
            GLU_CONTEXT.gluQuadricOrientation(center, GLU.GLU_OUTSIDE);
            GLU_CONTEXT.gluSphere(center, maskPoint.getIsSampled() ? sampledRadius : defaultRadius, 16, 16);
            GLU_CONTEXT.gluDeleteQuadric(center);
            gl.glPopMatrix();
        }

        // draw interpolation curves between points:
        gl.glBegin(GL2.GL_POINTS);
        gl.glPointSize(5);
        gl.glColor3d(1, 1, 1);

        List<Cylinder> cylinders = getCylinders(drawableInteractiveMask.getMask());
        List<CrossSection3D> curves = interpolatePoints(drawableInteractiveMask.getMask());
        Cylinder cyl;
        CrossSection3D curve;
        for (int i = 0; i < curves.size(); i++) {
            cyl = cylinders.get(i);
            curve = curves.get(i);
            for (List<Point3d> segment : curve.getSegments()) {
                for (Point3d point : segment) {
                    if (cyl.containsPoint(point)) {
                        gl.glVertex3d(point.x, point.y, point.z);
                    }
                }
            }
        }
        gl.glEnd();
    }

    /**
     * interpolates curves to draw onto face
     *
     * @return list of curves
     */
    protected List<CrossSection3D> interpolatePoints(SurfaceMask mask) {
        List<CrossSection3D> curves = new ArrayList<>();

        for (int i = 0; i < mask.getPoints().size() - 1; i++) {
            var p1 = mask.getPoints().get(i);
            var p2 = mask.getPoints().get(i + 1);
            curves.add(interpolateLine(p1, p2));
        }

        if (mask.getPoints().size() >= 3) {
            curves.add(interpolateLine(mask.getPoints().get(mask.getPoints().size() - 1), mask.getPoints().get(0)));
        }

        return curves;
    }

    /**
     * Interpolates a single curve between surface mask points using a cutting plane defined by the said two points
     * and a normal of mesh triangle upon which a point lies.
     *
     * @param p1
     * @param p2
     * @return a single curve
     */
    protected CrossSection3D interpolateLine(SurfaceMask.MaskPoint3D p1, SurfaceMask.MaskPoint3D p2) {
        var p3 = new Point3d(p1.getPosition().x,
                p1.getPosition().y,
                p1.getPosition().z);
        p3.add(new Point3d(p1.getNormal()));
        Plane plane = new Plane(p1.getPosition(), p2.getPosition(), p3);
        return CrossSectionServices.compute(p1.getFacet(), new CrossSectionConfig(plane));
    }

    /**
     * Calculates bounding cylinders to draw a connecting line between two points correctly
     *
     * @return a list of bounding cylinders
     */
    protected List<Cylinder> getCylinders(SurfaceMask mask) {
        List<Cylinder> cylinders = new ArrayList<>();
        for (int i = 0; i < mask.getPoints().size() - 1; i++) {
            var p1 = mask.getPoints().get(i);
            var p2 = mask.getPoints().get(i + 1);
            Cylinder cyl = new Cylinder(p1.getPosition(), p2.getPosition(), 10);
            cylinders.add(cyl);
        }

        if (mask.getPoints().size() >= 3) {
            cylinders.add(new Cylinder(mask.getPoints().get(mask.getPoints().size() - 1).getPosition(),
                    mask.getPoints().get(0).getPosition(),
                    10));
        }
        return cylinders;
    }

    /**
     * Legacy code to get bounding boxes for lines between points
     *
     * @return list of boxes for neighbouring points
     */
    protected List<Box> getBoxes(SurfaceMask mask) {
        List<Box> boxes = new ArrayList<>();
        for (int i = 0; i < mask.getPoints().size() - 1; i++) {
            var p1 = mask.getPoints().get(i);
            var p2 = mask.getPoints().get(i + 1);
            Box box = new Box(
                    new Point3d(min(p1.getPosition().x, p2.getPosition().x),
                            min(p1.getPosition().y, p2.getPosition().y),
                            min(p1.getPosition().z, p2.getPosition().z)),
                    new Point3d(max(p1.getPosition().x, p2.getPosition().x),
                            max(p1.getPosition().y, p2.getPosition().y),
                            max(p1.getPosition().z, p2.getPosition().z))
            );
            boxes.add(box);
        }

        if (mask.getPoints().size() >= 3) {
            var p1 = mask.getPoints().get(mask.getPoints().size() - 1).getPosition();
            var p2 = mask.getPoints().get(0).getPosition();
            new Box(
                    new Point3d(min(p1.x, p2.x),
                            min(p1.y, p2.y),
                            min(p1.z, p2.z)),
                    new Point3d(max(p1.x, p2.x),
                            max(p1.y, p2.y),
                            max(p1.z, p2.z))
            );
            boxes.add(new Box(
                    new Point3d(min(p1.x, p2.x),
                            min(p1.y, p2.y),
                            min(p1.z, p2.z)),
                    new Point3d(max(p1.x, p2.x),
                            max(p1.y, p2.y),
                            max(p1.z, p2.z))
            ));
        }
        return boxes;
    }
}
