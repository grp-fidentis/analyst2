package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.drawables.DrawableFeaturePoints;
import cz.fidentis.analyst.rendering.RenderingMode;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * Renderer for DrawableFeaturePoints.
 *
 * @param <T> type of rendered class
 * @author Radek Oslejsek
 * @author Daniel Schramm
 * @author Katerina Zarska
 */
public class DrawableFeaturePointsRenderer<T extends DrawableFeaturePoints> extends AbstractDrawableRenderer<T> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawableFeaturePoints.class;
    }

    @Override
    protected void renderObject(GL2 gl, T drawableFeaturePoints) {
        List<Landmark> featurePoints = drawableFeaturePoints.getFeaturePoints();
        Map<Integer, Color> specialColors = drawableFeaturePoints.getSpecialColors();
        Map<Integer, RenderingMode> specialRenderModes = drawableFeaturePoints.getSpecialRenderModes();
        Map<Integer, Double> specialSizes = drawableFeaturePoints.getSpecialSizes();
        double defaultPerimeter = drawableFeaturePoints.getDefaultPerimeter();


        for (int i = 0; i < featurePoints.size(); i++) {
            Landmark fp = featurePoints.get(i);

            Color specialColor = specialColors.get(i);
            Color defaultColor = drawableFeaturePoints.getColor(i);

            float[] rgba = {
                    defaultColor.getRed() / 255f,
                    defaultColor.getGreen() / 255f,
                    defaultColor.getBlue() / 255f,
                    defaultColor.getTransparency()};

            if (specialColor != null) {
                rgba[0] = specialColor.getRed() / 255f;
                rgba[1] = specialColor.getGreen() / 255f;
                rgba[2] = specialColor.getBlue() / 255f;
                rgba[3] = drawableFeaturePoints.getTransparency();
            }
            gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, rgba, 0);

            RenderingMode specialRenderMode = specialRenderModes.get(i);
            if (specialRenderMode != null && specialRenderMode == RenderingMode.HIDE) { // skip this point
                continue;
            }
            if (specialRenderMode != null) {
                setPolygonMode(gl, drawableFeaturePoints);
            }

            gl.glPushMatrix();
            gl.glTranslated(fp.getX(), fp.getY(), fp.getZ());
            GLUquadric center = GLU_CONTEXT.gluNewQuadric();
            GLU_CONTEXT.gluQuadricDrawStyle(center, GLU.GLU_FILL);
            GLU_CONTEXT.gluQuadricNormals(center, GLU.GLU_FLAT);
            GLU_CONTEXT.gluQuadricOrientation(center, GLU.GLU_OUTSIDE);
            GLU_CONTEXT.gluSphere(center, specialSizes.getOrDefault(i, defaultPerimeter), 16, 16);
            GLU_CONTEXT.gluDeleteQuadric(center);
            gl.glPopMatrix();

            if (specialRenderMode != null) {
                setPolygonMode(gl, drawableFeaturePoints); // set default render mode
            }
        }
    }


}
