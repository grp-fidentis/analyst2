package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.drawables.DrawableLine;

import javax.vecmath.Point3d;

/**
 * Renderer for DrawableLine.
 *
 * @author Radek Oslejsek
 */
public class DrawableLineRenderer extends AbstractDrawableRenderer<DrawableLine> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawableLine.class;
    }

    @Override
    protected void renderObject(GL2 gl, DrawableLine drawableLine) {
        Point3d startPoint = drawableLine.getStartPoint();
        Point3d endPoint = drawableLine.getEndPoint();

        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(startPoint.x, startPoint.y, startPoint.z);
        gl.glVertex3d(endPoint.x, endPoint.y, endPoint.z);
        gl.glEnd();
    }


}
