package cz.fidentis.analyst.drawables.renderers.impl;

import cz.fidentis.analyst.drawables.DrawableFpWeights;

/**
 * Renderer for DrawableFpWeights.
 *
 * @author Radek Oslejsek
 */
public class DrawableFpWeightsRenderer extends DrawableFeaturePointsRenderer<DrawableFpWeights> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawableFpWeights.class;
    }

}
