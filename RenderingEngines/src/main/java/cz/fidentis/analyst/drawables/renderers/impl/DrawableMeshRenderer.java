package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.drawables.DrawableMesh;
import cz.fidentis.analyst.rendering.RenderingMode;

import java.awt.*;
import java.io.File;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL2ES1.GL_TEXTURE_ENV;
import static com.jogamp.opengl.GL2ES1.GL_TEXTURE_ENV_MODE;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_SHININESS;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_SPECULAR;

/**
 * Renderer for DrawableMesh.
 *
 * @param <T> type of rendered object
 * @author Radek Oslejsek
 * @author Richard Pajersky
 * @author Katerina Zarska
 */
public class DrawableMeshRenderer<T extends DrawableMesh> extends AbstractDrawableRenderer<T> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawableMesh.class;
    }

    @Override
    protected void initRendering(GL2 gl, T drawableMesh) {
        super.initRendering(gl, drawableMesh);

        MeshModel model = drawableMesh.getModel();
        Texture texture = drawableMesh.getTexture();

        // set color of highlights
        Color col = drawableMesh.getHighlights();
        float[] highlights = {col.getRed(), col.getGreen(), col.getBlue(), 1};
        gl.glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50);
        gl.glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, highlights, 0);

        if (drawableMesh.getRenderMode() == RenderingMode.TEXTURE &&
                model.hasMaterial() &&
                model.getMaterial().hasTexture() &&
                (texture == null)) {

            gl.glActiveTexture(GL_TEXTURE0);
            gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            gl.glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
            try {
                texture = TextureIO.newTexture(new File(model.getMaterial().texturePath()), true);
                texture.bind(gl);
                drawableMesh.setTexture(texture);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    protected void renderObject(GL2 gl, T drawableMesh) {
        Texture texture = drawableMesh.getTexture();

        if (texture != null && drawableMesh.getRenderMode() == RenderingMode.TEXTURE) { // turn on temporarily
            gl.glActiveTexture(GL_TEXTURE0);
            gl.glBindTexture(GL_TEXTURE_2D, texture.getTextureObject());
            gl.glEnable(GL_TEXTURE_2D);
        }

        for (MeshFacet facet : drawableMesh.getFacets()) {
            gl.glBegin(GL_TRIANGLES); //vertices are rendered as triangles

            for (MeshTriangle tri : facet) {
                for (MeshPoint mPoint : tri) {
                    // render the normals
                    if (mPoint.getNormal() != null) {
                        gl.glNormal3d(mPoint.getNormal().x, mPoint.getNormal().y, mPoint.getNormal().z);
                    }

                    // render the vertices
                    if (drawableMesh.getRenderMode() == RenderingMode.TEXTURE && mPoint.getTexCoord() != null) {
                        gl.glTexCoord2d(mPoint.getTexCoord().x, mPoint.getTexCoord().y);
                    }

                    gl.glVertex3d(mPoint.getPosition().x, mPoint.getPosition().y, mPoint.getPosition().z);
                }
            }

            /*
            // get the normal and tex coordinate indices for face i
            for (int v = 0; v < facet.getCornerTable().getSize(); v++) {            
                MeshPoint mPoint = facet.getVertices().get(facet.getCornerTable().getRow(v).getVertexIndex());

                // render the normals
                if (mPoint.getNormal() != null) {
                    gl.glNormal3d(mPoint.getNormal().x, mPoint.getNormal().y, mPoint.getNormal().z);
                }

                // render the vertices
                if (getRenderMode() == RenderingMode.TEXTURE && mPoint.getTexCoord() != null) {
                    gl.glTexCoord2d(mPoint.getTexCoord().x, mPoint.getTexCoord().y);
                }
                gl.glVertex3d(mPoint.getPosition().x, mPoint.getPosition().y, mPoint.getPosition().z);
            }
             */

            gl.glEnd();
        }

        gl.glDisable(GL_TEXTURE_2D); // turn off
    }
}
