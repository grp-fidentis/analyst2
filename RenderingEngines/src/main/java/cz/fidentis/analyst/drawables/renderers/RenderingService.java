package cz.fidentis.analyst.drawables.renderers;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.drawables.Drawable;
import cz.fidentis.analyst.drawables.renderers.impl.RenderingServiceImpl;

/**
 * @author Marek Seďa
 */
public interface RenderingService {

    RenderingServiceImpl INSTANCE = new RenderingServiceImpl();

    /**
     * Method allowing to render generic Drawable
     * @param gl
     * @param drawable
     */
    static void render(GL2 gl, Drawable drawable) {
        INSTANCE.render(gl, drawable);
    }
}
