package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.drawables.DrawableFpSkeleton;
import cz.fidentis.analyst.drawables.renderers.RenderingService;

/**
 * Renderer for DrawableFpSkeleton.
 *
 * @author Katerina Zarska
 */
public class DrawableFpSkeletonRenderer extends AbstractDrawableRenderer<DrawableFpSkeleton> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawableFpSkeleton.class;
    }

    @Override
    protected void renderObject(GL2 gl, DrawableFpSkeleton drawableFpSkeleton) {
        drawableFpSkeleton.getSkeleton().forEach(line -> RenderingService.render(gl, line));
    }
}
