package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.drawables.DrawablePlane;

import javax.vecmath.Point3d;

/**
 * Renderer for DrawablePlane.
 *
 * @param <T> type of rendered object
 * @author Radek Oslejsek
 * @author Dominik Racek
 */
public class DrawablePlaneRenderer<T extends DrawablePlane> extends AbstractDrawableRenderer<T> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawablePlane.class;
    }

    @Override
    protected void renderObject(GL2 gl, T drawablePlane) {

        boolean showBoundingBox = drawablePlane.isShownBBobx();
        Box bbox = drawablePlane.getBBox();

        renderObject(gl, drawablePlane.getPlane().getMesh(bbox), showBoundingBox, bbox);
    }

    protected void renderObject(GL2 gl, MeshFacet facet, boolean showBoundingBox, Box bbox) {
        gl.glBegin(GL2.GL_TRIANGLES); //vertices are rendered as triangles

        for (MeshTriangle tri : facet) {
            for (MeshPoint mPoint : tri) {
                Point3d vert = mPoint.getPosition();
                gl.glVertex3d(vert.x, vert.y, vert.z);
            }
        }

        /*
        // get the normal and tex coordinate indices for face i
        for (int v = 0; v < facet.getCornerTable().getSize(); v++) {
            Point3d vert = facet.getVertices().get(facet.getCornerTable().getRow(v).getVertexIndex()).getPosition();
            gl.glVertex3d(vert.x, vert.y, vert.z);
        }
         */

        gl.glEnd();

        if (showBoundingBox) {
            Point3d min = bbox.minPoint();
            Point3d max = bbox.maxPoint();
            gl.glBegin(GL2.GL_LINES);

            // bottom rectangle:
            gl.glVertex3d(min.x, min.y, min.z);
            gl.glVertex3d(max.x, min.y, min.z);

            gl.glVertex3d(max.x, min.y, min.z);
            gl.glVertex3d(max.x, min.y, max.z);

            gl.glVertex3d(max.x, min.y, max.z);
            gl.glVertex3d(min.x, min.y, max.z);

            gl.glVertex3d(min.x, min.y, max.z);
            gl.glVertex3d(min.x, min.y, min.z);

            // top rectangle:
            gl.glVertex3d(min.x, max.y, min.z);
            gl.glVertex3d(max.x, max.y, min.z);

            gl.glVertex3d(max.x, max.y, min.z);
            gl.glVertex3d(max.x, max.y, max.z);

            gl.glVertex3d(max.x, max.y, max.z);
            gl.glVertex3d(min.x, max.y, max.z);

            gl.glVertex3d(min.x, max.y, max.z);
            gl.glVertex3d(min.x, max.y, min.z);

            // vertical edges:
            gl.glVertex3d(min.x, min.y, min.z);
            gl.glVertex3d(min.x, max.y, min.z);

            gl.glVertex3d(max.x, min.y, min.z);
            gl.glVertex3d(max.x, max.y, min.z);

            gl.glVertex3d(max.x, min.y, max.z);
            gl.glVertex3d(max.x, max.y, max.z);

            gl.glVertex3d(min.x, min.y, max.z);
            gl.glVertex3d(min.x, max.y, max.z);

            gl.glEnd();
        }
    }
}
