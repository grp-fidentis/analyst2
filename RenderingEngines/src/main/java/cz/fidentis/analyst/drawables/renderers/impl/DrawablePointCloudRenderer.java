package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.drawables.DrawablePointCloud;

/**
 * Renderer for DrawablePointCloud.
 *
 * @author Radek Oslejsek
 */
public class DrawablePointCloudRenderer extends AbstractDrawableRenderer<DrawablePointCloud> {

    public static final float FP_DEFAULT_SIZE = 3.0f;

    @Override
    public Class<?> getDrawableClass() {
        return DrawablePointCloud.class;
    }

    @Override
    protected void renderObject(GL2 gl, DrawablePointCloud drawablePointCloud) {

        float[] rgba = {
                drawablePointCloud.getColor().getRed() / 255f,
                drawablePointCloud.getColor().getGreen() / 255f,
                drawablePointCloud.getColor().getBlue() / 255f,
                drawablePointCloud.getTransparency()
        };

        //gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, rgba, 0); // set default color
        gl.glColor3fv(rgba, 0); // set default color
        gl.glPointSize(FP_DEFAULT_SIZE);
        gl.glDisable(GL2.GL_LIGHTING);

        gl.glBegin(GL2.GL_POINTS);
        drawablePointCloud.getPoints().forEach(mp -> {
            gl.glVertex3d(mp.getX(), mp.getY(), mp.getZ());
            /*
            gl.glPushMatrix(); 
            gl.glTranslated(mp.getX(), mp.getY(), mp.getZ());
            GLUquadric center = GLU_CONTEXT.gluNewQuadric();
            GLU_CONTEXT.gluQuadricDrawStyle(center, GLU.GLU_FILL);
            GLU_CONTEXT.gluQuadricNormals(center, GLU.GLU_FLAT);
            GLU_CONTEXT.gluQuadricOrientation(center, GLU.GLU_OUTSIDE);
            GLU_CONTEXT.gluSphere(center, 0.7, 16, 16);
            GLU_CONTEXT.gluDeleteQuadric(center);
            gl.glPopMatrix();    
            */
        });
        gl.glEnd();

        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPointSize(1f);
    }
}
