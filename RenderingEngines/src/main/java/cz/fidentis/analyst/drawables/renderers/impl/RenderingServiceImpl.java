package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.drawables.Drawable;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Seďa
 */
@SuppressWarnings("ALL")
public class RenderingServiceImpl {

    private final Map<Class<?>, AbstractDrawableRenderer> renderers = new HashMap<>();

    /**
     * Constructor of RenderingServiceImpl
     * initializes all available renderers
     */
    public RenderingServiceImpl() {
        registerRenderer(new DrawablePlaneRenderer<>());
        registerRenderer(new DrawableMeshRenderer<>());
        registerRenderer(new DrawableLineRenderer());
        registerRenderer(new DrawablePointCloudRenderer());
        registerRenderer(new DrawableInteractiveMaskRenderer());
        registerRenderer(new DrawableCuttingPlaneRenderer());
        registerRenderer(new DrawableFaceRenderer());
        registerRenderer(new DrawableFeaturePointsRenderer<>());
        registerRenderer(new DrawableFpWeightsRenderer());
        registerRenderer(new DrawableFpSkeletonRenderer());
    }

    private void registerRenderer(AbstractDrawableRenderer renderer) {
        AbstractDrawableRenderer existingRenderer = renderers.put(renderer.getDrawableClass(), renderer);
        if (existingRenderer != null) {
            throw new IllegalArgumentException("Renderer for class " + renderer.getDrawableClass() + " is already registered");
        }
    }

    /**
     * Method allowing to render generic Drawable
     *
     * @param gl
     * @param drawable
     */
    public void render(GL2 gl, Drawable drawable) {
        AbstractDrawableRenderer renderer = renderers.get(drawable.getClass());
        if (renderer == null) {
            throw new IllegalStateException("No renderer found for class " + drawable.getClass());
        }
        renderer.render(gl, drawable);
    }

}
