package cz.fidentis.analyst.drawables.renderers.impl;

import com.jogamp.opengl.GL2;
import cz.fidentis.analyst.drawables.DrawableFace;
import cz.fidentis.analyst.rendering.HeatmapRenderer;

/**
 * Renderer for DrawableFace.
 *
 * @author Radek Oslejsek
 * @author Daniel Schramm
 */
public class DrawableFaceRenderer extends DrawableMeshRenderer<DrawableFace> {

    @Override
    public Class<?> getDrawableClass() {
        return DrawableFace.class;
    }

    @Override
    protected void renderObject(GL2 gl, DrawableFace drawableFace) {
        if (drawableFace.getHeatMap() != null) {
            HeatmapRenderer rend = new HeatmapRenderer();
            rend.drawMeshModel(gl, drawableFace.getModel(), drawableFace.getHeatMap(), drawableFace.getTransparency());
        } else {
            super.renderObject(gl, drawableFace);
        }
    }

}
