package cz.fidentis.analyst.data.surfacemask;

import java.awt.*;
import java.util.List;

/**
 * An interface specifying the needed operations for 2D representations of Surface mask
 * @author Mario Chromik
 */
public interface SurfaceMask2D {
    /**
     * Gets points to be projected.
     * @return a list of Point
     */
    List<PointToProject> getPointsToProject();


    /**
     * Selects a point for further manipulation
     * @param location of the point to select
     * @return true if selected
     */
    boolean selectPoint(Point location);

    /**
     * Sets/resets the selected point
     * @param point to set
     */
    void setSelectedPoint(Point point);

    /**
     * Define a new point for the mask
     * @param point new point to add
     * @return true if addition succeded
     */
    boolean addNewPoint(Point point);


    /**
     * check if location is within the mask shape
     * @param middle location to check
     * @return true is inside
     */
    boolean containsPoint(Point middle);

    /**
     * Shifts the mask by a delta in the x and y direction.
     * Delta is calculated from the previous and the current shift point
     * @param dx delta in the x direction
     * @param dy delta in the y direction
     */
    void shiftMask(int dx, int dy);

    /**
     * Sets shift point
     * @param point point to set
     */
    void setShiftPoint(Point point);

    /**
     * Gets the shift point
     * @return shift point
     */
    Point getShiftPoint();

    /**
     * gets the selceted point
     * @return selected point
     */
    Point getSelectedPoint();

    /**
     * Updates selected point by changing it's location
     * @param point location to set
     */
    void updateSelectedPoint(Point point);

    /**
     * Sets the sampling strength
     * @param samplingStrength
     */
    void setSamplingStrength(int samplingStrength);

    /**
     *
     * @return color of mask
     */
    Color getColor();

    /**
     * Sets color of mask
     * @param color to be set
     */
    void setColor(Color color);

    /**
     * A class representing point to be projected
     * @author Mario Chromik
     */
    class PointToProject {
        private boolean isSelected;
        private boolean isSampled;
        private Point point;

        /**
         * Constructor with three parameters
         * @param isSelected specifies if the point has been selected
         * @param isSampled specifies if the point is the result of sampling
         * @param point point itself
         */
        public PointToProject(boolean isSelected, boolean isSampled, Point point) {
            this.isSelected = isSelected;
            this.isSampled = isSampled;
            this.point = point;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public boolean isSampled() {
            return isSampled;
        }

        public Point getPoint() {
            return point;
        }
    }
}
