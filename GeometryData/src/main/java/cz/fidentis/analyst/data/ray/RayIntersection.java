package cz.fidentis.analyst.data.ray;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;

import javax.vecmath.Point3d;

/**
 * A single ray intersection. The natural ordering sorts ray intersections
 * by their distance from the ray's origin (regardless on the direction, i.e.,
 * absolute values is taken into account). In case of the same absolute values
 * of the distances, an object with positive distance is preferred (is defined
 * as smaller) over the one with negative value.
 *
 * @author Radek Oslejsek
 */
public interface RayIntersection extends MeshPoint, Comparable<RayIntersection>{

    /**
     * Returns distance from the ray's origin to this ray intersection.
     * @return distance from the ray's origin to this ray intersection
     */
    double getDistance();

    /**
     * Returns hit triangle
     * @return hit triangle
     */
    MeshTriangle getHitTriangle();

    /**
     * Determines if the ray intersection is in the direction of the ray (direct hit)
     * or i opposite direction (indirect hit)
     *
     * @return {@code true} if the hit is in the ray's direction
     */
    boolean isDirectHit();

    /**
     * Set the distance of the intersection point from the ray's origin.
     * If the intersection point lies in the opposite direction than ray's normal,
     * then a negative distance is set.
     *
     * @param rayOrigin The origin of the ray that has been used to compute this intersection
     */
    void setDistance(Point3d rayOrigin);

    @Override
    int compareTo(RayIntersection other);
}
