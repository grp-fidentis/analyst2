package cz.fidentis.analyst.data.surfacemask;

import java.util.List;

/**
 * Represents a single layer within a {@link SurfaceMaskLayers}, containing a surface mask
 * and a history of previous states for undo/redo functionality.
 *
 * @author Martin Bjaloň
 */
public interface Layer {

    /**
     * Returns the current surface mask in this layer.
     * @return The current {@link SurfaceMask2D}.
     */
    SurfaceMask2D getCurrentMask();

    /**
     * Sets a new current surface mask for this layer.
     * @param dir 1 - forward; -1 - back in masks.
     */
    void setCurrentMask(int dir);

    /**
     * Returns all surface masks in this layer.
     *
     * @return A list of all {@link SurfaceMask2D} objects in this layer.
     */
    List<SurfaceMask2D> getAllMasks();

    /**
     * Adds a new surface mask to this layer and sets it as the current mask.
     *
     * @param mask The {@link SurfaceMask2D} to be added.
     */
    void addMask(SurfaceMask2D mask);

    /**
     * Sets the visibility of this layer.
     *
     * @param visible {@code true} to make the layer visible; {@code false} to hide it.
     */
    void setVisibility(boolean visible);

    /**
     * Checks if the layer is currently visible.
     *
     * @return {@code true} if the layer is visible; {@code false} otherwise.
     */
    boolean isVisible();

    /**
     * Update the history, if the max depth is exceeded, the history is halved, and sets
     * the current index to the last entry added.
     */
    void updateHistory();

    /**
     * Moves back to the previous mask state in the history if possible.
     */
    void goBack();

    /**
     * Moves forward to the next mask state in the history if possible.
     */
    void goForward();
}