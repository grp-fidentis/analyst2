package cz.fidentis.analyst.data.surfacemask.impl;

import cz.fidentis.analyst.data.surfacemask.SurfaceMaskRectangle;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a 2D surface mask in the shape of a rectangle
 * @author Mario Chromik
 */
public class SurfaceMaskRectangleImpl extends SurfaceMask2DImpl implements SurfaceMaskRectangle {
    private Point origin = null;
    private Point resize = null;

    /**
     * Paremeterless constructor
     */
    public SurfaceMaskRectangleImpl() {

    }

    /**
     * Copy constructor
     * @param mask to copy
     */
    public SurfaceMaskRectangleImpl(SurfaceMaskRectangleImpl mask) {
        if (mask.origin == null || mask.resize == null) {
            return;
        }
        this.origin = new Point(mask.origin.x, mask.origin.y);
        this.resize = new Point(mask.resize.x, mask.resize.y);
    }

    public Point getOrigin() {
        return origin;
    }

    public void setOrigin(Point origin) {
        this.origin = origin;
    }

    public Point getResize() {
        return resize;
    }

    public void setResize(Point resize) {
        this.resize = resize;
    }

    /**
     * Calculates the rectangle's width
     * @return the width as a positive integer
     */
    public int getWidth() {
        return Math.abs(resize.x - origin.x);
    }

    /**
     * Calculates the rectangle's height
     * @return the height as a positive integer
     */
    public int getHeight() {
        return Math.abs(resize.y - origin.y);
    }

    /**
     * Samples a line of the rectangle for projection
     * @param start start of line to sample
     * @param end end of line to sample
     * @return list of sampled points
     */
    private ArrayList<PointToProject> sampleLine(Point start, Point end) {
        int xDiff = (end.x - start.x) / getSamplingStrength();
        int yDiff = (end.y - start.y) / getSamplingStrength();
        Point sampledPoint = new Point(start.x, start.y);
        ArrayList<PointToProject> sampledPoints = new ArrayList<>();
        for (int i = 0; i < getSamplingStrength(); i++) {
            sampledPoint = new Point(sampledPoint.x + xDiff, sampledPoint.y + yDiff);
            sampledPoints.add(new PointToProject(false, true, sampledPoint));
        }
        return sampledPoints;
    }
    @Override
    public List<PointToProject> getPointsToProject() {
        if (this.origin == null) {
            return null;
        }

        ArrayList<PointToProject> pointsToProject = new ArrayList<>();
        Point[] rectanglePoints = {
                new Point(origin.x, origin.y),
                new Point(origin.x, origin.y + getHeight()),
                new Point(origin.x + getWidth(), origin.y + getHeight()),
                new Point(origin.x + getWidth(), origin.y)
        };

        pointsToProject.add(new PointToProject(false, false, rectanglePoints[0]));
        pointsToProject.addAll(sampleLine(rectanglePoints[0], rectanglePoints[1]));
        pointsToProject.add(new PointToProject(false, false, rectanglePoints[1]));
        pointsToProject.addAll(sampleLine(rectanglePoints[1], rectanglePoints[2]));
        pointsToProject.add(new PointToProject(false, false, rectanglePoints[2]));
        pointsToProject.addAll(sampleLine(rectanglePoints[2], rectanglePoints[3]));
        pointsToProject.add(new PointToProject(false, false, rectanglePoints[3]));
        pointsToProject.addAll(sampleLine(rectanglePoints[3], rectanglePoints[0]));

        return pointsToProject;
    }

    @Override
    public void shiftMask(int dx, int dy) {
        origin.setLocation(origin.x + dx, origin.y + dy);
        resize.setLocation(resize.x + dx, resize.y + dy);
    }

    /**
     * Checks if point is inside of surface mask shape
     * @param middle location to check
     * @return true if inside else false
     */
    public boolean containsPoint(Point middle) {
        int xMin = Math.min(origin.x, resize.x);
        int xMax = Math.max(origin.x, resize.x);
        int yMin = Math.min(origin.y, resize.y);
        int yMax = Math.max(origin.y, resize.y);

        if (xMin <= middle.x && middle.x <= xMax  && yMin <= middle.y && middle.y <= yMax) {
            setShiftPoint(middle);
            return true;
        }
        return false;
    }

    @Override
    public boolean selectPoint(Point location) {
        if (resize == null) {
            return false;
        }
        Rectangle rect = new Rectangle(resize.x - POINT_SIZE, resize.y - POINT_SIZE, POINT_SIZE, POINT_SIZE);
        if (rect.contains(location)) {
            setSelectedPoint(resize);
            return true;
        }

        return false;
    }


    @Override
    public boolean addNewPoint(Point point) {
        if (origin != null) {
            return false;
        }
        origin = point;
        resize = new Point(point.x + DEFAULT_SIZE, point.y + DEFAULT_SIZE);
        return true;
    }
}
