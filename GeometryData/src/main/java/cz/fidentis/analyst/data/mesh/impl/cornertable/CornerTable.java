package cz.fidentis.analyst.data.mesh.impl.cornertable;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Representation of mesh in memory using corner table. 
 * Face = triangle.
 *
 * @author Matej Lukes
 */
public class CornerTable implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final List<CornerTableRow> rows = new ArrayList<>();
    
    /**
     * Key: vertex index
     * Value: lists of rows (their index) containing the vertex with the index
     */
    private final Map<Integer, List<Integer>> vertexToRow = new HashMap<>();
    
    /**
     * Constructor of CornerTable
     */
    public CornerTable() {

    }

    /**
     * Copy constructor of CornerTable
     *
     * @param cornerTable copied CornerTable
     */
    public CornerTable(CornerTable cornerTable) {
        for (CornerTableRow row : cornerTable.rows) {
            rows.add(new CornerTableRow(row));
        }
        for (Integer i: cornerTable.vertexToRow.keySet()) {
            vertexToRow.put(i, new ArrayList<>(cornerTable.vertexToRow.get(i)));
        }
    }

    /**
     * returns index of face that contains corner.
     * returns -2 if index is less than 0 or more than number of rows in corner table
     *
     * @param index index of corner
     * @return index of corner
     */
    public int getIndexOfFace(int index) {
        if (index < 0 || index > rows.size()) {
            return -2;
        }
        return index / 3;
    }

    /**
     * returns index of opposite corner.
     * returns -1 if corner does not have opposite
     * returns -2 if index is less than 0 or more than number of rows in corner table
     *
     * @param index index of corner
     * @return index of corner
     */
    public int getIndexOfOppositeCorner(int index) {
        if (index < 0 || index > rows.size()) {
            return -2;
        }
        return rows.get(index).getOppositeCornerIndex();
    } // opposite

    /**
     * returns index of next corner in Face
     * returns -2 if index is less than 0 or more than number of rows in corner table
     *
     * @param index index of corner
     * @return index of next corner
     */
    public int getIndexOfNextCornerInFace(int index) {
        if (index < 0 || index > rows.size()) {
            return -2;
        }
        if ((index % 3) == 2) {
            return index - 2;
        }
        return index + 1;
    }

    /**
     * returns index of previous corner in Face
     * returns -2 if index is less than 0 or more than number of rows in corner table
     *
     * @param index index of corner
     * @return index of previous corner
     */
    public int getIndexOfPreviousCornerInFace(int index) {
        if (index < 0 || index > rows.size()) {
            return -2;
        }
        return getIndexOfNextCornerInFace(getIndexOfNextCornerInFace(index));
    }

    /**
     * returns opposite corner of edge on the left side of corner
     * returns -2 if index is less than 0 or more than number of rows in corner table
     *
     * @param index index of corner
     * @return index of corner
     */
    public int getIndexOfTipCornerOnLeft(int index) {
        if (index < 0 || index > rows.size()) {
            return -2;
        }
        return getIndexOfOppositeCorner(getIndexOfPreviousCornerInFace(index));
    }

    /**
     * returns opposite corner of edge on the right side of corner
     * returns -2 if index is less than 0 ord more than number of rows in corner table
     *
     * @param index index of corner
     * @return index of corner
     */
    public int getIndexOfTipCornerOnRight(int index) {
        if (index < 0 || index > rows.size()) {
            return -2;
        }
        return getIndexOfOppositeCorner(getIndexOfNextCornerInFace(index));
    }

    /**
     * returns corner next to the corner opposite the edge on the left side
     * returns -2 if index is less than 0 ord more than number of rows in corner table
     *
     * @param index index of corner
     * @return index of corner
     */
    public int getNextAroundCorner(int index) {
        if (index < 0 || index > rows.size()) {
            return -2;
        }
        int tipLeftCornerIndex = getIndexOfTipCornerOnLeft(index);
        if (tipLeftCornerIndex == -1) {
            return -1;
        }

        return getIndexOfNextCornerInFace(tipLeftCornerIndex);
    }

    /**
     * adds row at the end of corner table
     *
     * @param row that is going to be added to corner table
     */
    public void addRow(CornerTableRow row) {
        rows.add(row);
        int vIndex = row.getVertexIndex();
        if (!vertexToRow.containsKey(vIndex)) {
            vertexToRow.put(vIndex, new ArrayList<>());
        }
        vertexToRow.get(vIndex).add(rows.size()-1);
    }

    /**
     * replaces row of corner table at specified index
     *
     * @param index index of replaced row
     * @param row   new row
     */
    public void replaceRow(int index, CornerTableRow row) {
        CornerTableRow oldRow = rows.get(index);
        int oldVertIndex = oldRow.getVertexIndex();
        
        List<Integer> oldReferences = vertexToRow.get(oldVertIndex);
        
        oldReferences.remove(oldReferences.indexOf(index)); // !!!!
        if (oldReferences.isEmpty()) {
            vertexToRow.remove(oldVertIndex);
        }
        
        rows.set(index, row);
        
        int vIndex = row.getVertexIndex();
        if (!vertexToRow.containsKey(vIndex)) {
            vertexToRow.put(vIndex, new ArrayList<>());
        }
        vertexToRow.get(vIndex).add(index);
    }

    /**
     * returns number of rows in corner table
     *
     * @return number of rows in corner table
     */
    public int getSize() {
        return rows.size();
    }

    /**
     * returns row of corner table with specified index
     *
     * @param index index of desired row
     * @return row of corner table
     */
    public CornerTableRow getRow(int index) {
        return rows.get(index);
    }

    /**
     * Returns corners (rows of the corner table) in which the given vertex appears.
     *
     * @param vertexIndex index of vertex
     * @return list of rows in which the given vertex appears.
     */
    public List<CornerTableRow> getCornersByVertexIndex(int vertexIndex) {
        return vertexToRow.get(vertexIndex).stream()
                .map(rowIndex -> rows.get(rowIndex))
                .collect(Collectors.toList());
    }
    
    /**
     * Returns cache.
     *
     * @return cache
     */
    public Map<Integer, List<Integer>> getVertToCornerCache() {
        return Collections.unmodifiableMap(vertexToRow);
    }

    /**
     * returns list of indexes of faces that have a corner at specific vertex
     *
     * @param vertexIndex index of vertex
     * @return list of indexes of faces
     */
    public List<Integer> getTriangleIndexesByVertexIndex(int vertexIndex) {
        return vertexToRow.get(vertexIndex).stream()
                .map(rowIndex -> getIndexOfFace(rowIndex))
                .collect(Collectors.toList());
    }
    
    /**
     * returns indexes of vertices of triangle
     *
     * @param triangleIndex index of triangle
     * @return list of indexes
     */
    public List<Integer> getIndexesOfVerticesByTriangleIndex(int triangleIndex) {
        List<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            CornerTableRow row = getRow(3 * triangleIndex + i);
            indexes.add(row.getVertexIndex());
        }
        return indexes;
    }
    
    @Override
    public String toString() {
        String ret = "";
        for (int i = 0; i < getSize(); i++) {
            ret += getRow(i).getVertexIndex() + " | " + getRow(i).getOppositeCornerIndex() + System.lineSeparator();
        }
        return ret;
    }
}
