package cz.fidentis.analyst.data.shapes;

import javax.vecmath.Point2d;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A 2D cross-section curve computed from {@link CrossSection3D}.
 *
 * @author Samuel Smoleniak
 * @author Peter Conga
 */
public class CrossSection2D {

    /**
     * 2D segments of the curve
     */
    private final List<List<Point2d>> curveSegments = new ArrayList<>();

    /**
     * Unique points of the curve that delimit segments
     */
    private final List<Point2d> endPoints = new ArrayList<>();

    /**
     * Feature points near the cutting plane
     */
    private final List<Point2d> featurePoints = new ArrayList<>();
    private List<Line2D> normals = new ArrayList<>();

    /**
     * Constructor used in tests.
     * @param curveSegments list of curve segments
     */
    public CrossSection2D(List<List<Point2d>> curveSegments) {
        this.curveSegments.addAll(curveSegments);
        for (List<Point2d> segment2d : curveSegments) {
            if (segment2d.isEmpty()) {
                continue;
            }
            endPoints.add(segment2d.get(0));
            endPoints.add(segment2d.get(segment2d.size() - 1));
        }
    }

    /**
     * Constructor. Projects the given 3d curve and its feature points into 2d.
     * @param curveSegments list of curve segments
     * @param featurePoints 2d feature points lying close to the curve
     */
    public CrossSection2D(List<List<Point2d>> curveSegments, List<Point2d> featurePoints) {
        this(curveSegments);

        if (featurePoints != null) {
            this.featurePoints.addAll(featurePoints);
        }
    }
    
    public List<Point2d> getFeaturePoints() {
        return featurePoints;
    }
    
    public List<List<Point2d>> getCurveSegments() {
        return Collections.unmodifiableList(curveSegments);
    }
    
    public List<Line2D> getNormals() {
        return normals;
    }
    
    public void setNormals(List<Line2D> normals) {
        this.normals = normals;
    }

    /**
     * Sets normal vector at the given index
     * @param index index
     * @param normal normal vector
     */
    public void setNormalAt(int index, Line2D normal) {
        normals.set(index, normal);
    }
    
    /**
     * Returns a reduced curve according to sampling
     * @param sampling numerical representation of how much we want to keep the original curve
     * 100 will keep the original, 0 will be fully reduced
     * @return reduced list of points
     */
    public CrossSection2D subSample(int sampling) {
        
        double epsilon;
        CrossSection2D result = new CrossSection2D(new ArrayList<>());
        for (List<Point2d> curveSegment : this.getCurveSegments()) {
            epsilon = getEpsilonfromSampling(curveSegment, sampling);
            result.curveSegments.add(ramerDouglasPeucker(curveSegment, epsilon));
        }
        calculateNormals(result);
        result.setNormals(this.normals);
        return result;
    }

    /**
     * Returns 2D Hausdorff distance.
     *
     * @param curve Another curve
     * @param sampling Sub-sampling
     * @return 2D Hausdorff distance
     */
    public double hausdorffDistance(CrossSection2D curve, int sampling) {
        return Math.max(
                computeDistance(this, curve, sampling),
                computeDistance(curve, this, sampling)
        );
    }

    /**
     * Computes Hausdorff distance from one curve to the other.
     * Skips points to only use sampling-% of total points.
     * @param curveX curve one
     * @param curveY curve two
     * @param sampling Sub-sampling
     * @return Hausdorff distance from curveX to curveY
     */
    private double computeDistance(CrossSection2D curveX, CrossSection2D curveY, int sampling) {
        double hausdorffDistanceAsymmetric = Double.NEGATIVE_INFINITY;
        int usedPoints = 0;
        int visitedPoints = 0;

        for (List<Point2d> segment : curveX.getCurveSegments()) {
            for (Point2d pointX : segment) {
                visitedPoints++;
                if ((usedPoints / (float) visitedPoints) * 100 > sampling) {
                    continue;
                }
                double pointXShortestDistance = findShortestDistancePointToCurve(pointX, curveY, sampling);
                if (pointXShortestDistance > hausdorffDistanceAsymmetric) {
                    hausdorffDistanceAsymmetric = pointXShortestDistance;
                }
                usedPoints++;
            }
        }
        return hausdorffDistanceAsymmetric;
    }

    /**
     * Finds shortest of all distances from given point to any point in given curve.
     * Skips the endpoints of a curve to correctly handle curves of different lengths
     * and missing segments of curves. Skips points to only use sampling-% of total points.
     * @param point point to find distance from
     * @param curve curve to find distance to
     * @param sampling Sub-sampling
     * @return the shortest distance from given point to the given curve or
     * {@code Double.NEGATIVE_INFINITY} if closest point is endpoint
     */
    private double findShortestDistancePointToCurve(Point2d point, CrossSection2D curve, int sampling) {
        double shortestDistance = Double.POSITIVE_INFINITY;
        Point2d closestCurvePoint = new Point2d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        int usedPoints = 0;
        int visitedPoints = 1;

        for (List<Point2d> segment : curve.getCurveSegments()) {
            for (Point2d curvePoint : segment) {
                visitedPoints++;
                if ((usedPoints / (float) visitedPoints) * 100 > sampling) {
                    continue;
                }
                if (point.distance(curvePoint) < shortestDistance) {
                    shortestDistance = point.distance(curvePoint);
                    closestCurvePoint = curvePoint;
                }
                usedPoints++;
            }
        }

        // if closest point of curve to given point is endPoint, don't use it
        // for Hausdorff distance
        if (curve.isEndPoint(closestCurvePoint)) {
            return Double.NEGATIVE_INFINITY;
        }
        return shortestDistance;
    }
    
    /**
     * Calculates the maximum epsilon that will be needed for sub-sampling
     * @param sampling sampling strength
     * @return maximum perpendicular distance from the line between the endpoints
     */
    private double getEpsilonfromSampling(List<Point2d> points, int sampling) {
        
        double avgDist = 0;
        Point2d[] endpoints;
        try {
            endpoints = new Point2d[] {points.get(0), points.get(points.size() - 1)};
        } catch (IndexOutOfBoundsException e) {
            return 0;
        }
        for (int i = 0; i < points.size() - 1; i++) {
            double distance = perpendicularDistance(points.get(i), endpoints[0], endpoints[1]);
            avgDist += distance;
        }

        double factor = Math.pow(1 - (sampling / 100.0), 4);
        return (avgDist / points.size()) * factor;
    }
    
    /**
     * Calculates perpendicular distance of a point from a line
     * @param point point to calculate perpendicular distance to
     * @param lineStart one endpoint of the line
     * @param lineEnd second endpoint of the line
     * @return perpendicular distance of point from line
     */
    private double perpendicularDistance(Point2d point, Point2d lineStart, Point2d lineEnd) {
        
        double x = point.x;
        double y = point.y;
        double x1 = lineStart.x;
        double y1 = lineStart.y;
        double x2 = lineEnd.x;
        double y2 = lineEnd.y;
        
        double numerator = Math.abs((y2 - y1) * x - (x2 - x1) * y + x2 * y1 - y2 * x1);
        double denominator = Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
        
        return numerator / denominator;
    }
    
    /**
     * Ramer-Douglas-Peucker algorithm to reduce the vertices of a curve
     * Ramer, Urs (1972). "An iterative procedure for the polygonal approximation of plane curves".
     * Computer Graphics and Image Processing. 1 (3): 244–256. 
     * ISSN 0146-664X
     * Douglas, David; Peucker, Thomas (1973). "Algorithms for the reduction of the number of points
     * required to represent a digitized line or its caricature". Cartographica:
     * The International Journal for Geographic Information and Geovisualization. 10 (2): 112–122.
     * DOI 10.3138/FM57-6770-U75U-7727
     * 
     * @param points List of point in the curve
     * @param epsilon The perpendicular distance above which we keep the point on the curve. 
     * If the point has a smaller perpendicular distance, we discard it.
     * @return reduced List of points
     */
    private List<Point2d> ramerDouglasPeucker(List<Point2d> points, double epsilon) {
        
        if (points.size() < 3) {
            return new ArrayList<>(points);
        }
        int index = -1;
        double maxDistance = 0.0;
        for (int i = 1; i < points.size() - 1; i++) {
            double distance = perpendicularDistance(points.get(i), points.get(0), points.get(points.size() - 1));
            if (distance > maxDistance) {
                index = i;
                maxDistance = distance;
            }
        }
        if (maxDistance > epsilon) {
            List<Point2d> leftRecursive = ramerDouglasPeucker(points.subList(0, index + 1), epsilon);
            List<Point2d> rightRecursive = ramerDouglasPeucker(points.subList(index, points.size()), epsilon);
            List<Point2d> result = new ArrayList<>(leftRecursive);
            result.addAll(rightRecursive.subList(1, rightRecursive.size()));
            return result;
        } else {
            List<Point2d> result = new ArrayList<>();
            result.add(points.get(0));
            result.add(points.get(points.size() - 1));
            return result;
        }
    }
    
    /**
     * Calculates normal vectors for all lines in curve
     * @param curve curve to calculate the normal vectors on
     */
    private void calculateNormals(CrossSection2D curve) {
        if (curve.getCurveSegments().isEmpty()) {
            return;
        }
        Point2d a = null;
        normals.clear();
        
        boolean clockwiseNormal;
        // Calculate the direction of normals
        Point2d endPoint1 = curve.getCurveSegments().get(0).get(0);
        Point2d endPoint2 = curve.getCurveSegments().get(curve.getCurveSegments().size() - 1).get(curve.getCurveSegments().get(curve.getCurveSegments().size() - 1).size() - 1);
        if (Math.abs(endPoint1.y - endPoint2.y) < Math.abs(endPoint1.x - endPoint2.x)) {
            clockwiseNormal = endPoint1.x < endPoint2.x;
        } else {
            clockwiseNormal = endPoint1.y > endPoint2.y;
        }
            
        for (List<Point2d> curveSegment : curve.getCurveSegments()) {
            if (curveSegment.isEmpty()) {
                return;
            }

            for (int i = 0; i < curveSegment.size() - 1; i++) {
                if (a == null) {
                    a = curveSegment.get(i);
                }

                Point2d b = curveSegment.get(i + 1);

                // Point in the middle of the point pair
                Point2d middle = new Point2d(((a.x + b.x) / 2), ((a.y + b.y) / 2));
                // Calculate normal vector and rotate to the right direction
                Point2d normal;
                if (clockwiseNormal) {
                    normal = new Point2d((b.y - a.y) * (-1), b.x - a.x);
                } else {
                    normal = new Point2d(b.y - a.y, (b.x - a.x) * (-1));
                }

                // Calculate the length of the normal vector
                double length = Math.sqrt(Math.pow(normal.x, 2) + Math.pow(normal.y, 2));
                // Normalize vector size
                normal.set(normal.x / length, normal.y / length);

                normals.add(new Line2D.Double(middle.x, middle.y, normal.x, normal.y));

                a = b;
            }
        }
    }
    
    /**
     * Return true if given point is an endpoint of a curve.
     * @param point point of curve
     * @return {@code true} if point is and endpoint of one of curve segments
     */
    public boolean isEndPoint(Point2d point) {
        return endPoints.contains(point);
    }
    
}
