package cz.fidentis.analyst.data.mesh;

import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTable;

import javax.vecmath.Point3d;
import java.io.Serializable;
import java.util.List;

/**
 * An encapsulated mesh plate, i.e., multiple triangles sharing vertices.
 * Mesh facet is iterable (triangle as returned) and visitable by 
 * {@link MeshVisitor}s.
 *
 * @author Matej Lukes
 */
public interface MeshFacet extends Iterable<MeshTriangle>, Serializable {
    
    /**
     * returns vertex of specified index
     *
     * @param index index of vertex
     * @return vertex
     */
    MeshPoint getVertex(int index);

    /**
     * adds vertex to MeshFacet
     *
     * @param point new vertex
     */
    void addVertex(MeshPoint point);
    
    /**
     * returns number of vertices in MeshFacet
     *
     * @return number of vertices
     */
    int getNumberOfVertices();

    /**
     * returns list of vertices in MeshFacet
     *
     * @return list of vertices
     */
    List<MeshPoint> getVertices();

    /**
     * returns Corner Table representing MeshFacet
     *
     * @return corner table
     */
    CornerTable getCornerTable();
    
    /**
     * Returns true if normals of vertices are calculated.
     * @return true if normals of vertices are calculated.
     */
    boolean hasVertexNormals();
    
    /**
     * Calculates normals of vertices from normals of triangles.
     */
    void calculateVertexNormals();
    
    /**
     * Returns number of triangles.
     * 
     * @return number of triangles
     */
    int getNumTriangles();

    /**
     * Return triangle instances. The order corresponds with the corner
     * table, i.e., the i-th returned triangle corresponds to i-th triangle in 
     * the corner table.
     * 
     * @return triangles of the mesh facet
     */
    List<MeshTriangle> getTriangles();
    
    /**
     * Returns triangles sharing the given mesh vertex.
     * 
     * @param vertexIndex Index of the mesh vertex
     * @return Triangles sharing the mesh vertex
     */
    List<MeshTriangle> getAdjacentTriangles(int vertexIndex);

    /**
     * Returns triangles sharing the given edge.
     *
     * @param index1 Index of the first point of the edge
     * @param index2 Index of the second point of the edge
     * @return Triangles sharing the edge
     */
    List<MeshTriangle> getAdjacentTriangles(int index1, int index2);

    /**
     * Returns adjacent triangles, i.e., triangles sharing an edge or a vertex with the given triangle
     *
     * @param triangle The triangle
     * @return Unique adjacent triangles
     */
    List<MeshTriangle> getAdjacentTriangles(MeshTriangle triangle);

    /**
     * Returns neighboring triangles, i.e., triangles sharing an edge with the given triangle.
     * In contrast to the {@link #getAdjacentTriangles(MeshTriangle)},
     * triangles sharing only a vertex are omitted.
     *
     * @param triangle The triangle
     * @return Unique neighboring triangles
     */
    List<MeshTriangle> getNeighboringTriangles(MeshTriangle triangle);
    
    /**
     * Finds and returns a point lying at triangles around (sharing) the given mesh vertex
     * and being the closest to a 3D point.
     * If finding the closest adjacent point fails due to weird topology (e.g., triangles with vertices in a line,
     * with the same coordinates, etc.), then the vertex at {@code vertIndex} index is returned
     * as the closest point.
     *
     * @param point 3D point
     * @param vertexIndex Index of mesh vertex
     * @return The closest surface point near the mesh vertex being closest to the given 3D point.
     */
    Point3d getClosestAdjacentPoint(Point3d point, int vertexIndex);
    
    /**
     * Returns the distance between a 3D point and triangles around (sharing)
     * the given mesh vertex. It is the distance between the 3D point and 
     * point found by the {@link MeshFacet#getClosestAdjacentPoint} method.
     * 
     * @param point 3D point
     * @param vertexIndex Index of mesh vertex
     * @return Distance
     */
    double curvatureDistance(Point3d point, int vertexIndex);
    
    /**
     * Visits this facet.
     * 
     * @param visitor Visitor
     */
    void accept(MeshVisitor visitor);   
    
    /**
     * Computes centers of circumcircle of all triangles.
     * These points represent the point of Voronoi area used for Delaunay triangulation, for instance.
     * The list is computed only once (during the first call) and than cached.
     * The order corresponds to the order of triangles, i.e., the i-th point
     * is the Voronoi point of i-th triangle.
     * 
     * @return Voronoi points of all triangles.
     */
    List<Point3d> calculateVoronoiPoints();
    
    /**
     * Returns 1-ring neighborhood, i.e., triangles around the given mesh point.
     * 
     * @param vertexIndex Index of mesh vertex
     * @return Triangles around the vertex or {@code null}
     */
    TriangleFan getOneRingNeighborhood(int vertexIndex);
    
    /**
     * Removes duplicate vertices that differ only in normal vectors or texture coordinates.
     * Multiple normals are replaced with the average normal. If the texture coordinate 
     * differ then randomly selected one is used.
     * 
     * @return {@code true} if the mesh was changed, {@code false} if there were
     * no duplicities.
     */
    boolean simplify();
}
