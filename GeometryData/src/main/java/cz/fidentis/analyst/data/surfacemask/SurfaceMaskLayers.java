package cz.fidentis.analyst.data.surfacemask;

import cz.fidentis.analyst.data.surfacemask.impl.LayerImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages multiple layers for surface masks, allowing for addition, removal,
 * and selection of layers.
 *
 * @author Martin Bjaloň
 */

public class SurfaceMaskLayers {
    private final List<Layer> layers;
    private Layer currentLayer;

    /**
     * Initializes a new SurfaceMaskLayerManager with an empty list of layers.
     */
    public SurfaceMaskLayers() {
        layers = new ArrayList<>();
    }

    /**
     * Create new Layer
     * @return new Layer
     */
    public static Layer createLayer() {
        return new LayerImpl();
    }

    /**
     * Adds a new {@link SurfaceMask2D} as a layer.
     */
    public void addLayer(Layer layer) {
        layers.add(layer);
        currentLayer = layer;
    }


    /**
     * Gets the current active layer.
     * @return The current {@link Layer}, or null if there are no layers.
     */
    public Layer getCurrentLayer() {
        if (currentLayer == null) {
            return null;
        }
        return currentLayer;
    }

    /**
     * Sets the current layer
     * @param layer to be set as current.
     */
    public void setCurrentLayer(Layer layer) {
        this.currentLayer = layer;
    }


    /**
     * Gets the list of all layers managed by this manager.
     * @return A list of layers.
     */
    public List<Layer> getLayers() {
        return layers;
    }

    /**
     * Remove layer from list of layers
     * @param layer to be removed
     */
    public void removeLayer(Layer layer) {
        layers.remove(layer);
    }
}
