package cz.fidentis.analyst.data.mesh.impl.facet;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.MeshVisitor;
import cz.fidentis.analyst.data.mesh.impl.MeshPointImpl;
import cz.fidentis.analyst.data.mesh.impl.MeshTriangleImpl;
import cz.fidentis.analyst.data.mesh.impl.TriangleFanImpl;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTable;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.*;

/**
 * Mash facet is a compact triangular mesh without duplicated vertices.
 *
 * @author Matej Lukes
 * @author Dominik Racek
 */
public class MeshFacetImpl implements MeshFacet {
    
    /**
     * Centers of circumcircle of all triangles computed on demand. 
     * These points represent the point of Voronoi area used for Delaunay 
     * triangulation, for instance.
     */
    private List<Point3d> voronoiPoints;
    
    private List<MeshPoint> vertices = new ArrayList<>();
    
    private final CornerTable cornerTable;
    
    /**
     * Creates and empty mesh
     */
    public MeshFacetImpl() {
        cornerTable = new CornerTable();
    }

    /**
     * Creates a point cloud without mesh topology.
     *
     * @param meshPoints Mesh points
     */
    public MeshFacetImpl(Collection<MeshPoint> meshPoints) {
        this.vertices.addAll(Objects.requireNonNull(meshPoints));
        cornerTable = null;
    }

    /**
     * Copy constructor of MeshFacet
     *
     * @param facet copied MeshFacet
     */
    public MeshFacetImpl(MeshFacet facet) {
        facet.getVertices().forEach(v -> vertices.add(new MeshPointImpl(v)));
        cornerTable = new CornerTable(facet.getCornerTable());
    }
    
    @Override
    public void accept(MeshVisitor visitor) {
        visitor.visitMeshFacet(this);
    }
    
    @Override
    public MeshPoint getVertex(int index) {
        return vertices.get(index);
    }

    @Override
    public void addVertex(MeshPoint point) {
        vertices.add(point);
    }
    
    @Override
    public int getNumberOfVertices() {
        return vertices.size();
    }

    @Override
    public List<MeshPoint> getVertices() {
        return Collections.unmodifiableList(vertices);
    }

    @Override
    public CornerTable getCornerTable() {
        return cornerTable;
    }
    
    @Override
    public boolean hasVertexNormals() {
        return !this.vertices.isEmpty() && this.vertices.getFirst().getNormal() != null;
    }
    
    /**
     * REPLACE WITH BETTER IMPLEMENTATION
     * @throws RuntimeException if there are duplicate meth points in the mesh facet
     */
    @Override
    public synchronized void calculateVertexNormals() {
        Map<Point3d, Vector3d> normalMap = new HashMap<>(); // key = mesh point, value = normal
        
        // init normals:
        Point3d centroid = new Point3d();
        for (MeshPoint point: vertices) { 
            if (normalMap.put(point.getPosition(), new Vector3d(0, 0, 0)) != null) {
                throw new RuntimeException("Duplicate mesh point in the MeshFacet: " + point);
            }
            centroid.x += point.getPosition().x / vertices.size();
            centroid.y += point.getPosition().y / vertices.size();
            centroid.z += point.getPosition().z / vertices.size();
        }
        
        // calculate normals from corresponding triangles
        for (MeshTriangle t : this) {
            Vector3d triangleNormal = new Vector3d(t.getPoint1().getPosition());
            triangleNormal.sub(t.getPoint3().getPosition());
            triangleNormal.normalize();
            
            Vector3d edge2 = new Vector3d(t.getPoint1().getPosition());
            edge2.sub(t.getPoint2().getPosition());
            edge2.normalize();
            
            triangleNormal.cross(triangleNormal, edge2);
            //Vector3d triangleNormal = (t.getPoint3().subtractPosition(t.getPoint1())).crossProduct(t.getPoint2().subtractPosition(t.getPoint1())).getPosition();
            
            normalMap.get(t.getPoint1().getPosition()).add(triangleNormal);
            normalMap.get(t.getPoint2().getPosition()).add(triangleNormal);
            normalMap.get(t.getPoint3().getPosition()).add(triangleNormal);
        }

        // Normalize computed normal vectors and, simultaneously, check how many of them
        // are properly oriented (with similar direction like a vector from the centroid to the
        // corresponding mesh vertex.
        long okNormals = normalMap.entrySet().stream()
                .mapToDouble(e -> {
                    e.getValue().normalize(); // normalize the normal vector
                    Vector3d v = new Vector3d(e.getKey()); // get vector from the centroid to the vertex
                    v.sub(centroid);
                    v.normalize();
                    return e.getValue().dot(v); // return cosine of the angle
                })
                .filter(c -> c > 0.0) // similar direction
                .count();

        if (okNormals < vertices.size() / 3.0) { // the majority has opposite orientation => invert all normals
            normalMap.values().parallelStream().forEach(n -> n.scale(-1.0)); 
        }

        for (MeshPoint p: getVertices()) {
            p.setNormal(normalMap.get(p.getPosition()));
        }
    }
    
    @Override
    public int getNumTriangles() {
        return cornerTable.getSize() / 3;
    }
    
    @Override
    public List<MeshTriangle> getTriangles() {
        List<MeshTriangle> ret = new ArrayList<>(getNumTriangles());
        for (MeshTriangle tri : this) {
            ret.add(tri);
        }
        return ret;
    }
    
    @Override
    public List<MeshTriangle> getAdjacentTriangles(int vertexIndex) {
        List<MeshTriangle> ret = new ArrayList<>();
            
        List<Integer> adjacentTrianglesI = cornerTable.getTriangleIndexesByVertexIndex(vertexIndex);
        for (Integer triI: adjacentTrianglesI) {
            List<Integer> triVerticesI = cornerTable.getIndexesOfVerticesByTriangleIndex(triI);
                MeshTriangle tri = new MeshTriangleImpl(
                    this,
                    triVerticesI.get(0),
                    triVerticesI.get(1),
                    triVerticesI.get(2));
                ret.add(tri);
            }
        
        return ret;
    }

    @Override
    public List<MeshTriangle> getAdjacentTriangles(int index1, int index2) {
        List<MeshTriangle> output = getAdjacentTriangles(index1);
        List<MeshTriangle> other = getAdjacentTriangles(index2);
        output.retainAll(other);
        return output;
    }

    @Override
    public List<MeshTriangle> getAdjacentTriangles(MeshTriangle tri) {
        Set<MeshTriangle> output = new HashSet<>();
        output.addAll(getAdjacentTriangles(tri.getIndex1()));
        output.addAll(getAdjacentTriangles(tri.getIndex2()));
        output.addAll(getAdjacentTriangles(tri.getIndex3()));
        output.remove(tri);

        return new ArrayList<>(output);
    }

    @Override
    public List<MeshTriangle> getNeighboringTriangles(MeshTriangle tri) {
        Set<MeshTriangle> output = new HashSet<>();

        //Find only the three triangles sharing edges with tri
        List<MeshTriangle> l1 = getAdjacentTriangles(tri.getIndex1());
        List<MeshTriangle> l2 = getAdjacentTriangles(tri.getIndex2());
        List<MeshTriangle> l3 = getAdjacentTriangles(tri.getIndex3());

        //Calculate the three intersections
        List<MeshTriangle> i1 = new ArrayList<>(l1);
        i1.retainAll(l2);

        List<MeshTriangle> i2 = new ArrayList<>(l2);
        i2.retainAll(l3);

        List<MeshTriangle> i3 = new ArrayList<>(l3);
        i3.retainAll(l1);

        output.addAll(i1);
        output.addAll(i2);
        output.addAll(i3);
        output.remove(tri);

        return new ArrayList<>(output);
    }
    
    @Override
    public Point3d getClosestAdjacentPoint(Point3d point, int vertexIndex) {
        double dist = Double.POSITIVE_INFINITY;
        Point3d ret = null;
        
        for (MeshTriangle tri: this.getAdjacentTriangles(vertexIndex)) {
            Point3d projection = tri.getClosestPoint(point);
            if (projection == null) {
                continue;
            }
            Vector3d aux = new Vector3d(projection);
            aux.sub(point);
            double d = aux.length();
            if (d < dist) {
                dist = d;
                ret = projection;
            }
        }

        // If not point has been found due to weird topology (collapsed triangles, etc.),
        // return the vertex itself
        return ret == null ? getVertex(vertexIndex).getPosition() : ret;
    }
    
    @Override
    public double curvatureDistance(Point3d point, int vertexIndex) {
        double dist = Double.POSITIVE_INFINITY;
        
        for (MeshTriangle tri: this.getAdjacentTriangles(vertexIndex)) {
            Point3d projection = tri.getClosestPoint(point);
            Vector3d aux = new Vector3d(projection);
            aux.sub(point);
            double d = aux.length();
            if (d < dist) {
                dist = d;
            }
        }
        
        return dist;
    }
    
    @Override
    public Iterator<MeshTriangle> iterator() {
        return new Iterator<>() {
            private int index;


            @Override
            public boolean hasNext() {
                return index < cornerTable.getSize();
            }

            @Override
            public MeshTriangle next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                int i1 = cornerTable.getRow(index + 0).getVertexIndex();
                int i2 = cornerTable.getRow(index + 1).getVertexIndex();
                int i3 = cornerTable.getRow(index + 2).getVertexIndex();

                MeshTriangle tri = new MeshTriangleImpl(MeshFacetImpl.this, i1, i2, i3);

                index += 3;
                return tri;
            }
        };
    }
    
    @Override
    public synchronized List<Point3d> calculateVoronoiPoints() {
        if (voronoiPoints == null) {
            voronoiPoints = new ArrayList<>(getNumTriangles());
            for (MeshTriangle tri: this) {
                voronoiPoints.add(tri.getVoronoiPoint());
            }
        } 
        return Collections.unmodifiableList(voronoiPoints);
    }
    
    @Override
    public TriangleFanImpl getOneRingNeighborhood(int vertexIndex) {
        if (vertexIndex < 0 || vertexIndex >= this.getNumberOfVertices()) {
            return null;
        }
        return new TriangleFanImpl(this, vertexIndex);
    }
    
    @Override
    public boolean simplify() {
        // aggregate duplicates into the map, remember old positions:
        Map<Point3d, MeshPoint> mapPoints = new HashMap<>();
        Map<Point3d, List<Integer>> mapOrigPositions = new HashMap<>();
        for (int i = 0; i < this.getNumberOfVertices(); i++) {
            Point3d v = this.getVertex(i).getPosition();
            Vector3d n = this.getVertex(i).getNormal();
            Vector3d t = this.getVertex(i).getTexCoord();
            if (!mapPoints.containsKey(v)) {
                mapPoints.put(v, new MeshPointImpl(v, n, t));
                mapOrigPositions.put(v, new ArrayList<>());
            } else if (n != null) {
                MeshPoint orig = mapPoints.get(v);
                Vector3d newNormal = new Vector3d(orig.getNormal());
                newNormal.add(n);
                newNormal.normalize();
                mapPoints.put(v, new MeshPointImpl(orig.getPosition(), newNormal, orig.getTexCoord()));
            }
            mapOrigPositions.get(v).add(i);
        }
        
        if (mapPoints.size() == getNumberOfVertices()) {
            return false;
        }
        
        // create shrinked list of vertices:
        List<MeshPoint> newVertices = new ArrayList<>(mapPoints.size());
        Map<Integer, Integer> mapOrigNew = new HashMap<>();
        for (Point3d v : mapPoints.keySet()) {
            MeshPoint p = mapPoints.get(v);
            if (p.getNormal() != null) {
                p.getNormal().normalize();
            }
            newVertices.add(p);
            
            for (Integer pos: mapOrigPositions.get(v)) {
                mapOrigNew.put(pos, newVertices.size()-1);
            }
        }
       
        // update corner table:
        for (int i = 0; i < this.cornerTable.getSize(); i++) {
            int origIndex = cornerTable.getRow(i).getVertexIndex();
            CornerTableRow newRow = new CornerTableRow(cornerTable.getRow(i));
            newRow.setVertexIndex(mapOrigNew.get(origIndex));
            cornerTable.replaceRow(i, newRow);
        }
        
        this.vertices = newVertices;
        return true;
    }

}

