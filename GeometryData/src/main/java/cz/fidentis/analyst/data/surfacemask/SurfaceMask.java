package cz.fidentis.analyst.data.surfacemask;

import cz.fidentis.analyst.data.mesh.MeshFacet;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Class representing the Interactive Mask, it is defined as a list of points
 * lines between them and bounding cylinders.
 * 
 * @author Mario Chromik
 */
public class SurfaceMask {
    private final List<MaskPoint3D> points = new ArrayList<>();
    
    /**
     * @return {@code true} if the mast has no point defined so far.
     */
    public boolean isEmpty() {
        return points.isEmpty();
    }
    
    /**
     * Adds point of Interactive Mask
     * @param surfacePoint a point lying on the surface
     * @param facet facet of the surface
     */
    public void addPoint(Point3d surfacePoint, MeshFacet facet, Vector3d normal, boolean isSelected, boolean isSampled) {
        if (points.isEmpty()) {
           points.add(new MaskPoint3D(surfacePoint, facet, normal, isSelected, isSampled));
           return;
        }
        
        if (points.size() == 1) {
           points.add(new MaskPoint3D(surfacePoint, facet, normal,isSelected, isSampled));
           return;
        }

        // add to the end
        points.add(new MaskPoint3D(surfacePoint, facet, normal, isSelected, isSampled));
    }
    

    /**
     * get points of interactive mask
     * @return point
     */
    public List<MaskPoint3D> getPoints() {
        return Collections.unmodifiableList(points);
    }

    
    /**
     * Class representing a single point of Interactive Mask.
     * 
     * @author Mario Chromik
     */
    public static final class MaskPoint3D {
        private final boolean isSelected;
        private final boolean isSampled;
        private final Point3d position;
        private final MeshFacet facet;
        private final Vector3d normal;
        /**
         * Constructor.
         * 
         * @param pos position of an Interactive Mask point in 3D space
         * @param fac facet of IM point
         */
        private MaskPoint3D(Point3d pos, MeshFacet fac, Vector3d norm, boolean isSelected, boolean isSampled) {
            position = pos;
            facet = fac;
            normal = norm;
            this.isSelected = isSelected;
            this.isSampled = isSampled;
        }
    
        public Point3d getPosition() {
            return position;
        }
        
        public MeshFacet getFacet() {
            return facet;
        }

        public Vector3d getNormal() {
            return normal;
        }

        public boolean getIsSampled() {
            return isSampled;
        }

        public boolean getIsSelected() {
            return isSelected;
        }
    }
}