package cz.fidentis.analyst.data.mesh;

import cz.fidentis.analyst.data.ray.impl.RayIntersectionImpl;
import cz.fidentis.analyst.data.ray.Ray;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * Adapter for the corner table representing a single triangle of {@code MeshFacet}.
 *
 * @author Natalia Bebjakova
 * @author Dominik Racek
 * @author Enkh-Undral EnkhBayar
 * @author Karol Kovac
 * @author Radek Oslejsek
 */
public interface MeshTriangle extends Iterable<MeshPoint>, Serializable {

    /**
     * Interpolation of the triangle surface points.
     *
     * @author Radek Oslejsek
     */
    enum Smoothing {
        /**
         * No interpolation. Point from the supporting plane of the triangle is used as the surface point.
         * Triangle's normal vector is  used as the surface normal.
         */
        NONE,

        /**
         * Interpolation of the normal vector only.
         * Point from the supporting plane of the triangle is used as the surface point.
         * Normal vector is interpolated from normals of vertices.
         */
        NORMAL,

        /**
         * Interpolation of the shape only.
         * Point from the supporting plane if moved "above" the surface to interpolate curvature of the surface.
         * Triangle's normal vector is  used as the surface normal.
         */
        SHAPE,

        /**
         * Complete interpolation.
         * Point from the supporting plane if moved "above" the surface to interpolate curvature of the surface.
         * Normal vector is interpolated from normals of vertices.
         */
        NORMAL_AND_SHAPE
    }

    /**
     * Returns 3D coordinates of the triangle's vertex.
     * @return 3D coordinates of the triangle's vertex
     */
    Point3d getVertex1();

    /**
     * Returns 3D coordinates of the triangle's vertex.
     * @return 3D coordinates of the triangle's vertex
     */
    Point3d getVertex2();

    /**
     * Returns 3D coordinates of the triangle's vertex.
     * @return 3D coordinates of the triangle's vertex
     */
    Point3d getVertex3();

    /**
     * Returns mesh point of the triangle's vertex.
     * @return mesh point of the triangle's vertex
     */
    MeshPoint getPoint1();

    /**
     * Returns mesh point of the triangle's vertex.
     * @return mesh point of the triangle's vertex
     */
    MeshPoint getPoint2();

    /**
     * Returns mesh point of the triangle's vertex.
     * @return mesh point of the triangle's vertex
     */
    MeshPoint getPoint3();

    /**
     * Returns the index under which the vertex is stored in the array of facet vertices
     * @return the index under which the vertex is stored in the array of facet vertices
     */
    int getIndex1();

    /**
     * Returns the index under which the vertex is stored in the array of facet vertices
     * @return the index under which the vertex is stored in the array of facet vertices
     */
    int getIndex2();

    /**
     * Returns the index under which the vertex is stored in the array of facet vertices
     * @return the index under which the vertex is stored in the array of facet vertices
     */
    int getIndex3();

    /**
     * Returns mesh facet backing the triangle.
     * @return mesh facet backing the triangle.
     */
    MeshFacet getFacet();

    /**
     * Computes and returns normalized normal vector from the vertices.
     * This vector is in the direction of the normals of vertices of the triangle.
     * May return {@code (NaN, NaN, NaN)} vector if the triangle is deformed
     * (alle vertices are in a line, all vertices have the same coordinates, etc.)
     *
     * @return normalized normal vector from the vertices.
     */
    Vector3d computeOrientedNormal();

    /**
     * Computes ray-triangle intersection.
     * Based on
     * <a href="https://courses.cs.washington.edu/courses/csep557/10au/lectures/triangle_intersection.pdf">this article</a>
     * but adapted to two-side triangles.
     *
     * @param ray a ray
     * @param smoothing smoothing strategy
     * @return the ray intersection or {@code null}
     */
    RayIntersectionImpl getRayIntersection(Ray ray, Smoothing smoothing);

    /**
     * Computes the point laying on the triangle which is closest to
     * given 3D point. Return point is either one of the triangle's vertices,
     * a point laying on triangles edge, or a point laying on the plane of
     * the triangle inside the triangle boundaries.
     *
     * @param point 3D point
     * @return the closest point or {@code null} if the input parameter is missing
     * or if triangle is "weird", e.g., has all vertices in a line, all vertices
     * with the same coordinates, etc.
     */
    Point3d getClosestPoint(Point3d point);

    /**
     * Checks whether the triangle is intersected by a plane
     *
     * @param normal normal defining the plane
     * @param d distance defining the plane
     * @return true if the triangle is intersected by the plane
     */
    boolean checkIntersectionWithPlane(Vector3d normal, double d);

    /**
     * Selects the common points shared by two triangles
     *
     * @param other The other triangle
     * @return the common points of two triangles
     */
    List<Point3d> getCommonPoints(MeshTriangle other);

    @Override
    Iterator<MeshPoint> iterator();

    /**
     * Return a center of circumcircle. This point represents the point
     * of Voronoi area used for Delaunay triangulation, for instance.
     *
     * @return the center of circumcircle
     */
    Point3d getVoronoiPoint();

    /**
     * Function tests if the mesh triangle is closer to the sample point than a certain distance and if is, calculates
     * the second fundamental form of the sample point regarding the plane defined by the mesh triangle, otherwise
     * returns null.
     * <p>
     *     For more details, see:
     *     <a href="https://ieeexplore.ieee.org/document/597794">V. Interrante et al.: Conveying the 3D shape of smoothly curving transparent surfaces via texture</a>,
     *     1997, doi: 10.1109/2945.597794
     * </p>
     *
     * @param samplePosition position of a point on model's surface selected to be a glyph
     * @param distanceAccepted squared distance; the triangle needs to be closer to the sample point than this distance
     *                         to be taken into account
     * @param base1 first vector of orthogonal base; the base consists of three vectors: the normal of the sample point
     *              and two vectors(base1 and base2) defining the tangent plane of the model at the sample point
     * @param base2 second vector of orthogonal base; the base consists of three vectors: the normal of the sample point
     *              and two vectors(base1 and base2) defining the tangent plane of the model at the sample point
     * @return curvature of the sample point represented by the rate the surface normal tips in the directions of
     *         orthogonal base
     */
    Vector4d getCurvatureOfTrianglePlane(Point3d samplePosition, double distanceAccepted, Vector3d base1, Vector3d base2);
}
