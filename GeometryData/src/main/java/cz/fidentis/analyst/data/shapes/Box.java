package cz.fidentis.analyst.data.shapes;

import cz.fidentis.analyst.data.mesh.MeshPoint;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.Serial;
import java.io.Serializable;

/**
 * A 3D box (e.g., a bounding box) of {@link MeshPoint}s.
 *
 * @author Natalia Bebjakova
 * @author Radek Oslejsek
 *
 * @param minPoint minimum corner point
 * @param maxPoint maximum corner point
 */
public record Box(Point3d minPoint, Point3d maxPoint) implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * Copy constructor.
     * @param bbox Original bounding box. Must not be {@code null}
     */
    public Box(Box bbox) {
        this(bbox.minPoint, bbox.maxPoint);
    }

    /**
     * Return centroid of the bounding cube.
     *
     * @return middle point of the bounding box
     */
    public Point3d midPoint() {
        Point3d p = new Point3d(minPoint);
        p.add(maxPoint);
        p.scale(0.5);
        return p;
    }

    /**
     * Return volume diagonal of the bounding box.
     *
     * @return maximal diagonal of bounding box
     */
    public double diagonalLength() {
        Vector3d v = new Vector3d(maxPoint);
        v.sub(minPoint);
        return v.length();
    }

    /**
     * Returns description of BoundignBox.
     *
     * @return String representation of the bounding box
     */
    @Override
    public String toString() {
        String str = "BoundingBox: ";
        str += System.lineSeparator();
        str += "\t" + "- min point : " + this.minPoint + System.lineSeparator();
        str += "\t" + "- max point : " + this.maxPoint + System.lineSeparator();
        return str;
    }

}
