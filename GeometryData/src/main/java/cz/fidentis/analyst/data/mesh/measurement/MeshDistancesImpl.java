package cz.fidentis.analyst.data.mesh.measurement;

import cz.fidentis.analyst.data.mesh.MeshFacet;

import java.io.Serial;
import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

/**
 * The implementation of mesh distances.
 *
 * @author Radek Oslejsek
 */
public class MeshDistancesImpl implements MeshDistances {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * Layers/decorators above the original distance measurement.
     * Last decorator = top layer
     */
    private final List<FacetDistancesDecorator> layers = new ArrayList<>();

    /**
     * A map of mesh facets and their decorated distance measurements
     */
    private final Map<MeshFacet, FacetDistances> facetDistances = new HashMap<>();

    @Override
    public FacetDistances addRawFacetMeasurement(RawFacetDistances facetDistances) {
        FacetDistances fd = Objects.requireNonNull(facetDistances);
        for (FacetDistancesDecorator layer : layers) { // decorate the raw distance with all layers
            fd = layer.clone(fd);
        }
        return this.facetDistances.put(Objects.requireNonNull(facetDistances.getMeasuredFacet()), fd);
    }

    @Override
    public void addLayer(FacetDistancesDecorator layer) {
        layers.add(Objects.requireNonNull(layer));
        // replace original FacetDistances with the new decorator pointing to the original FacetDistances object:
        facetDistances.entrySet().forEach(entry -> entry.setValue(layer.clone(entry.getValue())));
    }

    @Override
    public void clearLayers() {
        facetDistances.entrySet().forEach(entry -> { // for each mesh facet
            FacetDistances rawDist = entry.getValue();
            while (rawDist instanceof FacetDistancesDecorator) {
                rawDist = ((FacetDistancesDecorator) rawDist).getSubElement();
            }
            entry.setValue(rawDist);
        });
        layers.clear();
    }

    @Override
    public boolean containsFacet(MeshFacet meshFacet) {
        return facetDistances.containsKey(meshFacet);
    }

    @Override
    public boolean containsFacets(Collection<MeshFacet> meshFacets) {
        return facetDistances.keySet().containsAll(meshFacets);
    }

    @Override
    public FacetDistances getFacetMeasurement(MeshFacet meshFacet) {
        return facetDistances.get(meshFacet);
    }

    @Override
    public Map<MeshFacet, List<Double>> distancesAsMap() {
        Map<MeshFacet, List<Double>> ret = new HashMap<>();
        facetDistances.forEach((key, value) ->
                ret.put(key, value.stream()
                        .map(DistanceRecord::getDistance)
                        .toList()));
        return ret;
    }

    @Override
    public Map<MeshFacet, List<Double>> weightsAsMap() {
        Map<MeshFacet, List<Double>> ret = new HashMap<>();
        facetDistances.forEach((key, value) ->
                ret.put(key, value.stream()
                        .map(DistanceRecord::getWeight)
                        .toList()));
        return ret;
    }

    @Override
    public Stream<DistanceRecord> stream() {
        return facetDistances.values().stream() // a stream with adjusted weights
                .flatMap(FacetDistances::stream);
    }

    @Override
    public DoubleStream streamOfRawDistances() {
        return stream().mapToDouble(DistanceRecord::getDistance);
    }

    @Override
    public DoubleStream streamOfWeightedDistances() {
        return stream().mapToDouble(rec -> rec.getDistance() * rec.getWeight());
    }

    @Override
    public DoubleSummaryStatistics getDistanceStats() {
        return stream()
                .mapToDouble(DistanceRecord::getDistance)
                .filter(Double::isFinite)
                .summaryStatistics();
    }

    @Override
    public DoubleSummaryStatistics getWeightedStats() {
        return stream()
                .mapToDouble(rec -> rec.getDistance() * rec.getWeight())
                .filter(Double::isFinite)
                .summaryStatistics();
    }

    @Override
    public double getSampleStandardDeviation() {
        DoubleSummaryStatistics stats = getDistanceStats();
        double avg = stats.getAverage();
        long count = stats.getCount();
        double sum = stream()
                .mapToDouble(DistanceRecord::getDistance)
                .filter(Double::isFinite)
                .map(dist -> (dist - avg) * (dist- avg))
                .sum();
        return Math.sqrt((1.0 / (count-1)) * sum);
    }

    @Override
    public Iterator<FacetDistances> iterator() {
        return facetDistances.values().iterator();
    }
}
