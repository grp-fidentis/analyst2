package cz.fidentis.analyst.data.mesh.impl.io;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Utility class for exporting model to .obj file (vertices, normals and triangles)
 * 
 * @author Natalia Bebjakova
 */
public class MeshObjExporter {

    /**
     * Exports all facets of the model to .obj file
     *
     * @param model Model to be exported.
     * @param exportFile File to which model is exported
     * @throws IOException 
     */
    public static void exportModelToObj(MeshModel model, File exportFile) throws IOException {
        for(MeshFacet facet : model.getFacets()) {
            exportFacetToObj(facet, exportFile);
        }
    }

    /**
     * Exports facet to OBJ file.
     * It writes vertices, normals and triangles to file
     * 
     * @param facet Facet of the model to be exported, so far every model has one
     * @param exportFile file for exporting.
     * @throws java.io.IOException 
     */
    private static void exportFacetToObj(MeshFacet facet, File exportFile) throws IOException {
        int formatIndex = exportFile.getName().lastIndexOf(".");
        String fileName; //name that is writen to file
        
        if (formatIndex < 0) {
            fileName = exportFile.getName();
        } else {
            fileName = exportFile.getName().substring(0, formatIndex);
        }

        exportFile = new File(exportFile.getParent() + File.separator + fileName + ".obj");
        
        try (BufferedWriter out = new BufferedWriter(new FileWriter(exportFile))) {
           
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
            otherSymbols.setDecimalSeparator('.');  //dot as separator for decimal numbers
            DecimalFormat df = new DecimalFormat("#.####", otherSymbols);
             
            //writes vertices of the facet to file
            for (int j = 0; j < facet.getNumberOfVertices(); j++) {
                out.write("v " + df.format(facet.getVertices().get(j).getPosition().x) + " " 
                        + df.format(facet.getVertices().get(j).getPosition().y) + " " 
                        + df.format(facet.getVertices().get(j).getPosition().z));
                out.newLine();
            }
            
            //detects if first vertex has normal
            boolean hasAllNormals = facet.getVertices().get(0).getNormal() != null;
            
            //writes normals if there are any 
            for (int i = 0; i < facet.getNumberOfVertices(); i++) {
                if(facet.getVertex(i).getNormal() != null) {
                out.write("vn " + df.format(facet.getVertices().get(i).getNormal().x) + " "
                        + df.format(facet.getVertices().get(i).getNormal().y) + " "
                        + df.format(facet.getVertices().get(i).getNormal().z));
                out.newLine();
                }
            }
            
            for (int i = 0; i < facet.getCornerTable().getSize(); i += 3) {
                int v1i = facet.getCornerTable().getRow(i).getVertexIndex();
                int v2i = facet.getCornerTable().getRow(i + 1).getVertexIndex();
                int v3i = facet.getCornerTable().getRow(i + 2).getVertexIndex();
                
                out.write("f ");
                if (facet.getVertex(v1i).getNormal() != null && hasAllNormals) {
                    out.write((v1i + 1) + "//" + (v1i + 1) + " "
                            + (v2i + 1) + "//" + (v2i + 1) + " "
                            + (v3i + 1) + "//" + (v3i + 1)); 
                    out.newLine();
                } else {
                    out.write((v1i + 1) + " " + (v2i + 1) + " " + (v3i + 1));
                    out.newLine();
                }
            }
            
            out.write("#" + facet.getCornerTable().getSize() / 3 + " triangles");
            out.newLine();
        }
    }
    
}

