package cz.fidentis.analyst.data.surfacemask.impl;

import cz.fidentis.analyst.data.surfacemask.SurfaceMask2D;

import java.awt.*;
import java.util.List;

/**
 * Base class for SurfaceMask2D classes
 * @author Mario Chromik
 */
public abstract class SurfaceMask2DImpl implements SurfaceMask2D {
    private Point selectedPoint = null;
    private Point shiftPoint = null;
    private int samplingStrength = 5;
    private Color color;

    @Override
    public abstract List<PointToProject> getPointsToProject();
    @Override
    public abstract void shiftMask(int dx, int dy);
    @Override
    public abstract boolean selectPoint(Point location);
    @Override
    public abstract boolean addNewPoint(Point point);
    @Override
    public abstract boolean containsPoint(Point middle);
    /**
     * Gets selected point
     * @return selected point
     */
    public Point getSelectedPoint() {
        return selectedPoint;
    }

    /**
     * Sets selected point
     * @param point to set
     */
    public void setSelectedPoint(Point point) {
        selectedPoint = point;
    }

    /**
     * Gets sampling strength
     * @return
     */
    public int getSamplingStrength() {
        return samplingStrength;
    }

    /**
     * Sets sampling strength
     * @param strength to set
     */
    public void setSamplingStrength(int strength) {
        samplingStrength = strength;
    }

    /**
     * Updates selected point by changing it's location
     * @param location to set
     */
    public void updateSelectedPoint(Point location) {
        if (selectedPoint != null) {
            selectedPoint.setLocation(location);
        }
    }

    /**
     * Gets shift point
     * @return shift point
     */
    public Point getShiftPoint() {
        return shiftPoint;
    }

    /**
     * Sets shift point
     * @param point to set
     */
    public void setShiftPoint(Point point) {
        shiftPoint = point;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
