package cz.fidentis.analyst.data.mesh.measurement;

import cz.fidentis.analyst.data.mesh.MeshFacet;

import java.util.Objects;

/**
 * Addition layers above the basic distance measurements.
 * These decorators/layers affect weights of computed distances.
 *
 * @author Radek Oslejsek
 */
public abstract class FacetDistancesDecorator implements FacetDistances {

    private final FacetDistances subElement;

    /**
     * Constructor.
     *
     * @param subElement decorated sub-element, must not by {@code null}
     */
    public FacetDistancesDecorator(FacetDistances subElement) {
        this.subElement = Objects.requireNonNull(subElement);
    }

    /**
     * Constructor without sub-element! Use only for future clonning by the {@link #clone(FacetDistances)} method.
     */
    public FacetDistancesDecorator() {
        this.subElement = null;
    }

    /**
     * Creates a copy of the decorator but decorating given sub-element.
     * @param subElement decorated sub-element, must not by {@code null}
     */
    public abstract FacetDistancesDecorator clone(FacetDistances subElement);

    @Override
    public abstract DistanceRecord get(int pointIndex);

    @Override
    public int size() {
        return (subElement == null) ? -1 : subElement.size();
    }

    @Override
    public MeshFacet getMeasuredFacet() {
        return (subElement == null) ? null : subElement.getMeasuredFacet();
    }

    /**
     * Returns decorated sub-element
     * @return decorated sub-element
     */
    public FacetDistances getSubElement() {
        return subElement;
    }
}
