package cz.fidentis.analyst.data.mesh.impl.io;

import com.mokiat.data.front.parser.*;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.Material;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.mesh.impl.MeshModelImpl;
import cz.fidentis.analyst.data.mesh.impl.MeshPointImpl;
import cz.fidentis.analyst.data.mesh.impl.facet.MeshFacetImpl;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.*;
import java.util.*;


/**
 * Utility class for loading human face from OBJ data format.
 * Expects only one human face in one file stored as one object.
 * 
 * @author Marek Barinka
 * @author Katerina Zarska
 */
public class MeshObjLoader {

    /**
     * Opens file and loads data into MeshModel
     * @param file File containing face to load into a MeshModel
     * @return Complete MeshModel
     * @throws IOException There was problem with reading the file
     */
    public static MeshModel readFromObj(File file) throws IOException {
        OBJModel model;        
        try (InputStream is = new FileInputStream(file)) {
            IOBJParser objParser = new OBJParser();
            model = objParser.parse(is);
        }
        
        if (model.getObjects().isEmpty()) {
            throw new IOException("File doesn't contain any model");
        }

        OBJObject object = model.getObjects().get(0);

        return parseObjectToModel(model, object, loadMaterial(model, file));
    }

    /**
     * Parse OBJObject into MeshModel
     * @param model Model is needed in future. It's holding data pools
     * @param object Object to parse. It corresponds to our MeshModel
     * @return Returns complete model
     * @throws IOException Data are corrupted
     */
    private static MeshModel parseObjectToModel(OBJModel model, OBJObject object, Material material) throws IOException {
        MeshModel meshModel = new MeshModelImpl(material);
        // Our facet = loader mesh, create and fill all facets
        for (OBJMesh mesh : object.getMeshes()) {
            MeshFacet meshFacet = parseMeshToFacet(model, mesh);
            meshModel.addFacet(meshFacet);
        }
        return meshModel;
    }
    
    /**
     * Loads mtl into Material
     * @param library material library extracted from obj model
     * @param path path to object file directory
     * @return finished material
     */
    private static Material mtlToMaterial(MTLLibrary library, String path) {
        MTLMaterial mtl = library.getMaterials().get(0);
        
        Vector3d ambient = new Vector3d(mtl.getAmbientColor().r, mtl.getAmbientColor().g, mtl.getAmbientColor().b);
        Vector3d diffuse = new Vector3d(mtl.getDiffuseColor().r, mtl.getDiffuseColor().g, mtl.getDiffuseColor().b);
        Vector3d specular = new Vector3d(mtl.getSpecularColor().r, mtl.getSpecularColor().g, mtl.getSpecularColor().b);
        
        float shininess = mtl.getSpecularExponent();
        
        String texFileName = mtl.getDiffuseTexture();
        if (texFileName != null){
            texFileName = path + texFileName;
        }
        
        return new Material(
                null,
                ambient, diffuse, specular,
                shininess, 0.0, 0.0,
                texFileName);
    }

    /**
     * Parse OBJMesh into MeshFacet containing corner table data
     * @param model Model is needed in future. It's holding data pools
     * @param mesh Mesh to parse. It correspond to our MeshFacet
     * @return Returns complete facet
     * @throws IOException Data are corrupted
     */
    private static MeshFacet parseMeshToFacet(OBJModel model, OBJMesh mesh) throws IOException {
        MeshFacet meshFacet = new MeshFacetImpl();
        Map<MeshPoint, Integer> vertices = new HashMap<>();
        Map<Edge, Integer> edges = new HashMap<>();
        
        for (OBJFace face : mesh.getFaces()) {
            processFace(model, face, meshFacet, vertices, edges);
        }
        
        return meshFacet;
    }

    /**
     * Process one face in source data into data and insert them into CornerTable
     * @param model Model is needed in the future. It's holding data pools
     * @param face Face to process
     * @param meshFacet MeshFacet containing data pools
     * @param vertices Map containing information about processed vertices
     *                  and their index in CornerTable
     * @param edges Map containing edges and index of their opposite vertex
     * @throws IOException Data are corrupted
     */

    private static void processFace(OBJModel model, OBJFace face, MeshFacet meshFacet,
                    Map<MeshPoint, Integer> vertices, Map<Edge, Integer> edges) throws IOException {
        List<MeshPoint> trianglePoints = parseFaceToTriangle(model, face);
        
        int actRow = meshFacet.getCornerTable().getSize();
        
        // This cycle adds integer indices of new mesh points and add them to CornerTable
        for (MeshPoint vertex : trianglePoints) {
            Integer vertIndex = vertices.get(vertex);
            if (vertIndex == null) {
                int newIndex = meshFacet.getNumberOfVertices();
                vertices.put(vertex, newIndex);
                meshFacet.addVertex(vertex);
                vertIndex = newIndex;
            }
            CornerTableRow cornerTableRow = new CornerTableRow(vertIndex, -1);
            meshFacet.getCornerTable().addRow(cornerTableRow);
        }

        List<Edge> triangleEdges = new ArrayList<>();
        triangleEdges.add(new Edge(trianglePoints.get(0).getPosition(),
                    trianglePoints.get(1).getPosition(), actRow + 2));
        triangleEdges.add(new Edge(trianglePoints.get(1).getPosition(),
                    trianglePoints.get(2).getPosition(), actRow + 0));
        triangleEdges.add(new Edge(trianglePoints.get(2).getPosition(),
                    trianglePoints.get(0).getPosition(), actRow + 1));

        for (Edge e : triangleEdges) {
            // We are processing edge which we already found
            // We can set corner.opposite on both corners
            if (edges.containsKey(e)) {
                int oppositeCornerIndex = edges.get(e);
                meshFacet.getCornerTable().getRow(oppositeCornerIndex).setOppositeCornerIndex(e.getCornerIndex());
                meshFacet.getCornerTable().getRow(e.getCornerIndex()).setOppositeCornerIndex(oppositeCornerIndex);
                edges.remove(e);
            } else {
                edges.put(e, e.getCornerIndex());
            }
        }
    }

    /**
     * Parse face from face data into list of MeshPoint
     * @param model Model contains data pool
     * @param face Face contains information about actually processed triangle
     * @return List containing three MeshPoints parsed from face
     * @throws IOException If face is non-triangular
     */
    private static List<MeshPoint> parseFaceToTriangle(OBJModel model, OBJFace face) throws IOException {
        List<MeshPoint> result = new ArrayList<>();

        List<OBJDataReference> references = face.getReferences();

        for (OBJDataReference reference : references) {
            final OBJVertex vertex = model.getVertex(reference);
            Point3d coords = new Point3d(vertex.x, vertex.y, vertex.z);
            Vector3d norm = null;
            Vector3d texCoords = null;
            if (reference.hasNormalIndex()) {
                final OBJNormal normal = model.getNormal(reference);
                norm = new Vector3d(normal.x, normal.y, normal.z);
                norm.normalize();
            }
            if (reference.hasTexCoordIndex()) {
                final OBJTexCoord texCoord = model.getTexCoord(reference);
                texCoords = new Vector3d(texCoord.u, texCoord.v, texCoord.w);
            }
            result.add(new MeshPointImpl(coords, norm, texCoords));
        }
        if (result.size() != 3) {
            throw new IOException("Mesh contains non-triangular face");
        }
        return result;
    }

    private static Material loadMaterial(OBJModel model, File file) throws IOException {
        if (model.getMaterialLibraries().isEmpty()) {
            return null;
        }

        final IMTLParser mtlParser = new MTLParser();
        String path = file.getCanonicalFile().getParent() + File.separator;
        File mtlFile = new File(path + model.getMaterialLibraries().get(0));
        if (!mtlFile.exists()) {
            return null;
        }

        final MTLLibrary mtlLibrary = mtlParser.parse(new FileInputStream(mtlFile));
        return mtlLibrary.getMaterials().isEmpty() ? null :  mtlToMaterial(mtlLibrary, path);
    }

    /**
     * Helper class for finding opposite corners
     * 
     * @author Marek Barinka
     */
    private static class Edge {

        private final Point3d v1;
        private final Point3d v2;
        private final int cornerIndex;

        Edge(Point3d v1, Point3d v2, int cornerIndex) {
            this.v1 = v1;
            this.v2 = v2;
            this.cornerIndex = cornerIndex;
        }

        /**
         * Returns new edge containing same vertices with opposite order
         * and invalid cornerIndex value.
         * @return Inverted edge in @method(equals) meaning
         */
        public Edge getInvertedEdge() {
            return new Edge(this.v2, this.v1, -1);
        }

        public int getCornerIndex() {
            return cornerIndex;
        }

        /**
         * Hash code must be generated this way because of @method(equals)
         * @return hash code of edge
         */
        @Override
        public int hashCode() {
            int hash = 3;
            hash += Objects.hashCode(this.v1);
            hash += Objects.hashCode(this.v2);
            return hash;
        }

        /**
         * Two edges are considered same if they have same vertices.
         * @param obj Other edge to test
         * @return true if edges are same with opposite direction
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof Edge)) {
                return false;
            }
            final Edge other = (Edge) obj;
            return (other.v1.equals(this.v1) && other.v2.equals(this.v2)) || 
                    (other.v1.equals(this.v2) && other.v2.equals(this.v1));
        }
    }
}