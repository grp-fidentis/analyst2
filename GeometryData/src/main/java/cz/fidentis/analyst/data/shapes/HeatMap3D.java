package cz.fidentis.analyst.data.shapes;

import cz.fidentis.analyst.data.mesh.MeshFacet;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * A 3D meat map with a saturation "layer".
 *
 * @author Radek Oslejsek
 * @author Daniel Schramm
 */
public class HeatMap3D {

    /**
     * The default color for infinite values
     */
    public static final Color INITE_VALUE_COLOR = Color.BLACK;

    /**
     * Values at mesh vertices that are to be transferred to colors.
     */
    private final Map<MeshFacet, List<Double>> values;

    /**
     * Values at mesh vertices that determine the saturation level of heatmap colors.
     */
    private final Map<MeshFacet, List<Double>> saturation;

    /**
     * Minimum cut-off threshold for values
     */
    private double minValue;

    /**
     * Maximum cut-off threshold for values
     */
    private double maxValue;

    // https://www.color-hex.com/color-palette/15656
    public static final Color MIN_COLOR = new Color(214,158,159);
    public static final Color MAX_COLOR = new Color(228,224,177);

    /**
     * Constructor for given min-max values. Any values less than min and bigger than max
     * Maps are assumed immutable, i.e., additional changes to them do not affect the heatmap.
     * Maps have to contain the same facets.
     *
     * @param values Mesh values to be transformed into colors. Must not be {@code null}
     * @param saturation Saturation values Mesh values to be transformed into colors. Can be {@code null}
     * @param minValue Min cut-off value
     * @param maxValue Max cut-off value
     */
    public HeatMap3D(
            Map<MeshFacet, List<Double>> values,
            Map<MeshFacet, List<Double>> saturation,
            double minValue,
            double maxValue) {

        this.values = Objects.requireNonNull(values);
        this.saturation = saturation;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    /**
     * Constructor. Maps are assumed immutable, i.e., additional changes to them do not affect the heatmap.
     * Maps have to contain the same facets.
     *
     * @param values Mesh values to be transformed into colors. Must not be {@code null}
     * @param saturation Saturation values Mesh values to be transformed into colors. Can be {@code null}
     */
    public HeatMap3D(Map<MeshFacet, List<Double>> values, Map<MeshFacet, List<Double>> saturation) {
        this(values, saturation, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY);
        values.values().stream()
                .flatMap(Collection::stream)
                .filter(Double::isFinite)
                .forEach(val -> {
                    minValue = Math.min(minValue, val);
                    maxValue = Math.max(maxValue, val);
                });
    }

    /**
     * Returns mesh facet from which the heatmap consists
     * @return mesh facet from which the heatmap consists
     */
    public Set<MeshFacet> getFacets() {
        return Collections.unmodifiableSet(values.keySet());
    }

    /**
     * Maps the value and weight of given vertex into a heatmap color.
     * @param meshFacet Mesh facet covered by this heat map
     * @param vertexIndex Facet's vertex
     * @return A heatmap color
     */
    public Color getColor(MeshFacet meshFacet, int vertexIndex) {
        if (!values.containsKey(meshFacet)) {
            return null;
        }

        double val = values.get(meshFacet).get(vertexIndex);
        double sat = saturation == null ? 1d : saturation.get(meshFacet).get(vertexIndex);

        return Double.isFinite(val) ? getColor(val, sat) : INITE_VALUE_COLOR;
    }

    private Color getColor(double heatmapValue, double heatmapSaturation) {
        double relativeValue = ((heatmapValue - minValue) / (maxValue - minValue));
        relativeValue = Math.max(0.0, relativeValue); // cut off the value if it is outside the min-max range
        relativeValue = Math.min(1.0, relativeValue);

        float[] hsb1 = Color.RGBtoHSB(MIN_COLOR.getRed(), MIN_COLOR.getGreen(), MIN_COLOR.getBlue(), null);
        float h1 = hsb1[0];
        float s1 = hsb1[1];
        float b1 = hsb1[2];

        float[] hsb2 = Color.RGBtoHSB(MAX_COLOR.getRed(), MAX_COLOR.getGreen(), MAX_COLOR.getBlue(), null);
        float h2 = hsb2[0];
        float s2 = hsb2[1];
        float b2 = hsb2[2];

        float hue = getHue(h1, h2, relativeValue);
        float saturation = (float) (((1 - relativeValue) * s1 + relativeValue * s2) * heatmapSaturation);
        float brightness = (float) ((1 - relativeValue) * b1 + relativeValue * b2);

        return Color.getHSBColor(hue, saturation, brightness);
    }

    /**
     * Returns final hue based on the clockwise or counter-clockwise distance between hues h1 and h2
     * @param h1 First hue
     * @param h2 Second hue
     * @param measuredValue Measured value
     * @return Final hue
     */
    private static float getHue(float h1, float h2, double measuredValue) {
        float distCCW;
        float distCW;

        if (h1 >= h2) {
            distCCW = h1 - h2;
            distCW = 1 + h2 - h1;
        } else {
            distCCW = 1 + h1 - h2;
            distCW = h2 - h1;
        }

        float hue;

        if (distCW >= distCCW) {
            hue = (float) (h1 + (distCW * measuredValue));
        } else {
            hue = (float) (h1 - (distCCW * measuredValue));
        }

        if (hue < 0) {
            hue = 1 + hue;
        }
        if (hue > 1) {
            hue = hue - 1;
        }
        return hue;
    }
}
