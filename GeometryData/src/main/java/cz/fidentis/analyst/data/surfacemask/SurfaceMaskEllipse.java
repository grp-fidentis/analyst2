package cz.fidentis.analyst.data.surfacemask;

import java.awt.Point;

/**
 * Interface defining specific functionality for 2D surface mask ellipse
 * @author Mario Chromik
 */
public interface SurfaceMaskEllipse extends SurfaceMask2D {
    int POINT_SIZE = 20;

    int DEFAULT_SIZE = 100;

    /**
     * Gets the origin point
     * @return origin point
     */
    Point getOrigin();

    /**
     * Gets the resize point
     * @return resize point
     */
    Point getResize();

    /**
     * Gets height of a rectangle
     * @return height
     */
    int getHeight();

    /**
     * Gets width of a rectangle
     * @return
     */
    int getWidth();
}
