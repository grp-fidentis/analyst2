package cz.fidentis.analyst.data.surfacemask;

import java.awt.Point;
import java.awt.Rectangle;

/**
 * Interface defining the specific functionality of 2D surface mask line
 * @author Mario Chromik
 */
public interface SurfaceMaskLine extends SurfaceMask2D {
    int IN_BETWEEN_BBOX_OFFSET = 5;
    int POINT_SIZE = 10;

    /**
     * Gets a linked list of mask points
     * @return linked list
     */
    MaskPointList getMaskPointsList();

    /**
     * Deletes a point from the linked list
     * @param point to delete
     */
    void deletePoint(Point point);

    /**
     * Class representing the mask by a set of points connected by edges.
     * One direction linked list, with added size to make iteration over the points simpler
     * Additionally the last and the first points can be connected, closing the mask.
     * @author Mario Chromik
     */
    class MaskPointList {
        private MaskPoint first = null;
        private MaskPoint last = null;
        private int size = 0;

        /**
         * a mask is closeable if there is more than 2 points
         * @return true if closeable, else false
         */
        public boolean isCloseable() {
            return first != last  && size > 2;
        }

        /**
         * Checks if the mask is empty
         * @return true if empty, else false
         */
        public boolean isEmpty() {
            return first == null && last == null;
        }

        /**
         * Checks if the mask has been closed. The mask has been closed if its not empty,
         * is closeable and last point is connected to the first.
         * @return true if closed else false
         */
        public boolean isClosed() {
            return !isEmpty() && isCloseable() && last.next != null && last.next == first;
        }

        /**
         * gets the first point
         * @return first point
         */
        public MaskPoint getFirst() {
            return first;
        }

        /**
         * sets the first point
         * @param first point to set
         */
        public void setFirst(MaskPoint first) {
            this.first = first;
        }

        /**
         * gets the last point
         * @return the last point
         */
        public MaskPoint getLast() {
            return last;
        }

        /**
         * sets the last point
         * @param last point to set
         */
        public void setLast(MaskPoint last) {
            this.last = last;
        }

        public int getSize() {
            return size;
        }

        /**
         * adds a new point to the end
         * @param point to add
         */
        public void addPoint(Point point) {
            MaskPoint newPoint = new MaskPoint(point);
            if (isEmpty()) {
                this.first = newPoint;

            }
            if (last != null) {
                this.last.next = newPoint;
            }

            this.last = newPoint;
            this.size++;
        }

        /**
         * Inserts a new point behind an existing point with a successor. The inserted point will
         * be the new successor.
         * @param first point to insert behind
         * @param middle point to insert
         */
        public void insertBehind(MaskPoint first, MaskPoint middle) {
            if (first == this.last) {
                this.last = middle;
            }
            middle.next = first.next;
            first.next = middle;
            this.size++;
        }

        /**
         * deletes a selected point
         * @param previous predecessor of the point to be deleted
         * @param delete point to delete
         */
        public void deletePoint(MaskPoint previous, MaskPoint delete) {
            if (this.first == this.last) {
                this.first = null;
                this.last = null;

            } else if (this.first == delete) {
                if (isClosed()) {
                    this.last.next = null;
                }
                this.first = delete.next;

                delete.next = null;
                if (previous != null) {
                    previous.next = null;
                }

            } else if (this.last == delete) {
                this.last = previous;
                if (isClosed()) {
                    this.last.next = null;
                }
                previous.next = null;
                delete.next = null;

            } else {
                previous.next = delete.next;
            }

            this.size--;
        }

        /**
         * Gets a string representation of the list
         * @return a string representation of the list
         */
        public String toString() {
            MaskPoint point = this.first;
            StringBuilder sb = new StringBuilder();
            while (point != this.last) {
                sb.append("POINT:" + point.point.x + ", " + point.point.y + "; ");
                point = point.next;
            }
            return sb.toString();
        }
    }

    /**
     * A class representing the node in a mask point list
     * @author Mario Chromik
     */
    class MaskPoint {
        private Point point;
        private MaskPoint next = null;

        /**
         * Constructor with a single parameter
         * @param point initial point
         */
        public MaskPoint(Point point) {
            this.point = point;
        }

        /**
         * gets the point value of this node
         * @return point value
         */
        public Point getPoint() {
            return point;
        }

        /**
         * sets the successor of this node
         * @param next successor to set
         */
        public void setNext(MaskPoint next) {
            this.next = next;
        }

        public MaskPoint getNext() {
            return this.next;
        }

        /**
         * Verifies if this node is to be selected based on the location of the selected point
         * @param location location of the point
         * @return true if node is to be selected, else false
         */
        public boolean isPointToBeSelected(Point location) {
            Rectangle rect = new Rectangle(point.x - POINT_SIZE/2, point.y - POINT_SIZE/2, POINT_SIZE, POINT_SIZE);
            return rect.contains(location);
        }
    }
}
