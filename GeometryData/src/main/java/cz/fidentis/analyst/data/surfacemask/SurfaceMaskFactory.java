package cz.fidentis.analyst.data.surfacemask;

import cz.fidentis.analyst.data.surfacemask.impl.SurfaceMaskEllipseImpl;
import cz.fidentis.analyst.data.surfacemask.impl.SurfaceMaskLineImpl;
import cz.fidentis.analyst.data.surfacemask.impl.SurfaceMaskRectangleImpl;

/**
 * This interface provides services that instantiates surface masks.
 *
 * @author Mario Chromik
 */
public interface SurfaceMaskFactory {

    /**
     * Types of masks
     * @author Mario Chromik
     */
    enum MaskType {
        ELLIPSE, LINE, RECTANGLE
    }

    /**
     * Creates an empty mask of the given type
     *
     * @param maskType mask type
     * @return mask
     */
    static SurfaceMask2D create2dMask(MaskType maskType) {
        return switch (maskType) {
            case ELLIPSE -> new SurfaceMaskEllipseImpl();
            case LINE -> new SurfaceMaskLineImpl();
            case RECTANGLE -> new SurfaceMaskRectangleImpl();
        };
    }

    /**
     * Creates an empty mask of the given type
     *
     * @param mask original mask
     * @return mask
     */
    static SurfaceMask2D clone2dMask(SurfaceMask2D mask) {
        if (mask instanceof SurfaceMaskEllipse) {
            return new SurfaceMaskEllipseImpl((SurfaceMaskEllipseImpl) mask);
        } else if (mask instanceof SurfaceMaskRectangleImpl) {
            return new SurfaceMaskRectangleImpl((SurfaceMaskRectangleImpl) mask);
        } else if (mask instanceof SurfaceMaskLineImpl) {
            return new SurfaceMaskLineImpl((SurfaceMaskLineImpl) mask);
        } else {
            return null;
        }
    }
}
