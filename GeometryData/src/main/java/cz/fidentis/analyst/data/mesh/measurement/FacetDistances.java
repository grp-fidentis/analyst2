package cz.fidentis.analyst.data.mesh.measurement;

import cz.fidentis.analyst.data.mesh.MeshFacet;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * The result of the surface-to-surface distance measurement (i.e., multiple facets towards multiple facets).
 *
 * @author Radek Oslejsek
 */
public interface FacetDistances extends Iterable<DistanceRecord> {

    /**
     * Returns distance measurement for a single facet's vertex.
     *
     * @param pointIndex index of the facet's vertex from the range from zero to {@code MeshFacet.getNumberOfVertices()} - 1.
     * @return the mesh point with the distance value
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    DistanceRecord get(int pointIndex);

    /**
     * Returns the number of facet's vertices, i.e., the number of measured distances.
     * @return the number of facet's vertices, i.e., the number of measured distances.
     */
    int size();

    /**
     * Returns the source facet from which the measurement was performed.
     * @return the source facet from which the measurement was performed.
     */
    MeshFacet getMeasuredFacet();

    @Override
    default Iterator<DistanceRecord> iterator() {
        return new Iterator<>() {
            private int counter = 0;

            @Override
            public boolean hasNext() {
                return counter < size();
            }

            @Override
            public DistanceRecord next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                } else {
                    return get(counter++);
                }
            }
        };
    }

    /**
     * Returns a stream distance measurement records for all facet's vertices
     * @return a stream distance measurement records for all facet's vertices
     */
    default Stream<DistanceRecord> stream() {
        // Convert the iterator to Spliterator
        Spliterator<DistanceRecord> spliterator = Spliterators.spliteratorUnknownSize(iterator(), 0);
        // Get a Sequential Stream from spliterator
        return StreamSupport.stream(spliterator, false);
    }
}
