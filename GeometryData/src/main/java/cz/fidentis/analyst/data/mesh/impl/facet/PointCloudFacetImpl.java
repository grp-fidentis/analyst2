package cz.fidentis.analyst.data.mesh.impl.facet;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.impl.TriangleFanImpl;

import javax.vecmath.Point3d;
import java.util.*;

/**
 * A special facet consisting of mesh points only, without mesh topology (edges).
 * Therefore, many methods are not implemented.
 *
 * @author Radek Oslejsek
 */
public class PointCloudFacetImpl extends MeshFacetImpl {

    /**
     * Constructor.
     *
     * @param meshPoints Points of the mesh (point cloud). Must not be {@code null}
     */
    public PointCloudFacetImpl(Collection<MeshPoint> meshPoints) {
        super(meshPoints);
    }
    
    @Override
    public synchronized void calculateVertexNormals() {
       throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @return 
     */
    @Override
    public int getNumTriangles() {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     */
    @Override
    public List<MeshTriangle> getTriangles() {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @param vertexIndex
     * @return
     */
    @Override
    public List<MeshTriangle> getAdjacentTriangles(int vertexIndex) {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @param point
     * @param vertexIndex
     * @return 
     */
    @Override
    public Point3d getClosestAdjacentPoint(Point3d point, int vertexIndex) {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @param point
     * @param vertexIndex
     * @return 
     */
    @Override
    public double curvatureDistance(Point3d point, int vertexIndex) {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @return 
     */
    @Override
    public Iterator<MeshTriangle> iterator() {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @return 
     */
    @Override
    public List<Point3d> calculateVoronoiPoints() {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @param vertexIndex
     * @return 
     */
    @Override
    public TriangleFanImpl getOneRingNeighborhood(int vertexIndex) {
        throw new UnsupportedOperationException();
    }
    
    /**
     * This method is not supported
     * @return 
     */
    @Override
    public boolean simplify() {
        throw new UnsupportedOperationException();
    }
    
}
