package cz.fidentis.analyst.data.mesh.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.impl.RayIntersectionImpl;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;
import java.io.Serial;
import java.util.*;

/**
 * Adapter for the corner table representing a single triangle of {@code MeshFacet}.
 *
 * @author Natalia Bebjakova
 * @author Dominik Racek
 * @author Enkh-Undral EnkhBayar
 * @author Karol Kovac
 * @author Radek Oslejsek
 */

public class MeshTriangleImpl implements MeshTriangle {

    @Serial
    private static final long serialVersionUID = 1L;
    public static final double EPS = 1e-10; // tolerance

    private final MeshFacet facet;

    /**
     * Under which index is the corresponding vertex stored in the mesh facet
     */
    public final int index1;
    public final int index2;
    public final int index3;

    private Point3d voronoiPoint;

    /**
     * Creates new triangle
     *
     * @param facet Mesh facet
     * @param i1 which index is the first vertex stored in the mesh facet
     * @param i2 which index is the second vertex stored in the mesh facet
     * @param i3 which index is the third vertex stored in the mesh facet
     */
    public MeshTriangleImpl(MeshFacet facet, int i1, int i2, int i3) {
        if (facet == null) {
            throw new IllegalArgumentException("facet");
        }
        this.index1 = i1;
        this.index2 = i2;
        this.index3 = i3;
        this.facet = facet;
    }

    @Override
    public MeshFacet getFacet() {
        return facet;
    }

    @Override
    public Point3d getVertex1() {
        return facet.getVertex(index1).getPosition();
    }

    @Override
    public Point3d getVertex2() {
        return facet.getVertex(index2).getPosition();
    }

    @Override
    public Point3d getVertex3() {
        return facet.getVertex(index3).getPosition();
    }

    @Override
    public MeshPoint getPoint1() {
        return facet.getVertex(index1);
    }

    @Override
    public MeshPoint getPoint2() {
        return facet.getVertex(index2);
    }

    @Override
    public MeshPoint getPoint3() {
        return facet.getVertex(index3);
    }

    @Override
    public int getIndex1() {
        return index1;
    }

    @Override
    public int getIndex2() {
        return index2;
    }

    @Override
    public int getIndex3() {
        return index3;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(obj == null || obj.getClass()!= this.getClass()) {
            return false;
        }

        MeshTriangleImpl mt = (MeshTriangleImpl) obj;

        return ((index1 == mt.index1 || index1 == mt.index2 || index1 == mt.index3)
                && (index2 == mt.index1 || index2 == mt.index2 || index2 == mt.index3)
                && (index3 == mt.index1 || index3 == mt.index2 || index3 == mt.index3));
    }

    @Override
    public int hashCode() {
        return Objects.hash(index1, index2, index3);
    }

    @Override
    public Vector3d computeOrientedNormal() {
        Vector3d ab = new Vector3d(getVertex1());
        ab.sub(getVertex2());
        Vector3d ac = new Vector3d(getVertex1());
        ac.sub(getVertex3());
        Vector3d normal = new Vector3d();
        normal.cross(ab, ac);
        normal.normalize();
        orientNormal(normal);
        return normal;
    }

    @Override
    public RayIntersectionImpl getRayIntersection(Ray ray, Smoothing smoothing) {
        Point3d origin = ray.origin();
        Vector3d direction = ray.direction();

        Vector3d[] edges = new Vector3d[3]; // edges A->B, B->C, C->A
        Vector3d[] inner = new Vector3d[3]; // vectors from vertices to the intersection point
        Vector3d[] cross = new Vector3d[3]; // cross products

        // Compute and orient normal:
        edges[0] = getEdge(getVertex1(), getVertex2()); // A->B
        edges[2] = getEdge(getVertex1(), getVertex3()); // A->C => invert after usage!
        Vector3d normal = new Vector3d();
        normal.cross(edges[0], edges[2]);
        normal.normalize();
        orientNormal(normal);
        edges[2].scale(-1.0); // C->A

        // Determine the point of intersection:
        double np = normal.dot(new Vector3d(origin));
        double nv = normal.dot(direction);

        if (Math.abs(nv) < EPS) { // the ray is parallel to the triangle's plane
            return null;
        }

        double offset = normal.dot(new Vector3d(getVertex1()));
        double t = (offset - np) / nv;

        Point3d intersection = new Point3d(
                origin.x + direction.x * t,
                origin.y + direction.y * t,
                origin.z + direction.z * t);

        if (!isPointInTriangle(intersection, normal, edges, inner, cross)) {
            return null;
        }

        if (smoothing == Smoothing.NONE) {
            return new RayIntersectionImpl(intersection, normal, this, t >= 0);
        }

        Point3d bcCoords = getBarycentricCoords(cross);

        normal = (smoothing == Smoothing.NORMAL || smoothing == Smoothing.NORMAL_AND_SHAPE)
                ? getInterpolatedNormal(bcCoords)
                : normal;

        if (smoothing == Smoothing.NORMAL) {
            return new RayIntersectionImpl(intersection, normal, this, t >= 0);
        }

        // Interpolate shape:
        intersection = (smoothing == Smoothing.SHAPE || smoothing == Smoothing.NORMAL_AND_SHAPE)
                ? getInterpolatedIntersection(bcCoords, inner)
                : intersection;

        return new RayIntersectionImpl(intersection, normal, this, t >= 0);
    }

    @Override
    public Point3d getClosestPoint(Point3d point) {
        if (point == null) {
            return null;
        }

        // Project point to the triangle plane:
        Vector3d normal = computeOrientedNormal();
        Vector3d helperVector = new Vector3d(point);
        helperVector.sub(getVertex1());
        double dist = helperVector.dot(normal);
        Point3d projection = new Point3d(point);
        projection.scaleAdd(-dist, normal, projection);

        if (!isPointInTriangle(point, normal, new Vector3d[3], new Vector3d[3], new Vector3d[3])) {
            projection = getProjectionToClosestEdge(projection);
        }
        return projection;
    }

    @Override
    public boolean checkIntersectionWithPlane(Vector3d normal, double d) {
        double p1 = (normal.x * getVertex1().x) + (normal.y * getVertex1().y) + (normal.z * getVertex1().z) - d;
        double p2 = (normal.x * getVertex2().x) + (normal.y * getVertex2().y) + (normal.z * getVertex2().z) - d;
        double p3 = (normal.x * getVertex3().x) + (normal.y * getVertex3().y) + (normal.z * getVertex3().z) - d;

        //If one point is on one side of the plane and the other on the other side
        return ((p1 <= 0 || p2 <= 0 || p3 <= 0) && (p1 >= 0 || p2 >= 0 || p3 >= 0));
    }

    @Override
    public List<Point3d> getCommonPoints(MeshTriangle other) {
        List<Point3d> output = new ArrayList<>();

        if (getVertex1().equals(other.getVertex1()) || getVertex1().equals(other.getVertex2())
                || getVertex1().equals(other.getVertex3())) {
            output.add(getVertex1());
        }

        if (getVertex2().equals(other.getVertex1()) || getVertex2().equals(other.getVertex2())
                || getVertex2().equals(other.getVertex3())) {
            output.add(getVertex2());
        }

        if (getVertex3().equals(other.getVertex1()) || getVertex3().equals(other.getVertex2())
                || getVertex3().equals(other.getVertex3())) {
            output.add(getVertex3());
        }

        return output;
    }

    @Override
    public Iterator<MeshPoint> iterator() {
        return new Iterator<>() {
            private int counter = 0;

            @Override
            public boolean hasNext() {
                return counter < 3;
            }

            @Override
            public MeshPoint next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return switch (counter++) {
                    case 0 -> facet.getVertex(index1);
                    case 1 -> facet.getVertex(index2);
                    case 2 -> facet.getVertex(index3);
                    default -> null;
                };
            }
        };
    }

    @Override
    public Point3d getVoronoiPoint() {
        if (voronoiPoint != null) {
            return voronoiPoint;
        }

        MeshPoint vertex1 = facet.getVertex(index1);
        MeshPoint vertex2 = facet.getVertex(index2);
        MeshPoint vertex3 = facet.getVertex(index3);

        double a = subtractPosition(vertex2.getPosition(), vertex3).length();
        double b = subtractPosition(vertex3.getPosition(), vertex1).length();
        double c = subtractPosition(vertex2.getPosition(), vertex1).length();

        double d1 = a * a * (b * b + c * c - a * a);
        double d2 = b * b * (c * c + a * a - b * b);
        double d3 = c * c * (a * a + b * b - c * c);
        double dSum = d1 + d2 + d3;

        d1 /= dSum;
        d2 /= dSum;
        d3 /= dSum;

        Point3d v1Half = new Point3d(vertex2.getPosition());
        v1Half.add(vertex3.getPosition());
        v1Half.scale(0.5);

        Point3d v2Half = new Point3d(vertex1.getPosition());
        v1Half.add(vertex3.getPosition());
        v1Half.scale(0.5);

        Point3d v3Half = new Point3d(vertex2.getPosition());
        v1Half.add(vertex1.getPosition());
        v1Half.scale(0.5);

        if (d1 < 0) {
            Vector3d v = new Vector3d();
            v.cross(subtractPosition(v2Half, vertex3), subtractPosition(v1Half, vertex3));
            double v3Area = v.length() / 2.0;

            v.cross(subtractPosition(v3Half, vertex2), subtractPosition(v1Half, vertex2));
            double v2Area = v.length() / 2.0;

            v.cross(subtractPosition(v1Half, vertex1), subtractPosition(v3Half, vertex1));
            double v1Area = v.length() / 2.0;
            v.cross(subtractPosition(v1Half, vertex1), subtractPosition(v3Half, vertex1));
            v1Area += v.length() / 2.0;

            voronoiPoint = new Point3d(v1Area, v2Area, v3Area);
            return voronoiPoint;
        }
        if (d2 < 0) {
            Vector3d v = new Vector3d();
            v.cross(subtractPosition(v3Half, vertex1), subtractPosition(v2Half, vertex1));
            double v1Area = v.length() / 2.0;

            v.cross(subtractPosition(v1Half, vertex3), subtractPosition(v2Half, vertex3));
            double v3Area = v.length() / 2.0;

            v.cross(subtractPosition(v2Half, vertex2), subtractPosition(v1Half, vertex2));
            double v2Area = v.length() / 2.0;
            v.cross(subtractPosition(v2Half, vertex2), subtractPosition(v3Half, vertex2));
            v2Area += v.length() / 2.0;

            voronoiPoint = new Point3d(v1Area, v2Area, v3Area);
            return voronoiPoint;
        }
        if (d3 < 0) {
            Vector3d v = new Vector3d();
            v.cross(subtractPosition(v1Half, vertex2), subtractPosition(v3Half, vertex2));
            double v2Area = v.length() / 2.0;

            v.cross(subtractPosition(v2Half, vertex1), subtractPosition(v3Half, vertex1));
            double v1Area = v.length() / 2.0;

            v.cross(subtractPosition(v3Half, vertex3), subtractPosition(v2Half, vertex3));
            double v3Area = v.length() / 2.0;
            v.cross(subtractPosition(v3Half, vertex3), subtractPosition(v1Half, vertex3));
            v3Area += v.length() / 2.0;

            voronoiPoint = new Point3d(v1Area, v2Area, v3Area);
            return voronoiPoint;
        }

        Point3d aux1 = new Point3d(vertex1.getPosition());
        Point3d aux2 = new Point3d(vertex2.getPosition());
        Point3d aux3 = new Point3d(vertex3.getPosition());
        aux1.scale(d1);
        aux2.scale(d2);
        aux3.scale(d3);

        Point3d circumcenter = new Point3d(aux1);
        circumcenter.add(aux2);
        circumcenter.add(aux3);

        Vector3d v = new Vector3d();
        v.cross(subtractPosition(v2Half, vertex1), subtractPosition(circumcenter, vertex1));
        double v1Area = v.length() / 2.0;
        v.cross(subtractPosition(v3Half, vertex1), subtractPosition(circumcenter, vertex1));
        v1Area += v.length() / 2.0;

        v.cross(subtractPosition(v3Half, vertex2), subtractPosition(circumcenter, vertex2));
        double v2Area = v.length() / 2.0;
        v.cross(subtractPosition(v1Half, vertex2), subtractPosition(circumcenter, vertex2));
        v2Area += v.length() / 2.0;

        v.cross(subtractPosition(v1Half, vertex3), subtractPosition(circumcenter, vertex3));
        double v3Area = v.length() / 2.0;
        v.cross(subtractPosition(v2Half, vertex3), subtractPosition(circumcenter, vertex3));
        v3Area += v.length() / 2.0;

        voronoiPoint = new Point3d(v1Area, v2Area, v3Area);
        return voronoiPoint;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("TRI {" + System.lineSeparator());
        for (MeshPoint p: this) {
            ret.append("   ").append(p).append(System.lineSeparator());
        }
        ret.append("}");
        return ret.toString();
    }

    protected static Vector3d getEdge(Point3d v1, Point3d v2) {
        return new Vector3d(v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);
    }
    /**
     * Checks if point lying on the plane of the triangle also lies inside
     * the triangle boundaries.
     *
     * @param point Point located on the plane of the triangle
     * @param normal Normal vector
     * @param edges An array of vectors A->B, B->C, and C->A. If some of them is missing, then is computed
     *              automatically and stored in the array.
     * @param inner An array of vectors A->P, B->P, and C->P. If some of them is missing, then is computed
     *              automatically and stored in the array.
     * @param cross An output array of cross products A->B x A->P, etc.
     * @return true if point is in triangle, false otherwise
     */
    protected boolean isPointInTriangle(Point3d point, Vector3d normal, Vector3d[] edges, Vector3d[] inner, Vector3d[] cross) {
        // edge AB
        if (edges[0] == null) { // A->B
            edges[0] = getEdge(getVertex1(), getVertex2());
        }
        if (inner[0] == null) { // A->P
            inner[0] = getEdge(getVertex1(), point);
        }
        cross[0] = new Vector3d();
        cross[0].cross(edges[0], inner[0]);
        if (normal.dot(cross[0]) < 0) { // point is on the right side of AB
            return false;
        }

        // edge BC
        if (edges[1] == null) { // B->C
            edges[1] = getEdge(getVertex2(), getVertex3());
        }
        if (inner[1] == null) { // B->P
            inner[1] = getEdge(getVertex2(), point);
        }
        cross[1] = new Vector3d();
        cross[1].cross(edges[1], inner[1]);
        if (normal.dot(cross[1]) < 0) { // point is on the right side of BC
            return false;
        }

        // edge CA
        if (edges[2] == null) { // C->A
            edges[2] = getEdge(getVertex3(), getVertex1());
        }
        if (inner[2] == null) { // C->P
            inner[2] = getEdge(getVertex3(), point);
        }
        cross[2] = new Vector3d();
        cross[2].cross(edges[2], inner[2]);
        return normal.dot(cross[2]) >= 0; // point is on the right side of CA
    }

    /**
     * Orient given triangle normal so that it is in the direction of the normals of vertices of the triangle.
     *
     * @param normal normalized normal vector to be oriented. The vector can be inverted!
     * @return {@code true} if the normal direction was inverted
     */
    protected boolean orientNormal(Vector3d normal) {
        // if normal is in opposite direction then point normals, inverse it
        int positiveSignCount = 0;
        Vector3d[] normals = {getPoint1().getNormal(), getPoint2().getNormal(), getPoint3().getNormal()};
        for (Vector3d pointNormal : normals) {
            if (pointNormal == null) {
                continue;
            }
            double result = pointNormal.dot(normal);
            if (result > 0) {
                positiveSignCount++;
            } else {
                positiveSignCount--;
            }
        }
        if (positiveSignCount < 0) {
            normal.scale(-1);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Computes projection of tmp1 3D point laying on the plane of the triangle to the nearest edge of the triangle
     *
     * @param point    point laying on the plane of the triangle
     * @return perpendicular projection to the nearest edge
     */
    protected Point3d getProjectionToClosestEdge(Point3d point) {
        double minDistance = Double.POSITIVE_INFINITY;
        Point3d closestProjection = null;

        Point3d projection = getProjectionToEdge(point, getVertex2(), getVertex1());
        Vector3d aux = new Vector3d(point);
        aux.sub(projection);
        double dist = aux.length();
        if (dist < minDistance) {
            minDistance = dist;
            closestProjection = projection;
        }

        projection = getProjectionToEdge(point, getVertex3(), getVertex2());
        aux.sub(point, projection);
        dist = aux.length();
        if (dist < minDistance) {
            minDistance = dist;
            closestProjection = projection;
        }

        projection = getProjectionToEdge(point, getVertex1(), getVertex3());
        aux.sub(point, projection);
        dist = aux.length();
        if (dist < minDistance) {
            closestProjection = projection;
        }

        return closestProjection;
    }

    /**
     * Computes projection to edge
     *
     * @param point Point laying on the plane of the triangle
     * @param v1 first vertex of edge
     * @param v2 second vertex of edge
     * @return projection to edge
     */
    protected Point3d getProjectionToEdge(Point3d point, Point3d v1, Point3d v2) {
        Vector3d ab = new Vector3d(v2);
        ab.sub(v1);
        Vector3d ap = new Vector3d(point);
        ap.sub(v1);

        double abLenSquared = ab.lengthSquared();

        double t = ab.dot(ap) / abLenSquared;
        Point3d projection = new Point3d(
                v1.x + t * ab.x,
                v1.y + t * ab.y,
                v1.z + t * ab.z);

        Vector3d projectionToEdgeVertex1 = new Vector3d(v1);
        projectionToEdgeVertex1.sub(projection);
        double len1 = projectionToEdgeVertex1.length();

        Vector3d projectionToEdgeVertex2 = new Vector3d(v2);
        projectionToEdgeVertex2.sub(projection);
        double len2 = projectionToEdgeVertex2.length();

        if (Math.abs((len1 + len2) - Math.sqrt(abLenSquared)) < EPS) {
            return projection;
        }

        if (len1 < len2) {
            return v1;
        }

        return v2;
    }

    protected Vector3d subtractPosition(Point3d p1, MeshPoint p2) {
        Vector3d e = new Vector3d(p1);
        e.sub(p2.getPosition());
        return e;
    }

    /**
     * Calculates interpolated normal of a point, weights = barycentric coordinates
     *
     * @param barycentricCoords barycentric coordinates are returned by {@link #getBarycentricCoords(Vector3d[])}
     * @return interpolated normal
     */
    protected Vector3d getInterpolatedNormal(Tuple3d barycentricCoords) {
        // interpolate normal:
        Vector3d weightedNormalA = new Vector3d(getPoint1().getNormal());
        weightedNormalA.scale(barycentricCoords.x);
        Vector3d weightedNormalB = new Vector3d(getPoint2().getNormal());
        weightedNormalB.scale(barycentricCoords.y);
        Vector3d weightedNormalC = new Vector3d(getPoint3().getNormal());
        weightedNormalC.scale(barycentricCoords.z);

        Vector3d out = new Vector3d();
        out.add(weightedNormalA);
        out.add(weightedNormalB);
        out.add(weightedNormalC);
        out.normalize();

        return out;
    }

    /**
     * Calculates new position of a point from thr triangle so that the point lies on "smoothed" surface interpolated by normal in
     * triangle vertices.
     * Based on <a href="https://stackoverflow.com/questions/38717963/best-way-to-interpolate-triangle-surface-using-3-positions-and-normals-for-ray-t">algorithm</a>.
     *
     * @param barycentricCoords barycentric coordinates of the triangle as returned by {@link #getBarycentricCoords(Vector3d[])}
     * @param inner An array of vertex->point vectors returned by {@link #isPointInTriangle(Point3d, Vector3d, Vector3d[], Vector3d[], Vector3d[])}
     * @return interpolated point
     */
    protected Point3d getInterpolatedIntersection(Tuple3d barycentricCoords, Vector3d[] inner) {
        Vector3d ap = new Vector3d(-inner[0].x, -inner[0].y, -inner[0].z);
        Vector3d bp = new Vector3d(-inner[1].x, -inner[1].y, -inner[1].z);
        Vector3d cp = new Vector3d(-inner[2].x, -inner[2].y, -inner[2].z);

        Vector3d r1 = new Vector3d(getPoint1().getNormal());
        Vector3d r2 = new Vector3d(getPoint2().getNormal());
        Vector3d r3 = new Vector3d(getPoint3().getNormal());

        r1.scale(ap.dot(getPoint1().getNormal()));
        r2.scale(bp.dot(getPoint2().getNormal()));
        r3.scale(cp.dot(getPoint3().getNormal()));

        r1.add(getVertex1());
        r2.add(getVertex2());
        r3.add(getVertex3());

        r1.scale(barycentricCoords.x);
        r2.scale(barycentricCoords.y);
        r3.scale(barycentricCoords.z);

        Point3d ret = new Point3d(r1);
        ret.add(r2);
        ret.add(r3);

        return ret;
    }

    protected Point3d getBarycentricCoords(Vector3d[] cross) {
        double areaAPB = cross[0].length() / 2.0;
        double areaBPC = cross[1].length() / 2.0;
        double areaCPA = cross[2].length() / 2.0;
        double areaABC = areaAPB + areaBPC + areaCPA;
        return new Point3d(areaBPC / areaABC, areaCPA / areaABC, areaAPB / areaABC);
    }

    /**
     * Computes the intersection between an infinite line and its coplanar lines segment.
     * The infinite line is described like a ray, i.e., it has an origin and direction.
     * But the intersection is searched in both directions. Negative length of returned
     * ray indicates that the intersection point lies opposite direction than the
     * input first segment expected.
     *
     * @param origin starting point of first line
     *         Must not be {@code null}
     * @param vector <b>unit</b> direction vector of the first line.
     *         Must not be {@code null}
     * @param first first point on second line
     *         Must not be {@code null}
     * @param second second point on second line
     *         Must not be {@code null}
     * @param intersection output parameter storing the coordinates of the intersection.
     *                     The point is set to (infinity, infinity, infinity) if the lines are parallel,
     *                     or they don't lie at the same line.
     * @return {@code false} if the intersection point lies in the opposite direction than the input first segment expected.
     */
    protected static boolean getLinesIntersection(Point3d origin, Vector3d vector, Point3d first, Point3d second, Point3d intersection) {
        Point3d o = new Point3d(origin);
        Vector3d v = new Vector3d(vector);

        Point3d pointA = new Point3d(first);
        Point3d pointB = new Point3d(second);
        Vector3d ab = new Vector3d(pointB);
        ab.sub(pointA);

        Vector3d check = new Vector3d();
        check.cross(v, ab);

        Vector3d ao = new Vector3d(o);
        ao.sub(pointA);

        // https://lucidar.me/en/mathematics/check-if-a-point-belongs-on-a-line-segment/
        // Special case: the first segment is parallel to the second segment
        if (check.epsilonEquals(new Vector3d(), EPS)) { // lines are parallel
            check.cross(ao, ab);
            if (!check.epsilonEquals(new Vector3d(), EPS)) { // lines are not identical
                intersection.set(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
                return false;
            }
            double kao = ab.dot(ao);
            double kab = ab.dot(ab);
            if (kao < -EPS) { // origin is before A (ray is out of the AB line and the point A is closer that B)
                if (v.dot(ab) > 0) { // v is in direction of ab => A is closer and in the direction of the ray
                    intersection.set(pointA);
                    return true;
                } else { // v is in oposite direction of ab => A is closer, but in oposite direction
                    intersection.set(pointA);
                    return false;
                }
            }
            if (kao <= EPS) { // origin == A
                intersection.set(pointA);
                return true;
            }
            if (kao < kab - EPS) { // origin is between A and B
                intersection.set(origin);
                return true;
            }
            if (kao <= kab + EPS) { // origin == B
                intersection.set(pointB);
                return true;
            }

            // origin is after B
            if (v.dot(ab) < 0) { //v is in opposite direction of ab
                intersection.set(pointB);
                return true;
            } else {
                intersection.set(pointB);
                return false;
            }
        }

        // Generic case when the segments are not parallel
        // https://stackoverflow.com/a/22545485/15815615
        ab.normalize();
        double scalar = v.dot(ab);

        Vector3d oa = new Vector3d(ao);
        oa.scale(-1);
        double tmp1 = oa.dot(v) * scalar;
        double tmp2 = ao.dot(ab);
        double tmp3 = 1 - scalar * scalar;
        double s = (tmp1 + tmp2) / tmp3;

        Vector3d scaledV = new Vector3d(ab);
        scaledV.scale(s);
        intersection.set(pointA);
        intersection.add(scaledV);

        // check if intersection is between A and B
        double[] aCoor = {pointA.x, pointA.y, pointA.z};
        double[] bCoor = {pointB.x, pointB.y, pointB.z};
        double[] iCoor = {intersection.x, intersection.y, second.z};
        for (int i = 0; i < 3; i++) {
            if (!(Double.min(aCoor[i], bCoor[i]) <= iCoor[i] + EPS &&
                    iCoor[i] - EPS <= Double.max(aCoor[i], bCoor[i]))) {
                intersection.set(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
                return false;
            }
        }

        double t = oa.dot(v) + ab.dot(v) * s;
        return (t >= 0);
    }

    @Override
    public Vector4d getCurvatureOfTrianglePlane(Point3d samplePosition, double distanceAccepted, Vector3d base1, Vector3d base2) {
        if (getVertex1().distanceSquared(samplePosition) < distanceAccepted
                || getVertex2().distanceSquared(samplePosition) < distanceAccepted
                || getVertex3().distanceSquared(samplePosition) < distanceAccepted) {

            Vector3d normal = new Vector3d();
            normal.add(getPoint1().getNormal());
            normal.add(getPoint2().getNormal());
            normal.add(getPoint3().getNormal());
            normal.normalize();

            Vector3d middlePoint = new Vector3d();
            middlePoint.add(getPoint1().getPosition());
            middlePoint.add(getPoint2().getPosition());
            middlePoint.add(getPoint3().getPosition());
            middlePoint.scale(1 / (3.0f));

            Vector3d samplePoint = new Vector3d(samplePosition);
            Vector3d sampleToMiddle = new Vector3d();
            sampleToMiddle.sub(middlePoint, samplePoint);

            double omega11 = base1.dot(normal) / base1.dot(sampleToMiddle);
            double omega21 = base2.dot(normal) / base1.dot(sampleToMiddle);
            double omega12 = base1.dot(normal) / base2.dot(sampleToMiddle);
            double omega22 = base2.dot(normal) / base2.dot(sampleToMiddle);

            return new Vector4d(omega11, omega21, omega12, omega22);
        }
        return null;
    }
}
