package cz.fidentis.analyst.data.shapes;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.impl.facet.MeshRectangleFacetImpl;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Immutable symmetry plane.
 * 
 * @author Natalia Bebjakova
 * @author Dominik Racek
 * @author Mario Chromik
 */
public class Plane implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Vector3d normal;

    private double distance;

    /**
     * Constructor.
     * 
     * @param normal Normalized (!) normal vector of the plane
     * @param dist distance
     * @throws IllegalArgumentException if the @code{plane} argument is null
     */
    public Plane(Tuple3d normal, double dist) {
        this.normal = new Vector3d(normal);
        this.distance = dist;
    }
    
    /**
     * Copy constructor with possible normalization.
     *
     * @param plane original plane
     * @param normalize normalize the normal vector and distance so that the normal vector has length 1
     * @throws IllegalArgumentException if the @code{plane} argument is null
     */
    public Plane(Plane plane, boolean normalize) {
        this(plane.getNormal(), plane.getDistance());
        if (normalize) {
            double length = normal.length();
            if (length != 1.0) {
                normal.x /= length;
                normal.y /= length;
                normal.z /= length;
                distance /= length;
            }
        }
    }

    /**
     * Copy constructor without normalization.
     *
     * @param plane original plane
     * @throws IllegalArgumentException if the @code{plane} argument is null
     */
    public Plane(Plane plane) {
        this(plane, false);
    }
    
    /**
     * Construction from a 3D point.
     * 
     * @param point point in space
     * @throws IllegalArgumentException if the @code{plane} argument is null
     */
    public Plane(Tuple3d point) {
        this.normal = new Vector3d(point);
        this.distance = this.normal.length();
        this.normal.normalize();
    }
    
    /**
     * Symmetry plane constructed from two points.
     * 
     * @param point1 point in space
     * @param point2 point in space
     * @throws IllegalArgumentException if the @code{plane} argument is null
     */
    public Plane(Tuple3d point1, Tuple3d point2) {
        this.normal = new Vector3d(point1);
        this.normal.sub(point2);
        this.normal.normalize();
        this.distance = this.normal.dot(new Vector3d(
                (point1.x + point2.x) / 2.0,
                (point1.y + point2.y) / 2.0,
                (point1.z + point2.z) / 2.0)
        );
    }
    
    /**
     * Construction from three points.
     * 
     * @param p1 A first point from the plane
     * @param p2 A second point from the plane
     * @param p3 A third point from the plane
     * @throws NullPointerException if some point is missing
     */
    public Plane(Point3d p1, Point3d p2, Point3d p3) {
        Vector3d vec2 = new Vector3d(p3);
        vec2.sub(p1);
        this.normal = new Vector3d(p2);
        this.normal.sub(p1);
        this.normal.cross(this.normal, vec2);
        this.normal.normalize();
        this.distance = normal.dot(new Vector3d(p1));
    }
    
    /**
     * Creates average plane from existing planes.
     * 
     * @param planes Source planes, at least two
     * @throws IllegalArgumentException if the {@code planes} list is {@code null} or empty
     */
    public Plane(List<Plane> planes) {
        if (planes == null || planes.size() < 2) {
            throw new IllegalArgumentException("planes");
        }
        
        Vector3d n = planes.getFirst().getNormal();
        double d = planes.getFirst().getDistance();
        Vector3d refDir = planes.getFirst().getNormal();
        for (int i = 1; i < planes.size(); i++) {
            Vector3d normDir = planes.get(i).getNormal();
            if (normDir.dot(refDir) < 0) {
                n.sub(normDir);
                d -= planes.get(i).getDistance();
            } else {
                n.add(normDir);
                d += planes.get(i).getDistance();
            }
        }

        double normalLength = n.length();
        n.scale(1.0 / normalLength); // normalize
        this.normal = n;
        this.distance = d / normalLength;
    }
    
    protected Plane(double distance) {
        this.distance = distance;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.normal);
        hash = 97 * hash + Long.hashCode(Double.doubleToLongBits(this.distance));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Plane other = (Plane) obj;
        if (Double.doubleToLongBits(this.distance) != Double.doubleToLongBits(other.distance)) {
            return false;
        }
        return Objects.equals(this.normal, other.normal);
    }
    
    /**
     * Returns string description of the plane
     * 
     * @return description of the plane
     */
    @Override
    public String toString(){
        return normal + " dist " + distance;
    }
    
    public double getDistance() {
        return distance;
    }
    
    /**
     * Returns a point laying at the plane
     * @return a point laying at the plane
     */
    public Point3d getPlanePoint() {
        Point3d ret = new Point3d(normal);
        ret.scale(distance);
        return ret;
    }
    
    /**
     * Returns a normalized vector laying on the plane and directing up (toward the plus Y axis). 
     * 
     * @return a normalized vector laying on the plane and directing up 
     *         (toward the plus Y axis) or {@code null}
     */
    protected Vector3d getUpPlanarVector() {
        Vector3d v = new Vector3d(projectToPlane(new Point3d(0,1,0)));
        v.sub(projectToPlane(new Point3d(0,0,0)));
        if (v.length() == 0) {
            return null;
        }
        v.normalize();
        return v;
    }
    
    /**
     * Returns a normalized vector laying on the plane and directing right (toward the plus X axis). 
     * 
     * @return a normalized vector laying on the plane and directing up 
     *         (toward the plus X axis) or {@code null}
     */
    protected Vector3d getRightPlanarVector() {
        Vector3d v = new Vector3d(projectToPlane(new Point3d(1,0,0)));
        v.sub(projectToPlane(new Point3d(0,0,0)));
        if (v.length() == 0) {
            return null;
        }
        v.normalize();
        return v;
    }
    
    /**
     * Returns a normalized vector laying on the plane and directing ahead (toward the plus Z axis). 
     * 
     * @return a normalized vector laying on the plane and directing ahead 
     *         (toward the plus Z axis) or {@code null}
     */
    protected Vector3d getFrontPlanarVector() {
        Vector3d v = new Vector3d(projectToPlane(new Point3d(0,0,1)));
        v.sub(projectToPlane(new Point3d(0,0,0)));
        if (v.length() == 0) {
            return null;
        }
        v.normalize();
        return v;
    }
    
    /**
     * Translate the plane along its normal
     *
     * @param value a value to be added to the current plane's distance value
     * @return shifted plane
     */
    public Plane shift(double value) {
        return new Plane(this.normal, this.distance + value);
    }
    
    /**
     * Returns a plane with flipped direction
     * @return a plane with flipped direction
     */
    public Plane flip() {
        return new Plane(new Vector3d(-normal.x, -normal.y, -normal.z), -distance);
    }
    
    /**
     * Computes and returns transformation matrix that transforms given plane
     * into this plane.
     * 
     * @param other plane to be transformed
     * @param forbidRotation If {@code false}, then the rotation around 
     *        the normal is ignored.
     * @return transformation matrix or {@code null}
     */
    public Matrix4d getAlignmentMatrix(Plane other, boolean forbidRotation) {
        if (other == null) {
            return null;
        }
        
        Matrix4d retMat = getRotationAroundAxis(this.normal, other.getNormal(), null); // normal's rotation
        
        Point3d p = other.getPlanePoint();
        retMat.transform(p);
        Vector3d trDir = new Vector3d(this.getPlanePoint());
        
        if (p.equals(trDir)) { // no translation
            return retMat;
        }
        
        trDir.sub(p);
        
        Matrix4d trMat = new Matrix4d( // translation
                0, 0, 0, trDir.x,
                0, 0, 0, trDir.y,
                0, 0, 0, trDir.z,
                0, 0, 0, 0
        );
        retMat.add(trMat);
        
        if (forbidRotation) {
            Vector3d myVec = this.getUpPlanarVector();
            Vector3d otherVec = other.getUpPlanarVector();
            if (myVec == null || otherVec == null) {
                myVec = this.getRightPlanarVector();
                otherVec = other.getRightPlanarVector();
            }
            if (myVec == null || otherVec == null) {
                myVec = this.getFrontPlanarVector();
                otherVec = other.getFrontPlanarVector();
            }
            
            if (myVec != null && otherVec != null) {
                retMat.transform(otherVec); // now, it lays on me with random orientation
                Vector3d rotAxis = new Vector3d(myVec);
                rotAxis.cross(rotAxis, otherVec);
                Matrix4d axisTrMat = getRotationAroundAxis(myVec, otherVec, rotAxis);
                retMat.mul(axisTrMat, retMat);
            }
        }
        
        return retMat;
    }
    
    /**
     * Returns a point laying on the plane.
     * 
     * @param point A 3D point, which is projected to the plane
     * @return a point laying on the plane.
     * @throws NullPointerException if the {@code point} is {@code null}
     */
    public Point3d projectToPlane(Point3d point) {
        Point3d ret = new Point3d(normal);
        ret.scale(-getPointDistance(point));
        ret.add(point);
        return ret;
    }
    
    /**
     * Returns a point laying on the opposite side of the plane ("mirrors" the point).
     * 
     * @param point A 3D point to be reflected
     * @return a point on the opposite side of the plane.
     * @throws NullPointerException if the {@code point} is {@code null}
     */
    public Point3d reflectPointOverPlane(Point3d point) {
        Point3d ret = new Point3d(normal);
        ret.scale(-2.0 * getPointDistance(point));
        ret.add(point);
        return ret;
    }

    /**
     * Reflects the give unit vector over this plane.
     * 
     * @param vector A normalized 3D vector
     * @return reflected vector
     * @throws NullPointerException if the {@code point} is {@code null}
     */
    public Vector3d reflectUnitVectorOverPlane(Vector3d vector) {
        double s = 2 * this.getNormal().dot(vector);
        s /= this.getNormal().dot(this.getNormal());
        
        Vector3d v = new Vector3d(this.getNormal());
        v.scale(s);
        
        Vector3d rv = new Vector3d(vector);
        rv.sub(v);
        return rv;
    }
    
    /**
     * Returns rectangular mesh facet of this symmetry plane.
     * 
     * @param point A 3D point, which is projected to the plane and used as a center of the rectangular facet
     * @param width Width
     * @param height Height
     * @return a rectangular mesh facet of this symmetry plane
     * @throws NullPointerException if the {@code point} is {@code null}
     * @throws IllegalArgumentException if {@code width} or {@code height} are &lt;= 0
     */
    public MeshFacet getMesh(Point3d point, double width, double height) {
        return new MeshRectangleFacetImpl(projectToPlane(point), normal, width, height);
    }
    
    /**
     * Returns rectangular mesh facet of this symmetry plane. The centroid and size
     * are estimated from bounding box.
     * 
     * @param bbox Bounding box
     * @return a rectangular mesh facet of this symmetry plane
     * @throws NullPointerException if the {@code midPoint} or {@code bbox} are {@code null}
     */
    public MeshFacet getMesh(Box bbox) {
        return getMesh(bbox.midPoint(), bbox.diagonalLength(), bbox.diagonalLength());
    }
    
    /**
     * Returns reference to the normal vector
     * @return reference to the normal vector
     */
    public Vector3d getNormal() {
        return normal;
    }

    /**
     * Returns distance of the point from the plane.
     * @param point Point whose distance is to be computed
     * @return Point's distance. If the point is on the opposite side with
     * the respect to the plane's normal, the negative distance is returned
     */
    public double getPointDistance(Point3d point) {
        Point3d v = new Point3d(point);
        v.sub(this.getPlanePoint());
        return (normal.x * v.x) + (normal.y * v.y) + (normal.z * v.z);
        //return ((normal.x * point.x) + (normal.y * point.y) + (normal.z * point.z) + distance) / Math.sqrt(normSquare);
    }

    /**
     * Calculates an intersection of a plane and a line given by two points
     *
     * @param p1 first point of the line
     * @param p2 second point of the line
     * @return The point of intersection of null if no point found
     */
    public Point3d getIntersectionWithLine(Point3d p1, Point3d p2) {
        double distance1 = getPointDistance(p1);
        double distance2 = getPointDistance(p2);
        
        if (distance1 * distance2 > 0) { // both are positive or negative
            return null;
        }

        double t = distance1 / (distance1 - distance2);

        //Logger.print("EEE "+p1+ " "+getPointDistance(p1));
        Point3d output = new Point3d(p2);
        output.sub(p1);
        output.scale(t);
        output.add(p1);

        return output;
    }

    /**
     * Similarity metric based on the deviation of the normal vectors and the difference in distances.
     * 0 = perfect match.
     *
     * @param other Another plane to compare with
     * @return Similarity of the two planes as a number bigger or equal to zero. Zero means perfect match.
     */
    public double similarity(Plane other) {
        return 1.0 - Math.abs(this.normal.dot(other.normal)) +
                Math.abs(this.getPlanePoint().distance(other.getPlanePoint()));
    }
    
    /**
     * Computes transformation matrix that rotates the given normal vector 
     * so that it fits our normal vector.
     * Based on <a href="https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d">this discussion</a>
     *
     * @param targetV Vector towards which we want to rotate the second vector
     * @param trV Vector to be rotated
     * @param axis Rotation axis. If {@code null}, then it's automatically computed
     * @return transformation matrix
     */
    protected static Matrix4d getRotationAroundAxis(Vector3d targetV, Vector3d trV, Vector3d axis) {
        Matrix4d rotMat = new Matrix4d();
        rotMat.setIdentity();
        
        double cos = targetV.dot(trV); // cosine of the angle
        if (cos == -1) { // exactly oposite direction
            rotMat.mul(-1.0);
            return rotMat;
        }
        
        Vector3d k = axis;
        if (k == null) {
            k = new Vector3d();
            k.cross(trV, targetV);
        }
        
        Matrix4d cpMat = new Matrix4d( // skew-symmetric cross-product matrix of v
                 0.0, -k.z,  k.y, 0.0,
                 k.z,  0.0, -k.x, 0.0,
                -k.y,  k.x,  0.0, 0.0,
                 0.0,  0.0,  0.0, 0.0
        );
        rotMat.add(cpMat);
        
        cpMat.mul(cpMat);
        cpMat.mul(1.0 / (1.0 + cos));
        rotMat.add(cpMat);
        
        return rotMat;
    }
    
    /**
     * Rotates plane over Z axis by 90 degrees.
     */
    public void rotateOverZ() {
        double normalX = normal.x;
        normal.x = normal.y;
        normal.y = normalX;
    }
    
    /**
     * Rotates plane over Y axis by 90 degrees.
     */
    public void rotateOverY() {
         double normalX = normal.x;
         normal.x = normal.z;
         normal.z = normalX;
    }
    
}
