package cz.fidentis.analyst.data.surfacemask.impl;

import cz.fidentis.analyst.data.surfacemask.Layer;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask2D;
import cz.fidentis.analyst.data.surfacemask.SurfaceMaskFactory;
import cz.fidentis.analyst.data.surfacemask.SurfaceMaskLayers;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a single layer within a {@link SurfaceMaskLayers}, containing a surface mask
 * and a history of previous states for undo/redo functionality.
 *
 * @author Martin Bjaloň
 */
public class LayerImpl implements Layer {
    private List<SurfaceMask2D> masks;
    private SurfaceMask2D currentMask;
    private List<LayerImpl> history;
    private static final  int HISTORY_DEPTH = 5;
    private int currentHistoryIndex;
    private boolean visible;

    /**
     * Constructs a new Layer with the specified surface mask.
     */
    public LayerImpl() {
        this.masks = new ArrayList<>();
        this.history = new ArrayList<>();
        this.visible = true;
        this.currentHistoryIndex = -1;
    }

    @Override
    public SurfaceMask2D getCurrentMask() {
        return currentMask;
    }

    @Override
    public void setCurrentMask(int dir) {
        if (masks.indexOf(currentMask) + dir >= masks.size() || masks.indexOf(currentMask) + dir < 0) {
            return;
        }

        currentMask.setColor(Color.BLACK);
        currentMask = masks.get(masks.indexOf(currentMask) + dir);
        currentMask.setColor(Color.GREEN);
    }

    @Override
    public List<SurfaceMask2D> getAllMasks() {
        return masks;
    }

    @Override
    public void addMask(SurfaceMask2D mask) {
        if (this.currentMask != null) {
            this.currentMask.setColor(Color.BLACK);
        }
        mask.setColor(Color.GREEN);
        this.currentMask = mask;
        this.masks.add(mask);
    }

    @Override
    public void setVisibility(boolean visible) {
        this.visible = visible;
    }

    @Override
    public void updateHistory() {
        this.history.add(cloneLayer());

        if (history.size() > HISTORY_DEPTH * 2) {
            history = history.subList(HISTORY_DEPTH, history.size());
        }
        currentHistoryIndex = history.size() - 1;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public void goBack() {
        if (currentHistoryIndex - 1 < 0) {
            return;
        }
        currentHistoryIndex -= 1;
        masks = history.get(currentHistoryIndex).masks;
        currentMask.setColor(Color.BLACK);
        currentMask = history.get(currentHistoryIndex).currentMask;
        currentMask.setColor(Color.GREEN);
    }

    @Override
    public void goForward() {
        if (currentHistoryIndex + 1 >= history.size()) {
            return;
        }

        currentHistoryIndex += 1;
        masks = history.get(currentHistoryIndex).masks;
        currentMask.setColor(Color.BLACK);
        currentMask = history.get(currentHistoryIndex).currentMask;
        currentMask.setColor(Color.GREEN);
    }

    /**
     * Creates a deep copy of the current layer, including its masks and state.
     *
     * @return A new {@link LayerImpl} object that is a clone of the current layer.
     */
    private LayerImpl cloneLayer() {
        LayerImpl clone = new LayerImpl();
        for (SurfaceMask2D mask : this.masks) {
            clone.masks.add(SurfaceMaskFactory.clone2dMask(mask));
        }

        clone.currentMask = this.currentMask;
        clone.visible = this.visible;
        return clone;
    }
}
