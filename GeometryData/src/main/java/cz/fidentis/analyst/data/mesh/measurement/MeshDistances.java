package cz.fidentis.analyst.data.mesh.measurement;

import cz.fidentis.analyst.data.mesh.MeshFacet;

import java.io.Serializable;
import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

/**
 * The result of the surface-to-surface distance measurement (i.e., multiple facets towards multiple facets).
 * The object stores raw distance measurements of multiple facets and allows to decorate them with layers that modify
 * weights of the raw distances based on feature point priorities, masks, etc.
 *
 * @author Radek Oslejsek
 */
public interface MeshDistances extends Iterable<FacetDistances>, Serializable {

    /**
     * Adds distance measurement for a facet. Previous measurement is returned, if exists.
     *
     * @param facetDistances facet distance measurement without decorations (layers above).
     * @return previous distance data for the facet or {@code null}
     */
    FacetDistances addRawFacetMeasurement(RawFacetDistances facetDistances);

    /**
     * Add a new layer (decorates a raw measurement or its top layer)
     * @param layer New layer. Must not be {@code null}
     */
    void addLayer(FacetDistancesDecorator layer);

    /**
     * Removes all layers
     */
    void clearLayers();

    /**
     * Checks whether this measurement includes the given facet.
     *
     * @param meshFacet mesh facet to be checked
     * @return {@code true} if the facet is among the measured facets.
     */
    boolean containsFacet(MeshFacet meshFacet);

    /**
     * Checks whether this measurement includes all given facets.
     *
     * @param meshFacets mesh facets to be checked
     * @return {@code true} if all facets are among the measured facets.
     */
    boolean containsFacets(Collection<MeshFacet> meshFacets);

    /**
     * Returns decorated distance measurement for the given facet or {@code null}
     *
     * @param meshFacet required mesh facet
     * @return the decorated distance measurement or {@code null}
     */
    FacetDistances getFacetMeasurement(MeshFacet meshFacet);

    /**
     * Transforms {@code FacetDistance} objects into the map of the lists of distance values
     *
     * @return a map of distance values for each measured facet (nearest neighbors are omitted)
     */
    Map<MeshFacet, List<Double>> distancesAsMap();

    /**
     * Transforms {@code FacetDistance} objects into the map of the lists of weights
     *
     * @return a map of weights for each measured facet (nearest neighbors are omitted)
     */
    Map<MeshFacet, List<Double>> weightsAsMap();

    /**
     * Returns a stream of all decorated distance measurements (all vertices of all facets).
     * The stream includes possible invalid measurements, i.e., point distances with
     * infinite or NaN values, with missing nearing neighbors, etc. Use stream filtering
     * to remove them.
     *
     * @return a stream of all decorated distance measurements (all vertices of all facets)
     */
    Stream<DistanceRecord> stream();

    /**
     * Returns a stream of raw distances (i.e., distances that were measured and are not affected by weights)
     * of all vertices of all facets. The stream can include infinite numbers or NaNs values.
     * Use stream filtering to remove them.
     *
     * @return a stream of raw distances
     */
    DoubleStream streamOfRawDistances();

    /**
     * Returns a stream of weighted distances (i.e., distances multiplied by their weight)
     * of all vertices of all facets. The stream can include infinite numbers or NaNs values.
     * Use stream filtering to remove them.
     *
     * @return a stream of raw distances
     */
    DoubleStream streamOfWeightedDistances();

    /**
     * Returns statistics of all distances. Only finite values are taken into account
     * while infinite or NaN values are omitted from the calculation.
     * <b>Weights are ignored!</b>
     *
     * @return statistics of all computed distances.
     */
    DoubleSummaryStatistics getDistanceStats();

    /**
     * Returns statistics of all distances. Only finite values are taken into account
     * while infinite or NaN values are omitted from the calculation.
     * <b>The distances are multiplied by weights.</b>
     *
     * @return statistics of all computed distances.
     */
    DoubleSummaryStatistics getWeightedStats();

    /**
     * Returns <a href="https://en.wikipedia.org/wiki/Standard_deviation">corrected sample standard deviation</a>.
     * Only finite values are taken into account while infinite or NaN values are omitted from the calculation.
     * <b>Weights are ignored!</b>
     *
     * @return corrected sample standard deviation
     */
    double getSampleStandardDeviation();

    @Override
    Iterator<FacetDistances> iterator();
}
