package cz.fidentis.analyst.data.ray;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A ray with origin, unit direction, and length.
 * 
 * @author Radek Oslejsek
 * @param origin Ray's origin
 * @param direction Ray's unit direction
 * @param length  Ray's length
 */
public record Ray(Point3d origin, Vector3d direction, double length) {

    /**
     * Constructor a standard ray with length 1
     * 
     * @param origin Ray's origin
     * @param direction Ray's unit direction
     */
    public Ray(Point3d origin, Vector3d direction) {
        this(origin, direction, 1.0);
    }
    
    /**
     * Constructor.
     * 
     * @param origin Ray's origin
     * @param direction Ray's unit direction
     * @param length  Ray's length
     */
    public Ray(Point3d origin, Vector3d direction, double length) {
        this.origin = new Point3d(origin);
        this.direction = new Vector3d(direction);
        this.length = length;
    }
    
    /**
     * Copy constructor.
     * 
     * @param orig Original ray. Must not be {@code null}
     */
    public Ray(Ray orig) {
        this(orig.origin, orig.direction, orig.length);
    }

    /**
     * Returns the ray with inverted direction.
     * @return the ray with inverted direction.
     */
    public Ray invert() {
        Vector3d dir = this.direction;
        dir.scale(-1.0);
        return new Ray(origin, dir, length);
    }
    
    @Override
    public String toString() {
        return "origin: " + origin + " direction:" + direction;
    }
}
