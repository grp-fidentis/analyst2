package cz.fidentis.analyst.data.mesh.impl;

import cz.fidentis.analyst.data.mesh.*;
import cz.fidentis.analyst.data.mesh.impl.facet.MeshFacetImpl;

import javax.vecmath.Point3d;
import java.io.Serial;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The main object for triangular meshes. Each mesh model consists of one or
 * more mesh facets.
 *
 * @author Matej Lukes
 * @author Radek Oslejsek
 * @author Katerina Zarska
 */
public class MeshModelImpl implements MeshModel {

    @Serial
    private static final long serialVersionUID = 1L;

    private List<MeshFacet> facets = new ArrayList<>();
    private Material material;
    private boolean vertNormalsEstimated = false;

    /**
     * Constructor of an empty mesh.
     */
    public MeshModelImpl() {
        this((Material) null);
    }

    /**
     * Constructor for an empty mesh.
     *
     * @param material Material. Can be {@code null}
     */
    public MeshModelImpl(Material material) {
        this.material = material;
    }

    /**
     * Copy constructor.
     *
     * @param meshModel copied MeshModel
     */
    public MeshModelImpl(MeshModel meshModel) {
        for (MeshFacet facet : meshModel.getFacets()) {
            facets.add(new MeshFacetImpl(facet));
        }
        material = meshModel.getMaterial();
    }

    @Override
    public List<MeshFacet> getFacets() {
        return Collections.unmodifiableList(facets);
    }

    @Override
    public void addFacet(MeshFacet facet) {
        facets.add(facet);
    }

    @Override
    public Material getMaterial() {
        return material;
    }

    @Override
    public boolean hasMaterial() {
        return material != null;
    }

    @Override
    public boolean hasCurvature() {
        return facets.get(0).getVertices().get(0).getCurvature() != null;
    }

    @Override
    public void compute(MeshVisitor visitor, boolean concurrently) {
        if (concurrently && visitor.isThreadSafe()) {
            ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

            for (MeshFacet f : this.facets) {
                Runnable worker = () -> f.accept(visitor);
                executor.execute(worker);
            }

            // Wait until all executors are finished:
            executor.shutdown();
            while (!executor.isTerminated()) {
            }
        } else {
            for (MeshFacet f : this.facets) {
                f.accept(visitor);
            }
        }
    }

    @Override
    public void compute(MeshVisitor visitor) {
        compute(visitor, false);
    }

    @Override
    public void simplifyModel() {
        for (MeshFacet f : this.facets) {
            f.simplify();
        }
    }

    @Override
    public String toString() {
        int verts = 0;
        for (MeshFacet f : facets) {
            verts += f.getNumberOfVertices();
        }
        return facets.size() + " facets with " + verts + " vertices";
    }

    @Override
    public long getNumVertices() {
        return facets
                .stream()
                .mapToInt(MeshFacet::getNumberOfVertices)
                .sum();
    }

    @Override
    public Point3d getCentroid() {
        Point3d c = new Point3d(0, 0, 0);
        final long size = getNumVertices();
        getFacets().stream()
                .flatMap(f -> f.getVertices().stream())
                .map(MeshPoint::getPosition)
                .forEach(p -> {
                    c.x += p.x / size;
                    c.y += p.y / size;
                    c.z += p.z / size;
                });
        return c;
    }

    @Override
    public boolean hasEstimatedVertNormals() {
        return vertNormalsEstimated;
    }

    @Override
    public boolean estimateVertexNormals(boolean recomputeAll) {
        if (recomputeAll) {
            getFacets().forEach(MeshFacet::calculateVertexNormals);
            vertNormalsEstimated = true;
            return true;
        }

        List<MeshFacet> missing = getFacets().stream()
                .filter(f -> !f.hasVertexNormals())
                .toList();

        if (!missing.isEmpty()) {
            missing.forEach(MeshFacet::calculateVertexNormals);
            vertNormalsEstimated = true;
            return true;
        }

        return false;
    }

    @Override
    public boolean isPersistentDataModified() {
        return false;
    }
}
