/**
 * The top-level package for data structures related to triangular meshes.
 */
package cz.fidentis.analyst.data.mesh;
