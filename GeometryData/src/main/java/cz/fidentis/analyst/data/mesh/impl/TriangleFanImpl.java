package cz.fidentis.analyst.data.mesh.impl;

import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTable;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.TriangleFan;

import java.util.*;

/**
 * The implementation of the triangle fan interface.
 * 
 * @author Radek Oslejsek
 */
public class TriangleFanImpl implements TriangleFan {
    
    private final int centralVertex;
    private final MeshFacet facet;
    private final List<Integer> vertices = new LinkedList<>();
    private final List<Integer> triangles = new LinkedList<>();
    
    /**
     * Constructor.
     * 
     * @param facet Mesh facet
     * @param vert Central vertex for the triangle fan
     * @throws IllegalArgumentException if some parameter is missing or wrong
     */
    public TriangleFanImpl(MeshFacet facet, int vert) {
        if (facet == null) {
            throw new IllegalArgumentException("facet");
        }
        
        centralVertex = vert;
        this.facet = facet;
        
        // get first appearance of the vertex in the corner table:
        int vertRow = facet.getCornerTable().getVertToCornerCache().get(vert).get(0);
        
        computeOneRingData(vertRow);
    }
    
    /**
     * Returns {@code true} if the triangle fan is enclosed (i.e., is not boundary).
     * 
     * @return {@code true} if the triangle fan is enclosed (i.e., is not boundary).
     */
    public boolean isEnclosed() {
        return Objects.equals(vertices.get(0), vertices.get(vertices.size()-1));
    }
    
    /**
     * Returns {@code true} if the triangle fan is boundary (i.e., is not enclosed).
     * 
     * @return {@code true} if the triangle fan is enclosed (i.e., is not boundary).
     */
    public boolean isBoundary() {
        return !isEnclosed();
    }
    
    /**
     * Vertex indices of the 1-ring neighborhood. If the triangle fan is enclosed
     * then the first and last vertices are the same.
     * 
     * @return Vertex indices of the 1-ring neighborhood.
     */
    public List<Integer> getVertices() {
        return Collections.unmodifiableList(vertices);
    }
    
    /**
     * Triangle indices of the 1-ring neighborhood. The first triangle is related to
     * the edge delimited by first and second vertex, etc. The number of triangles 
     * corresponds to the number of vertices minus one.
     * 
     * @return Triangle indices of the 1-ring neighborhood.
     */
    public List<Integer> getTriangles() {
        return Collections.unmodifiableList(triangles);
    }
    
    private void computeOneRingData(int vertRow) {
        CornerTable ct = facet.getCornerTable();
        int ringVertRow = ct.getIndexOfNextCornerInFace(vertRow);
        while (true) {
            vertices.add(facet.getCornerTable().getRow(ringVertRow).getVertexIndex());
            if (vertices.size() > 1 && isEnclosed()) {
                return; // the ring is closed; we are done
            }
            ringVertRow = ct.getIndexOfOppositeCorner(ct.getIndexOfNextCornerInFace(ringVertRow));
            if (ringVertRow == -1) {
                break; // we reached an open end
            }
            triangles.add(ct.getIndexOfFace(ringVertRow));
            if (vertices.size() > ct.getSize()) { //emergency break
                throw new RuntimeException("Error in mesh topology");
            }
        }
        
        ringVertRow = ct.getIndexOfPreviousCornerInFace(vertRow);
        while (true) {
            vertices.add(0, ct.getRow(ringVertRow).getVertexIndex());
            if (vertices.size() > 1 && isEnclosed()) {
                return; // the ring is closed; we are done
            }
            ringVertRow = ct.getIndexOfOppositeCorner(ct.getIndexOfPreviousCornerInFace(ringVertRow));
            if (ringVertRow == -1) {
                break; // we reached an open end
            }
            triangles.add(ct.getIndexOfFace(ringVertRow));
            if (vertices.size() > ct.getSize()) { //emergency break
                throw new RuntimeException("Error in mesh topology");
            }
        }
    }
    
    @Override
    public Iterator<MeshTriangle> iterator() {
        return new Iterator<>() {
            private int index;
    
            @Override
            public boolean hasNext() {
                return index < triangles.size();
            }

            @Override
            public MeshTriangleImpl next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                
                MeshTriangleImpl tri = new MeshTriangleImpl(facet, centralVertex, index, index+1);
                index++;
                return tri;
            }    
        };
    }
    
}
