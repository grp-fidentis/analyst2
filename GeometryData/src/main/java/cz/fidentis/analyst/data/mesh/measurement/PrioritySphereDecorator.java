package cz.fidentis.analyst.data.mesh.measurement;

import javax.vecmath.Point3d;
import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * Given a sphere, this decorator increases weight of measurements inside the sphere proportionally to their
 * distance to sphere's center.
 * <p>
 *     For facet's measurements from outside the sphere, the weights are untouched.
 *     For measurements from inside the sphere, the weight is linearly approximated by the distance from
 *     the sphere's center (in the center the weight is 1.0, at the border the weight is 0.0).
 *     The weight is changed only if the new value is bigger that existing weight.
 * </p>
 *
 * @author Radek Oslejsek
 */
public class PrioritySphereDecorator extends FacetDistancesDecorator implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Point3d center;
    private final double radius;

    /**
     * Constructor.
     *
     * @param subElement decorated sub-element, must not by {@code null}
     * @param center Position of the priority sphere. Must not be {@code null}
     * @param radius Radius of the priority sphere (must be greater than or equal to zero)
     */
    public PrioritySphereDecorator(FacetDistances subElement, Point3d center, double radius) {
        super(subElement);
        if (radius < 0) {
            throw new IllegalArgumentException("sphereRadius");
        }
        this.center = Objects.requireNonNull(center);
        this.radius = radius;
    }

    /**
     * Constructor without sub-element! Use only for future clonning by the {@link #clone(FacetDistances)} method.
     *
     * @param center Position of the priority sphere. Must not be {@code null}
     * @param radius Radius of the priority sphere (must be greater than or equal to zero)
     */
    public PrioritySphereDecorator(Point3d center, double radius) {
        if (radius < 0) {
            throw new IllegalArgumentException("sphereRadius");
        }
        this.center = Objects.requireNonNull(center);
        this.radius = radius;
    }

    @Override
    public FacetDistancesDecorator clone(FacetDistances subElement) {
        return new PrioritySphereDecorator(subElement, center, radius);
    }

    @Override
    public DistanceRecord get(int pointIndex) {
        DistanceRecord rec = getSubElement().get(pointIndex);
        double distance = center.distance(rec.getFacetPoint().getPosition());
        if (distance <= radius) {
            double weight = 1 - distance / radius;
            if (weight > rec.getWeight()) {
                rec.setWeight(weight);
            }
        }
        return rec;
    }
}
