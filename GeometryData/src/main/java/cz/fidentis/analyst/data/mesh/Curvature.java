package cz.fidentis.analyst.data.mesh;

import javax.vecmath.Vector3d;
import java.io.Serial;
import java.io.Serializable;

/**
 * Curvature of a single mesh point.
 * 
 * @author Radek Oslejsek
 *
 * @param minPrincipal Minimum principal curvature
 * @param maxPrincipal Maximum principal curvature
 * @param gaussian Gaussian curvature
 * @param mean Mean curvature
 * @param minCurvatureDir direction of the minimum principal curvature. Can be {@code null}
 * @param maxCurvatureDir direction of the maximum principal curvature. Can be {@code null}
 */
public record Curvature(
        double minPrincipal, double maxPrincipal,
        double gaussian, double mean,
        Vector3d minCurvatureDir, Vector3d maxCurvatureDir) implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * Copy constructor.
     * 
     * @param curvature curvature
     */
    public Curvature(Curvature curvature) {
        this(curvature.minPrincipal, curvature.maxPrincipal,
                curvature.gaussian, curvature.mean,
                curvature.minCurvatureDir, curvature.maxCurvatureDir);
    }

    /**
     * Constructor that computes Gaussian and mean curvature automatically from principal curvatures.
     *
     * @param minPrincipal Minimum principal curvature
     * @param maxPrincipal Maximum principal curvature
     * @param minCurvatureDir direction of the minimum principal curvature. Can be {@code null}
     * @param maxCurvatureDir direction of the maximum principal curvature. Can be {@code null}
     */
    public Curvature(double minPrincipal, double maxPrincipal, Vector3d minCurvatureDir, Vector3d maxCurvatureDir) {
        this(minPrincipal, maxPrincipal,
                minPrincipal * maxPrincipal, 0.5 * (minPrincipal + maxPrincipal),
                minCurvatureDir, maxCurvatureDir);
    }

}
