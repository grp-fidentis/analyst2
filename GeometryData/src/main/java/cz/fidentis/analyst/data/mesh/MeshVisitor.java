package cz.fidentis.analyst.data.mesh;

/**
 * Required interface for stateful inspection of meshes.
 * It follows the principles of the Visitor GoF patterns.
 * Once instantiated, the visitor can be gradually applied to multiple mesh facets.
 * It inspects the state of the mesh facets one by one, and (cumulatively) computes results.
 * <p>
 * Implement this interface whenever you want to define new algorithm over a mesh.
 * </p>
 * <p>
 * If the visitor is thread-safe, then a single instance of the visitor
 * can visit concurrently (and asynchronously) multiple mesh facets. Otherwise,
 * the parallel inspection is still possible, but a new instance of the visitor
 * has to be used for each mesh facet.
 * </p>
 *
 * @author Radek Oslejsek
 */
@FunctionalInterface
public interface MeshVisitor {

    /**
     * Returns {@code true} if the implementation is thread-safe and then
     * <b>a single visitor instance</b> can be applied to multiple mesh facets simultaneously.
     * <p>
     * Thread-safe implementation means that any read or write from/to the visitor's
     * state is protected by {@code synchronized}.
     * </p>
     *
     * @return {@code true} if the implementation is thread-safe.
     */
    default boolean isThreadSafe() {
        return true;
    }

    /**
     * Dispose all disposable resources this visitor was using.
     * Distances can still be retrieved after calling this method.
     * Subclasses should override this method if they need to dispose something.
     */
    default void dispose() {
    }

    /**
     * The inspection method to be implemented by specific visitors.
     *
     * @param facet Mesh facet to be visited.
     */
    void visitMeshFacet(MeshFacet facet);
}
