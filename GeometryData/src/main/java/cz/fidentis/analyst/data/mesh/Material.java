package cz.fidentis.analyst.data.mesh;

import javax.vecmath.Vector3d;
import java.io.Serial;
import java.io.Serializable;

/**
 * Material.
 * 
 * @author Matej Lukes
 * @author Katerina Zarska
 * @param name Name of the material
 * @param ambient Ambient color
 * @param diffuse Diffuse color
 * @param specular Specular color
 * @param shininess Shininess
 * @param alpha Alpha channel
 * @param illumination Illumination
 * @param texturePath Path to the texture file
 */
public record Material(
        String name,
        Vector3d ambient,
        Vector3d diffuse,
        Vector3d specular,
        double shininess,
        double alpha,
        double illumination,
        String texturePath
) implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     *
     * @return string representation of Material
     */
    @Override
    public String toString() {
        StringBuilder material = new StringBuilder();
        material.append("shininess ").append(shininess).append(System.lineSeparator());
        material.append("alpha ").append(alpha).append(System.lineSeparator());
        material.append("illumination ").append(illumination).append(System.lineSeparator());
        if (ambient != null) {
            material.append("ambient ").append(ambient.x).append(" ")
                    .append(ambient.y).append(" ")
                    .append(ambient.z).append(System.lineSeparator());
        }
        if (diffuse != null) {
            material.append("diffuse ").append(diffuse.x).append(" ")
                    .append(diffuse.y).append(" ")
                    .append(diffuse.z).append(System.lineSeparator());
        }
        if (specular != null) {
            material.append("specular Colors ").append(specular.x)
                    .append(" ").append(specular.y).append(" ")
                    .append(specular.z).append(System.lineSeparator());
        }

        return material.toString();
    }

    /**
     * @param name compared string
     * @return true if material name is same as provided string.
     */
    public boolean hasName(String name) {
        return this.name.equals(name);
    }

    /**
     * @return true if texture file exists
     */
    public boolean hasTexture() {
        return texturePath != null;
    }

}
