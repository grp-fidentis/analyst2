package cz.fidentis.analyst.data.shapes;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.Serializable;

/**
 * Information about a point selected to be a glyph. Needed for rendering shadow-casting glyphs.
 *
 * @param location        position
 * @param normal          normal vector
 * @param maxCurvatureDir the maximal principal curvature at the sample point
 * @param minCurvatureDir the minimal principal curvature at the sample point
 *
 * @author Ondrej Simecek
 */
public record Glyph(Point3d location, Vector3d normal, Vector3d maxCurvatureDir, Vector3d minCurvatureDir) implements Serializable {

    /**
     * Create a glyph with a default orientation if the min/max curvature is not known.
     *
     * @param location position
     * @param normal   normal vector
     */
    public Glyph(Point3d location, Vector3d normal) {
        this(location, normal, new Vector3d(), new Vector3d());

        Vector3d temp = new Vector3d(1, 0, 0);
        if (normal.y == 0 && normal.z == 0) {
            temp = new Vector3d(0, 1, 0);
        }
        Vector3d base3 = new Vector3d(normal);
        base3.normalize();
        Vector3d base1 = new Vector3d();
        Vector3d base2 = new Vector3d();
        base1.cross(base3, temp);
        base2.cross(base3, base1);

        maxCurvatureDir.set(base1);
        minCurvatureDir.set(base2);
    }
}
