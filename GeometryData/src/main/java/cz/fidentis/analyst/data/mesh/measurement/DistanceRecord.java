package cz.fidentis.analyst.data.mesh.measurement;

import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.util.Objects;

/**
 * This objects stores data about the distance from a single facet's vertex to another surface.
 *
 * @author Radek Oslejsek
 */
public class DistanceRecord {

    /**
     * Facet's vertex from which the measurement was performed.
     */
    private final MeshPoint facetPoint;

    /**
     * Measured distance
     */
    private final double distance;

    /**
     * The closest point from the surface towards which the measurement was performed
     */
    private final MeshPoint nearestNeighbor;

    /**
     * Weight
     */
    private double weight = 1.0;

    /**
     * Constructor.
     * @param facetPoint facet's source point, must not be {@code null}
     * @param distance measured distance, can be infinite ot NaN
     * @param nearestNeighbor the closest point from another surface (can be {@code null})
     */
    public DistanceRecord(MeshPoint facetPoint, double distance, MeshPoint nearestNeighbor) {
        this.facetPoint = Objects.requireNonNull(facetPoint);
        this.distance = distance;
        this.nearestNeighbor = nearestNeighbor;
    }

    public MeshPoint getFacetPoint() {
        return facetPoint;
    }

    public double getDistance() {
        return distance;
    }

    public MeshPoint getNearestNeighbor() {
        return nearestNeighbor;
    }

    public double getWeight() {
        return weight;
    }

    /**
     * Sets new weight
     * @param weight new weight value, must be bigger than or equal to zero
     */
    public void setWeight(double weight) {
        if (weight < 0.0) {
            throw new IllegalArgumentException("weight must by >= 0");
        }
        this.weight = weight;
    }
}
