package cz.fidentis.analyst.data.shapes;

import java.io.Serial;
import java.io.Serializable;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A bounding cylinder.
 * 
 * @author Mario Chromik
 */
public class Cylinder implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Point3d pointA;
    private final Point3d pointB;
    private final Vector3d axis;
    private final double lengthSquared;
    private double radius;
    
    /**
     * Constructor.
     * 
     * @param p1 first point of axis
     * @param p2 second point of axis
     * @param r radius
     */
    public Cylinder(Point3d p1, Point3d p2, double r) {
        pointA = new Point3d(p1);
        pointB = new Point3d(p2);
        axis = new Vector3d(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
        Vector3d dummy = new Vector3d(axis);
        dummy.normalize();
        dummy.scale(0.1);
        pointA.add(dummy);
        pointB.sub(dummy);
        lengthSquared = axis.x*axis.x + axis.y*axis.y + axis.z*axis.z;
        radius = r;  
    } 
    
    /**
     * Copy constructor.
     * 
     * @param cylinder cylinder to copy  
     */
    public Cylinder(Cylinder cylinder) {
        pointA = cylinder.getPointA();
        pointB = cylinder.getPointB();
        axis = cylinder.getAxis();
        lengthSquared = cylinder.getLengthSquared();
        radius = cylinder.getRadius();
    }
    
    /**
     * @return axis
     */
    public Vector3d getAxis() {
        return axis;
    }
    
    /**
     * @return squared length of the axis
     */
    public double getLengthSquared() {
        return lengthSquared;
    }
    
    /**
     * @return the first point of the axis
     */
    public Point3d getPointA() {
        return pointA;
    }
    
    /**
     * @return the second point of the axis
     */
    public Point3d getPointB() {
        return pointB;
    }
    
    /**
     * @return radius
     */
    public double getRadius() {
        return radius;
    }
    
    /**
     * Scales the cylinder.
     * 
     * @param factor scale factor
     */
    public void scale(double factor) {
        radius *= factor;
    }
        
    
    /**
     * Determines if point is within cylinder.
     * Based on: https://www.flipcode.com/archives/Fast_Point-In-Cylinder_Test.shtml
     * 
     * @param testPoint a 3D point to be tested
     * @return {@code true} if point is within the cylinder, {@code false} otherwise.
     */
    public boolean containsPoint(Point3d testPoint) {
        double pdx, pdy, pdz;
        double dot, dsq;
        
        pdx = testPoint.x - pointA.x;
        pdy = testPoint.y - pointA.y;
        pdz = testPoint.z - pointA.z;
        
        dot = pdx * axis.x + pdy * axis.y + pdz * axis.z;
        
        if (dot < 0 || dot > lengthSquared) {
            return false;
        } 
        
        dsq = (pdx*pdx + pdy*pdy + pdz*pdz) - dot*dot/lengthSquared;
        
        return dsq <= radius*radius;
    }
}
