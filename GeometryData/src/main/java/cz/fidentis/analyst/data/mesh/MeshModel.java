package cz.fidentis.analyst.data.mesh;

import javax.vecmath.Point3d;
import java.io.Serializable;
import java.util.List;

/**
 * The main object for triangular meshes. Each mesh model consists of one or
 * more mesh facets.
 *
 * @author Matej Lukes
 * @author Radek Oslejsek
 * @author Katerina Zarska
 */
public interface MeshModel extends Serializable {

    /**
     * returns list of mesh facets
     *
     * @return list of mesh facets
     */
    List<MeshFacet> getFacets();

    /**
     * Adds a new mesh facet to the model.
     *
     * @param facet new mesh facet
     */
    void addFacet(MeshFacet facet);

    /**
     * gets the material of the model
     *
     * @return material
     */
    Material getMaterial();

    /**
     * @return {@code true} if material exists
     */
    boolean hasMaterial();

    /**
     * Returns {@code true} if the mesh vertices have set curvature values.
     * @return {@code true} if the mesh vertices have set curvature values.
     */
    boolean hasCurvature();

    /**
     * Applies the visitor to all mesh facets. If the visitor is thread-safe and
     * the {@code concurrently} is {@code true}, then the visitor is applied to
     * all mesh facet concurrently using all CPU cores.
     *
     * @param visitor Visitor to be applied for the computation
     * @param concurrently Parallel computation
     * @throws NullPointerException if the visitor is {@code null}
     */
    void compute(MeshVisitor visitor, boolean concurrently);

    /**
     * Applies the visitor to all mesh facets sequentially.
     *
     * @param visitor Visitor to be applied for the computation
     * @throws NullPointerException if the visitor is {@code null}
     */
    void compute(MeshVisitor visitor);

    /**
     * Removes duplicate vertices that differ only in normal vectors or texture
     * coordinates. Multiple normals are replaced with the average normal. If
     * the texture coordinate differ then randomly selected one is used.
     */
    void simplifyModel();

    /**
     * Returns number of vertices (sum of all facets).
     *
     * @return Number of vertices
     */
    long getNumVertices();

    /**
     * Returns central point computed from mesh vertices
     *
     * @return centroid
     */
    Point3d getCentroid();

    /**
     * Returns {@code true} if at least any vertex normals were automatically estimated.
     * Returns {@code false} if all vertex normals were predefined in the mesh file
     *
     * @return {@code true} if at least any vertex normals were automatically estimated.
     */
    boolean hasEstimatedVertNormals();

    /**
     * Recomputes normals of vertices.
     *
     * @param recomputeAll If {@code true}, then all normals are recomputed (estimated).
     *                     If {@code false}, then only missing vertex normals are recomputed.
     * @return {@code true} if some normals were recomputed
     */
    boolean estimateVertexNormals(boolean recomputeAll);

    /**
     * Recomputes vertex normals of only mesh faces for which the normals are missing.
     *
     * @return {@code true} if (some) normals were recomputed
     */
    default boolean estimateMissingVertexNormals() {
        return estimateVertexNormals(false);
    }

    /**
     * Returns {@code true}, if persistent data stored in the file(s) was modified,
     * e.g., the model was moved in the space.
     *
     * @return {@code true}, if persistent data stored in the file(s) was modified.
     */
    boolean isPersistentDataModified();
}
