package cz.fidentis.analyst.data.ray.impl;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.impl.MeshPointImpl;
import cz.fidentis.analyst.data.mesh.impl.MeshTriangleImpl;
import cz.fidentis.analyst.data.ray.RayIntersection;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Objects;

/**
 * The implementation of a ray intersection.
 *
 * @author Radek Oslejsek
 */
public class RayIntersectionImpl extends MeshPointImpl implements RayIntersection {

    private final transient MeshTriangleImpl hitTriangle;
    private double distance = Double.NaN;
    private final boolean directHit;

    /**
     * Constructor.
     *
     * @param position Intersection position, must not be {@code null}
     * @param normal   normal of MeshPoint
     * @param tri triangle with which the ray intersects. Must not be {@code null}
     * @param directHit determines whether the {@code position} is in the ray's direction
     * @throws IllegalArgumentException if some parameter is null
     */
    public RayIntersectionImpl(Point3d position, Vector3d normal, MeshTriangleImpl tri, boolean directHit) {
        super(position, normal, null);
        if (tri == null) {
            throw new IllegalArgumentException("facet is null");
        }
        if (position == null) {
            throw new IllegalArgumentException("position is null");
        }
        this.hitTriangle = tri;
        this.directHit = directHit;
    }

    @Override
    public double getDistance() {
        return distance;
    }

    @Override
    public MeshTriangle getHitTriangle() {
        return hitTriangle;
    }

    @Override
    public boolean isDirectHit() {
        return this.directHit;
    }

    @Override
    public void setDistance(Point3d rayOrigin) {
        this.distance = getPosition().distance(rayOrigin);
        if (!directHit) {
            this.distance *= -1.0;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.getPosition());
        hash = 17 * hash + Objects.hashCode(this.getNormal());
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.distance) ^ (Double.doubleToLongBits(this.distance) >>> 32));
        hash = 17 * hash + Objects.hashCode(this.hitTriangle);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RayIntersectionImpl other = (RayIntersectionImpl) obj;
        if (Double.doubleToLongBits(this.distance) != Double.doubleToLongBits(other.distance)) {
            return false;
        }
        if (!Objects.equals(getPosition(), other.getPosition())) {
            return false;
        }
        if (!Objects.equals(getNormal(), other.getNormal())) {
            return false;
        }
        return Objects.equals(this.hitTriangle, other.hitTriangle);
    }

    @Override
    public int compareTo(RayIntersection o) {
        if (this.equals(o)) {
            return 0;
        }

        int diff = Double.compare(Math.abs(this.distance), Math.abs(o.getDistance()));
        if  (diff != 0) {
            return diff;
        } else {
            return (this.distance > 0) ? -1 : 1;
        }
    }
}
