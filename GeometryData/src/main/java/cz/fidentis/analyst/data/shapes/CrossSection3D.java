package cz.fidentis.analyst.data.shapes;

import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A 3D poly-line consisting of multiple segments produced, e.g., by cutting planes.
 * 
 * @author Radek Oslejsek
 * @author Samuel Smoleniak
 */
public class CrossSection3D {
    
    private final List<List<Point3d>> segments = new ArrayList<>();

    /**
     * Add a new point to the beginning of the curve segment.
     * 
     * @param segment Index of the segment
     * @param point A 3D point to be inserted
     * @throws IllegalArgumentException if the segment does not exist or the point is {@code null}
     */
    public void addPointToSegmentStart(int segment, Point3d point) {
        if (segment < 0 || segment > segments.size() - 1) {
            throw new IllegalArgumentException("segment");
        }
        
        if (point == null) {
            throw new IllegalArgumentException("point");
        }
        
        segments.get(segment).add(0, point);
    }
    
    /**
     * Add a new point to the end of the curve segment.
     * 
     * @param segment Index of the segment
     * @param point A 3D point to be inserted
     * @throws IllegalArgumentException if the segment does not exist or the point is {@code null}
     */
    public void addPointToSegmentEnd(int segment, Point3d point) {
        if (segment < 0 || segment > segments.size() - 1) {
            throw new IllegalArgumentException("segment");
        }
        
        if (point == null) {
            throw new IllegalArgumentException("point");
        }
        
        segments.get(segment).add(point);
    }
    
    /**
     * Adds a new segment to the curve.
     * 
     * @return Index of the new segment
     */
    public int addNewSegment() {
        segments.add(new ArrayList<>());
        return segments.size() - 1;
    }
    
    /**
     * Returns number of points in given segment.
     * 
     * @param segment Index of the segment
     * @return number of points in given segment.
     * @throws IllegalArgumentException if the segment does not exist
     */
    public int getSegmentSize(int segment) {
        if (segment < 0 || segment > segments.size() - 1) {
            throw new IllegalArgumentException("segment");
        }
        return segments.get(segment).size();
    }
    
    /**
     * Returns given segment.
     * 
     * @param segment Index of the segment
     * @return Points of the segment
     * @throws IllegalArgumentException if the segment does not exist
     */
    public List<Point3d> getSegment(int segment) {
        if (segment < 0 || segment > segments.size() - 1) {
            throw new IllegalArgumentException("segment");
        }
        return Collections.unmodifiableList(segments.get(segment));
    }
    
    /**
     * Returns all segments.
     * @return Curve segments
     */
    public List<List<Point3d>> getSegments() {
        return Collections.unmodifiableList(segments);
    }

    /**
     * Flattening, i.e., the projection into the X, Y, or Z 2D plane.
     *
     * @param dir Either (1,0,0), (0,1,0) or (0,0,1) projection direction.
     * @return the 3D segmented poly-line projected on the plane with given direction.
     */
    public List<List<Point2d>> getFlattened(Vector3d dir) {
        List<List<Point2d>> curveSegments = new ArrayList<>();
        for (List<Point3d> segment : getSegments()) {
            curveSegments.add(
                    segment.stream()
                            .map(a -> flattenPoint(a, dir))
                            .toList());
        }
        return curveSegments;
    }

    /**
     * Flattens a 3D point into 2D based on given plane normal by removing a coordinate.
     * @param point3d given point
     * @param planeNormal approximately orthogonal normal vector
     * @return 2D point
     */
    public static Point2d flattenPoint(Point3d point3d, Vector3d planeNormal) {
        if (Math.round(planeNormal.x) == 1) {
            return new Point2d(point3d.z, -point3d.y);
        } else if (Math.round(planeNormal.y) == 1) {
            return new Point2d(point3d.x, point3d.z);
        } else {
            return new Point2d(point3d.x, -point3d.y);
        }
    }
    
}
