package cz.fidentis.analyst.data.mesh.impl.cornertable;

import java.io.Serial;
import java.io.Serializable;

/**
 * Single row in corner table.
 *
 * @author Matej Lukes
 */
public class CornerTableRow implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * Index to the corner (corner table row) with a vertex on the opposite triangle
     */
    private int oppositeCornerRow;
    
    /**
     * Index to the shared array of vertices
     */
    private int vertexIndex;

    /**
     * Constructor of a row in corner
     *
     * @param vertexIndex       index of associated vertex in MashFacet
     * @param oppositeCornerRow index of the opposite corner, -1 if there is no opposite corner
     */
    public CornerTableRow(int vertexIndex, int oppositeCornerRow) {
        this.vertexIndex = vertexIndex;
        this.oppositeCornerRow = oppositeCornerRow;
    }

    /**
     * Copy constructor of a row in corner
     *
     * @param cornerTableRow copied row
     */
    public CornerTableRow(CornerTableRow cornerTableRow) {
        this.vertexIndex = cornerTableRow.getVertexIndex();
        this.oppositeCornerRow = cornerTableRow.getOppositeCornerIndex();
    }

    /**
     * returns vertex of corner
     *
     * @return vertex
     */
    public int getVertexIndex() {
        return vertexIndex;
    }

    /**
     * @return index of opposite corner
     */
    public int getOppositeCornerIndex() {
        return oppositeCornerRow;
    }

    /**
     * sets index of the opposite corner
     *
     * @param index index of the opposite corner in corner table
     */
    public void setOppositeCornerIndex(int index) {
        this.oppositeCornerRow = index;
    }
    
    /**
     * sets index of the vertex
     * 
     * @param index New index
     */
    public void setVertexIndex(int index) {
        this.vertexIndex = index;
    }
}
