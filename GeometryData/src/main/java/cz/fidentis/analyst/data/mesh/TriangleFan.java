package cz.fidentis.analyst.data.mesh;

import java.util.Iterator;
import java.util.List;

/**
 * 1-ring neighborhood of a given mesh point. The neighborhood consists of
 * a ring of vertices forming triangles around the given central point.
 *
 * @author Radek Oslejsek
 */
public interface TriangleFan extends Iterable<MeshTriangle> {

    /**
     * Returns {@code true} if the triangle fan is enclosed (i.e., is not boundary).
     *
     * @return {@code true} if the triangle fan is enclosed (i.e., is not boundary).
     */
    boolean isEnclosed();

    /**
     * Returns {@code true} if the triangle fan is boundary (i.e., is not enclosed).
     *
     * @return {@code true} if the triangle fan is enclosed (i.e., is not boundary).
     */
    boolean isBoundary();

    /**
     * Vertex indices of the 1-ring neighborhood. If the triangle fan is enclosed
     * then the first and last vertices are the same.
     *
     * @return Vertex indices of the 1-ring neighborhood.
     */
    List<Integer> getVertices();

    /**
     * Triangle indices of the 1-ring neighborhood. The first triangle is related to
     * the edge delimited by first and second vertex, etc. The number of triangles
     * corresponds to the number of vertices minus one.
     *
     * @return Triangle indices of the 1-ring neighborhood.
     */
    List<Integer> getTriangles();

    /**
     * Vertex 1 of the returned triangle always corresponds to the central vertex
     * around which the triangle fan is constructed.
     *
     * @return A mesh triangle
     */
    @Override
    Iterator<MeshTriangle> iterator();
}
