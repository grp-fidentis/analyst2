/**
 * {@code cz.fidentis.analyst.mesh.impl} package includes Mesh implementation classes that
 * are not visible to classes of other modules.
 */
package cz.fidentis.analyst.data.mesh.impl;
