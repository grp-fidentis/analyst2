package cz.fidentis.analyst.data.mesh.impl.facet;

import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.mesh.impl.MeshPointImpl;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A rectangular mesh facet consisted of two triangles.
 * 
 * @author Radek Oslejsek
 */
public class MeshRectangleFacetImpl extends MeshFacetImpl {
    
    /**
     * Constructor. The vertical and horizontal direction are estimated automatically.
     * 
     * @param center Central point of the rectangle
     * @param normal Normalized normal vector 
     * @param width Width
     * @param height Height
     * @throws IllegalArgumentException if {@code width} or {@code height} are &lt;= 0
     */
    public MeshRectangleFacetImpl(Point3d center, Vector3d normal, double width, double height) {
        if (width <= 0) {
            throw new IllegalArgumentException("width");
        }
        
        if (height <= 0) {
            throw new IllegalArgumentException("height");
        }
        
        Vector3d hDir = new Vector3d();
        if (Math.abs(normal.dot(new Vector3d(0.0, 1.0, 0.0))) > Math.abs(normal.dot(new Vector3d (1.0, 0.0, 0.0)))) {
            hDir.cross(normal, new Vector3d(1.0, 0.0, 0.0));
        } else {
            hDir.cross(normal, new Vector3d(0.0, 1.0, 0.0));
        }
        hDir.normalize();

        Vector3d vDir = new Vector3d();
        vDir.cross(normal, hDir);
        vDir.normalize();
        
        initRectangle(center, hDir, vDir, normal, width, height);
    }
    
    /**
     * Constructor.
     * 
     * @param center Central point of the rectangle
     * @param hDir Normalized horizontal direction of the rectangle
     * @param vDir Normalized vertical direction of the rectangle
     * @param w Width
     * @param h Height
     */
    public MeshRectangleFacetImpl(Point3d center, Vector3d hDir, Vector3d vDir, double w, double h) {
        this(center, hDir, vDir, null, w, h);
    }
    
    /**
     * Constructor.
     * 
     * @param center Central point of the rectangle
     * @param hDir Normalized horizontal direction of the rectangle
     * @param vDir Normalized vertical direction of the rectangle
     * @param normal Optional normalized normal direction perpendicular to {@code hDir} and {@code vDir}. Can be {@code null}.
     * @param w Width
     * @param h Height
     */
    public MeshRectangleFacetImpl(Point3d center, Vector3d hDir, Vector3d vDir, Vector3d normal, double w, double h) {
        initRectangle(center, hDir, vDir, null, w, h);
    }
    
    protected final void initRectangle(Point3d center, Vector3d hDir, Vector3d vDir, Vector3d normal, double w, double h) {
        Vector3d myNormal;
        
        if (normal == null) {
            myNormal = new Vector3d();
            myNormal.cross(hDir, vDir);
        } else {
            myNormal = new Vector3d(normal);
        }
        
        Point3d corner = new Point3d(center);
        Vector3d aScaled = new Vector3d(hDir);
        Vector3d bScaled = new Vector3d(vDir);
        aScaled.scale(w/2f);
        bScaled.scale(h/2f);
        corner.sub(aScaled);
        corner.sub(bScaled);
        
        Point3d p = new Point3d(corner);
        addVertex(new MeshPointImpl(p, myNormal, null));
        
        Vector3d dir = new Vector3d(vDir);
        dir.scale(h);
        p = new Point3d(corner);
        p.add(dir);
        addVertex(new MeshPointImpl(p, myNormal, null));
        
        dir = new Vector3d(hDir);
        dir.scale(w);
        p.add(dir);
        addVertex(new MeshPointImpl(p, myNormal, null));
        
        p = new Point3d(corner);
        p.add(dir);
        addVertex(new MeshPointImpl(p, myNormal, null));
        
        getCornerTable().addRow(new CornerTableRow(0,  5));
        getCornerTable().addRow(new CornerTableRow(1, -1));
        getCornerTable().addRow(new CornerTableRow(3, -1));
        getCornerTable().addRow(new CornerTableRow(3, -1));
        getCornerTable().addRow(new CornerTableRow(1, -1));
        getCornerTable().addRow(new CornerTableRow(2,  0));
    }
}
