package cz.fidentis.analyst.data.mesh;

import com.esotericsoftware.kryo.Kryo;
import cz.fidentis.analyst.data.mesh.impl.MeshModelImpl;
import cz.fidentis.analyst.data.mesh.impl.MeshPointImpl;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTable;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.mesh.impl.facet.MeshFacetImpl;
import cz.fidentis.analyst.data.mesh.impl.facet.PointCloudFacetImpl;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Collection;
import java.util.Objects;

/**
 * This interface provides services that instantiates objects related to meshes.
 *
 * @author Radek Oslejsek
 */
public interface MeshFactory {

    /**
     * Returns an empty mesh model
     * @return an empty mesh model
     */
    static MeshModel createEmptyMeshModel() {
        return new MeshModelImpl();
    }

    /**
     * Returns model's deep copy by cloning all model's facets.
     *
     * @param meshModel Original mesh model
     * @return A copy of the mesh model
     */
    static MeshModel cloneMeshModel(MeshModel meshModel) {
        return new MeshModelImpl(meshModel);
    }

    /**
     * Returns a facet consisting of mesh points only, without mesh topology (edges).
     *
     * @param meshPoints Points of the mesh (point cloud). Must not be {@code null}
     * @return a facet consisting of mesh points only, without mesh topology (edges)
     */
    static MeshFacet createPointCloudFacet(Collection<MeshPoint> meshPoints) {
        return new PointCloudFacetImpl(meshPoints);
    }

    /**
     * Returns an empty facet that is to be filled by vertices and topology later on.
     *
     * @return an empty facet
     */
    static MeshFacet createEmptyMeshFacet() {
        return new MeshFacetImpl();
    }

    /**
     * Returns facet's clone. Both vertices and topology connections (edges) are duplicated.
     *
     * @param facet Mesh facet to be duplicated. Must not be {@code null}
     * @return Facet's clone
     */
    static MeshFacet cloneMeshFacet(MeshFacet facet) {
        return new MeshFacetImpl(Objects.requireNonNull(facet));
    }

    /**
     * Returns new mesh point.
     *
     * @param position 3D location
     * @param normal Normal vector. Can be {@code null}
     * @param texCoord Texture coordinates. Can be {@code null}
     * @param curvature Curvature of the mesh point. Can be {@code null}
     */
    static MeshPoint createMeshPoint(Point3d position, Vector3d normal, Vector3d texCoord, Curvature curvature) {
        return new MeshPointImpl(position, normal, texCoord, curvature);
    }

    /**
     * Returns new mesh point. Normal vector and texture coordinates are set to {@code null}
     *
     * @param position 3D location
     */
    static MeshPoint createMeshPoint(Point3d position) {
        return new MeshPointImpl(position, null, null);
    }

    /**
     * Creates an average mesh point from given points.
     * <ul>
     *     <li>The normal vector is determined by averaging the normal vectors of all points.</li>
     *     <li>The minimal principal curvature is taken from the point with maximal value.</li>
     *     <li>The maximal principal curvature is taken from the point with minimal value.</li>
     *     <li>The Gaussian curvature are taken from the point for which the absolute value of its Gaussian curvature is the largest.</li>
     *     <li>The mean curvature is computed by averaging mean curvatures of points.</li>
     * </ul>
     *
     * @param meshPoints Mesh points. Must not be {@code null} or empty
     */
    static MeshPoint createAverageMeshPoint(Collection<MeshPoint> meshPoints) {
        return new MeshPointImpl(meshPoints);
    }

    /**
     * Registers relevant mesh-related classes in the Kryo object so that
     * their instances can be serialized quickly using the Kryo serialization library.
     * The Kryo serialization is used by auto-swapping algorithms of the FIDENTIS.
     *
     * @param kryo A Kryo serialization instance. Must not be {@code null}
     */
    static void registerClassesInKryo(Kryo kryo) {
        kryo.register(MeshModelImpl.class);
        kryo.register(MeshFacetImpl.class);
        kryo.register(MeshPointImpl.class);
        kryo.register(CornerTable.class);
        kryo.register(CornerTableRow.class);
        kryo.register(Material.class);
        kryo.register(Curvature.class);
    }
}
