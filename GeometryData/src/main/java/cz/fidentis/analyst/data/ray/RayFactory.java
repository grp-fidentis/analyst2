package cz.fidentis.analyst.data.ray;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.impl.MeshTriangleImpl;
import cz.fidentis.analyst.data.ray.impl.RayIntersectionImpl;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * This interface provides services that instantiates objects related to ray intersections.
 *
 * @author Marek Horský
 */
public interface RayFactory {

    /**
     * Returns new Ray Intersection.
     *
     * @param position 3D location
     * @param normal 3D vector
     * @param triangle the intersected {@link MeshTriangle}
     * @param directHit true, if the hit is direct, false otherwise
     */
    static RayIntersection createRayIntersection(Point3d position, Vector3d normal, MeshTriangle triangle, boolean directHit) {
        return new RayIntersectionImpl(position, normal, (MeshTriangleImpl) triangle, directHit);
    }
}
