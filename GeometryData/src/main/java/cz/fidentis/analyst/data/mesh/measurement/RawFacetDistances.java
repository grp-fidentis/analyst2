package cz.fidentis.analyst.data.mesh.measurement;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.util.*;

/**
 * The result of the facet-to-surface distance measurement, i.e.,
 * distances and nearest neighbors of all vertices of a single mesh facet.
 * Weights are set to 1.0.
 *
 * @author Radek Oslejsek
 */
public class RawFacetDistances implements FacetDistances {

    private final MeshFacet facet;

    private final List<Double> distances;

    private final List<MeshPoint> nearestPoints;

    /**
     * Constructor.
     * @param facet A mesh facet whose distance was measured
     * @param distances Measured distances at facet's points. The size and order of values have to correspond to
     *                  facet's vertices.
     * @param nearestPoints Closets points (nearest neighbors). The size and order of values have to correspond to
     *                      facet's vertices.
     */
    public RawFacetDistances(MeshFacet facet, List<Double> distances, List<MeshPoint> nearestPoints) {
        this.facet = Objects.requireNonNull(facet);
        this.distances = new ArrayList<>(Objects.requireNonNull(distances));
        this.nearestPoints = new ArrayList<>(Objects.requireNonNull(nearestPoints));
        if (distances.size() != facet.getNumberOfVertices() || nearestPoints.size() != facet.getNumberOfVertices()) {
            throw new IllegalArgumentException("Number of vertices mismatch");
        }
    }

    @Override
    public DistanceRecord get(int pointIndex) {
        return new DistanceRecord(facet.getVertex(pointIndex), distances.get(pointIndex), nearestPoints.get(pointIndex));
    }

    @Override
    public int size() {
        return distances.size();
    }

    @Override
    public MeshFacet getMeasuredFacet() {
        return facet;
    }
}
