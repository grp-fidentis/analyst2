package cz.fidentis.analyst.data.surfacemask.impl;

import cz.fidentis.analyst.data.surfacemask.SurfaceMaskLine;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing surface mask as a sequence of points in 2D for operations in GUI
 * @author Mario Chromik
 */
public class SurfaceMaskLineImpl extends SurfaceMask2DImpl implements SurfaceMaskLine {
    private MaskPointList maskPointsList = new MaskPointList();
    /**
     * Parameterless construtor
     */
    public SurfaceMaskLineImpl() {}

    /**
     * Copy constructor
     * @param maskLine mask to copy
     */
    public SurfaceMaskLineImpl(SurfaceMaskLineImpl maskLine) {
        if (maskLine.getMaskPoints() == null || maskLine.getMaskPoints().size() == 0) {
            return;
        }
        for (Point point : maskLine.getMaskPoints()) {
            this.getMaskPointsList().addPoint(new Point(point.x, point.y));
        }
        //close the mask if the copied mask is closed
        if (maskLine.getMaskPointsList().isClosed()) {
            this.getMaskPointsList().getLast().setNext(this.getMaskPointsList().getFirst());
        }
    }

    /**
     * Gets mask points
     * @return List of points
     */
    public List<Point> getMaskPoints() {
        List<Point> points = new ArrayList<>();
        MaskPoint point = getMaskPointsList().getFirst();
        if (point == null) {
            return null;
        }
        for (int i = 0; i < getMaskPointsList().getSize(); i++) {
            points.add(point.getPoint());
            point = point.getNext();
        }
        return points;
    }

    @Override
    public MaskPointList getMaskPointsList() {
        return maskPointsList;
    }


    /**
     * Check if point is in between two mask points by constructing a bound box
     * @param a first mask point
     * @param b second mask point
     * @param middle point to check
     * @return true if between else false
     */
    private boolean isBetween(Point a, Point b, Point middle) {
        int xMin = Math.min(a.x, b.x) - IN_BETWEEN_BBOX_OFFSET;
        int xMax = Math.max(a.x, b.x) + IN_BETWEEN_BBOX_OFFSET;
        int yMin = Math.min(a.y, b.y) - IN_BETWEEN_BBOX_OFFSET;
        int yMax = Math.max(a.y, b.y) + IN_BETWEEN_BBOX_OFFSET;

        return xMin <= middle.x && middle.x <= xMax  && yMin <= middle.y && middle.y <= yMax;
    }

    /**
     * Inserts a point in between two mask points. Insertion happens only when mask is closed
     * @param location location to insert the new point
     */
    public void insertPoint(Point location) {
        MaskPoint point = getMaskPointsList().getFirst();
        if (point == null) {
            return;
        }
        for (int i = 0; i < getMaskPointsList().getSize(); i++) {
            if (point.getNext() == null) {
                break;
            }
            if (isBetween(point.getPoint(), point.getNext().getPoint(), location)) {
                getMaskPointsList().insertBehind(point, new MaskPoint(location));
                break;
            }
            point = point.getNext();
        }
    }

    /**
     * deletes a selected point
     * @param location location of the point to be deleted
     */
    public void deletePoint(Point location) {
        MaskPoint point = getMaskPointsList().getFirst();
        if (point == null) {
            return;
        }

        if (getMaskPointsList().getSize() == 1 || point.isPointToBeSelected(location)) {
            getMaskPointsList().deletePoint(null, point);
            return;
        }

        for (int i = 0; i < getMaskPointsList().getSize(); i++) {
            if (point != null && point.getNext() != null && point.getNext().isPointToBeSelected(location))  {
                getMaskPointsList().deletePoint(point, point.getNext());
                return;
            }
            point = point.getNext();
        }
    }


    /**
     * Closes the mask by adding an edge from the last point to the first.
     */
    private void finishMask() {
        if (getSelectedPoint() == getMaskPointsList().getFirst().getPoint() && getMaskPointsList().isCloseable()) {
            getMaskPointsList().getLast().setNext(getMaskPointsList().getFirst());
        }
    }


    /**
     * Samples a line between drawn points and adds to list of points to project
     * @param start starting point
     * @param end end point
     * @return sampled points to project
     */
    private ArrayList<PointToProject> sampleLine(Point start, Point end) {
        int xDiff = (end.x - start.x) / getSamplingStrength();
        int yDiff = (end.y - start.y) / getSamplingStrength();
        Point sampledPoint = new Point(start.x, start.y);
        ArrayList<PointToProject> sampledPoints = new ArrayList<>();
        for (int i = 0; i < getSamplingStrength(); i++) {
            sampledPoint = new Point(sampledPoint.x + xDiff, sampledPoint.y + yDiff);
            sampledPoints.add(new PointToProject(false, true, sampledPoint));
        }
        return sampledPoints;
    }

    /**
     * Gets a list of points from the mask and sampled points between any two mask points
     * @return points to project
     */
    @Override
    public ArrayList<PointToProject> getPointsToProject() {
        if (getMaskPointsList().getSize() == 0) {
            return null;
        }
        ArrayList<PointToProject> pointsToProject = new ArrayList<>();
        MaskPoint point = getMaskPointsList().getFirst();

        for (int i = 0; i < getMaskPointsList().getSize(); i++) {
            if (point.getNext() == null) {
                break;
            }
            pointsToProject.add(new PointToProject( point.getPoint() == getSelectedPoint(), false, point.getPoint()));
            pointsToProject.addAll(sampleLine(point.getPoint(), point.getNext().getPoint()));
            point = point.getNext();
        }
        return pointsToProject;
    }

    @Override
    public void shiftMask(int dx, int dy) {
        MaskPoint point = getMaskPointsList().getFirst();
        if (point == null) {
            return;
        }
        for (int i = 0; i < getMaskPointsList().getSize(); i++) {
            point.getPoint().setLocation(point.getPoint().x + dx, point.getPoint().y + dy);
            point = point.getNext();
        }
    }

    @Override
    public boolean selectPoint(Point location) {
        MaskPoint point = getMaskPointsList().getFirst();
        if (point == null) {
            return false;
        }
        for (int i = 0; i < getMaskPointsList().getSize(); i++) {
            if (point.isPointToBeSelected(location))  {
                setSelectedPoint(point.getPoint());
                return true;
            }
            point = point.getNext();
        }
        return false;
    }

    @Override
    public boolean addNewPoint(Point point) {
       selectPoint(point);
       if (getMaskPointsList().isClosed()) {
            if (getSelectedPoint() == null) {
                insertPoint(point);
            }
        } else if (getMaskPointsList().isCloseable() && getSelectedPoint() == getMaskPointsList().getFirst().getPoint()) {
            finishMask();
            setSelectedPoint(null);
        } else if (getSelectedPoint() == null){
           getMaskPointsList().addPoint(point);
        }

        return true;
    }

    @Override
    public boolean containsPoint(Point location) {
        MaskPoint point = getMaskPointsList().getFirst();

        if (point == null) {
            return false;
        }
        int xMin = point.getPoint().x, yMin = point.getPoint().y;
        int xMax = point.getPoint().x, yMax = point.getPoint().y;
        for (int i = 0; i < getMaskPointsList().getSize(); i++) {
            if (point.getPoint().x < xMin) {
                xMin = point.getPoint().x;
            }
            if (point.getPoint().y < yMin) {
                yMin = point.getPoint().y;
            }
            if (point.getPoint().x > xMax) {
                xMax = point.getPoint().x;
            }
            if (point.getPoint().y > yMax) {
                yMax = point.getPoint().y;
            }
            point = point.getNext();
        }
        if (xMin <= location.x && location.x <= xMax  && yMin <= location.y && location.y <= yMax) {
            setShiftPoint(location);
            return true;
        }
        return false;
    }

}
