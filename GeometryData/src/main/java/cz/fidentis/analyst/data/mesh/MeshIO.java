package cz.fidentis.analyst.data.mesh;

import cz.fidentis.analyst.data.mesh.impl.io.MeshObjExporter;
import cz.fidentis.analyst.data.mesh.impl.io.MeshObjLoader;

import java.io.File;
import java.io.IOException;

/**
 * Reading and writing mesh models from/to files.
 * Currently, only {@code .OBJ} files are supported.
 *
 * @author Marek Barinka
 * @author Katerina Zarska
 * @author Radek Oslejsek
 */
public interface MeshIO {

    /**
     * Reads mesh model from a file.
     *
     * @param file File containing face to load into a MeshModel
     * @return A mesh model
     * @throws IOException There was problem with reading the file
     */
    static MeshModel readMeshModel(File file) throws IOException {
        return MeshObjLoader.readFromObj(file);
    }

    /**
     * Exports all facets of the model to .obj file
     *
     * @param model Model to be exported. Must not be {@code null}
     * @param exportFile File to which model is exported
     * @throws IOException
     */
    static void exportMeshModel(MeshModel model, File exportFile) throws IOException {
        MeshObjExporter.exportModelToObj(model, exportFile);
    }
}
