package cz.fidentis.analyst.data.surfacemask.impl;

import cz.fidentis.analyst.data.surfacemask.SurfaceMaskEllipse;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a surface mask in the shape of an ellipse
 * @author Mario Chromik
 */
public class SurfaceMaskEllipseImpl extends SurfaceMaskRectangleImpl implements SurfaceMaskEllipse {
    private static final int DEFAULT_SAMPLING_ANGLE = 60;

    /**
     * Parameterless constructor
     */
    public SurfaceMaskEllipseImpl() {
        super();
    }

    /**
     * Copy constructor
     * @param mask mask to copy
     */
    public SurfaceMaskEllipseImpl(SurfaceMaskEllipseImpl mask) {
        super();
        if (mask.getOrigin() == null || mask.getResize() == null) {
            return;
        }
        setOrigin(new Point(mask.getOrigin().x, mask.getOrigin().y));
        setResize(new Point(mask.getResize().x, mask.getResize().y));
    }

    @Override
    public List<PointToProject> getPointsToProject(){
        if (getOrigin() == null) {
            return new ArrayList<>();
        }

        Point center = new Point(Math.min(getOrigin().x, getResize().x) + (getWidth() / 2),
                Math.min(getOrigin().y, getResize().y) + (getHeight() / 2));
        ArrayList<PointToProject> pointsToProject = new ArrayList<>();
        for (int angle = DEFAULT_SAMPLING_ANGLE / getSamplingStrength(); angle < 360; angle += DEFAULT_SAMPLING_ANGLE / getSamplingStrength() ) {
            pointsToProject.add(new PointToProject(false, true,
                    new Point(center.x + (int) (getWidth() / 2 * Math.cos(Math.toRadians(angle))),
                    center.y + (int) (getHeight() / 2 * Math.sin(Math.toRadians(angle))))));
        }
        return pointsToProject;
    }

}
