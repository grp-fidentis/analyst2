package cz.fidentis.analyst.data.mesh;

import javax.vecmath.Point3d;

/**
 * Interface for working with position in classes that contain the {@code Point3d}.
 *
 * @author Jakub Kolman
 */
public interface IPosition {

    /**
     * get position of the {@code Point3d} object
     *
     * @return position of the {@code Point3d} object
     */
    Point3d getPosition();

    default double getX() {
        return getPosition().x;
    }

    default double getY() {
        return getPosition().y;
    }

    default double getZ() {
        return getPosition().z;
    }
}