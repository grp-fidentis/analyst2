package cz.fidentis.analyst.data.mesh.measurement;

import java.io.Serial;
import java.io.Serializable;

/**
 * This decorator sets weight of all distance values to zero.
 *
 * @author Radek Oslejsek
 */
public class ZeroWeightsDecorator extends FacetDistancesDecorator implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     *
     * @param subElement decorated sub-element
     */
    public ZeroWeightsDecorator(FacetDistances subElement) {
        super(subElement);
    }

    /**
     * Constructor without sub-element! Use only for future clonning by the {@link #clone(FacetDistances)} method.
     */
    public ZeroWeightsDecorator() {}

    @Override
    public FacetDistancesDecorator clone(FacetDistances subElement) {
        return new ZeroWeightsDecorator(subElement);
    }

    @Override
    public DistanceRecord get(int pointIndex) {
        DistanceRecord rec = getSubElement().get(pointIndex);
        rec.setWeight(0.0);
        return rec;
    }
}
