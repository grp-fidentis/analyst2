package cz.fidentis.analyst.data.mesh.impl;

import cz.fidentis.analyst.data.mesh.Curvature;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.Serial;
import java.util.Collection;
import java.util.Objects;

/**
 * MeshPoint represents a point with position, normal, and texture coordinates.
 *
 * @author Matej Lukes
 */
public class MeshPointImpl implements MeshPoint {

    @Serial
    private static final long serialVersionUID = 1L;

    private Point3d position;
    private Vector3d normal;
    private Vector3d texCoord;
    private Curvature curvature;

    /**
     * Constructor. Curvature is not set.
     *
     * @param position position of MeshPoint
     * @param normal   normal of MeshPoint
     * @param texCoord coordinates in texture
     */
    public MeshPointImpl(Point3d position, Vector3d normal, Vector3d texCoord) {
        this(position, normal, texCoord, null);
    }
    
    /**
     * Complete constructor with curvature
     *
     * @param position position of MeshPoint
     * @param normal   normal of MeshPoint
     * @param texCoord coordinates in texture
     * @param curvature curvature of the mesh point
     */
    public MeshPointImpl(Point3d position, Vector3d normal, Vector3d texCoord, Curvature curvature) {
        if (position == null) {
            throw new IllegalArgumentException("position cannot be null");
        } else {
            this.position = new Point3d(position);
        }
        
        if (normal != null) {
            this.normal = new Vector3d(normal);
        }
        
        if (texCoord != null) {
            this.texCoord = new Vector3d(texCoord);
        }
        
        if (curvature != null) {
            this.curvature = new Curvature(curvature);
        }
    }

    /**
     * Copy constructor.
     * 
     * @param meshPoint copied meshPoint
     */
    public MeshPointImpl(MeshPoint meshPoint) {
        this(meshPoint.getPosition(), meshPoint.getNormal(), 
                meshPoint.getTexCoord(), meshPoint.getCurvature());
    }
    
    /**
     * Creates average point.
     * 
     * @param meshPoints Mesh points
     * @throws IllegalArgumentException if {@code meshPoints} is {@code null} or empty.
     */
    public MeshPointImpl(Collection<MeshPoint> meshPoints) {
        if (meshPoints == null || meshPoints.isEmpty()) {
            throw new IllegalArgumentException("meshPoints");
        } 
        
        position = new Point3d(0, 0, 0);
        normal = new Vector3d(0, 0, 0);
        texCoord = new Vector3d(0, 0, 0);
        
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        double mean = 0.0;
        double gauss = 0.0;
        
        final long size = meshPoints.size();
        for (MeshPoint mp: meshPoints) {
            position.x += mp.getPosition().x / size;
            position.y += mp.getPosition().y / size;
            position.z += mp.getPosition().z / size;
            
            if (mp.getNormal() != null) {
                normal.add(mp.getNormal());
            }
            
            if (mp.getTexCoord() != null) {
                texCoord.x += mp.getTexCoord().x / size;
                texCoord.y += mp.getTexCoord().y / size;
                texCoord.z += mp.getTexCoord().z / size;
            }
            
            Curvature c = mp.getCurvature();
            if (c != null) {
                min = Math.min(min, c.minPrincipal());
                max = Math.max(max, c.maxPrincipal());
                if (Math.abs(gauss) < Math.abs(c.gaussian())) {
                    gauss = c.gaussian();
                }
                mean += c.mean() / size;
            }
        }
        
        if (normal.equals(new Vector3d(0,0,0))) {
            normal = null;
        } else {
            normal.normalize();
        }
        
        if (texCoord.equals(new Vector3d(0,0,0))) {
            texCoord = null;
        } 
        
        if (min != Double.POSITIVE_INFINITY) {
            this.curvature = new Curvature(min, max, gauss, mean, null, null);
        }
    }
    
    @Override
    public Vector3d getNormal() {
        return normal;
    }

    @Override
    public Point3d getPosition() {
        return position;
    }
    
    @Override
    public Curvature getCurvature() {
        return curvature;
    }
    
    @Override
    public Vector3d getTexCoord() {
        return texCoord;
    }
    
    @Override
    public void setPosition(Point3d newPos) {
        if (newPos != null) {
            this.position = new Point3d(newPos);
        }
    }
    
    @Override
    public void setNormal(Vector3d newNormal) {
        this.normal = new Vector3d(newNormal);
    }
    
    @Override
    public void setCurvature(Curvature curvature) {
        this.curvature = curvature;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MeshPointImpl)) {
            return false;
        }

        MeshPointImpl other = (MeshPointImpl) obj;
        
        if (!this.position.equals(other.position)) {
            return false;
        }
        
        if (this.normal == null) {
            if (other.normal != null) {
                return false;
            } 
        } else {
            if (other.normal == null) {
                return false;
            } 
            if (!this.normal.equals(other.normal)) {
                return false;
            }
        }
        
        if (this.texCoord == null) {
            if (other.texCoord != null) {
                return false;
            } 
        } else {
            if (other.texCoord == null) {
                return false;
            } 
            if (!this.texCoord.equals(other.texCoord)) {
                return false;
            }
        }
        
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.position);
        hash = 67 * hash + Objects.hashCode(this.normal);
        hash = 67 * hash + Objects.hashCode(this.texCoord);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append("p=(").append(String.format("%.2f", position.x)).
                append("  ").append(String.format("%.2f", position.y)).
                append("  ").append(String.format("%.2f", position.z)).
                append(") ");
        if (normal != null) {
            ret.append("n=(").append(String.format("%.2f", normal.x)).
                    append("  ").append(String.format("%.2f", normal.y)).
                    append("  ").append(String.format("%.2f", normal.z)).
                    append(") ");
        }
        if (texCoord != null) {
            ret.append("t=(").append(String.format("%.2f", texCoord.x)).
                    append("  ").append(String.format("%.2f", texCoord.y)).
                    append("  ").append(String.format("%.2f", texCoord.z)).
                    append(") ");
        }
        return ret.toString();
    }
}
