package cz.fidentis.analyst.data.mesh;


import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.Serializable;

/**
 * MeshPoint represents a point with position, normal, and texture coordinates
 *
 * @author Matej Lukes
 */
public interface MeshPoint extends IPosition, Serializable {

    /**
     * Helper method that calculates distance of two 3D points.
     * 
     * @param v1 The first 3D point. Must not be null.
     * @param v2 The second 3D point. Must not be null.
     * @return distance
     * @throws NullPointerException if some input parameter is missing.
     */
    static double distance(Point3d v1, Point3d v2) {
        return Math.sqrt(Math.pow(v1.x - v2.x, 2)
                + Math.pow(v1.y - v2.y, 2)
                + Math.pow(v1.z - v2.z, 2));
    }
    
    /**
     * Returns normal vector at the point
     * @return normal vector at the point
     */
    Vector3d getNormal();

    @Override
    Point3d getPosition();
    
    /**
     * @return curvature
     */
    Curvature getCurvature();
    
    /**
     * @return texture coordinates
     */
    Vector3d getTexCoord();

    /**
     * 
     * @param newPos New position, must not be {@code null}
     */
    void setPosition(Point3d newPos);
    
    /**
     * Sets the normal vector
     * @param newNormal New normal vector or {@code null}
     */
    void setNormal(Vector3d newNormal);

    /**
     * Sets curvature of the mesh point.
     * @param curvature Curvature
     */
    void setCurvature(Curvature curvature);

}
