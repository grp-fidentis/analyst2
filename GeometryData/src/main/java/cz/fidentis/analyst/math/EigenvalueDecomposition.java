package cz.fidentis.analyst.math;

import javax.vecmath.Matrix4d;
import java.io.Serial;

/**
 * Eigenvalues and eigenvectors of a real matrix. 
 * Taken from https://math.nist.gov/javanumerics/jama/doc/Jama/EigenvalueDecomposition.html
 * and adapted for Matrix4d.
 * <p>
 * If A is symmetric, then A = V*D*V' where the eigenvalue matrix D is
 * diagonal and the eigenvector matrix V is orthogonal.
 * I.e. A = V.times(D.times(V.transpose())) and
 * V.times(V.transpose()) equals the identity matrix.
 * </p>
 * <p>
 * If A is not symmetric, then the eigenvalue matrix D is block diagonal
 * with the real eigenvalues in 1-by-1 blocks and any complex eigenvalues,
 * lambda + i*mu, in 2-by-2 blocks, [lambda, mu; -mu, lambda].  The
 * columns of V represent the eigenvectors in the sense that A*V = V*D,
 * i.e. A.times(V) equals V.times(D).  The matrix V may be badly
 * conditioned, or even singular, so the validity of the equation
 * A = V*D*inverse(V) depends upon V.cond().
 * </p>
 * 
 * @author Maria Kocurekova
 */
public class EigenvalueDecomposition implements java.io.Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

/* ------------------------
   Class variables
 * ------------------------ */

    /** Row and column dimension (square matrix).
     @serial matrix dimension.
     */
    private int n;

    /** Symmetry flag.
     @serial internal symmetry flag.
     */
    private boolean issymmetric;

    /** Arrays for internal storage of eigenvalues.
     @serial internal storage of eigenvalues.
     */
    private double[] d, e;

    /** Array for internal storage of eigenvectors.
     @serial internal storage of eigenvectors.
     */
    private double[][] arrayV;

    /** Array for internal storage of nonsymmetric Hessenberg form.
     @serial internal storage of nonsymmetric Hessenberg form.
     */
    private double[][] arrayH;

    /** Working storage for nonsymmetric algorithm.
     @serial working storage for nonsymmetric algorithm.
     */
    private double[] ort;

/* ------------------------
   Private Methods
 * ------------------------ */

    // Symmetric Householder reduction to tridiagonal form.

    private void tred2 () {

        //  This is derived from the Algol procedures tred2 by
        //  Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
        //  Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
        //  Fortran subroutine in EISPACK.

        for (int j = 0; j < n; j++) {
            d[j] = arrayV[n-1][j];
        }

        // Householder reduction to tridiagonal form.

        for (int i = n-1; i > 0; i--) {

            // Scale to avoid under/overflow.

            double scale = 0.0;
            double h = 0.0;
            for (int k = 0; k < i; k++) {
                scale = scale + Math.abs(d[k]);
            }
            if (scale == 0.0) {
                e[i] = d[i-1];
                for (int j = 0; j < i; j++) {
                    d[j] = arrayV[i-1][j];
                    arrayV[i][j] = 0.0;
                    arrayV[j][i] = 0.0;
                }
            } else {

                // Generate Householder vector.

                for (int k = 0; k < i; k++) {
                    d[k] /= scale;
                    h += d[k] * d[k];
                }
                double f = d[i-1];
                double g = Math.sqrt(h);
                if (f > 0) {
                    g = -g;
                }
                e[i] = scale * g;
                h = h - f * g;
                d[i-1] = f - g;
                for (int j = 0; j < i; j++) {
                    e[j] = 0.0;
                }

                // Apply similarity transformation to remaining columns.

                for (int j = 0; j < i; j++) {
                    f = d[j];
                    arrayV[j][i] = f;
                    g = e[j] + arrayV[j][j] * f;
                    for (int k = j+1; k <= i-1; k++) {
                        g += arrayV[k][j] * d[k];
                        e[k] += arrayV[k][j] * f;
                    }
                    e[j] = g;
                }
                f = 0.0;
                for (int j = 0; j < i; j++) {
                    e[j] /= h;
                    f += e[j] * d[j];
                }
                double hh = f / (h + h);
                for (int j = 0; j < i; j++) {
                    e[j] -= hh * d[j];
                }
                for (int j = 0; j < i; j++) {
                    f = d[j];
                    g = e[j];
                    for (int k = j; k <= i-1; k++) {
                        arrayV[k][j] -= (f * e[k] + g * d[k]);
                    }
                    d[j] = arrayV[i-1][j];
                    arrayV[i][j] = 0.0;
                }
            }
            d[i] = h;
        }

        // Accumulate transformations.

        for (int i = 0; i < n-1; i++) {
            arrayV[n-1][i] = arrayV[i][i];
            arrayV[i][i] = 1.0;
            double h = d[i+1];
            if (h != 0.0) {
                for (int k = 0; k <= i; k++) {
                    d[k] = arrayV[k][i+1] / h;
                }
                for (int j = 0; j <= i; j++) {
                    double g = 0.0;
                    for (int k = 0; k <= i; k++) {
                        g += arrayV[k][i+1] * arrayV[k][j];
                    }
                    for (int k = 0; k <= i; k++) {
                        arrayV[k][j] -= g * d[k];
                    }
                }
            }
            for (int k = 0; k <= i; k++) {
                arrayV[k][i+1] = 0.0;
            }
        }
        for (int j = 0; j < n; j++) {
            d[j] = arrayV[n-1][j];
            arrayV[n-1][j] = 0.0;
        }
        arrayV[n-1][n-1] = 1.0;
        e[0] = 0.0;
    }

    // Symmetric tridiagonal QL algorithm.

    private void tql2 () {

        //  This is derived from the Algol procedures tql2, by
        //  Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
        //  Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
        //  Fortran subroutine in EISPACK.

        for (int i = 1; i < n; i++) {
            e[i-1] = e[i];
        }
        e[n-1] = 0.0;

        double f = 0.0;
        double tst1 = 0.0;
        double eps = Math.pow(2.0,-52.0);
        for (int l = 0; l < n; l++) {

            // Find small subdiagonal element

            tst1 = Math.max(tst1,Math.abs(d[l]) + Math.abs(e[l]));
            int m = l;
            while (m < n) {
                if (Math.abs(e[m]) <= eps*tst1) {
                    break;
                }
                m++;
            }

            // If m == l, d[l] is an eigenvalue,
            // otherwise, iterate.

            if (m > l) {
                int iter = 0;
                do {
                    iter = iter + 1;  // (Could check iteration count here.)

                    // Compute implicit shift

                    double g = d[l];
                    double p = (d[l+1] - g) / (2.0 * e[l]);
                    double r = Math.hypot(p,1.0);
                    if (p < 0) {
                        r = -r;
                    }
                    d[l] = e[l] / (p + r);
                    d[l+1] = e[l] * (p + r);
                    double dl1 = d[l+1];
                    double h = g - d[l];
                    for (int i = l+2; i < n; i++) {
                        d[i] -= h;
                    }
                    f = f + h;


                    // Implicit QL transformation.

                    p = d[m];
                    double c = 1.0;
                    double c2 = c;
                    double c3 = c;
                    double el1 = e[l+1];
                    double s = 0.0;
                    double s2 = 0.0;
                    for (int i = m-1; i >= l; i--) {
                        c3 = c2;
                        c2 = c;
                        s2 = s;
                        g = c * e[i];
                        h = c * p;
                        r = Math.hypot(p,e[i]);
                        e[i+1] = s * r;
                        s = e[i] / r;
                        c = p / r;
                        p = c * d[i] - s * g;
                        d[i+1] = h + s * (c * g + s * d[i]);

                        // Accumulate transformation.

                        for (int k = 0; k < n; k++) {
                            h = arrayV[k][i+1];
                            arrayV[k][i+1] = s * arrayV[k][i] + c * h;
                            arrayV[k][i] = c * arrayV[k][i] - s * h;
                        }
                    }
                    p = -s * s2 * c3 * el1 * e[l] / dl1;
                    e[l] = s * p;
                    d[l] = c * p;

                    // Check for convergence.

                } while (Math.abs(e[l]) > eps*tst1);
            }
            d[l] = d[l] + f;
            e[l] = 0.0;
        }

        // Sort eigenvalues and corresponding vectors.

        for (int i = 0; i < n-1; i++) {
            int k = i;
            double p = d[i];
            for (int j = i+1; j < n; j++) {
                if (d[j] < p) {
                    k = j;
                    p = d[j];
                }
            }
            if (k != i) {
                d[k] = d[i];
                d[i] = p;
                for (int j = 0; j < n; j++) {
                    p = arrayV[j][i];
                    arrayV[j][i] = arrayV[j][k];
                    arrayV[j][k] = p;
                }
            }
        }
    }

    // Nonsymmetric reduction to Hessenberg form.

    private void orthes () {

        //  This is derived from the Algol procedures orthes and ortran,
        //  by Martin and Wilkinson, Handbook for Auto. Comp.,
        //  Vol.ii-Linear Algebra, and the corresponding
        //  Fortran subroutines in EISPACK.

        int low = 0;
        int high = n-1;

        for (int m = low+1; m <= high-1; m++) {

            // Scale column.

            double scale = 0.0;
            for (int i = m; i <= high; i++) {
                scale = scale + Math.abs(arrayH[i][m-1]);
            }
            if (scale != 0.0) {

                // Compute Householder transformation.

                double h = 0.0;
                for (int i = high; i >= m; i--) {
                    ort[i] = arrayH[i][m-1]/scale;
                    h += ort[i] * ort[i];
                }
                double g = Math.sqrt(h);
                if (ort[m] > 0) {
                    g = -g;
                }
                h = h - ort[m] * g;
                ort[m] = ort[m] - g;

                // Apply Householder similarity transformation
                // H = (I-u*u'/h)*H*(I-u*u')/h)

                for (int j = m; j < n; j++) {
                    double f = 0.0;
                    for (int i = high; i >= m; i--) {
                        f += ort[i]*arrayH[i][j];
                    }
                    f = f/h;
                    for (int i = m; i <= high; i++) {
                        arrayH[i][j] -= f*ort[i];
                    }
                }

                for (int i = 0; i <= high; i++) {
                    double f = 0.0;
                    for (int j = high; j >= m; j--) {
                        f += ort[j]*arrayH[i][j];
                    }
                    f = f/h;
                    for (int j = m; j <= high; j++) {
                        arrayH[i][j] -= f*ort[j];
                    }
                }
                ort[m] = scale*ort[m];
                arrayH[m][m-1] = scale*g;
            }
        }

        // Accumulate transformations (Algol's ortran).

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arrayV[i][j] = (i == j ? 1.0 : 0.0);
            }
        }

        for (int m = high-1; m >= low+1; m--) {
            if (arrayH[m][m-1] != 0.0) {
                for (int i = m+1; i <= high; i++) {
                    ort[i] = arrayH[i][m-1];
                }
                for (int j = m; j <= high; j++) {
                    double g = 0.0;
                    for (int i = m; i <= high; i++) {
                        g += ort[i] * arrayV[i][j];
                    }
                    // Double division avoids possible underflow
                    g = (g / ort[m]) / arrayH[m][m-1];
                    for (int i = m; i <= high; i++) {
                        arrayV[i][j] += g * ort[i];
                    }
                }
            }
        }
    }


    // Complex scalar division.

    private transient double cdivr, cdivi;
    private void cdiv(double xr, double xi, double yr, double yi) {
        double r,d;
        if (Math.abs(yr) > Math.abs(yi)) {
            r = yi/yr;
            d = yr + r*yi;
            cdivr = (xr + r*xi)/d;
            cdivi = (xi - r*xr)/d;
        } else {
            r = yr/yi;
            d = yi + r*yr;
            cdivr = (r*xr + xi)/d;
            cdivi = (r*xi - xr)/d;
        }
    }


    // Nonsymmetric reduction from Hessenberg to real Schur form.

    private void hqr2 () {

        //  This is derived from the Algol procedure hqr2,
        //  by Martin and Wilkinson, Handbook for Auto. Comp.,
        //  Vol.ii-Linear Algebra, and the corresponding
        //  Fortran subroutine in EISPACK.

        // Initialize
        int nn = this.n;
        int n = nn-1;
        int low = 0;
        int high = nn-1;
        double eps = Math.pow(2.0,-52.0);
        double exshift = 0.0;
        double p=0,q=0,r=0,s=0,z=0,t,w,x,y;

        // Store roots isolated by balanc and compute matrix norm
        double norm = storeRootsGetNorm(nn, low, high);
        
        // Outer loop over eigenvalue index
        int iter = 0;
        while (n >= low) {

            // Look for single small sub-diagonal element
            int l = n;
            while (l > low) {
                s = Math.abs(arrayH[l-1][l-1]) + Math.abs(arrayH[l][l]);
                if (s == 0.0) {
                    s = norm;
                }
                if (Math.abs(arrayH[l][l-1]) < eps * s) {
                    break;
                }
                l--;
            }

            // Check for convergence
            if (l == n) { // One root found
                arrayH[n][n] = arrayH[n][n] + exshift;
                d[n] = arrayH[n][n];
                e[n] = 0.0;
                n--;
                iter = 0;
            } else if (l == n-1) { // Two roots found
                w = arrayH[n][n-1] * arrayH[n-1][n];
                p = (arrayH[n-1][n-1] - arrayH[n][n]) / 2.0;
                q = p * p + w;
                z = Math.sqrt(Math.abs(q));
                arrayH[n][n] = arrayH[n][n] + exshift;
                arrayH[n-1][n-1] = arrayH[n-1][n-1] + exshift;
                x = arrayH[n][n];

                if (q >= 0) { // Real pair
                    z = (p >= 0) ? p + z : p - z;
                    d[n-1] = x + z;
                    d[n] = d[n-1];
                    if (z != 0.0) {
                        d[n] = x - w / z;
                    }
                    e[n-1] = 0.0;
                    e[n] = 0.0;
                    x = arrayH[n][n-1];
                    s = Math.abs(x) + Math.abs(z);
                    p = x / s;
                    q = z / s;
                    r = Math.sqrt(p * p+q * q);
                    p = p / r;
                    q = q / r;

                    for (int j = n-1; j < nn; j++) { // Row modification
                        z = arrayH[n-1][j];
                        arrayH[n-1][j] = q * z + p * arrayH[n][j];
                        arrayH[n][j] = q * arrayH[n][j] - p * z;
                    }

                    for (int i = 0; i <= n; i++) { // Column modification
                        z = arrayH[i][n-1];
                        arrayH[i][n-1] = q * z + p * arrayH[i][n];
                        arrayH[i][n] = q * arrayH[i][n] - p * z;
                    }

                    for (int i = low; i <= high; i++) { // Accumulate transformations
                        z = arrayV[i][n-1];
                        arrayV[i][n-1] = q * z + p * arrayV[i][n];
                        arrayV[i][n] = q * arrayV[i][n] - p * z;
                    }
                } else { // Complex pair
                    d[n-1] = x + p;
                    d[n] = x + p;
                    e[n-1] = z;
                    e[n] = -z;
                }
                n = n - 2;
                iter = 0;
            } else { // No convergence yet
                // Form shift
                x = arrayH[n][n];
                y = 0.0;
                w = 0.0;
                if (l < n) {
                    y = arrayH[n-1][n-1];
                    w = arrayH[n][n-1] * arrayH[n-1][n];
                }

                if (iter == 10) { // Wilkinson's original ad hoc shift
                    exshift += x;
                    for (int i = low; i <= n; i++) {
                        arrayH[i][i] -= x;
                    }
                    s = Math.abs(arrayH[n][n-1]) + Math.abs(arrayH[n-1][n-2]);
                    x = y = 0.75 * s;
                    w = -0.4375 * s * s;
                }

                if (iter == 30) { // MATLAB's new ad hoc shift
                    s = (y - x) / 2.0;
                    s = s * s + w;
                    if (s > 0) {
                        s = Math.sqrt(s);
                        if (y < x) {
                            s = -s;
                        }
                        s = x - w / ((y - x) / 2.0 + s);
                        for (int i = low; i <= n; i++) {
                            arrayH[i][i] -= s;
                        }
                        exshift += s;
                        x = y = w = 0.964;
                    }
                }

                iter = iter + 1;   // (Could check iteration count here.)

                // Look for two consecutive small sub-diagonal elements
                int m = n-2;
                while (m >= l) {
                    z = arrayH[m][m];
                    r = x - z;
                    s = y - z;
                    p = (r * s - w) / arrayH[m+1][m] + arrayH[m][m+1];
                    q = arrayH[m+1][m+1] - z - r - s;
                    r = arrayH[m+2][m+1];
                    s = Math.abs(p) + Math.abs(q) + Math.abs(r);
                    p = p / s;
                    q = q / s;
                    r = r / s;
                    if (m == l) {
                        break;
                    }
                    if (Math.abs(arrayH[m][m-1]) * (Math.abs(q) + Math.abs(r)) <
                            eps * (Math.abs(p) * (Math.abs(arrayH[m-1][m-1]) + Math.abs(z) +
                                    Math.abs(arrayH[m+1][m+1])))) {
                        break;
                    }
                    m--;
                }

                updateH(m);

                // Double QR step involving rows l:n and columns m:n
                for (int k = m; k <= n-1; k++) {
                    boolean notlast = (k != n-1);
                    if (k != m) {
                        p = arrayH[k][k-1];
                        q = arrayH[k+1][k-1];
                        r = (notlast ? arrayH[k+2][k-1] : 0.0);
                        x = Math.abs(p) + Math.abs(q) + Math.abs(r);
                        if (x == 0.0) {
                            continue;
                        }
                        p = p / x;
                        q = q / x;
                        r = r / x;
                    }

                    s = Math.sqrt(p * p + q * q + r * r);
                    if (p < 0) {
                        s = -s;
                    }
                    if (s != 0) {
                        if (k != m) {
                            arrayH[k][k-1] = -s * x;
                        } else if (l != m) {
                            arrayH[k][k-1] = -arrayH[k][k-1];
                        }
                        p = p + s;
                        x = p / s;
                        y = q / s;
                        z = r / s;
                        q = q / p;
                        r = r / p;
                        
                        // Row modification
                        for (int j = k; j < nn; j++) {
                            p = arrayH[k][j] + q * arrayH[k+1][j];
                            if (notlast) {
                                p = p + r * arrayH[k+2][j];
                                arrayH[k+2][j] = arrayH[k+2][j] - p * z;
                            }
                            arrayH[k][j] = arrayH[k][j] - p * x;
                            arrayH[k+1][j] = arrayH[k+1][j] - p * y;
                        }

                        // Column modification
                        for (int i = 0; i <= Math.min(n,k+3); i++) {
                            p = x * arrayH[i][k] + y * arrayH[i][k+1];
                            if (notlast) {
                                p = p + z * arrayH[i][k+2];
                                arrayH[i][k+2] = arrayH[i][k+2] - p * r;
                            }
                            arrayH[i][k] = arrayH[i][k] - p;
                            arrayH[i][k+1] = arrayH[i][k+1] - p * q;
                        }

                        // Accumulate transformations
                        for (int i = low; i <= high; i++) {
                            p = x * arrayV[i][k] + y * arrayV[i][k+1];
                            if (notlast) {
                                p = p + z * arrayV[i][k+2];
                                arrayV[i][k+2] = arrayV[i][k+2] - p * r;
                            }
                            arrayV[i][k] = arrayV[i][k] - p;
                            arrayV[i][k+1] = arrayV[i][k+1] - p * q;
                        }
                    }  // (s != 0)
                }  // k loop
            }  // check convergence
        }  // while (n >= low)

        if (norm != 0.0) {
            backSubstitute(nn, eps, norm); // Backsubstitute to find vectors of upper triangular form
            backTransform(nn, low, high); // Back transformation to get eigenvectors of original matrix
        }    
    }
    
    private void updateH(int m) {
        for (int i = m+2; i <= n; i++) {
            arrayH[i][i-2] = 0.0;
            if (i > m+2) {
                arrayH[i][i-3] = 0.0;
            }
        }
    }
    
    private double storeRootsGetNorm(int nn, int low, int high) {
        double norm = 0.0;
        for (int i = 0; i < nn; i++) {
            if (i < low | i > high) {
                d[i] = arrayH[i][i];
                e[i] = 0.0;
            }
            for (int j = Math.max(i-1,0); j < nn; j++) {
                norm = norm + Math.abs(arrayH[i][j]);
            }
        }
        return norm;
    }
    
    private void backSubstitute(int nn, double eps, double norm) {
        double p=0,q=0,r=0,s=0,z=0,t,w,x,y;
        
        for (n = nn-1; n >= 0; n--) {
            p = d[n];
            q = e[n];

            // Real vector

            if (q == 0) {
                int l = n;
                arrayH[n][n] = 1.0;
                for (int i = n-1; i >= 0; i--) {
                    w = arrayH[i][i] - p;
                    r = 0.0;
                    for (int j = l; j <= n; j++) {
                        r = r + arrayH[i][j] * arrayH[j][n];
                    }
                    if (e[i] < 0.0) {
                        z = w;
                        s = r;
                    } else {
                        l = i;
                        if (e[i] == 0.0) {
                            if (w != 0.0) {
                                arrayH[i][n] = -r / w;
                            } else {
                                arrayH[i][n] = -r / (eps * norm);
                            }

                            // Solve real equations

                        } else {
                            x = arrayH[i][i+1];
                            y = arrayH[i+1][i];
                            q = (d[i] - p) * (d[i] - p) + e[i] * e[i];
                            t = (x * s - z * r) / q;
                            arrayH[i][n] = t;
                            if (Math.abs(x) > Math.abs(z)) {
                                arrayH[i+1][n] = (-r - w * t) / x;
                            } else {
                                arrayH[i+1][n] = (-s - y * t) / z;
                            }
                        }

                        // Overflow control

                        t = Math.abs(arrayH[i][n]);
                        if ((eps * t) * t > 1) {
                            for (int j = i; j <= n; j++) {
                                arrayH[j][n] = arrayH[j][n] / t;
                            }
                        }
                    }
                }

                // Complex vector

            } else if (q < 0) {
                int l = n-1;

                // Last vector component imaginary so matrix is triangular

                if (Math.abs(arrayH[n][n-1]) > Math.abs(arrayH[n-1][n])) {
                    arrayH[n-1][n-1] = q / arrayH[n][n-1];
                    arrayH[n-1][n] = -(arrayH[n][n] - p) / arrayH[n][n-1];
                } else {
                    cdiv(0.0,-arrayH[n-1][n],arrayH[n-1][n-1]-p,q);
                    arrayH[n-1][n-1] = cdivr;
                    arrayH[n-1][n] = cdivi;
                }
                arrayH[n][n-1] = 0.0;
                arrayH[n][n] = 1.0;
                for (int i = n-2; i >= 0; i--) {
                    double ra,sa,vr,vi;
                    ra = 0.0;
                    sa = 0.0;
                    for (int j = l; j <= n; j++) {
                        ra = ra + arrayH[i][j] * arrayH[j][n-1];
                        sa = sa + arrayH[i][j] * arrayH[j][n];
                    }
                    w = arrayH[i][i] - p;

                    if (e[i] < 0.0) {
                        z = w;
                        r = ra;
                        s = sa;
                    } else {
                        l = i;
                        if (e[i] == 0) {
                            cdiv(-ra,-sa,w,q);
                            arrayH[i][n-1] = cdivr;
                            arrayH[i][n] = cdivi;
                        } else {

                            // Solve complex equations

                            x = arrayH[i][i+1];
                            y = arrayH[i+1][i];
                            vr = (d[i] - p) * (d[i] - p) + e[i] * e[i] - q * q;
                            vi = (d[i] - p) * 2.0 * q;
                            if (vr == 0.0 & vi == 0.0) {
                                vr = eps * norm * (Math.abs(w) + Math.abs(q) +
                                        Math.abs(x) + Math.abs(y) + Math.abs(z));
                            }
                            cdiv(x*r-z*ra+q*sa,x*s-z*sa-q*ra,vr,vi);
                            arrayH[i][n-1] = cdivr;
                            arrayH[i][n] = cdivi;
                            if (Math.abs(x) > (Math.abs(z) + Math.abs(q))) {
                                arrayH[i+1][n-1] = (-ra - w * arrayH[i][n-1] + q * arrayH[i][n]) / x;
                                arrayH[i+1][n] = (-sa - w * arrayH[i][n] - q * arrayH[i][n-1]) / x;
                            } else {
                                cdiv(-r-y*arrayH[i][n-1],-s-y*arrayH[i][n],z,q);
                                arrayH[i+1][n-1] = cdivr;
                                arrayH[i+1][n] = cdivi;
                            }
                        }

                        // Overflow control

                        t = Math.max(Math.abs(arrayH[i][n-1]),Math.abs(arrayH[i][n]));
                        if ((eps * t) * t > 1) {
                            for (int j = i; j <= n; j++) {
                                arrayH[j][n-1] = arrayH[j][n-1] / t;
                                arrayH[j][n] = arrayH[j][n] / t;
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void backTransform(int nn, int low, int high) {
        for (int i = 0; i < nn; i++) { // Vectors of isolated roots
            if (i < low | i > high) {
                for (int j = i; j < nn; j++) {
                    arrayV[i][j] = arrayH[i][j];
                }
            }
        }

        for (int j = nn-1; j >= low; j--) { // Back transformation to get eigenvectors of original matrix
            for (int i = low; i <= high; i++) {
                double z = 0.0;
                for (int k = low; k <= Math.min(j,high); k++) {
                    z = z + arrayV[i][k] * arrayH[k][j];
                }
                arrayV[i][j] = z;
            }
        }
    }


/* ------------------------
   Constructor
 * ------------------------ */

    /** Check for symmetry, then construct the eigenvalue decomposition
     Structure to access D and V.
     @param arg    Square matrix
     */

    public EigenvalueDecomposition(Matrix4d arg) {
        n = 4;
        double[][] arrayA = new double[n][n];
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                arrayA[i][j] = arg.getElement(i,j);
            }
        }


        arrayV = new double[n][n];
        d = new double[n];
        e = new double[n];

        issymmetric = true;
        for (int j = 0; (j < n) & issymmetric; j++) {
            for (int i = 0; (i < n) & issymmetric; i++) {
                issymmetric = (arrayA[i][j] == arrayA[j][i]);
            }
        }

        if (issymmetric) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    arrayV[i][j] = arrayA[i][j];
                }
            }

            // Tridiagonalize.
            tred2();

            // Diagonalize.
            tql2();

        } else {
            arrayH = new double[n][n];
            ort = new double[n];

            for (int j = 0; j < n; j++) {
                for (int i = 0; i < n; i++) {
                    arrayH[i][j] = arrayA[i][j];
                }
            }

            // Reduce to Hessenberg form.
            orthes();

            // Reduce Hessenberg to real Schur form.
            hqr2();
        }
    }

/* ------------------------
   Public Methods
 * ------------------------ */

    /** Return the eigenvector matrix
     @return     V
     */

    public Matrix4d getV () {
        Matrix4d res =  new Matrix4d(arrayV[0][0],arrayV[0][1],arrayV[0][2],arrayV[0][3],
                arrayV[1][0],arrayV[1][1],arrayV[1][2],arrayV[1][3],
                arrayV[2][0],arrayV[2][1],arrayV[2][2],arrayV[2][3],
                arrayV[3][0],arrayV[3][1],arrayV[3][2],arrayV[3][3]);

        return res;
    }



    /** Return the block diagonal eigenvalue matrix
     @return     D
     */

    public Matrix4d getD () {
        double[][] arrayD = new double[n][n];
     /*   Matrix4d X = new Matrix4d();
        double[][] D=new double[n][n];
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                D[i][j] = X.getElement(i, j);
            }
        }*/

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arrayD[i][j] = 0.0;
            }
            arrayD[i][i] = d[i];
            if (e[i] > 0) {
                arrayD[i][i+1] = e[i];
            } else if (e[i] < 0) {
                arrayD[i][i-1] = e[i];
            }
        }
        return new Matrix4d(arrayD[0][0],arrayD[0][1],arrayD[0][2],arrayD[0][3],
                arrayD[1][0],arrayD[1][1],arrayD[1][2],arrayD[1][3],
                arrayD[2][0],arrayD[2][1],arrayD[2][2],arrayD[2][3],
                arrayD[3][0],arrayD[3][1],arrayD[3][2],arrayD[3][3]);
    }
}