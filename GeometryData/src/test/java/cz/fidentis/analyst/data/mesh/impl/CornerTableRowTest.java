package cz.fidentis.analyst.data.mesh.impl;

//import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.*;

import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import org.junit.jupiter.api.Test;


//import static org.testng.AssertJUnit.assertEquals;

/**
 * Unit test for cornerTableRow
 */
public class CornerTableRowTest {

    /**
     * unit test for getVertex
     */
    @Test
    void getVertexIndex() {
        CornerTableRow row = new CornerTableRow(42, -1);
        assertEquals(42, row.getVertexIndex());
    }

    /**
     * Unit test for getOppositeCornerIndex
     */
    @Test
    void getOppositeCornerIndex() {
        CornerTableRow row = new CornerTableRow(0, 42);
        assertEquals(42, row.getOppositeCornerIndex());
    }

    /**
     * Unit test for setOppositeCornerIndex
     */
    @Test
    void setOppositeCornerIndex() {
        CornerTableRow row = new CornerTableRow(0, 42);
        assertEquals(42, row.getOppositeCornerIndex());
        row.setOppositeCornerIndex(21);
        assertEquals(21, row.getOppositeCornerIndex());
    }
}
