package cz.fidentis.analyst.data.mesh.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.mesh.impl.facet.MeshFacetImpl;
import cz.fidentis.analyst.data.ray.Ray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
/**
 *
 * @author oslejsek
 * @author Dominik Racek
 */
class MeshTriangleImplTest {
    final double EPS = 1e-7;

    @Test
    void getClosestPointTest() {
        MeshFacet facet = createFacet();
        MeshTriangleImpl tri = new MeshTriangleImpl(facet, 0, 1, 2);

        Assertions.assertEquals(new Point3d(0.25, 0.25, 0), tri.getClosestPoint(new Point3d(0, 0.5, -1)));
        Assertions.assertEquals(new Point3d(0.25, 0.25, 0), tri.getClosestPoint(new Point3d(0, 0.5,  0)));
        Assertions.assertEquals(new Point3d(0.25, 0.25, 0), tri.getClosestPoint(new Point3d(0, 0.5,  1)));

        Assertions.assertEquals(new Point3d(1, 1, 0), tri.getClosestPoint(new Point3d(1, 1, -1)));
        Assertions.assertEquals(new Point3d(1, 1, 0), tri.getClosestPoint(new Point3d(1, 1,  0)));
        Assertions.assertEquals(new Point3d(1, 1, 0), tri.getClosestPoint(new Point3d(1, 1,  1)));

        Assertions.assertEquals(new Point3d(0.7, 0.7, 0), tri.getClosestPoint(new Point3d(0.7, 0.7, -1)));
        Assertions.assertEquals(new Point3d(0.7, 0.7, 0), tri.getClosestPoint(new Point3d(0.7, 0.7,  0)));
        Assertions.assertEquals(new Point3d(0.7, 0.7, 0), tri.getClosestPoint(new Point3d(0.7, 0.7,  1)));
    }

    private MeshFacet createFacet() {
        MeshFacet facet = new MeshFacetImpl();
        facet.addVertex(new MeshPointImpl(new Point3d(0, 0, 0), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(1, 0, 0), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(1, 1, 0), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(2, 1, 0), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(1, 2, 0), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(2, 0, 0), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(3, 0, 0), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(3, 1, 0), null, null));
        return facet;
    }

    /**
     * Unit test for triangle equals
     */
    @Test
    void trianglesEquals() {
        MeshFacet facet = createFacet();

        MeshTriangleImpl tri1 = new MeshTriangleImpl(facet, 0, 1, 2);
        MeshTriangleImpl tri2 = new MeshTriangleImpl(facet, 2, 0, 1);
        MeshTriangleImpl tri3 = new MeshTriangleImpl(facet, 1, 2, 0);
        MeshTriangleImpl tri4 = new MeshTriangleImpl(facet, 1, 2, 3);
        MeshTriangleImpl tri5 = new MeshTriangleImpl(facet, 2, 3, 4);

        Assertions.assertEquals(tri1, tri2);
        Assertions.assertEquals(tri1, tri3);
        Assertions.assertNotEquals(tri1, tri4);
        Assertions.assertNotEquals(tri1, tri5);
        Assertions.assertNotEquals(tri4, tri5);
    }

    /**
     * Unit test for getCommonPoints
     */
    @Test
    void getCommonPoints() {
        MeshFacet facet = createFacet();

        MeshTriangleImpl tri1 = new MeshTriangleImpl(facet, 0, 1, 2);
        MeshTriangleImpl tri2 = new MeshTriangleImpl(facet, 1, 2, 3);
        MeshTriangleImpl tri3 = new MeshTriangleImpl(facet, 2, 3, 4);
        MeshTriangleImpl tri4 = new MeshTriangleImpl(facet, 5, 6, 7);

        List<Point3d> common1 = tri1.getCommonPoints(tri2);
        List<Point3d> common2 = tri2.getCommonPoints(tri3);
        List<Point3d> common3 = tri1.getCommonPoints(tri3);
        List<Point3d> common4 = tri1.getCommonPoints(tri4);

        Assertions.assertEquals(2, common1.size());
        Assertions.assertTrue(common1.contains(new Point3d(1, 0, 0)));
        Assertions.assertTrue(common1.contains(new Point3d(1, 1, 0)));

        Assertions.assertEquals(2, common2.size());
        Assertions.assertTrue(common2.contains(new Point3d(1, 1, 0)));
        Assertions.assertTrue(common2.contains(new Point3d(2, 1, 0)));


        Assertions.assertEquals(1, common3.size());
        Assertions.assertTrue(common3.contains(new Point3d(1, 1, 0)));

        Assertions.assertEquals(0, common4.size());
    }

    /**
     * Unit test for getCommonPoints
     */
    @Test
    void checkIntersectionWithPlane() {
        MeshFacet facet = createFacet();

        Vector3d normal = new Vector3d(1, 0, 0);
        double d1 = 1;
        double d2 = 0.5;

        MeshTriangleImpl tri1 = new MeshTriangleImpl(facet, 0, 1, 2);
        MeshTriangleImpl tri2 = new MeshTriangleImpl(facet, 1, 2, 3);
        MeshTriangleImpl tri3 = new MeshTriangleImpl(facet, 2, 3, 4);
        MeshTriangleImpl tri4 = new MeshTriangleImpl(facet, 5, 6, 7);

        Assertions.assertTrue(tri1.checkIntersectionWithPlane(normal, d1));
        Assertions.assertTrue(tri2.checkIntersectionWithPlane(normal, d1));
        Assertions.assertTrue(tri3.checkIntersectionWithPlane(normal, d1));
        Assertions.assertFalse(tri4.checkIntersectionWithPlane(normal, d1));

        Assertions.assertTrue(tri1.checkIntersectionWithPlane(normal, d2));
        Assertions.assertFalse(tri2.checkIntersectionWithPlane(normal, d2));
        Assertions.assertFalse(tri3.checkIntersectionWithPlane(normal, d2));
        Assertions.assertFalse(tri4.checkIntersectionWithPlane(normal, d2));

        Vector3d n2 = new Vector3d(-1, -1, 0);
        n2.normalize();
        Assertions.assertTrue(tri1.checkIntersectionWithPlane(n2, -0.5));
        Assertions.assertFalse(tri2.checkIntersectionWithPlane(n2, -0.5));
        Assertions.assertFalse(tri3.checkIntersectionWithPlane(n2, -0.5));
        Assertions.assertFalse(tri4.checkIntersectionWithPlane(n2, -0.5));
    }

    @Test
    void getRayIntersection2Test() {
        MeshFacet facet = getTrivialFacet();
        MeshTriangle tri = facet.getTriangles().get(0);
        
        var ray = new Ray(new Point3d(0,10,0), new Vector3d(0,-1,0));
        var result = tri.getRayIntersection(ray, MeshTriangleImpl.Smoothing.NONE);
        assertNotNull(result);
        assertEquals(new Point3d(0,0,0), result.getPosition());
        assertTrue(result.isDirectHit());
        
        ray = new Ray(new Point3d(-1,10,-1), new Vector3d(0,-1,0));
        result = tri.getRayIntersection(ray, MeshTriangleImpl.Smoothing.NONE);
        assertNotNull(result);
        assertEquals(new Point3d(-1,1,-1), result.getPosition());
        assertTrue(result.isDirectHit());
        
        ray = new Ray(new Point3d(0,0,0), new Vector3d(0,1,0));
        result = tri.getRayIntersection(ray, MeshTriangleImpl.Smoothing.NONE);
        assertNotNull(result);
        assertEquals(new Point3d(0,0,0), result.getPosition());
        assertTrue(result.isDirectHit());
        
        ray = new Ray(new Point3d(0,1,0), new Vector3d(0,1,0));
        result = tri.getRayIntersection(ray, MeshTriangleImpl.Smoothing.NONE);
        assertNotNull(result);
        assertEquals(new Point3d(0,0,0), result.getPosition());
        assertFalse(result.isDirectHit());
        
        var dir = new Vector3d(-1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(1,1,1), dir);
        result = tri.getRayIntersection(ray, MeshTriangleImpl.Smoothing.NONE);
        assertNotNull(result);
        assertEquals(new Point3d(0,0,0), result.getPosition());
        assertTrue(result.isDirectHit());
        
        dir = new Vector3d(1,1,1);
        dir.normalize();
        ray = new Ray(new Point3d(1,1,1), dir);
        result = tri.getRayIntersection(ray, MeshTriangleImpl.Smoothing.NONE);
        assertNotNull(result);
        assertEquals(new Point3d(0,0,0), result.getPosition());
        assertFalse(result.isDirectHit());
        
        dir = new Vector3d(-1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(1,1,10), dir);
        result = tri.getRayIntersection(ray, MeshTriangleImpl.Smoothing.NONE);
        assertNull(result);
    }
    
    @Test
    void getLinesIntersection2Test() {
        var dir = new Vector3d(1,1,1);
        dir.normalize();
        var ray = new Ray(new Point3d(0,0,0), dir);
        Point3d intersection = new Point3d();
        boolean opositeDir;

        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(-10, 1, 1), new Point3d(10, 1, 1), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(1,1,1), EPS));
        assertTrue(opositeDir);

        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(-10, -1, -1), new Point3d(10, -1, -1), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(-1,-1,-1), EPS));
        assertFalse(opositeDir);

        MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(2, 1, 0), new Point3d(10, 1, 0), intersection);
        assertTrue(intersection.equals(new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)));

        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(-10, 0, 0), new Point3d(10, 0, 0), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(0,0,0), EPS));
        assertTrue(opositeDir);
        
        // parallel directions
        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(10, 10, 10), new Point3d(20, 20, 20), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(10,10,10), EPS));
        assertTrue(opositeDir);

        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(-10, -10, -10), new Point3d(-20, -20, -20), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(-10,-10,-10), EPS));
        assertFalse(opositeDir);

        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(0, 0, 0), new Point3d(20, 20, 20), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(0,0,0), EPS));
        assertTrue(opositeDir);

        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(20, 20, 20), new Point3d(0, 0, 0), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(0,0,0), EPS));
        assertTrue(opositeDir);

        opositeDir = MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(-20, -20, -20), new Point3d(20, 20, 20), intersection);
        assertTrue(intersection.epsilonEquals(new Point3d(0,0,0), EPS));
        assertTrue(opositeDir);

            MeshTriangleImpl.getLinesIntersection(
                ray.origin(), ray.direction(), new Point3d(-20, -19, -20), new Point3d(20, 21, 20), intersection);
        assertTrue(intersection.equals(new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)));
    }
    
    protected MeshFacet getTrivialFacet() {
        MeshFacet facet = new MeshFacetImpl();
        facet.addVertex(new MeshPointImpl(new Point3d(-1,  1, -1), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d(-1,  1,  1), null, null));
        facet.addVertex(new MeshPointImpl(new Point3d( 1, -1,  0), null, null));

        facet.getCornerTable().addRow(new CornerTableRow(0, -1));
        facet.getCornerTable().addRow(new CornerTableRow(1, -1));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));

        return facet;
    }    
}
