package cz.fidentis.analyst.data.shapes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


/**
 *
 * @author Radek Oslejsek
 */
public class PlaneTest {
    
    @Test
    public void construction() {
        Vector3d norm = new Vector3d(-1, -1, -1);
        norm.normalize();
        
        Plane p = new Plane(norm, -3);
        Assertions.assertEquals(norm, p.getNormal());
        Assertions.assertEquals(-3, p.getDistance());
        
        p = new Plane(p);
        Assertions.assertEquals(norm, p.getNormal());
        Assertions.assertEquals(-3, p.getDistance());

        norm.scale(1.6);
        Plane p2 = new Plane(norm, -3*1.6);
        p2 = new Plane(p2, true);
        Assertions.assertTrue(p.similarity(p2) < 0.001);
    }
    
    @Test
    public void modification() {
        Vector3d norm = new Vector3d(-1, -1, -1);
        norm.normalize();
        Plane p = new Plane(norm, -3);
        
        Assertions.assertTrue(p.getPlanePoint()
                .epsilonEquals(new Vector3d(1.7320508075688776, 1.7320508075688776, 1.7320508075688776), 0.001));
        Assertions.assertTrue(p.shift(1)
                .getPlanePoint().epsilonEquals(new Vector3d(1.1547005383792517, 1.1547005383792517, 1.1547005383792517), 0.001));
        Assertions.assertTrue(p.shift(-1)
                .getPlanePoint().epsilonEquals(new Vector3d(2.3094010767585034, 2.3094010767585034, 2.3094010767585034), 0.001));
        
        Vector3d n2 = new Vector3d(norm);
        n2.scale(-1);
        Assertions.assertEquals(n2, p.flip().getNormal());
        Assertions.assertEquals(3, p.flip().getDistance());
    }
    
    @Test
    public void projectToPlane() {
        Plane p = new Plane(new Vector3d(-1, 0, 0), -3);
        Assertions.assertTrue(p.projectToPlane(new Point3d(9,9,9))
                .epsilonEquals(new Vector3d(3, 9, 9), 0.001));
        Assertions.assertTrue(p.projectToPlane(new Point3d(-9,-9,-9))
                .epsilonEquals(new Vector3d(3, -9, -9), 0.001));
        Assertions.assertTrue(p.projectToPlane(new Point3d(0,0,0))
                .epsilonEquals(new Vector3d(3, 0, 0), 0.001));
        
        p = new Plane(new Vector3d(-1, 0, 0), 3);
        Assertions.assertTrue(p.projectToPlane(new Point3d(9,9,9))
                .epsilonEquals(new Vector3d(-3, 9, 9), 0.001));
        Assertions.assertTrue(p.projectToPlane(new Point3d(-9,-9,-9))
                .epsilonEquals(new Vector3d(-3, -9, -9), 0.001));
        Assertions.assertTrue(p.projectToPlane(new Point3d(0,0,0))
                .epsilonEquals(new Vector3d(-3, 0, 0), 0.001));        
        
        Vector3d norm = new Vector3d(-1, -1, -1);
        norm.normalize();
        p = new Plane(norm, -3);
        Assertions.assertTrue(p.projectToPlane(new Point3d(9,9,9))
                .epsilonEquals(new Vector3d(1.7320508075688776, 1.7320508075688776, 1.7320508075688776), 0.001));
        Assertions.assertTrue(p.projectToPlane(new Point3d(-9,-9,-9))
                .epsilonEquals(new Vector3d(1.7320508075688776, 1.7320508075688776, 1.7320508075688776), 0.001));
        Assertions.assertTrue(p.projectToPlane(new Point3d(0,0,0))
                .epsilonEquals(new Vector3d(1.7320508075688776, 1.7320508075688776, 1.7320508075688776), 0.001));
    }
    
    @Test
    public void reflectOverPlane() {
        Plane p = new Plane(new Vector3d(-1, 0, 0), -3);
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(9,9,9))
                .epsilonEquals(new Vector3d(-3, 9, 9), 0.001));
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(-9,-9,-9))
                .epsilonEquals(new Vector3d(15, -9, -9), 0.001));
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(0,0,0))
                .epsilonEquals(new Vector3d(6, 0, 0), 0.001));
        
        p = new Plane(new Vector3d(-1, 0, 0), 3);
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(9,9,9))
                .epsilonEquals(new Vector3d(-15, 9, 9), 0.001));
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(-9,-9,-9))
                .epsilonEquals(new Vector3d(3, -9, -9), 0.001));
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(0,0,0))
                .epsilonEquals(new Vector3d(-6, 0, 0), 0.001));
        
        Vector3d norm = new Vector3d(-1, -1, -1);
        norm.normalize();
        p = new Plane(norm, -3);
        
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(9,9,9))
                .epsilonEquals(new Vector3d(-5.535898384862248, -5.535898384862248, -5.535898384862248), 0.001));
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(-9,-9,-9))
                .epsilonEquals(new Vector3d(12.464101615137768, 12.464101615137768, 12.464101615137768), 0.001));
        Assertions.assertTrue(p.reflectPointOverPlane(new Point3d(0,0,0))
                .epsilonEquals(new Vector3d(3.464101615137756, 3.464101615137756, 3.464101615137756), 0.001));
    }
    
    @Test
    public void reflectUnitVectorOverPlane() {
        Plane p = new Plane(new Vector3d(-1, 0, 0), -3);
        
        Vector3d n = new Vector3d(1, 0, 0);
        Vector3d nn = new Vector3d(-1, 0, 0);
        Assertions.assertEquals(nn, p.reflectUnitVectorOverPlane(n));
        
        n = new Vector3d(1, 1, 1);
        n.normalize();
        nn = new Vector3d(-1, 1, 1);
        nn.normalize();
        Assertions.assertTrue(p.reflectUnitVectorOverPlane(n).epsilonEquals(nn, 0.001));

        Vector3d nn2 = new Vector3d(-1, -1, -1);
        nn2.normalize();
        p = new Plane(nn2, 3);

        n = new Vector3d(-1, -1, -1);
        n.normalize();
        nn = new Vector3d(1, 1, 1);
        nn.normalize();
        Assertions.assertEquals(nn, p.reflectUnitVectorOverPlane(n));

        nn2 = new Vector3d(-1, 0, -1);
        nn2.normalize();
        p = new Plane(nn2, 3);

        n = new Vector3d(1, 0, 0);
        nn = new Vector3d(0, 0, -1);
        Assertions.assertEquals(nn, p.reflectUnitVectorOverPlane(n));
    }
    
    @Test
    public void getPointDistance() {
        Plane p = new Plane(new Vector3d(-1, 0, 0), -3);
        Assertions.assertEquals(-6, p.getPointDistance(new Point3d(9, 9, 9)));
        Assertions.assertEquals(12, p.getPointDistance(new Point3d(-9, -9, -9)));
        Assertions.assertEquals(3, p.getPointDistance(new Point3d(0, 0, 0)));
        
        p = new Plane(new Vector3d(-1, 0, 0), 3);
        Assertions.assertEquals(-12, p.getPointDistance(new Point3d(9, 9, 9)));
        Assertions.assertEquals(6, p.getPointDistance(new Point3d(-9, -9, -9)));
        Assertions.assertEquals(-3, p.getPointDistance(new Point3d(0, 0, 0)));
        
        p = new Plane(new Vector3d(1, 0, 0), 3);
        Assertions.assertEquals(6, p.getPointDistance(new Point3d(9, 9, 9)));
        Assertions.assertEquals(-12, p.getPointDistance(new Point3d(-9, -9, -9)));
        Assertions.assertEquals(-3, p.getPointDistance(new Point3d(0, 0, 0)));
        
        p = new Plane(new Vector3d(1, 0, 0), -3);
        Assertions.assertEquals(12, p.getPointDistance(new Point3d(9, 9, 9)));
        Assertions.assertEquals(-6, p.getPointDistance(new Point3d(-9, -9, -9)));
        Assertions.assertEquals(3, p.getPointDistance(new Point3d(0, 0, 0)));
    }
    
    @Test
    public void getRotationAroundAxis() {
        Plane p = new Plane(new Vector3d(1, 0, 0), 0);
        
        Vector3d n = new Vector3d(1, 0, 0);
        Plane.getRotationAroundAxis(p.getNormal(), n, null).transform(n);
        Assertions.assertEquals(p.getNormal(), n);
        
        n = new Vector3d(-1, 0, 0);
        Plane.getRotationAroundAxis(p.getNormal(), n, null).transform(n);
        Assertions.assertEquals(p.getNormal(), n);
        
        n = new Vector3d(0, 1, 0);
        Plane.getRotationAroundAxis(p.getNormal(), n, null).transform(n);
        Assertions.assertEquals(p.getNormal(), n);
        
        n = new Vector3d(0, -1, 0);
        Plane.getRotationAroundAxis(p.getNormal(), n, null).transform(n);
        Assertions.assertEquals(p.getNormal(), n);
        
        n = new Vector3d(1, 1, 1);
        n.normalize();
        Plane.getRotationAroundAxis(p.getNormal(), n, null).transform(n);
        Assertions.assertTrue(p.getNormal().epsilonEquals(n, 0.001));
        
        n = new Vector3d(-1, -1, -1);
        n.normalize();
        Plane.getRotationAroundAxis(p.getNormal(), n, null).transform(n);
        Assertions.assertTrue(p.getNormal().epsilonEquals(n, 0.001));
        
    }
    
    @Test
    public void getAlignmentMatrix() {
        Vector3d n1 = new Vector3d(1, 0, 0);
        n1.normalize();
        Plane plane1 = new Plane(n1, 3);
        
        Vector3d n2 = new Vector3d(0, 1, 0);
        n2.normalize();
        Plane plane2 = new Plane(n2, 3);
        Point3d p = plane2.getPlanePoint();
        plane1.getAlignmentMatrix(plane2, true).transform(p);
        Assertions.assertEquals(plane1.getPlanePoint(), p);
        
        n2 = new Vector3d(0, 1, 0);
        n2.normalize();
        plane2 = new Plane(n2, -3);
        p = plane2.getPlanePoint();
        plane1.getAlignmentMatrix(plane2, true).transform(p);
        Assertions.assertEquals(plane1.getPlanePoint(), p);
        
        n2 = new Vector3d(-1, -1, -1);
        n2.normalize();
        plane2 = new Plane(n2, -10);
        p = plane2.getPlanePoint();
        plane1.getAlignmentMatrix(plane2, true).transform(p);
        //Assertions.assertEquals(plane1.getPlanePoint(), p);
        Assertions.assertTrue(plane1.getPlanePoint().epsilonEquals(p, 0.001));
        
        n2 = new Vector3d(-1, -1, -1);
        n2.normalize();
        plane2 = new Plane(n2, 10);
        p = plane2.getPlanePoint();
        plane1.getAlignmentMatrix(plane2, true).transform(p);
        //Assertions.assertEquals(plane1.getPlanePoint(), p);
        Assertions.assertTrue(plane1.getPlanePoint().epsilonEquals(p, 0.001));
    }

    @Test
    public void similarity() {
        Plane p1 = new Plane(new Vector3d(-1, 0, 0), 1);
        Plane p2 = new Plane(new Vector3d(1, 0, 0), -1);
        Plane p3 = new Plane(new Vector3d(-1, 0, 0), -1);
        Plane p4 = new Plane(new Vector3d(1, 0, 0), 0);
        Plane p5 = new Plane(new Vector3d(0, 1, 0), 0);
        Assertions.assertEquals(0.0, p1.similarity(p1));
        Assertions.assertEquals(0.0, p1.similarity(p2));
        Assertions.assertTrue(p1.similarity(p3) > 0);
        Assertions.assertTrue(p4.similarity(p5) > 0);
    }
}
