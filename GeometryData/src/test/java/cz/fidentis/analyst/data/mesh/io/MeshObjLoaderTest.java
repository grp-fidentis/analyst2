package cz.fidentis.analyst.data.mesh.io;

import com.mokiat.data.front.parser.*;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.impl.io.MeshObjLoader;
import cz.fidentis.analyst.data.mesh.impl.MeshPointImpl;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link MeshObjLoader}
 * @author Marek Barinka; 456295
 */
public class MeshObjLoaderTest {
    
    Path testFileDirectory = Paths.get("src", "test", "resources", "cz", "fidentis", "analyst", "mesh", "io");
    
    @Test
    void nullFileTest() throws IOException {
        Exception ex = assertThrows(NullPointerException.class, () -> MeshObjLoader.readFromObj((File) null));
    }
    
    @Test()
    void emptyFileTest() throws IOException {
        File empty = new File(testFileDirectory.toFile(), "Empty.obj");
        Exception ex = assertThrows(IOException.class, () -> MeshObjLoader.readFromObj(empty));
        assertTrue(ex.getMessage().contains("File doesn't contain any model"));
    }
    
    @Test
    void moreObjectFileTest() { //TODO: Not testing in loader
        File moreObjects = new File(testFileDirectory.toFile(), "MoreObjects.obj");
        Exception ex = assertThrows(IOException.class, () -> MeshObjLoader.readFromObj(moreObjects));
    }
    
    @Test
    void nonTriangularFaceAloneTest() {
        File plane = new File(testFileDirectory.toFile(), "Plane.obj");
        Exception ex = assertThrows(IOException.class, () -> MeshObjLoader.readFromObj(plane));
        assertTrue(ex.getMessage().contains("Mesh contains non-triangular face"));
    }
    
    @Test
    void nonTriangularFaceInTriangularModelTest() {
        File icoSphereQuad = new File(testFileDirectory.toFile(), "IcoSphere-withQuad.obj");
        Exception ex = assertThrows(IOException.class, () -> MeshObjLoader.readFromObj(icoSphereQuad));
        assertTrue(ex.getMessage().contains("Mesh contains non-triangular face"));
    }
    
    //@Test  - off due to the unification of normal vectors in MeshObjLoader.read
    void validFileIcoSphereTest() throws IOException {
        File icoSphere = new File(testFileDirectory.toFile(), "IcoSphere-Triangles.obj");
        MeshModel m = MeshObjLoader.readFromObj(icoSphere);
        assertEquals(m.getFacets().get(0).getCornerTable().getSize(), 240);
        assertEquals(m.getFacets().get(0).getNumberOfVertices(), 240);
        assertEquals(m.getFacets().size(), 1);
        
        OBJModel model;
        final IOBJParser parser = new OBJParser();
        try (InputStream is = new FileInputStream(icoSphere)) {
            model = parser.parse(is);
        }
        
        OBJObject object = model.getObjects().get(0);
        Set<MeshPoint> points = getPointsInObject(model, object);
        
        MeshFacet facet = m.getFacets().get(0);
        for (int i = 0; i < facet.getNumberOfVertices(); i++) {
            MeshPoint p = facet.getVertex(i);
            assertTrue(points.contains(p));
        }
    }
    
    //@Test  - off due to the unification of normal vectors in MeshObjLoader.read
    void validFileTetrahedronTest() throws IOException {
        File tetrahedron = new File(testFileDirectory.toFile(), "Tetrahedron.obj");
        MeshModel m = MeshObjLoader.readFromObj(tetrahedron);
        assertEquals(m.getFacets().get(0).getCornerTable().getSize(), 12);
        assertEquals(m.getFacets().get(0).getNumberOfVertices(), 12);
        assertEquals(m.getFacets().size(), 1);
        
        OBJModel model;
        final IOBJParser parser = new OBJParser();
        try (InputStream is = new FileInputStream(tetrahedron)) {
            model = parser.parse(is);
        }
        
        OBJObject object = model.getObjects().get(0);
        Set<MeshPoint> points = getPointsInObject(model, object);
        
        MeshFacet facet = m.getFacets().get(0);
        for (int i = 0; i < facet.getNumberOfVertices(); i++) {
            MeshPoint p = facet.getVertex(i);
            assertTrue(points.contains(p));
        }
    }
    
    
    //@Test  - off due to the unification of normal vectors in MeshObjLoader.read
    void validFileIco20Test() throws IOException {
        File ico20 = new File(testFileDirectory.toFile(), "IcoSphere-20.obj");
        MeshModel m = MeshObjLoader.readFromObj(ico20);
        assertEquals(m.getFacets().get(0).getCornerTable().getSize(), 60);
        assertEquals(m.getFacets().get(0).getNumberOfVertices(), 60);
        assertEquals(m.getFacets().size(), 1);
        
        OBJModel model;
        final IOBJParser parser = new OBJParser();
        try (InputStream is = new FileInputStream(ico20)) {
            model = parser.parse(is);
        }
        
        OBJObject object = model.getObjects().get(0);
        Set<MeshPoint> points = getPointsInObject(model, object);
        
        MeshFacet facet = m.getFacets().get(0);
        for (int i = 0; i < facet.getNumberOfVertices(); i++) {
            MeshPoint p = facet.getVertex(i);
            assertTrue(points.contains(p));
        }
    }
    
    private Set<MeshPoint> getPointsInObject(OBJModel model, OBJObject object) {
        Set<MeshPoint> points = new HashSet<>();
        for (OBJMesh mesh : object.getMeshes()) {
            for (OBJFace face : mesh.getFaces()) {
                for (OBJDataReference reference : face.getReferences()) {
                    final OBJVertex vertex = model.getVertex(reference);
                    Point3d coords = new Point3d(vertex.x, vertex.y, vertex.z);
                    Vector3d norm = null;
                    Vector3d texCoords = null;
                    if (reference.hasNormalIndex()) {
                        final OBJNormal normal = model.getNormal(reference);
                        norm = new Vector3d(normal.x, normal.y, normal.z);
                    }
                    if (reference.hasTexCoordIndex()) {
                        final OBJTexCoord texCoord = model.getTexCoord(reference);
                        texCoords = new Vector3d(texCoord.u, texCoord.v, texCoord.w);
                    }
                    points.add(new MeshPointImpl(coords, norm, texCoords));
                }
            }
        }
        return points;
    }
}
