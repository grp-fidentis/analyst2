package cz.fidentis.analyst.data.mesh.impl;

import static org.junit.jupiter.api.Assertions.*;

import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTable;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import org.junit.jupiter.api.Test;


/**
 * Unit tests for CornerTable
 */
public class CornerTableTest {

    /**
     * Unit test for getIndexOfFace
     */
    @Test
    void getIndexOfFace() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 10; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        for (int i = 0; i < 10; i++) {
            assertEquals(i / 3, table.getIndexOfFace(i));
        }
    }

    /**
     * Unit test for getIndexOfFaceNegativeIndex with negative index
     */
    @Test
    void getIndexOfFaceNegativeIndex() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfFace(-1));
    }

    /**
     * Unit test for getIndexOfFaceNegativeIndex with index out of range
     */
    @Test
    void getIndexOfFaceIndexOutOfRange() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfFace(2));
    }

    /**
     * Unit test for getIndexOfOppositeCorner
     */
    @Test
    void getIndexOfOppositeCorner() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 10; i++) {
            table.addRow(new CornerTableRow(i, i - 1));
        }

        for (int i = 1; i < 10; i++) {
            assertEquals(i - 1, table.getIndexOfOppositeCorner(i));
        }
    }

    /**
     * Unit test for getIndexOfOppositeCorner without opposite corner
     */
    @Test
    void getIndexOfOppositeCornerNoOppositeCorner() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-1, table.getIndexOfOppositeCorner(0));
    }

    /**
     * Unit test for getIndexOfOppositeCorner with negative index of corner
     */
    @Test
    void getIndexOfOppositeCornerNegativeIndex() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfOppositeCorner(-1));
    }

    /**
     * Unit test for getIndexOfOppositeCorner with index of corner out of range
     */
    @Test
    void getIndexOfOppositeCornerOutOfRange() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfOppositeCorner(2));
    }

    /**
     * Unit test for getIndexOfNextCornerInFace
     */
    @Test
    void getIndexOfNextCornerInFaceFirstTriangle() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        assertEquals(1, table.getIndexOfNextCornerInFace(0));
        assertEquals(2, table.getIndexOfNextCornerInFace(1));
        assertEquals(0, table.getIndexOfNextCornerInFace(2));
    }

    /**
     * Unit test for getIndexOfNextCornerInFace
     */
    @Test
    void getIndexOfNextCornerInFaceMiddleTriangle() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        assertEquals(4, table.getIndexOfNextCornerInFace(3));
        assertEquals(5, table.getIndexOfNextCornerInFace(4));
        assertEquals(3, table.getIndexOfNextCornerInFace(5));
    }

    /**
     * Unit test for getIndexOfNextCornerInFace
     */
    @Test
    void getIndexOfNextCornerInFaceLastTriangle() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        assertEquals(7, table.getIndexOfNextCornerInFace(6));
        assertEquals(8, table.getIndexOfNextCornerInFace(7));
        assertEquals(6, table.getIndexOfNextCornerInFace(8));
    }

    /**
     * Unit test for getIndexOfNextCornerInFace with negative index of corner
     */
    @Test
    void getIndexOfNextCornerInFaceNegativeIndex() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfNextCornerInFace(-1));
    }

    /**
     * Unit test for getIndexOfNextCornerInFace with index of corner out of range
     */
    @Test
    void getIndexOfNextCornerInFaceOutOfRange() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfNextCornerInFace(2));
    }

    /**
     * Unit test for getIndexOfPreviousCornerInFace
     */
    @Test
    void getIndexOfPreviousCornerInFaceFirstTriangle() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        assertEquals(2, table.getIndexOfPreviousCornerInFace(0));
        assertEquals(0, table.getIndexOfPreviousCornerInFace(1));
        assertEquals(1, table.getIndexOfPreviousCornerInFace(2));
    }

    /**
     * Unit test for getIndexOfPreviousCornerInFace
     */
    @Test
    void getIndexOfPreviousCornerInFaceMiddleTriangle() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        assertEquals(5, table.getIndexOfPreviousCornerInFace(3));
        assertEquals(3, table.getIndexOfPreviousCornerInFace(4));
        assertEquals(4, table.getIndexOfPreviousCornerInFace(5));
    }

    /**
     * Unit test for getIndexOfPreviousCornerInFace
     */
    @Test
    void getIndexOfPreviousCornerInFaceLastTriangle() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        assertEquals(8, table.getIndexOfPreviousCornerInFace(6));
        assertEquals(6, table.getIndexOfPreviousCornerInFace(7));
        assertEquals(7, table.getIndexOfPreviousCornerInFace(8));
    }

    /**
     * Unit test for getIndexOfPreviousCornerInFace with negative index of corner
     */
    @Test
    void getIndexOfPreviousCornerInFaceNegativeIndex() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfPreviousCornerInFace(-1));
    }

    /**
     * Unit test for getIndexOfPreviousCornerInFace with index of corner out of range
     */
    @Test
    void getIndexOfPreviousCornerInFaceOutOfRange() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(-2, table.getIndexOfPreviousCornerInFace(2));
    }

    /**
     * Unit test for getIndexOfTipCornerOnLeft
     */
    @Test
    void getIndexOfTipCornerOnLeft() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(5, table.getIndexOfTipCornerOnLeft(0));
    }

    /**
     * Unit test for getIndexOfTipCornerOnLeft with no left corner
     */
    @Test
    void getIndexOfTipCornerOnLeftNoLeftCorner() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-1, table.getIndexOfTipCornerOnLeft(1));
    }

    /**
     * Unit test for getIndexOfTipCornerOnLeft with negative index of corner
     */
    @Test
    void getIndexOfTipCornerOnLeftNegativeIndex() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-2, table.getIndexOfTipCornerOnLeft(-1));
    }

    /**
     * Unit test for getIndexOfTipCornerOnLeft with index of corner out of range
     */
    @Test
    void getIndexOfTipCornerOnLeftOutOfRange() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-2, table.getIndexOfTipCornerOnLeft(6));
    }

    /**
     * Unit test for getIndexOfTipCornerOnRight
     */
    @Test
    void getIndexOfTipCornerOnRight() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(5, table.getIndexOfTipCornerOnRight(1));
    }

    /**
     * Unit test for getIndexOfTipCornerOnRight with no right corner
     */
    @Test
    void getIndexOfTipCornerOnLeftNoRightCorner() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-1, table.getIndexOfTipCornerOnRight(0));
    }

    /**
     * Unit test for getIndexOfTipCornerOnRight with negative index of corner
     */
    @Test
    void getIndexOfTipCornerOnRightNegativeIndex() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-2, table.getIndexOfTipCornerOnRight(-1));
    }

    /**
     * Unit test for getIndexOfTipCornerOnRight with index of corner out of range
     */
    @Test
    void getIndexOfTipCornerOnRightOutOfRange() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-2, table.getIndexOfTipCornerOnLeft(6));
    }

    /**
     * Unit test for getNextAroundCorner
     */
    @Test
    void getNextAroundCorner() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(3, table.getNextAroundCorner(0));
    }

    /**
     * Unit test for getNextAroundCorner with no corner on around position
     */
    @Test
    void getNextAroundCornerNoAroundCorner() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-1, table.getNextAroundCorner(1));
    }

    /**
     * Unit test for getNextAroundCorner with negative index of corner
     */
    @Test
    void getNextAroundCornerNegativeIndex() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-2, table.getNextAroundCorner(-1));
    }

    /**
     * Unit test for getNextAroundCorner with index of corner out of range
     */
    @Test
    void getNextAroundCornerOutOfRange() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(-2, table.getNextAroundCorner(6));
    }

    /**
     * Unit test for addRow
     */
    @Test
    void addRow() {
        CornerTable table = new CornerTable();
        assertEquals(0, table.getSize());
        table.addRow(new CornerTableRow(0, -1));
        assertEquals(1, table.getSize());
    }

    /**
     * Unit test for replaceRow
     */
    @Test
    void replaceRow() {
        CornerTable table = new CornerTable();
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(2, 5));
        table.addRow(new CornerTableRow(0, -1));
        table.addRow(new CornerTableRow(1, -1));
        table.addRow(new CornerTableRow(3, 2));

        assertEquals(6, table.getSize());

        table.replaceRow(1, new CornerTableRow(table.getRow(1).getVertexIndex(), 42));
        assertEquals(6, table.getSize());
        assertEquals(42, table.getRow(1).getOppositeCornerIndex());
    }

    /**
     * Unit test for getSize
     */
    @Test
    void getSize() {
        CornerTable table = new CornerTable();

        assertEquals(0, table.getSize());

        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        assertEquals(9, table.getSize());
    }

    /**
     * Unit test for getRow
     */
    @Test
    void getRow() {
        CornerTable table = new CornerTable();
        for (int i = 0; i < 9; i++) {
            table.addRow(new CornerTableRow(i, -1));
        }

        for (int i = 0; i < 9; i++) {
            assertEquals(i, table.getRow(i).getVertexIndex());
        }
    }
}
