package cz.fidentis.analyst.data.shapes;

import cz.fidentis.analyst.data.shapes.CrossSection2D;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point2d;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for computing Hausdorff distance of 2d curves.
 * This class tests whether missing segments and curves of different lengths 
 * are treated properly when computing Hausdorff distance of 2 curves.
 * 
 * @author Samuel Smoleniak
 */
public class HausdorffDistance2dTest {
    
    private CrossSection2D createHorizontalPolyChain(double startingX, double y, int size) {
        List<List<Point2d>> curveSegments = new ArrayList<>();
        curveSegments.add(createHorizontalCurveSegment(startingX, y, size));
        return new CrossSection2D(curveSegments);
    }
    
    private List<Point2d> createHorizontalCurveSegment(double startingX, double y, int size) {
        List<Point2d> segmentPoints = new ArrayList<>();
        for (int i = 0; i < size; ++i) {
            segmentPoints.add(new Point2d((float) startingX + i, y));
        }
        return segmentPoints;
    }
    
    /**
     * This test covers the case of two curves (horizontal lines) with the same length,
     * but different starting points. Correct implementation should return "1.0" as their
     * distance since protruding ends of curves should not be taken into account,
     * when computing curve distance.
     */
    @Test
    public void testEndPointSkipping() {
        CrossSection2D curveA = createHorizontalPolyChain(1, 1, 5);
        CrossSection2D curveB = createHorizontalPolyChain(3, 2, 5);
        
        assertTrue(curveA.isEndPoint(new Point2d(1.0, 1.0)));
        assertTrue(curveA.isEndPoint(new Point2d(5.0, 1.0)));
        
        assertTrue(curveB.isEndPoint(new Point2d(3.0, 2.0)));
        assertTrue(curveB.isEndPoint(new Point2d(7.0, 2.0)));

        assertEquals(1.0, curveA.hausdorffDistance(curveB,100));
    }
    
    @Test
    public void testMissingSegments() {
        CrossSection2D fullCurve = createHorizontalPolyChain(2, 2, 7);
        
        List<List<Point2d>> splitCurveSegments = new ArrayList<>();
        splitCurveSegments.add(createHorizontalCurveSegment(1, 1, 3));
        splitCurveSegments.add(createHorizontalCurveSegment(5, 1, 3));
        CrossSection2D splitCurve = new CrossSection2D(splitCurveSegments);
        
        assertEquals(1.0, fullCurve.hausdorffDistance(splitCurve,100));
    }
}
