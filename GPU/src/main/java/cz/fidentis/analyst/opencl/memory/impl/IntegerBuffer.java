package cz.fidentis.analyst.opencl.memory.impl;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLMemory;
import cz.fidentis.analyst.opencl.memory.BufferGPU;

import java.nio.IntBuffer;

/**
 * Simple resizable buffer to store integers
 *
 * @author Marek Horský
 */
public class IntegerBuffer extends BufferGPU<IntBuffer> {
    private CLBuffer<IntBuffer> intBuffer;
    private CLBuffer<IntBuffer> currentSubBuffer; // Only the occupied part of the allocated buffer

    /**
     * Initializes buffer of integers
     */

    public IntegerBuffer(CLContext context) {
        super(context);
        resize(1);
    }

    @Override
    public void release() {
        this.intBuffer.release();
    }

    @Override
    public CLBuffer<IntBuffer> get() {
        return currentSubBuffer;
    }

    @Override
    public void rewind() {
        this.intBuffer.getBuffer().rewind();
        this.currentSubBuffer.getBuffer().rewind();
    }

    @Override
    protected void allocateBuffer(int desiredSize) {
        if (intBuffer != null) {
            release();
        }
        this.intBuffer = getClContext().createIntBuffer(desiredSize, CLMemory.Mem.READ_WRITE);
    }

    @Override
    protected void resizeSubBuffer(int desiredSize) {
        if (currentSubBuffer != null && !currentSubBuffer.isReleased()) {
            currentSubBuffer.release();
        }
        currentSubBuffer = intBuffer
                .createSubBuffer(0, desiredSize, CLMemory.Mem.READ_WRITE);
    }

}
