package cz.fidentis.analyst.opencl.memory;

import com.jogamp.opencl.CLContext;

import java.nio.FloatBuffer;
import java.util.List;

/**
 * Resizable buffer of GPU memory. Can be populated on host-side.
 *
 * @param <T> Any type to be stored in buffer
 * @author Marek Horský
 */
public abstract class WriteBufferGPU<T> extends BufferGPU<FloatBuffer> {

    /**
     * Creates Write Buffer in specified OpenCl Context with default element size set to 1
     *
     * @param clContext OpenCl Context
     */
    public WriteBufferGPU(CLContext clContext) {
        super(clContext);
    }

    /**
     * Creates Write Buffer in specified OpenCl Context with given element size
     *
     * @param clContext OpenCl Context
     * @param elementSize element size
     */
    protected WriteBufferGPU(CLContext clContext, int elementSize) {
        super(clContext, elementSize);
    }

    /**
     * Copies contents to GPU
     */
    public abstract void writeToGPU();

    /**
     * Populated buffer with desired elements.
     * Overwrites previous data and resizes itself, if needed.
     * Is asynchronous! Make sure to call finish() method before use
     */
    public abstract void putAll(List<T> points);

    /**
     * Gets data, that are currently copied to GPU
     *
     * @return data
     */
    public abstract List<T> getOriginalData();
}
