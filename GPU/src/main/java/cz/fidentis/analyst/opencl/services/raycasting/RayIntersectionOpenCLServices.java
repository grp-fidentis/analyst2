package cz.fidentis.analyst.opencl.services.raycasting;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.opencl.memory.CLResources;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Set;

/**
 * Provides Ray Intersection Services implemented on GPU
 *
 * @author Marek Horský
 */
public class RayIntersectionOpenCLServices implements CLResources {
    private final BeamIntersectionVisitor visitor;

    /**
     * Creates instance of Ray Intersection OpenCL Services. Keeps the instance of {@link BeamIntersectionVisitor}
     * to allow for re-use of allocated memory
     *
     * @param clContext OpenCl Context
     * @param config    Configuration of {@link BeamIntersectionVisitor}
     */
    public RayIntersectionOpenCLServices(CLContext clContext, RayIntersectionOpenCLConfig config) {
        visitor = config.getVisitor(clContext);
    }

    @Override
    public void release() {
        visitor.release();
    }

    /**
     * Computes closest ray intersections between rays and triangles loaded in octree,
     * but reads results only as {@link MeshPoint} to save time
     *
     * @param originPoints Buffer of origin points
     * @param direction Buffer of ray direction
     * @return Set of closest ray intersection mesh points
     */

    public Set<MeshPoint> computeClosestPoints(OctreeOpenCL octree, WriteBufferGPU<Point3d> originPoints, WriteBufferGPU<Vector3d> direction){
        visitor.visitOctree(octree);
        visitor.setBeam(originPoints, direction);
        visitor.calculateClosestIntersections();
        return visitor.readResultsAsMeshPoints();
    }

    /**
     * Computes closest ray intersections between rays and triangles loaded in octree
     *
     * @param originPoints List of ray origin points
     * @param direction Ray direction vector
     * @return Set of closest ray intersection
     */
    public Set<RayIntersection> computeClosest(OctreeOpenCL octree, WriteBufferGPU<Point3d> originPoints, WriteBufferGPU<Vector3d> direction){
        visitor.visitOctree(octree);
        visitor.setBeam(originPoints, direction);
        visitor.calculateClosestIntersections();
        return visitor.readResults();
    }
}
