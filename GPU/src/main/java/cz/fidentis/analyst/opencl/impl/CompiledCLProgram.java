package cz.fidentis.analyst.opencl.impl;

import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLException;
import com.jogamp.opencl.CLKernel;
import cz.fidentis.analyst.opencl.CLProgram;
import cz.fidentis.analyst.opencl.CLProgramDef;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * A proxy object storing the information about a CL program compiled on GPU.
 *
 * @author Radek Oslejsek
 * @author Marek Horský
 */
public class CompiledCLProgram implements CLProgram {

    /**
     * OpenCL context in which the program is compiled
     */
    private final CLContext clContext;
    private final CLProgramDef programDef;
    private com.jogamp.opencl.CLProgram compiledProgram;

    /**
     * Links OpenCL source files into single file, then compiles with provided compiler options
     *
     * @param clContext  OpenCL context. Must not be {@code null}.
     * @param programDef The definition of the program to be compiled
     */
    public CompiledCLProgram(CLContext clContext, CLProgramDef programDef) {
        this.clContext = clContext;
        this.programDef = programDef;
    }

    /**
     * Compiles the program on GPU.
     *
     * @return this object
     * @throws CLException if the compilation fails
     */
    public CompiledCLProgram compile() throws CLException {
        StringBuilder builder = new StringBuilder();

        programDef.getSources().forEach(CLSourceDef -> {
            try {
                URL url = getClass().getResource("/opencl/" + CLSourceDef);
                if (url == null) {
                    throw new CLException("Unable to open " + CLSourceDef);
                }
                URLConnection connection = url.openConnection();
                builder.append(new String(connection.getInputStream().readAllBytes(), StandardCharsets.UTF_8))
                        .append(System.lineSeparator());
            } catch (IOException | CLException ex) {
                throw new CLException(CLSourceDef + ": " + ex.getMessage());
            }
        });
        compiledProgram = clContext.createProgram(builder.toString())
                .build(programDef.getCompilerOptions().toArray(new String[0]));
        return this;
    }

    /**
     * Compiles the program on GPU.
     *
     * @param binaries Uses already compiled binaries as source
     * @return this object
     * @throws CLException if the compilation fails
     */
    public CompiledCLProgram compile(Map<CLDevice, byte[]> binaries) throws CLException {
        compiledProgram = clContext.createProgram(binaries)
                .build(programDef.getCompilerOptions().toArray(new String[0]));
        return this;
    }

    /**
     * Ask GPU to release the program from its memory.
     */
    public void release() {
        compiledProgram.release();
        compiledProgram = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompiledCLProgram program)) {
            return false;
        }

        return programDef == program.programDef;
    }

    @Override
    public int hashCode() {
        return programDef.hashCode();
    }

    @Override
    public CLKernel getKernel(String key) {
        return compiledProgram.createCLKernel(key);
    }

    @Override
    public Map<CLDevice, byte[]> getBinaries() {
        return compiledProgram.getBinaries();
    }
}
