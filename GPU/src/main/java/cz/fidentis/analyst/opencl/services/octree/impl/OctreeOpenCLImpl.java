package cz.fidentis.analyst.opencl.services.octree.impl;

import com.jogamp.opencl.*;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.opencl.CLProgram;
import cz.fidentis.analyst.opencl.CLProgramDef;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.memory.BufferFactory;
import cz.fidentis.analyst.opencl.memory.BufferGPU;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.memory.impl.FacetBuffer;
import cz.fidentis.analyst.opencl.services.common.CommonKernelServices;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;

import javax.vecmath.Point3d;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collection;

/**
 * Builds Octree on GPU in top-down direction. Mesh Facet triangles are copied to GPU and subdivided into uniform octants
 * until maximum depth constraint is reached.
 * The process involves:
 * Memory allocation / initialization
 * Builds tree structure from vertices.
 * Finds non-empty leaves and largest leaf size
 * Allocates leaf memory
 * Populates structure with triangles
 *
 * @author Marek Horský
 */
public class OctreeOpenCLImpl implements OctreeOpenCL {
    private static final int MAX_DEPTH = 7; //
    private static final int OCTREE_SIZE = (int) Math.floor(Math.pow(8, MAX_DEPTH));
    private final CLCommandQueue queue;
    private final CLContext clContext;
    private final CLProgram octreeBuilderProgram;
    private final FacetBuffer facetBuffer;
    private CLBuffer<FloatBuffer> octreeBBox;
    private BufferGPU<IntBuffer> tree;
    private BufferGPU<IntBuffer> leaves;
    private BufferGPU<IntBuffer> triangleCounters;
    private CLBuffer<IntBuffer> counter;
    private int leafCount;
    private int largestLeaf;
    private int triangleThreads;
    private int vertexThreads;
    private int maxLeafCount;

    /**
     * Builds an octree from facets for desired CL Context. Everyone working with the octree needs to have the same CL Context
     *
     * @param clContext Desired CL Context
     */
    public OctreeOpenCLImpl(CLContext clContext) {
        this.clContext = clContext;
        this.queue = clContext.getMaxFlopsDevice().createCommandQueue();
        this.octreeBuilderProgram = OpenCLServices.useProgram(clContext, CLProgramDef.OCTREE_CONSTRUCTION);
        this.facetBuffer = new FacetBuffer(clContext);
        this.maxLeafCount = OCTREE_SIZE;
        prepareBuffers();
    }

    @Override
    public WriteBufferGPU<MeshTriangle> getTriangles() {
        return facetBuffer.getMeshTriangleBuffer();
    }

    @Override
    public int getMaxTrianglesPerLeaf() {
        return largestLeaf;
    }

    @Override
    public CLBuffer<IntBuffer> getTriangleIndices() {
        return leaves.get();
    }

    @Override
    public MeshTriangle getTriangle(int index) {
        return facetBuffer.getMeshTriangleBuffer()
                .getOriginalData()
                .get(index);
    }

    @Override
    public CLBuffer<FloatBuffer> getBBoxBuffer() {
        return octreeBBox;
    }

    @Override
    public CLBuffer<IntBuffer> getTree() {
        return tree.get();
    }

    @Override
    public Box getBBox() {
        return new Box(
                new Point3d(octreeBBox.getBuffer().get(0), octreeBBox.getBuffer().get(1), octreeBBox.getBuffer().get(2)),
                new Point3d(octreeBBox.getBuffer().get(4), octreeBBox.getBuffer().get(5), octreeBBox.getBuffer().get(6)));
    }

    @Override
    public void release() {
        queue.release();
        counter.release();
        triangleCounters.release();
        facetBuffer.release();
        tree.release();
        leaves.release();
    }

    @Override
    public void build(Collection<MeshFacet> facets) {
        facetBuffer.loadAsynchronously(facets);
        CommonKernelServices.initializeBuffer(clContext, queue, tree.get(), maxLeafCount, -1);

        vertexThreads = CommonKernelServices.getNearestGreaterMultiple(
                facetBuffer.getVertexBuffer().getCount(),
                CommonKernelServices.WARP_GROUP_SIZE);

        octreeBBox = CommonKernelServices.calculateBBox(clContext, queue, facetBuffer.getVertexBuffer());
        assignVertices(facetBuffer.getVertexBuffer());
        checkTriangles(facetBuffer.getMeshTriangleBuffer());
        largestLeaf = CommonKernelServices.getLargestInteger(clContext, queue, triangleCounters.get(), leafCount);
        assignTriangles(facetBuffer.getMeshTriangleBuffer());
    }

    private void prepareBuffers() {
        triangleCounters = BufferFactory.getIntBuffer(clContext);
        leaves = BufferFactory.getIntBuffer(clContext);
        tree = BufferFactory.getIntBuffer(clContext);
        tree.resize(OCTREE_SIZE);
        counter = clContext.createIntBuffer(1, CLMemory.Mem.READ_WRITE);
    }

    /**
     * Generates tree from vertices. Stopping condition {@code MAX_DEPTH} is used due to memory constraints and atomic constraints.
     * Writes tree as indexes to array. Each node is exactly 1 index pointing to his 1st child.
     * The 8 children are stored continuously (Maintains memory locality)
     */
    private void assignVertices(WriteBufferGPU<Point3d> vertexBuffer) {
        counter.getBuffer().rewind();
        counter.getBuffer().put(0, 8); // To count tree size, OpenCl atomic_add always returns old value, so I start 1 step ahead
        queue.putWriteBuffer(counter, false);

        CLKernel vertexBuild = octreeBuilderProgram.getKernel("vertexBuild")
                .putArg(vertexBuffer.getCount())
                .putArg(MAX_DEPTH)
                .putArgs(vertexBuffer.get(), octreeBBox, tree.get(), counter);

        queue.put1DRangeKernel(vertexBuild, 0, vertexThreads, CommonKernelServices.WARP_GROUP_SIZE)
                .putReadBuffer(counter, false)
                .finish();

        maxLeafCount = counter.getBuffer().get(0);
        triangleCounters.resize(maxLeafCount);
        CommonKernelServices.initializeBuffer(clContext, queue, triangleCounters.get(), triangleCounters.getCount(), 0);
        vertexBuild.release();
    }

    /**
     * Traverses tree structure generated from vertices 1 thread per triangle. Each triangle can be stored in multiple leaves,
     * therefore Stack is used. If Triangle arrives at the end of tree, leaf index is read from atomic counter and
     * stored as negative number in the node. This way, only non-empty leaves are generated. We also count how many triangles aim for each leaf,
     * so we can calculate the largest leaf.
     */

    private void checkTriangles(WriteBufferGPU<MeshTriangle> meshTriangleBuffer) {
        counter.getBuffer().put(0, -3); // Leaf counter ==> Distributes unique leaf indexes
        queue.putWriteBuffer(counter, false);

        triangleThreads = CommonKernelServices.getNearestGreaterMultiple(
                meshTriangleBuffer.getCount(),
                CommonKernelServices.WARP_GROUP_SIZE);

        CLKernel checkTriangles = octreeBuilderProgram.getKernel("checkTriangles")
                .putArg(meshTriangleBuffer.getCount())
                .putArgs(octreeBBox, meshTriangleBuffer.get(), tree.get(), triangleCounters.get(), counter);

        queue.put1DRangeKernel(checkTriangles, 0, triangleThreads, CommonKernelServices.WARP_GROUP_SIZE)
                .putReadBuffer(counter, false)
                .finish();

        leafCount = -(counter.getBuffer().get() + 2);
        checkTriangles.release();
    }

    /**
     * Traverses tree structure generated from vertices 1 thread per triangle once again, this time stores the triangles correct leaves.
     */
    private void assignTriangles(WriteBufferGPU<MeshTriangle> meshTriangleBuffer) {
        leaves.resize(leafCount * largestLeaf);
        CommonKernelServices.initializeBuffer(clContext, queue, leaves.get(), leaves.getCount(), -1);
        CommonKernelServices.initializeBuffer(clContext, queue, triangleCounters.get(), triangleCounters.getCount(), 0);

        CLKernel assignTriangles = octreeBuilderProgram.getKernel("assignTriangles")
                .putArg(meshTriangleBuffer.getCount())
                .putArg(largestLeaf)
                .putArgs(octreeBBox, meshTriangleBuffer.get(), tree.get(), triangleCounters.get(), leaves.get());

        queue.put1DRangeKernel(assignTriangles, 0, triangleThreads, CommonKernelServices.WARP_GROUP_SIZE)
                .finish();
        assignTriangles.release();
    }
}
