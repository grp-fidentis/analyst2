package cz.fidentis.analyst.opencl.impl;

import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLException;
import cz.fidentis.analyst.opencl.CLProgramDef;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * This object deals with programs of a single OpenCL context.
 * The compilation of programs is lazy (only after the first request).
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 * @author Marek Horský
 */
public class CLProgramsManager {

    /**
     * OpenCL context of compiled programs and shaders
     */
    private final CLContext clContext;

    /**
     * The "list" of already compiled programs
     */
    private final Map<CLProgramDef, CompiledCLProgram> compiledPrograms = new HashMap<>();

    /**
     * Constructor.
     *
     * @param clContext OpenCL context. Must not be {@code null}.
     */
    public CLProgramsManager(CLContext clContext) {
        this.clContext = Objects.requireNonNull(clContext);
    }

    /**
     * Lazy compilation and re-used of CL programs.
     *
     * @param programSpec Which CL program to compile/return
     * @return The reference object to the compiled CL program
     * @throws CLException if the compilation fails
     */
    public CompiledCLProgram getOrCompile(CLProgramDef programSpec, Map<CLDevice, byte[]> binaries) throws CLException {
        if (compiledPrograms.containsKey(programSpec)) {
            return compiledPrograms.get(programSpec);
        }
        CompiledCLProgram clProgram = new CompiledCLProgram(clContext, programSpec);
        compiledPrograms.put(programSpec, clProgram);
        if (binaries != null) {
            clProgram.compile(binaries);
        } else {
            clProgram.compile();
        }
        return clProgram;
    }

    /**
     * Removes the program and its shaders from this manager and from GPU memory.
     *
     * @param programDef Which CL program to release
     */
    public void release(CLProgramDef programDef) {
        if (!compiledPrograms.containsKey(programDef)) {
            compiledPrograms.get(programDef).release();
        }
    }

    /**
     * Removes all programs, asks for releasing GPU memory.
     */
    public void releaseAll() {
        compiledPrograms.keySet().forEach(this::release);
        compiledPrograms.clear();
    }

    /**
     * Returns the OpenCL context
     *
     * @return the OpenCL context
     */
    public CLContext getClContext() {
        return clContext;
    }
}
