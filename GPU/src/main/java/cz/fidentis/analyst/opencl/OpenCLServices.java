package cz.fidentis.analyst.opencl;

import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLPlatform;
import cz.fidentis.analyst.opencl.impl.SingletonGpuManagerCL;

/**
 * Stateless services for compiling OpenCL programs.
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 * @author Marek Horský
 */
public interface OpenCLServices {

    /**
     * Checks if any present device supports OpenCL or returns false straight away, if initialization is impossible
     * Also enforces minimal supported work group size to 256 as this value needs to be present as constant in .c source files
     *
     * @return true, if OpenCL is available, false otherwise
     */
    static boolean isOpenCLAvailable() {
        try {
            if (CLPlatform.listCLPlatforms().length != 0) {
                CLContext clContext = CLContext.create();
                int maxSupportedWorkGroupSize = clContext.getMaxFlopsDevice().getMaxWorkGroupSize();
                clContext.release();
                return maxSupportedWorkGroupSize >= 256;
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    /**
     * Creates OpenCL Context using most powerful available device
     * The decision of the device relies on inner calculations of JogAmp's implementation,
     * which can mistake number of processing units on cards RTX 4xxx as of version 2.5.0
     *
     * @return OpenCL context
     */

    static CLContext createContext() {
        return isOpenCLAvailable()
                ? CLContext.create() //Toggle index to manually change device
                : null;
    }

    /**
     * Compiles the OpenCL program, re-uses already compiled programs.
     *
     * @param clContext  OpenCL context. Must not be {@code null}.
     * @param programDef OpenCL program definition
     * @return The reference object to the compiled OpenCL program
     */
    static CLProgram useProgram(CLContext clContext, CLProgramDef programDef) {
        return SingletonGpuManagerCL.getInstance().getOrCompile(clContext, programDef);
    }

    /**
     * Should be called before the OpenCL context is erased.
     *
     * @param clContext OpenCL context. Must not be {@code null}.
     */
    static void release(CLContext clContext) {
        SingletonGpuManagerCL.getInstance()
                .release(clContext);
    }
}
