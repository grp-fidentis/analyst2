package cz.fidentis.analyst.opencl.memory;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.opencl.memory.impl.IntegerBuffer;
import cz.fidentis.analyst.opencl.memory.impl.MeshTriangleBuffer;
import cz.fidentis.analyst.opencl.memory.impl.RayIntersectionBuffer;
import cz.fidentis.analyst.opencl.memory.impl.VoxelBuffer;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Factory for OpenCL buffers with additional functionality
 *
 * @author Marek Horský
 */
public interface BufferFactory {
    /**
     * Creates resizable {@link IntegerBuffer}
     *
     * @return IntegerBuffer
     */
    static BufferGPU<IntBuffer> getIntBuffer(CLContext clContext) {
        return new IntegerBuffer(clContext);
    }

    /**
     * Creates resizable {@link VoxelBuffer}
     *
     * @return VoxelBuffer
     */
    static WriteBufferGPU<Point3d> getVoxelPointBuffer(CLContext context) {
        return new VoxelBuffer<>(context);
    }

    /**
     * Creates resizable {@link VoxelBuffer}
     *
     * @return VoxelBuffer
     */
    static WriteBufferGPU<Vector3d> getVoxelVectorBuffer(CLContext context) {
        return new VoxelBuffer<>(context);
    }

    /**
     * Creates resizable {@link RayIntersectionBuffer} to receive intersections calculate on GPU
     *
     * @return RayIntersectionBuffer
     */
    static BufferGPU<FloatBuffer> getRayIntersectionBuffer(CLContext context) {
        return new RayIntersectionBuffer(context);
    }

    /**
     * Buffer to hold Mesh Triangles in GPU memory
     *
     * @return MeshTriangleBuffer
     */
    static WriteBufferGPU<MeshTriangle> getMeshTriangleBuffer(CLContext clContext) {
        return new MeshTriangleBuffer(clContext);
    }
}
