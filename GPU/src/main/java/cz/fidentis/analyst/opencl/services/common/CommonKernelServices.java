package cz.fidentis.analyst.opencl.services.common;

import com.jogamp.opencl.*;
import cz.fidentis.analyst.opencl.CLProgram;
import cz.fidentis.analyst.opencl.CLProgramDef;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.memory.CLResources;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;

import javax.vecmath.Point3d;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Generic kernel operations on GPU data
 *
 * @author Marek Horský
 */
public interface CommonKernelServices extends CLResources {
    int MAX_GROUP_SIZE = 256; // Max Group Size of OpenCL kernels. Must match MAX_GROUP_SIZE defined in CL source files
    int WARP_GROUP_SIZE = 32; // Optimal group size for GPU occupation, must match WARP_GROUP_SIZE defined in CL source files

    /**
     * Calculates Bounding Box of buffer of vertices.
     *
     * @param vertexBuffer Buffer of vertices. It is not modified during the process
     * @return Buffer of 8 floats containing the bounding box
     */
    static CLBuffer<FloatBuffer> calculateBBox(CLContext clContext, CLCommandQueue queue, WriteBufferGPU<Point3d> vertexBuffer) {
        CLProgram kernelServicesProgram = OpenCLServices.useProgram(clContext, CLProgramDef.REDUCTION_SERVICES);
        int bboxSize = Math.max(8, vertexBuffer.getCount() / 16); // Determines buffer size requirements
        CLBuffer<FloatBuffer> octreeBBoxes = clContext.createFloatBuffer(bboxSize, CLMemory.Mem.READ_WRITE);
        int threadCount = getNearestGreaterMultiple(vertexBuffer.getCount(), MAX_GROUP_SIZE);

        CLKernel copyAndMergeBoundingBoxes = kernelServicesProgram.getKernel("copyAndMergeBoundingBoxes")
                .putArg(vertexBuffer.getCount())
                .putArgs(vertexBuffer.get(), octreeBBoxes);

        queue.put1DRangeKernel(copyAndMergeBoundingBoxes, 0, threadCount, MAX_GROUP_SIZE);

        threadCount /= MAX_GROUP_SIZE;
        CLKernel mergeBoundingBoxes = kernelServicesProgram.getKernel("mergeBoundingBoxes");
        int shift = 1;
        int size = threadCount;
        while (threadCount > 1) {
            threadCount = getNearestGreaterMultiple(threadCount, WARP_GROUP_SIZE);

            mergeBoundingBoxes.rewind();
            mergeBoundingBoxes.putArg(size)
                    .putArg(shift)
                    .putArgs(octreeBBoxes);
            queue.finish()
                    .put1DRangeKernel(mergeBoundingBoxes, 0, threadCount, WARP_GROUP_SIZE);

            shift *= WARP_GROUP_SIZE;
            threadCount /= WARP_GROUP_SIZE;
        }

        queue.finish();
        CLBuffer<FloatBuffer> octreeBBox = octreeBBoxes.createSubBuffer(0, 8, CLMemory.Mem.READ_ONLY);
        queue.putReadBuffer(octreeBBox, false)
                .finish();

        copyAndMergeBoundingBoxes.release();
        mergeBoundingBoxes.release();
        return octreeBBox;
    }

    /**
     * Reduces the buffer to find the largest integer. Wipes the buffer in the process
     *
     * @param intBuffer Buffer of integers
     * @param count     Size of the buffer
     * @return largest integer
     */
    static int getLargestInteger(CLContext clContext, CLCommandQueue queue, CLBuffer<IntBuffer> intBuffer, int count) {
        CLProgram kernelServicesProgram = OpenCLServices.useProgram(clContext, CLProgramDef.REDUCTION_SERVICES);
        CLKernel getLargestInteger = kernelServicesProgram.getKernel("getLargestInteger");
        int shift = 1;
        int threadCount = count;
        do {
            threadCount = getNearestGreaterMultiple(threadCount, WARP_GROUP_SIZE);
            getLargestInteger.rewind();
            getLargestInteger.putArg(shift)
                    .putArg(count)
                    .putArgs(intBuffer);

            queue.put1DRangeKernel(getLargestInteger, 0, threadCount, WARP_GROUP_SIZE)
                    .finish();

            shift *= WARP_GROUP_SIZE;
            threadCount /= WARP_GROUP_SIZE;
        }
        while (threadCount > 1);

        queue.putReadBuffer(intBuffer, false)
                .finish();
        getLargestInteger.release();
        return intBuffer.getBuffer().get(0);
    }


    /**
     * Initializes memory. Fills with values
     *
     * @param buffer GPU Buffer to be initialized
     * @param size   Size of the buffer
     * @param value  to fill the buffer with
     */
    static void initializeBuffer(CLContext clContext, CLCommandQueue queue, CLBuffer<IntBuffer> buffer, int size, int value) {
        CLProgram kernelServicesProgram = OpenCLServices.useProgram(clContext, CLProgramDef.REDUCTION_SERVICES);
        int threads = getNearestGreaterMultiple(size, WARP_GROUP_SIZE);
        CLKernel initializeMemory = kernelServicesProgram.getKernel("initializeMemory");
        initializeMemory.putArg(size)
                .putArg(value)
                .putArgs(buffer);

        queue.put1DRangeKernel(initializeMemory, 0, threads, WARP_GROUP_SIZE)
                .finish();
        initializeMemory.release();
    }

    /**
     * Returns nearest multiple of {@code groupSize} greater than {@code value}
     *
     * @param value     value
     * @param groupSize size of group
     * @return nearest greater multiple
     */
    static int getNearestGreaterMultiple(int value, int groupSize) {
        return (((value - 1) / groupSize) + 1) * groupSize;
    }
}
