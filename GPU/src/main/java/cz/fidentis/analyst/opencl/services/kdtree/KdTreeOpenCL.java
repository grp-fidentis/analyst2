package cz.fidentis.analyst.opencl.services.kdtree;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.opencl.services.kdtree.impl.KdTreeOpenCLImpl;

import javax.vecmath.Point3d;
import java.util.Collection;
import java.util.List;

/**
 * Builds left-balanced Kd-tree on CPU.
 * Doesn't provide support for the addition of points after building,
 * contrary to {@link cz.fidentis.analyst.data.kdtree}.
 * Kd-tree must be rebuilt after addition/removal of points.
 *
 * @author Ľubomír Jurčišin
 */

public interface KdTreeOpenCL {
    /**
     * Creates the kdTree from MeshPoints.
     *
     * @param points Collection of MeshPoint.
     */
    static KdTreeOpenCL create(Collection<MeshPoint> points) {
        return new KdTreeOpenCLImpl(points);
    }

    /**
     * Gets the array in which the Kd-tree is stored. Left child is
     *
     * @return List of Points.
     */
    List<Point3d> getKdTreeArray();

    /**
     * Gets the root point which is at index 0 of the KdTree array.
     *
     * @return Root point.
     */
    Point3d getRoot();

    /**
     * Gets the number of nodes in the tree.
     * It is equal to the length of the KdTree array.
     *
     * @return Number of nodes in the tree.
     */
    int getNumNodes();

    /**
     * Gets the depth of the tree.
     *
     * @return Depth of the tree.
     */
    int getDepth();
}
