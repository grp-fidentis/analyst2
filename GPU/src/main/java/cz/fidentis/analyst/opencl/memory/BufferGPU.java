package cz.fidentis.analyst.opencl.memory;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;

import java.nio.Buffer;

/**
 * Buffer in GPU memory. Read-Only on host side.
 *
 * @param <T> Any type to be stored in buffer
 * @author Marek Horský
 */

public abstract class BufferGPU<T extends Buffer> implements CLResources {
    private final CLContext clContext;
    private final CLCommandQueue queue;
    private int count; // Number of elements within the buffer
    private int size; // Currently allocated memory
    private int elementSize = 1;

    /**
     * Creates Buffer in specified OpenCl Context with default element size set to 1
     *
     * @param clContext OpenCl Context
     */
    public BufferGPU(CLContext clContext) {
        this(clContext, 1);
    }

    /**
     * Creates Buffer in specified OpenCl Context with given element size
     *
     * @param clContext   OpenCl Context
     * @param elementSize element size
     */
    protected BufferGPU(CLContext clContext, int elementSize) {
        this.clContext = clContext;
        this.queue = clContext.getMaxFlopsDevice().createCommandQueue();
        this.elementSize = elementSize;
    }

    /**
     * Some elements take multiple values to be continuously stored in buffer.
     *
     * @return Size of 1 element loaded in buffer.
     */
    public int getElementSize() {
        return elementSize;
    }

    /**
     * @return Count of the elements loaded inside. Do not confuse with the actual size of allocated memory
     */
    public int getCount() {
        return count;
    }

    /**
     * @return Memory size of the buffer. Do not confuse with current element count
     */
    public int getSize() {
        return count * elementSize;
    }

    /**
     * Get current OpenCL context
     *
     * @return OpenCL context
     */
    public CLContext getClContext() {
        return clContext;
    }

    /**
     * Creating new buffer and releasing the old one is costly. Reuse them if they are already big enough.
     *
     * @param elementCount element count to fit in
     */
    public void resize(int elementCount) {
        int desiredSize = elementCount * elementSize;
        if (size < desiredSize) {
            this.size = desiredSize;
            allocateBuffer(desiredSize);
        }
        if (count != elementCount) {
            this.count = elementCount;
            resizeSubBuffer(desiredSize);
        }
    }

    /**
     * Get underlying CLBuffer
     *
     * @return CLBuffer
     */
    public abstract CLBuffer<T> get();

    /**
     * Rewinds the buffer
     */
    public abstract void rewind();

    /**
     * Allocate the buffer to fit the elements
     *
     * @param desiredSize desired size of the buffer
     */
    protected abstract void allocateBuffer(int desiredSize);

    /**
     * Creating new SubBuffer of the main buffer to operate with smaller amount of memory
     *
     * @param desiredSize desired size of the buffer
     */
    protected abstract void resizeSubBuffer(int desiredSize);

    /**
     * Get current OpenCL queue
     *
     * @return OpenCL queue
     */
    protected CLCommandQueue getQueue() {
        return queue;
    }
}
