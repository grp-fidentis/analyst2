package cz.fidentis.analyst.opencl.impl;

import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import cz.fidentis.analyst.opencl.CLProgram;
import cz.fidentis.analyst.opencl.CLProgramDef;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A singleton object managing with OpenCL contexts and their CL programs.
 * Stores binaries to allow for faster compilation of already compiled programs for new contexts
 *
 * @author Radek Oslejsek
 * @author Marek Horský
 */
public final class SingletonGpuManagerCL {
    private final Map<CLContext, CLProgramsManager> clContexts = new HashMap<>();
    private final Map<CLProgramDef, Map<CLDevice, byte[]>> compiledBinaries = new HashMap<>();

    /**
     * Compiles the CL program, re-uses already compiled programs or re-compiles program binaries for desired context.
     *
     * @param clContext OpenCL context. Must not be {@code null}.
     * @param program   CL program
     * @return The reference object to the compiled CL program
     */
    public CLProgram getOrCompile(CLContext clContext, CLProgramDef program) {
        clContexts.putIfAbsent(
                Objects.requireNonNull(clContext),
                new CLProgramsManager(clContext)
        );
        CompiledCLProgram compiledCLProgram;
        compiledCLProgram = clContexts.get(clContext)
                .getOrCompile(program, compiledBinaries.get(program));
        compiledBinaries.putIfAbsent(program, compiledCLProgram.getBinaries());
        return compiledCLProgram;
    }

    /**
     * Should be called before the OpenCL context is erased.
     *
     * @param clContext OpenCL context. Must not be {@code null}.
     */
    public void release(CLContext clContext) {
        CLProgramsManager mngr = clContexts.getOrDefault(clContext, null);
        if (mngr != null) {
            mngr.releaseAll();
        }
        clContexts.remove(clContext);
        clContext.release();
    }

    /************************************************
     * Thread-safe implementation of the Singleton:
     ************************************************/

    private static volatile SingletonGpuManagerCL instance;
    private static final Object MUTEX = new Object();

    private SingletonGpuManagerCL() {
    }

    /**
     * Returns the single instance
     *
     * @return the single instance
     */
    public static SingletonGpuManagerCL getInstance() {
        SingletonGpuManagerCL localInstance = instance;
        if (localInstance == null) {
            synchronized (MUTEX) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SingletonGpuManagerCL();
                }
            }
        }
        return localInstance;
    }
}
