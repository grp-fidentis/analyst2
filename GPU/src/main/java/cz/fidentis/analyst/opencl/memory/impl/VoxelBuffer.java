package cz.fidentis.analyst.opencl.memory.impl;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLMemory;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;

import javax.vecmath.Tuple3d;
import java.nio.FloatBuffer;
import java.util.List;

/**
 * Buffer to hold 3D coordinates compatible with {@link Tuple3d} and it's descendants
 * Even though 3 float values are stored, we need to allocate more space
 * since OpenCL assumes the vector types (float3) to be aligned to match 4 * sizeof(component) boundary.
 *
 * @param <T> extending {@link Tuple3d}
 * @author Marek Horský
 */

public class VoxelBuffer<T extends Tuple3d> extends WriteBufferGPU<T> {
    private CLBuffer<FloatBuffer> float3DBuffer;
    private CLBuffer<FloatBuffer> currentSubBuffer; // Only the occupied part of the allocated buffer
    private List<T> originalData;

    /**
     * Initializes buffer of Voxels
     */

    public VoxelBuffer(CLContext context) {
        super(context, 4);
        resize(1);
    }

    @Override
    public List<T> getOriginalData() {
        return originalData;
    }

    @Override
    public void writeToGPU() {
        getQueue().putWriteBuffer(currentSubBuffer, false)
                .finish();
    }

    @Override
    public void release() {
        this.float3DBuffer.release();
    }

    @Override
    public CLBuffer<FloatBuffer> get() {
        return currentSubBuffer;
    }

    @Override
    public void rewind() {
        this.float3DBuffer.getBuffer().rewind();
        this.currentSubBuffer.getBuffer().rewind();
    }

    /**
     * Puts 3D coordinates into buffer
     *
     * @param index index according to the original data
     * @param point 3D coordinates
     */
    public void put(int index, T point) {
        if (point != null) {
            this.float3DBuffer.getBuffer()
                    .put(index * 4, (float) point.x)
                    .put(index * 4 + 1, (float) point.y)
                    .put(index * 4 + 2, (float) point.z);
        }
    }

    /**
     * Puts 3D coordinates into buffer + zero value to align the data
     *
     * @param point 3D coordinates extends {@link Tuple3d}. Can not be null
     */
    public void put(T point) {
        this.float3DBuffer.getBuffer()
                .put((float) point.x)
                .put((float) point.y)
                .put((float) point.z)
                .put(0.0f); //memory alignment
    }

    /**
     * Populates buffer with 3D coordinates + adds zero values to align the data
     * Overwrites previous data and resizes itself, if needed
     */
    @Override
    public void putAll(List<T> points) {
        resize(points.size());
        this.originalData = points;
        points.forEach(this::put);
        rewind();
        writeToGPU();
    }

    @Override
    protected void allocateBuffer(int desiredSize) {
        if (float3DBuffer != null) {
            release();
        }
        this.float3DBuffer = getClContext().createFloatBuffer(desiredSize, CLMemory.Mem.READ_ONLY);
    }

    @Override
    protected void resizeSubBuffer(int desiredSize) {
        if (currentSubBuffer != null && !currentSubBuffer.isReleased()) {
            currentSubBuffer.release();
        }
        currentSubBuffer = float3DBuffer
                .createSubBuffer(0, desiredSize, CLMemory.Mem.READ_ONLY);
    }
}
