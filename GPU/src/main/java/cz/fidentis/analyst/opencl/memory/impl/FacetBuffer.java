package cz.fidentis.analyst.opencl.memory.impl;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.opencl.memory.BufferFactory;
import cz.fidentis.analyst.opencl.memory.CLResources;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;

import javax.vecmath.Point3d;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * This buffer encapsulates loading of {@link MeshFacet} instances to GPU memory.
 * Since loading of larger amounts of data from host to device is very slow, this class implements CPU-side parallelization of the process
 *
 * @author Marek Horský
 */

public class FacetBuffer implements CLResources {
    private final ExecutorService executor = Executors.newFixedThreadPool(2);
    private final WriteBufferGPU<Point3d> vertexBuffer;
    private final WriteBufferGPU<MeshTriangle> triangleBuffer;
    private Future<Void> triangleFuture;
    private Future<Void> vertexFuture;

    /**
     * Initializes facet buffer
     *
     * @param clContext {@link CLContext} of the buffer
     */
    public FacetBuffer(CLContext clContext) {
        this.vertexBuffer = BufferFactory.getVoxelPointBuffer(clContext);
        this.triangleBuffer = BufferFactory.getMeshTriangleBuffer(clContext);
    }

    @Override
    public synchronized void release() {
        executor.shutdownNow();
        vertexBuffer.release();
        triangleBuffer.release();
    }

    /**
     * Loads Facets asynchronously to GPU memory. Does not wait for finish
     *
     * @param facets facets to be loaded
     */
    public synchronized void loadAsynchronously(Collection<MeshFacet> facets) {
        vertexFuture = executor.submit(() -> {
            vertexBuffer.putAll(facets
                    .stream()
                    .flatMap(facet -> facet.getVertices()
                            .stream()
                            .map(MeshPoint::getPosition))
                    .toList());
            return null;
        });
        triangleFuture = executor.submit(() -> {
            triangleBuffer.putAll(facets
                    .stream()
                    .flatMap(facet -> facet.getTriangles()
                            .stream())
                    .toList());
            return null;
        });
    }

    /**
     * Gets vertex buffer, but waits for it to be loaded on GPU
     *
     * @return loaded Vertex Buffer
     */
    public synchronized WriteBufferGPU<Point3d> getVertexBuffer() {
        if (vertexFuture != null) {
            try {
                vertexFuture.get();
                vertexFuture = null;
            } catch (InterruptedException | ExecutionException e) {
                System.err.println("Facet vertices loading to GPU was interrupted");
            }
        }
        return vertexBuffer;
    }

    /**
     * Gets MeshTriangle Buffer, but waits for it to be loaded on GPU
     *
     * @return loaded MeshTriangle Buffer
     */
    public synchronized WriteBufferGPU<MeshTriangle> getMeshTriangleBuffer() {
        if (triangleFuture != null) {
            try {
                triangleFuture.get();
                triangleFuture = null;
            } catch (InterruptedException | ExecutionException e) {
                System.err.println("Facet triangles loading to GPU was interrupted");
            }
        }
        return triangleBuffer;
    }
}
