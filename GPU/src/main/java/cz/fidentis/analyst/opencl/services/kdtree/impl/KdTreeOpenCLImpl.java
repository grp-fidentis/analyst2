package cz.fidentis.analyst.opencl.services.kdtree.impl;

import cz.fidentis.analyst.data.mesh.IPosition;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.opencl.services.kdtree.KdTreeOpenCL;

import javax.vecmath.Point3d;
import java.util.*;


/**
 * Recursively builds a left-balanced Kd-tree and stores it in an array.
 *
 * @author Ľubomír Jurčišin
 */
public class KdTreeOpenCLImpl implements KdTreeOpenCL {
    private final Point3d[] kdTree;
    private int depth;

    /**
     * Builds a left-balanced Kd-tree from MeshPoints. Stores the resulting array and the tree's depth.
     *
     * @param points - MeshPoints from which the Kd-tree is built
     */
    public KdTreeOpenCLImpl(Collection<MeshPoint> points) {
        if (points == null) {
            kdTree = new Point3d[0];
            return;
        }
        kdTree = new Point3d[points.size()];
        depth = build(points);
    }

    @Override
    public List<Point3d> getKdTreeArray() {
        return Arrays.stream(kdTree).toList();
    }

    @Override
    public Point3d getRoot() {
        if (kdTree == null) {
            return null;
        }
        return kdTree[0];
    }

    @Override
    public int getNumNodes() {
        return kdTree.length;
    }

    @Override
    public int getDepth() {
        return depth;
    }

    private int build(Collection<MeshPoint> points) {
        return buildRecursively(points.toArray(new MeshPoint[0]), 0, 0);
    }

    private int buildRecursively(MeshPoint[] points, int kdTreeLocation, int depth) {
        if (points.length == 1) {
            kdTree[kdTreeLocation] = new Point3d(points[0].getX(), points[0].getY(), points[0].getZ());
            return depth;
        }

        if (points.length == 0) {
            return depth;
        }

        switch (depth % 3) {
            case 0:
                Arrays.sort(points, Comparator.comparingDouble(IPosition::getX));
                break;
            case 1:
                Arrays.sort(points, Comparator.comparingDouble(IPosition::getY));
                break;
            case 2:
                Arrays.sort(points, Comparator.comparingDouble(IPosition::getZ));
                break;
            default:
                break;
        }

        int maximum = largestPowerOfTwo(points.length);
        int remainder = points.length - (maximum - 1);

        // J. A. Baerentzen. (2003, Aug 25). On Left-balancing Binary Trees [Online].
        // Available: https://www2.imm.dtu.dk/pubdb/edoc/imm2535.pdf
        int leftAmount;
        int rightAmount;
        if (remainder <= maximum/2) {
            leftAmount = (maximum - 2)/2 + remainder;
            rightAmount = (maximum - 2)/2;
        } else {
            leftAmount = (maximum - 2)/2 + maximum/2;
            rightAmount = (maximum - 2)/2 + remainder - maximum/2;
        }

        MeshPoint[] left = Arrays.copyOfRange(points, 0, leftAmount);
        MeshPoint[] right = Arrays.copyOfRange(points, leftAmount + 1, leftAmount + 1 + rightAmount);

        kdTree[kdTreeLocation] = new Point3d(points[leftAmount].getX(), points[leftAmount].getY(), points[leftAmount].getZ());
        int leftDepth = buildRecursively(left, 2 * kdTreeLocation + 1, depth + 1);
        int rightDepth = buildRecursively(right, 2 * kdTreeLocation + 2, depth + 1);

        return Math.max(leftDepth, rightDepth);
    }


    private int largestPowerOfTwo(int n) {
         // calculates the largest 2^x so that the result is smaller than n
         // needed to know how many levels the kd-tree will have
        int power = 0;
        while (2 << power <= n) {
            power++;
        }
        return 2 << (power - 1);
    }
}
