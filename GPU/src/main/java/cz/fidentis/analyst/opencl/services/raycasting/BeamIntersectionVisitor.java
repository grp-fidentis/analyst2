package cz.fidentis.analyst.opencl.services.raycasting;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.opencl.memory.CLResources;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Set;

/**
 * This visitor throws a beam towards octree and finds closest intersections
 * ray with facets.
 * The reading of results is separated from calculation, because sometimes we don't need {@link RayIntersection},
 * but only {@link MeshPoint}. Minimizing the data reading results in speed-up.
 *
 * @author Marek Horský
 */
public interface BeamIntersectionVisitor extends CLResources {
    /**
     * Visits OpenCl Octree
     *
     * @param octree instance of {@link OctreeOpenCL}
     */
    void visitOctree(OctreeOpenCL octree);

    /**
     * Stores {@link Ray} components in visitor
     *
     * @param originsBuffer origin points of ray
     * @param directionsBuffer common direction of all rays
     */
    void setBeam(WriteBufferGPU<Point3d> originsBuffer, WriteBufferGPU<Vector3d> directionsBuffer);

    /**
     * Dispatches asynchronous kernel to calculate the closest ray intersections with {@link OctreeOpenCL}.
     * The method does not wait for it to finish.
     */
    void calculateClosestIntersections();

    /**
     * Waits for results and then copies them from GPU as MeshPoints
     * Ignores calculated distances to speed-up reading
     *
     * @return Set of closest {@link MeshPoint}
     */
    Set<MeshPoint> readResultsAsMeshPoints();

    /**
     * Checks if the results are ready and then copies them from GPU
     *
     * @return Set of closest {@link RayIntersection}
     */
    Set<RayIntersection> readResults();
}
