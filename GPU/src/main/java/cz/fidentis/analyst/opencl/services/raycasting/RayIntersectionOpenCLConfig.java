package cz.fidentis.analyst.opencl.services.raycasting;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.opencl.services.raycasting.impl.BeamIntersectionVisitorImpl;

/**
 * Configuration of ray casting
 *
 * @param smoothing Smoothing strategy
 * @param filter    If {@code true}, then only triangles with the same orientation are taken into account
 * @author Radek Oslejsek
 * @author Marek Horský
 */
public record RayIntersectionOpenCLConfig(MeshTriangle.Smoothing smoothing, boolean filter) {
    /**
     * Instantiates and returns a visitor.
     *
     * @param clContext OpenCl Context of the visitor
     * @return a visitor
     */
    public BeamIntersectionVisitor getVisitor(CLContext clContext) {
        return new BeamIntersectionVisitorImpl(clContext, smoothing, filter);
    }
}
