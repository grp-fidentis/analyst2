package cz.fidentis.analyst.opencl;

import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLKernel;

import java.util.Map;

/**
 * A CL program, which links multiple source files together
 *
 * @author Marek Horský
 */
public interface CLProgram {
    /**
     * Gets kernel from CLProgram
     *
     * @param key name of the kernel method
     * @return kernel to be executed
     */
    CLKernel getKernel(String key);

    /**
     * Gets compiled binaries of CLProgram. It is faster to recompile binary for new context than to compile the program from scratch.
     *
     * @return compiled binaries
     */
    Map<CLDevice, byte[]> getBinaries();
}
