package cz.fidentis.analyst.opencl.memory.impl;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import java.nio.FloatBuffer;
import java.util.List;

/**
 * Class providing readable conversion of {@link MeshTriangle} data to reside in OpenCL buffers
 * Each triangle is represented by 3 points and 3 vectors.
 * Each point and vector are copied to separate buffers, so the whole triangle is stored under one index.
 * For ex. To get 5th triangle on GPU --> we need to collect the components under index 5
 *
 * @author Marek Horský
 */

public class MeshTriangleBuffer extends WriteBufferGPU<MeshTriangle> {
    private final VoxelBuffer<Tuple3d> triangles;
    private List<MeshTriangle> originalData;

    /**
     * Initializes buffer of size according to element count
     *
     * @param clContext {@link CLContext} of the buffer
     */
    public MeshTriangleBuffer(CLContext clContext) {
        super(clContext, 6); // Each triangle consists of 6 Voxels
        this.triangles = new VoxelBuffer<>(clContext);
    }

    @Override
    public void release() {
        triangles.release();
    }

    @Override
    public void writeToGPU() {
        triangles.writeToGPU();
    }

    @Override
    public CLBuffer<FloatBuffer> get() {
        return triangles.get();
    }

    @Override
    public void rewind() {
        triangles.rewind();
    }

    @Override
    public void putAll(List<MeshTriangle> triangles) {
        resize(triangles.size());
        this.originalData = triangles;
        this.originalData.forEach(this::put);
        rewind();
        writeToGPU();
    }

    @Override
    public List<MeshTriangle> getOriginalData() {
        return originalData;
    }

    @Override
    protected void allocateBuffer(int elementCount) {
        triangles.resize(elementCount);
    }

    @Override
    protected void resizeSubBuffer(int elementCount) {
    }

    /**
     * Puts MeshTriangle to desired index in buffers
     *
     * @param tri MeshTriangle
     */
    private void put(MeshTriangle tri) {
        triangles.put(tri.getVertex1());
        triangles.put(tri.getVertex2());
        triangles.put(tri.getVertex3());
        if (tri.getPoint1().getNormal() != null) { // Triangle does not have computed normals
            triangles.put(tri.getPoint1().getNormal());
            triangles.put(tri.getPoint2().getNormal());
            triangles.put(tri.getPoint3().getNormal());
        } else {
            triangles.put(new Point3d());
            triangles.put(new Point3d());
            triangles.put(new Point3d());
        }
    }
}
