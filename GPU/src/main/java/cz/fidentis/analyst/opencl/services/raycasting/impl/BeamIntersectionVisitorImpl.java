package cz.fidentis.analyst.opencl.services.raycasting.impl;

import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLKernel;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.ray.RayFactory;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.opencl.CLProgramDef;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.memory.BufferFactory;
import cz.fidentis.analyst.opencl.memory.BufferGPU;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.services.common.CommonKernelServices;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;
import cz.fidentis.analyst.opencl.services.raycasting.BeamIntersectionVisitor;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.nio.FloatBuffer;
import java.util.HashSet;
import java.util.Set;

/**
 * Visitor providing methods to ray cast {@link OctreeOpenCL} in parallel utilizing GPU.
 * Visits octree and calculates closest intersections. Can be reused multiple times.
 *
 * @author Marek Horský
 */
public class BeamIntersectionVisitorImpl implements BeamIntersectionVisitor {
    private final CLCommandQueue queue;
    private final CLKernel calculateClosestIntersections;
    private final BufferGPU<FloatBuffer> intersectionsBuffer;
    private final boolean filter;
    private final MeshTriangle.Smoothing smoothing;
    private OctreeOpenCL octree;
    private WriteBufferGPU<Point3d> rayOriginsBuffer;
    private WriteBufferGPU<Vector3d> rayDirectionsBuffer;
    private int threadCount;

    /**
     * Constructor compiles OpenCL source-code needed to calculate ray intersections
     *
     * @param smoothing Smoothing strategy
     * @param filter    If {@code true}, then only triangles with the same orientation are taken into account
     */
    public BeamIntersectionVisitorImpl(CLContext context, MeshTriangle.Smoothing smoothing, boolean filter) {
        this.queue = context.getMaxFlopsDevice().createCommandQueue();
        this.calculateClosestIntersections = OpenCLServices
                .useProgram(context, CLProgramDef.OCTREE_ACCELERATED_RAY_CASTING)
                .getKernel("calculateClosestIntersections");
        this.intersectionsBuffer = BufferFactory.getRayIntersectionBuffer(context);
        this.filter = filter;
        this.smoothing = smoothing;
    }

    @Override
    public void visitOctree(OctreeOpenCL octree) {
        this.octree = octree;
    }

    @Override
    public void setBeam(WriteBufferGPU<Point3d> rayOriginsBuffer, WriteBufferGPU<Vector3d> rayDirectionsBuffer) {
        this.intersectionsBuffer.resize(rayOriginsBuffer.getCount());
        this.rayOriginsBuffer = rayOriginsBuffer;
        this.rayDirectionsBuffer = rayDirectionsBuffer;
        this.threadCount = CommonKernelServices.getNearestGreaterMultiple(rayOriginsBuffer.getCount(), CommonKernelServices.WARP_GROUP_SIZE);
    }

    @Override
    public void calculateClosestIntersections() {

        calculateClosestIntersections.rewind();
        calculateClosestIntersections.putArg(rayOriginsBuffer.getCount())
                .putArg(octree.getMaxTrianglesPerLeaf())  // Largest detected leaf
                .putArg(smoothing.ordinal())
                .putArg(filter ? 1 : 0) // Applied filter
                .putArgs(rayOriginsBuffer.get(), rayDirectionsBuffer.get(), // Rays
                        octree.getTriangles().get(),   // Mesh triangles Normals
                        octree.getBBoxBuffer(), octree.getTree(), octree.getTriangleIndices(), // Octree
                        intersectionsBuffer.get());  // Result

        queue.put1DRangeKernel(calculateClosestIntersections, 0, threadCount, CommonKernelServices.WARP_GROUP_SIZE)
                .putReadBuffer(intersectionsBuffer.get(), false);
    }

    @Override
    public Set<MeshPoint> readResultsAsMeshPoints() {
        Set<MeshPoint> result = new HashSet<>(intersectionsBuffer.getCount());
        FloatBuffer points = intersectionsBuffer.get().getBuffer();

        queue.finish(); // To ensure results are copied from GPU

        for (int index = 0; index < intersectionsBuffer.getSize(); index += intersectionsBuffer.getElementSize()) {
            if (points.get(index + 3) != 0) { // 0 means no triangle intersected
                result.add(MeshFactory.createMeshPoint(
                        new Point3d(points.get(index), points.get(index + 1), points.get(index + 2)),
                        new Vector3d(points.get(index + 4), points.get(index + 5), points.get(index + 6)),
                        null,
                        null));
            }
        }
        return result;
    }

    @Override
    public Set<RayIntersection> readResults() {
        Set<RayIntersection> result = new HashSet<>(rayOriginsBuffer.getCount());
        FloatBuffer points = intersectionsBuffer.get().getBuffer();
        int index;
        int triangle;

        queue.finish(); // To ensure results are copied from GPU

        for (int i = 0; i < rayOriginsBuffer.getCount(); i++) {
            index = i * 8;
            triangle = (int) points.get(index + 3);
            if (triangle != 0) {
                RayIntersection inter = RayFactory.createRayIntersection(
                        new Point3d(points.get(index), points.get(index + 1), points.get(index + 2)),
                        new Vector3d(points.get(index + 4), points.get(index + 5), points.get(index + 6)),
                        octree.getTriangle(Math.abs(triangle < 0 ? triangle + 1 : triangle - 1)),
                        triangle >= 0);
                inter.setDistance(rayOriginsBuffer.getOriginalData().get(i));
                result.add(inter);
            }
        }
        return result;
    }

    @Override
    public void release() {
        this.queue.release();
        this.calculateClosestIntersections.release();
        this.intersectionsBuffer.release();
    }
}
