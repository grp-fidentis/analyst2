package cz.fidentis.analyst.opencl.memory;

/**
 * Interface to allow release of GPU resources held by this object
 *
 * @author Marek Horský
 */
public interface CLResources {
    /**
     * Explicitly releases GPU resources held by this object
     */
    void release();
}
