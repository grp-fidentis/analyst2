package cz.fidentis.analyst.opencl;

/**
 * Definitions of OpenCl source files
 * Source codes (.c files) must be located in the "/opencl" subdirectory of resources.
 *
 * @author Radek Oslejsek
 * @author Marek Horský
 */
public enum CLSourceDef {
    SPATIAL_DATA_UTILS("SpatialDataUtils.c"),
    BOUNDING_BOX_UTILS("BoundingBoxUtils.c"),
    RAY_INTERSECTION("RayIntersection.c"),
    COMPUTE_INTERSECTIONS("ComputeIntersections.c"),
    OCTREE_TRAVERSAL("OctreeTraversal.c"),
    OCTREE_BUILDER("OctreeBuilder.c"),
    COMMON_KERNEL_SERVICES("CommonKernelServices.c"),
    TRIANGLE_STACK_STRUCTURES("TriangleStackStructures.c"),
    OCTREE_TRAVERSAL_STRUCTURES("OctreeTraversalStructures.c"),
    POISSON_GPU_UTILS("ProjectorUtils.c");
    private final String file;

    CLSourceDef(String file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return file;
    }

    public String getFile() {
        return file;
    }
}
