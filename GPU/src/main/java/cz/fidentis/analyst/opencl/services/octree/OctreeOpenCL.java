package cz.fidentis.analyst.opencl.services.octree;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.opencl.memory.CLResources;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.memory.impl.MeshTriangleBuffer;
import cz.fidentis.analyst.opencl.services.octree.impl.OctreeOpenCLImpl;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collection;

/**
 * Creates octree in OpenCL memory
 * Octree is static - Does not support modifications after creation
 * The implementation is thread-safe
 *
 * @author Marek Horský
 */
public interface OctreeOpenCL extends CLResources {

    /**
     * Creates empty instance of OpenCL Octree.
     *
     * @return a new OpenCL Octree
     */
    static OctreeOpenCL create(CLContext clContext) {
        return new OctreeOpenCLImpl(clContext);
    }

    /**
     * Builds the octree with provided facets. Reuses allocated memory if possible
     *
     * @param facets Facets to be loaded in octree
     */
    void build(Collection<MeshFacet> facets);

    /**
     * Gets Triangles from the octree. The triangles are loaded in {@link MeshTriangleBuffer}
     *
     * @return MeshTriangleBuffer instance
     */
    WriteBufferGPU<MeshTriangle> getTriangles();

    /**
     * Largest existing leaf - maximal number of triangles
     *
     * @return The Largest existing leaf
     */
    int getMaxTrianglesPerLeaf();

    /**
     * Triangle indexes stored in array to represent leaves. Each leaf has reserved length of {@code getMayTrianglesPerLeaf}
     *
     * @return leaves to triangles indexation
     */
    CLBuffer<IntBuffer> getTriangleIndices();

    /**
     * Gets triangle corresponding to its index from GPU buffer
     *
     * @param index of triangle from GPU
     * @return MeshTriangle instance
     */
    MeshTriangle getTriangle(int index);

    /**
     * Gets BBox calculated during the Octree creation. Equal to Bounding Box of all vertices of the submitted facets
     *
     * @return Octree Bounding Box
     */
    Box getBBox();

    /**
     * Gets Octree Bounding Box in buffer
     *
     * @return CLBuffer containing Bounding Box
     */
    CLBuffer<FloatBuffer> getBBoxBuffer();

    /**
     * Get indexes pointing from parent nodes to their children and leaves to their respective triangle content
     * index -1 --> reserved as empty value
     * indexes > -1 --> Pointers to nodes, each value points to where node's children are stored continuously
     * For example: If we find 64 at index 5, it means children of node 5 are stored at indexes 64-72
     * indexes < -1 --> To save memory, the negative value signify the child is leaf.
     * Making the value positive gives us pointer to the leaf's triangles in Triangle Indices
     *
     * @return CLBuffer of integers
     */
    CLBuffer<IntBuffer> getTree();

}
