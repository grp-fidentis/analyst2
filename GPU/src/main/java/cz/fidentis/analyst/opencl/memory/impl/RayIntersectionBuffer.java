package cz.fidentis.analyst.opencl.memory.impl;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLMemory;
import com.jogamp.opencl.CLSubBuffer;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.opencl.memory.BufferGPU;

import java.nio.FloatBuffer;

/**
 * Buffer to hold results of ray intersections on GPU
 * Each intersection is aligned to 8 continuous values
 * 0,1,2 --> Intersection Point
 * 3 --> Triangle index --> in GPU memory - needed if we want to recreate the {@link RayIntersection} on CPU side
 *          Triangle index is positive if the HIT is direct, Negative, if the hit is indirect, and 0 if no HIT happened
 * 4,5,6 --> Intersection Normal
 * 7 --> Ray Intersection distance
 *
 * @author Marek Horský
 */
public class RayIntersectionBuffer extends BufferGPU<FloatBuffer> {
    private CLBuffer<FloatBuffer> intersections;
    private CLSubBuffer<FloatBuffer> currentIntersections;

    /**
     * Initializes buffer of ray intersections
     *
     */
    public RayIntersectionBuffer(CLContext clContext) {
        super(clContext, 8);
        resize(1);
    }

    @Override
    public CLBuffer<FloatBuffer> get() {
        return currentIntersections;
    }

    @Override
    public void rewind() {
        intersections.getBuffer().rewind();
        currentIntersections.getBuffer().rewind();
    }

    @Override
    public void release() {
        intersections.release();
    }

    @Override
    protected void allocateBuffer(int desiredSize) {
        if (intersections != null) {
            intersections.release();
        }
        intersections = getClContext().createFloatBuffer(desiredSize, CLMemory.Mem.WRITE_ONLY);
    }

    @Override
    protected void resizeSubBuffer(int desiredSize) {
        if (currentIntersections != null && !currentIntersections.isReleased()) {
            currentIntersections.release();
        }
        currentIntersections = intersections
                .createSubBuffer(0, desiredSize, CLMemory.Mem.WRITE_ONLY);
    }
}
