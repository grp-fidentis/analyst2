package cz.fidentis.analyst.opencl;

import com.jogamp.opencl.CLProgram;

import java.util.ArrayList;
import java.util.List;

/**
 * Definitions of CL programs, i.e., their dependencies on source files
 *
 * @author Radek Oslejsek
 * @author Marek Horský
 */
public enum CLProgramDef {

    /**
     * Traverses Octree structure and finds closest intersections with beam.
     */
    OCTREE_ACCELERATED_RAY_CASTING(
            List.of(CLSourceDef.SPATIAL_DATA_UTILS,
                    CLSourceDef.OCTREE_TRAVERSAL_STRUCTURES,
                    CLSourceDef.RAY_INTERSECTION,
                    CLSourceDef.OCTREE_TRAVERSAL,
                    CLSourceDef.COMPUTE_INTERSECTIONS),
            List.of(CLProgram.CompilerOptions.ENABLE_MAD,
                    CLProgram.CompilerOptions.FAST_RELAXED_MATH,
                    CLProgram.CompilerOptions.NO_SIGNED_ZEROS)
    ),

    /**
     * Constructs linearized octree out of MeshFacets. Minimum OpenCL version 1.2 (due to global atomics)
     */
    OCTREE_CONSTRUCTION(
            List.of(CLSourceDef.SPATIAL_DATA_UTILS,
                    CLSourceDef.BOUNDING_BOX_UTILS,
                    CLSourceDef.TRIANGLE_STACK_STRUCTURES,
                    CLSourceDef.OCTREE_BUILDER),
            List.of(CLProgram.CompilerOptions.ENABLE_MAD,
                    CLProgram.CompilerOptions.FAST_RELAXED_MATH,
                    CLProgram.CompilerOptions.NO_SIGNED_ZEROS)
    ),

    /**
     * Common kernel reduction operations like find the largest number in buffer or Bounding box calculations
     */
    REDUCTION_SERVICES(
            List.of(CLSourceDef.SPATIAL_DATA_UTILS,
                    CLSourceDef.BOUNDING_BOX_UTILS,
                    CLSourceDef.COMMON_KERNEL_SERVICES),
            List.of(CLProgram.CompilerOptions.ENABLE_MAD,
                    CLProgram.CompilerOptions.FAST_RELAXED_MATH,
                    CLProgram.CompilerOptions.NO_SIGNED_ZEROS)
    ),

    /**
     * Kernels specific to GPU-based Poisson sub-sampling
     */
    POISSON_GPU_UTILS(
            List.of(CLSourceDef.SPATIAL_DATA_UTILS,
                    CLSourceDef.POISSON_GPU_UTILS),
            List.of(CLProgram.CompilerOptions.ENABLE_MAD,
                    CLProgram.CompilerOptions.FAST_RELAXED_MATH,
                    CLProgram.CompilerOptions.NO_SIGNED_ZEROS)
    );

    private final List<CLSourceDef> sources = new ArrayList<>();
    private final List<String> compilerOptions = new ArrayList<>();

    CLProgramDef(List<CLSourceDef> sources, List<String> compilerOptions) {
        this.sources.addAll(sources);
        this.compilerOptions.addAll(compilerOptions);
    }

    public List<CLSourceDef> getSources() {
        return sources;
    }

    public List<String> getCompilerOptions() {
        return compilerOptions;
    }
}
