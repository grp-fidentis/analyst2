package cz.fidentis.analyst.glsl.buffers;

/**
 * A texture buffer backed by another buffer.
 * This buffer is used for the {@code GL_TEXTURE_BUFFER} OpenGL target.
 *
 * @author Radek Oslejsek
 */
public interface TextureBuffer extends GlslBuffer {

    /**
     * Allocates the buffer using {@code glTexBuffer} OpenGL method
     *
     * @param filter Filtering mode
     * @param dataObject name of the buffer object which data store gets attached to the texture object
     */
    void allocate(int filter, int dataObject);

}
