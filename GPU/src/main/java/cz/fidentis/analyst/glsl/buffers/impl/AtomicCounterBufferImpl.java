package cz.fidentis.analyst.glsl.buffers.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.AtomicCounterBuffer;
import cz.fidentis.analyst.glsl.buffers.BufferDef;

import static com.jogamp.opengl.GL2ES3.GL_ATOMIC_COUNTER_BUFFER;

/**
 * A buffer for {@code GL_ATOMIC_COUNTER} target.
 *
 * @author Radek Oslejsek
 */
public class AtomicCounterBufferImpl extends SsboBufferImpl implements AtomicCounterBuffer {

    /**
     * Constructor.
     *
     * @param bufferName buffer name
     * @param glContext  OpenGL context
     * @param usage      OpenGL usage type
     */
    public AtomicCounterBufferImpl(BufferDef bufferName, GLContext glContext, int usage) {
        super(bufferName, glContext, usage, 4, GL_ATOMIC_COUNTER_BUFFER);
    }

    @Override
    public void reset() {
        writeBytesTo(0,4, byteBuffer -> byteBuffer.putInt(0));
    }

    @Override
    public void allocate() {
        allocate(1, true);
    }
}
