package cz.fidentis.analyst.glsl.code.impl;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import cz.fidentis.analyst.glsl.code.GlslShaderDef;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static com.jogamp.opengl.GL.GL_FALSE;
import static com.jogamp.opengl.GL2.GL_COMPILE_STATUS;
import static com.jogamp.opengl.GL2.GL_INFO_LOG_LENGTH;

/**
 * A proxy object storing the information about a shader compiled on GPU.
 *
 * @author Radek Oslejsek
 */
public class CompiledShader {

    /**
     * ID of the shader in the OpenGL context
     */
    private int id;

    /**
     * OpenGL context in which the shader is compiled
     */
    private final GLContext glContext;

    private final GlslShaderDef shaderDef;

    /**
     * Constructor that compiles the shader on GPU.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL2.
     * @param shaderDef The definition of the shader to be compiled
     * @throws GLException if the GLSL source file reading or compilation fails
     */
    public CompiledShader(GLContext glContext, GlslShaderDef shaderDef) throws GLException {
        this.glContext = Objects.requireNonNull(glContext);
        this.shaderDef = Objects.requireNonNull(shaderDef);
        try {
            URL url = getClass().getResource("/shaders/" + shaderDef);
            if (url == null) {
                throw new GLException("Unable to open " + shaderDef);
            }
            URLConnection connection = url.openConnection();
            String sourceCode = new String(connection.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
            this.id = compileShaderCode(glContext.getGL().getGL2(), shaderDef.getShaderType(), sourceCode);
        } catch (IOException | GLException ex) {
            throw new GLException(shaderDef.getFile() + ": " + ex.getMessage(), ex);
        }
    }

    /**
     * Ask GPU to release the shader from its memory.
     * Shaders are removed only after they are not attached to any GLSL program anymore.
     */
    public void release() {
        if (id != -1) {
            glContext.getGL().getGL2().glDeleteShader(id);
            id = -1;
        }
    }

    /**
     * Returns the ID of the shader in the OpenGL context
     * @return shader's ID
     */
    public int getId() {
        return id;
    }

    public GlslShaderDef getShaderDef() {
        return shaderDef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompiledShader that)) {
            return false;
        }

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }

    /**
     * A utility method which compiles given GLSL code
     *
     * @param gl OpenGL context
     * @param shaderType Shader type to create, i.e., {@code GL_VERTEX_SHADER} or {@code GL_FRAGMENT_SHADER}
     * @param shaderSourceCode Shader's source code in GLSL
     * @return shader's ID in the provided OpenGL context
     * @throws GLException if the shader initiation fails
     */
    private int compileShaderCode(GL2 gl, int shaderType, String shaderSourceCode) {
        // compile shader
        int shaderId = gl.glCreateShader(shaderType);

        gl.glShaderSource(shaderId, 1, new String[]{shaderSourceCode}, new int[]{shaderSourceCode.length()}, 0);
        gl.glCompileShader(shaderId);

        // check shader status
        int[] compileStatus = new int[1];
        gl.glGetShaderiv(shaderId, GL_COMPILE_STATUS, compileStatus, 0);

        if (GL_FALSE == compileStatus[0]) {
            int[] infoLogLength = new int[1];
            gl.glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, infoLogLength, 0);

            byte[] infoLogBytes = new byte[infoLogLength[0]];
            gl.glGetShaderInfoLog(shaderId, infoLogLength[0], infoLogLength, 0, infoLogBytes, 0);

            throw new GLException(new String(infoLogBytes, 0, infoLogLength[0]));
        }

        return shaderId;
    }
}
