package cz.fidentis.analyst.glsl.buffergroups;

import cz.fidentis.analyst.glsl.GlslServices;
import cz.fidentis.analyst.glsl.buffergroups.impl.RayCastingDistBufferGroup;
import cz.fidentis.analyst.glsl.buffergroups.impl.VisualEffectsBufferGroup;

/**
 * Names for predefined buffer groups used together by GLSL programs and their shaders.
 * Implementation is provided in the {@code impl} package.
 * Instantiation is managed by the {@link GlslServices#registerBuffers } service.
 *
 * @author Pavol Kycina
 */
public enum BufferGroupDef {

    /**
     * A group of buffers used for ray-casting distance measurement.
     * For the list of buffers, see {@link RayCastingDistBufferGroup}.
     */
    RAY_CASTING_DISTANCE,

    /**
     * A group of buffers used for visual effect of face-to-face comparison scene.
     * For the list of buffers, see {@link VisualEffectsBufferGroup}.
     */
    VISUAL_EFFECTS

}
