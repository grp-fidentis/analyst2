package cz.fidentis.analyst.glsl.buffergroups.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffergroups.GlslBufferGroup;
import cz.fidentis.analyst.glsl.buffers.*;
import cz.fidentis.analyst.glsl.buffers.impl.AbstractBuffer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * This class represents a group of registered buffers in an OpenGL context.
 * These buffers nay or may not be allocated.
 *
 * @author Pavol Kycina
 * @author Radek Oslejsek
 */
public abstract class AbstractBufferGroup implements GlslBufferGroup {

    private final GLContext glContext;
    private final Map<BufferDef, GlslBuffer> buffers = new HashMap<>();

    /**
     * Constructor. It registers buffers in the OpenGL context and initialize their state without any data.
     *
     * @param glContext Active OpenGL context
     * @param buffers List of buffers
     */
    public AbstractBufferGroup(GLContext glContext, List<AbstractBuffer> buffers) {
        this.glContext = Objects.requireNonNull(glContext);
        buffers.forEach(b -> this.buffers.put(b.getBufferName(), b));
    }

    @Override
    public SsboBuffer getSSboBuffer(BufferDef bufferName) {
        return (buffers.get(bufferName) instanceof SsboBuffer ret) ? ret : null;
    }

    @Override
    public ImageBuffer getImageBuffer(BufferDef bufferName) {
        return (buffers.get(bufferName) instanceof ImageBuffer ret) ? ret : null;
    }

    @Override
    public TextureBuffer getTextureAdapterBuffer(BufferDef bufferName) {
        return (buffers.get(bufferName) instanceof TextureBuffer ret) ? ret : null;
    }

    @Override
    public FrameBuffer getFrameBuffer(BufferDef bufferName) {
        return (buffers.get(bufferName) instanceof FrameBuffer ret) ? ret : null;
    }

    @Override
    public PixelUnpackBuffer getPixelUnpackBuffer(BufferDef bufferName) {
        return (buffers.get(bufferName) instanceof PixelUnpackBuffer ret) ? ret : null;
    }

    @Override
    public AtomicCounterBuffer getAtomicCounterBuffer(BufferDef bufferName) {
        return (buffers.get(bufferName) instanceof AtomicCounterBuffer ret) ? ret : null;
    }

    @Override
    public void bindSsboBuffers() {
        buffers.values().stream()
                .filter(buffer -> buffer instanceof SsboBuffer)
                .map(SsboBuffer.class::cast)
                .forEach(SsboBuffer::bind);
    }

    @Override
    public boolean bindBuffer(BufferDef buffer) {
        if (!buffers.containsKey(buffer)) {
            return false;
        } else {
            buffers.get(buffer).bind();
            return true;
        }
    }

    @Override
    public void destroyBuffers() {
        int[] names = buffers.values().stream()
                .map(GlslBuffer::getGlName)
                .mapToInt(Integer::intValue)
                .toArray();
        glContext.getGL().getGL4().glDeleteBuffers(names.length, names, 0);
        buffers.clear();
    }
}
