package cz.fidentis.analyst.glsl.buffers.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.PixelUnpackBuffer;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static com.jogamp.opengl.GL.GL_WRITE_ONLY;
import static com.jogamp.opengl.GL2ES3.GL_PIXEL_UNPACK_BUFFER;

/**
 * A buffer for {@code GL_PIXEL_UNPACK_BUFFER} target.
 *
 * @author Radek Oslejsek
 */
public class PixelUnpackBufferImpl extends AbstractBuffer implements PixelUnpackBuffer {

    private final int usage;
    private final long itemSize;

    /**
     * Constructor.
     *
     * @param bufferName buffer name
     * @param glContext  OpenGL context
     * @param usage      OpenGL usage type
     * @param itemSize   The size of buffer's items
     */
    public PixelUnpackBufferImpl(BufferDef bufferName, GLContext glContext, int usage, long itemSize) {
        super(bufferName, glContext, GL_PIXEL_UNPACK_BUFFER);

        this.usage = usage;
        this.itemSize = itemSize;

        int[] bufferNames = new int[1];
        glContext.getGL().getGL4().glGenBuffers(1, bufferNames, 0);
        setGlName(bufferNames[0]);
    }

    @Override
    public void bind() {
        glContext().getGL().glBindBuffer(getGlTarget(), getGlName());
    }

    @Override
    public int getUsage() {
        return usage;
    }

    @Override
    public long getItemSize() {
        return itemSize;
    }

    @Override
    public void allocate(long items, boolean reset) {
        bind();
        glContext().getGL().glBufferData(getGlTarget(), items * getItemSize(), null, getUsage());

        if (reset) {
            ByteBuffer data = glContext().getGL().glMapBuffer(getGlTarget(), GL_WRITE_ONLY);
            byte[] aData = new byte[data.capacity()];
            Arrays.fill(aData, (byte) 0x00);
            data.rewind();
            data.put(aData);
            glContext().getGL().glUnmapBuffer(getGlTarget());
        }
    }


}
