package cz.fidentis.analyst.glsl.buffers.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.ImageBuffer;

import static com.jogamp.opengl.GL.*;

/**
 * A texture buffer storing an image.
 * This buffer is used for the {@code GL_TEXTURE_2D} OpenGL target.
 *
 * @author Radek Oslejsek
 * @author Ondrej Simecek
 */
public class ImageBufferImpl extends AbstractBuffer implements ImageBuffer {

    private final int internalFormat;
    private final int access;

    private int width = -1;
    private int height = -1;
    private int format;
    private int type;


    /**
     * Constructor.
     *
     * @param bufferName     Buffer name
     * @param glContext      OpenGL context
     * @param internalFormat The {@code format} parameter of the {@code glBindImageTexture} function
     *                       and {@code internalFormat} of the {@code glTexBuffer} and {@code glTexImage2D} functions
     * @param access         The {@code access} parameter of the {@code glBindImageTexture} function
     */
    public ImageBufferImpl(BufferDef bufferName, GLContext glContext, int internalFormat, int access) {
        super(bufferName, glContext, GL_TEXTURE_2D);

        this.internalFormat = internalFormat;
        this.access = access;

        int[] bufferNames = new int[1];
        glContext.getGL().glGenTextures(1, bufferNames, 0);
        setGlName(bufferNames[0]);
    }

    @Override
    public void allocate(int width, int height, int format, int type, int filter) {
        this.width = width;
        this.height = height;
        this.format = format;
        this.type = type;

        glContext().getGL().glBindTexture(getGlTarget(), getGlName());
        glContext().getGL().glTexParameteri(getGlTarget(), GL_TEXTURE_MIN_FILTER, filter);
        glContext().getGL().glTexParameteri(getGlTarget(), GL_TEXTURE_MAG_FILTER, filter);

        glContext().getGL().glTexImage2D(getGlTarget(), 0, internalFormat, width, height, 0, format, type, null);
    }

    @Override
    public void bind() {
        glContext().getGL().glBindTexture(getGlTarget(), getGlName());
        glContext().getGL().getGL2().glBindImageTexture(getBindingIndex(), getGlName(),
                0, true, 0, access, internalFormat);
    }

    @Override
    public void clear(long val) {
        if (width >= 0 && height >= 0) {
            glContext().getGL().glTexSubImage2D(getGlTarget(), 0, 0, 0, width, height, format, type, val);
        }
    }
}
