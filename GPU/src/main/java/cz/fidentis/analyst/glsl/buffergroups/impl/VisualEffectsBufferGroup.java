package cz.fidentis.analyst.glsl.buffergroups.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.impl.*;

import java.util.List;

import static com.jogamp.opengl.GL.GL_WRITE_ONLY;
import static com.jogamp.opengl.GL2ES3.*;

/**
 * This class implements a collection of buffers used for 3D visual effect used by {@code ShadersManager}.
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 */
public class VisualEffectsBufferGroup extends AbstractBufferGroup {

    /**
     * Constructor.
     *
     * @param glContext OpenGL context.
     */
    public VisualEffectsBufferGroup(GLContext glContext) {
        super(glContext, List.of(
                new SsboBufferImpl(BufferDef.VIS_DIFF_GLYPH_LOCATIONS_SSBO_BUFFER, glContext, GL_DYNAMIC_COPY, 12 * 4),
                new ImageBufferImpl(BufferDef.VIS_DIFF_DEPTH_TEXTURE_BUFFER, glContext, GL_DEPTH_COMPONENT, GL_READ_WRITE),
                new FrameBufferImpl(BufferDef.VIS_DIFF_DEPTH_FRAME_BUFFER, glContext),
                new ImageBufferImpl(BufferDef.VIS_DIFF_HEAD_POINTER_TEXTURE_BUFFER, glContext, GL_R32UI, GL_READ_WRITE),
                new TextureBufferImpl(BufferDef.VIS_DIFF_FRAGMENT_LIST_TEXTURE_BUFFER, glContext, GL_RGBA32UI, GL_WRITE_ONLY),
                new SsboBufferImpl(BufferDef.VIS_DIFF_FRAGMENT_LIST_SSBO_BUFFER, glContext, GL_DYNAMIC_COPY, 2 * 16),
                new AtomicCounterBufferImpl(BufferDef.VIS_DIFF_ATOMIC_COUNTER_SSBO_BUFFER, glContext, GL_DYNAMIC_COPY),
                new PixelUnpackBufferImpl(BufferDef.VIS_DIFF_HEAD_POINTER_INIT_PU_BUFFER, glContext, GL_STATIC_DRAW, 4)
        ));
    }
}
