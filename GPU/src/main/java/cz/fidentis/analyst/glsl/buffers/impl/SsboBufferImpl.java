package cz.fidentis.analyst.glsl.buffers.impl;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.SsboBuffer;

import java.nio.ByteBuffer;
import java.util.function.Consumer;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL2.GL_RED;
import static com.jogamp.opengl.GL3.GL_SHADER_STORAGE_BUFFER;

/**
 * Shader storage buffer objects (SSBO) for the {@code GL_SHADER_STORAGE_BUFFER} target.
 * In the buffer definition enum, you must specify on which SSBO binding index should your buffer be bound to when used. (this is the
 * same binding index as defined in your shader source code)
 *
 * @author Pavol Kycina
 */
public class SsboBufferImpl extends AbstractBuffer implements SsboBuffer {

    private final int usage;
    private final long itemSize;

    /**
     * Constructor.
     *
     * @param bufferName buffer name
     * @param glContext OpenGL context
     * @param usage OpenGL usage type
     * @param itemSize The size of buffer's items
     */
    public SsboBufferImpl(BufferDef bufferName, GLContext glContext, int usage, long itemSize) {
        this(bufferName, glContext, usage, itemSize, GL_SHADER_STORAGE_BUFFER);
    }

    /**
     * Constructor.
     *
     * @param bufferName buffer name
     * @param glContext OpenGL context
     * @param usage OpenGL usage type
     * @param itemSize The size of buffer's items
     * @param target OpenGL target
     */
    protected SsboBufferImpl(BufferDef bufferName, GLContext glContext, int usage, long itemSize, int target) {
        super(bufferName, glContext, target);

        this.usage = usage;
        this.itemSize = itemSize;

        int[] bufferNames = new int[1];
        glContext.getGL().getGL4().glCreateBuffers(1, bufferNames, 0);
        setGlName(bufferNames[0]);
    }

    @Override
    public void bind() {
        glContext().getGL().getGL4().glBindBufferBase(getGlTarget(), getBindingIndex(), getGlName()); //todo: glbindbuffersbsae?
    }

    @Override public void allocate(long items, boolean reset) {
        allocate(items, 0, reset);
    }

    @Override
    public void allocate(long items, long extraSpace, boolean reset) {
        GL4 gl = glContext().getGL().getGL4();

        // allocates physical memory on GPU
        gl.glNamedBufferData(getGlName(), items * getItemSize() + extraSpace, null, getUsage());

        if (reset) {
            gl.glClearNamedBufferData(getGlName(), GL_R32F, GL_RED, GL_FLOAT, null); // zero buffer
        }
    }

    @Override
    public void writeItemsTo(long offset, long numItems, Consumer<ByteBuffer> filler) {
        GL4 gl = glContext().getGL().getGL4();

        ByteBuffer buffer = gl.glMapNamedBufferRange(getGlName(), offset * getItemSize(), numItems * getItemSize(), GL_MAP_WRITE_BIT);
        filler.accept(buffer);
        gl.glUnmapNamedBuffer(getGlName());
    }

    @Override
    public void writeBytesTo(long offset, long size, Consumer<ByteBuffer> filler) {
        GL4 gl = glContext().getGL().getGL4();

        ByteBuffer buffer = gl.glMapNamedBufferRange(getGlName(), offset, size, GL_MAP_WRITE_BIT);
        filler.accept(buffer);
        gl.glUnmapNamedBuffer(getGlName());
    }

    @Override
    public void readFrom(long offset, long items, Consumer<ByteBuffer> reader) {
        GL4 gl = glContext().getGL().getGL4();

        ByteBuffer dataToRead = gl.glMapNamedBufferRange(getGlName(), offset * getItemSize(), items * getItemSize(), GL_MAP_READ_BIT);
        reader.accept(dataToRead);
        gl.glUnmapNamedBuffer(getGlName());
    }

    @Override
    public int getUsage() {
        return usage;
    }

    @Override
    public long getItemSize() {
        return itemSize;
    }
}
