/**
 * Services for GLSL shaders, programs, and buffers.
 */
package cz.fidentis.analyst.glsl;
