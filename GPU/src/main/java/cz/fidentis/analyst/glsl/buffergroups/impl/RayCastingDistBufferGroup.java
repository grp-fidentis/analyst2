package cz.fidentis.analyst.glsl.buffergroups.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.impl.SsboBufferImpl;

import java.util.List;

import static com.jogamp.opengl.GL.GL_STATIC_DRAW;
import static com.jogamp.opengl.GL2ES3.GL_DYNAMIC_COPY;
import static com.jogamp.opengl.GL2ES3.GL_STATIC_COPY;
import static com.jogamp.opengl.GL2ES3.GL_STATIC_READ;

/**
 * This class implements a collection of buffers used by the {@code MeshDistanceRCGPU} visitor.
 *
 * @author Pavol Kycina
 */
public class RayCastingDistBufferGroup extends AbstractBufferGroup {

    /**
     * Constructor.
     *
     * @param glContext OpenGL context.
     */
    public RayCastingDistBufferGroup(GLContext glContext) {
        super(glContext, List.of(
                new SsboBufferImpl(BufferDef.RC_DIST_RAYS, glContext, GL_STATIC_DRAW, 4 * 4 * 2),
                new SsboBufferImpl(BufferDef.RC_DIST_VERTICES, glContext, GL_STATIC_DRAW, 4 * 4 * 2),
                new SsboBufferImpl(BufferDef.RC_DIST_TRIANGLES, glContext, GL_STATIC_DRAW, 4 * 4),
                new SsboBufferImpl(BufferDef.RC_DIST_SORTED_TRIANGLES, glContext, GL_STATIC_COPY, 4),
                new SsboBufferImpl(BufferDef.RC_DIST_CELL_BOUNDS, glContext, GL_STATIC_COPY, 4),
                new SsboBufferImpl(BufferDef.RC_DIST_GRID_INFO, glContext, GL_STATIC_READ, 8 * 4),
                new SsboBufferImpl(BufferDef.RC_DIST_DISTANCES, glContext, GL_STATIC_READ, 4 * 4),
                new SsboBufferImpl(BufferDef.RC_DIST_CELL_ELEMENTS, glContext, GL_DYNAMIC_COPY, 4),
                new SsboBufferImpl(BufferDef.RC_DIST_TRIANGLES_REDUCTION, glContext, GL_DYNAMIC_COPY, 8 * 4)
        ));
    }
}
