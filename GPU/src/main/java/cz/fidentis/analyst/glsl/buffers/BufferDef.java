package cz.fidentis.analyst.glsl.buffers;

/**
 * Every buffer must have defined an uniques name here.
 * Moreover, buffers must have defined a binding index, which is used in the corresponding GLGL programs/shaders.
 * Use -1 as binding index for buffers that do not use it.
 *
 * @author Pavol Kycina
 */
public enum BufferDef {

    /* *********************************************************************
     * SSBO buffers used by the ray-casting-based computation mesh distance
     ***********************************************************************/
    RC_DIST_RAYS(0),
    RC_DIST_VERTICES(1),
    RC_DIST_TRIANGLES(2),
    RC_DIST_SORTED_TRIANGLES(3),
    RC_DIST_CELL_BOUNDS(4),
    RC_DIST_GRID_INFO(5),
    RC_DIST_DISTANCES(6),
    RC_DIST_CELL_ELEMENTS(7),
    RC_DIST_TRIANGLES_REDUCTION(8),


    /* *********************************************************************************
     * Buffers used for visual effect (visual difference) in the face-to-face analysis.
     * Binding indices across the same buffer type, e.g., {@code _FRAME_BUFFER}
     * must be unique.
     ***********************************************************************************/

    /**
     * Texture object, which has data attached to it from the {@code VIS_DIFF_DEPTH_FRAME_BUFFER}.
     */
    VIS_DIFF_DEPTH_TEXTURE_BUFFER(-1),

    /**
     * Frame-buffer, which stores the distance of objects from the light source.
     */
    VIS_DIFF_DEPTH_FRAME_BUFFER(0),

    /**
     * Texture of pointers pointing to the first item of a list in the {@code VIS_DIFF_FRAGMENT_LIST_TEXTURE_BUFFER}.
     * It is filled in the ShadingFS shader and used in the ColorMixing and Contours shaders.
     */
    VIS_DIFF_HEAD_POINTER_TEXTURE_BUFFER(1),

    /**
     * Texture object, which has data attached to it from the {@code VIS_DIFF_FRAGMENT_LIST_SSBO_BUFFER}.
     */
    VIS_DIFF_FRAGMENT_LIST_TEXTURE_BUFFER(2),

    /**
     * Locations of glyphs with computed curvatures
     */
    VIS_DIFF_GLYPH_LOCATIONS_SSBO_BUFFER(4),

    /**
     * Linked lists of fragments. Each list consists of fragments with the same screen space position. It is filled with
     * data(shaded fragment + extra information about the fragment) in the ShadingFS shader and used in the ColorMixing
     * and Contours shaders.
     */
    VIS_DIFF_FRAGMENT_LIST_SSBO_BUFFER(-1),

    /**
     * Counter that tracks the number of items stored in the fragment list to avoid concurrent writing. It is
     * incremented every time a fragment is processed in the ShadingFS shader.
     */
    VIS_DIFF_ATOMIC_COUNTER_SSBO_BUFFER(0),

    /**
     * Buffer with initial values the {@code VIS_DIFF_HEAD_POINTER_TEXTURE_BUFFER} resets to at the beginning of
     * each rendering pass.
     */
    VIS_DIFF_HEAD_POINTER_INIT_PU_BUFFER(-1);

    /* ****************************************************************
     * END OF DEFINITIONS
     ******************************************************************/

    private final int bindingIndex;

    BufferDef(int bindingIndex) {
        this.bindingIndex = bindingIndex;
    }

    public int getBindingIndex() {
        return bindingIndex;
    }

}
