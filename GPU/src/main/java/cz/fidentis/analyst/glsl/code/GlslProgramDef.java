package cz.fidentis.analyst.glsl.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Defined GLSL programs, i.e., their dependencies on defined shaders and optional unified variables
 * used inside the program code.
 *
 * @author Radek Oslejsek
 * @author Pavol Kycina
 */
public enum GlslProgramDef {

    /* *****************************************************************************
     * GLSL PROGRAMS FOR VISUAL EFFECTS USED IN FACE-TO-FACE SCENE
     *******************************************************************************/

    /**
     * Needed for the view-dependent (correct) transparency, "fog", and glyphs shown in the 3D scene
     */
    SHADING_GLSL_PROGRAM(
            List.of(GlslShaderDef.SHADING_VS, GlslShaderDef.SHADING_FS),
            List.of("modelNumber", "isFace", "materialMode", "lightModelViewMatrix", "lightProjectionMatrix", "renderGlyphs")
    ),

    /**
     * Needed for the view-dependent (correct) transparency and the "fog" in the 3D scene
     */
    COLOR_MIXING_GLSL_PROGRAM(
            List.of(GlslShaderDef.COLOR_MIXING_VS, GlslShaderDef.COLOR_MIXING_FS),
            List.of("innerSurfaceSolid", "minZ", "maxZ", "fogVersion", "fogColor")
    ),

    /**
     * Visualization of intersection contours in the 3D scene
     */
    CONTOURS_GLSL_PROGRAM(
            List.of(GlslShaderDef.COLOR_MIXING_VS, GlslShaderDef.CONTOUR_FS)
    ),

    /**
     * Used by the visualization of glyphs (casting shadows)
     */
    SHADOW_MAP_GLSL_PROGRAM(
            List.of(GlslShaderDef.SHADOW_MAP_VS, GlslShaderDef.SHADOW_MAP_FS)
    ),

    /* *********************************************************************************
     * GLSL PROGRAMS FOR THE COMPUTATION OF RAY-CASTING-BASED MESH DISTANCE MEASUREMENT
     ***********************************************************************************/

    /**
     * Used by uniform grid construction, precomputes the triangle's perimeter and triangle's bounding box.
     */
    TRIANGLE_STATS_GLSL_PROGRAM(List.of(GlslShaderDef.TRIANGLE_STATS_CS)),

    /**
     * Used by uniform grid construction, calculates the grid's bounding box and estimates the proper cell size.
     */
    GRID_PARAMETERS_GLSL_PROGRAM(List.of(GlslShaderDef.GRID_PARAMETERS_CS), List.of("level", "invocations")),

    /**
     * Used by uniform grid construction, precomputes how much memory we need to allocate for which cell.
     */
    GRID_CELL_OVERLAP_COUNT_GLSL_PROGRAM(List.of(GlslShaderDef.GRID_CELL_OVERLAP_COUNT_CS)),

    /**
     * Used by uniform grid construction, creates an array (cell bounds) which is used to index cell's triangles.
     */
    GRID_CELL_BOUNDS_GLSL_PROGRAM(List.of(GlslShaderDef.GRID_CELL_BOUNDS_CS), List.of("level", "upsweep")),

    /**
     * Used by uniform grid construction, creates an array with triangles to which cell bounds point.
     */
    GRID_SORTED_TRIANGLES_GLSL_PROGRAM(List.of(GlslShaderDef.GRID_SORTED_TRIANGLES_CS)),

    /**
     * Ray-casting distance measurement between two meshes using already created uniform grid.
     */
    DISTANCE_RAY_CASTING_GLSL_PROGRAM(List.of(GlslShaderDef.DISTANCE_RAY_CASTING_CS), List.of("relative_distance_multiplier"));

    /* *****************************************************************************
     * END OF DEFINITIONS
     *******************************************************************************/


    private final List<GlslShaderDef> shaders = new ArrayList<>();
    private final List<String> uniformLocations = new ArrayList<>();

    GlslProgramDef(List<GlslShaderDef> shaders, List<String> uniformLocations) {
        this.shaders.addAll(shaders);
        if (uniformLocations != null) {
            this.uniformLocations.addAll(uniformLocations);
        }
    }

    GlslProgramDef(List<GlslShaderDef> shaders) {
        this(shaders, null);
    }

    public List<GlslShaderDef> getShaders() {
        return Collections.unmodifiableList(shaders);
    }

    public List<String> getUniformLocations() {
        return Collections.unmodifiableList(uniformLocations);
    }
}
