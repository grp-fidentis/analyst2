package cz.fidentis.analyst.glsl.code.impl;

import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import cz.fidentis.analyst.glsl.code.GlslProgramDef;
import cz.fidentis.analyst.glsl.code.GlslShaderDef;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This object deals with shaders and programs of a single OpenGL context.
 * Compiled shaders can be used by multiple programs of the context and vice versa.
 * The compilation of shaders and programs is lazy (only after the first request).
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 * @author Pavol Kycina
 */
public class ProgramsManager {

    /**
     * OpenGL context of compiled programs and shaders
     */
    private final GLContext glContext;

    /**
     * The "list" of already compiled shaders
     */
    private final Map<GlslShaderDef, CompiledShader> compiledShaders = new HashMap<>();

    /**
     * The "list" of already compiled programs
     */
    private final Map<GlslProgramDef, CompiledProgram> compiledPrograms = new HashMap<>();

    /**
     * The list of linked program-shader pairs
     */
    private final List<Linkage> pairs = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL2.
     */
    public ProgramsManager(GLContext glContext) {
        this.glContext = Objects.requireNonNull(glContext);
    }

    /**
     * Lazy compilation and re-used of GLSL programs.
     *
     * @param programSpec Which GLSL program to compile/return
     * @return The reference object to the compiled GLSL program
     * @throws GLException if the compilation fails
     */
    public CompiledProgram getOrCompile(GlslProgramDef programSpec) throws GLException {
        if (compiledPrograms.containsKey(programSpec)) {
            return compiledPrograms.get(programSpec);
        }

        compileMissingShaders(programSpec.getShaders());

        CompiledProgram program = new CompiledProgram(glContext, programSpec);
        compiledPrograms.put(programSpec, program);

        programSpec.getShaders().forEach(ps -> {
            CompiledShader ch = compiledShaders.get(ps);
            program.addShader(ch);
            pairs.add(new Linkage(program, ch));
        });
        program.compile();

        return program;
    }

    protected void compileMissingShaders(Collection<GlslShaderDef> shaderSpecs) {
        Set<GlslShaderDef> missing = new HashSet<>(shaderSpecs);
        missing.removeAll(compiledShaders.keySet());
        missing.forEach(sh -> compiledShaders.put(sh, new CompiledShader(glContext, sh)));
    }

    /**
     * Removes the program and its shaders from this manager and from GPU memory.
     *
     * @param programDef Which GLSL program to release
     * @param preserveShaders If {@code true}, then linked shaders remain allocated on GPU
     *                        even if they are not used by any program.
     *                        If {@code false}, then such shaders are released from the GPU memory
     */
    public void release(GlslProgramDef programDef, boolean preserveShaders) {
        if (!compiledPrograms.containsKey(programDef)) {
            return;
        }

        CompiledProgram program = compiledPrograms.get(programDef);
        compiledPrograms.remove(programDef);
        program.release();

        Set<Linkage> pairsToBeRemoved = pairs.stream()
                .filter(pair -> pair.program.equals(program))
                .collect(Collectors.toSet());
        pairs.removeAll(pairsToBeRemoved);

        if (!preserveShaders) {
            var usedShaders = pairs.stream()
                    .map(pair -> pair.shader.getShaderDef())
                    .collect(Collectors.toSet());
            var unusedShaders = new HashSet<>(compiledShaders.keySet());
            unusedShaders.removeAll(usedShaders);
            unusedShaders.forEach(shader -> compiledShaders.get(shader).release());
            compiledShaders.keySet().removeAll(unusedShaders);
        }
    }

    /**
     * Removes all programs, asks for releasing GPU memory.
     *
     * @param preserveShaders If {@code true}, then linked shaders remain allocated on GPU.
     *                        If {@code false}, then all shaders are released from the GPU memory.
     */
    public void releaseAll(boolean preserveShaders) {
        compiledPrograms.keySet().forEach(program -> release(program, true));
        if (!preserveShaders) {
            compiledShaders.values().forEach(CompiledShader::release);
            compiledShaders.clear();
        }
    }

    /**
     * Returns the OpenGL context
     * @return the OpenGL context
     */
    public GLContext getGlContext() {
        return glContext;
    }

    /**
     * Helper class storing the linked pair: program shader.
     * @param program compiled program
     * @param shader compiled shader
     * @author Radek Oslejsek
     */
    private record Linkage(CompiledProgram program, CompiledShader shader) {}
}
