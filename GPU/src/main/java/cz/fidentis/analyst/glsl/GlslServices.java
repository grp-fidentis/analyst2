package cz.fidentis.analyst.glsl;

import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffergroups.BufferGroupDef;
import cz.fidentis.analyst.glsl.buffergroups.GlslBufferGroup;
import cz.fidentis.analyst.glsl.buffergroups.impl.RayCastingDistBufferGroup;
import cz.fidentis.analyst.glsl.buffergroups.impl.VisualEffectsBufferGroup;
import cz.fidentis.analyst.glsl.code.GlslProgram;
import cz.fidentis.analyst.glsl.code.GlslProgramDef;
import cz.fidentis.analyst.glsl.code.impl.CompiledProgram;
import cz.fidentis.analyst.glsl.code.impl.SingletonGpuManager;

/**
 * Stateless services for compiling vertex and fragment shaders and composing them into GLSL programs.
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 * @author Pavol Kycina
 */
public interface GlslServices {

    /**
     * Compiles the GLSL program, re-uses already compiled programs.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL2.
     * @param programDef GLSL program definition
     * @return The reference object to the compiled GLSL program
     */
    static GlslProgram useProgram(GLContext glContext, GlslProgramDef programDef) {
        CompiledProgram program = SingletonGpuManager.getInstance().getOrCompile(glContext, programDef);
        glContext.getGL().getGL2().glUseProgram(program.getProgramId());
        return program;
    }

    /**
     * Executes the GLSL program <b>previously set by the {@code useProgram()}</b> service.
     * Do not forget to bind necessary buffers before running GLSL program -
     * see {@link #registerBuffers}, {@link GlslBufferGroup#bindBuffer} and {@link GlslBufferGroup#bindSsboBuffers}.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL4.
     * @param numGroupsX The number of work groups to be launched in the X dimension.
     * @param numGroupsY The number of work groups to be launched in the Y dimension.
     * @param numGroupsZ The number of work groups to be launched in the Z dimension.
     */
    static void runProgram(GLContext glContext, int numGroupsX, int numGroupsY, int numGroupsZ) {
        glContext.getGL().getGL4().glDispatchCompute(numGroupsX, numGroupsY, numGroupsZ);
        glContext.getGL().getGL4().glMemoryBarrier(GL4.GL_SHADER_STORAGE_BARRIER_BIT);
    }

    /**
     * Registers a bunch of buffers required by a GLSL program(s) in given GL context.
     * Registered buffers are unbound and do not have allocated space.
     * You must destroy these buffers before they go out of scope, or you create a memory leak in GL context -
     * see {@link GlslBufferGroup#destroyBuffers}.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL4.
     * @return the group of initialized buffers
     */
    static GlslBufferGroup registerBuffers(GLContext glContext, BufferGroupDef bufferGroupDef) {
        return switch (bufferGroupDef) {
            case RAY_CASTING_DISTANCE -> new RayCastingDistBufferGroup(glContext);
            case VISUAL_EFFECTS -> new VisualEffectsBufferGroup(glContext);
        };
    }

    /**
     * Should be called before the OpenGL context is erased.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL2.
     */
    static void release(GLContext glContext) {
        SingletonGpuManager.getInstance().release(glContext);
    }
}
