package cz.fidentis.analyst.glsl.buffers;

/**
 * A buffer for {@code GL_ATOMIC_COUNTER} target.
 *
 * @author Radek Oslejsek
 */

public interface AtomicCounterBuffer extends SsboBuffer {

    /**
     * Resets the counter to zero.
     */
    void reset();

    /**
     * Allocate counter of the size of 4 bytes (int).
     */
    void allocate();
}
