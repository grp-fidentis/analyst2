package cz.fidentis.analyst.glsl.code;

import cz.fidentis.analyst.glsl.buffergroups.GlslBufferGroup;

/**
 * A GLSL program, which links multiple shaders together, for example vertex shader and a fragment shader.
 * Update the {@link GlslProgramDef} and {@link GlslShaderDef} enums whenever you want to add a new GLSL program
 * and its shaders. Update the {@link GlslBufferGroup} enum to define their buffers.
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 * @author Pavol Kycina
 */
public interface GlslProgram {

    /**
     * Returns the ID of the program in the OpenGL context
     * @return program's ID
     */
    int getProgramId();

    /**
     * Returns the GLSL program definition which was used to create this program.
     * @return program's definition
     */
    GlslProgramDef getProgramDef();

    /**
     * Returns ID of the given named uniform location in the OpenGL context.
     * @param key name
     * @return ID of the given named uniform location in the OpenGL context or -1
     */
    int getUniformLocation(String key);

    /**
     * Sets the value to a uniform {@code Matrix4fv} variable defined in the compiled program.
     *
     * @param varName variable name (see {@link GlslProgramDef})
     * @param value Value to be assigned
     * @throws UnsupportedOperationException if the variable does nto exist or the value is nto supported.
     */
    void setMat4fVar(String varName, float[] value);

    /**
     * Sets the value to a uniform {@code 1f} or {@code 1i} variable defined in the compiled program.
     *
     * @param varName variable name (see {@link GlslProgramDef})
     * @param value Value to be assigned. Must be either float or integer
     * @throws UnsupportedOperationException if the variable does nto exist or the value is nto supported.
     */
    void setNumberVar(String varName, Number value);

    /**
     * Sets the value to a uniform {@code 4f} variable defined in the compiled program.
     *
     * @param varName variable name (see {@link GlslProgramDef})
     * @param v1 First value to be assigned
     * @param v2 Second value to be assigned
     * @param v3 Third value to be assigned
     * @param v4 Fourth value to be assigned
     * @throws UnsupportedOperationException if the variable does nto exist or the value is nto supported.
     */
    void setVec4fVar(String varName, float v1, float v2, float v3, float v4);
}
