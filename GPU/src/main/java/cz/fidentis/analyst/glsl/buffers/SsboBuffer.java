package cz.fidentis.analyst.glsl.buffers;

import java.nio.ByteBuffer;
import java.util.function.Consumer;

/**
 * Shader Storage Buffer Object (SSBO) buffer, i.e., the {@code GL_SHADER_STORAGE_BUFFER} OpenGL target.
 *
 * @author Pavol Kycina
 * @author Radek Oslesek
 */
public interface SsboBuffer extends GlslBuffer {

    /**
     * Returns buffer's usage type
     * @return usage type
     */
    int getUsage();

    /**
     * SSBO buffers typically store some amount of items of the same type (integers, floats, etc.).
     * This method returns the size (number of bytes) of a single item.
     *
     * @return items' size
     */
    long getItemSize();

    /**
     * Allocates memory for this SSBO buffer on GPU. Can be used multiple times to resize the buffer.
     * You can allocate space for unbound buffer and vice versa.
     * Resulting buffer will occupy {@code items * }{@link #getItemSize} bytes.
     *
     * @param items how many items are going to be stored in the buffer
     * @param reset if {@code true}, then the buffer is filled with zeros
     */
    void allocate(long items, boolean reset);

    /**
     * Allocates memory for this SSBO buffer on GPU. Can be used multiple times to resize the buffer.
     * You can allocate space for unbound buffer and vice versa.
     * Resulting buffer will occpy {@code items * }{@link #getItemSize} {@code + extraSpace} bytes.
     *
     * @param items how many items are going to be stored in the buffer. Can be zero.
     * @param extraSpace extra space. Can be zero.
     * @param reset if {@code true}, then the buffer is filled with zeros
     */
    void allocate(long items, long extraSpace, boolean reset);

    /**
     * Writes data to the buffer. The size of written data is {@code numItemes * }{@link #getItemSize}.
     * Buffer must be already allocated and have sufficient size, otherwise GL error is thrown.
     *
     * @param offset numItems offset ({@code numItems} to be skipped). Can be zero
     * @param numItems how many numItems we want to write
     * @param filler filler that writes data to the buffer
     */
    void writeItemsTo(long offset, long numItems, Consumer<ByteBuffer> filler);

    /**
     * Writes data to the buffer.
     * Buffer must be already allocated and have sufficient size, otherwise GL error is thrown.
     *
     * @param offset number of bytes to be skipped. Can be zero
     * @param size how many bytes to write
     * @param filler filler that writes data to the buffer
     */
    void writeBytesTo(long offset, long size, Consumer<ByteBuffer> filler);

    /**
     * Reads data from the buffer.
     * Buffer must be already allocated and have sufficient size, otherwise GL error is thrown.
     *
     * @param offset items offset (items to be skipped). Can be zero
     * @param items how many items we want to read
     * @param reader reader that reads the data from the buffer
     */
    void readFrom(long offset, long items, Consumer<ByteBuffer> reader);

}
