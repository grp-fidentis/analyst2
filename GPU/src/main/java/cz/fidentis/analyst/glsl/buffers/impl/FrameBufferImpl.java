package cz.fidentis.analyst.glsl.buffers.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.FrameBuffer;
import cz.fidentis.analyst.glsl.buffers.ImageBuffer;

import static com.jogamp.opengl.GL.*;

/**
 * Frame buffer used for the {@code GL_FRAMEBUFFER} OpenGL target.
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 */
public class FrameBufferImpl extends AbstractBuffer implements FrameBuffer {

    /**
     * Constructor.
     *
     * @param bufferDef Buffer name
     * @param glContext  OpenGL context
     */
    public FrameBufferImpl(BufferDef bufferDef, GLContext glContext) {
        super(bufferDef, glContext, GL_FRAMEBUFFER);

        int[] bufferNames = new int[1];
        glContext.getGL().glGenFramebuffers(1, bufferNames, 0);
        setGlName(bufferNames[0]);
    }

    @Override
    public void allocate(ImageBuffer textureBuffer) {
        bind();
        glContext().getGL().glFramebufferTexture2D(getGlTarget(), GL_DEPTH_ATTACHMENT,
                textureBuffer.getGlTarget(), textureBuffer.getGlName(), 0);
    }

    @Override
    public void bind() {
        glContext().getGL().glBindFramebuffer(getGlTarget(), getGlName());
        glContext().getGL().getGL2().glDrawBuffer(GL_NONE);
        glContext().getGL().getGL2().glReadBuffer(GL_NONE);
    }
}
