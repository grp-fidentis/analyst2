package cz.fidentis.analyst.glsl.buffers;

/**
 * A buffer for {@code GL_PIXEL_UNPACK_BUFFER} target.
 *
 * @author Pavol Kycina
 * @author Radek Oslesek
 */
public interface PixelUnpackBuffer extends  GlslBuffer {

    /**
     * Returns buffer usage type
     * @return usage type
     */
    int getUsage();

    /**
     * This buffer stores some amount of items of the same type (integers, floats, etc.).
     * Therefore, this method returns the size (number of bytes) of a single item.
     *
     * @return items' size
     */
    long getItemSize();

    /**
     * Allocates memory for this buffer on GPU. Can be used multiple times to resize the buffer.
     * Resulting buffer will occupy {@code items * }{@link #getItemSize} bytes.
     *
     * @param items how many items are going to be in the buffer (resulting buffer will have {@code items * ssboDef.getItemSize()} bytes)
     * @param reset if {@code true}, then fills the buffer by zeros
     */
    void allocate(long items, boolean reset);

}
