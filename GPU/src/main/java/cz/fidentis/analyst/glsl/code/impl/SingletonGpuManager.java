package cz.fidentis.analyst.glsl.code.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.code.GlslProgramDef;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A singleton object managing with OpenGL contexts and their GLSL programs, shaders and buffers.
 *
 * @author Radek Oslejsek
 */
public final class SingletonGpuManager {
    private final Map<GLContext, ProgramsManager> glContexts = new HashMap<>();

    /**
     * Compiles the GLSL program, re-uses already compiled programs.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL2.
     * @param program GLSL program
     * @return The reference object to the compiled GLSL program
     */
    public CompiledProgram getOrCompile(GLContext glContext, GlslProgramDef program) {
        glContexts.putIfAbsent(
                Objects.requireNonNull(glContext),
                new ProgramsManager(glContext)
        );
        return glContexts.get(glContext).getOrCompile(program);
    }

    /**
     * Should be called before the OpenGL context is erased.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL2.
     */
    public void release(GLContext glContext) {
        ProgramsManager mngr = glContexts.getOrDefault(glContext, null);
        if (mngr != null) {
            mngr.releaseAll(false);
        }
    }

    /************************************************
     * Thread-safe implementation of the Singleton:
     ************************************************/

    private static volatile SingletonGpuManager instance;
    private static final Object MUTEX = new Object();

    private SingletonGpuManager() {
    }

    /**
     * Returns the single instance
     * @return the single instance
     */
    public static SingletonGpuManager getInstance() {
        SingletonGpuManager localInstance = instance;
        if (localInstance == null) {
            synchronized (MUTEX) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SingletonGpuManager();
                }
            }
        }
        return localInstance;
    }
}
