package cz.fidentis.analyst.glsl.buffers;

/**
 * Frame buffer used for the {@code GL_FRAMEBUFFER} OpenGL target.
 *
 * @author Radek Oslejsek
 */
public interface FrameBuffer extends GlslBuffer {

    /**
     * Allocates and binds the buffer (links it to the {@code GL_FRAMEBUFFER} OpenGL target).
     *
     * @param depthTextureBuffer Existing texture buffer whose image is to be attached
     */
    void allocate(ImageBuffer depthTextureBuffer);

}
