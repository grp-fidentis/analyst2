/**
 * Buffers defined for GLSL shaders and programs.
 */
package cz.fidentis.analyst.glsl.buffers;
