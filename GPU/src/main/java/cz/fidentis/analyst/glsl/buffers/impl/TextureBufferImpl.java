package cz.fidentis.analyst.glsl.buffers.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.TextureBuffer;

import static com.jogamp.opengl.GL.GL_TEXTURE_MAG_FILTER;
import static com.jogamp.opengl.GL.GL_TEXTURE_MIN_FILTER;
import static com.jogamp.opengl.GL2ES3.GL_TEXTURE_BUFFER;

/**
 * A texture buffer backed by another buffer.
 * This buffer is used for the {@code GL_TEXTURE_BUFFER} OpenGL target.
 *
 * @author Rdek oslejsek
 */
public class TextureBufferImpl extends AbstractBuffer implements TextureBuffer {

    private final int internalFormat;
    private final int access;

    /**
     * Constructor.
     *
     * @param bufferName     Buffer name
     * @param glContext      OpenGL context
     * @param internalFormat The {@code format} parameter of the {@code glBindImageTexture} function
     *                       and {@code internalFormat} of the {@code glTexBuffer} and {@code glTexImage2D} functions
     * @param access         The {@code access} parameter of the {@code glBindImageTexture} function
     */
    public TextureBufferImpl(BufferDef bufferName, GLContext glContext, int internalFormat, int access) {
        super(bufferName, glContext, GL_TEXTURE_BUFFER);

        this.internalFormat = internalFormat;
        this.access = access;

        int[] bufferNames = new int[1];
        glContext.getGL().glGenTextures(1, bufferNames, 0);
        setGlName(bufferNames[0]);
    }

    @Override
    public void allocate(int filter, int dataObject) {
        glContext().getGL().glBindTexture(getGlTarget(), getGlName());
        glContext().getGL().glTexParameteri(getGlTarget(), GL_TEXTURE_MIN_FILTER, filter);
        glContext().getGL().glTexParameteri(getGlTarget(), GL_TEXTURE_MAG_FILTER, filter);

        glContext().getGL().getGL2().glTexBuffer(getGlTarget(), internalFormat, dataObject);
    }

    @Override
    public void bind() {
        glContext().getGL().glBindTexture(getGlTarget(), getGlName());
        glContext().getGL().getGL2().glBindImageTexture(getBindingIndex(), getGlName(),
                0, true, 0, access, internalFormat);
    }
}
