package cz.fidentis.analyst.glsl.buffers;

/**
 * A texture buffer storing an image.
 * This buffer is used for the {@code GL_TEXTURE_2D} OpenGL target.
 *
 * @author Radek Oslejsek
 */
public interface ImageBuffer extends GlslBuffer {

    /**
     * Allocates the buffer using {@code glTexImage2D} OpenGL method
     *
     * @param width {@code glTexImage2D()} width parameter
     * @param height {@code glTexImage2D()} height parameter
     * @param format {@code glTexImage2D()} format parameter
     * @param type {@code glTexImage2D()} format parameter
     * @param filter Filtering mode
     */
    void allocate(int width, int height, int format, int type, int filter);

    /**
     * Clear the buffer with given value.
     *
     * @param val Value
     */
    void clear(long val);
}
