package cz.fidentis.analyst.glsl.code.impl;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLException;
import cz.fidentis.analyst.glsl.code.GlslProgram;
import cz.fidentis.analyst.glsl.code.GlslProgramDef;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import static com.jogamp.opengl.GL.GL_FALSE;
import static com.jogamp.opengl.GL2.GL_INFO_LOG_LENGTH;
import static com.jogamp.opengl.GL2.GL_LINK_STATUS;

/**
 * A proxy object storing the information about a GLSL program compiled on GPU.
 * The class follow the Builder principles. Usage:
 * <pre>
 *     CompiledProgram program = new CompiledProgram(...)
 *           .addShader(shader1)
 *           .addShader(shader2)
 *           .addShader(shader3)
 *           .compile();
 * </pre>
 *
 * @author Radek Oslejsek
 */
public class CompiledProgram implements GlslProgram {

    /**
     * Named uniform location. Key is the name, value is the ID on the GPU.
     */
    private final Map<String, Integer> uniformLocations = new HashMap<>();

    /**
     * ID of the program in the OpenGL context
     */
    private int programId;

    /**
     * OpenGL context in which the program is compiled
     */
    private final GLContext glContext;

    private final GlslProgramDef programDef;

    /**
     * Links existing shaders into a program.
     *
     * @param glContext OpenGL context. Must not be {@code null}. Must be at least GL2.
     * @param programDef The definition of the program to be compiled
     * @throws GLException if the program initiation fails
     */
    public CompiledProgram(GLContext glContext, GlslProgramDef programDef) {
        this.glContext = glContext;
        this.programId = glContext.getGL().getGL2().glCreateProgram();
        this.programDef = programDef;
    }

    /**
     * Links program with the given shader.
     *
     * @param shader compiled shader to be added to the program
     * @return this object
     */
    public CompiledProgram addShader(CompiledShader shader) {
        glContext.getGL().getGL2().glAttachShader(programId, shader.getId());
        return this;
    }

    /**
     * Compiles the program on GPU.
     *
     * @return this object
     * @throws GLException if the compilation fails
     */
    public CompiledProgram compile() throws GLException {
        GL2 gl = glContext.getGL().getGL2();

        gl.glLinkProgram(programId);

        // check program status
        int[] linkStatus = new int[1];
        gl.glGetProgramiv(programId, GL_LINK_STATUS, linkStatus, 0);

        if (GL_FALSE == linkStatus[0]) {
            int[] infoLogLength = new int[1];
            gl.glGetProgramiv(programId, GL_INFO_LOG_LENGTH, infoLogLength, 0);

            byte[] infoLogBytes = new byte[infoLogLength[0]];
            gl.glGetProgramInfoLog(programId, infoLogLength[0], infoLogLength, 0, infoLogBytes, 0);

            throw new GLException(new String(infoLogBytes, 0, infoLogLength[0]));
        }

        programDef.getUniformLocations().forEach(key ->
                this.uniformLocations.put(
                        key,
                        glContext.getGL().getGL2().glGetUniformLocation(programId, key)));

        return this;
    }

    /**
     * Ask GPU to release the program from its memory.
     */
    public void release() {
        if (programId != -1) {
            glContext.getGL().getGL2().glDeleteProgram(programId);
            programId = -1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompiledProgram program)) {
            return false;
        }

        return getProgramId() == program.getProgramId();
    }

    @Override
    public int hashCode() {
        return getProgramId();
    }

    @Override
    public int getProgramId() {
        return programId;
    }

    @Override
    public GlslProgramDef getProgramDef() {
        return programDef;
    }

    @Override
    public int getUniformLocation(String key) {
        return uniformLocations.getOrDefault(key, -1);
    }

    @Override
    public void setMat4fVar(String varName, float[] value) {
        if (getUniformLocation(varName) < 0) {
            throw new UnsupportedOperationException("unknown variable");
        }
        if (value.length != 16 || getUniformLocation(varName) < 0) {
            throw new UnsupportedOperationException("float[16] required");
        }

        glContext.getGL().getGL2().glUniformMatrix4fv(getUniformLocation(varName),
                1, false, FloatBuffer.wrap(value));
    }

    @Override
    public void setNumberVar(String varName, Number value) {
        if (getUniformLocation(varName) < 0) {
            throw new UnsupportedOperationException("unknown variable");
        }

        if (value instanceof Float floatVal) {
            glContext.getGL().getGL2().glUniform1f(getUniformLocation(varName), floatVal);
        } else if (value instanceof Integer intVal) {
            glContext.getGL().getGL2().glUniform1i(getUniformLocation(varName), intVal);
        } else {
            throw new UnsupportedOperationException("float or int required");
        }
    }

    @Override
    public void setVec4fVar(String varName, float v1, float v2, float v3, float v4) {
        if (getUniformLocation(varName) < 0) {
            throw new UnsupportedOperationException("unknown variable");
        }

        glContext.getGL().getGL2().glUniform4f(getUniformLocation(varName), v1, v2, v3, v4);
    }
}
