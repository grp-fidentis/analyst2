package cz.fidentis.analyst.glsl.buffergroups;

import cz.fidentis.analyst.glsl.GlslServices;
import cz.fidentis.analyst.glsl.buffers.*;

/**
 * This interface represents a group of registered (not necessarily allocated or bound) buffers.
 * To add a new group, update the {@link BufferGroupDef} and provide the implementation in the {@code impl} package.
 * Then, <b>update the {@link GlslServices#registerBuffers }</b> appropriately. Also, use this service
 * for instantiation.
 *
 * @author Pavol Kycina
 */
public interface GlslBufferGroup {

    /**
     * Returns registered SSBO buffer.
     *
     * @param bufferName Buffer name
     * @return Allocated buffer or {@code null}
     */
    SsboBuffer getSSboBuffer(BufferDef bufferName);

    /**
     * Returns registered texture image buffer.
     *
     * @param bufferName Buffer name
     * @return Allocated buffer or {@code null}
     */
    ImageBuffer getImageBuffer(BufferDef bufferName);

    /**
     * Returns registered texture adapter buffer.
     *
     * @param bufferName Buffer name
     * @return Allocated buffer or {@code null}
     */
    TextureBuffer getTextureAdapterBuffer(BufferDef bufferName);

    /**
     * Returns registered frame buffer.
     *
     * @param bufferName Buffer name
     * @return Allocated buffer or {@code null}
     */
    FrameBuffer getFrameBuffer(BufferDef bufferName);

    /**
     * Returns registered simple object buffer.
     *
     * @param bufferName Buffer name
     * @return Allocated buffer or {@code null}
     */
    PixelUnpackBuffer getPixelUnpackBuffer(BufferDef bufferName);

    /**
     * Returns registered atomic counter buffer.
     *
     * @param bufferName Buffer name
     * @return Allocated buffer or {@code null}
     */
    AtomicCounterBuffer getAtomicCounterBuffer(BufferDef bufferName);

    /**
     * Bind all SBBO buffers at once.
     */
    void bindSsboBuffers();

    /**
     * Bind a give buffer only, including non-SSBO buffers.
     *
     * @param buffer A buffer to be bound
     * @return {@code false} if the buffer does not exist
     */
    boolean bindBuffer(BufferDef buffer);

    /**
     * Releases used memory on GPU hold by buffers. No buffer methods are allowed to be called after this.
     */
    void destroyBuffers();
}
