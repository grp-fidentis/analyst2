package cz.fidentis.analyst.glsl.buffers;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffergroups.GlslBufferGroup;

/**
 * A GLSL buffer.
 * <p>
 * Every buffer instance should be included in some {@link GlslBufferGroup} and managed (e.g., instantiated)
 * through this group.
 * </p>
 * <p>
 * Buffers are further separated by their OpenGL targets, e.g., {@code GL_SHADER_STORAGE_BUFFER} (SSBO buffers),
 * {@code GL_TEXTURE_2D}, {@code GL_PIXEL_UNPACK_BUFFER}, etc. The targets mainly differ in the buffer allocation.
 * See corresponding sub-interfaces for more details.
 * </p>
 *
 * @author Pavol Kycina
 * @author Radek Oslesek
 */
public interface GlslBuffer {

    /**
     * Returns buffer name (enum defined in this API)
     * @return buffer name
     */
    BufferDef getBufferName();

    /**
     * Returns binding index used in code of GLSL shaders.
     * Biding index of value -1 means that the biding index is not used/relevant for this buffer.
     *
     * @return binding index.
     */
    int getBindingIndex();

    /**
     * Returns the name (ID) under which is the buffer registered inside the OpenGL context.
     * @return GLSL name (ID)
     */
    int getGlName();

    /**
     * Returns the OpenGL context under which is the buffer registered.
     * @return OpenGL context
     */
    GLContext glContext();

    /**
     * Returns the OpenGL target.
     * @return the OpenGL target
     */
    int getGlTarget();

    /**
     * Binds this buffer to the OpenGL context (binding points of GPU rendering pipeline), so it become visible
     * for shaders. Buffers can be bound to their binding points even unallocated.
     * Do not use this method directly. As every buffer  should be included in a {@link GlslBufferGroup},
     * use {@link GlslBufferGroup#bindBuffer} to bind buffers individually or {@link GlslBufferGroup#bindSsboBuffers}
     * to allocate all SSBO buffers at once.
     */
    void bind();
}
