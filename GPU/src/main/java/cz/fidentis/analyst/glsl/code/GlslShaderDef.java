package cz.fidentis.analyst.glsl.code;

import static com.jogamp.opengl.GL2.GL_FRAGMENT_SHADER;
import static com.jogamp.opengl.GL2.GL_VERTEX_SHADER;
import static com.jogamp.opengl.GL3.GL_COMPUTE_SHADER;

/**
 * Definitions of defined GLSL shaders.
 * Source codes (.glsl files) must be located in the "/shaders" subdirectory of resources.
 *
 * @author Radek Oslejsek
 */
public enum GlslShaderDef {

    /* *****************************************************************************
     * SHADERS FOR VISUAL EFFECTS USED IN FACE-TO-FACE SCENE
     *******************************************************************************/

    SHADING_VS("ShadingVS.glsl", GL_VERTEX_SHADER),
    SHADING_FS("ShadingFS.glsl", GL_FRAGMENT_SHADER),
    COLOR_MIXING_VS("ColorMixingVS.glsl", GL_VERTEX_SHADER),
    COLOR_MIXING_FS("ColorMixingFS.glsl", GL_FRAGMENT_SHADER),
    CONTOUR_FS("ContoursFS.glsl", GL_FRAGMENT_SHADER),
    SHADOW_MAP_VS("ShadowMapVS.glsl", GL_VERTEX_SHADER),
    SHADOW_MAP_FS("ShadowMapFS.glsl", GL_FRAGMENT_SHADER),

    /* *********************************************************************************
     * SHADERS FPR THE COMPUTATION OF RAY-CASTING-BASED MESH DISTANCE MEASUREMENT
     ***********************************************************************************/

    TRIANGLE_STATS_CS("raycasting/TriangleStatsCS.glsl", GL_COMPUTE_SHADER),
    GRID_PARAMETERS_CS("raycasting/GridParametersCS.glsl", GL_COMPUTE_SHADER),
    GRID_CELL_BOUNDS_CS("raycasting/GridCellBoundsCS.glsl", GL_COMPUTE_SHADER),
    GRID_CELL_OVERLAP_COUNT_CS("raycasting/GridCellOverlapCountCS.glsl", GL_COMPUTE_SHADER),
    GRID_SORTED_TRIANGLES_CS("raycasting/GridCellTrianglesFillCS.glsl", GL_COMPUTE_SHADER),
    DISTANCE_RAY_CASTING_CS("raycasting/DistanceRayCastingCS.glsl", GL_COMPUTE_SHADER);

    /* *****************************************************************************
     * END OF DEFINITIONS
     *******************************************************************************/


    
    private final String file;
    private final int shaderType;

    GlslShaderDef(String file, int shaderType) {
        this.file = file;
        this.shaderType = shaderType;
    }

    @Override
    public String toString() {
        return file;
    }

    public int getShaderType() {
        return shaderType;
    }

    public String getFile() {
        return file;
    }
}
