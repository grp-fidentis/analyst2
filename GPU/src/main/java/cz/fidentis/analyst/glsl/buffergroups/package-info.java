/**
 * Groups of GLSL buffers used together by GLSL programs and shaders.
 */
package cz.fidentis.analyst.glsl.buffergroups;
