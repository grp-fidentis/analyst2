package cz.fidentis.analyst.glsl.buffers.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.buffers.GlslBuffer;

import java.util.Objects;

/**
 *  A common code for OpenGL buffers registered in an OpenGL context under a name.
 *
 * @author Pavol Kycina
 */
public abstract class AbstractBuffer implements GlslBuffer {

    private final BufferDef bufferName;
    private final GLContext glContext;
    private int glName;
    private final int glTarget;

    /**
     * Constructor. The OpenGL name must be set in subclasses!
     *
     * @param bufferName Buffer name
     * @param glContext OpenGL context
     * @param glTarget OpenGL target
     */
    public AbstractBuffer(BufferDef bufferName, GLContext glContext, int glTarget) {
        this.bufferName = Objects.requireNonNull(bufferName);
        this.glContext= Objects.requireNonNull(glContext);
        this.glTarget = glTarget;

        this.glName = -1;
    }

    public void setGlName(int glName) {
        this.glName = glName;
    }

    @Override
    public BufferDef getBufferName() {
        return bufferName;
    }

    @Override
    public int getBindingIndex() {
        return bufferName.getBindingIndex();
    }

    @Override
    public int getGlName() {
        return glName;
    }

    @Override
    public GLContext glContext() {
        return glContext;
    }

    public int getGlTarget() {
        return glTarget;
    }

}
