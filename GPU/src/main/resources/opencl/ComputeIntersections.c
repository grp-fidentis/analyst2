#define STACK_SIZE 64

/*
    Precalculates information about ray (scaling outside of bounding boy and bit masks)
*/
inline struct DirectionInfo getDirection(Point3D lowerBound, Point3D upperBound, Vector3D rayDirection) {
    char subNodeMask = 0;
    Point3D originAdd = (Point3D) {0,0,0};
    Vector3D scaledDirection = rayDirection * -10000.0f;

    if (rayDirection.x < 0.0f) {
        originAdd = (Point3D) {lowerBound.x + upperBound.x,originAdd.y,originAdd.z};
        rayDirection = (Vector3D) {rayDirection.x*-1,rayDirection.y,rayDirection.z};
        subNodeMask |= 4;
    }
    if (rayDirection.y < 0.0f) {
        originAdd = (Point3D) {originAdd.x,lowerBound.y + upperBound.y,originAdd.z};
        rayDirection = (Vector3D) {rayDirection.x,rayDirection.y*-1,rayDirection.z};
        subNodeMask |= 2;
    }
    if (rayDirection.z < 0.0f) {
        originAdd = (Point3D) {originAdd.x,originAdd.y,lowerBound.z + upperBound.z};
        rayDirection = (Vector3D) {rayDirection.x,rayDirection.y,rayDirection.z*-1};
        subNodeMask |= 1;
    }

    return (struct DirectionInfo) {originAdd, rayDirection, scaledDirection,lowerBound,upperBound,
    subNodeMask, 1^subNodeMask,2^subNodeMask,3^subNodeMask,4^subNodeMask,5^subNodeMask,6^subNodeMask,7^subNodeMask};
}

/*
    Kernel to calculate closest intersections of rays with triangles loaded in octree
    Only the closest intersections are aggregated

    Return float8 ray intersection in a index format as follows:
    i0,i1,i2 ==> intersection point coordinates
    i3 ==> intersected triangle index. The index is encoded as (index+1) to reserve 0 value as intersection miss.
    If the hit is INDIRECT, the index is additional negative (-index)

    i4,i5,i6 ==> intersection normal vector
    i7 ==> distance between the intersection point and ray origin point

    @author Marek Horsky
*/

__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void calculateClosestIntersections(const int rayCount, const int maxLeafSize,
      const int smoothing, const int filter,
      global Point3D* origins, global Vector3D* directions, // Ray direction
      global MeshTriangle* triangles, // Mesh Triangles ==> triangle == 6 float3 values stored continuously
      global Point3D* octreeBBox,
      global int* indices, global int* triangleIndices, // Index mappings - nodes to octants and leaves to triangles
      global float8* closestIntersections){ // Results --> Everything is casted to floats in float8* type to avoid waste of space during alignment. There is a room for better representation

      int threadID = get_global_id(0);
      local struct DirectionInfo info[1];

      Point3D rayOrigin = origins[threadID];
      Vector3D rayDirection = directions[0];

      if(get_local_id(0)==0.0f){
          info[0] = getDirection(octreeBBox[0], octreeBBox[1], rayDirection); // Precalculates scaling and bit mask for sub-octant traversal
      }
      OctNode stackMemory[STACK_SIZE];
      OctNodeStack stack = {-1, stackMemory};

      barrier(CLK_LOCAL_MEM_FENCE); // Waits for info to be precomputed

      if(threadID>=rayCount){ // Terminates useless threads - Created due to work-group alignment to warp size
          return;
      }

      RayIntersection intersection = traverseOctree(maxLeafSize, // Max leaf size
                                    smoothing, // Smoothing
                                    filter, // Applied filter
                                    rayOrigin, rayDirection, info, // Ray
                                    stack, // Octree traversal stack
                                    indices, // Octree
                                    triangleIndices, triangles); // Leaf octant triangle contents & Mesh Triangles

      closestIntersections[threadID] = (float8) {intersection.point.x,intersection.point.y,intersection.point.z,intersection.triangle,
                                          intersection.normal.x,intersection.normal.y,intersection.normal.z,intersection.distance};
}