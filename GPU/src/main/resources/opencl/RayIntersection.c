/*
    Performs the ray intersection calculation based on the CPU implementation of the Curles's solution
    https://courses.cs.washington.edu/courses/csep557/10au/lectures/triangle_intersection.pdf
    This solution has lower precision.
    @author Marek Horsky
*/

//Filter
#define APPLIED_FILTER 1.0f

//Intersection types
#define MISS 0.0f
#define HIT 1.0f
#define DIRECT_HIT 2.0f

//Smoothing types
#define NONE 0.0f
#define NORMAL 1.0f
#define SHAPE 2.0f
#define NORMAL_AND_SHAPE 3.0f

struct Intersection{
    Point3D point;
    Vector3D normal;
    int triangle; // 0 == MISS, NEGATIVE == HIT, positive == DIRECT_HIT
    float distance;
};

typedef struct Intersection RayIntersection;

inline RayIntersection performRayCast(const int smoothing, const int filter, const MeshTriangle meshTriangle,
                                        const int triangleIndex, const Point3D origin, const Vector3D direction){
    Vector3D edges[3]; // edges A->B, B->C, C->A
    Vector3D inner[3]; // vectors from vertices to the intersection point
    Vector3D crosses[3]; // cross products

    // Compute and orient normal:
    edges[0] = getEdge(meshTriangle.position1, meshTriangle.position2); // A->B
    edges[2] = getEdge(meshTriangle.position1, meshTriangle.position3); // A->C => invert after usage!

    Vector3D normal = cross(edges[0], edges[2]);
    normal = orientNormal(meshTriangle, fast_normalize(normal));

    if (filter == APPLIED_FILTER && dot(normal, direction) < 0.0f) { // skip this triangle
        return (RayIntersection) {0,0,0, FLT_MAX};
    }

    edges[2] = -edges[2]; // C->A

    // Determine the point of intersection:
    float np = dot(normal, origin);
    float nv = dot(normal, direction);

    float offset = dot(normal, meshTriangle.position1);
    float t = half_divide(offset - np, nv);

    Point3D intersection = {mad(direction.x, t, origin.x),
                            mad(direction.y, t, origin.y),
                            mad(direction.z, t, origin.z)};

    if (!isPointInTriangle(meshTriangle, intersection, normal, edges, inner, crosses)) {
        return (RayIntersection) {0,0,0,FLT_MAX};
    }

    if(smoothing != NONE){
        Point3D bcCoords = getBarycentricCoords(crosses);

        if(smoothing == NORMAL || smoothing == NORMAL_AND_SHAPE){
            normal = getInterpolatedNormal(meshTriangle, bcCoords);
        }

        if(smoothing == SHAPE || smoothing == NORMAL_AND_SHAPE){
            intersection = getInterpolatedIntersection(meshTriangle, bcCoords, inner);
        }
    }
    return (RayIntersection) {intersection, normal, t >= 0 ? triangleIndex+1 : -(triangleIndex+1), fabs(fast_distance(intersection, origin))};
}