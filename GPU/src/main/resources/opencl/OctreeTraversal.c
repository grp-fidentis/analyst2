/*
    This file provides kernel responsible for parallel octree traversal.
    @author Marek Horsky
*/

struct DirectionInfo{
    Point3D originAdd;
    Vector3D normalizedDirection;
    Vector3D scaledDirection;
    Point3D rootLowerBound;
    Point3D rootUpperBound;
    char mask;
    char mask1;
    char mask2;
    char mask3;
    char mask4;
    char mask5;
    char mask6;
    char mask7;
};

inline Point3D getOrigin(Point3D originAdd, Point3D origin){
    return (Point3D) {originAdd.x!=0.0f ? originAdd.x - origin.x : origin.x,
                        originAdd.y!=0.0f ? originAdd.y - origin.y : origin.y,
                        originAdd.z!=0.0f ? originAdd.z - origin.z : origin.z};
}

inline Point3D getNextCellPoint(OctNode node) {
    return 0.5f * (node.t0+node.t1);
}

inline char getFistSubNode(Point3D t0, Point3D tm) {
    if(t0.x > t0.y){
        if(t0.x > t0.z){ // entry plane: YZ
            return (tm.y < t0.x)<<1 | (tm.z < t0.x);
        }
        return (tm.x < t0.z)<<2 | (tm.y < t0.z)<<1; // entry plane: XY
    }
    if(t0.z > t0.y){ // entry plane: XY
        return (tm.x < t0.z)<<2 | (tm.y < t0.z)<<1; // entry plane: XY
    }
    return (tm.x < t0.y)<<2 | (tm.z < t0.y); // entry plane: XZ
}

inline char nextSubNode(float tx1, char yzExitPlane, float ty1, char xzExitPlane, float tz1, char xyExitPlane) {
    if(tx1 < ty1){
        return tx1 < tz1 ? yzExitPlane : xyExitPlane;
    }
    return tz1 < ty1 ? xyExitPlane : xzExitPlane;
}

inline OctNodeStack traverseChildren(OctNodeStack stack, global int* indices, OctNode node, Point3D tm, char octantIndex, local struct DirectionInfo* info){
    do {
        switch (octantIndex) {
            case 0:
                stack = put(stack, (OctNode) {node.t0, tm,
                            indices[node.childIndex+info[0].mask]});
                octantIndex = nextSubNode(tm.x, 4, tm.y, 2, tm.z, 1);
                break;
            case 1:
                stack = put(stack, (OctNode) {(Point3D) {node.t0.x, node.t0.y, tm.z},(Point3D) {tm.x, tm.y, node.t1.z},
                            indices[node.childIndex+info[0].mask1]});
                octantIndex = nextSubNode(tm.x, 5, tm.y, 3, node.t1.z, 8);
                break;
            case 2:
                stack = put(stack, (OctNode) {(Point3D) {node.t0.x, tm.y, node.t0.z}, (Point3D) {tm.x, node.t1.y, tm.z},
                            indices[node.childIndex+info[0].mask2]});
                octantIndex = nextSubNode(tm.x, 6, node.t1.y, 8, tm.z, 3);
                break;
            case 3:
                stack = put(stack, (OctNode) {(Point3D) {node.t0.x, tm.y, tm.z}, (Point3D) {tm.x, node.t1.y, node.t1.z},
                            indices[node.childIndex+info[0].mask3]});
                octantIndex = nextSubNode(tm.x, 7, node.t1.y, 8, node.t1.z, 8);
                break;
            case 4:
                stack = put(stack, (OctNode) {(Point3D) {tm.x, node.t0.y, node.t0.z}, (Point3D) {node.t1.x, tm.y, tm.z},
                            indices[node.childIndex+info[0].mask4]});
                octantIndex = nextSubNode(node.t1.x, 8, tm.y, 6, tm.z, 5);
                break;
            case 5:
                stack = put(stack, (OctNode) {(Point3D) {tm.x, node.t0.y, tm.z}, (Point3D) {node.t1.x, tm.y, node.t1.z},
                            indices[node.childIndex+info[0].mask5]});
                octantIndex = nextSubNode(node.t1.x, 8, tm.y, 7, node.t1.z, 8);
                break;
            case 6:
                stack = put(stack, (OctNode) {(Point3D) {tm.x, tm.y, node.t0.z}, (Point3D) {node.t1.x, node.t1.y, tm.z},
                            indices[node.childIndex+info[0].mask6]});
                octantIndex = nextSubNode(node.t1.x, 8, node.t1.y, 8, tm.z, 7);
                break;
            case 7:
                stack = put(stack, (OctNode) {tm, node.t1,
                            indices[node.childIndex+info[0].mask7]});
                octantIndex = 8;
                break;
            default:
                break;
        }
    } while(octantIndex < 8.0f);
    return stack;
}

/*
    Traverses octree top-down and calculates intersections on the spot. Returns the closest one
*/

inline RayIntersection traverseOctree(const int maxLeafSize, const int smoothing, const int filter,
        const Point3D rayOrigin, const Vector3D rayDirection, local struct DirectionInfo* dirInfo,
        OctNodeStack stack, global int* indices, global int* triangleIndices,
        global MeshTriangle* triangles){ // MeshTriangle points

    RayIntersection intersection = (RayIntersection) {0,0,0,FLT_MAX};
    int triangleIndex;
    short triangleLeafIndex;
    RayIntersection processedIntersection;
    Point3D tm;

    Point3D origin = getOrigin(dirInfo[0].originAdd, rayOrigin + dirInfo[0].scaledDirection);
    Point3D t0 = half_divide((dirInfo[0].rootLowerBound - origin),dirInfo[0].normalizedDirection);
    Point3D t1 = half_divide((dirInfo[0].rootUpperBound - origin),dirInfo[0].normalizedDirection);

    if (fmax(fmax(t0.x, t0.y), t0.z) >= fmin(fmin(t1.x, t1.y), t1.z)) {  // Out of root bounding box
        return intersection;
    }

    OctNode node = {t0, t1, 0};
    stack = put(stack, node);
    while(stack.head > STACK_CAP){
        node = peek(stack);
        stack = pop(stack);

        if(node.childIndex==EMPTY){ // This octant has no children
            continue;
        }

        if(node.childIndex<EMPTY){
            node.childIndex = (-node.childIndex)*maxLeafSize; // Index of the hit leaf is negative number - so we can easily detect leaves
            triangleLeafIndex = 0;
            while(triangleLeafIndex < maxLeafSize){
                triangleIndex = triangleIndices[node.childIndex+triangleLeafIndex];
                if(triangleIndex==EMPTY){
                    break;
                }
                processedIntersection = performRayCast(smoothing, filter, triangles[triangleIndex],triangleIndex, rayOrigin, rayDirection);
                if(processedIntersection.distance < intersection.distance){
                    intersection = processedIntersection;
                }
                triangleLeafIndex++;
            }
            continue;
        }

        tm = getNextCellPoint(node);
        stack = traverseChildren(stack, indices, node, tm, getFistSubNode(node.t0, tm), dirInfo);
    }
    return intersection;
}