/*
    This file contains kernels to build octree in GPU memory
    @author Marek Horsky
*/

#define STACK_SIZE 64
#define PROCESSED_INDEX 1.0f // Uses index 1 as "being processed" value
#define PROCESSED_LEAF -2.0f // Reserves counter -1 as "being processed" value

inline void createLeaf(const int index, volatile global int* indices, volatile global int* leafCounter){
    if(atomic_cmpxchg(&indices[index], EMPTY, PROCESSED_LEAF)==EMPTY){ // (old == cmp) ? val : old
        atomic_xchg(&indices[index], atomic_sub(leafCounter, 1));
    }
    while(indices[index]>=PROCESSED_LEAF); // Spinlock if the accessed octant is currently being processed
}

inline int createNode(const int index, volatile global int* indices, volatile global int* counter){
    if(atomic_cmpxchg(&indices[index], EMPTY, PROCESSED_INDEX)==EMPTY){ // assigns (old == cmp) ? val : old
        atomic_xchg(&indices[index], atomic_add(counter, 8));
    }
    while(indices[index]<=PROCESSED_INDEX); // Spinlock if the accessed octant is currently being processed
    return indices[index];
}

inline bool isTriangleBoundingBoxInOctant(const BoundingBox octantBBox, const BoundingBox triangleBBox) {
    return (triangleBBox.largestPoint.x >= octantBBox.smallestPoint.x && octantBBox.largestPoint.x >= triangleBBox.smallestPoint.x)
        && (triangleBBox.largestPoint.y >= octantBBox.smallestPoint.y && octantBBox.largestPoint.y >= triangleBBox.smallestPoint.y)
        && (triangleBBox.largestPoint.z >= octantBBox.smallestPoint.z && octantBBox.largestPoint.z >= triangleBBox.smallestPoint.z);
}

inline int getOctant(const Point3D middlePoint, const Point3D point){
    return ((point.x > middlePoint.x)<<2)+((point.y > middlePoint.y)<<1)+(point.z > middlePoint.z);
}

inline BoundingBox getOctantBBox(const Point3D middlePoint, const char octant, const BoundingBox bbox){ // Manually unrolled
    switch(octant){
        case 0:
            return (BoundingBox) {bbox.smallestPoint,middlePoint};
        case 1:
            return (BoundingBox) {(Point3D){bbox.smallestPoint.x, bbox.smallestPoint.y, middlePoint.z},
                                  (Point3D){middlePoint.x, middlePoint.y, bbox.largestPoint.z}};
        case 2:
            return (BoundingBox) {(Point3D){bbox.smallestPoint.x, middlePoint.y, bbox.smallestPoint.z},
                                  (Point3D){middlePoint.x, bbox.largestPoint.y, middlePoint.z}};
        case 3:
            return (BoundingBox) {(Point3D){bbox.smallestPoint.x, middlePoint.y, middlePoint.z},
                                  (Point3D){middlePoint.x, bbox.largestPoint.y, bbox.largestPoint.z}};
        case 4:
            return (BoundingBox) {(Point3D){middlePoint.x, bbox.smallestPoint.y, bbox.smallestPoint.z},
                                  (Point3D){bbox.largestPoint.x, middlePoint.y, middlePoint.z}};
        case 5:
            return (BoundingBox) {(Point3D){middlePoint.x, bbox.smallestPoint.y, middlePoint.z},
                                  (Point3D){bbox.largestPoint.x, middlePoint.y, bbox.largestPoint.z}};
        case 6:
            return (BoundingBox) {(Point3D){middlePoint.x, middlePoint.y, bbox.smallestPoint.z},
                                  (Point3D){bbox.largestPoint.x, bbox.largestPoint.y, middlePoint.z}};
        default:
            return (BoundingBox) {middlePoint,bbox.largestPoint};
    }
}

/*
    Kernels used to build linearized octree out of vertices and triangles
    Build octree structure from vertices in 1 thread per 1 vertex manner. Maximal depth is the stopping constraint.
    Creation of nodes atomically triggered upon thread-entry, so only non-empty branches are generated.
*/

__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void vertexBuild(const int size, int depth, global Point3D* vertices, global BoundingBox* octreeBBox,
        volatile global int* indices, volatile global int* counter){
    int threadID = get_global_id(0);
    if(threadID >= size){
        return;
    }

    int index = 0; // Root
    BoundingBox octantBBox = octreeBBox[0];
    Point3D vertex = vertices[threadID];
    Point3D middlePoint;
    char octant;

    while(depth > 0.0f){
        middlePoint = getMiddlePoint(octantBBox);
        octant = getOctant(middlePoint, vertex);
        octantBBox = getOctantBBox(middlePoint, octant, octantBBox); // get new octant BBox
        index = createNode(index+octant, indices, counter);
        depth--;
    }
}

/*
    Check current node. If the node does not have children, it is set as leaf ==> leaf index as negative value is written to node.
    The negativity signifies it is a leaf. Also triangles for each leaf are counted, so max value can be computed later.
*/

inline OctNodeStack check(const char octant, const int* indexes, OctNodeStack stack,
                            const OctNode node, const BoundingBox triangleBBox, const Point3D middlePoint,
                            volatile global int* indices, volatile global int* triangleCounters, volatile global int* leafCounter){

    BoundingBox octantBBox = getOctantBBox(middlePoint, octant, node.bbox);
    if(isTriangleBoundingBoxInOctant(octantBBox, triangleBBox)){
        int index = indexes[octant];
        if(index > EMPTY){
            stack = put(stack, (OctNode) {octantBBox, index});
        }
        else{
            createLeaf(node.index+octant, indices, leafCounter);
            atomic_add(&triangleCounters[-indices[node.index+octant]], 1);
        }
    }
    return stack;
}

/*
    Traverses the tree structure in 1 triangle per 1 thread manner.
    Each triangle can end-up in multiple leaves, therefore Stack structure is used.
*/

__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void checkTriangles(const int size, global BoundingBox* octreeBBox, global MeshTriangle* triangles,
                                volatile global int* indices, volatile global int* triangleCounters, volatile global int* leafCounter){
    const int threadID = get_global_id(0);
    BoundingBox octantBBox = octreeBBox[0];
    BoundingBox triangleBBox = getTriangleBBox(triangles[threadID]);

    if(threadID >= size || !isTriangleBoundingBoxInOctant(octantBBox, triangleBBox)){ // Should not happen in valid structures
        return; // Triangle is not in the octree
    }

    OctNode stackMemory[STACK_SIZE];
    OctNodeStack stack = {-1, stackMemory};
    OctNode node = (OctNode) {octantBBox, 0};
    stack = put(stack, node);
    int indexes[8];
    Point3D middlePoint;

    while(stack.head > STACK_CAP){
        node = peek(stack);
        stack = pop(stack);

        middlePoint = getMiddlePoint(node.bbox); // Manual unroll for-loop
        indexes[0] = indices[node.index];
        indexes[1] = indices[node.index+1];
        indexes[2] = indices[node.index+2];
        indexes[3] = indices[node.index+3];
        indexes[4] = indices[node.index+4];
        indexes[5] = indices[node.index+5];
        indexes[6] = indices[node.index+6];
        indexes[7] = indices[node.index+7];
        stack = check(0, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
        stack = check(1, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
        stack = check(2, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
        stack = check(3, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
        stack = check(4, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
        stack = check(5, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
        stack = check(6, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
        stack = check(7, indexes, stack, node, triangleBBox, middlePoint, indices, triangleCounters, leafCounter);
    }
}

/*
    Checks whether the current node is within octant. If the octant is a leaf, the triangle is assigned to the leaf.
*/

inline OctNodeStack assign(const char octant, const int* indexes, const int leafSize, const int threadID, OctNode node, const Point3D middlePoint,
                        const BoundingBox triangleBBox, OctNodeStack stack, volatile global int* leafCounters, volatile global int* leaves){
    const int index = indexes[octant];
    if(index!=EMPTY){ // no node
        BoundingBox bbox = getOctantBBox(middlePoint, octant, node.bbox);
        if(isTriangleBoundingBoxInOctant(bbox, triangleBBox)){
            if(index<EMPTY){ // is leaf
                int leafIndex = -index;
                int count = atomic_add(&leafCounters[leafIndex], 1);
                leaves[(leafIndex*leafSize)+count] = threadID;
            }
            else{
                stack = put(stack, (OctNode) {bbox, index});
            }
        }
    }
    return stack;
}

/*
    Traverses the tree structure in 1 triangle per 1 thread manner. Each triangle can end-up in multiple leaves, therefore Stack structure is used.
    Assigns triangles to corresponding leaves.
*/

__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void assignTriangles(const int size, const int leafSize, global BoundingBox* octreeBBox, global MeshTriangle* triangles,
                                global int* indices, volatile global int* leafCounters, volatile global int* leaves){
    const int threadID = get_global_id(0);
    BoundingBox octantBBox = octreeBBox[0];
    BoundingBox triangleBBox = getTriangleBBox(triangles[threadID]);
    OctNode stackMemory[STACK_SIZE];
    OctNodeStack stack = {-1, stackMemory};

    if(threadID >= size || !isTriangleBoundingBoxInOctant(octantBBox, triangleBBox)){ // Should not happen in valid structures
        return; // Triangle is not in the octree
    }

    Point3D middlePoint;
    int indexes[8];

    OctNode node = (OctNode) {octantBBox, 0};
    stack = put(stack, node);

    while(stack.head > STACK_CAP){
        node = peek(stack);
        stack = pop(stack);

        middlePoint = getMiddlePoint(node.bbox); // Manual unroll for-loop
        indexes[0] = indices[node.index];
        indexes[1] = indices[node.index+1];
        indexes[2] = indices[node.index+2];
        indexes[3] = indices[node.index+3];
        indexes[4] = indices[node.index+4];
        indexes[5] = indices[node.index+5];
        indexes[6] = indices[node.index+6];
        indexes[7] = indices[node.index+7];
        stack = assign(0, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
        stack = assign(1, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
        stack = assign(2, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
        stack = assign(3, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
        stack = assign(4, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
        stack = assign(5, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
        stack = assign(6, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
        stack = assign(7, indexes, leafSize, threadID, node, middlePoint, triangleBBox, stack, leafCounters, leaves);
    }
}