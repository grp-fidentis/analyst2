/*
    This file presents methods on GPU 3D vector data types
    @author Marek Horsky
*/
#define EMPTY -1.0f // Empty memory (initialized to this value)
#define MAX_GROUP_SIZE 256
#define WARP_GROUP_SIZE 32

typedef float3 Point3D;
typedef float3 Vector3D;

struct Triangle {
  Point3D position1;
  Point3D position2;
  Point3D position3;
  Vector3D normal1;
  Vector3D normal2;
  Vector3D normal3;
};

typedef struct Triangle MeshTriangle;

inline bool isZero(Vector3D vec){
    return vec.x == 0.0f && vec.y == 0.0f && vec.z == 0.0f;
}

inline char changeSignCount(Vector3D normalTri, Vector3D normal){
    if(isZero(normalTri)){
        return 0;
    }
    return dot(normalTri, normal) > 0.0f ? 1 : -1;
}

inline Vector3D orientNormal(MeshTriangle triangle, Vector3D normal) {
    // if normal is in opposite direction then point normals, inverse it
    char positiveSignCount = changeSignCount(triangle.normal1, normal)
            + changeSignCount(triangle.normal2, normal)
            + changeSignCount(triangle.normal3, normal);
    return positiveSignCount < 0.0f ? -normal : normal;
}

inline Vector3D getEdge(Point3D v1, Point3D v2) {
    return v2 - v1;
}

inline Vector3D computeOrientedNormal(MeshTriangle triangle) {
    Vector3D ab = triangle.position1 - triangle.position2;
    Vector3D ac = triangle.position1 - triangle.position3;
    Vector3D normal = fast_normalize(cross(ab, ac));
    return orientNormal(triangle, normal);;
}

inline Point3D getBarycentricCoords(Vector3D crosses[]){
    float areaAPB = half_divide(fast_length(crosses[0]),2.0f);
    float areaBPC = half_divide(fast_length(crosses[1]),2.0f);
    float areaCPA = half_divide(fast_length(crosses[2]),2.0f);
    float areaABC = areaAPB + areaBPC + areaCPA;
    return (Point3D) {half_divide(areaBPC,areaABC), half_divide(areaCPA,areaABC), half_divide(areaAPB,areaABC)};
}

inline bool isPointInTriangle(MeshTriangle triangle, Point3D point, Vector3D normal, Vector3D edges[], Vector3D inner[], Vector3D crosses[]) {
    // edge AB
    //edges[0] = getEdge(triangle.position1, triangle.position2); // A->B  Computed previously
    inner[0] = getEdge(triangle.position1, point); // A->P
    crosses[0] = cross(edges[0], inner[0]);

    if (dot(normal, crosses[0]) < 0.0f) { // point is on the right side of AB
        return false;
    }

    // edge BC
    edges[1] = getEdge(triangle.position2, triangle.position3); // B->C
    inner[1] = getEdge(triangle.position2, point);
    crosses[1] = cross(edges[1], inner[1]);
    if (dot(normal, crosses[1]) < 0.0f) { // point is on the right side of BC
        return false;
    }

    // edge CA
    //edges[2] = getEdge(triangle.position3, triangle.position1); // C->A   Computed previously
    inner[2] = getEdge(triangle.position3, point); // C->P
    crosses[2] = cross(edges[2], inner[2]);

    return dot(normal, crosses[2]) >= 0.0f; // point is on the right side of CA
}

inline Vector3D getInterpolatedNormal(MeshTriangle triangle, Point3D barycentricCoords) {
    // interpolate normal:
    Vector3D weightedNormalsSum = mad(triangle.normal1, barycentricCoords.x,
                                     mad(triangle.normal2, barycentricCoords.y,
                                     triangle.normal3* barycentricCoords.z));

    return fast_normalize(weightedNormalsSum);
}

inline Point3D getInterpolatedIntersection(MeshTriangle triangle, Point3D barycentricCoords, Vector3D inner[]) {
    Vector3D ap = -inner[0];
    Vector3D bp = -inner[1];
    Vector3D cp = -inner[2];

    Vector3D r1 = mad(triangle.normal1, dot(ap, triangle.normal1), triangle.position1);
    Vector3D r2 = mad(triangle.normal2, dot(bp, triangle.normal2), triangle.position2);
    Vector3D r3 = mad(triangle.normal3, dot(cp, triangle.normal3), triangle.position3);

    return r1* barycentricCoords.x + r2* barycentricCoords.y + r3* barycentricCoords.z;
}
