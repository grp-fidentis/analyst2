/*
    Stack for octree traversal.
    @author Marek Horsky
*/
#define STACK_CAP -1.0f

struct Node{
    Point3D t0;
    Point3D t1;
    int childIndex;
};

typedef struct Node OctNode;

struct Stack{
    int head;
    OctNode* memory;
};

inline struct Node peek(struct Stack stack){
    return stack.memory[stack.head];
}

inline struct Stack pop(struct Stack stack){
    stack.head--;
    return stack;
}

inline struct Stack put(struct Stack stack, struct Node entry){
    stack.head++;
    stack.memory[stack.head] = entry;
    return stack;
}

typedef struct Stack OctNodeStack;