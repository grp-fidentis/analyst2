/*
    This file contains generic kernel operations on GPU data.
    @author Marek Horsky
*/

/*
    This kernel reduces the 3D data into single bounding box (only initial call)
*/
__attribute__ ((reqd_work_group_size(MAX_GROUP_SIZE, 1, 1)))
kernel void copyAndMergeBoundingBoxes(const int size, global Point3D* vertices, global BoundingBox* bounds){
    int threadID = get_global_id(0);
    short localID = get_local_id(0);

    local BoundingBox boxes[MAX_GROUP_SIZE];
    boxes[localID] = threadID < size ? getVertexBBox(vertices[threadID]) : getMaxBoundingBox();
    barrier(CLK_LOCAL_MEM_FENCE);

    for(short groupShift = MAX_GROUP_SIZE>>1; groupShift>=1; groupShift>>=1){
        if(localID<groupShift){
            boxes[localID] = updateBBox(boxes[localID],boxes[localID+groupShift]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if(localID == 0.0f){
        bounds[get_group_id(0)] = boxes[0];
    }
}

/*
    This kernel reduces the 3D data into single bounding box (only consecutive calls)
*/
__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void mergeBoundingBoxes(const int size, const int shift, global BoundingBox* bounds){
    int threadID = get_global_id(0) * shift;
    short localID = get_local_id(0);

    local BoundingBox boxes[WARP_GROUP_SIZE];
    boxes[localID] = threadID < size ? bounds[threadID] : getMaxBoundingBox();
    barrier(CLK_LOCAL_MEM_FENCE);

    for(short groupShift = WARP_GROUP_SIZE>>1; groupShift>=1; groupShift>>=1){
        if(localID<groupShift){
            boxes[localID] = updateBBox(boxes[localID],boxes[localID+groupShift]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if(localID == 0.0f){
        bounds[threadID] = boxes[0];
    }
}

/*
    This kernel finds largest number in a buffer. Needs to be called repeatedly for global synchronization.
    The shift has to be multiplied by the WARP_GROUP_SIZE each run. After it outgrows bufferSize, the value at index 0 will be the largest one
*/
__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void getLargestInteger(const int shift, const int bufferSize, global int* buffer){
    int threadID = get_global_id(0) * shift;
    short localID = get_local_id(0);

    local int localBuffer[WARP_GROUP_SIZE];
    localBuffer[localID] = threadID < bufferSize ? buffer[threadID] : 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    for(short localShift = 16; localShift > 0; localShift >>=1){
        if(localID<localShift){
            localBuffer[localID] = max(localBuffer[localID], localBuffer[localID+localShift]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if(localID == 0.0f){
        buffer[threadID] = localBuffer[0];
    }
}

/*
    Initializes memory to desired value
*/
__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void initializeMemory(const int size, const int value, global int* buffer){
    int threadID = get_global_id(0);
    if(threadID < size){
        buffer[threadID] = value;
    }
}