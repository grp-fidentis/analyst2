/*
    Kernels transforming projector in GPU memory
    @author Marek Horsky
*/

__attribute__ ((reqd_work_group_size(WARP_GROUP_SIZE, 1, 1)))
kernel void rotateProjector(const int size, const float thetaX, const float thetaY, constant Point3D* centroid, global Point3D* projector, global Point3D* direction){
    int threadID = get_global_id(0);
    if(threadID >= size){
        return;
    }

    float xSin = half_sin(thetaX);
    float xCos = half_cos(thetaX);

    float ySin = half_sin(thetaY);
    float yCos = half_cos(thetaY);

    //sub
    Point3D centroidPoint = centroid[0];
    Point3D p = projector[threadID] - centroidPoint;

    //transform
    Point3D newP = {dot((Point3D) {yCos,0.0f,ySin}, p),
                    dot((Point3D) {xSin*ySin,xCos,(-xSin)*yCos}, p),
                    dot((Point3D) {xCos*(-ySin),xSin,xCos*yCos}, p)};

    //add
    projector[threadID]=newP+centroidPoint;
    if(threadID == 0.0f){
        direction[0] = centroidPoint - projector[0];
    }
}