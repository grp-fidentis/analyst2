/*
    This file contains methods operating with BoundingBox on GPU.
    @author Marek Horsky
*/

struct Box{
    Point3D smallestPoint;
    Point3D largestPoint;
};

typedef struct Box BoundingBox;

inline Point3D getLowerBound(MeshTriangle triangle){
    return (Point3D) {fmin(fmin(triangle.position1.x,triangle.position2.x),triangle.position3.x),
                         fmin(fmin(triangle.position1.y,triangle.position2.y),triangle.position3.y),
                         fmin(fmin(triangle.position1.z,triangle.position2.z),triangle.position3.z)};
}

inline Point3D getUpperBound(MeshTriangle triangle){
    return (Point3D) {fmax(fmax(triangle.position1.x,triangle.position2.x),triangle.position3.x),
                        fmax(fmax(triangle.position1.y,triangle.position2.y),triangle.position3.y),
                        fmax(fmax(triangle.position1.z,triangle.position2.z),triangle.position3.z)};
}

inline Point3D getLower(Point3D point1, Point3D point2){
    return (Point3D) {fmin(point1.x,point2.x),fmin(point1.y,point2.y),fmin(point1.z,point2.z)};
}

inline Point3D getUpper(Point3D point1, Point3D point2){
    return (Point3D) {fmax(point1.x,point2.x),fmax(point1.y,point2.y),fmax(point1.z,point2.z)};
}

inline Point3D getMiddlePoint(BoundingBox box){
    return half_divide((box.smallestPoint+box.largestPoint), 2.0f);
}

inline BoundingBox getTriangleBBox(MeshTriangle triangle){
    return (BoundingBox) {getLowerBound(triangle), getUpperBound(triangle)};
}

inline BoundingBox getVertexBBox(Point3D vertex){
    return (BoundingBox) {vertex, vertex};
}

inline BoundingBox updateBBox(BoundingBox box1, BoundingBox box2){
    return (BoundingBox) {getLower(box1.smallestPoint,box2.smallestPoint), getUpper(box1.largestPoint,box2.largestPoint)};
}

inline BoundingBox getMaxBoundingBox(){
    return (BoundingBox) {(Point3D)(FLT_MAX,FLT_MAX,FLT_MAX), (Point3D)(FLT_MIN,FLT_MIN,FLT_MIN)};
}