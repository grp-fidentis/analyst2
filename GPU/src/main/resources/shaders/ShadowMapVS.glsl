#version 430 compatibility
out vec3 originalPosition;

// Based on shader by Katarina Furmanova available at https://github.com/Fidentis/Analyst/blob/master/Renderer/src/cz/fidentis/renderer/shaders/ShadowMapVS.glsl
void main() {
    originalPosition = vec3(gl_Vertex);
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}