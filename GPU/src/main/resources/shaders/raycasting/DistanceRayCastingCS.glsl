#version 430

struct Ray {
    vec3 origin;
    vec3 direction;
};

struct Triangle {
    vec3 v1;
    vec3 v2;
    vec3 v3;
    vec3 normal;
};

struct GridInfo {
    ivec3 size;
    ivec3 origin;
    float cell_size;
};

struct Vertex {
    vec3 position;
    vec3 normal;
};

#define EPS 1e-10
const float POS_INFINITY = uintBitsToFloat(0x7F800000); // positive infinity
const vec3 POS_INFINITY_VEC = vec3(POS_INFINITY);
const vec3 INVALID_INTERSECTION = POS_INFINITY_VEC;

layout(local_size_x = 64) in;

layout(std430, binding = 0) readonly buffer inputRaysBuffer {
    Ray rays[];
};

layout(std430, binding = 1) readonly buffer inputVerticesBuffer {
    Vertex vertices[];
};

layout(std430, binding = 2) readonly buffer inputIndicesBuffer {
    ivec3 triangles[];
};

layout(std430, binding = 3) readonly buffer inputTrianglesBuffer {
    int sorted_triangles[];
};

layout(std430, binding = 4) readonly buffer inputCellBoundsBuffer {
    int cells_bounds[];
};

layout(std430, binding = 5) readonly buffer inputGridInfoBuffer {
    GridInfo grid;
};

layout(std430, binding = 6) writeonly buffer outputIntersectionsBuffer {
    vec4 results[];
};

uniform int relative_distance_multiplier; // can be 1 or -1

vec3 calculateTriangleOrientedNormal(const Vertex v1, const Vertex v2, const Vertex v3);
vec4 getCloserIntersection(const vec3 origin, const vec3 p1, const vec3 p2, const int second_dist_multiplier);
vec3 testIntersection(const Triangle tri, const Ray ray);
bool isPointInTriangle(const Triangle tri, const vec3 point);
int getCellIndex(const ivec3 cell_coords);
bool isOutside(const ivec3 cell_coords);
bool isPointInsideBB(const vec3 bb_origin, const vec3 bb_corner, const vec3 point);
vec3 getBoundingBoxIntersection(const Ray ray, const vec3 ray_direction_inv);
vec3 calculateIntersection(const Ray ray, const int direction_multiplier);

/*
    Tests all rays for mesh intersection.
    Results are stored as vec4, first three for intersection point and last for the
    distance to that point from ray's origin.
    All four values are positive infinity if there is no intersection.

    Intersection algorithm is based on implementation from MeshTriangle.java

    @author Pavol Kycina
*/
void main() {
    const uint gid = min(gl_GlobalInvocationID.x, rays.length() - 1);

    Ray ray = rays[gid];
    vec3 intersection_positive_dir = calculateIntersection(ray, 1);

    ray.direction *= (-1);
    vec3 intersection_negative_dir = calculateIntersection(ray, -1);

    results[gid] = getCloserIntersection(ray.origin, intersection_positive_dir, intersection_negative_dir, relative_distance_multiplier);
}

vec4 getCloserIntersection(const vec3 origin, const vec3 p1, const vec3 p2, const int second_dist_multiplier) {
    float dist1 = distance(origin, p1);
    float dist2 = distance(origin, p2);
    return dist1 < dist2 ? vec4(p1, dist1) : vec4(p2, dist2 * second_dist_multiplier);
}

vec3 getBoundingBoxIntersection(const Ray ray, const vec3 ray_direction_inv) {
    // inspired from: https://tavianator.com/2011/ray_box.html
    vec3 min_bound = grid.origin * grid.cell_size;
    vec3 max_bound = (grid.origin + grid.size) * grid.cell_size;

    vec3 t1 = (min_bound - ray.origin) * ray_direction_inv;
    vec3 t2 = (max_bound - ray.origin) * ray_direction_inv;

    vec3 vmin = min(t1, t2);
    vec3 vmax = max(t1, t2);

    float tmin = max(max(vmin.x, vmin.y), vmin.z);
    float tmax = min(min(vmax.x, vmax.y), vmax.z);

    return tmax >= tmin ? max(tmin, 0) * ray.direction + ray.origin : INVALID_INTERSECTION;
}

vec3 calculateIntersection(const Ray ray, const int direction_multiplier) {
    vec3 closest_intersection = INVALID_INTERSECTION;

    // initialize traverse
    const vec3 direction = ray.direction;
    const ivec3 direction_sign = ivec3(sign(direction));
    const vec3 ray_direction_inv = mix(1 / direction, POS_INFINITY_VEC, equal(direction, vec3(0, 0, 0)));

    vec3 ray_loc_traverse = getBoundingBoxIntersection(ray, ray_direction_inv) - grid.origin * grid.cell_size; // this location is relative to grid origin from this point
    ivec3 cell_coords = ivec3(ray_loc_traverse / grid.cell_size); // these coords are relative to grid origin

    // move from one cell to the next
    do {
        // get info about current cell
        int cell_index = getCellIndex(cell_coords);

        int min_bound = cells_bounds[cell_index];
        int max_bound = cell_index == cells_bounds.length() - 1 ? sorted_triangles.length() - 1 : cells_bounds[cell_index + 1];

        // test each triangle in current cell
        for (int i = min_bound; i < max_bound; i++) {
            int triangle_index = sorted_triangles[i];

            ivec3 triangle_vertex_indices = triangles[triangle_index];

            Vertex v1 = vertices[triangle_vertex_indices.x];
            Vertex v2 = vertices[triangle_vertex_indices.y];
            Vertex v3 = vertices[triangle_vertex_indices.z];

            vec3 triangle_normal = calculateTriangleOrientedNormal(v1, v2, v3);

            // skip triangles which are oriented to the other side
            if(dot(triangle_normal, direction * direction_multiplier) < 0) {
                continue;
            }

            Triangle triangle = Triangle(v1.position, v2.position, v3.position, triangle_normal);

            vec3 found_intersection = testIntersection(triangle, ray);

            if (found_intersection != INVALID_INTERSECTION) {
                closest_intersection = getCloserIntersection(ray.origin, found_intersection, closest_intersection, 1).xyz;
            }
        }
        vec3 cell_bb_corner = (cell_coords + grid.origin) * grid.cell_size;

        if(isPointInsideBB(cell_bb_corner, cell_bb_corner + vec3(grid.cell_size), closest_intersection)) {
            break; // we found the intersection and it must be the closest
        }

        // next cell traverse:

        // calculate on which "inner cell-walls" should ray land
        // result is absolute coordinations of walls for each dimension - together they do not make sense
        vec3 n = (cell_coords + clamp(direction_sign, 0, 1)) * grid.cell_size;

        n -= ray_loc_traverse; // calculate vector for wall collision from current position

        // calculate factors for each dimension: "let ray extend with factor 't' and it will collide with cell wall"
        vec3 t = mix(n * ray_direction_inv, ray_direction_inv, isinf(ray_direction_inv)); // mix here is used to prevent 0 * inf

        float min_t = min(min(t.x, t.y), t.z); // find minimal t
        ivec3 incrememt = ivec3(t.x == min_t, t.y == min_t, t.z == min_t) * direction_sign;

        cell_coords += incrememt;
        ray_loc_traverse += min_t * direction;

    } while(!isOutside(cell_coords));

    return closest_intersection;
}

vec3 calculateTriangleOrientedNormal(const Vertex v1, const Vertex v2, const Vertex v3) {
    vec3 ab = v1.position - v2.position;
    vec3 ac = v1.position - v3.position;
    vec3 triangle_normal = cross(ab, ac);
    //triangle_normal = normalize(triangle_normal); redundant?

    if(sign(dot(v1.normal, triangle_normal))
            + sign(dot(v2.normal, triangle_normal))
            + sign(dot(v3.normal, triangle_normal)) < 0) {
        // if the normal is in the opposite direction than point normals, inverse it
        triangle_normal *= (-1);
    }
    return triangle_normal;
}


bool isOutside(const ivec3 grid_coords) {
    return any(lessThan(grid_coords, vec3(0))) || any(greaterThanEqual(grid_coords, grid.size));
}

bool isPointInsideBB(const vec3 bb_origin, const vec3 bb_corner, const vec3 point) {
    vec3 s = step(bb_origin, point) - step(bb_corner, point);
    return s.x != 0 && s.y != 0 && s.z != 0;
}

// returns index in the input cell buffer
int getCellIndex(const ivec3 grid_coords) {
    return grid_coords.x * grid.size.y * grid.size.z
            + grid_coords.y * grid.size.z
            + grid_coords.z;
}

bool isPointInTriangle(const Triangle tri, const vec3 point) {
    vec3 AB = tri.v2 - tri.v1;
    vec3 BC = tri.v3 - tri.v2;
    vec3 CA = tri.v1 - tri.v3;

    vec3 cross_productAB = cross(AB, point - tri.v1);
    vec3 cross_productBC = cross(BC, point - tri.v2);
    vec3 cross_productCA = cross(CA, point - tri.v3);

    return dot(tri.normal, cross_productCA) >= 0 // point is on the left side of CA
            && dot(tri.normal, cross_productAB) >= 0 // point is on the left side of AB
            && dot(tri.normal, cross_productBC) >= 0; // point is on the left side of BC
}

vec3 testIntersection(const Triangle tri, const Ray ray) {
    // Determine the point of intersection:
    float np = dot(tri.normal, ray.origin);
    float nv = dot(tri.normal, ray.direction);

    bool is_parallel = abs(nv) < EPS;

    float offset = dot(tri.normal, tri.v1);
    float t = (offset - np) / nv;

    vec3 intersection = ray.origin + (ray.direction * t);
    bool in_triangle = isPointInTriangle(tri, intersection);
    return t >= 0 && !is_parallel && in_triangle ? intersection : INVALID_INTERSECTION;
}