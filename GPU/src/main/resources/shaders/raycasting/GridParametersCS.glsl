#version 430

struct GridInfo {
    ivec3 size;
    ivec3 origin;
    float cell_size;
};

struct TriangleReductionData {
    vec3 min_corner;
    vec3 max_corner;
    float edges_len_sum;
};

#define MAXIMUM_CELL_COUNT 10000000 // to prevent memory exhaustion

layout(local_size_x = 64) in;

layout(std430, binding = 5) buffer inputGridInfoBuffer {
    GridInfo grid_info;
};

layout(std430, binding = 8) buffer reductionBuffer {
    TriangleReductionData reduction[];
};

uniform int level;
uniform int invocations;

ivec3 locationToCell(vec3 loc) {
    return ivec3(floor(loc / grid_info.cell_size));
}

/*
    Calculates bounding box for the uniform grid based on given triangles data.
    Also estimates the optional cell size for the grid using heuristic.

    The min/max bounding corners and sum of the edge distances are computed using parallel reduction.
    This implementation works in-place and for arbitrary triangle array size >= 2.
    This shader should be dispatched in multiple rounds and with work group size reduced with the half of the previous size.
    On last single invocation, results will be written to the grid info buffer.

    @author Pavol Kycina
*/
void main() {
    int two_powered = 1 << level; // pre-compute

    uint first_index = gl_GlobalInvocationID.x * two_powered * 2 + two_powered - 1;
    uint second_index = min(first_index + two_powered, reduction.length() - 1);

    if(first_index >= second_index) {
        return;
    }

    TriangleReductionData triangle_data1 = reduction[first_index];
    TriangleReductionData triangle_data2 = reduction[second_index];

    float edges_summed = triangle_data1.edges_len_sum + triangle_data2.edges_len_sum;
    vec3 min_corner = min(triangle_data1.min_corner, triangle_data2.min_corner);
    vec3 max_corner = max(triangle_data1.max_corner, triangle_data2.max_corner);

    reduction[second_index] = TriangleReductionData(min_corner, max_corner, edges_summed);

    if(invocations == 1) { // last iteration, process results (work for one thread)
        // doubled average edge length is a good heuristic for cell size, feel free to implement better
        float cell_size = edges_summed / (reduction.length() * 3) * 2;

        vec3 bounding_box_size = max_corner - min_corner;
        float volume = (bounding_box_size.x * bounding_box_size.y * bounding_box_size.z);

        int estimated_cell_count = int(volume / pow(cell_size, 3));

        if(estimated_cell_count > MAXIMUM_CELL_COUNT) { // rather drop time performance than run out of memory
            cell_size = pow(volume / MAXIMUM_CELL_COUNT, 1 / 3.0);
        }

        grid_info.cell_size = cell_size;
        grid_info.origin = locationToCell(min_corner);
        grid_info.size = locationToCell(max_corner) - grid_info.origin + ivec3(1);
    }
}