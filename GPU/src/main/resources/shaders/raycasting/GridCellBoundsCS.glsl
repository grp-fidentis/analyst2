#version 430

layout(local_size_x = 64) in;

layout(std430, binding = 7) buffer cellElementsBuffer {
    int cell_elements[];
};

uniform int level;
uniform int upsweep;

/*
    Prefix sum algorithm, traverse all cells and prefix-sum their number of triangles which overlap them.
    These prefix-sums will be useful when we will have sorted triangles based on their overlap order and we
    want to know which triangles overlap with given cell fast.

    This is the implementation of Blelloch parallel prefix scan algorithm.

    @author Pavol Kycina
*/
void main() {
    int two_powered = 1 << level; // pre-compute
    uint first_index = gl_GlobalInvocationID.x * two_powered * 2 + two_powered - 1;
    uint second_index = min(first_index + two_powered, cell_elements.length() - 1);

    if(first_index >= second_index) {
        return;
    }

    if(upsweep == 1) {
        // up-sweep stage of Blelloch algorithm
        cell_elements[second_index] += cell_elements[first_index];
    } else {
        // down-sweep stage of Blelloch algorithm
        int temp = cell_elements[first_index];
        cell_elements[first_index] = cell_elements[second_index];
        cell_elements[second_index] += temp;
    }
}