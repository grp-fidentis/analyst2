#version 430

struct Vertex {
    vec3 position;
    vec3 normal;
};

struct TriangleReductionData {
    vec3 min_corner;
    vec3 max_corner;
    float edges_len_sum;
};

layout(local_size_x = 64) in;

layout(std430, binding = 1) readonly buffer inputVerticesBuffer {
    Vertex vertices[];
};

layout(std430, binding = 2) readonly buffer inputTrianglesBuffer {
    ivec3 triangles[];
};

layout(std430, binding = 8) writeonly buffer triangleStatsBuffer {
    TriangleReductionData triangle_stats[];
};

/*
    Calculate min/max bounding box corner of the triangle and sum it's edge lengths.
    These data will be required for building uniform grid.

    @author Pavol Kycina
*/
void main() {
    const uint gid = min(gl_GlobalInvocationID.x, triangles.length() - 1);

    ivec3 triangle_vertex_indices = triangles[gid];

    vec3 v1 = vertices[triangle_vertex_indices.x].position;
    vec3 v2 = vertices[triangle_vertex_indices.y].position;
    vec3 v3 = vertices[triangle_vertex_indices.z].position;

    // calculate sum of edges + min bound + max bound
    vec3 triangle_corner_min = min(min(v1, v2), v3);
    vec3 triangle_corner_max = max(max(v1, v2), v3);
    float edges_len_sum = distance(v1, v2) + distance(v2, v3) + distance(v3, v1);

    triangle_stats[gid] = TriangleReductionData(triangle_corner_min, triangle_corner_max, edges_len_sum);
}