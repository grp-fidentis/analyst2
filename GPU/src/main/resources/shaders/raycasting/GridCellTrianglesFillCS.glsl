#version 430

struct GridInfo {
    ivec3 size;
    ivec3 origin;
    float cell_size;
};

struct Vertex {
    vec3 position;
    vec3 normal;
};

layout(local_size_x = 64) in;

layout(std430, binding = 1) readonly buffer inputVerticesBuffer {
    Vertex vertices[];
};

layout(std430, binding = 2) readonly buffer inputIndicesBuffer {
    ivec3 triangles[];
};

layout(std430, binding = 5) readonly buffer inputGridInfoBuffer {
    GridInfo grid_info;
};

layout(std430, binding = 7) buffer cellElementsBuffer {
    int cell_elements[];
};

layout(std430, binding = 3) writeonly buffer inputTrianglesBuffer {
    int sorted_triangles[];
};

layout(std430, binding = 4) readonly buffer inputCellBoundsBuffer {
    int cells_bounds[];
};

ivec3 locationToCell(vec3 loc) {
    return ivec3(floor(loc / grid_info.cell_size));
}

/*
    For each cell finds which triangles overlap it and add them to the sorted triangles.
    These triangles will be ordered, so the first triangles in the array will belong to the first non-empty cell in grid.
    Ranges for each cell are given in cell bounds array.

    This is very similar to the pre-construction phase algorithm, but this time we have allocated space for sorted triangles
    and we know where to put them in parallel based on given cell bounds.

    related paper: http://www.ce.uniroma2.it/publications/parco2013_uniformgrids.pdf

    @author Pavol Kycina
*/
void main() {
    if(gl_GlobalInvocationID.x >= triangles.length()) {
        return;
    }

    ivec3 triangle_vertex_indices = triangles[gl_GlobalInvocationID.x];

    vec3 v1 = vertices[triangle_vertex_indices.x].position;
    vec3 v2 = vertices[triangle_vertex_indices.y].position;
    vec3 v3 = vertices[triangle_vertex_indices.z].position;

    ivec3 min_corner_normalized = locationToCell(min(min(v1, v2), v3)) - grid_info.origin;
    ivec3 max_corner_normalized = locationToCell(max(max(v1, v2), v3)) - grid_info.origin;

    for(int x = min_corner_normalized.x; x <= max_corner_normalized.x; x++) {
        for(int y = min_corner_normalized.y; y <= max_corner_normalized.y; y++) {
            for (int z = min_corner_normalized.z; z <= max_corner_normalized.z; z++) {
                int cell_index = x * grid_info.size.y * grid_info.size.z + y * grid_info.size.z + z;
                int offset = atomicAdd(cell_elements[cell_index], 1);
                sorted_triangles[cells_bounds[cell_index] + offset] = int(gl_GlobalInvocationID.x);
            }
        }
    }
}