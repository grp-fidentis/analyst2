#version 430 compatibility
out vec3 position;
out vec3 originalPosition;
out vec3 normal;
out vec2 textureCoord;
out vec4 shadowCoord;
out vec4 heatmapDiffuse;

uniform mat4 lightModelViewMatrix;
uniform mat4 lightProjectionMatrix;

// Based on shader by Katarina Furmanova available at https://github.com/Fidentis/Analyst/blob/master/Renderer/src/cz/fidentis/renderer/shaders/FirstPassVS.glsl
void main() {
        originalPosition = vec3(gl_Vertex);
        position = vec3(gl_ModelViewMatrix * gl_Vertex);
        normal = normalize(gl_NormalMatrix * gl_Normal);
        heatmapDiffuse = gl_Color;
        textureCoord = gl_MultiTexCoord0.st;
        shadowCoord = lightProjectionMatrix * lightModelViewMatrix * gl_Vertex;

        gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}