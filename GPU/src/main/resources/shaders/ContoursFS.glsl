#version 420 compatibility
layout (binding = 1, r32ui) uniform uimage2D head_pointer_image;
layout (binding = 2, rgba32ui) uniform uimageBuffer list_buffer;

layout (location = 0) out vec4 color;

// Based on shader by Katarina Furmanova available at https://github.com/Fidentis/Analyst/blob/master/Renderer/src/cz/fidentis/renderer/shaders/FinalPassFS.glsl

// This shader draws contours of faces intersections. For a pixel to be considered an intersection, it needs to
// have at least one neighbour which satisfies two conditions: it has to belong to the other model and be close to the
// processed fragment.
void main(void) {
    vec4 final_color;
    ivec2 coords = ivec2(gl_FragCoord).xy;
    uvec4 fragment = imageLoad(list_buffer, int(imageLoad(head_pointer_image, coords).x));
    uint model = uint(unpackUnorm4x8(fragment.w).x);
    bool crossing = false;

    // search for an intersection in a 3x3 neighbourhood
    if (fragment.x != 0) {
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                uint index = imageLoad(head_pointer_image, ivec2(coords.x+i, coords.y+j)).x;
                uvec4 neighbour = imageLoad(list_buffer, int(index));

                if (neighbour.x != 0) {
                    uint neighbourModel = uint(unpackUnorm4x8(neighbour.w).x);
                    // the nearest neighbour fragment belongs to different face than the computed one and they are close
                    if (neighbourModel != model && abs(uintBitsToFloat(fragment.z)-uintBitsToFloat(neighbour.z)) < 0.000002f) {
                        crossing = true;
                    }
                }
            }
        }
    }
     
    if (crossing) {
        final_color = vec4(vec3(0), 1);
    } else {
        discard;
    }

    color = final_color;
}