#version 430 compatibility
// position of the vertex in the world coordinates
in vec3 originalPosition;

out vec4 color;

// buffer of information about glyphs: position and principal curvature directions
layout(std430, binding = 4) buffer sampleBuffer {
    int sampleCount;
    float sampleData[];
};

// Based on shader by Katarina Furmanova available at https://github.com/Fidentis/Analyst/blob/master/Renderer/src/cz/fidentis/renderer/shaders/ShadowMapFS.glsl

// This shader draws glyphs from the perspective of the light. The depth of this render is to be used to draw the shadow
// of the glyphs later down the rendering pipeline.
float vertexDistanceFromPlane(vec3 vertex, vec3 planePoint, vec3 planeNormal) {
        vec3 w = vec3(vertex-planePoint);

        float D = dot(planeNormal,planeNormal);
        float N = -dot(w,planeNormal);

        // they are not parallel
        // compute intersect param
        float sI = N / D;

        vec3 intersection = vec3(planeNormal*sI + vertex);

        return distance(intersection,vertex);
}

// find if the fragment is part of a glyph cross from the postion of the light; the rendered result is then used as a shadow texture
void main() {
     float isCross = 1.0f;
     for(int i = 0; i < sampleCount; i++){
         vec3 vertex = vec3(sampleData[12*i], sampleData[12*i+1], sampleData[12*i+2]);
         vec3 normal = vec3(sampleData[12*i+3], sampleData[12*i+4], sampleData[12*i+5]); // made redundant
         vec3 curvatureMax = vec3(sampleData[12*i+6], sampleData[12*i+7], sampleData[12*i+8]);
         vec3 curvatureMin = vec3(sampleData[12*i+9], sampleData[12*i+10], sampleData[12*i+11]);
         
          if(distance(vertex, originalPosition) < 20f){
              if ((
              vertexDistanceFromPlane(originalPosition, vertex, curvatureMin) < 1f && vertexDistanceFromPlane(originalPosition, vertex, curvatureMax) < 5f) || (
              vertexDistanceFromPlane(originalPosition, vertex, curvatureMax) < 1f && vertexDistanceFromPlane(originalPosition, vertex, curvatureMin) < 3f))
                {
                 isCross = 0.0f;
                 break;
                }
             }
         }

    if(isCross == 1.0f){
        discard;
    } else {
        color = vec4(0f, 0f, 0f, 1f);
    }
}