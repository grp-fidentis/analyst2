#version 430 compatibility
in vec3 position;
in vec3 normal;
in vec3 originalPosition;
in vec2 textureCoord;
in vec4 shadowCoord;
in vec4 heatmapDiffuse;

uniform int modelNumber;
uniform int isFace;
uniform int materialMode;
uniform int renderGlyphs;

layout (early_fragment_tests) in;
layout (binding = 0) uniform sampler2D face_texture;
layout (binding = 0, offset = 0) uniform atomic_uint index_counter;
// image of pointers pointing at the first fragments in thevlist buffer
layout (binding = 1, r32ui) uniform uimage2D head_pointer_image;
// linked list of proccesed fragments
layout (binding = 2, rgba32ui) uniform uimageBuffer list_buffer;
// texture of glyph shadows
layout (binding = 3) uniform sampler2D shadow_texture;
// buffer of information about glyphs: position and principal curvature directions
layout(std430, binding = 4) buffer sampleBuffer {
    int sampleCount;
    float sampleData[];
};

// shade fragment according to the Phong shading model https://github.com/Fidentis/Analyst/blob/master/Renderer/src/cz/fidentis/renderer/shaders/FirstPassFS.glsl

// Shades a fragment and stores information about it in a buffer(list_buffer). The buffer consists of the linked lists
// of fragment data. Each linked list contains fragmentof the same screen position.
// The fragment data structure includes: link to the next data entry (item.x), fragment color (item.y), depth value (item.z),
// id of the object the fragment belongs to (item.w.x), is the fragment part of a glyph cross (item.w.y),
// does the fragment lie in a shadow (item.w.x), does the fragment come from a DrawableFace (item.w.w)
float vertexDistanceFromPlane(vec3 vertex, vec3 planePoint, vec3 planeNormal) {
    vec3 w = vec3(vertex-planePoint);

    float D = dot(planeNormal,planeNormal);
    float N = -dot(w,planeNormal);

    // they are not parallel
    // compute intersect param
    float sI = N / D;

    vec3 intersection = vec3(planeNormal*sI + vertex);

    return distance(intersection,vertex);
}

vec4 shadeFragment() {
    vec3 l = normalize(gl_LightSource[0].position.xyz - position);
    vec4 diffuse = vec4(0);

    switch (materialMode) {
        case 0:
        diffuse = gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * max(0.0, dot(abs(normal), l));
        break;
        case 1:
        diffuse = gl_LightSource[0].diffuse * texture(face_texture, textureCoord) * max(0.0, dot(abs(normal), l));
        break;
        case 2:
        diffuse = gl_LightSource[0].diffuse * heatmapDiffuse * max(0.0, dot(abs(normal), l));
        break;
    }
    vec4 ambient = gl_LightSource[0].ambient * gl_FrontMaterial.ambient;

    vec3 c = normalize(-position);
    vec3 s = normalize(c + l);
    vec4 specular = gl_LightSource[0].specular * gl_FrontMaterial.specular * pow(max(dot(/*abs(*/normal/*)*/, s), 0.0), gl_FrontMaterial.shininess);

    vec4 color = diffuse + ambient*2 + specular;
    return vec4(color.rgb, gl_FrontMaterial.diffuse.a);
}

void main(void) {
    uint old_head;
    vec4 frag_color = shadeFragment();

    uint index = atomicCounterIncrement(index_counter);
    old_head = imageAtomicExchange(head_pointer_image, ivec2(gl_FragCoord.xy), int(index));

    float shadow = 1;
    float isCross = 1;

    // find if fragment is part of glyph cross or glyph's shadow
    if (isFace == 1 && renderGlyphs == 1) {

        // does fragment lie in shadow?
        vec3 shadowCoordProjected = shadowCoord.xyz / shadowCoord.w;
        shadowCoordProjected = shadowCoordProjected * 0.5 + 0.5;
        float distanceFromLight = texture2D(shadow_texture, shadowCoordProjected.st).z;
        shadowCoordProjected.z -= 0.0005;
        if (shadowCoord.w > 0.0){
            shadow = distanceFromLight < shadowCoordProjected.z ? 0.5 : 1.0;
        }

        // is fragment part of glyph's cross?
        for (int i=0; i < sampleCount; i++){
            vec3 vertex = vec3(sampleData[12*i], sampleData[12*i+1], sampleData[12*i+2]);
            vec3 norm = vec3(sampleData[12*i+3], sampleData[12*i+4], sampleData[12*i+5]); // made redundant
            vec3 curvatureMax = vec3(sampleData[12*i+6], sampleData[12*i+7], sampleData[12*i+8]);
            vec3 curvatureMin = vec3(sampleData[12*i+9], sampleData[12*i+10], sampleData[12*i+11]);

            if (distance(vertex, originalPosition) < 20) {
                if ((vertexDistanceFromPlane(originalPosition, vertex, curvatureMin) < 1
                        && vertexDistanceFromPlane(originalPosition, vertex, curvatureMax) < 5)
                    || (vertexDistanceFromPlane(originalPosition, vertex, curvatureMax) < 1
                        && vertexDistanceFromPlane(originalPosition, vertex, curvatureMin) < 3))
                {
                    isCross = 0;
                    break;
                }
            }
        }
    }

    // pack information about the fragment
    uvec4 item;
    // the pointer to the next fragment at given screen position
    item.x = old_head;
    // the color
    item.y = packUnorm4x8(frag_color);
    // the fragment's depth
    item.z = floatBitsToUint(gl_FragCoord.z);
    // the object's id, is it a part of a glyph, is it in shadow, is it a fragment of a human face
    item.w = packUnorm4x8(vec4(modelNumber, isCross, shadow, isFace));

    memoryBarrier();
    imageStore(list_buffer, int(index), item);
}