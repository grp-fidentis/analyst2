#version 420 compatibility
uniform vec4 fogColor;
uniform int fogVersion;
uniform float minZ;
uniform float maxZ;
uniform bool innerSurfaceSolid;

// The per-pixel image containing pointers to the first fragment in the list_buffer
layout (binding = 1, r32ui) uniform uimage2D head_pointer_image;
// Buffer containing linked lists of fragments
layout (binding = 2, rgba32ui) uniform uimageBuffer list_buffer;

// This is the output color
layout (location = 0) out vec4 color;

// The maximum number of overlapping fragments allowed
#define MAX_FRAGMENTS 15

// Temporary array used for sorting fragments
uvec4 fragment_list[MAX_FRAGMENTS];
uint fragment_pointer[MAX_FRAGMENTS];
uint face_frag_pointer[MAX_FRAGMENTS];

// Based on shader by Katarina Furmanova available at https://github.com/Fidentis/Analyst/blob/master/Renderer/src/cz/fidentis/renderer/shaders/OITresultFS.glsl

// A shader mixing the final color of a fragment based on transparency and optionally fog simulation and glyph ownership.
// First, the program sorts the fragments based on their depth. Then it modifies the fragment color based on the
// selected technique and makes the fragment opaque if it belongs to a glyph cross. If the option innerSurfaceOpaque is
// enabled, the program makes the inner parts of faces opaque. Finally, it mixes the fragment colors together satisfying
// correct transparency to create the final color.

void main(void) {
    // get all fragments on this screen space position
    uint current_index;
    uint fragment_count = 0;

    current_index = imageLoad(head_pointer_image, ivec2(gl_FragCoord).xy).x;

    while (current_index != 0 && fragment_count < MAX_FRAGMENTS)
    {
        uvec4 fragment = imageLoad(list_buffer, int(current_index));
        fragment_list[fragment_count] = fragment;
        fragment_pointer[fragment_count] = current_index;
        current_index = fragment.x;
        fragment_count++;
    }

    // sort the fragments by depth, highest fragment first
    uint i, j;
    if (fragment_count > 1) {
        for (i = 0; i < fragment_count - 1; i++) {
            for (j = i + 1; j < fragment_count; j++) {
                uvec4 fragment1 = fragment_list[i];
                uvec4 fragment2 = fragment_list[j];

                float depth1 = uintBitsToFloat(fragment1.z);
                float depth2 = uintBitsToFloat(fragment2.z);

                if (depth1 < depth2)
                {
                    fragment_list[i] = fragment2;
                    fragment_list[j] = fragment1;

                    uint temp = fragment_pointer[i];
                    fragment_pointer[i] = fragment_pointer[j];
                    fragment_pointer[j] = temp;
                }
            }
        }
    }

    //find fragments that belong to faces
    uint face_frag_count = 0;

    for (i = 0; i < fragment_count; i++){
        if (uint(unpackUnorm4x8(fragment_list[i].w).w) == 1) {
            face_frag_pointer[face_frag_count] = i;
            face_frag_count++;
        }
    }

    // modulate the color and opacity of individual fragments based on fog simulation and glyphs

    // set the fragment opaque, if it is a part of glyph cross
    if (face_frag_count == 1) {
        vec4 modulator = unpackUnorm4x8(fragment_list[fragment_count-1].y);
        if (uint(unpackUnorm4x8(fragment_list[fragment_count-1].w).y) == 0) {
            modulator.a = 1;
        }
        fragment_list[fragment_count-1].y = packUnorm4x8(modulator);
    }

   if (face_frag_count > 1) {
       vec4 modulator = unpackUnorm4x8(fragment_list[face_frag_pointer[face_frag_count-1]].y);
       uint lastModel = uint(unpackUnorm4x8(fragment_list[face_frag_pointer[face_frag_count-1]].w).x);
       uint sameFragCount = 1;

       // simulating fog by mapping color to the front face
       if (fogVersion == 1 && unpackUnorm4x8(fragment_list[fragment_count-1].w).y == 1) {
           modulator = mix(modulator,fogColor,5*abs(uintBitsToFloat(fragment_list[face_frag_pointer[face_frag_count-1]].z)-
               uintBitsToFloat(fragment_list[face_frag_pointer[face_frag_count-2]].z))/abs(minZ-maxZ));
       }

       // simulating fog by mapping opacity to the front face
       else if (fogVersion == 2 && unpackUnorm4x8(fragment_list[fragment_count-1].w).y == 1) {
           modulator.a = 0.2f*modulator.a+8*abs(uintBitsToFloat(fragment_list[face_frag_pointer[face_frag_count-1]].z)-
                    uintBitsToFloat(fragment_list[face_frag_pointer[face_frag_count-2]].z))/abs(minZ-maxZ);
           modulator = vec4(fogColor.rgb,modulator.a);
       }

       // set the fragment opaque, if it is a part of glyph cross
       if (uint(unpackUnorm4x8(fragment_list[face_frag_pointer[face_frag_count-1]].w).y) == 0) {
           modulator.a = 1;
       }
       fragment_list[face_frag_pointer[face_frag_count-1]].y = packUnorm4x8(modulator);

       for (i = 0; i < face_frag_count-1; i++) {
           j = face_frag_count - 2 - i;

           // the fragment belongs to the same model as the last
           if (uint(unpackUnorm4x8(fragment_list[face_frag_pointer[j]].w).x) == lastModel) {

               // set the fragment opaque, if it is a part of glyph cross
               vec4 modulator = unpackUnorm4x8(fragment_list[face_frag_pointer[j]].y);
               if (uint(unpackUnorm4x8(fragment_list[face_frag_pointer[j]].w).y)==0) {
                   modulator.a = 1;
               }

               // simulating fog by mapping color to the back face
               if (fogVersion == 3 && mod(sameFragCount,2) == 1) {
                   modulator = mix(modulator,fogColor,5*abs(uintBitsToFloat(fragment_list[face_frag_pointer[j]].z)-
                        uintBitsToFloat(fragment_list[face_frag_pointer[j+1]].z))/abs(minZ-maxZ));

               }
               fragment_list[face_frag_pointer[j]].y = packUnorm4x8(modulator);
               sameFragCount ++;

           // the fragment belongs to the other model to the last
           } else {
               lastModel = uint(unpackUnorm4x8(fragment_list[face_frag_pointer[j]].w).x);

               if (mod(sameFragCount,2) == 1 && innerSurfaceSolid) {
                   vec4 modulator = unpackUnorm4x8(fragment_list[face_frag_pointer[j]].y);
                   modulator = vec4(modulator.rgb*unpackUnorm4x8(fragment_list[face_frag_pointer[j]].w).z, 1);
                   // simulating fog by mapping color to the back face
                   if (fogVersion == 3) {
                       modulator = mix(modulator,fogColor,5*abs(uintBitsToFloat(fragment_list[face_frag_pointer[j]].z)-
                            uintBitsToFloat(fragment_list[face_frag_pointer[j+1]].z))/abs(minZ-maxZ));
                   }
                   fragment_list[face_frag_pointer[j]].y = packUnorm4x8(modulator);
               }
               sameFragCount = 1;
           }
       }
   }

    if (fragment_count > 0) {
       imageAtomicExchange(head_pointer_image, ivec2(gl_FragCoord.xy), fragment_pointer[face_frag_pointer[face_frag_count-1]]);
    }

    // mix all the fragments
    vec4 final_color = vec4(vec3(1), 0);
    for (i = 0; i < fragment_count; i++) {
        //store the list in reverse order(nearest fragment first); used for contours
        if (i < fragment_count-1) {
            fragment_list[fragment_count-i-1].x = fragment_pointer[fragment_count-i-2];
        } else {
            fragment_list[0].x = 0;
        }
        imageStore(list_buffer, int(fragment_pointer[fragment_count-i-1]), fragment_list[fragment_count-i-1]);

        vec4 modulator = unpackUnorm4x8(fragment_list[i].y);
        final_color = mix(final_color, modulator, modulator.a);
    }

   color = final_color;
}