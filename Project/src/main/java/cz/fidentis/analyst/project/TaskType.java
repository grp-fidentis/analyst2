package cz.fidentis.analyst.project;

/**
 * @author Ondřej Bazala
 * @since 04.09.2023
 */
public enum TaskType {
    SINGLE_FACE_ANALYSIS,
    PAIR_COMPARISON,
    BATCH_PROCESSING;

    /**
     * Returns task type based on the number of faces.
     *
     * @param numberOfFaces Number of faces
     * @return task type
     */
    public static TaskType getTaskTypeForNumberOfFaces(int numberOfFaces) {
        if (numberOfFaces == 1) {
            return SINGLE_FACE_ANALYSIS;
        } else if (numberOfFaces == 2) {
            return PAIR_COMPARISON;
        } else {
            return BATCH_PROCESSING;
        }
    }
}
