package cz.fidentis.analyst.project.impl;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskService;
import cz.fidentis.analyst.project.TaskType;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author Marek Seďa
 */
public class TaskServiceImpl implements TaskService {

    @Override
    public FaceReference getAvgFace(Task task) {
        return task.getAverageFace();
    }

    @Override
    public FaceReference getPrimaryFace(Task task) {
        return task.getFaces().stream().filter(it -> !it.isAverageFace()).toList().getFirst();
    }

    @Override
    public FaceReference getSecondaryFace(Task task) {
        List<FaceReference> faces = task.getFaces().stream().filter(it -> !it.isAverageFace()).toList();
        if (faces.size() > 1) {
            return faces.get(1);
        } else {
            return null;
        }
    }

    @Override
    public void replaceAverageFace(Task task, FaceReference faceReference) {
        List<FaceReference> newFaceReferences = task.getFaces().stream().filter(it -> !it.isAverageFace()).collect(Collectors.toList());
        if (faceReference != null) {
            newFaceReferences.add(faceReference);
        }
        task.setFaces(newFaceReferences);
    }

    @Override
    public Task createTask(List<FaceReference> references) {
        List<FaceReference> referenceCopies = references.stream().map(FaceService.INSTANCE::createCopyOfFace).toList();
        return new TaskImpl(getTaskDefaultName(references), TaskType.getTaskTypeForNumberOfFaces(references.size()), referenceCopies);
    }


    private String getTaskDefaultName(List<FaceReference> references) {
        if (references.isEmpty()) {
            return "";
        } else if (references.size() == 1) {
            return references.get(0).getName();
        } else if (references.size() == 2) {
            return references.get(0).getName()
                    + ":"
                    + references.get(1).getName();
        } else {
            return references.size() + " faces";
        }

    }
}
