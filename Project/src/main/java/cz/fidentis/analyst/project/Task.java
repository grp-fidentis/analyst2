package cz.fidentis.analyst.project;

import cz.fidentis.analyst.data.face.FaceReference;

import java.io.Serializable;
import java.util.List;

/**
 * Class representing single task
 * @author Marek Seďa
 */
public interface Task extends Serializable {

    /**
     * @return task's name
     */
    String getName();

    /**
     *
     * @return task's type
     */
    TaskType getType();

    /**
     *
     * @param faces
     */
    void setFaces(List<FaceReference> faces);

    /**
     * Return reference to average face
     * @return reference to average face
     */
    FaceReference getAverageFace();

    /**
     * @return face references
     */
    List<FaceReference> getFaces();

}
