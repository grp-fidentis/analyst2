package cz.fidentis.analyst.project;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.project.impl.TaskServiceImpl;

import java.util.List;

/**
 * @author Marek Seďa
 */
public interface TaskService {

    TaskServiceImpl INSTANCE = new TaskServiceImpl();

     /**
     * Returns the average human face or {@code null}
     * @return the average human face or {@code null}
     */
     FaceReference getAvgFace(Task task);

     /**
      * Creates a task
      * @param references References to faces
      * @return the task
      */
     Task createTask(List<FaceReference> references);

     /**
      * Replaces the average face
      * @param task Task
      * @param faceReference Reference to new average face
      */
     void replaceAverageFace(Task task, FaceReference faceReference);

     /**
      * Returns primary face
      * @param task Task
      * @return primary face
      */
     FaceReference getPrimaryFace(Task task);

     /**
      * Returns secondary face
      * @param task Task
      * @return secondary face
      */
     FaceReference getSecondaryFace(Task task);
}
