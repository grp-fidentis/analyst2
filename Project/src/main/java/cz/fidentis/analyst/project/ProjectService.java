package cz.fidentis.analyst.project;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.project.impl.ProjectServiceImpl;

import java.io.File;
import java.io.IOException;
import java.util.function.Function;

/**
 * @author Marek Seďa
 */
public interface ProjectService {

    ProjectService INSTANCE = new ProjectServiceImpl();

    /**
     * Saves the project's state into the project's folder.
     * This method writes info about project's faces into the JSON file
     * with the name {@code PROJECT_FILE_NAME} located in the project's folder.
     * This method doesn't write tasks on disk.
     *
     * @return {@code true} if the project was successfully written
     * @throws IOException on IO errors
     */
    boolean saveProject(Function<Integer, Void> progressCallback) throws IOException;

    /**
     * Re-initializes an existing project.
     *
     * @param path Path to the project's dir
     * @throws java.io.IOException on IO error
     */
    Project loadProject(File path, Function<Integer, Void> progressCallback) throws IOException;

    /**
     * Returns a path to the JSON project file
     *
     * @param dir Project's folder
     * @return path to the JSON project file
     */
    File getProjectFile(File dir);

    /**
     * Creates a new project
     * @return a new project
     */
    Project createNewProject();

    /**
     * Return opened project
     * @return opened project
     */
    Project getOpenedProject();

    /**
     * Returns project's recent directory
     * @return project's recent directory
     */
    File getRecentProjectDir();

    /**
     * Returns face reference
     * @param faceName Face name
     * @return face reference
     */
    FaceReference getFaceReferenceByName(String faceName);

    /**
     * Adds a task
     * @param task task
     * @return result
     */
    boolean addTask(Task task);

    /**
     * Removes task
     * @param task task
     * @return result
     */
    boolean removeTask(Task task);

    /**
     * Adds a face
     * @param faceReference face reference
     * @return result
     */
    boolean addFace(FaceReference faceReference);

    /**
     * Removes face
     * @param faceReference face reference
     * @return result
     */
    boolean removeFaceFromProject(FaceReference faceReference);

}
