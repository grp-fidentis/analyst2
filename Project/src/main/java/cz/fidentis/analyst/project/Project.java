package cz.fidentis.analyst.project;

import cz.fidentis.analyst.data.face.FaceReference;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * @author Marek Seďa
 */
public interface Project extends Serializable {

    /**
     * Returns project's name decoded from the name of the project's folder.
     * If the project is not yet saved then {@code DEFAULT_PROJECT_NAME} is used.
     *
     * @return project's name
     */
    String getName();

    /**
     * Determines whether the project has changed and the should be saved.
     * @return {@code true} if the project has changed and the should be saved.
     */
    boolean changed();

    /**
     * Sets the project's modification indicator
     * @param changed modification indicator
     */
    void setChanged(boolean changed);

    /**
     * Returns a path to the last stored project file folder
     *
     * @return a path to the last stored project file folder or {@code null}
     */

    /**
     *
     * @return face references
     */
    List<Task> getTasks();

    /**
     *
     * @return
     */
    List<FaceReference> getFaces();

    /**
     * Determines whether the project has assigned a folder (has been saved in the past).
     * @return {@code true} if the project has assigned a folder (has been saved in the past)
     */
    boolean hasProjectDir();

    /**
     * Returns project's folder or {@code null}
     * @return project's folder or {@code null}
     */
    File getProjectDir();

    /**
     * Sets project's folder
     * @param path Path to the folder
     * @return {@code true} if the given path directs to existing folder
     */
    boolean setProjectDir(File path);

}
