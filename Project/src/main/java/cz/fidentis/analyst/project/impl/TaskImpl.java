package cz.fidentis.analyst.project.impl;

import com.google.common.collect.ImmutableList;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.project.Task;
import cz.fidentis.analyst.project.TaskType;

import java.util.List;

/**
 * @author Marek Seďa
 */
public class TaskImpl implements Task {
    private final String name;
    private final TaskType type;
    private List<FaceReference> references;

    /**
     * Constructor.
     * @param name Task name
     * @param type Task type
     * @param references References to faces
     */
    public TaskImpl(String name, TaskType type, List<FaceReference> references) {
        this.name = name;
        this.type = type;
        this.references = references;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public TaskType getType() {
        return type;
    }

    @Override
    public void setFaces(List<FaceReference> faces) {
        this.references = faces;
    }

    @Override
    public List<FaceReference> getFaces() {
        return ImmutableList.copyOf(references);
    }

    @Override
    public FaceReference getAverageFace() {
        for (FaceReference reference : references) {
            if (reference.isAverageFace()) {
                return reference;
            }
        }
        return null;
    }
}
