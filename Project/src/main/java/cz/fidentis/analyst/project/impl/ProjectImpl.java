package cz.fidentis.analyst.project.impl;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.project.Project;
import cz.fidentis.analyst.project.Task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A FIDENTIS project.
 *
 * @author Matej Kovar
 * @author Radek Oslejsek
 */
public class ProjectImpl implements Project {
    
    private transient boolean changed = false;
    
    /**
     * Project's tasks
     */
    private final List<Task> tasks;

    /**
     * Project's tasks
     */
    private final List<FaceReference> faceReferences;

    /**
     * Path to the project directory
     */
    private File dir;
    

    /**
     * Default name of a new unsaved project
     */
    private static final String DEFAULT_PROJECT_NAME = "New Project";

    /**
     * Constructor for a new empty project.
     */
    public ProjectImpl() {
        this.tasks = new ArrayList<>();
        this.faceReferences = new ArrayList<>();
    }
    
    /**
     * Constructor for an existing project.
     * 
     * @param path Path to the project's dir
     * @throws java.io.IOException on IO error
     */
    public ProjectImpl(File path, List<FaceReference> faceReferences, List<Task> tasks) throws IOException {
        this.dir = path;
        this.tasks = tasks;
        this.faceReferences = faceReferences;
    }

    @Override
    public String getName() {
        if (hasProjectDir()) {
            String name = dir.toString().substring(
                    dir.toString().lastIndexOf(File.separatorChar) + 1,
                    dir.toString().length()
            );
            if (name.endsWith(File.separator)) {
                name = name.substring(0, name.length()-1);
            }
            return name;
        } else {
            return DEFAULT_PROJECT_NAME;
        }
    }

    @Override
    public boolean changed() {
        return changed;
    }

    @Override
    public boolean hasProjectDir() {
        return dir != null;
    }

    @Override
    public File getProjectDir() {
        return this.dir;
    }

    @Override
    public boolean setProjectDir(File path) {
        if (path == null || !path.isDirectory()) {
            return false;
        } else {
            this.dir = path;
            return true;
        }
    }

    @Override
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public List<FaceReference> getFaces() {
        return faceReferences;
    }

    @Override
    public List<Task> getTasks() {
        return tasks;
    }
}
