package cz.fidentis.analyst.project.impl;

import com.google.common.collect.Lists;
import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.data.face.FaceService;
import cz.fidentis.analyst.data.face.HumanFace;
import cz.fidentis.analyst.project.Project;
import cz.fidentis.analyst.project.ProjectService;
import cz.fidentis.analyst.project.Task;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.prefs.Preferences;

/**
 * @author Marek Seďa
 */
public class ProjectServiceImpl implements ProjectService {

    private Project project;

    /**
     * Name of the JSON file
     */
    private static final String PROJECT_FILE_NAME = "fidentis";

    /**
     * A preferences key for storing path to the recent project
     */
    private static final String RECENT_PROJECT_PREF_KEY = "pathToMostRecentProject";

    /**
     * Saves project.
     *
     * @param progressCallback callback function
     * @return result
     * @throws IOException
     */
    public boolean saveProject(Function<Integer, Void> progressCallback) throws IOException {
        if (!project.hasProjectDir()) {
            return false;
        }
        progressCallback.apply(0);
        File file = getProjectFile(project.getProjectDir());
        int facesCount = project.getFaces().size() + project.getTasks().stream().map(task -> task.getFaces().size()).reduce(0, Integer::sum);

        int savedFaces = 0;
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(project);
            for (FaceReference faceReference : project.getFaces()) {
                HumanFace humanFace = FaceService.INSTANCE.getFaceByReference(faceReference);
                oos.writeObject(humanFace);
                progressCallback.apply((int) Math.round(100.0 * ++savedFaces / facesCount));
            }
            for (Task task : project.getTasks()) {
                for (FaceReference faceReference : task.getFaces()) {
                    HumanFace humanFace = FaceService.INSTANCE.getFaceByReference(faceReference);
                    oos.writeObject(humanFace);
                    progressCallback.apply((int) Math.round(100.0 * ++savedFaces / facesCount));
                }
            }
        }


        Preferences userPreferences = Preferences.userNodeForPackage(ProjectImpl.class);
        userPreferences.put(RECENT_PROJECT_PREF_KEY, file.getAbsolutePath());
        project.setChanged(false);
        return true;
    }

    /**
     * Loads project.
     *
     * @param path Path to the project's dir
     * @param progressCallback Callback function
     * @return Project
     * @throws IOException on failure
     */
    public Project loadProject(File path, Function<Integer, Void> progressCallback) throws IOException {
        File file = getProjectFile(path);
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            Project newProject = (Project) ois.readObject();
            int facesCount = newProject.getFaces().size() + newProject.getTasks().stream().map(task -> task.getFaces().size()).reduce(0, Integer::sum);

            int loadedFaces = 0;
            List<HumanFace> faces = new ArrayList<>();
            for (FaceReference ignored : newProject.getFaces()) {
                faces.add((HumanFace) ois.readObject());
                progressCallback.apply((int) Math.round(100.0 * ++loadedFaces / facesCount));
            }
            for (Task task : newProject.getTasks()) {
                for (FaceReference ignored : task.getFaces()) {
                    faces.add((HumanFace) ois.readObject());
                    progressCallback.apply((int) Math.round(100.0 * ++loadedFaces / facesCount));
                }
            }
            FaceService.INSTANCE.replaceFaces(faces);
            project = newProject;
        } catch (ClassNotFoundException e) {
            throw new IOException(e);
        }
        Preferences userPreferences = Preferences.userNodeForPackage(ProjectImpl.class);
        userPreferences.put(RECENT_PROJECT_PREF_KEY, file.getAbsolutePath());
        return project;
    }

    /**
     * Returns project file
     * @param dir Project's folder
     * @return project file
     */
    public File getProjectFile(File dir) {
        return new File(dir, PROJECT_FILE_NAME + ".bin");
    }

    /**
     * Create new project
     * @return new project
     */
    public Project createNewProject() {
        try {
            project = new ProjectImpl();
            FaceService.INSTANCE.replaceFaces(Lists.newArrayList());
            return project;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Returns recent project's directory
     * @return recent project's directory
     */
    public File getRecentProjectDir() {
        Preferences userPreferences = Preferences.userNodeForPackage(ProjectImpl.class);
        String path = userPreferences.get(RECENT_PROJECT_PREF_KEY, "");
        if ("".equals(path)) {
            return null;
        }
        return new File(path.substring(0, path.lastIndexOf(File.separator)));
    }

    @Override
    public FaceReference getFaceReferenceByName(String faceName) {
        for (FaceReference face : project.getFaces()) {
            if (face.getName().equals(faceName)) {
                return face;
            }
        }
        return null;
    }

    @Override
    public boolean addFace(FaceReference faceReference) {
        project.setChanged(true);
        return project.getFaces().add(faceReference);
    }

    @Override
    public boolean addTask(Task task) {
        project.setChanged(true);
        return project.getTasks().add(task);
    }

    @Override
    public Project getOpenedProject() {
        return project;
    }

    @Override
    public boolean removeTask(Task task) {
        for (FaceReference faceReference : task.getFaces()) {
            FaceService.INSTANCE.removeFace(faceReference);
        }
        project.setChanged(true);
        return project.getTasks().remove(task);
    }

    @Override
    public boolean removeFaceFromProject(FaceReference faceReference) {
        return project.getFaces().remove(faceReference);
    }

}
