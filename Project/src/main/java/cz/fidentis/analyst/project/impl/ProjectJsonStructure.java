package cz.fidentis.analyst.project.impl;

import cz.fidentis.analyst.data.face.FaceReference;
import cz.fidentis.analyst.project.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class determine the structure of the JSON project configuration file.
 * It is used by Jackson serialization and deserialization.
 * All attributes have to have public getters and setters to be (de)serialized.
 *
 * @author Radek Oslejsek
 */
public class ProjectJsonStructure {

    private List<Task> tasks = new ArrayList<>();

    private List<FaceReference> faceReferences = new ArrayList<>();

    /**
     * Constructor from faces proxy used for serialization.
     * @param tasks Tasks of the project
     */
    public ProjectJsonStructure(List<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * An empty constructor required by Jackson deserialization.
     */
    public ProjectJsonStructure() {
    }

    /**
     * Returns a list of paths to project's faces
     * @return a list of paths to project's faces
     */
    public List<Task> getTasks() {
        return Collections.unmodifiableList(tasks);
    }

    /**
     * Setter used by Jackson deserialization.
     * @param tasks Tasks of the project
     */
    public void setTasks(List<Task> tasks) {
        this.tasks = new ArrayList<>(tasks);
    }

    public List<FaceReference> getFaceReferences() {
        return faceReferences;
    }

    public void setFaceReferences(List<FaceReference> faceReferences) {
        this.faceReferences = faceReferences;
    }
}
