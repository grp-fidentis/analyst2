package cz.fidentis.analyst.engines.symmetry;

import cz.fidentis.analyst.data.mesh.MeshVisitor;
import cz.fidentis.analyst.data.shapes.Plane;

/**
 * Stateful symmetry estimation visitor.
 * First, the visitor is created. Then, it is applied to mesh facets. Finally, the samples are returned.
 * 
 * @author Radek Oslejsek
 */
public abstract class SymmetryVisitor implements MeshVisitor {
   
    /**
     * Returns a symmetry plane.
     * @return a symmetry plane or {@code null}
     */
    public abstract Plane getSymmetryPlane();
}
