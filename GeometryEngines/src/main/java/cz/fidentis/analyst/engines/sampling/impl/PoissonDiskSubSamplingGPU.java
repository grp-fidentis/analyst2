package cz.fidentis.analyst.engines.sampling.impl;

import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLKernel;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.opencl.CLProgramDef;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.memory.BufferFactory;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.services.common.CommonKernelServices;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLConfig;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLServices;

import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This implementation overrides {@link PoissonDiskSubSampling} sample generation
 * to use GPU-based Ray Casting algorithm implemented in OpenCl.
 * Each instance creates new OpenCL context and is responsible for it's disposing before destruction.
 *
 * @author Karol Kovac
 * @author Marek Horský
 */
public class PoissonDiskSubSamplingGPU extends PoissonDiskSubSampling {
    // ################### PROJECTION SETTINGS ###################
    // maximal angle between projector normal and z-axis, defining range for rotations
    public static final double PROJECTOR_MAX_ANGLE = 90;
    // minimal rotation applied to projector each iteration
    public static final double PROJECTOR_MIN_ROTATION = 10;
    public static final int MIN_PROJECTION_ITERATIONS = 20;

    private CLContext clContext;
    private RayIntersectionOpenCLServices rayIntersectionOpenCLServices;
    private WriteBufferGPU<Point3d> meshCentroidBuffer;
    private WriteBufferGPU<Point3d> projectorBuffer;
    private WriteBufferGPU<Vector3d> directionBuffer;
    private OctreeOpenCL octree;
    private CLCommandQueue queue;

    /**
     * Constructor for the maximum number of samples. The density (minimal distance between samples)
     * is approximated so that the mean absolut percentage error (MAPE) of this approximation is ~ 5.27%, and the median
     * absolute percentage error (MDAPE) is ~ 4.52%.
     *
     * @param max       Required number of samples. Must be bigger than zero
     * @param smoothing Surface samples can be equipped by interpolated normal and or adapted position to
     *                  interpolate surface curvature.
     *                  Use {@code Smoothing.NONE} to simply get samples from triangle planes without normal vectors.
     * @throws IllegalArgumentException if the input parameter is wrong
     */
    public PoissonDiskSubSamplingGPU(int max, MeshTriangle.Smoothing smoothing) {
        super(max, smoothing);
    }

    @Override
    public List<MeshPoint> getSamples() {
        if (clContext == null) {
            initialize();
        }
        this.octree.build(facets);
        Box bbox = octree.getBBox();
        Point3d meshCentroid = bbox.midPoint();
        double projectorSize = bbox.diagonalLength();

        int iterationCount = 1;
        double currentProjectorAngleX = 0;
        double currentProjectorAngleY = 0;
        double newProjectorAngleX;
        double newProjectorAngleY;
        Set<MeshPoint> intersections;
        Set<MeshPoint> validSamples;

        // generate 2D poisson samples
        Point2d[][] grid = generatePoissonSamples(projectorSize);

        // build projector, place 2D poisson samples in space
        List<Point3d> projector = createProjectorFromGrid(grid, meshCentroid, projectorSize,
                bbox.diagonalLength() / 2.0);

        meshCentroidBuffer.putAll(List.of(meshCentroid));
        projectorBuffer.putAll(projector);
        directionBuffer.putAll(List.of(new Vector3d(0.0, 0.0, -1.0)));

        // first initial projection without projector rotation
        Set<MeshPoint> samples = projectorRaysOctreeIntersections(octree, projectorBuffer, directionBuffer);
        // build kd-tree, form initial projection intersections
        KdTree kdTree = KdTree.create(samples);

        // iteratively, rotate projector, cast rays, merge intersection
        do {
            // pick random projector angle within range
            do {
                newProjectorAngleX = PROJECTOR_MAX_ANGLE * (Math.random() - 0.5);
                newProjectorAngleY = PROJECTOR_MAX_ANGLE * (Math.random() - 0.5);
            } while (Math.abs(newProjectorAngleX - currentProjectorAngleX) < PROJECTOR_MIN_ROTATION &&
                    Math.abs(newProjectorAngleY - currentProjectorAngleY) < PROJECTOR_MIN_ROTATION);

            // ensure rotation isn't too small to increase chance of generating valid samples
            rotateProjectorPlaneOnGPU(clContext, projectorBuffer, meshCentroidBuffer, directionBuffer,
                    newProjectorAngleX - currentProjectorAngleX,
                    newProjectorAngleY - currentProjectorAngleY);

            // cast rays and filter intersections
            intersections = projectorRaysOctreeIntersections(octree, projectorBuffer, directionBuffer);
            validSamples = filterRaysIntersections(kdTree, intersections);

            // update structures
            iterationCount += 1;
            currentProjectorAngleX = newProjectorAngleX;
            currentProjectorAngleY = newProjectorAngleY;
            samples.addAll(validSamples);
            addSamplesToKdTree(validSamples, kdTree);

        } while (iterationCount <= MIN_PROJECTION_ITERATIONS || !validSamples.isEmpty());
        setRealSamples(samples.size());
        return new ArrayList<>(samples);
    }

    @Override
    public String toString() {
        return "Poisson disk sub-sampling (OpenCL)" + super.toString();
    }

    @Override
    public void dispose() {
        if (clContext != null && !clContext.isReleased()) {
            meshCentroidBuffer.release();
            projectorBuffer.release();
            directionBuffer.release();
            octree.release();
            rayIntersectionOpenCLServices.release();
            queue.release();
            OpenCLServices.release(clContext);
        }
    }

    private void initialize() {
        this.clContext = OpenCLServices.createContext();
        this.queue = clContext.getMaxFlopsDevice().createCommandQueue();
        this.rayIntersectionOpenCLServices = new RayIntersectionOpenCLServices(clContext,
                new RayIntersectionOpenCLConfig(smoothing, false));
        this.octree = OctreeOpenCL.create(clContext);
        this.meshCentroidBuffer = BufferFactory.getVoxelPointBuffer(clContext);
        this.projectorBuffer = BufferFactory.getVoxelPointBuffer(clContext);
        this.directionBuffer = BufferFactory.getVoxelVectorBuffer(clContext);
    }

    /**
     * Casts rays from projector calculate intersections with interpolated normals.
     *
     * @param projector projector samples to act as ray origins
     * @return rays intersections with the mesh
     */
    private Set<MeshPoint> projectorRaysOctreeIntersections(OctreeOpenCL octree, WriteBufferGPU<Point3d> projector, WriteBufferGPU<Vector3d> direction) {
        return rayIntersectionOpenCLServices.computeClosestPoints(octree, projector, direction);
    }

    /**
     * Rotates projector on GPU-side to minimize host-device communication
     *
     * @param clContext OpenCL context
     * @param projector buffer of projector samples to be rotated
     * @param centroid  center coordinate of mesh
     * @param direction Buffer to receive ray direction
     * @param thetaX    angle
     * @param thetaY    angle
     */
    private void rotateProjectorPlaneOnGPU(CLContext clContext, WriteBufferGPU<Point3d> projector, WriteBufferGPU<Point3d> centroid, WriteBufferGPU<Vector3d> direction, double thetaX, double thetaY) {
        // build 2 matrices, rotation around x,y-axis and combine them
        CLKernel rotateProjector = OpenCLServices.useProgram(clContext, CLProgramDef.POISSON_GPU_UTILS)
                .getKernel("rotateProjector");

        rotateProjector.putArg(projector.getCount())
                .putArg((float) Math.toRadians(thetaX))
                .putArg((float) Math.toRadians(thetaY))
                .putArgs(centroid.get(),
                        projector.get(),
                        direction.get());

        int threads = CommonKernelServices.getNearestGreaterMultiple(projector.getCount(), CommonKernelServices.WARP_GROUP_SIZE);

        queue.put1DRangeKernel(rotateProjector, 0, threads, CommonKernelServices.WARP_GROUP_SIZE)
                .finish();

        rotateProjector.release();
    }
}
