package cz.fidentis.analyst.engines.icp;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Stateless services for IPC registration.
 * It is a wrapper to the stateful implementation of {@link cz.fidentis.analyst.engines.icp.IcpVisitor}.
 * Return history of transformations has the following structure:
 * <p>
 *     Keys in the map contain mesh facets that were inspected and transformed.
 *     For each transformed facet, a list of transformations to the primary mesh
 *     is stored. The order of transformations corresponds to the order of ICP iterations,
 *     i.e., the i-th value is the transformation applied in the i-th iteration on the visited mesh facet.
 *     It also means that the size of the list corresponds to the number of iterations
 *     performed for given mesh facet.
 * </p>
 *
 * @author Radek Oslejsek
 */
public interface IcpServices {

    /**
     * Transforms (!) given mesh facets and returns the history of applied transformations.
     *
     * @param facets Meshes to be transformed
     * @param config Configuration of the service
     * @return the history of applied transformations
     */
    static Map<MeshFacet, List<IcpTransformation>> transform(Collection<MeshFacet> facets, IcpConfig config) {
        var visitor = config.getVisitor();
        facets.forEach(facet -> facet.accept(visitor));
        return visitor.getTransformations();
    }

    /**
     * Transforms (!) given mesh facets and returns the history of applied transformations.
     *
     * @param facet Mesh to be transformed
     * @param config Configuration of the service
     * @return the history of applied transformations
     */
    static Map<MeshFacet, List<IcpTransformation>> transform(MeshFacet facet, IcpConfig config) {
        return transform(Collections.singleton(facet), config);
    }

    /**
     * Transforms (!) given mesh facets and returns the history of applied transformations.
     *
     * @param model Mesh to be transformed
     * @param config Configuration of the service
     * @return the history of applied transformations
     */
    static Map<MeshFacet, List<IcpTransformation>> transform(MeshModel model, IcpConfig config) {
        return transform(model.getFacets(), config);
    }
}
