package cz.fidentis.analyst.engines.bbox;

import cz.fidentis.analyst.engines.bbox.impl.BoundingBoxVisitorImpl;

/**
 * Configuration for the computation of bounding box from inspected meshes.
 *
 * @author Radek Oslejsek
 */
public record BoundingBoxConfig() {

    /**
     * Instantiates and returns a visitor.
     *
     * @return a visitor
     */
    public BoundingBoxVisitor getVisitor() {
        return new BoundingBoxVisitorImpl();
    }
}
