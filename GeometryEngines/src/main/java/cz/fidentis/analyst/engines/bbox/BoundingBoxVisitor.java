package cz.fidentis.analyst.engines.bbox;

import cz.fidentis.analyst.data.mesh.MeshVisitor;
import cz.fidentis.analyst.data.shapes.Box;

/**
 * Stateful computation of a bounding box.
 * First, the visitor is created. Then, it is applied to mesh facets. Finally, the samples are returned.
 *
 * @author Radek Oslejsek
 */
public interface BoundingBoxVisitor extends MeshVisitor {

    /**
     * Returns computed bounding box.
     *
     * @return Bounding box or {@code null}
     */
    Box getBoundingBox();
}
