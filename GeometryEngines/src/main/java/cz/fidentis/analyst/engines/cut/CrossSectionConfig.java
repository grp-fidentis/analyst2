package cz.fidentis.analyst.engines.cut;

import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.cut.impl.CrossSectionVisitorImpl;

/**
 * Configuration for cross-section calculations of inspected meshes.
 *
 * @author Radek Oslejsek
 *
 * @param plane Cutting plane
 */
public record CrossSectionConfig(Plane plane) {

    /**
     * Instantiates and returns a visitor.
     *
     * @return a visitor
     */
    public CrossSectionVisitor getVisitor() {
        return new CrossSectionVisitorImpl(plane);
    }
}
