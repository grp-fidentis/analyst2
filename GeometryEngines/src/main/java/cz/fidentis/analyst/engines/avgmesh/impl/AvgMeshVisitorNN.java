package cz.fidentis.analyst.engines.avgmesh.impl;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshVisitor;
import cz.fidentis.analyst.engines.distance.impl.MeshDistanceNN;
import cz.fidentis.analyst.data.mesh.measurement.FacetDistances;

import javax.vecmath.Vector3d;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Visitor capable to create an average face by deforming given face template
 * so that its vertices are in the "average position" with respect to the visited faces.
 * The average position is computed as the centroid of mass of the closest points
 * (<b>nearest neighbors</b>) from inspected faces.
 *
 * @author Radek Oslejsek
 */
public class AvgMeshVisitorNN implements AvgMeshVisitor {
    
    private MeshModel avgMeshModel = null;
    
    private int numInspectedFacets = 0;
    
    /**
     * Key: My facets. 
     * Value: Sum of translations of my vertices according to nearest neighbors of visited meshes
     */
    private final Map<MeshFacet, List<Vector3d>> transformations = new HashMap<>();
    
    /**
     * Constructor.
     * 
     * @param templateFacet Mesh facet which is transformed to the averaged mesh. 
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorNN(MeshFacet templateFacet) {
        this(new HashSet<>(Collections.singleton(templateFacet)));
        if (templateFacet == null) {
            throw new IllegalArgumentException("templateFacet");
        }
    }
    
    /**
     * Constructor.
     * 
     * @param templateFacets Mesh facets that are transformed to the averaged mesh
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorNN(Collection<MeshFacet> templateFacets) {
        if (templateFacets == null || templateFacets.isEmpty()) {
            throw new IllegalArgumentException("templateFacets");
        }
        
        // fill vertTarns with empty list for each facet
        templateFacets.parallelStream().forEach(f -> {
                transformations.put(f, Stream.generate(Vector3d::new)
                        .limit(f.getVertices().size())
                        .toList());
        });
    }
    
    /**
     * Constructor.
     * 
     * @param templateModel Mesh model which is transformed to the averaged mesh
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorNN(MeshModel templateModel) {
        this(new HashSet<>(templateModel.getFacets()));
    }
    
    @Override
    public void visitKdTree(KdTree kdTree) {
        avgMeshModel = null; // AVG mesh model will be re-computed in the getAveragedMeshModel()
        numInspectedFacets++;
        
        // compute distance from me to the mesh stored in the k-d tree:
        MeshDistanceNN hDist = new MeshDistanceNN(
                kdTree, 
                true,
                false, // relative distance
                true,  // parallel computation
                false  // auto crop
        );
        transformations.keySet().forEach(f -> hDist.visitMeshFacet(f)); 
        
        // compute shifts of my vertices
        for (MeshFacet myFacet: transformations.keySet()) {
            FacetDistances facetDistances = hDist.getDistancesOfVisitedFacets().getFacetMeasurement(myFacet);

            // shift vertices concurrently
            IntStream.range(0, facetDistances.size()).parallel().forEach(i -> {
                Vector3d moveDir = new Vector3d(facetDistances.get(i).getNearestNeighbor().getPosition());
                moveDir.sub(myFacet.getVertex(i).getPosition());
                transformations.get(myFacet).get(i).add(moveDir);
            });
        }
    }

    @Override
    public MeshModel getAveragedMeshModel() {
        if (avgMeshModel == null) {
            avgMeshModel = MeshFactory.createEmptyMeshModel();
            for (MeshFacet f: transformations.keySet()) { // clone all facets of the template face
                MeshFacet newFacet = MeshFactory.cloneMeshFacet(f);
                IntStream.range(0, newFacet.getNumberOfVertices()).parallel().forEach(i -> {
                    Vector3d tr = new Vector3d(transformations.get(f).get(i));
                    tr.scale(1.0/numInspectedFacets);
                    newFacet.getVertex(i).getPosition().add(tr);
                });
                avgMeshModel.addFacet(newFacet);
            }
        }
        return avgMeshModel;
    }
    
}
