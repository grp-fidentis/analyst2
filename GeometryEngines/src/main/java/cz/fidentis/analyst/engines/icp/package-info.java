/**
 * Iterative Closest Point (ICP) superimposition of meshes.
 */
package cz.fidentis.analyst.icp;
