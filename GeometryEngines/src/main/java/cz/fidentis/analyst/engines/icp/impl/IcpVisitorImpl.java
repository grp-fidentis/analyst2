package cz.fidentis.analyst.engines.icp.impl;

import com.google.common.primitives.Doubles;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.distance.impl.MeshDistanceNN;
import cz.fidentis.analyst.data.mesh.measurement.DistanceRecord;
import cz.fidentis.analyst.data.mesh.measurement.FacetDistances;
import cz.fidentis.analyst.engines.icp.IcpTransformation;
import cz.fidentis.analyst.engines.icp.IcpVisitor;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingServices;
import cz.fidentis.analyst.math.EigenvalueDecomposition;
import cz.fidentis.analyst.math.Quaternion;

import javax.vecmath.*;
import java.util.*;

/**
 * The implementation of the ICP visitor.
 * 
 * @author Maria Kocurekova
 * @author Radek Oslejsek
 * @author Zuzana Ferkova
 */
public class IcpVisitorImpl implements IcpVisitor {

    /**
     * Transformed mesh facets and their history of transformations.
     * Key = visited mesh facets.
     * Value = History of transformations (transformation performed in each ICP iteration).
     */
    protected final Map<MeshFacet, List<IcpTransformation>> transformations = new HashMap<>();

    /**
     * Maximal number of ICP iterations
     */
    protected final int maxIteration;

    /**
     * Whether to scale mesh facets as well
     */
    private final boolean scale;

    /**
     * Acceptable error. When reached, then the ICP stops.
     */
    private final double error;

    /**
     * K-d tree of the primary triangular mesh(es).
     */
    protected final KdTree primaryKdTree;

    protected final int crop;

    private final PointSamplingConfig samplingStrategy;

    /**
     * Constructor.
     *
     * @param mainFacets Primary mesh facets. Must not be {@code null}.
     * Inspected facets are transformed toward these primary mesh.
     * @param maxIteration Maximal number of ICP iterations (it includes computing
     * new transformation and applying it). A number bigger than zero.
     * Reasonable number seems to be 10.
     * @param scale If {@code true}, then the scale factor is also computed.
     * @param error Acceptable error - a number bigger than or equal to zero.
     * Mean distance of vertices is computed for each ICP iteration.
     * If the difference between the previous and current mean distances is less than the error,
     * then the ICP computation stops. Reasonable number seems to be 0.05.
     * @param strategy One of the reduction strategies. If {@code null}, then {@code NoUndersampling} is used.
     * @param crop The iteration number from which partially overlapped parts of surfaces are omitted from distance minimization
     *             (i.e. the faces are auto-cropped for the ICP step). Set the parameter to zero to turn the auto-cropping
     *             feature on for all ICP iterations. Set the parameter to any negative number to disable the auto-cropping
     *             feature. In general, use auto-cropping if the faces are pre-aligned. If unsure, use value 1.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public IcpVisitorImpl(Collection<MeshFacet> mainFacets, int maxIteration, boolean scale, double error, PointSamplingConfig strategy, int crop) {
        if (mainFacets == null) {
            throw new IllegalArgumentException("mainFacets");
        }
        if (maxIteration <= 0) {
            throw new IllegalArgumentException("maxIteration");
        }
        if (error < 0.0) {
            throw new IllegalArgumentException("error");
        }
        this.primaryKdTree = KdTree.create(new ArrayList<>(mainFacets));
        this.error = error;
        this.maxIteration = maxIteration;
        this.scale = scale;
        this.samplingStrategy = (strategy == null)
                ? new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 0)
                : strategy;
        this.crop = crop;
    }

    /**
     * Constructor.
     *
     * @param mainModel Primary mesh model. Must not be {@code null}.
     * Inspected facets are transformed toward this primary mesh.
     * @param maxIteration Maximal number of ICP iterations (it includes computing
     * new transformation and applying it). A number bigger than zero.
     * Reasonable number seems to be 10.
     * @param scale If {@code true}, then the scale factor is also computed.
     * @param error Acceptable error - a number bigger than or equal to zero.
     * Mean distance of vertices is computed for each ICP iteration.
     * If the difference between the previous and current mean distances is less than the error,
     * then the ICP computation stops. Reasonable number seems to be 0.05.
     * @param strategy One of the reduction strategies. If {@code null}, then {@code NoUndersampling} is used.
     * @param crop The iteration number from which partially overlapped parts of surfaces are omitted from distance minimization
     *             (i.e. the faces are auto-cropped for the ICP step). Set the parameter to zero to turn the auto-cropping
     *             feature on for all ICP iterations. Set the parameter to any negative number to disable the auto-cropping
     *             feature. In general, use auto-cropping if the faces are pre-aligned. If unsure, use value 1.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public IcpVisitorImpl(MeshModel mainModel, int maxIteration, boolean scale, double error, PointSamplingConfig strategy, int crop) {
        this(new HashSet<>(mainModel.getFacets()), maxIteration, scale, error, strategy, crop);
        if (mainModel.getFacets().isEmpty()) {
            throw new IllegalArgumentException("mainModel");
        }
    }

    /**
     * Constructor.
     *
     * @param primaryKdTree The k-d tree of the primary mesh. Must not be {@code null}.
     * Inspected facets are transformed toward this primary mesh.
     * @param maxIteration Maximal number of ICP iterations (it includes computing
     * new transformation and applying it). A number bigger than zero.
     * Reasonable number seems to be 10.
     * @param scale If {@code true}, then the scale factor is also computed.
     * @param error Acceptable error - a number bigger than or equal to zero.
     * Mean distance of vertices is computed for each ICP iteration.
     * If the difference between the previous and current mean distances is less than the error,
     * then the ICP computation stops. Reasonable number seems to be 0.05.
     * @param strategy One of the reduction strategies. If {@code null}, then {@code NoUndersampling} is used.
     * @param crop The iteration number from which partially overlapped parts of surfaces are omitted from distance minimization
     *             (i.e. the faces are auto-cropped for the ICP step). Set the parameter to zero to turn the auto-cropping
     *             feature on for all ICP iterations. Set the parameter to any negative number to disable the auto-cropping
     *             feature. In general, use auto-cropping if the faces are pre-aligned. If unsure, use value 1.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public IcpVisitorImpl(KdTree primaryKdTree, int maxIteration, boolean scale, double error, PointSamplingConfig strategy, int crop) {
        if (primaryKdTree == null) {
            throw new IllegalArgumentException("primaryKdTree");
        }
        if (maxIteration <= 0) {
            throw new IllegalArgumentException("maxIteration");
        }
        if (error < 0.0) {
            throw new IllegalArgumentException("error");
        }
        this.primaryKdTree = primaryKdTree;
        this.error = error;
        this.maxIteration = maxIteration;
        this.scale = scale;
        this.samplingStrategy = (strategy == null)
                ? new PointSamplingConfig(PointSamplingConfig.Method.NO_SAMPLING, 0)
                : strategy;
        this.crop = crop;
    }
    
    @Override
    public Map<MeshFacet, List<IcpTransformation>> getTransformations() {
        return Collections.unmodifiableMap(transformations);
    }
    
    @Override
    public boolean isThreadSafe() {
        return false;
    }
    
    /**
     * Returns the maximal number of ICP iterations used for the computation.
     * 
     * @return the maximal number of ICP iterations used for the computation
     */
    public int getMaxIterations() {
        return this.maxIteration;
    }
    
    /**
     * Returns maximal acceptable error used for the computation.
     * 
     * @return maximal acceptable error used for the computation
     */
    public double getError() {
        return this.error;
    }
    
    /**
     * Returns {@code true} if the inspected mesh faces were also scaled.
     * 
     * @return {@code true} if the inspected mesh faces were also scaled.
     */
    public boolean getScale() {
        return this.scale;
    }
    
    /**
     * Returns k-d tree of the primary mesh to which other meshes has been transformed.
     * 
     * @return k-d tree of the primary mesh
     */
    public KdTree getPrimaryKdTree() {
        return this.primaryKdTree;
    }
    
    @Override
    public void visitMeshFacet(MeshFacet transformedFacet) {
        MeshDistanceNN distVisitor = new MeshDistanceNN(
                this.primaryKdTree, 
                true,
                false, // relative distance
                true,  // parallel computation
                false  // disable auto-cropping by default
        );

        MeshFacet reducedFacet = MeshFactory.createPointCloudFacet(PointSamplingServices.sample(transformedFacet, samplingStrategy));

        if (!transformations.containsKey(transformedFacet)) {
            transformations.put(transformedFacet, new ArrayList<>(maxIteration));
        }
        
        int currentIteration = 0;
        double lastObjective = 0.0;
        boolean backedByOrigMesh = samplingStrategy.isBackedByOrigMesh();
        
        while (true) {
            if (currentIteration >= maxIteration) {
                return;
            }

            if (currentIteration == crop) { // switch auto-cropping function from required iteration
                distVisitor.setCrop(true);
            }
            
            // Find nearest neighbors
            distVisitor.visitMeshFacet(reducedFacet); // repeated invocation re-calculates the distance
            
            // Compute centroid of both facets and the minimization objective
            Point3d mainCenter = new Point3d();
            Point3d trCenter = new Point3d();
            double objective = computeTrParams(distVisitor, reducedFacet, mainCenter, trCenter);

            if (currentIteration > 0 && Math.abs(objective - lastObjective) <= error) {
                return; // the previous iteration did not improve the registration significantly anymore
            }
            lastObjective = objective;
            
            // Find the best transformation and compute the minimization objective (relevant before the transformation is applied):
            IcpTransformation transformation = computeIcpTransformation(distVisitor, reducedFacet, mainCenter, trCenter);
            
            applyTransformation(transformedFacet, transformation);
            if (!backedByOrigMesh) { // samples have to be transformed as well
                applyTransformation(reducedFacet, transformation);
            }
            transformations.get(transformedFacet).add(transformation);
            
            currentIteration++;
        }
    }
    
    /*
     *  PRIVATE METHODS
     */

    /**
     * Computes basic transformation parameters used for the superimposition ICP step
     * @param hdVisitor A visitor with the computed closest points to the {@code transformedFacet}
     * @param transformedFacet Facet to be superimposed 
     * @param outMainCenter Output parameter which is set to the centroid of the closest points of the static facet
     * @param outTrCenter Output parameter which is set to the centroid of the transformed facet
     * @return ICP minimization objective (related to the state before the transformation)
     */
    private double computeTrParams(MeshDistanceNN hdVisitor, MeshFacet transformedFacet, Point3d outMainCenter, Point3d outTrCenter) {
        outMainCenter.set(0, 0, 0);
        outTrCenter.set(0, 0, 0);

        FacetDistances facetDistances = hdVisitor.getDistancesOfVisitedFacets().getFacetMeasurement(transformedFacet);
        List<MeshPoint> trPoints = transformedFacet.getVertices();

        //
        // Compute mean distance from centroids:
        //
        double objective = 0;
        int countOfNotNullPoints = 0;
        for (int i = 0; i < facetDistances.size(); i++) {
            DistanceRecord pDist = facetDistances.get(i);
            if (pDist.getNearestNeighbor() != null && Doubles.isFinite(pDist.getDistance())) {
                outMainCenter.add(pDist.getNearestNeighbor().getPosition());
                outTrCenter.add(trPoints.get(i).getPosition());
                countOfNotNullPoints++;
                objective += pDist.getDistance();
            }
        }

        if (countOfNotNullPoints > 0) {
            outMainCenter.scale(1.0 / countOfNotNullPoints);
            outTrCenter.scale(1.0 / countOfNotNullPoints);
            objective /= countOfNotNullPoints;
        }

        return objective;
    }

    /**
     * Computes transformation parameters (translation, rotation, and scale factor).
     * This method also computes and stores ICP minimization objective 
     * <strong>BEFORE the transformation</strong>!
     *
     * @param hdVisitor A visitor with computed the closest points to the {@code transformedFacet}
     * @param transformedFacet Facet to be superimposed 
     * @return Return an ICP transformation
     */
    private IcpTransformation computeIcpTransformation(MeshDistanceNN hdVisitor, MeshFacet transformedFacet, Point3d mainCenter, Point3d trCenter) {
        FacetDistances facetDistances = hdVisitor.getDistancesOfVisitedFacets().getFacetMeasurement(transformedFacet);
        List<MeshPoint> trPoints = transformedFacet.getVertices();
        
        //
        // Compute translation:
        //
        Vector3d translation = new Vector3d(
                mainCenter.x - trCenter.x,
                mainCenter.y - trCenter.y, 
                mainCenter.z - trCenter.z
        );
        
        //
        // Compute rotation:
        //
        Matrix4d sumMat = new Matrix4d();
        for (int i = 0; i < trPoints.size(); i++) {
            DistanceRecord pDist = facetDistances.get(i);
            if (pDist.getNearestNeighbor() != null && Doubles.isFinite(pDist.getDistance())) {
                sumMat.add(multMat(
                        relativeCoord4d(trPoints.get(i).getPosition(), trCenter),
                        relativeCoord4d(pDist.getNearestNeighbor().getPosition(), mainCenter)
                ));
            }
        }
        
        Quaternion q = new Quaternion(new EigenvalueDecomposition(sumMat));
        q.normalize();

        //
        // compute scale
        //
        double scaleFactor = 1.0;
        if (scale) {
            double sxUp = 0;
            double sxDown = 0;
            Matrix4d rotMat = q.toMatrix();
            for (int i = 0; i < facetDistances.size(); i++) {
                DistanceRecord pDist = facetDistances.get(i);
                if (pDist.getNearestNeighbor() != null && Doubles.isFinite(pDist.getDistance())) {
                    Vector4d relC = relativeCoord4d(pDist.getNearestNeighbor().getPosition(), mainCenter);
                    Vector4d relA = relativeCoord4d(trPoints.get(i).getPosition(), trCenter);
                    rotMat.transform(relA);                
                    sxUp += relC.dot(relA);
                    sxDown += relA.dot(relA);
                }
            }
            scaleFactor = (sxDown == 0.0) ? 0.0 : sxUp / sxDown;
        }

        return new IcpTransformation(translation, q, scaleFactor, mainCenter);
    }
    
    /**
     * Apply computed transformation to compared facet.
     *
     * @param transformedFacet Facet to be transformed
     * @param transformation Computed transformation.
     */
    private void applyTransformation(MeshFacet transformedFacet, IcpTransformation transformation) {
        transformedFacet.getVertices().parallelStream()
                .forEach(p -> {
                    p.setPosition(transformation.transformPoint(p.getPosition(), scale));
                    if (p.getNormal() != null) {
                        p.setNormal(transformation.transformNormal(p.getNormal()));
                    }
                }
        );
    }

    private Vector4d relativeCoord4d(Point3d p, Point3d center) {
        return new Vector4d(p.x - center.x, p.y - center.y, p.z - center.z, 1.0);
    }

    /**
     * Fast implementation of <pre>sumMatrixComp(a).mult(sumMatrixMain(b));</pre>
     */
    private Matrix4d multMat(Tuple4d a, Tuple4d b) {
        double axbx = a.x * b.x;
        double axby = a.x * b.y;
        double axbz = a.x * b.z;
        double aybx = a.y * b.x;
        double ayby = a.y * b.y;
        double aybz = a.y * b.z;
        double azbx = a.z * b.x;
        double azby = a.z * b.y;
        double azbz = a.z * b.z;
        
        return new Matrix4d(
                axbx+ayby+azbz,       aybz-azby,      -axbz+azbx,       axby-aybx,
                    -azby+aybz,  axbx-azbz-ayby,       axby+aybx,       axbz+azbx,
                     azbx-axbz,       aybx+axby,  ayby-azbz-axbx,       aybz+azby,
                    -aybx+axby,       azbx+axbz,       azby+aybz,  azbz-ayby-axbx
        );
    }
}
