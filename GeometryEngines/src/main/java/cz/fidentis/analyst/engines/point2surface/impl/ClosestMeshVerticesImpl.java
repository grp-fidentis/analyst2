package cz.fidentis.analyst.engines.point2surface.impl;

import cz.fidentis.analyst.data.kdtree.KdNode;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceVisitor;

import javax.vecmath.Point3d;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * This visitor finds the minimal distance between the given 3D point and points
 * (e.g., mesh vertices) stored in visited k-d trees.
 * All closest mesh facets and all their closest vertices are returned in addition to the distance.
 * <b>Possible independent vertices stored in the visited k-d tree are skipped at this moment
 * as they have no associated mesh facet.</b>
 *
 * @author Daniel Schramm
 * @author Radek Oslejsek
 */
public class ClosestMeshVerticesImpl implements PointToSurfaceDistanceVisitor {

    /**
     * Nearest vertices and their mesh facets
     */
    private Map<MeshFacet, List<Integer>> nearestVertices = new HashMap<>();

    private ClosestNodesImpl nodesVisitor;

    private final boolean checkOverlay;

    /**
     * Constructor.
     *
     * @param point A 3D point from which distance is computed. Must not be {@code null}
     * @param checkOverlay If {@code true} and the closest point to the given 3D reference point
     *        lies on the boundary of the mesh stored in the k-d tree,
     *        then the reference point is considered "outside" of the mesh
     *        and the distance is set to infinity.
     */
    public ClosestMeshVerticesImpl(Point3d point, boolean checkOverlay) {
        this.nodesVisitor = new ClosestNodesImpl(point);
        this.checkOverlay = checkOverlay;
    }
    
    @Override
    public Map<MeshFacet, List<MeshPoint>> getNearestPoints() {
        if (this.nearestVertices == null) {
            setNearestVertices();
        }

        // Transform vertex indices into vertices
        Map<MeshFacet, List<MeshPoint>> ret = new HashMap<>();
                nearestVertices.keySet().forEach(f ->
                nearestVertices.get(f).forEach(i ->
                        ret.computeIfAbsent(f, n -> new ArrayList<>()).add(f.getVertex(i))));

        return ret;
    }
        
    @Override
    public double getDistance() {
        if (!checkOverlay || nodesVisitor.getDistance() == Double.POSITIVE_INFINITY) {
            return nodesVisitor.getDistance();
        }

        if (this.nearestVertices == null) {
            setNearestVertices();
        }

        for (MeshFacet f: nearestVertices.keySet()) {
            for (Integer i: nearestVertices.get(f)) {
                if (f.getOneRingNeighborhood(i).isBoundary()) {
                    return Double.POSITIVE_INFINITY;
                }
            }
        }
        
        return nodesVisitor.getDistance();
    }

    @Override
    public void visitKdTree(KdTree kdTree) {
        synchronized (this) {
            this.nearestVertices = null;
        }
        nodesVisitor.visitKdTree(kdTree);
    }

    protected synchronized void setNearestVertices() {
        this.nearestVertices = nodesVisitor.getClosestNodes().stream()
                .map(KdNode::getFacets)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(
                        Entry::getKey,
                        Collectors.mapping(Entry::getValue, Collectors.toList())));
    }
}
