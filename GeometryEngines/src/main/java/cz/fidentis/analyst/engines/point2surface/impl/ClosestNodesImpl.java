package cz.fidentis.analyst.engines.point2surface.impl;

import cz.fidentis.analyst.data.kdtree.KdNode;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.kdtree.KdTreeVisitor;

import javax.vecmath.Point3d;
import java.util.*;

/**
 * For a given 3D point, this visitor returns the closest node(s) and its/their distance to the point.
 *
 * @author Daniel Schramm
 */
public class ClosestNodesImpl implements KdTreeVisitor {

    private final Point3d point3d;
    private double distance = Double.POSITIVE_INFINITY;
    private final Set<KdNode> closest = new HashSet<>();

    /**
     * Constructor.
     *
     * @param point The 3D location for which the closest nodes are to be computed.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public ClosestNodesImpl(Point3d point) {
        this.point3d = Objects.requireNonNull(point);
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public void visitKdTree(KdTree kdTree) {
        final KdNode root = kdTree.getRoot();
        if (root == null) {
            return;
        }

        KdNode searchedNode = root;

        /*
         * Reduce the search space by exploring the area where the closest node
         * may theoretically be located.
         */
        while (searchedNode != null) {
            final Point3d nodePos = searchedNode.getLocation();
            final double dist = nodePos.distance(point3d);
            synchronized (this) {
                if (dist < distance) {
                    distance = dist;
                    closest.clear();
                }
            }

            if (firstIsLessThanSecond(point3d, nodePos, searchedNode.getDepth())) {
                searchedNode = searchedNode.getLesser();
            } else {
                searchedNode = searchedNode.getGreater();
            }
        }

        /*
         * Search for vertices that could be potentially closer than
         * the closest distance already found or at the same distance.
         */
        final Queue<KdNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            searchedNode = queue.poll();
            final Point3d nodePos = searchedNode.getLocation();

            final double dist = nodePos.distance(point3d);
            synchronized (this) {
                if (dist < distance) {
                    distance = dist;
                    closest.clear();
                    closest.add(searchedNode);
                } else if (dist == distance) {
                    closest.add(searchedNode);
                }
            }

            final double distOnAxis = minDistanceIntersection(nodePos, point3d, searchedNode.getDepth());

            if (distOnAxis > distance) {
                if (firstIsLessThanSecond(point3d, nodePos, searchedNode.getDepth())) {
                    if (searchedNode.getLesser() != null) {
                        queue.add(searchedNode.getLesser());
                    }
                } else {
                    if (searchedNode.getGreater() != null) {
                        queue.add(searchedNode.getGreater());
                    }
                }
            } else {
                if (searchedNode.getLesser() != null) {
                    queue.add(searchedNode.getLesser());
                }
                if (searchedNode.getGreater() != null) {
                    queue.add(searchedNode.getGreater());
                }
            }
        }
    }

    protected Set<KdNode> getClosestNodes() {
        return Collections.unmodifiableSet(closest);
    }

    protected KdNode getAnyClosestNode() {
        return closest.stream().findAny().orElse(null);
    }

    protected Point3d getReferencePoint() {
        return point3d;
    }

    protected boolean firstIsLessThanSecond(Point3d v1, Point3d v2, int level){
        switch (level % 3) {
            case 0:
                return v1.x <= v2.x;
            case 1:
                return v1.y <= v2.y;
            case 2:
                return v1.z <= v2.z;
            default:
                break;
        }
        return false;
    }

    /**
     * Calculates distance between two points
     * (currently searched node and point to which we want to find nearest neighbor)
     * (based on axis)
     *
     */
    protected double minDistanceIntersection(Point3d nodePosition, Point3d pointPosition, int level){
        switch (level % 3) {
            case 0:
                return Math.abs(nodePosition.x - pointPosition.x);
            case 1:
                return Math.abs(nodePosition.y - pointPosition.y);
            default:
                return Math.abs(nodePosition.z - pointPosition.z);
        }
    }
}
