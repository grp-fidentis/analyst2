package cz.fidentis.analyst.engines.symmetry.impl;

import cz.fidentis.analyst.data.grid.UniformGrid3d;
import cz.fidentis.analyst.data.grid.UniformGrid4d;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Plane;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;
import java.util.List;
import java.util.Objects;

/**
 * A symmetry plane extends standard plane with functions related to the
 * similarity of planes and the measure of their quality (symmetry precision).
 * This version is used by the robust symmetry calculation methods.
 *
 * @author Radek Oslejsek
 */
public class CandidatePlaneRobust implements Comparable<CandidatePlaneRobust> {

    public static final double GRID_SIZE = 0.1; // similarity epsilon, should be 0.1

    private final double avgDist;
    private int numAvg; // psi
    private double symmetry = 0;

    private Plane plane;

    /**
     * New candidate symmetry plane constructed from two points.
     *
     * @param point1 point in space
     * @param point2 point in space
     * @param avgDist the average distance between vertices and their centroid
     * @throws IllegalArgumentException if the @code{plane} argument is null
     */
    public CandidatePlaneRobust(Tuple3d point1, Tuple3d point2, double avgDist) {
        this.plane = new Plane(point1, point2);
        this.avgDist = avgDist;
        this.numAvg = 0;
    }

    /**
     * Candidate symmetry plane constructed by averaging two planes.
     * This is a fast step-by-step implementation of {@link Plane#Plane(List)} averaging algorithm optimized
     * for the pruning step of the symmetry plane estimation.
     * The created new candidate plane has incremented number of averaged and <b>is not normalized</b>.
     *
     * @param pu the closest existing candidate. If pu.getNumAverages() == 0, then the plane must be normalized.
     * @param pv newly created <b>normalized</b> candidate
     */
    public CandidatePlaneRobust(CandidatePlaneRobust pu, CandidatePlaneRobust pv) {
        Vector3d pun = new Vector3d(pu.getNormal());
        Vector3d pvn = new Vector3d(pv.getNormal());
        double pud = pu.getDistance();
        double pvd = pv.getDistance();

        // new created plane has numAvg set to zero and normalized normal vector
        if (pu.getNumAverages() >= 1) {
            double scale = 1.0 / pu.getNormal().length();
            pun.scale(scale);
            pud *= scale;
            if (pu.getNumAverages() > 1) {
                scale = 1.0 / pu.getNumAverages();
                pvn.scale(scale);
                pvd *= scale;
            }
        }

        // change pun and pud values to correspond to the average plane:
        double dot = pun.x * pvn.x + pun.y * pvn.y + pun.z * pvn.z;
        if (dot >= 0) {
            pun.add(pvn);
            pud += pvd;
        } else {
            pun.sub(pvn);
            pud -= pvd;
        }

        this.plane = new Plane(pun, pud);
        this.avgDist = pu.avgDist;
        this.numAvg = pu.getNumAverages() + 1;
    }

    /**
     * New candidate symmetry plane constructed from an original plane.
     *
     * @param plane Original plane. Must not be {@code null}
     */
    public CandidatePlaneRobust(Plane plane) {
        this.plane = Objects.requireNonNull(plane);
        this.avgDist = 0;
        this.numAvg = 0;
    }

    public Vector3d getNormal() {
        return plane.getNormal();
    }

    public double getDistance() {
        return plane.getDistance();
    }

    public Plane getPlane() {
        return plane;
    }

    /**
     * Returns the closest plane from the cache to this plane.
     *
     * @param cache
     * @return the closest plane from the cache to this plane.
     */
    public CandidatePlaneRobust getClosestPlane(UniformGrid4d<CandidatePlaneRobust> cache) {
        Vector4d pp = this.getEstimationVector();
        List<CandidatePlaneRobust> closePlanes = cache.getClosest(pp);
        pp.scale(-1.0);
        closePlanes.addAll(cache.getClosest(pp));

        CandidatePlaneRobust closest = null;
        double dist = Double.POSITIVE_INFINITY;
        for (CandidatePlaneRobust plane : closePlanes) {
            double d = this.distance(plane);
            if (d < GRID_SIZE && d < dist) {
                dist = d;
                closest = plane;
            }
        }

        return (dist == Double.POSITIVE_INFINITY) ? null : closest;
    }
    
    /**
     * Returns a 4D vector usable as a location for the storage in the 
     * {@link UniformGrid4d}
     * 
     * @return a 4D vector usable as a location for the storage in the 
     * {@link UniformGrid4d}
     */
    public Vector4d getEstimationVector() {
        Vector3d n = getNormal();
        Vector4d ret = new Vector4d(n);
        ret.w = getDistance() / avgDist;
        if (numAvg > 0) { // a non-averaged plane has to be normalized
            ret.scale(1.0 / n.length());
        }
        return ret;
    }

    public int getNumAverages() {
        return numAvg;
    }

    /**
     *
     * @param pv the second plane
     * @return planes distance
     */
    protected double distance(CandidatePlaneRobust pv) {
        Vector4d pud = this.getEstimationVector();
        Vector4d pvd = pv.getEstimationVector();
        if (this.getNormal().dot(pv.getNormal()) >= 0.0) {
            pud.sub(pvd);
        } else {
            pud.add(pvd);
        }
        return pud.length();
    }

    /**
     * Symmetry measurement based on the Wendland’s function without additional
     * weights
     * <b>The plane is normalized and {@code numAvg} set to zero!</b>
     *
     * @param points Downsampled point cloud
     * @param grid The same cloud stored in the uniform grid
     * @param alpha the average distance between vertices and their centroid
     */
    public void measureSymmetry(List<MeshPoint> points, UniformGrid3d<MeshPoint> grid, double alpha) {
        normalizeIfNeeded();
        symmetry = 0.0;

        for (int i = 0; i < points.size(); i++) {
            MeshPoint p1 = points.get(i); // original point
            Point3d rp1 = plane.reflectPointOverPlane(points.get(i).getPosition()); // reflected point
            List<MeshPoint> closest = grid.getClosest(rp1);

            for (int j = 0; j < closest.size(); j++) {
                MeshPoint p2 = closest.get(j);

                if (p1 == p2) {
                    continue;
                }

                double phi = similarityFunction(rp1.distance(p2.getPosition()), alpha);
                if (phi == 0) {
                    continue;
                }

                symmetry += phi;
            }
        }
    }

    /**
     * Returns the symmetry measure computed by the {@link #getSymmetryMeasure()}.
     * @return the symmetry measure
     */
    public double getSymmetryMeasure() {
        return this.symmetry;
    }

    @Override
    public int compareTo(CandidatePlaneRobust o) {
        return Double.compare(o.symmetry, this.symmetry);
    }

    @Override
    public String toString() {
        return "S: " + symmetry + " " + super.toString();
    }

    /**
     * A similarity function Phi.
     *
     * @param length
     * @param alpha
     * @return similarity value
     */
    protected static double similarityFunction(double length, double alpha) {
        double al = alpha * length;
        if (al == 0 || al > 2.6) {
            return 0;
        }
        double q = al / 2.6;
        return Math.pow(1.0 - q, 5) * (8 * q * q + 5 * q + 1);
    }

    protected void setSymmetryMeasure(double symmetry) {
        this.symmetry = symmetry;
    }

    protected void normalizeIfNeeded() {
        if (numAvg > 0) { // normalize plane
            plane = new Plane(plane, true);
            numAvg = 0;
        }
    }
}
