package cz.fidentis.analyst.engines.bbox;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.shapes.Box;

import java.util.Collection;
import java.util.List;

/**
 * Stateless services for bounding box calculation.
 * It is a wrapper to the stateful implementation in {@link cz.fidentis.analyst.engines.bbox.BoundingBoxVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface BoundingBoxServices {

    /**
     * Returns bounding box of the given mesh.
     *
     * @param mesh Mesh model to be inspected
     * @param config Configuration of the service
     * @return result or {@code null}
     */
    static Box compute(MeshModel mesh, BoundingBoxConfig config) {
        return compute(mesh.getFacets(), config);
    }

    /**
     * Returns bounding box of the given mesh.
     *
     * @param mesh Mesh facet to be inspected
     * @param config Configuration of the service
     * @return result or {@code null}
     */
    static Box compute(MeshFacet mesh, BoundingBoxConfig config) {
        return compute(List.of(mesh), config);
    }

    /**
     * Returns bounding box of the given mesh.
     *
     * @param mesh Mesh facets to be inspected
     * @param config Configuration of the service
     * @return result or {@code null}
     */
    static Box compute(Collection<MeshFacet> mesh, BoundingBoxConfig config) {
        var visitor = config.getVisitor();
        mesh.forEach(facet -> facet.accept(visitor));
        return visitor.getBoundingBox();
    }
}
