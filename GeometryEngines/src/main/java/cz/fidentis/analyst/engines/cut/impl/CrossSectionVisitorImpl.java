package cz.fidentis.analyst.engines.cut.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.shapes.CrossSection3D;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.cut.CrossSectionVisitor;

import javax.vecmath.Point3d;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * A visitor that calculates the cross-section of a face and a cutting plane by 
 * splitting the mesh edges.
 * <p>
 * This visitor is thread-safe.
 * </p>
 *
 * @author Dominik Racek
 * @author Radek Oslejsek
 */
public class CrossSectionVisitorImpl implements CrossSectionVisitor {
    private final CrossSection3D curve;
    private final Set<Point3d> usedPoints;
    private Set<MeshTriangle> visited;
    private Set<MeshTriangle> toVisit;

    private final Plane plane;

    /**
     * Constructor for CrossSectionZigZag visitor
     *
     * @param plane cutting plane
     */
    public CrossSectionVisitorImpl(Plane plane) {
        this.plane = plane;
        this.curve = new CrossSection3D();
        this.usedPoints = new HashSet<>();
    }

    private void addPoint(Point3d p, boolean direction, int segment) {
        if (!usedPoints.contains(p)) {
            usedPoints.add(p);

            if (direction) {
                curve.addPointToSegmentEnd(segment, p);
            } else {
                curve.addPointToSegmentStart(segment, p);
            }
        }
    }

    private void visitMeshFacetRec(MeshFacet facet, int p1, int p2, MeshTriangle previous, boolean direction, int segment) {
        List<MeshTriangle> triangles = facet.getAdjacentTriangles(p1, p2);
        triangles.remove(previous);
        if (triangles.isEmpty()) {
            return;
        }

        //Find the next intersected edge
        MeshTriangle current = triangles.get(0);
        toVisit.remove(current);

        if (visited.contains(current)) {
            return;
        }
        visited.add(current);

        //Find the index of the last vertex of current (first ones being p1 and p2)
        int p3;
        if (current.getIndex1() != p1 && current.getIndex1() != p2) {
            p3 = current.getIndex1();
        } else if (current.getIndex2() != p1 && current.getIndex2() != p2) {
            p3 = current.getIndex2();
        } else {
            p3 = current.getIndex3();
        }

        //Next intersected edge will be between p1-p3 or p2-p3
        Point3d intersection = plane.getIntersectionWithLine(facet.getVertex(p1).getPosition(),
                facet.getVertex(p3).getPosition());

        if (intersection == null) {
            intersection = plane.getIntersectionWithLine(facet.getVertex(p2).getPosition(),
                    facet.getVertex(p3).getPosition());

            addPoint(intersection, direction, segment);
            visitMeshFacetRec(facet, p2, p3, current, direction, segment);
        } else {
            addPoint(intersection, direction, segment);
            visitMeshFacetRec(facet, p1, p3, current, direction, segment);
        }

    }

    @Override
    public void visitMeshFacet(MeshFacet facet) {
        //Logger out = Logger.measureTime();

        // Find all candidates
        visited = new HashSet<>();
        toVisit = facet.getTriangles().parallelStream()
                .filter(t -> t.checkIntersectionWithPlane(plane.getNormal(), plane.getDistance()))
                .collect(Collectors.toSet());
        
        synchronized (this) {
            while (!toVisit.isEmpty()) {
                MeshTriangle tri = toVisit.iterator().next();
                toVisit.remove(tri);

                // Figure out which lines are intersected
                Point3d intersection1 = plane.getIntersectionWithLine(tri.getVertex1(), tri.getVertex2());
                Point3d intersection2 = plane.getIntersectionWithLine(tri.getVertex2(), tri.getVertex3());
                Point3d intersection3 = plane.getIntersectionWithLine(tri.getVertex3(), tri.getVertex1());

                boolean allIntersectionsNull = intersection1 == null && intersection2 == null && intersection3 == null;
                boolean allPointsAlreadyUsed = Stream.of(intersection1, intersection2, intersection3)
                        .filter(Objects::nonNull)
                        .allMatch(usedPoints::contains);
                if (allIntersectionsNull || allPointsAlreadyUsed) {
                    continue;
                }

                int segment = curve.addNewSegment();
                if (intersection1 != null) {
                    //Intersection 1
                    addPoint(intersection1, true, segment);
                    visitMeshFacetRec(facet, tri.getIndex1(), tri.getIndex2(), tri, true, segment);
                    if (intersection2 != null) {
                        //Intersection 1 and 2
                        addPoint(intersection2, false, segment);
                        visitMeshFacetRec(facet, tri.getIndex2(), tri.getIndex3(), tri, false, segment);
                    } else if (intersection3 != null) {
                        //Intersection 1 and 3
                        addPoint(intersection3, false, segment);
                        visitMeshFacetRec(facet, tri.getIndex3(), tri.getIndex1(), tri, false, segment);
                    }
                } else if (intersection2 != null) {
                    //Intersection 2
                    addPoint(intersection2, true, segment);
                    visitMeshFacetRec(facet, tri.getIndex2(), tri.getIndex3(), tri, true, segment);
                    if (intersection3 != null) {
                        //Intersection 2 and 3
                        addPoint(intersection3, false, segment);
                        visitMeshFacetRec(facet, tri.getIndex3(), tri.getIndex1(), tri, false, segment);
                    }

                } else if (intersection3 != null) {
                    //Intersection 3 only
                    addPoint(intersection3, true, segment);
                    visitMeshFacetRec(facet, tri.getIndex3(), tri.getIndex1(), tri, true, segment);
                }
                //No intersection
            }
        }
    }

    @Override
    public CrossSection3D getCrossSectionCurve() {
        return curve;
    }
    
}
