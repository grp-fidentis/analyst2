/**
 * Algorithms for the computation of an average mesh from given meshes.
 */
package cz.fidentis.analyst.avgmesh;
