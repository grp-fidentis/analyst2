package cz.fidentis.analyst.engines.symmetry.impl;

import cz.fidentis.analyst.data.shapes.Plane;

import java.util.Objects;

/**
 * A symmetry plane extends standard plane with functions related to the
 * similarity of planes and the measure of their quality (symmetry precision).
 *
 * @author Radek Oslejsek
 */
public abstract class CandidatePlane {

    private Plane plane;

    /**
     * Constructor.
     * @param plane Original plane.
     */
    public CandidatePlane(Plane plane) {
        this.plane = Objects.requireNonNull(plane);
    }

    public Plane getPlane() {
        return plane;
    }
}
