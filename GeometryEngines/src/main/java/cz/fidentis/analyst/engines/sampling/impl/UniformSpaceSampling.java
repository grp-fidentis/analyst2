package cz.fidentis.analyst.engines.sampling.impl;

import cz.fidentis.analyst.data.grid.UniformGrid3d;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import javax.vecmath.Point3d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This downsampling algorithm divides the space uniformly using 3D grid.
 * Then, it computes and returns average position (centroid) of points in grid cells.
 * The number of samples is often slightly higher the required number.
 * 
 * @author Radek Oslejsek
 */
public class UniformSpaceSampling extends PointSamplingVisitorImpl {
    
    private static final double CELL_RESIZE_INIT_VALUE = 1.0;
    private static final double CELL_RESIZE_INIT_STEP = 1.0;
    private static final double CELL_RESIZE_STEP_CHANGE = 2.0;
    private static final double DOWNSAMPLING_TOLERANCE = 50;
    private static final double MAX_ITERATIONS = 100;
    
    private final List<MeshPoint> allVertices = new ArrayList<>();
    
    /**
     * Constructor.
     * 
     * @param max Required number of samples. Must be bigger than zero
     * @throws IllegalArgumentException if the input parameter is wrong
     */
    public UniformSpaceSampling(int max) {
        super(max);
    }
    
    @Override
    public boolean isBackedByOrigMesh() {
        return false;
    }

    @Override
    public void visitMeshFacet(MeshFacet facet) {
        if (facet != null) {
            allVertices.addAll(facet.getVertices());
        }
    }
    
    @Override
    public List<MeshPoint> getSamples() {
        // compute centroid of cell's points:
        List<MeshPoint> ret = createGrid().getNonEmptyCells().stream()
                .map(MeshFactory::createAverageMeshPoint)
                .collect(Collectors.toList());
        setRealSamples(ret.size());
        return ret;
    }
    
    @Override
    public String toString() {
        return "Uniform space sampling. " + super.toString();
    }
    
    protected List<MeshPoint> getOrigPoints() {
        return this.allVertices;
    }

    /**
     * Computes the average distance between vertices and their centroid.
     * 
     * @param vertices vertices
     * @param centroid their centroid
     * @return the average distance between vertices and their centroid.
     */
    protected double getAvgDist(Collection<MeshPoint> vertices, Point3d centroid) {
        return vertices.stream()
                .map(MeshPoint::getPosition)
                .mapToDouble(centroid::distance)
                .average()
                .orElse(0);
    }
    
    protected UniformGrid3d<MeshPoint> createGrid() {
        int numReducedVertices = Math.min(getRequiredSamples(), getOrigPoints().size());
        double k = CELL_RESIZE_INIT_VALUE;
        double step = CELL_RESIZE_INIT_STEP;
        int numCells;
        UniformGrid3d<MeshPoint> grid;
        
        MeshPoint centroid = MeshFactory.createAverageMeshPoint(allVertices);
        double avgDist = getAvgDist(allVertices, centroid.getPosition());
        
        int counter = 0;
        do {
            double cellSize = avgDist / k;
            grid =  UniformGrid3d.create(cellSize, allVertices, MeshPoint::getPosition);
            numCells = grid.numOccupiedCells();
            if (step > 0 && numCells > numReducedVertices) {
                step /= -CELL_RESIZE_STEP_CHANGE;
            } else if (step < 0 && numCells < numReducedVertices) {
                step /= -CELL_RESIZE_STEP_CHANGE;
            } else if (step == 0) {
                break;
            }
            if (counter++ > MAX_ITERATIONS) {
                break;
            }
            k += step;
        } while (numCells < numReducedVertices || numCells > numReducedVertices + DOWNSAMPLING_TOLERANCE);
        
        return grid;
    }
}