package cz.fidentis.analyst.engines.sampling.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Random sampling when vertices are selected randomly.
 * 
 * @author Radek Oslejsek
 */
public class RandomSampling extends PointSamplingVisitorImpl {
    
    private final List<MeshPoint> allVertices = new ArrayList<>();
    
    /**
     * Constructor.
     * 
     * @param max Maximal number of required samples. Must be bigger than zero
     * @throws IllegalArgumentException if the input parameter is wrong
     */
    public RandomSampling(int max) {
        super(max);
    }
     
    @Override
    public void visitMeshFacet(MeshFacet facet) {
        if (facet != null) {
            allVertices.addAll(facet.getVertices());
        }
    }        
    
    @Override
    public List<MeshPoint> getSamples() {
        // We can not generate more samples than there re is in the original mesh
        if (allVertices.size() <= getRequiredSamples()) {
            setRealSamples(allVertices.size());
            return Collections.unmodifiableList(allVertices);
        }

        // If the reduction is significant, then trigger random numbers (including the same number multiple times)
        // until the required number of samples is obtained.
        if (getRequiredSamples() / (double) allVertices.size() <= 0.2) {
            Random random = new Random();
            List<MeshPoint> ret = new ArrayList<>(getRequiredSamples());
            Set<Integer> usedIndex = new HashSet<>();
            while (ret.size() < getRequiredSamples()) {
                int rand = random.nextInt(allVertices.size());
                if (!usedIndex.add(rand)) {
                    ret.add(allVertices.get(rand));
                }
            }
            setRealSamples(ret.size());
            return ret;
        }
        
        // generate randomly ordered indexes:
        List<Integer> range = IntStream.range(0, allVertices.size()).boxed().collect(Collectors.toCollection(ArrayList::new));
        Collections.shuffle(range);


        if (getRequiredSamples() < allVertices.size() / 2) { // copy indices
            MeshPoint[] array = new MeshPoint[allVertices.size()];
            range.stream().limit(getRequiredSamples()).forEach(
                    i -> array[i] = allVertices.get(i)
            );
            List<MeshPoint> ret = Arrays.stream(array).filter(Objects::nonNull).collect(Collectors.<MeshPoint>toList());
            setRealSamples(ret.size());
            return ret;
        } else { // remove indices
            List<MeshPoint> copy = new ArrayList<>(allVertices);
            range.stream().limit(allVertices.size() - getRequiredSamples()).forEach(
                    i -> copy.set(i, null)
            );
            List<MeshPoint> ret = copy.parallelStream().filter(Objects::nonNull).collect(Collectors.<MeshPoint>toList());
            setRealSamples(ret.size());
            return ret;
        } 
    }

    @Override
    public boolean isBackedByOrigMesh() {
        return true;
    }

    @Override
    public String toString() {
        return "Random sampling. " + super.toString();
    }

}
