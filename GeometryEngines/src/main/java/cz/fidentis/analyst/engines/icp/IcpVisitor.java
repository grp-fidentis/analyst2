package cz.fidentis.analyst.engines.icp;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshVisitor;

import java.util.List;
import java.util.Map;

/**
 * Visitor applying the Iterative Closest Point (ICP) algorithm to register two meshes.
 * <strong>Inspected mesh facets are transformed</strong> (vertices are moved!) and the history 
 * of their transformations (transformation performed in each ICP iteration) is returned.
 * <p>
 * The main implementation is based on 
 * <pre>
 * Ferkova, Z. Comparison and Analysis of Multiple 3D Shapes [online]. 
 * Brno, 2016 [cit. 2022-02-08]. 
 * Available from: <a href="https://is.muni.cz/th/wx40f/">https://is.muni.cz/th/wx40f/</a>. 
 * Master's thesis. Masaryk University, Faculty of Informatics.
 * </pre>
 * Two ICP minimization objectives are implemented: Traditional asymmetric point-to-point
 * distance metric and 
 * a <a href="https://doi.org/10.1016/j.isprsjprs.2022.01.019">symmetric point-to-plane metric</a>,
 * which should provide wider convergence basin and higher convergence speed.
 * The second approach is used by default.
 * <p>
 * This visitor <strong>is not thread-safe</strong> due to efficiency reasons
 * and because of the algorithm principle, when it iterates multiple times through 
 * each inspected facet. Therefore, concurrent ICP transformation has to use 
 * an individual ICP visitor for each transformed (inspected) mesh facet.
 * Sequential inspection of multiple facets using a single visitor's instance 
 * is possible.
 * </p>
 * 
 * @author Maria Kocurekova
 * @author Radek Oslejsek
 * @author Zuzana Ferkova
 */
public interface IcpVisitor extends MeshVisitor   {
    
    /**
     * Returns the history of transformations (transformation performed in each 
     * ICP iteration for each inspected mesh facet).
     * Keys in the map contain mesh facets that were inspected and transformed. 
     * For each transformed facet, a list of transformations to the primary mesh
     * is stored. The order of transformations corresponds to the order of ICP iterations, 
     * i.e., the i-th value is the transformation applied in the i-th iteration on the visited mesh facet.
     * It also means that the size of the list corresponds to the number of iterations 
     * performed for given mesh facet.
     * 
     * @return The history of transformations (transformation performed in each ICP iteration for each inspected mesh facet).
     */
    Map<MeshFacet, List<IcpTransformation>> getTransformations();
    
    /**
     * Returns the maximal number of ICP iterations used for the computation.
     * 
     * @return the maximal number of ICP iterations used for the computation
     */
    //int getMaxIterations();
    
    /**
     * Returns maximal acceptable error used for the computation.
     * 
     * @return maximal acceptable error used for the computation
     */
    //double getError();
    
    /**
     * Returns {@code true} if the inspected mesh faces were also scaled.
     * 
     * @return {@code true} if the inspected mesh faces were also scaled.
     */
    //boolean getScale();
    
}
