package cz.fidentis.analyst.engines.symmetry;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.shapes.Plane;

import java.util.Collection;
import java.util.List;

/**
 * Stateless services for symmetry estimation.
 * It is a wrapper to the stateful implementation in {@link cz.fidentis.analyst.engines.symmetry.SymmetryVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface SymmetryServices {

    /**
     * Returns a symmetry plane.
     *
     * @param mesh Mesh model to be sub-sampled
     * @param strategy Symmetry estimation strategy.
     * @return a symmetry plane or {@code null}
     */
    static Plane estimate(MeshModel mesh, SymmetryConfig strategy) {
        return estimate(mesh.getFacets(), strategy);
    }

    /**
     * Returns a symmetry plane.
     *
     * @param mesh Mesh model to be sub-sampled
     * @param strategy Symmetry estimation strategy.
     * @return a symmetry plane or {@code null}
     */
    static Plane estimate(MeshFacet mesh, SymmetryConfig strategy) {
        return estimate(List.of(mesh), strategy);
    }

    /**
     * Returns a symmetry plane.
     *
     * @param mesh Mesh model to be sub-sampled
     * @param config Configuration of the service
     * @return a symmetry plane or {@code null}
     */
    static Plane estimate(Collection<MeshFacet> mesh, SymmetryConfig config) {
        var visitor = config.getVisitor();
        mesh.forEach(facet -> facet.accept(visitor));
        Plane plane =  visitor.getSymmetryPlane();
        visitor.dispose();
        return plane;
    }
}
