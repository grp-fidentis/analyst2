package cz.fidentis.analyst.engines.raycasting;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.engines.raycasting.impl.RayIntersectionVisitorImpl;

/**
 * Configuration of ray casting
 *
 * @author Radek Oslejsek
 *
 * @param ray Ray, must not be {@code null}
 * @param smoothing Smoothing strategy
 * @param filter If {@code true}, then only triangles with the same orientation are taken into account
 */
public record RayIntersectionConfig(Ray ray, MeshTriangle.Smoothing smoothing, boolean filter) {

    /**
     * Instantiates and returns a visitor.
     *
     * @return a visitor
     */
    public RayIntersectionVisitor getVisitor() {
        return new RayIntersectionVisitorImpl(ray, smoothing);
    }
}
