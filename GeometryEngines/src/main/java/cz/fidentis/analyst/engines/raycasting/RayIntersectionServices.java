package cz.fidentis.analyst.engines.raycasting;

import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.ray.RayIntersection;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

/**
 * Stateless services for ray casting.
 * It is a wrapper to the stateful implementation in {@link cz.fidentis.analyst.engines.raycasting.RayIntersectionVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface RayIntersectionServices {

    /**
     * Returns ray intersections <strong>in random order and without distances</strong>.
     *
     * @param octrees Meshes to be tested for ray intersection
     * @param config Configuration of the service
     * @return found intersections
     */
    static List<RayIntersection> computeUnsorted(Collection<Octree> octrees, RayIntersectionConfig config) {
        var visitor = config.getVisitor();
        octrees.forEach(octree -> octree.accept(visitor));
        return visitor.getUnsortedIntersections(config.filter());
    }

    /**
     * Returns ray intersections <strong>in random order and without distances</strong>.
     *
     * @param kdTree Mesh to be tested for ray intersection
     * @param config Configuration of the service
     * @return found intersections
     */
    static List<RayIntersection> computeUnsorted(Octree kdTree, RayIntersectionConfig config) {
        return computeUnsorted(List.of(kdTree), config);
    }

    /**
     * Returns found ray intersections <strong>sorted by distances</strong>.
     *
     * @param octrees Meshes to be tested for ray intersection
     * @param config Configuration of the service
     * @return found intersections
     */
    static SortedSet<RayIntersection> computeSorted(Collection<Octree> octrees, RayIntersectionConfig config) {
        var visitor = config.getVisitor();
        octrees.forEach(octree -> octree.accept(visitor));
        return visitor.getSortedIntersections(config.filter());
    }

    /**
     * Returns found ray intersections <strong>sorted by distances</strong>.
     *
     * @param octree Mesh to be tested for ray intersection. Must not be {@code null}
     * @param strategy Ray casting strategy and params. Must not be {@code null}
     * @return found intersections
     */
    static SortedSet<RayIntersection> computeSorted(Octree octree, RayIntersectionConfig strategy) {
        return computeSorted(List.of(octree), strategy);
    }

    /**
     * Returns a closets intersection point, either positive or negative, or {@code null}.
     *
     * @param octrees Meshes to be tested for ray intersection. Must not be {@code null}
     * @param strategy Ray casting strategy and params. Must not be {@code null}
     * @return found intersection
     */
    static RayIntersection computeClosest(Collection<Octree> octrees, RayIntersectionConfig strategy) {
        RayIntersectionVisitor visitor = strategy.getVisitor();
        octrees.forEach(octree -> octree.accept(visitor));
        return visitor.getClosestIntersection(strategy.filter());
    }

    /**
     * Returns a closets intersection point, either positive or negative, or {@code null}.
     *
     * @param octree Mesh to be tested for ray intersection. Must not be {@code null}
     * @param strategy Ray casting strategy and params.  Must not be {@code null}
     * @return found intersection
     */
    static RayIntersection computeClosest(Octree octree, RayIntersectionConfig strategy) {
        return computeClosest(List.of(octree), strategy);
    }
}
