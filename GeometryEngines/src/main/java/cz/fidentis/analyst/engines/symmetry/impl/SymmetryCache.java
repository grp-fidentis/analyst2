package cz.fidentis.analyst.engines.symmetry.impl;

import cz.fidentis.analyst.data.mesh.Curvature;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import javax.vecmath.Vector3d;
import java.util.List;

/**
 * Precomputed values for the symmetry plane estimation.
 *
 * @author Radek Oslejsek
 */
public class SymmetryCache {

    private Vector3d[][] normCosVec;
    private Vector3d[][] avgPos;
    private double avgGaussianCurvature;

    /**
     * Constructor.
     *
     * @param vertices Mesh vertices
     */
    public SymmetryCache(List<MeshPoint> vertices) {
        if (vertices == null || vertices.isEmpty()) {
            throw new IllegalArgumentException("points");
        }

        int size = vertices.size();

        normCosVec = new Vector3d[size][size];
        avgPos = new Vector3d[size][size];

        int counter = 0;
        for (int i = 0; i < size; i++) {
            MeshPoint meshPointI = vertices.get(i);

            Curvature gi = meshPointI.getCurvature();
            if (gi != null && gi.gaussian() != Double.NaN) {
                avgGaussianCurvature += gi.gaussian();
                counter++;
            }

            for (int j = 0; j < vertices.size(); j++) {
                MeshPoint meshPointJ = vertices.get(j);

                Vector3d ni = new Vector3d(meshPointI.getNormal());
                ni.sub(meshPointJ.getNormal());
                ni.normalize();
                normCosVec[i][j] = ni;

                Vector3d avrg = new Vector3d(meshPointI.getPosition());
                Vector3d aux = new Vector3d(meshPointJ.getPosition());
                avrg.add(aux);
                avrg.scale(0.5);
                avgPos[i][j] = avrg;
            }
        }
        if (counter != 0) {
            avgGaussianCurvature /= counter;
        } else {
            avgGaussianCurvature = Double.NaN;
        }
    }

    /**
     * Returns cosine of given normal vectors.
     *
     * @param i Index of the first vector
     * @param j Index of the second vector
     * @return cosine of given normals
     */
    public Vector3d getNormCosVec(int i, int j) {
        return normCosVec[i][j];
    }

    /**
     * Returns average position of given vertices
     *
     * @param i Index of the first vector
     * @param j Index of the second vector
     * @return average position of given vertices
     */
    public Vector3d getAvgPos(int i, int j) {
        return avgPos[i][j];
    }

    /**
     * Returns average Gaussian curvature
     * @return average Gaussian curvature
     */
    public double getAvgGaussianCurvature() {
        return avgGaussianCurvature;
    }
}
