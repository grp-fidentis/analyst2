package cz.fidentis.analyst.engines.distance.impl;

import com.jogamp.opengl.*;
import cz.fidentis.analyst.data.mesh.*;
import cz.fidentis.analyst.data.mesh.measurement.RawFacetDistances;
import cz.fidentis.analyst.glsl.*;
import cz.fidentis.analyst.glsl.buffergroups.BufferGroupDef;
import cz.fidentis.analyst.glsl.buffergroups.GlslBufferGroup;
import cz.fidentis.analyst.glsl.buffers.BufferDef;
import cz.fidentis.analyst.glsl.code.GlslProgram;
import cz.fidentis.analyst.glsl.code.GlslProgramDef;

import javax.vecmath.*;
import java.nio.ByteBuffer;
import java.util.*;

import static com.jogamp.opengl.GL.GL_FALSE;
import static com.jogamp.opengl.GL.GL_TRUE;

/**
 * This visitor searches for the closest points of the primary mesh (provided to the constructor)
 * and inspected meshes by casting rays (RC) in the direction of normal vectors on the inspected mesh.
 * <p>
 * Computation is done on the GPU (if available).
 * After GPU finishes visit (blocking), java-side reads the results from GPU.
 * </p>
 * <p>
 * The distance is computed either as absolute or relative. Absolute
 * represents Euclidean distance (all numbers are positive). On the contrary,
 * relative distance considers orientation of the visited mesh (determined by its normal vectors)
 * and produces positive or negative distances depending on whether the primary
 * mesh is "in front of" or "behind" the given vertex.
 * </p>
 *
 * @author Pavol Kycina
 */
public class MeshDistanceRCGPU extends MeshDistanceVisitorImpl {

    private static final int WORKGROUP_SIZE = 64; // this must be the same as defined in shaders!

    private final GLContext context;
    private final GlslBufferGroup ssbos;

    /**
     * Constructor.
     * Creates 3D uniform grid and populates it with triangles from facets.
     *
     * @param context active OpenGL context on which makeCurrent() can be called now and during visit
     * @param mainFacets facets from which to take triangles, at least two triangles must be present in total
     * @param relativeDistance If true, then the visitor calculates the relative distances with respect
     *                         to the normal vectors of source facets (normal vectors have to present),
     *                         i.e., we can get negative distances.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public MeshDistanceRCGPU(GLContext context, Collection<MeshFacet> mainFacets, boolean relativeDistance) {
        super(relativeDistance, true); // parallel here does not make sense
        this.context = context;

        context.makeCurrent();
        GL4 gl = context.getGL().getGL4();

        ssbos = GlslServices.registerBuffers(context, BufferGroupDef.RAY_CASTING_DISTANCE);

        int trianglesCount = mainFacets.stream().map(MeshFacet::getNumTriangles).reduce(Integer::sum).get();
        int verticesCount = mainFacets.stream().map(MeshFacet::getNumberOfVertices).reduce(Integer::sum).get();

        if(trianglesCount < 2) {
            throw new IllegalArgumentException("This GPU distance measurement method needs at least two triangles.");
        }

        fillBufferWithVertices(mainFacets, verticesCount);

        fillBufferWithTriangles(mainFacets, trianglesCount);

        createUniformGridOnGPU(gl, trianglesCount);

        context.release();
    }

    @Override
    public void visitMeshFacet(MeshFacet comparedFacet) {
        final List<MeshPoint> rays = comparedFacet.getVertices();

        final List<Double> distances = new ArrayList<>();
        final List<MeshPoint> nearestPoints = new ArrayList<>();

        context.makeCurrent();

        GlslProgram distanceProgram = GlslServices.useProgram(context, GlslProgramDef.DISTANCE_RAY_CASTING_GLSL_PROGRAM);
        ssbos.bindSsboBuffers();
        distanceProgram.setNumberVar("relative_distance_multiplier", relativeDistance() ? -1 : 1);

        int raysCount = rays.size();
        fillBufferWithRays(rays);

        // initialize read buffer so GPU will have available buffer to write into
        ssbos.getSSboBuffer(BufferDef.RC_DIST_DISTANCES).allocate(raysCount, false);

        // execute on GPU and wait for data
        GlslServices.runProgram(context, (raysCount / WORKGROUP_SIZE) + 1, 1, 1);

        // load the results into lists
        readResultsFromGPU(raysCount, distances, nearestPoints);
        context.release();

        getDistancesOfVisitedFacets().addRawFacetMeasurement(new RawFacetDistances(comparedFacet, distances, nearestPoints));
    }

    /**
     * Creates 3D bounded uniform grid on GPU from given triangles.
     * Created grid will be available in cell bounds buffer and sorted triangles buffer on GPU.
     * Parameters of the grid will be available in grid info buffer.
     *
     * @param gl active GL context
     * @param trianglesCount how many triangles are already loaded in buffers and how many we are going to put inside the grid
     */
    private void createUniformGridOnGPU(GL4 gl, int trianglesCount) {
        ssbos.bindSsboBuffers();

        ssbos.getSSboBuffer(BufferDef.RC_DIST_TRIANGLES_REDUCTION).allocate(trianglesCount, false);
        ssbos.getSSboBuffer(BufferDef.RC_DIST_GRID_INFO).allocate(1, false);

        // prepare triangle stats
        GlslServices.useProgram(context, GlslProgramDef.TRIANGLE_STATS_GLSL_PROGRAM);
        GlslServices.runProgram(context, (trianglesCount / WORKGROUP_SIZE) + 1, 1, 1);

        // calculate grid parameters
        GlslProgram gridParametersProgram = GlslServices.useProgram(context, GlslProgramDef.GRID_PARAMETERS_GLSL_PROGRAM);
        doReductionSteps(trianglesCount, false, gridParametersProgram);

        int[] boxedCellCount = new int[1];
        ssbos.getSSboBuffer(BufferDef.RC_DIST_GRID_INFO).readFrom(0, 1, byteBuffer -> {
            boxedCellCount[0] = byteBuffer.getInt() * byteBuffer.getInt() * byteBuffer.getInt(); // multiply grid cell dimensions
        });
        int cellCount = boxedCellCount[0];

        //System.out.println("cell count: " + cell_count); // only for debugging

        ssbos.getSSboBuffer(BufferDef.RC_DIST_CELL_ELEMENTS).allocate(cellCount, true);
        ssbos.getSSboBuffer(BufferDef.RC_DIST_CELL_BOUNDS).allocate(cellCount, false);

        // construction phase 1 -- calculate triangle overlap count for each cell
        GlslServices.useProgram(context, GlslProgramDef.GRID_CELL_OVERLAP_COUNT_GLSL_PROGRAM);
        GlslServices.runProgram(context, (trianglesCount / WORKGROUP_SIZE) + 1, 1, 1);

        // construction phase 2 -- prefix-sum, calculate cell bounds for sorted triangles array
        GlslProgram cellBoundsProgram = GlslServices.useProgram(context, GlslProgramDef.GRID_CELL_BOUNDS_GLSL_PROGRAM);

        // phase 2: up-sweep
        doReductionSteps(cellCount, false, cellBoundsProgram);

        ssbos.getSSboBuffer(BufferDef.RC_DIST_CELL_ELEMENTS).readFrom((cellCount - 1), 1, byteBuffer -> {
            int sortedTrianglesCount = byteBuffer.getInt();
            ssbos.getSSboBuffer(BufferDef.RC_DIST_SORTED_TRIANGLES).allocate(sortedTrianglesCount, false);
        });

        ssbos.getSSboBuffer(BufferDef.RC_DIST_CELL_ELEMENTS).writeItemsTo((cellCount - 1), 1, byteBuffer -> {
            byteBuffer.putInt(0); // put identity element (sum) at the end of an array - Blelloch algorithm
        });

        // phase 2: down-sweep
        doReductionSteps(cellCount, true, cellBoundsProgram);

        gl.glCopyNamedBufferSubData(
                ssbos.getSSboBuffer(BufferDef.RC_DIST_CELL_ELEMENTS).getGlName(),
                ssbos.getSSboBuffer(BufferDef.RC_DIST_CELL_BOUNDS).getGlName(),
                0, 0, cellCount * 4L);

        // construction phase 3 -- fill sorted triangles
        ssbos.getSSboBuffer(BufferDef.RC_DIST_CELL_ELEMENTS).allocate(cellCount, true);
        GlslServices.useProgram(context, GlslProgramDef.GRID_SORTED_TRIANGLES_GLSL_PROGRAM);
        GlslServices.runProgram(context, (trianglesCount / WORKGROUP_SIZE) + 1, 1, 1);
    }

    /**
     * Perform reduction in multiple steps. Each time, this informs shaders about current reduction step and
     * dispatches as many compute shaders as needed for current reduction level.
     * Dispatches exactly the number of remaining elements in active reduction, so one invocation may be extra sometimes.
     *
     * @param elements number of elements on which perform reduction
     * @param reverse if reduction should be in reverse (broadcast) - from max level (one invocation) to zero level
     */
    private void doReductionSteps(int elements, boolean reverse, GlslProgram program) {
        int levels = 0;
        while(1 << levels < elements) {
            levels++; // how many reduce iterations are going to happen
        }

        for (int i = 0; i < levels; i++) {
            int level = reverse ? -i + levels - 1 : i;
            int invocations = (int) Math.ceil(elements / Math.pow(2, level + 1));

            // tells the shader on which level it should reduce
            program.setNumberVar("level", level);

            switch (program.getProgramDef()) {
                // tells the shader if it is the last run (invocations == 1)
                case GRID_PARAMETERS_GLSL_PROGRAM -> program.setNumberVar("invocations", invocations);
                // tells the shader the reduction direction upsweep/downsweep
                case GRID_CELL_BOUNDS_GLSL_PROGRAM -> program.setNumberVar("upsweep", reverse ? GL_FALSE : GL_TRUE);
                default -> {}
            }

            GlslServices.runProgram(context, (invocations / WORKGROUP_SIZE) + 1, 1, 1);
        }
    }

    /**
     * Reads results from GPU buffer and appends them to the provided lists.
     *
     * @param raysCount how many rays are involved in this computation
     * @param distances list where raw computed distances will be appended, empty in most cases
     * @param nearestPoints list where raw computed intersection points will be appended, empty in most cases
     */
    private void readResultsFromGPU(int raysCount, List<Double> distances, List<MeshPoint> nearestPoints) {
        // todo: isnt boxing here expensive?

        ssbos.getSSboBuffer(BufferDef.RC_DIST_DISTANCES).readFrom(0, raysCount, byteBuffer -> {
            for (int i = 0; i < raysCount; i++) {
                float v1 = byteBuffer.getFloat();
                float v2 = byteBuffer.getFloat();
                float v3 = byteBuffer.getFloat();
                float distance = byteBuffer.getFloat();

                // check if this ray intersected
                if(v1 != Float.POSITIVE_INFINITY || v2 != Float.POSITIVE_INFINITY || v3 != Float.POSITIVE_INFINITY) {
                    distances.add((double) distance);
                    nearestPoints.add(MeshFactory.createMeshPoint(new Point3d(v1, v2, v3)));
                }else {
                    distances.add(Double.POSITIVE_INFINITY);
                    nearestPoints.add(null);
                }
            }
        });
    }

    /**
     * Fills buffer with vertex position.
     *
     * @param facets facets from which to take vertices
     * @param verticesCount sum of all vertices from given facets
     */
    private void fillBufferWithVertices(Collection<MeshFacet> facets, int verticesCount) {
        ssbos.getSSboBuffer(BufferDef.RC_DIST_VERTICES).allocate(verticesCount, false);
        ssbos.getSSboBuffer(BufferDef.RC_DIST_VERTICES).writeItemsTo(0, verticesCount, byteBuffer -> {
            for(MeshFacet facet : facets) {
                for(MeshPoint point : facet.getVertices()) {
                    putVec3(byteBuffer, point.getPosition());
                    putVec3(byteBuffer, point.getNormal());
                }
            }
        });
    }

    /**
     * Fills buffer with triangles (indices of their vertices - using index-array representation).
     *
     * @param facets facets from which to take triangles
     * @param trianglesCount sum of all vertices from given facets
     */
    private void fillBufferWithTriangles(Collection<MeshFacet> facets, int trianglesCount) {
        ssbos.getSSboBuffer(BufferDef.RC_DIST_TRIANGLES).allocate(trianglesCount, false);
        ssbos.getSSboBuffer(BufferDef.RC_DIST_TRIANGLES).writeItemsTo(0, trianglesCount, byteBuffer -> {
            for(MeshFacet facet : facets) {
                for (MeshTriangle triangle : facet) {
                    putIVec3(byteBuffer, triangle.getIndex1(), triangle.getIndex2(), triangle.getIndex3());
                }
            }
        });
    }

    /**
     * Fills buffer with given rays.
     *
     * @param rays rays to be appended into the buffer
     */
    private void fillBufferWithRays(Collection<MeshPoint> rays) {
        ssbos.getSSboBuffer(BufferDef.RC_DIST_RAYS).allocate(rays.size(), false);
        ssbos.getSSboBuffer(BufferDef.RC_DIST_RAYS).writeItemsTo(0, rays.size(), byteBuffer -> {
            for(MeshPoint ray : rays) {
                putVec3(byteBuffer, ray.getPosition());
                putVec3(byteBuffer, ray.getNormal());
            }
        });
    }

    /**
     * Put vec3 openGL data structure into the buffer (16 bytes due to openGL std430 alignment)
     *
     * @param buffer buffer where vec3 will be appended
     * @param vec vector from which float values will be taken
     */
    private static void putVec3(ByteBuffer buffer, Tuple3d vec) {
        putVec3(buffer, (float) vec.x, (float) vec.y, (float) vec.z);
    }

    /**
     * Put vec3 openGL data structure containing given floats into the buffer (16 bytes due to openGL std430 alignment)
     *
     * @param buffer buffer where vec3 will be appended
     * @param f1 x
     * @param f2 y
     * @param f3 z
     */
    private static void putVec3(ByteBuffer buffer, float f1, float f2, float f3) {
        buffer.putFloat(f1);
        buffer.putFloat(f2);
        buffer.putFloat(f3);
        buffer.putFloat(0); // unused 4th element of vec4, required for openGL alignment
    }

    /**
     * Put ivec3 openGL data structure containing given integers into the buffer (16 bytes due to openGL std430 alignment)
     *
     * @param buffer buffer where ivec3 will be appended
     * @param i1 x
     * @param i2 y
     * @param i3 z
     */
    private static void putIVec3(ByteBuffer buffer, int i1, int i2, int i3) {
        buffer.putInt(i1);
        buffer.putInt(i2);
        buffer.putInt(i3);
        buffer.putInt(0); // unused 4th element of vec4, required for openGL alignment
    }

    @Override
    public boolean isThreadSafe() {
        return false;
    }

    @Override
    public void dispose() {
        context.makeCurrent();
        ssbos.destroyBuffers();
        context.release();
    }
}
