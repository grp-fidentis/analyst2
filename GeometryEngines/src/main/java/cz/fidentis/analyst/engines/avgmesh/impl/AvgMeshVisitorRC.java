package cz.fidentis.analyst.engines.avgmesh.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.engines.avgmesh.AvgMeshVisitor;
import cz.fidentis.analyst.engines.distance.impl.MeshDistanceNN;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionServices;

import javax.vecmath.Vector3d;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Visitor capable to create an average face by deforming main face template so
 * that its vertices are in the "average position" with respect to the visited
 * faces. The average position is computed as the average position of
 * intersections of visited faces. Intersection are computed using <b>ray casting</b>
 * <strong>
 * It is supposed that the inspected faces are already registered (superimposed with the template ).
 * </strong>
 *
 * @author Enkh-Undral EnkhBayar
 * @author Radek Oslejsek
 */
public class AvgMeshVisitorRC implements AvgMeshVisitor {

    private MeshModel avgMeshModel = null;
    
    private int numInspectedFacets = 0;
    
    private final Map<MeshFacet, List<Double>> transformations = new HashMap<>();
    
    /**
     * Constructor.
     *
     * @param templateFacet Mesh facet which is transformed to the averaged mesh.
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorRC(MeshFacet templateFacet) {
        this(new HashSet<>(Collections.singleton(templateFacet)));
        if (templateFacet == null) {
            throw new IllegalArgumentException("templateFacet");
        }
    }

    /**
     * Constructor.
     *
     * @param templateFacets Mesh facets that are transformed to the averaged mesh
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorRC(Collection<MeshFacet> templateFacets) {
        if (templateFacets == null || templateFacets.isEmpty()) {
            throw new IllegalArgumentException("templateFacets");
        }

        // fill vertTarns with empty list for each facet
        templateFacets.forEach(f -> transformations.put(f,
                Stream.generate(() -> 0.0)
                        .limit(f.getVertices().size())
                        .collect(Collectors.toList())
        ));
    }

    /**
     * Constructor.
     *
     * @param templateModel Mesh model which is transformed to the averaged mesh
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorRC(MeshModel templateModel) {
        this(new HashSet<>(templateModel.getFacets()));
    }
    
    @Override
    public boolean isThreadSafe() {
        return true;
    }

    @Override
    public void visitOctree(Octree octree) {
        avgMeshModel = null;
        numInspectedFacets++;

        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        for (MeshFacet mainFacet : transformations.keySet()) {
            final List<Future<Double>> results = new ArrayList<>(mainFacet.getNumberOfVertices());
            
            for (int i = 0; i < mainFacet.getNumberOfVertices(); i++) {
                // cast the ray from the mes vertex
                Ray ray = new Ray(mainFacet.getVertex(i).getPosition(), mainFacet.getVertex(i).getNormal());
                results.add(executor.submit(() -> singleRayIntersection(octree, ray)));
            }

            executor.shutdown();
            while (!executor.isTerminated()) {
            }
            try {
                int i = 0;
                for (Future<Double> res : results) {
                    Double dist = res.get(); // waits until all computations are finished
                    if (!dist.isNaN() && !dist.isInfinite()) { // infinite == no hit :-(
                        synchronized (this) {
                            transformations.get(mainFacet).set(i,
                                    transformations.get(mainFacet).get(i) + dist);
                        }
                    }
                    i++;
                }
            } catch (final InterruptedException | ExecutionException ex) {
                Logger.getLogger(MeshDistanceNN.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public MeshModel getAveragedMeshModel() {
        if (avgMeshModel == null) {
            avgMeshModel = MeshFactory.createEmptyMeshModel();
            for (MeshFacet f: transformations.keySet()) { // clone all facets of the template face
                MeshFacet newFacet = MeshFactory.cloneMeshFacet(f);
                IntStream.range(0, newFacet.getNumberOfVertices()).parallel().forEach(i -> {
                    Vector3d tr = new Vector3d(newFacet.getVertex(i).getNormal()); // get original normal
                    // Dividing by numInspectedFacets instead of the number of sufficient intersections
                    // stabilizes the vertex in its original position if the intersection has not been found.
                    tr.scale(transformations.get(f).get(i) / numInspectedFacets); // move to average
                    newFacet.getVertex(i).getPosition().add(tr);
                });
                avgMeshModel.addFacet(newFacet);
            }
        }
        return avgMeshModel;
    }

    protected int getNumInspectedFacets() {
        return numInspectedFacets;
    }

    protected Map<MeshFacet, List<Double>> getTransformations() {
        return transformations;
    }

    protected void setAvgMeshModel(MeshModel avgMeshModel) {
        this.avgMeshModel = avgMeshModel;
    }

    protected void setNumInspectedFacets(int numInspectedFacets) {
        this.numInspectedFacets = numInspectedFacets;
    }

    /**
     * Computes ray intersection and returns its distance, either positive or negative.
     * Returns {@code NaN} if no intersection was found.
     * 
     * @param octree Octree
     * @param ray Ray
     * @return distance to the intersection point.
     */
    protected double singleRayIntersection(Octree octree, Ray ray) {
        RayIntersection ri = RayIntersectionServices.computeClosest(
                octree,
                new RayIntersectionConfig(ray, MeshTriangle.Smoothing.NONE, true)); // filter by normal orientation
        return (ri == null) ? Double.NaN : ri.getDistance();
    }
}
