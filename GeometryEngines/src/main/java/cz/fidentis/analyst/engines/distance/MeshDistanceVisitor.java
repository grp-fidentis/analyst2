package cz.fidentis.analyst.engines.distance;

import cz.fidentis.analyst.data.mesh.MeshVisitor;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;

import java.io.Serializable;

/**
 * Interface for visitors that search for the distance between inspected meshes (their vertices)
 * to the given primary mesh, which is provided to the visitor's constructor.
 *
 * @author Daniel Schramm
 * @author Radek Oslejsek
 * @author Pavol Kycina
 */
public interface MeshDistanceVisitor extends MeshVisitor, Serializable {

    /**
     * Returns the distance of inspected mesh facets to the source mesh facets.
     * <p>
     *     If <b>non or multiple</b> closest points were found or if the point was auto-cropped, then
     *     the distance is set to {@code Double.POSITIVE_INFINITY} and the nearest neighbor is set to {@code null}.
     * </p>
     * 
     * @return the distance of inspected mesh facets to the source mesh facets.
     */
    MeshDistances getDistancesOfVisitedFacets();

    /**
     * Returns {@code true} if the distance was computed as relative 
     * (with possibly negative values). 
     * 
     * @return {@code true} if the distance is computed as relative, {@code false} otherwise.
     */
    boolean relativeDistance();
    
    /**
     * Returns {@code true} if the distance computation is parallel.
     * 
     * @return {@code true} if the distance computation is parallel.
     */
    boolean inParallel();
}
