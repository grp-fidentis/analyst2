package cz.fidentis.analyst.engines.distance.impl;

import cz.fidentis.analyst.engines.distance.MeshDistanceVisitor;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistancesImpl;

import java.io.Serial;

/**
 * An abstract class for distance visitors that search for the distances of inspected meshes (their vertices)
 * to the primary mesh, which is provided to the visitor's constructor.
 *
 * @author Daniel Schramm
 * @author Radek Oslejsek
 */
public abstract class MeshDistanceVisitorImpl implements MeshDistanceVisitor {

    @Serial
    private static final long serialVersionUID = 1L;

    private final MeshDistances meshDistance = new MeshDistancesImpl();

    private final boolean relativeDist;
    
    private final boolean parallel;
    

    /**
     * Constructor.
     * 
     * @param relativeDistance If true, then the visitor calculates the relative distances with respect 
     * to the normal vectors of source facets (normal vectors have to present), 
     * i.e., we can get negative distances.
     * @param parallel If {@code true}, then the algorithm runs concurrently utilizing all CPU cores
     * @throws IllegalArgumentException if some parameter is wrong
     */
    protected MeshDistanceVisitorImpl(boolean relativeDistance, boolean parallel) {
        this.relativeDist = relativeDistance;
        this.parallel = parallel;
    }

    @Override
    public MeshDistances getDistancesOfVisitedFacets() {
        return meshDistance;
    }

    @Override
    public boolean relativeDistance() {
        return this.relativeDist;
    }
    
    @Override
    public boolean inParallel() {
        return parallel;
    }

    @Override
    public void dispose() {}
}
