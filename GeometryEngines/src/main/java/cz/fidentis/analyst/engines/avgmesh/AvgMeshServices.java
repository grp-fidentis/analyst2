package cz.fidentis.analyst.engines.avgmesh;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.Octree;

import java.util.Collection;
import java.util.Collections;

/**
 * Stateless services for average face calculation.
 *
 * @author Radek Oslejsek
 */
public interface AvgMeshServices {

    /**
     * Returns a list of samples from the given mesh.
     *
     * @param kdTree Mesh to be averaged
     * @param config Configuration of the service
     * @return the average mesh or {@code null}
     */
    static MeshModel computeFromNearestNeighbors(KdTree kdTree, AvgMeshConfig config) {
        return computeFromNearestNeighbors(Collections.singleton(kdTree), config);
    }

    /**
     * Returns a list of samples from the given mesh.
     *
     * @param kdTrees Meshes to be averaged
     * @param config Configuration of the service
     * @return the average mesh or {@code null}
     */
    static MeshModel computeFromNearestNeighbors(Collection<KdTree> kdTrees, AvgMeshConfig config) {
        var visitor = config.getNearestNeighborsVisitor();
        kdTrees.forEach(kdtree -> kdtree.accept(visitor));
        return visitor.getAveragedMeshModel();
    }

    /**
     * Returns a list of samples from the given mesh.
     *
     * @param octree Mesh to be averaged
     * @param config Configuration of the service
     * @return the average mesh or {@code null}
     */
    static MeshModel computeByRayCasting(Octree octree, AvgMeshConfig config) {
        return computeByRayCasting(Collections.singleton(octree), config);
    }

    /**
     * Returns a list of samples from the given mesh.
     *
     * @param octrees Meshes to be averaged
     * @param config Configuration of the service
     * @return the average mesh or {@code null}
     */
    static MeshModel computeByRayCasting(Collection<Octree> octrees, AvgMeshConfig config) {
        var visitor = config.getRayCastingVisitor();
        octrees.forEach(octree -> octree.accept(visitor));
        return visitor.getAveragedMeshModel();
    }
}
