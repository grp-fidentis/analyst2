package cz.fidentis.analyst.engines.curvature;

import cz.fidentis.analyst.engines.curvature.impl.CurvatureVisitorImpl;

/**
 * Configuration for the curvature computation.
 *
 * @author Radek Oslejsek
 */

public record CurvatureConfig() {

    /**
     * Instantiates and returns a visitor.
     *
     * @return a visitor
     */
    public CurvatureVisitor getVisitor() {
        return new CurvatureVisitorImpl();
    }
}
