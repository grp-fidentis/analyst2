package cz.fidentis.analyst.engines.bbox.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.engines.bbox.BoundingBoxVisitor;

import javax.vecmath.Point3d;
import java.io.Serial;
import java.io.Serializable;

/**
 * Visitor that computes a 3D bounding box (cube).
 * <p>
 * This visitor is thread-safe. 
 * </p>
 * 
 * @author Radek Oslejsek
 */
public class BoundingBoxVisitorImpl implements BoundingBoxVisitor, Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final Point3d maxPoint = new Point3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
    private final Point3d minPoint = new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
    
    @Override
    public synchronized void visitMeshFacet(MeshFacet facet) {
        facet.getVertices().stream()
                .map(MeshPoint::getPosition)
                .forEach(point -> {
                    minPoint.x = Math.min(minPoint.x, point.x);
                    minPoint.y = Math.min(minPoint.y, point.y);
                    minPoint.z = Math.min(minPoint.z, point.z);

                    maxPoint.x = Math.max(maxPoint.x, point.x);
                    maxPoint.y = Math.max(maxPoint.y, point.y);
                    maxPoint.z = Math.max(maxPoint.z, point.z);
                });
    }
    
    @Override
    public Box getBoundingBox() {
        return minPoint.equals(new Point3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY)) &&
                maxPoint.equals(new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY))
                ? null
                : new Box(minPoint, maxPoint);
    }
}
