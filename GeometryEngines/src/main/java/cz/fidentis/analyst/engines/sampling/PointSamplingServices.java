package cz.fidentis.analyst.engines.sampling;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.util.Collection;
import java.util.List;

/**
 * Stateless services for point sampling.
 * It is a wrapper to the stateful implementation in {@link cz.fidentis.analyst.engines.sampling.PointSamplingVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface PointSamplingServices {

    /**
     * Returns a list of samples from the given mesh.
     *
     * @param mesh Mesh model to be sub-sampled
     * @param config Configuration of the service
     * @return a point sampler or {@code null}
     */
    static List<MeshPoint> sample(MeshModel mesh, PointSamplingConfig config) {
        return sample(mesh.getFacets(), config);
    }

    /**
     * Returns a list of samples from the given mesh.
     *
     * @param mesh Mesh facet to be sub-sampled
     * @param config Configuration of the service
     * @return a point sampler or {@code null}
     */
    static List<MeshPoint> sample(MeshFacet mesh, PointSamplingConfig config) {
        return sample(List.of(mesh), config);
    }

    /**
     * Returns a list of samples from the given mesh.
     *
     * @param mesh Mesh facets to be sub-sampled
     * @param config Configuration of the service
     * @return a point sampler or {@code null}
     */
    static List<MeshPoint> sample(Collection<MeshFacet> mesh, PointSamplingConfig config) {
        var visitor = config.getVisitor();
        mesh.forEach(facet -> facet.accept(visitor));
        List<MeshPoint> samples = visitor.getSamples();
        visitor.dispose();
        return samples;
    }

}
