package cz.fidentis.analyst.engines.sampling;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.engines.sampling.impl.*;

/**
 * Point sampling strategies and their configuration.
 *
 * @author Radek Oslejsek

 * @param method     Point sampling method.
 * @param maxSamples Maximal number of required samples. Must be bigger than zero. Ignored for the "no sampling" method.
 * @param smoothing  Surface smoothing strategy. Used only for sampling methods, e.g., Poisson, that produce samples
 *                   from the mesh surface that differ from mesh vertices. For other methods can be {@code null}.
 */
public record PointSamplingConfig(Method method, int maxSamples, MeshTriangle.Smoothing smoothing) {

    /**
     * Simplified constructor for no smoothing.
     *
     * @param method Sampling method
     * @param maxSamples Required number of samples.
     */
    public PointSamplingConfig(Method method, int maxSamples) {
        this(method, maxSamples, MeshTriangle.Smoothing.NONE);
    }

    /**
     * Constructor for Poisson disk sampling with given minimal distance instance of maximal number of samples.
     *
     * @param minimalDistance Minimal distance
     * @param smoothing Smoothing
     */
    public PointSamplingConfig(double minimalDistance, MeshTriangle.Smoothing smoothing) {
        this(Method.POISSON, PoissonDiskSubSampling.distanceToNumSamples(minimalDistance), smoothing);
    }

    /**
     * Instantiates and returns a point sampling visitor.
     *
     * @return a point sampling visitor
     */
    public PointSamplingVisitor getVisitor() {
        return switch (method) {
            case RANDOM -> new RandomSampling(maxSamples);
            case POISSON -> new PoissonDiskSubSampling(maxSamples, smoothing);
            case POISSON_OPENCL -> new PoissonDiskSubSamplingGPU(maxSamples, smoothing);
            case NO_SAMPLING -> new NoSampling();
            case UNIFORM_SPACE -> new UniformSpaceSampling(maxSamples);
            case UNIFORM_SURFACE -> new UniformSurfaceSampling(maxSamples);
            case CURVATURE_MEAN -> new CurvatureSampling(CurvatureSampling.CurvatureAlg.MEAN, maxSamples);
            case CURVATURE_GAUSSIAN -> new CurvatureSampling(CurvatureSampling.CurvatureAlg.GAUSSIAN, maxSamples);
            case CURVATURE_MAX -> new CurvatureSampling(CurvatureSampling.CurvatureAlg.MAX, maxSamples);
            case CURVATURE_MIN -> new CurvatureSampling(CurvatureSampling.CurvatureAlg.MIN, maxSamples);
        };
    }

    /**
     * If {@code true}, then the returned points samples are points from the original mesh.
     * Therefore, the transformation of the original mesh also transform these samples.
     * If {@code false}, then new points are returned that are independent on the original mesh.
     *
     * @return {@code true} if the point samples include points of the original mesh
     */
    public boolean isBackedByOrigMesh() {
        var visitor = getVisitor();
        boolean ret = visitor.isBackedByOrigMesh();
        visitor.dispose();
        return ret;
    }

    /**
     * Sampling method.
     *
     * @author Radek Oslejsek
     */
    public enum Method {
        /**
         * Fake sampling where all original mesh vertices are used.
         */
        NO_SAMPLING,

        /**
         * Randomly selected mesh vertices.
         */
        RANDOM,

        /**
         * The mesh is uniformly divided into a 3D grid, then the average position (centroid)
         * in each non-empty cell is used as the sample. It means that samples may not lie on the mesh surface.
         */
        UNIFORM_SPACE,

        /**
         * The mesh is uniformly divided into a 3D grid, then a random mesh vertex in each non-empty cell
         * is used as the sample. It means that samples lie on the mesh surface.
         */
        UNIFORM_SURFACE,

        /**
         * The Poisson disk sampling that produces uniform distribution of samples lying on the surface. But the
         * samples differ from mesh vertices.
         */
        POISSON,

        /**
         * The Poisson disk sampling that produces uniform distribution of samples lying on the surface. But the
         * samples differ from mesh vertices - Uses OpenCL to perform RayIntersection
         */
        POISSON_OPENCL,

        /**
         * A relevance-based point sampling using the highest mean curvature.
         */
        CURVATURE_MEAN,

        /**
         * A relevance-based point sampling using the highest Gaussian curvature.
         */
        CURVATURE_GAUSSIAN,

        /**
         * A relevance-based point sampling using the highest max curvature.
         */
        CURVATURE_MAX,

        /**
         * A relevance-based point sampling using the highest min curvature.
         */
        CURVATURE_MIN
    }

}
