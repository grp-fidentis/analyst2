package cz.fidentis.analyst.engines.curvature;

import cz.fidentis.analyst.data.mesh.Curvature;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.curvature.impl.CurvatureUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Stateless services for curvature calculation.
 * It is a wrapper to the stateful implementation in {@link cz.fidentis.analyst.engines.curvature.CurvatureVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface CurvatureServices {

    /**
     * Sets curvature to visited mesh vertices.
     *
     * @param mesh Mesh model to be inspected
     * @param config Configuration of the service
     */
    static void computeAndSet(MeshModel mesh, CurvatureConfig config) {
        computeAndSet(mesh.getFacets(), config);
    }

    /**
     * Sets curvature to visited mesh vertices.
     *
     * @param mesh Mesh facet to be inspected
     * @param config Configuration of the service
     */
    static void computeAndSet(MeshFacet mesh, CurvatureConfig config) {
        computeAndSet(List.of(mesh), config);
    }

    /**
     * Sets curvature to visited mesh vertices.
     *
     * @param mesh Mesh facets to be inspected
     * @param config Configuration of the service
     */
    static void computeAndSet(Collection<MeshFacet> mesh, CurvatureConfig config) {
        var visitor = config.getVisitor();
        mesh.forEach(facet -> facet.accept(visitor));
    }

    /**
     * Calculates principal curvatures and principal directions of a single point lying on a mesh surface
     *
     * @param point Point on a surface
     * @param facets Mesh surface
     * @return curvature
     */
    static Curvature compute(MeshPoint point, Collection<MeshFacet> facets) {
        return CurvatureUtil.calculatePrincipalCurvatures(point, facets);
    }

    /**
     * Calculates principal curvatures and principal directions of a single point lying on a mesh surface
     *
     * @param point Point on a surface
     * @param facet Mesh surface
     * @return curvature
     */
    static Curvature compute(MeshPoint point, MeshFacet facet) {
        return compute(point, Collections.singleton(facet));
    }

    /**
     * Calculates principal curvatures and principal directions of a single point lying on a mesh surface
     *
     * @param point Point on a surface
     * @param mesh Mesh surface
     * @return curvature
     */
    static Curvature compute(MeshPoint point, MeshModel mesh) {
        return compute(point, mesh.getFacets());
    }
}
