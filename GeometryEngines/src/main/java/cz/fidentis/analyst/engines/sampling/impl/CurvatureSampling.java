package cz.fidentis.analyst.engines.sampling.impl;

import cz.fidentis.analyst.data.mesh.Curvature;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.curvature.CurvatureConfig;
import cz.fidentis.analyst.engines.curvature.CurvatureServices;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A relevance-based point sampling using the highest curvature
 * (see <a href="https://graphics.stanford.edu/papers/zipper/zipper.pdf">this paper</a>).
 * If curvature is not set in inspected facets, then it is computed and set automatically.
 * This visitor is tread-safe.
 * 
 * @author Radek Oslejsek
 * @author Natalia Bebjakova
 */
public class CurvatureSampling extends PointSamplingVisitorImpl {
    
    /**
     * Curvature algorithm used for the selection of the top X significant points.
     * 
     * @author Radek Oslejsek
     */
    public enum CurvatureAlg {
        MEAN,
        GAUSSIAN,
        MAX,
        MIN
    }

    private final CurvatureAlg curvatureAlg;

    private final List<MeshPoint> allVertices = new ArrayList<>();
    
    /**
     * Constructor.
     * 
     * @param cAlg Curvature strategy to be used for the selection of significant points.
     * @param max Maximal number of significant points
     * @throws IllegalArgumentException if the {@code curbatureVisitor} is missing
     */
    public CurvatureSampling(CurvatureAlg cAlg, int max) {
        super(max);
        this.curvatureAlg = cAlg;
    }
    
    @Override
    public void visitMeshFacet(MeshFacet facet) {
        if (facet != null) {
            synchronized(this) {
                if (facet.getVertex(0).getCurvature() == null) {
                    CurvatureServices.computeAndSet(facet, new CurvatureConfig());
                }
                allVertices.addAll(facet.getVertices());
            }
        }
    }
    
    @Override
    public List<MeshPoint> getSamples() {
        List<MeshPoint> ret = allVertices.stream()
                .sorted((MeshPoint mp1, MeshPoint mp2) -> {
                    Curvature c1 = mp1.getCurvature();
                    Curvature c2 = mp2.getCurvature();
                    return switch (curvatureAlg) {
                        case MEAN -> Double.compare(c1.mean(), c2.mean());
                        case GAUSSIAN -> Double.compare(c1.gaussian(), c2.gaussian());
                        case MAX -> Double.compare(c1.maxPrincipal(), c2.maxPrincipal());
                        case MIN -> Double.compare(c1.minPrincipal(), c2.minPrincipal());
                    };
                })
                .limit(getRequiredSamples())
                .collect(Collectors.toList());
        setRealSamples(ret.size());
        return ret;
    }

    @Override
    public boolean isBackedByOrigMesh() {
        return true;
    }

    public CurvatureAlg getCurvatureAlg() {
        return this.curvatureAlg;
    }
    
    @Override
    public String toString() {
        return this.curvatureAlg + " curvature sampling. " + super.toString();
    }
    
}
