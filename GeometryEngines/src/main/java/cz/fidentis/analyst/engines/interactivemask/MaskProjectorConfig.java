package cz.fidentis.analyst.engines.interactivemask;

import javax.vecmath.Vector3d;

/**
 * Configuration for MaskProjector
 *
 * @param canvasWidth
 * @param canvasHeight
 * @param panelWidth
 * @param panelHeight
 * @param fov
 * @param cameraPosition
 * @param cameraCenter
 * @param cameraUpDirection
 * @param bboxScale
 * @param scaleFactor
 * @author Mario Chromik
 */
public record MaskProjectorConfig(int canvasWidth, int canvasHeight, int panelWidth,
                                  int panelHeight, int fov, Vector3d cameraPosition,
                                  Vector3d cameraCenter, Vector3d cameraUpDirection, double bboxScale,
                                  double scaleFactor) {

}
