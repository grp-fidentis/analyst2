package cz.fidentis.analyst.engines.sampling.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.sampling.PointSamplingVisitor;

import java.util.List;

/**
 * An abstract class for point sampling methods.
 *
 * @author Radek Oslejsek
 */
public abstract class PointSamplingVisitorImpl implements PointSamplingVisitor {

    /**
     * Real number of samples produced by the last call of {@link #getSamples()} method.
     */
    private int realSamples = 0;

    private int requiredSamples;

    /**
     * Constructor.
     *
     * @param max Maximal number of required samples. Must be bigger than zero
     * @throws IllegalArgumentException if the input parameter is wrong
     */
    protected PointSamplingVisitorImpl(int max) {
        if (max <= 0) {
            throw new IllegalArgumentException("max");
        }
        this.requiredSamples = max;
    }

    @Override
    public abstract void visitMeshFacet(MeshFacet facet);

    @Override
    public abstract List<MeshPoint> getSamples();

    @Override
    public abstract boolean isBackedByOrigMesh();

    @Override
    public void setRequiredSamples(int max) {
        if (max <= 0) {
            throw new IllegalArgumentException("max");
        }
        this.requiredSamples = max;
    }

    /**
     * Returns the number of required samples.
     * @return the number of required samples
     */
    public int getRequiredSamples() {
        return requiredSamples;
    }

    @Override
    public String toString() {
        return "Required samples: " + requiredSamples + ". Real number of last calculated samples: " + realSamples;
    }

    protected void setRealSamples(int realSamples) {
        this.realSamples = realSamples;
    }
}
