package cz.fidentis.analyst.engines.point2surface;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Stateless services for point-to-mesh distance measurement.
 * It is a wrapper to the stateful implementation of {@link cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface PointToSurfaceDistanceServices {

    /**
     * Returns the distance from the 3D point and the meshes stored in th k-D trees.
     *
     * @param kdTrees Meshes to be checked
     * @param config Configuration of the service
     * @return the distance from a 3D point
     */
    static double measureDistance(Collection<KdTree> kdTrees, PointToSurfaceDistanceConfig config) {
        var visitor = config.getVisitor();
        kdTrees.forEach(kdTree -> kdTree.accept(visitor));
        return visitor.getDistance();
    }

    /**
     * Returns the distance from the 3D point and the mesh stored in th k-D tree.
     *
     * @param kdTree Mesh to be checked
     * @param config Configuration of the service
     * @return the distance from a 3D
     */
    static double measureDistance(KdTree kdTree, PointToSurfaceDistanceConfig config) {
        return measureDistance(Collections.singleton(kdTree), config);
    }

    /**
     * Returns the closest point from the meshes stored in the k-D trees and the 3D point.
     *
     * @param kdTrees Meshes to be checked
     * @param config Configuration of the service
     * @return the closest points from visited meshes to the 3D point
     */
    static Map<MeshFacet, List<MeshPoint>> findNearestPoints(Collection<KdTree> kdTrees, PointToSurfaceDistanceConfig config) {
        var visitor = config.getVisitor();
        kdTrees.forEach(kdTree -> kdTree.accept(visitor));
        return visitor.getNearestPoints();
    }

    /**
     * Returns the closest point from the mesh stored in the k-D tree and the 3D point.
     *
     * @param kdTree Mesh to be checked
     * @param config Configuration of the service
     * @return the closest points from visited meshes to the 3D point
     */
    static Map<MeshFacet, List<MeshPoint>> findNearestPoints(KdTree kdTree, PointToSurfaceDistanceConfig config) {
        return findNearestPoints(Collections.singleton(kdTree), config);
    }

    /**
     * Returns the distance and the closest points
     *
     * @param kdTrees Meshes to be checked
     * @param config Configuration of the service
     * @return the distance and closest points
     */
    static Pair<Double, Map<MeshFacet, List<MeshPoint>>> measure(Collection<KdTree> kdTrees, PointToSurfaceDistanceConfig config) {
        var visitor = config.getVisitor();
        kdTrees.forEach(kdTree -> kdTree.accept(visitor));
        return Pair.of(visitor.getDistance(), visitor.getNearestPoints());
    }

    /**
     * Returns the distance and the closest points
     *
     * @param kdTree Mesh to be checked
     * @param config Configuration of the service
     * @return the distance and closest points
     */
    static Pair<Double, Map<MeshFacet, List<MeshPoint>>> measure(KdTree kdTree, PointToSurfaceDistanceConfig config) {
        return  measure(Collections.singleton(kdTree), config);
    }
}
