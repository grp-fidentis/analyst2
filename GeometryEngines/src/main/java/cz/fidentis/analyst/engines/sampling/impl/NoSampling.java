package cz.fidentis.analyst.engines.sampling.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * No point sampling. All mesh points are returned.
 * 
 * @author Radek Oslejsek
 */
public class NoSampling extends PointSamplingVisitorImpl {
    
    private final List<MeshPoint> allVertices = new ArrayList<>();

    /**
     * Constructor.
     *
     * @throws IllegalArgumentException if the input parameter is wrong
     */
    public NoSampling() {
        super(Integer.MAX_VALUE);
    }

    @Override
    public void visitMeshFacet(MeshFacet facet) {
        if (facet != null) {
            allVertices.addAll(facet.getVertices());
        }
    }
    
    @Override
    public List<MeshPoint> getSamples() {
        setRealSamples(allVertices.size());
        return Collections.unmodifiableList(allVertices);
    }

    @Override
    public boolean isBackedByOrigMesh() {
        return true;
    }

    @Override
    public String toString() {
        return "No sampling. " + super.toString();
    }

    
}
