package cz.fidentis.analyst.engines.sampling.impl;

import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Similar downsampling strategy to the {@link UniformSpaceSampling}.
 * However, a random vertex from the mesh is selected from each non-empty cell instead 
 * of computing a new average point. Therefore, the samples lie on the mesh surface.
 * 
 * @author Radek Oslejsek
 */
public class UniformSurfaceSampling extends UniformSpaceSampling {
    
    /**
     * Constructor.
     * 
     * @param max Required number of samples. Must be bigger than zero
     * @throws IllegalArgumentException if the input parameter is wrong
     */
    public UniformSurfaceSampling(int max) {
        super(max);
    }
    
    @Override
    public boolean isBackedByOrigMesh() {
        return true;
    }
    
    @Override
    public List<MeshPoint> getSamples() {
        // no sub-sampling:
        if (getOrigPoints().size() <= getRequiredSamples()) {
            setRealSamples(getOrigPoints().size());
            return Collections.unmodifiableList(getOrigPoints());
        }

        // Get a random vertex of the cell:
        Random rand = new Random();
        List<MeshPoint> ret = createGrid().getNonEmptyCells().stream()
                .map(list -> list.get(rand.nextInt(list.size()))) 
                .collect(Collectors.toList());
        setRealSamples(ret.size());
        return ret;
    }
    
    @Override
    public String toString() {
        return "Uniform surface sampling. " + super.toString();
    }
}
