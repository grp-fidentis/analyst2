package cz.fidentis.analyst.engines.symmetry;

import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.symmetry.impl.SymmetryVisitorMesh;
import cz.fidentis.analyst.engines.symmetry.impl.SymmetryVisitorPointCloud;

/**
 * Symmetry estimation strategies and their configuration.
 *
 * @author Radek Oslejsek
 *
 * @param method Symmetry estimation method
 * @param priSampling Primary sampling strategy used for finding candidate planes
 * @param maxSamplesForCandidatePruning Secondary subsampling used for finding the best candidate (pruning of candidates).
 *                                      <b>Uniform 3D grid is always used for this purpose.</b>
 */
public record SymmetryConfig(Method method, PointSamplingConfig priSampling, int maxSamplesForCandidatePruning) {

    /**
     * Simplified constructor for the same sampling strategies or for symmetry methods with only one sampling strategy.
     *
     * @param method Symmetry estimation method
     * @param strategy Sub-sampling strategy
     */
    public SymmetryConfig(Method method, PointSamplingConfig strategy) {
        this(method, strategy, 0);
    }

    /**
     * Instantiates and returns a symmetry visitor.
     *
     * @return a symmetry visitor
     */
    public SymmetryVisitor getVisitor() {
        return switch (method) {
            case ROBUST_POINT_CLOUD -> new SymmetryVisitorPointCloud(priSampling, maxSamplesForCandidatePruning);
            case ROBUST_MESH -> new SymmetryVisitorMesh(priSampling, maxSamplesForCandidatePruning);
        };
    }

    /**
     * Symmetry method.
     *
     * @author Radek Oslejsek
     */
    public enum Method {
        ROBUST_POINT_CLOUD,
        ROBUST_MESH
    }
}
