package cz.fidentis.analyst.engines.sampling.impl;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.engines.bbox.BoundingBoxConfig;
import cz.fidentis.analyst.engines.bbox.BoundingBoxServices;
import cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceConfig;
import cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceServices;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionServices;

import javax.vecmath.Matrix3d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This downsampling algorithm evenly distributes samples over the mesh surface
 * while preserving at least a minimal distance d between samples. It generates
 * 2D plane with poisson samples, iteratively project poisson samples onto the
 * mesh from different angles and filter valid projected poisson samples.
 * The minimal distance is approximated from required number of samples and number
 * of samples is on average +- 5% required number.
 *
 * @author Karol Kovac
 */
public class PoissonDiskSubSampling extends PointSamplingVisitorImpl {

    /**
     * Coefficients of the linear model used to approximate minimal sample distance
     * from knowing the number of required samples.
     */
    private static final double[] APPROXIMATION_COEFS = {1.910653, 22630.019925};
    private double minimalDistance; // minimal distance allowed between two samples
    private final Random rnd = new Random();
    protected final List<MeshFacet> facets = new ArrayList<>();
    protected final MeshTriangle.Smoothing smoothing;

    // ################### PROJECTION SETTINGS ###################
    // maximal angle between projector normal and z-axis, defining range for rotations
    public static final double PROJECTOR_MAX_ANGLE = 90;
    // minimal rotation applied to projector each iteration
    public static final double PROJECTOR_MIN_ROTATION = 10;
    public static final int MIN_PROJECTION_ITERATIONS = 20;

    // ########## 2D POISSON SAMPLES GENERATION SETTINGS ##########
    public static final int POISSON_MAX_ITERATIONS = 30;

    /**
     * Constructor for the maximum number of samples. The density (minimal distance between samples)
     * is approximated so that the mean absolut percentage error (MAPE) of this approximation is ~ 5.27%, and the median
     * absolute percentage error (MDAPE) is ~ 4.52%.
     *
     * @param max       Required number of samples. Must be bigger than zero
     * @param smoothing Surface samples can be equipped by interpolated normal and or adapted position to
     *                  interpolate surface curvature.
     *                  Use {@code Smoothing.NONE} to simply get samples from triangle planes without normal vectors.
     * @throws IllegalArgumentException if the input parameter is wrong
     */
    public PoissonDiskSubSampling(int max, MeshTriangle.Smoothing smoothing) {
        super(max);
        this.smoothing = smoothing;
        this.minimalDistance = Math.sqrt(APPROXIMATION_COEFS[1] / (getRequiredSamples() - APPROXIMATION_COEFS[0]));
    }

    /**
     * Constructor for required sample density.
     * *
     *
     * @param minimalDistance Minimal distance (i.e., the density of samples). Must be bigger than zero
     * @param smoothing       Surface samples can be equipped by interpolated normal and or adapted position to
     *                        interpolate surface curvature.
     *                        Use {@code Smoothing.NONE} to simply get samples from triangle planes without normal vectors.
     * @throws IllegalArgumentException if the input parameters are wrong
     */
    public PoissonDiskSubSampling(double minimalDistance, MeshTriangle.Smoothing smoothing) {
        super(distanceToNumSamples(minimalDistance));
        if (minimalDistance <= 0.0) {
            throw new IllegalArgumentException("minimalDistance");
        }
        this.smoothing = smoothing;
        this.minimalDistance = minimalDistance;
    }

    @Override
    public void setRequiredSamples(int max) {
        super.setRequiredSamples(max);
        this.minimalDistance = Math.sqrt(APPROXIMATION_COEFS[1] / (getRequiredSamples() - APPROXIMATION_COEFS[0]));
    }

    /**
     * Estimates minimal distance from the given number of samples.
     *
     * @param minimalDistance Minimal distance
     * @return Approximate number of samples
     */
    public static int distanceToNumSamples(double minimalDistance) {
        return (int) (minimalDistance * minimalDistance / APPROXIMATION_COEFS[1] + APPROXIMATION_COEFS[0]);
    }

    @Override
    public boolean isBackedByOrigMesh() {
        return false;
    }

    @Override
    public void visitMeshFacet(MeshFacet facet) {
        if (facet != null) {
            this.facets.add(facet);
        }
    }

    @Override
    public List<MeshPoint> getSamples() {
        Octree octree = Octree.create(this.facets);  // for rays intersections
        Box bbox = BoundingBoxServices.compute(this.facets, new BoundingBoxConfig());
        Point3d meshCentroid = bbox.midPoint();
        double projectorSize = bbox.diagonalLength();

        int iterationCount = 1;
        double currentProjectorAngleX = 0;
        double currentProjectorAngleY = 0;
        double newProjectorAngleX;
        double newProjectorAngleY;
        Set<MeshPoint> intersections;
        Set<MeshPoint> validSamples;

        // generate 2D poisson samples
        Point2d[][] grid = generatePoissonSamples(projectorSize);

        // build projector, place 2D poisson samples in space
        List<Point3d> projector = createProjectorFromGrid(grid, meshCentroid, projectorSize,
                bbox.diagonalLength() / 2.0);

        // first initial projection without projector rotation
        Set<MeshPoint> samples = projectorRaysOctreeIntersections(projector, octree, new Vector3d(0.0, 0.0, -1.0));


        // build kd-tree, form initial projection intersections
        KdTree kdTree = KdTree.create(samples);

        // iteratively, rotate projector, cast rays, merge intersection
        do {
            // pick random projector angle within range
            do {
                newProjectorAngleX = PROJECTOR_MAX_ANGLE * (Math.random() - 0.5);
                newProjectorAngleY = PROJECTOR_MAX_ANGLE * (Math.random() - 0.5);
            } while (Math.abs(newProjectorAngleX - currentProjectorAngleX) < PROJECTOR_MIN_ROTATION &&
                    Math.abs(newProjectorAngleY - currentProjectorAngleY) < PROJECTOR_MIN_ROTATION);
            // ensure rotation isn't too small to increase chance of generating valid samples

            // rotate projector
            rotateProjectorPlane(projector, meshCentroid, newProjectorAngleX - currentProjectorAngleX,
                    newProjectorAngleY - currentProjectorAngleY);

            // calculate rays direction
            Point3d projectorCentroid = projector.getFirst();
            Vector3d direction = new Vector3d(meshCentroid.x - projectorCentroid.x,
                    meshCentroid.y - projectorCentroid.y,
                    meshCentroid.z - projectorCentroid.z);

            // cast rays and filter intersections
            intersections = projectorRaysOctreeIntersections(projector, octree, direction);
            validSamples = filterRaysIntersections(kdTree, intersections);

            // update structures
            iterationCount += 1;
            currentProjectorAngleX = newProjectorAngleX;
            currentProjectorAngleY = newProjectorAngleY;
            samples.addAll(validSamples);
            addSamplesToKdTree(validSamples, kdTree);

        } while (iterationCount <= MIN_PROJECTION_ITERATIONS || !validSamples.isEmpty());
        setRealSamples(samples.size());
        return new ArrayList<>(samples);
    }

    @Override
    public String toString() {
        return "Poisson disk sub-sampling. " + super.toString();
    }

    /**
     * Adds samples to the KdTree
     *
     * @param samples samples to add
     * @param kdTree  tree to which samples are added
     */
    protected void addSamplesToKdTree(Set<MeshPoint> samples, KdTree kdTree) {
        for (MeshPoint sample : samples) {
            kdTree.addNode(sample);
        }
    }

    /**
     * Filters intersections not breaking minimal distance rule.
     *
     * @param kdTree        tree with samples
     * @param intersections sample candidates
     * @return (valid) sample candidates
     */
    protected Set<MeshPoint> filterRaysIntersections(KdTree kdTree, Set<MeshPoint> intersections) {
        return intersections.parallelStream()
                .filter(sample -> this.minimalDistance < PointToSurfaceDistanceServices.measureDistance(
                        kdTree,
                        new PointToSurfaceDistanceConfig(
                                PointToSurfaceDistanceConfig.Method.POINT_TO_VERTICES,
                                sample.getPosition(),
                                false)))
                .collect(Collectors.toSet());
    }

    /**
     * Casts rays from projector calculate intersections with interpolated normals.
     *
     * @param projectorSamples rays starting points
     * @param meshOctree       mesh to check for intersections
     * @param direction        rays direction
     * @return rays intersections with the mesh
     */
    private Set<MeshPoint> projectorRaysOctreeIntersections(List<Point3d> projectorSamples, Octree meshOctree, Vector3d direction) {
        return projectorSamples.parallelStream()
                .map(origin -> RayIntersectionServices.computeClosest(
                        meshOctree,
                        new RayIntersectionConfig(new Ray(origin, direction), smoothing, false)))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    /**
     * Rotates projector plane around mesh centroid in x,y-axis by given angles.
     *
     * @param projector samples in projector plane
     * @param thetaX    angle to rotate around x-axis
     * @param thetaY    angle to rotate around y-axis
     * @param centroid  mesh centroid
     */
    protected void rotateProjectorPlane(List<Point3d> projector, Point3d centroid, double thetaX, double thetaY) {
        // build 2 matrices, rotation around x,y-axis and combine them
        Matrix3d mat1 = new Matrix3d();
        Matrix3d mat2 = new Matrix3d();
        mat1.rotX(Math.toRadians(thetaX));
        mat2.rotY(Math.toRadians(thetaY));
        mat1.mul(mat2);

        projector.parallelStream().forEach(sample -> {
            sample.sub(centroid);       // translate
            mat1.transform(sample);     // rotate
            sample.add(centroid);       // translate back
        });
    }

    /**
     * Creates projector plane (parallel to XY plane) from grid .
     *
     * @param grid   grid with poisson samples
     * @param radius radius of bounding sphere
     * @return projector plane samples, first point is plane centroid not a poisson sample
     */
    protected List<Point3d> createProjectorFromGrid(Point2d[][] grid, Point3d meshCentroid, double projectorSize,
                                                    double radius) {
        List<Point3d> projectorSamples = new ArrayList<>();
        Point3d planeCentroid = new Point3d(meshCentroid.x, meshCentroid.y, meshCentroid.z + radius);
        projectorSamples.addFirst(planeCentroid); // first point is plane centroid, do not cast rays from this point
        Vector3d toShift = new Vector3d(meshCentroid.x - projectorSize / 2,
                meshCentroid.y - projectorSize / 2,
                radius);
        for (Point2d[] row : grid) {
            for (Point2d sample : row) {
                if (sample != null) {
                    Point3d sample3D = new Point3d(sample.x, sample.y, meshCentroid.z);
                    sample3D.add(toShift);
                    projectorSamples.add(sample3D);
                }
            }
        }
        return projectorSamples;
    }

    /**
     * Generate 2D Poisson-disk sample gird
     *
     * @param minPlaneSize minimal side length of sampled plane
     * @return gird with generated points
     */
    protected Point2d[][] generatePoissonSamples(double minPlaneSize) {
        double a = this.minimalDistance / Math.sqrt(2); // base grid cell size
        double minPlaneDiagonal = Math.sqrt(2 * Math.pow(minPlaneSize, 2));
        int gridSize = (int) Math.ceil(minPlaneDiagonal / this.minimalDistance);

        Point2d[][] baseGrid = new Point2d[gridSize][gridSize];
        ActiveNodeList activeNodeList = new ActiveNodeList(gridSize);

        int iterationCount = 0;
        while (!activeNodeList.isEmpty() || iterationCount == POISSON_MAX_ITERATIONS) {

            // dart throwing
            int numberOfThrows = activeNodeList.size();
            for (int i = 0; i < numberOfThrows; i++) {

                // pick a node (uncovered region)
                int index = rnd.nextInt(activeNodeList.size());
                ActiveNode inspectedNode = activeNodeList.get(index);

                // check parent in baseGrid for existing point
                int parentX = inspectedNode.getParentX();
                int parentY = inspectedNode.getParentY();
                if (baseGrid[parentX][parentY] != null) {
                    continue;
                }

                // pick a random point from region defined by active node
                double k = rnd.nextDouble();
                double l = rnd.nextDouble();
                double side = inspectedNode.getSideLength(a);
                Point2d pointCandidate = new Point2d(side * inspectedNode.x + side * k,
                        side * inspectedNode.y + side * l);

                // check for existing point in neighborhood 5x5
                if (isBreakingMinDistanceRule(pointCandidate, baseGrid, parentX, parentY, minimalDistance)) {
                    continue;
                }

                // point is valid, update structures
                baseGrid[parentX][parentY] = pointCandidate;
                activeNodeList.remove(index);
            }

            // end of dart throwing iteration, active list subdivision
            activeNodeList = new ActiveNodeList(activeNodeList, baseGrid, minimalDistance, a);
            iterationCount += 1;
        }

        return baseGrid;
    }

    /**
     * Search for samples in grid in 5x5 neighbourhood.
     *
     * @param grid base grid
     * @param m    x cord of 5x5 neighbourhood center
     * @param n    y cord of 5x5 neighbourhood center
     * @return list of samples
     */
    private List<Point2d> getGridSamplesIn5x5(Point2d[][] grid, int m, int n) {
        List<Point2d> samples = new ArrayList<>();

        for (int u = -2; u <= 2; u++) {
            for (int v = -2; v <= 2; v++) {
                int x = m + u;
                int y = n + v;

                if (0 <= x && x < grid[0].length && 0 <= y && y < grid[0].length && grid[x][y] != null) { // valid cords check
                    samples.add(grid[x][y]);
                }
            }
        }
        return samples;
    }

    /**
     * Checks if node`s area is fully covered by disk of already existing sample.
     *
     * @param grid base grid
     * @param node tested node
     * @return true if no other sample can be generated in the node
     */
    private boolean isNodeCoveredByDiskOfSomeSample(ActiveNode node, Point2d[][] grid, double d, double a) {
        // samples outside 5x5 neighbourhood cannot cause collision
        return null != getGridSamplesIn5x5(grid, node.getParentX(), node.getParentY()).stream()
                .filter(sample -> node.isCoveredByDisk(sample, d, a))
                .findAny()
                .orElse(null);
    }

    /**
     * Checks if a point is breaking minimal distance rule.
     *
     * @param point examined point
     * @param grid  base grid
     * @param m     x cord of point in gridgene
     * @param n     y cord of point in grid
     * @return true if breaking minimal distance rule.
     */
    private boolean isBreakingMinDistanceRule(Point2d point, Point2d[][] grid, int m, int n, double d) {
        return null != getGridSamplesIn5x5(grid, m, n).stream()
                .filter(sample -> sample.distance(point) < d)
                .findAny()
                .orElse(null);
    }

    /**
     * Represents uncovered region of plane, octree node
     *
     * @author Karol Kovac
     */
    public static class ActiveNode {
        private final int x;
        private final int y;
        private final int depth;

        /**
         * Creates new active node.
         *
         * @param x     x coordinate
         * @param y     y coordinate
         * @param depth defining size of region represented by node
         */
        public ActiveNode(int x, int y, int depth) {
            this.x = x;
            this.y = y;
            this.depth = depth;
        }

        /**
         * Calculates X cord of parent node from base grid (with depth 0).
         *
         * @return X cord of parent in a base grid
         */
        public int getParentX() {
            return (int) (this.x / Math.pow(2, depth));
        }

        /**
         * Calculates Y cord of parent node from base grid (with depth 0).
         *
         * @return Y cord of parent in a base grid
         */
        public int getParentY() {
            return (int) (this.y / Math.pow(2, depth));
        }

        /**
         * Calculates corner points of area represented by node
         *
         * @return corner points [(minX, minY),(minX, maxY),(maxX, minY),(maxX, maxY)]
         */
        public Point2d[] getCornerPoints(double a) {
            double nodeLength = a / Math.pow(2, depth);
            return new Point2d[]{new Point2d(this.x * nodeLength, this.y * nodeLength),
                    new Point2d(this.x * nodeLength, (this.y + 1) * nodeLength),
                    new Point2d((this.x + 1) * nodeLength, this.y * nodeLength),
                    new Point2d((this.x + 1) * nodeLength, (this.y + 1) * nodeLength)};
        }

        /**
         * Creates 4 children nodes in depth +1, each covering 1/4 of this node
         *
         * @return array of children nodes
         */
        public ActiveNode[] getChildren() {
            return new ActiveNode[]{new ActiveNode(this.x * 2, this.y * 2, this.depth + 1),
                    new ActiveNode(this.x * 2, this.y * 2 + 1, this.depth + 1),
                    new ActiveNode(this.x * 2 + 1, this.y * 2, this.depth + 1),
                    new ActiveNode(this.x * 2 + 1, this.y * 2 + 1, this.depth + 1)};
        }

        /**
         * Calculates node side length
         *
         * @return side length
         */
        public double getSideLength(double a) {
            return a / Math.pow(2, this.depth);
        }

        /**
         * Checks if area represented by node is fully covered by disk of a given point
         *
         * @param point point to check
         * @return true if fully covered
         */
        public boolean isCoveredByDisk(Point2d point, double d, double a) {
            for (Point2d corner : this.getCornerPoints(a)) {
                if (corner.distance(point) > d) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * We need to get and remove random elements (active nodes) form the list. Using
     * {@code ArrayList} or {@code LinkedList} us suboptimal for this task.
     * Theregore, this implementation used {@code ArrayList} to fill the structure and
     * get random elements efficiently, but removing element is optimized by replacing it with the very last element
     * of the list and shortening the list length.
     *
     * @author Radek Oslejsek
     */
    private class ActiveNodeList {
        private final List<ActiveNode> nodes;
        private int size;

        /**
         * Creates an empty list with {@code gridSize}x{@code gridSize} elements.
         *
         * @param gridSize grid size
         */
        ActiveNodeList(int gridSize) {
            this.size = gridSize * gridSize;
            this.nodes = new ArrayList<>(size);
            for (int x = 0; x < gridSize; x++) {
                for (int y = 0; y < gridSize; y++) {
                    nodes.add(new ActiveNode(x, y, 0));
                }
            }
        }

        /**
         * Creates a list that subdivides all active nodes of the original list and removes child nodes that are covered
         * by the disk of some sample (where new sample cannot be fitted)
         *
         * @param oldActiveList old active list
         * @param grid          base grid
         * @param minDist       minimal distance
         * @param cellSize      base grid cell size
         */
        ActiveNodeList(ActiveNodeList oldActiveList, Point2d[][] grid, double minDist, double cellSize) {
            this.nodes = new ArrayList<>();
            for (int i = 0; i < oldActiveList.size(); i++) {
                ActiveNode node = oldActiveList.get(i);
                for (ActiveNode child : node.getChildren()) {
                    if (!isNodeCoveredByDiskOfSomeSample(child, grid, minDist, cellSize)) {
                        nodes.add(child);
                    }
                }
            }
        }

        /**
         * Returns active noded.
         *
         * @param index Index of the node
         * @return active node at the index
         */
        public ActiveNode get(int index) {
            return nodes.get(index);
        }

        /**
         * Removes active node.
         *
         * @param index Index of the node
         */
        public void remove(int index) {
            nodes.set(index, nodes.get(size - 1)); // copy last item to the place to be removed
            size--; // "remove" the last place
        }

        /**
         * Returns the size of the list.
         *
         * @return the size of the list
         */
        public int size() {
            return size;
        }

        /**
         * Return {@code true} if the list is empty
         *
         * @return {@code true} if the list is empty
         */
        public boolean isEmpty() {
            return size == 0;
        }
    }
}
