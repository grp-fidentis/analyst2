package cz.fidentis.analyst.engines.symmetry.impl;

import cz.fidentis.analyst.data.grid.UniformGrid3d;
import cz.fidentis.analyst.data.grid.UniformGrid4d;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingVisitor;
import cz.fidentis.analyst.engines.sampling.impl.UniformSpaceSampling;
import cz.fidentis.analyst.engines.symmetry.SymmetryVisitor;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A robust implementation of symmetry plane estimation.
 * The code is based on the
 * <a href="https://link.springer.com/article/10.1007/s00371-020-02034-w">...</a> paper.
 * This estimator <b>works with point clouds</b> (does not require manifold triangle mesh).
 * It has the following properties:
 * <ul>
 * <li>The Wendland's similarity functions is used to get the best candidate planes.</li>
 * <li>No additional weights are used.</li>
 * <li>The computation is accelerated by using a Uniform Grid for pruning candidate planes
 * and using two-level subsampling: radical subsampling for the generation
 * of candidate planes (cca. 100 points seems to be optimal) and less radical (cca. 1000 points)
 * subsampling for the selection of the best candidate(s).</li>
 * <li>Best results are achieved with Uniform Grid sampling with 100 and 1000
 * points (for the two phases).</li>
 * </ul>
 *
 * @author Radek Oslejsek
 */
public class SymmetryVisitorPointCloud extends SymmetryVisitor {

    private final PointSamplingVisitor samplingVisitor1;
    private final PointSamplingVisitor samplingVisitor2;

    private CandidatePlaneRobust symmetryPlane;


    /**
     * Constructor.
     *
     * @param samplingStrategy Subsampling strategy for finding candidate planes. Must not be {@code null}
     *                         Use {@code NoSampling} strategy to avoid subsampling.
     * @param maxSamplesForCandidatePruning Secondary subsampling used for finding the best candidate
     *                                      (pruning of candidates) uses always the <b>Uniform 3D grid.</b>
     *                                      This parameter sets the strength of this subsampling.
     */
    public SymmetryVisitorPointCloud(PointSamplingConfig samplingStrategy, int maxSamplesForCandidatePruning) {
        this.samplingVisitor1 = Objects.requireNonNull(samplingStrategy.getVisitor());
        this.samplingVisitor2 = new UniformSpaceSampling(maxSamplesForCandidatePruning);
    }

    @Override
    public Plane getSymmetryPlane() {
        if (symmetryPlane == null) {
            this.calculateSymmetryPlane();
        }
        return (symmetryPlane == null) ? null : symmetryPlane.getPlane();
    }

    @Override
    public void visitMeshFacet(MeshFacet facet) {
        samplingVisitor1.visitMeshFacet(facet);
        samplingVisitor2.visitMeshFacet(facet);
    }

    @Override
    public void dispose() {
        samplingVisitor1.dispose();
        samplingVisitor2.dispose();
    }

    /**
     * Calculates the symmetry plane.
     */
    protected void calculateSymmetryPlane() {
        //
        // Phase 1: Sub-sample the mesh, transform it so that the centroid is
        //          in the space origin, and then compute candidate planes
        //
        List<MeshPoint> meshSamples = samplingVisitor1.getSamples();
        Point3d origCentroid = MeshFactory.createAverageMeshPoint(meshSamples).getPosition();
        Set<CandidatePlaneRobust> candidates = generateCandidates(meshSamples, origCentroid);

        //
        // Phase 2: Down-sample the mesh again (usually to more samples than before),
        //          transform them in the same way as before, and measure their symmetry
        //          with respect to individual candidate planes.
        //
        meshSamples = samplingVisitor2.getSamples();
        measureSymmetry(meshSamples, origCentroid, candidates);
        //candidates.forEach(c -> System.out.println(c.getPlane()));

        //
        // Phase 3: "Adjust" the best 5 candidate planes so that they really 
        //          represent the local maxima using 
        //          the quasi-Newton optimization method L-BFGS
        // 
        // TO DO: candidates.stream().sorted().limit(5).forEach(optimize);

        //
        // Phase 4: Finally, get the best plane and move it back
        // 
        this.symmetryPlane = candidates.stream()
                .sorted()
                .findFirst()
                .orElse(null);

        if (this.symmetryPlane != null) {
            Point3d planePoint = this.symmetryPlane.getPlane().getPlanePoint();
            planePoint.add(origCentroid);
            Vector3d normal = this.symmetryPlane.getNormal();
            double dist = ((normal.x * planePoint.x) + (normal.y * planePoint.y) + (normal.z * planePoint.z))
                    / Math.sqrt(normal.dot(normal)); // distance of transformed surface point in the plane's normal direction
            this.symmetryPlane = new CandidatePlaneRobust(new Plane(normal, dist));
        }
    }

    /**
     * Copies mesh samples, moves them to the space origin, and then computes candidate planes.
     *
     * @param meshSamples Subsampled mesh
     * @param centroid    Centroid of the subsampled mesh
     * @return Candidate planes
     */
    protected Set<CandidatePlaneRobust> generateCandidates(List<MeshPoint> meshSamples, Point3d centroid) {
        ProcessedCloud cloud = new ProcessedCloud(meshSamples, centroid);
        UniformGrid4d<CandidatePlaneRobust> planesCache = UniformGrid4d.create(CandidatePlaneRobust.GRID_SIZE);

        for (int i = 0; i < cloud.points.size(); i++) {
            for (int j = i; j < cloud.points.size(); j++) { // from i !!!
                if (i == j) {
                    continue;
                }
                CandidatePlaneRobust candPlane = new CandidatePlaneRobust(
                        cloud.points.get(i).getPosition(),
                        cloud.points.get(j).getPosition(),
                        cloud.avgDistance);
                CandidatePlaneRobust closestPlane = candPlane.getClosestPlane(planesCache);
                if (closestPlane == null) { // add
                    planesCache.store(candPlane.getEstimationVector(), candPlane);
                } else { // replace with averaged plane 
                    CandidatePlaneRobust avgPlane = new CandidatePlaneRobust(closestPlane, candPlane);
                    planesCache.remove(closestPlane.getEstimationVector(), closestPlane);
                    planesCache.store(avgPlane.getEstimationVector(), avgPlane);
                }
            }
        }

        return planesCache.getAll().stream()
                .filter(plane -> plane.getNumAverages() >= 4)
                .collect(Collectors.toSet());
    }

    /**
     * Copies mesh samples, moves them to the space origin, and then measures the quality of
     * candidate planes. The results are stored in the candidate planes.
     *
     * @param meshSamples Subsampled mesh
     * @param centroid    Centroid of the subsampled mesh
     * @param candidates  Candidate planes
     */
    protected void measureSymmetry(List<MeshPoint> meshSamples, Point3d centroid, Set<CandidatePlaneRobust> candidates) {
        ProcessedCloud cloud = new ProcessedCloud(meshSamples, centroid);
        double alpha = 15.0 / cloud.avgDistance;
        UniformGrid3d<MeshPoint> samplesGrid = UniformGrid3d.create(2.6 / alpha, cloud.points, MeshPoint::getPosition);
        candidates.parallelStream().forEach(plane -> plane.measureSymmetry(cloud.points, samplesGrid, alpha));
    }


    /********************************************************************
     * A helper class that copies input mesh point and moves them 
     * so that the given centroid is in the space origin.
     *
     * @author Radek Oslejsek
     */
    protected static class ProcessedCloud {

        private final List<MeshPoint> points;
        private double avgDistance;

        /**
         * Moves orig points so that the centroid is in the space origin, copies
         * them into a new list. Also, computes the average distance of orig
         * points to the centroid (= average distance of shifted points into the
         * space origin).
         *
         * @param centroid Centroid.
         * @param points   Original mesh point
         */
        ProcessedCloud(List<MeshPoint> points, Point3d centroid) {
            this.points = new ArrayList<>(points.size());
            for (int i = 0; i < points.size(); i++) {
                MeshPoint mp = points.get(i);
                Point3d p = new Point3d(mp.getPosition());
                p.sub(centroid);
                this.points.add(MeshFactory.createMeshPoint(p, mp.getNormal(), mp.getTexCoord(), mp.getCurvature()));
                avgDistance += Math.sqrt(p.x * p.x + p.y * p.y + p.z * p.z) / points.size();
            }
        }
    }
}
