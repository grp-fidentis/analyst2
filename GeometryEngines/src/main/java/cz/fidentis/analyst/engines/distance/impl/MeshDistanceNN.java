package cz.fidentis.analyst.engines.distance.impl;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.kdtree.KdTreeVisitor;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.measurement.RawFacetDistances;
import cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceConfig;
import cz.fidentis.analyst.engines.point2surface.PointToSurfaceDistanceVisitor;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This visitor searches for the closest points of the primary mesh (provided to the constructor)
 * and inspected meshes using nearest neighbor (NN) search , then provides these closest points, their distances, etc.
 * This class can be used for Hausdorff distance computation, for instance:
 * <pre>
 MeshDistanceNN dA = new MeshDistanceNN(faceA);
 MeshDistanceNN dB = new MeshDistanceNN(faceB);
 dA.visitMeshFacet(faceB);
 dB.visitMeshFacet(faceA);
 double hausdorffDist = Math.max(dA.getStats.getMax(), dB.getStats.getMax());
 </pre>
 * <p>
 * This visitor is instantiated with a single k-d tree (either given as the input
 * parameter, or automatically created from the triangular mesh). 
 * When applied to other mesh facets, it finds points from the k-d tree 
 * that are the closest to each point of the inspected mesh.
 * </p>
 * <p>
 * This visitor is thread-safe, i.e., a single instance of the visitor can be used 
 * to inspect multiple meshes simultaneously. 
 * </p>
 * <p>
 * The distance is computed either as absolute or relative. Absolute
 * represents Euclidean distance (all numbers are positive). On the contrary, 
 * relative distance considers orientation of the visited mesh (determined by its normal vectors)
 * and produces positive or negative distances depending on whether the primary
 * mesh is "in front of" or "behind" the given vertex.
 * </p>
 * 
 * @author Daniel Schramm
 * @author Radek Oslejsek
 */
public class MeshDistanceNN extends MeshDistanceVisitorImpl {
    
    private final boolean pointToPoint;
    
    private boolean crop;
    
    /**
     * The source triangular mesh (set of mesh facets) stored in k-d tree.
     */
    private final KdTree kdTree;
    
    /**
     * Constructor.
     * 
     * @param mainKdTree The KD tree to which distance from the visited facets is to be computed. Must not be {@code null}
     * @param pointToPoint If {@code true}, then point-to-point distance measurement is used. Otherwise, point-to-triangle is used
     * @param relativeDistance If true, then the visitor calculates the relative distances with respect 
     * to the normal vectors of source facets (normal vectors have to present), 
     * i.e., we can get negative distances.
     * @param parallel If {@code true}, then the algorithm runs concurrently utilizing all CPU cores
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *        taken into account. Parts (vertices) that are out of the surface of the primary face are ignored 
     *        (their distance is set to {@code NaN}). 
     *        This feature makes the distance computation more symmetric.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public MeshDistanceNN(KdTree mainKdTree, boolean pointToPoint, boolean relativeDistance, boolean parallel, boolean crop) {
        super(relativeDistance, parallel);
        this.pointToPoint = pointToPoint;
        if (mainKdTree == null) {
            throw new IllegalArgumentException("mainKdTree");
        }
        this.kdTree = mainKdTree;
        this.crop = crop;
    }
    
    /**
     * Enables to change auto-cropping feature at runtime
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the distance computation more symmetric.
     */
    public void setCrop(boolean crop) {
        this.crop = crop;
    }
    
    /**
     * Return the KD tree to which distances are computed.
     * 
     * @return KD tree to which distances are computed
     */
    public KdTree getMainKdTree() {
        return kdTree;
    }
    
    @Override
    public void visitMeshFacet(MeshFacet comparedFacet) {
        final List<Double> distList = new ArrayList<>();
        final List<MeshPoint> nearestPointsList = new ArrayList<>();
        final List<MeshPoint> vertices = comparedFacet.getVertices();

        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        final List<Future<KdTreeVisitor>> results = new ArrayList<>(vertices.size());

        for (var vert: vertices) { // for each vertex of comparedFacet
            KdTreeVisitor visitor = instantiateVisitor(vert.getPosition());
            if (inParallel()) { // fork and continue
                results.add(executor.submit(new CallableVisitor(visitor)));
            } else { // process distance results immediately
                this.kdTree.accept(visitor); // compute the distance for i-th vertex or prepare the visitor for computation
                updateResults((PointToSurfaceDistanceVisitor) visitor, vert, distList, nearestPointsList);
            }
        }
    
        if (inParallel()) { // process asynchronous computation of distance
            executor.shutdown();
            while (!executor.isTerminated()){}
            try {
                int i = 0;
                for (Future<KdTreeVisitor> res: results) {
                    final KdTreeVisitor visitor = res.get(); // waits until all computations are finished
                    updateResults((PointToSurfaceDistanceVisitor) visitor, vertices.get(i), distList, nearestPointsList);
                    i++;
                }
            } catch (final InterruptedException | ExecutionException ex) {
                Logger.getLogger(MeshDistanceNN.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        synchronized (this) {
            getDistancesOfVisitedFacets().addRawFacetMeasurement(new RawFacetDistances(comparedFacet, distList, nearestPointsList));
        }
    }
    
    protected KdTreeVisitor instantiateVisitor(Point3d inspectedPoint) {
        return (new PointToSurfaceDistanceConfig(
                pointToPoint
                        ? PointToSurfaceDistanceConfig.Method.POINT_TO_VERTICES
                        : PointToSurfaceDistanceConfig.Method.POINT_TO_SURFACE,
                inspectedPoint,
                crop)).getVisitor();
    }

    protected void updateResults(PointToSurfaceDistanceVisitor vis, MeshPoint point, List<Double> distList, List<MeshPoint> nearestPointsList) {
        MeshPoint closestV = getClosestMeshPoint(vis);
        if (closestV == null) { // unable to get a single closest point
            distList.add(Double.POSITIVE_INFINITY);
            nearestPointsList.add(null);
            return;
        }

        double dist = vis.getDistance();
        int sign = 1;

        if (relativeDistance()) { // compute sign for relative distance
            Vector3d aux = new Vector3d(closestV.getPosition());
            aux.sub(point.getPosition());
            sign = aux.dot(point.getNormal()) < 0 ? -1 : 1;
        }

        distList.add(sign * dist);
        nearestPointsList.add(closestV);
    }

    /**
     * @param vis visitor
     * @return the only one existing closest vertex or {@code null}
     */
    protected MeshPoint getClosestMeshPoint(PointToSurfaceDistanceVisitor vis) {
        if (!Double.isFinite(vis.getDistance())) {
            return null; 
        }
        
        if (vis.getNearestPoints().size() != 1) {
            return null; // something is wrong because there should be only my inspected facet
        }
        
        MeshFacet myInspectedFacet = vis.getNearestPoints().keySet().stream().findFirst().get();
        
        if (vis.getNearestPoints().get(myInspectedFacet).size() != 1) {
            return null; // multiple closest points; we don't know which one to choose
        }
        
        // return the only one closest vertex
        return vis.getNearestPoints().get(myInspectedFacet).get(0);
    }
    
    /**
     * Helper call for asynchronous invocation of visitors.
     * 
     * @author Radek Oslejsek
     */
    private class CallableVisitor implements Callable<KdTreeVisitor> {
        private final KdTreeVisitor vis;
        
        /**
         * Constructor.
         * @param vis visitor to be called asynchronously
         */
        CallableVisitor(KdTreeVisitor vis) {
            this.vis = vis;
        }
        
        @Override
        public KdTreeVisitor call() throws Exception {
            try {
                kdTree.accept(vis); 
                return vis;
            } catch(UnsupportedOperationException ex) {
                return null;
            }
        }
    }
}
