package cz.fidentis.analyst.engines.distance.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.octree.OctreeVisitor;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.data.mesh.measurement.RawFacetDistances;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This visitor searches for the closest points of the primary mesh (provided to the constructor)
 * and inspected meshes by casting rays (RC) in the direction of normal vectors on the inspected mesh.
 * <p>
 * This visitor is instantiated with a single octree (either given as the input
 * parameter, or automatically created from the triangular mesh). 
 * When applied to other mesh facets, it finds points from the octree 
 * that are the closest to each point of the inspected mesh.
 * </p>
 * <p>
 * This visitor is thread-safe, i.e., a single instance of the visitor can be used 
 * to inspect multiple meshes simultaneously. 
 * </p>
 * <p>
 * The distance is computed either as absolute or relative. Absolute
 * represents Euclidean distance (all numbers are positive). On the contrary, 
 * relative distance considers orientation of the visited mesh (determined by its normal vectors)
 * and produces positive or negative distances depending on whether the primary
 * mesh is "in front of" or "behind" the given vertex.
 * </p>
 * 
 * @author Radek Oslejsek
 */
public class MeshDistanceRC extends MeshDistanceVisitorImpl {

    private Octree octree;
    
    /**
     * Constructor.
     * 
     * @param mainOctree An octree to which distance from the visited facets is to be computed. Must not be {@code null}
     * @param relativeDistance If true, then the visitor calculates the relative distances with respect 
     * to the normal vectors of source facets (normal vectors have to present), 
     * i.e., we can get negative distances.
     * @param parallel If {@code true}, then the algorithm runs concurrently utilizing all CPU cores
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public MeshDistanceRC(Octree mainOctree, boolean relativeDistance, boolean parallel) {
        super(relativeDistance, parallel);
        if (mainOctree == null) {
            throw new IllegalArgumentException("mainOctree");
        }
        this.octree = mainOctree;
    }
    
    @Override
    public void visitMeshFacet(MeshFacet comparedFacet) {
        final List<Double> distList = new ArrayList<>();
        final List<MeshPoint> nearestPointsList = new ArrayList<>();
        final List<MeshPoint> vertices = comparedFacet.getVertices();
        
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        final List<Future<OctreeVisitor>> results = new ArrayList<>(vertices.size());

        for (var vert: vertices) { // for each vertex of comparedFacet
            RayIntersectionVisitor visitor =
                    (new RayIntersectionConfig(
                            new Ray(vert.getPosition(), vert.getNormal()),
                            MeshTriangle.Smoothing.NONE,
                            true // doesn't matter, important is the value on reading results
                    )).getVisitor();
            if (inParallel()) { // fork and continue
                results.add(executor.submit(new CallableVisitor(visitor)));
            } else { // process distance results immediately
                this.octree.accept(visitor); // compute the distance for i-th vertex or prepare the visitor for computation
                updateResults(visitor, distList, nearestPointsList);
            }
        }
    
        if (inParallel()) { // process asynchronous computation of distance
            executor.shutdown();
            while (!executor.isTerminated()){}
            try {
                for (Future<OctreeVisitor> res: results) {
                    final RayIntersectionVisitor visitor = (RayIntersectionVisitor) res.get(); // waits until all computations are finished
                    updateResults(visitor, distList, nearestPointsList);
                }
            } catch (final InterruptedException | ExecutionException ex) {
                Logger.getLogger(MeshDistanceNN.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        synchronized (this) {
            getDistancesOfVisitedFacets().addRawFacetMeasurement(new RawFacetDistances(comparedFacet, distList, nearestPointsList));
        }
    }
    
    protected void updateResults(RayIntersectionVisitor vis, List<Double> distList, List<MeshPoint> nearestPointsList) {
        RayIntersection ri = vis.getClosestIntersection(true);
        if (ri == null) {
            distList.add(Double.POSITIVE_INFINITY);
            nearestPointsList.add(null);
        } else {
            distList.add(relativeDistance() ? ri.getDistance() : Math.abs(ri.getDistance()));
            nearestPointsList.add(MeshFactory.createMeshPoint(ri.getPosition()));
        }
    }
    
    /**
     * Helper call for asynchronous invocation of visitors.
     * 
     * @author Radek Oslejsek
     */
    private class CallableVisitor implements Callable<OctreeVisitor> {
        private OctreeVisitor vis;
        
        /**
         * Constructor.
         * @param vis visitor to be called asynchronously
         */
        CallableVisitor(OctreeVisitor vis) {
            this.vis = vis;
        }
        
        @Override
        public OctreeVisitor call() throws Exception {
            try {
                octree.accept(vis); 
                return vis;
            } catch(UnsupportedOperationException ex) {
                return null;
            }
        }
    }
}
