package cz.fidentis.analyst.engines.symmetry.impl;

import cz.fidentis.analyst.data.mesh.Curvature;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Plane;

import javax.vecmath.Vector3d;
import java.util.List;

/**
 * Symmetry plane with votes used for the decision about the best symmetry estimates of 3D mesh models.
 *
 * @author Natalia Bebjakova
 * @author Radek Oslejsek
 *
 */
public class CandidatePlaneMesh extends CandidatePlane implements Comparable<CandidatePlaneMesh> {

    public static final double MIN_ANGLE_COS = 0.985;
    public static final double AVG_CURVATURE_MULTIPLICATOR = 0.01;

    private final int votes;

    /**
     * Constructor.
     *
     * @param vertices Mesh vertices
     * @param cache Precomputed values form mesh vertices
     * @param i index of the first significant point used for the plane construction
     * @param j index of the second significant point used for the plane construction
     * @param maxDist Distance limit
     * @throws UnsupportedOperationException if the symmetry plane cannot be created
     */
    public CandidatePlaneMesh(List<MeshPoint> vertices, SymmetryCache cache, int i, int j, double maxDist) {
        super(createNormalizedPlane(vertices, cache, i, j));
        if (i == j) {
            throw new UnsupportedOperationException();
        }
        this.votes = computeVotes(vertices, cache, maxDist);
    }

    /**
     * Constructor.
     * @param plane Original plane
     */
    public CandidatePlaneMesh(Plane plane) {
        super(plane);
        votes = 0;
    }

    @Override
    public int compareTo(CandidatePlaneMesh other) {
        return Integer.compare(votes, other.votes);
    }

    @Override
    public String toString() {
        return getPlane().getNormal() + " " + getPlane().getDistance() + " " + votes;
    }

    public int getVotes() {
        return votes;
    }

    /**
     *
     * @param vertices Vertices
     * @param cache Cache
     * @param i Index of the first point
     * @param j Index of the second point
     */
    private static Plane createNormalizedPlane(List<MeshPoint> vertices, SymmetryCache cache, int i, int j) {
        MeshPoint meshPointI = vertices.get(i);
        MeshPoint meshPointJ = vertices.get(j);

        // accept only points with similar Gaussian curvature
        if (!similarCurvature(meshPointI.getCurvature(), meshPointJ.getCurvature(), cache.getAvgGaussianCurvature() * AVG_CURVATURE_MULTIPLICATOR)) {
            throw new UnsupportedOperationException();
        }

        Vector3d planeNormal = new Vector3d(meshPointI.getPosition());
        planeNormal.sub(meshPointJ.getPosition());
        planeNormal.normalize();

        // accept only point pair with oposite normals along with the plane normal:
        double normCos = cache.getNormCosVec(i, j).dot(planeNormal);
        if (Math.abs(normCos) < MIN_ANGLE_COS) {
            throw new UnsupportedOperationException();
        }

        // create normalized plane:
        double normalLength = planeNormal.length();
        planeNormal.scale(1.0 / normalLength);
        return new Plane(planeNormal, planeNormal.dot(cache.getAvgPos(i, j)) / normalLength);
    }

    /**
     * Computes votes for given plane
     *
     * @param maxDist Distance limit
     */
    private int computeVotes(List<MeshPoint> vertices, SymmetryCache cache, double maxDist) {
        Vector3d normal = getPlane().getNormal();
        double d = getPlane().getDistance();
        int votes = 0;

        for (int i = 0; i < vertices.size(); i++) {
            for (int j = 0; j < vertices.size(); j++) {
                if (i == j) {
                    continue;
                }

                if (!similarCurvature(vertices.get(i).getCurvature(), vertices.get(j).getCurvature(), cache.getAvgGaussianCurvature() * AVG_CURVATURE_MULTIPLICATOR)) {
                    continue;
                }

                double normCos = cache.getNormCosVec(i, j).dot(normal);
                if (Math.abs(normCos) < MIN_ANGLE_COS) {
                    continue;
                }

                Vector3d vec = new Vector3d(vertices.get(i).getPosition());
                vec.sub(vertices.get(j).getPosition());
                vec.normalize();
                double cos = vec.dot(normal);
                if (Math.abs(cos) < MIN_ANGLE_COS) {
                    continue;
                }

                if (Math.abs(normal.dot(cache.getAvgPos(i, j)) - d) <= maxDist) {
                    votes++;
                }
            }
        }

        return votes;
    }

    private static boolean similarCurvature(Curvature gi, Curvature gj, double gh) {
        if (gi == null || gj == null || gi.gaussian() == Double.NaN || gj.gaussian() == Double.NaN) {
            return true; // can't decide => continue in the computation
        }
        return (gi.gaussian() * gj.gaussian() > 0)
                && (Math.abs(gi.gaussian()) >= gh)
                && (Math.abs(gj.gaussian()) >= gh);
    }

}
