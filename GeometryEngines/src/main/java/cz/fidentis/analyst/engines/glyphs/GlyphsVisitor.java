package cz.fidentis.analyst.engines.glyphs;

import cz.fidentis.analyst.data.mesh.MeshVisitor;
import cz.fidentis.analyst.data.shapes.Glyph;

import java.util.List;

/**
 * Visitor, which calculate suitable position and orientation of glyphs from visited meshes.
 * It uses given sub-sampling strategy to locate glyphs.
 * <p>
 *     Based on
 *     <a href="https://ieeexplore.ieee.org/document/597794">V. Interrante et al.: Conveying the 3D shape of smoothly curving transparent surfaces via texture</a>,
 *     1997, doi: 10.1109/2945.597794
 * </p>
 *
 * @author Ondrej Simecek
 * @author Radek Oslejsek
 */
public interface GlyphsVisitor extends MeshVisitor {

    /**
     * Returns computed glyphs.
     * @return computed glyphs
     */
    List<Glyph> getGlyphs();
}
