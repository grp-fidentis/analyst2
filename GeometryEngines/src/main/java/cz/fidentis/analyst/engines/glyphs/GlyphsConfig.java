package cz.fidentis.analyst.engines.glyphs;

import cz.fidentis.analyst.engines.glyphs.impl.GlyphsVisitorImpl;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;

/**
 * Configuration object for glyphs.
 *
 * @author Radek Oslejsek
 *
 * @param pointSamplingConfig Configuration of sub-sampling method
 */
public record GlyphsConfig(PointSamplingConfig pointSamplingConfig) {

    /**
     * Instantiates and returns a symmetry visitor.
     *
     * @return a symmetry visitor
     */
    public GlyphsVisitor getVisitor() {
        return new GlyphsVisitorImpl(pointSamplingConfig);
    }
}
