package cz.fidentis.analyst.engines.sampling;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshVisitor;

import java.util.List;

/**
 * Stateful sub-sampling.
 * First, the visitor is created. Then, it is applied to mesh facets. Finally, the samples are returned.
 *
 * @author Radek Oslejsek
 */
public interface PointSamplingVisitor extends MeshVisitor {

    @Override
    void visitMeshFacet(MeshFacet facet);

    /**
     * Changes the number of required samples.
     *
     * @param max Maximal number of vertices. Must be bigger than zero
     */
    void setRequiredSamples(int max);

    /**
     * Returns the number of required samples.
     * @return the number of required samples
     */
    int getRequiredSamples();

    /**
     * Returns a list of vertices reduced according to the strategy.
     * The returned mesh points are backed by original points.
     *
     * @return selected vertices of inspected meshes
     */
    List<MeshPoint> getSamples();
    
    /**
     * If {@code true}, then the returned points samples are points from the original mesh.
     * Therefore, the transformation of the original mesh also transform these samples.
     * If {@code false}, then new points are returned that are independent on the original mesh.
     * 
     * @return {@code true} if the point samples include points of the original mesh
     */
    boolean isBackedByOrigMesh();

}
