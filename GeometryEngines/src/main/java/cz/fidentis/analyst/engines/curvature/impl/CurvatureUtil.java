package cz.fidentis.analyst.engines.curvature.impl;

import cz.fidentis.analyst.data.mesh.Curvature;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import javax.vecmath.Point3d;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector4d;
import java.util.Collection;
import java.util.Objects;

/**
 * A utility class that computes principal curvatures and min/max direction of any point on a mesh.
 *
 * @author Ondrej Simecek
 */
public class CurvatureUtil {

    private static final double NEIGHBOURHOOD_DISTANCE = 100; //squared for efficiency

    /**
     * Calculates principal curvatures and principal directions. If the calculation fails returns null.
     *
     * @param point point on a surface
     * @param facets The surface
     * @return curvature
     */
    public static Curvature calculatePrincipalCurvatures(MeshPoint point, Collection<MeshFacet> facets) {
        Vector3d[] bases = computeBases(point);
        Vector4d secondFundamentalForm = computeSecondFundamentalForm(facets, point.getPosition(), bases[0], bases[1]);
        double[] eigenValues = computeEigenValues(secondFundamentalForm);

        if (eigenValues == null) {
            return null;
        }

        Vector4d sFFminusEigen = new Vector4d(
                secondFundamentalForm.x - eigenValues[0],
                secondFundamentalForm.y,
                secondFundamentalForm.z,
                secondFundamentalForm.w - eigenValues[0]);
        double y = (sFFminusEigen.x + sFFminusEigen.z) / -(sFFminusEigen.y + sFFminusEigen.w);
        Vector2d eigenVector1 = new Vector2d(1, y);
        eigenVector1.normalize();

        //rotate base
        Vector3d tempBase1 = new Vector3d(bases[0]);
        Vector3d tempBase2 = new Vector3d(bases[1]);
        tempBase1.scale(eigenVector1.x);
        tempBase2.scale(eigenVector1.y);
        tempBase1.add(tempBase2);
        tempBase1.normalize();

        Vector3d maxCurvatureDir = new Vector3d(tempBase1);
        Vector3d minCurvatureDir = new Vector3d();
        minCurvatureDir.cross(maxCurvatureDir, point.getNormal());

        return new Curvature(eigenValues[0], eigenValues[1], maxCurvatureDir, minCurvatureDir);
    }

    private static Vector3d[] computeBases(MeshPoint point) {
        Vector3d temp = new Vector3d(1, 0, 0);
        if (point.getNormal().y == 0 && point.getNormal().z == 0) {
            temp = new Vector3d(0, 1, 0);
        }
        Vector3d base3 = new Vector3d(point.getNormal());
        base3.normalize();
        Vector3d base1 = new Vector3d();
        Vector3d base2 = new Vector3d();
        base1.cross(base3, temp);
        base2.cross(base3, base1);

        return new Vector3d[] {base1, base2, base3};
    }

    private static Vector4d computeSecondFundamentalForm(Collection<MeshFacet> facets, Point3d pointPosition, Vector3d base1, Vector3d base2) {
        Vector4d secondFundamentalForm = new Vector4d(); //second fundamental form
        long facesCounted = facets.stream()
                .map(MeshFacet::getTriangles)
                .flatMap(Collection::stream)
                .map(tri -> tri.getCurvatureOfTrianglePlane(
                        pointPosition,
                        NEIGHBOURHOOD_DISTANCE,
                        base1, base2))
                .filter(Objects::nonNull)
                .map(curv -> {
                    secondFundamentalForm.add(curv);
                    return curv;
                })
                .count();
        secondFundamentalForm.scale(1.0 / facesCounted);
        return secondFundamentalForm;
    }

    private static double[] computeEigenValues(Vector4d secondFundamentalForm) {
        double b = -secondFundamentalForm.w - secondFundamentalForm.x;
        double discriminant = b * b + 4 * (secondFundamentalForm.x * secondFundamentalForm.w -
                secondFundamentalForm.y * secondFundamentalForm.z);
        double eigenValue1;
        double eigenValue2;
        if (discriminant >= 0) {
            eigenValue1 = (-b + Math.sqrt(discriminant)) / 2;
            eigenValue2 = (-b - Math.sqrt(discriminant)) / 2;
            if (Math.abs(eigenValue1) < Math.abs(eigenValue2)) {
                double tempEigen = eigenValue1;
                eigenValue1 = eigenValue2;
                eigenValue2 = tempEigen;
            }
        } else {
            return null;
        }
        return new double[] {eigenValue1, eigenValue2};
    }
}
