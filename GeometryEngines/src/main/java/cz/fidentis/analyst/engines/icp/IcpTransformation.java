package cz.fidentis.analyst.engines.icp;

import cz.fidentis.analyst.math.Quaternion;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

/**
 * IcpTransformation class is holding computed data for transformation.
 *
 * @author Maria Kocurekova
 *
 * @param translation Translation represents translation of ComputedFacet on x, y, z axis.
 * @param rotation Rotation is represented by Quaternion
 *                 (x, y, z coordinate and scalar component).
 * @param scaleFactor ScaleFactor represents scale between two objects.
 *                    In case there is no scale the value is 1.
 * @param rotCenter Center of rotation of the transformed object. It is the center of the static object
 *                  towards which the transform object is to be registered.
 *                  If {@code null}, then the center of the origin is taken into account.
 */
public record IcpTransformation(Vector3d translation, Quaternion rotation, double scaleFactor, Tuple3d rotCenter) {

    /**
     * Apply transformation to given 3D point.
     *
     * @param point Original point
     * @param scale Whether to scale as well
     * @return transformed point or {@code null}
     */
    public Point3d transformPoint(Point3d point, boolean scale) {
        if (point == null) {
            return null;
        }

        Quaternion rotQuat = (rotCenter == null)
                ? new Quaternion(point.x, point.y, point.z, 1)
                : new Quaternion(point.x - rotCenter.x, point.y - rotCenter.y, point.z - rotCenter.z, 1);
        Quaternion rotationCopy = Quaternion.multiply(rotQuat, rotation.getConjugate());
        rotQuat = Quaternion.multiply(rotation, rotationCopy);

        Tuple3d tr = (rotCenter == null) ? new Point3d() : rotCenter;
        if(scale && !Double.isNaN(scaleFactor)) {
            return new Point3d(
                    rotQuat.x * scaleFactor + translation.x + tr.x,
                    rotQuat.y * scaleFactor + translation.y + tr.y,
                    rotQuat.z * scaleFactor + translation.z + tr.z
            );
        } else {
            return new Point3d(
                    rotQuat.x + translation.x + tr.x,
                    rotQuat.y + translation.y + tr.y,
                    rotQuat.z + translation.z + tr.z
            );
        }
    }
    
    /**
     * Rotates the normal vector accordingly.
     *
     * @param normal Normal vector
     * @return transformed point or {@code null}
     */
    public Vector3d transformNormal(Vector3d normal) {
        if (normal == null) {
            return null;
        }
        
        Quaternion rotQuat = new Quaternion(normal.x, normal.y, normal.z, 1);
        Quaternion rotationCopy = Quaternion.multiply(rotQuat, rotation.getConjugate());
        rotQuat = Quaternion.multiply(rotation, rotationCopy);
        
        return new Vector3d(rotQuat.x, rotQuat.y, rotQuat.z);
    }
}
