package cz.fidentis.analyst.engines.cut;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.shapes.CrossSection3D;

import java.util.Collection;
import java.util.List;

/**
 * Stateless services for the calculation of cross-sections.
 * It is a wrapper to the stateful implementation in {@link cz.fidentis.analyst.engines.cut.CrossSectionVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface CrossSectionServices {

    /**
     * Returns a cross-section curve of the given mesh.
     *
     * @param mesh Mesh model to be inspected
     * @param config Configuration of the service
     * @return result or {@code null}
     */
    static CrossSection3D compute(MeshModel mesh, CrossSectionConfig config) {
        return compute(mesh.getFacets(), config);
    }

    /**
     * Returns a cross-section curve of the given mesh.
     *
     * @param mesh Mesh facet to be inspected
     * @param config Configuration of the service
     * @return result or {@code null}
     */
    static CrossSection3D compute(MeshFacet mesh, CrossSectionConfig config) {
        return compute(List.of(mesh), config);
    }

    /**
     * Returns a cross-section curve of the given mesh.
     *
     * @param mesh Mesh facets to be inspected
     * @param config Configuration of the service
     * @return result or {@code null}
     */
    static CrossSection3D compute(Collection<MeshFacet> mesh, CrossSectionConfig config) {
        var visitor = config.getVisitor();
        mesh.forEach(facet -> facet.accept(visitor));
        return visitor.getCrossSectionCurve();
    }
}
