package cz.fidentis.analyst.engines.point2surface;

import cz.fidentis.analyst.data.kdtree.KdTreeVisitor;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.util.List;
import java.util.Map;

/**
 * Classes implementing this interface can compute minimal distance between a 3D point (set in a constructor)
 * to a mesh facets.
 * <p>
 * The distance computation can be either absolute or relative. Absolute means
 * that the Euclidean distance between 3D points in space is used. Because only
 * positive distances are taken into calculation, then the minimum distance is
 * the smallest found distance.
 * </p>
 * <p>
 * Relative distance also takes the normal vector of the 3D point into account.
 * If the "measured" point is located in the half space of the normal direction,
 * then the distance is positive. Otherwise, the distance is negative.
 * The minimum distance is a value "closest to zero".
 * </p>
 * 
 * @author Radek Oslejsek
 */
public interface PointToSurfaceDistanceVisitor extends KdTreeVisitor {

    /**
     * Returns the minimal found distance between a 3D point (set in a constructor) and visited mesh facets.
     * 
     * @return the minimal found distance, 
     * {@code Double.POSITIVE_INFINITY} if no distance has been computed so far
     */
    double getDistance();

    /**
     * Returns the closest mesh facets and their closest points.
     * As there can be more point in the same (smallest) distance, the method returns
     * a list instead of a single point. Similarly, as the closest point(s) can belong to
     * multiple facets, a map of facet is returned instead of a single facet.
     *
     * @return closest mesh facets and their closest points.
     */
    Map<MeshFacet, List<MeshPoint>> getNearestPoints();
}
