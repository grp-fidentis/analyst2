package cz.fidentis.analyst.engines.raycasting.impl;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.OctNode;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionVisitor;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.*;

/**
 * This visitor throws a ray from origin point and finds all intersections of the
 * ray with facets.
 * <p>
 * The implementation is based on<br/>
 * [1] <a href="https://lsi2.ugr.es/curena/inves/wscg00/revelles-wscg00.pdf">Revelles, 
 * Jorge, Carlos Urena, and Miguel Lastra. "An efficient parametric algorithm for octree traversal." (2000).</a>.
 * </p>
 *
 * @author Enkh-Undral EnkhBayar
 * @author Radek Oslejsek
 */
public class RayIntersectionVisitorImpl implements RayIntersectionVisitor {

    private final MeshTriangle.Smoothing smoothing;
    private final Ray ray;
    private final Set<TriangleProxy> processedTriangles = new HashSet<>();

    /**
     * Constructor.
     *
     * @param ray       Ray, must not be {@code null}
     * @param smoothing Smoothing strategy
     * @throws IllegalArgumentException if the {@code ray} is {@code null}
     */
    public RayIntersectionVisitorImpl(Ray ray, MeshTriangle.Smoothing smoothing) {
        if (ray == null) {
            throw new IllegalArgumentException("ray");
        }
        this.ray = ray;
        this.smoothing = smoothing;
    }

    @Override
    public void visitOctree(Octree octree) {
        if (octree == null) {
            throw new IllegalArgumentException("octree is null");
        }
        
        // move the ray away from the bounding box:
        Ray r = new Ray(ray);
        Vector3d dir = new Vector3d(ray.direction());
        dir.scale(-10000);
        r.origin().add(dir);
        
        traverseOctree(octree, r); // new approach
    }

    @Override
    public List<RayIntersection> getUnsortedIntersections(boolean filter) {
        List<RayIntersection> ret = new ArrayList<>();
        for (var proxy: this.processedTriangles) {
            if (filter && proxy.tri.computeOrientedNormal().dot(ray.direction()) < 0) { // skip
                continue;
            }

            RayIntersection i = proxy.tri.getRayIntersection(ray, smoothing);
            if (i != null) {
                ret.add(i);
            }
        }
        return ret;
    }
    
    @Override
    public SortedSet<RayIntersection> getSortedIntersections(boolean filter) {
        List<RayIntersection> list = getUnsortedIntersections(filter);
        list.forEach(i -> i.setDistance(ray.origin()));
        return new TreeSet<>(list);
    }
    
    @Override
    public RayIntersection getClosestIntersection(boolean filter) {
        return getSortedIntersections(filter).stream()
                .findFirst()
                .orElse(null);
    }
    
    /**
     * Traverse an octree and finds candidate triangles that might be intersected
     * by the ray.
     * 
     * @param octree An octree
     * @param ray A ray. It must start outside the root node of the octree!
     */
    protected void traverseOctree(Octree octree, Ray ray) {
        Ray r = new Ray(ray);
        int subNodeMask = getSubNodeMask(octree.getRoot(), r);
        
        List<Point3d> params = getParams(octree.getRoot(), r);
        Point3d t0 = params.get(0);
        Point3d t1 = params.get(1);
        
        double t0max = Math.max(Math.max(t0.x, t0.y), t0.z);
        double t1min = Math.min(Math.min(t1.x, t1.y), t1.z);
        
        if (t0max < t1min) {
            processSubTree(octree.getRoot(), t0, t1, subNodeMask);
        }
    }
    
    /**
     * Flips the ray so that its coordinates are positive and
     * sets the transformation mask of sub-node indexes accordingly. 
     * In contrast to the paper [1], this implementation is modified so that 
     * the octree can be placed anywhere in the space.
     * 
     * @param root Root node of the octree. 
     * @param ray Ray. It can be modified!
     * @return transformation mask of sub-node indexes
     */
    protected int getSubNodeMask(OctNode root, Ray ray) {
        int subNodeMask = 0;

        Point3d ro = ray.origin();
        Vector3d rd = ray.direction();

        Point3d lowerBound = root.getLowerBound();
        Point3d upperBound = root.getUpperBound();
        
        if (rd.x < 0.0) {
            ro.x = lowerBound.x + upperBound.x - ro.x;
            rd.x *= -1.0;
            subNodeMask |= 4;
        }
        if (rd.y < 0.0) {
            ro.y = lowerBound.y + upperBound.y - ro.y;
            rd.y *= -1.0;
            subNodeMask |= 2;
        }
        if (rd.z < 0.0) {
            ro.z = lowerBound.z + upperBound.z - ro.z;
            rd.z *= -1.0;
            subNodeMask |= 1;
        }
        
        return subNodeMask;
    }
    
    /**
     * Computes initial values of {@code t0} and {@code t1} from [1].
     * 
     * @param root Root node of the octree
     * @param ray ray
     * @return {@code t0} and {@code t1}
     */
    protected List<Point3d> getParams(OctNode root, Ray ray) {
        Point3d ro = ray.origin();
        Vector3d rd = ray.direction();
        
        Point3d t0 = new Point3d(root.getLowerBound());
        t0.sub(ro);
        t0.x /= rd.x;
        t0.y /= rd.y;
        t0.z /= rd.z;
        
        Point3d t1 = new Point3d(root.getUpperBound());
        t1.sub(ro);
        t1.x /= rd.x;
        t1.y /= rd.y;
        t1.z /= rd.z;
        
        return List.of(t0, t1);
    }
    
    /**
     * The main traversal algorithms from [1].
     */
    protected void processSubTree(OctNode node, Point3d t0, Point3d t1, int subNodeMask) {
        if (t1.x < 0.0 || t1.y < 0.0 || t1.z < 0.0) {
            return;
        }
        
        if (node.isLeafNode()) {
            processedTriangles.addAll(node.getTriangles().stream()
                    .map(TriangleProxy::new)
                    .toList()
            );
            return;
        }
        
        Point3d tm = getNextCellPoint(t0, t1, node);
        
        int octantIndex = getFistSubNode(t0, tm);
        Point3d newT0;
        Point3d newT1;
        do {
            switch (octantIndex) {
                case 0:
                    newT0 = t0;
                    newT1 = tm;
                    processSubTree(node.getOctant(subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = nextSubNode(tm.x, 4, tm.y, 2, tm.z, 1);
                    break;
                case 1:
                    newT0 = new Point3d(t0.x, t0.y, tm.z);
                    newT1 = new Point3d(tm.x, tm.y, t1.z);
                    processSubTree(node.getOctant(1^subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = nextSubNode(tm.x, 5, tm.y, 3, t1.z, 8);
                    break;
                case 2:
                    newT0 = new Point3d(t0.x, tm.y, t0.z);
                    newT1 = new Point3d(tm.x, t1.y, tm.z);
                    processSubTree(node.getOctant(2^subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = nextSubNode(tm.x, 6, t1.y, 8, tm.z, 3);
                    break;
                case 3:
                    newT0 = new Point3d(t0.x, tm.y, tm.z);
                    newT1 = new Point3d(tm.x, t1.y, t1.z);
                    processSubTree(node.getOctant(3^subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = nextSubNode(tm.x, 7, t1.y, 8, t1.z, 8);
                    break;
                case 4:
                    newT0 = new Point3d(tm.x, t0.y, t0.z);
                    newT1 = new Point3d(t1.x, tm.y, tm.z);
                    processSubTree(node.getOctant(4^subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = nextSubNode(t1.x, 8, tm.y, 6, tm.z, 5);
                    break;
                case 5:
                    newT0 = new Point3d(tm.x, t0.y, tm.z);
                    newT1 = new Point3d(t1.x, tm.y, t1.z);
                    processSubTree(node.getOctant(5^subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = nextSubNode(t1.x, 8, tm.y, 7, t1.z, 8);
                    break;
                case 6:
                    newT0 = new Point3d(tm.x, tm.y, t0.z);
                    newT1 = new Point3d(t1.x, t1.y, tm.z);
                    processSubTree(node.getOctant(6^subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = nextSubNode(t1.x, 8, t1.y, 8, tm.z, 7);
                    break;
                case 7:
                    newT0 = new Point3d(tm.x, tm.y, tm.z);
                    newT1 = new Point3d(t1.x, t1.y, t1.z);
                    processSubTree(node.getOctant(7^subNodeMask), newT0, newT1, subNodeMask);
                    octantIndex = 8;
                    break;
                default:
                    break;
            }
        } while(octantIndex < 8);
    }
    
    /**
     * Computes {@code t_m} from [1].
     */
    protected Point3d getNextCellPoint(Point3d t0, Point3d t1, OctNode node) {
        return new Point3d(
                0.5 * sumCoord(t0.x, t1.x, node.getLowerBound().x, node.getUpperBound().x, ray.origin().x),
                0.5 * sumCoord(t0.y, t1.y, node.getLowerBound().y, node.getUpperBound().y, ray.origin().y),
                0.5 * sumCoord(t0.z, t1.z, node.getLowerBound().z, node.getUpperBound().z, ray.origin().z)
        );
    }
    
    /**
     * Equation 12 from [1]. 
     */
    protected double sumCoord(double t0, double t1, double o0, double o1, double p) {
        if (Double.isInfinite(t0) && Double.isInfinite(t1)) {
            return (p < ((o0+o1)/2.0)) ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
        } else {
            return t0 + t1;
        }
    }

    /**
     * The {@code first_node} method from [1] implementing Tables 1 and 2.
     * It returns the first sub-node of the node intersected by a ray.
     * In the contract to the original paper, indexes 1 and 4 have to be switched 
     * (error in the paper?)
     */
    protected int getFistSubNode(Point3d t0, Point3d tm) {
        double max = Math.max(Math.max(t0.x, t0.y), t0.z);
        int ret = 0;
        
        if (max == t0.x) { // entry plane: YZ
            if (tm.y < t0.x) {
                ret |= 2; 
            }
            if (tm.z < t0.x) {
                ret |= 1; // 4 in the original paper
            }
        } else if (max == t0.y) { // entry plane: XZ
            if (tm.x < t0.y) {
                ret |= 4; // 1 in the original paper
            }
            if (tm.z < t0.y) {
                ret |= 1; // 4 in the original paper
            }            
        } else { // entry plane: XY
            if (tm.x < t0.z) {
                ret |= 4; // 1 in the original paper
            }
            if (tm.y < t0.z) {
                ret |= 2; 
            }
        }
        
        return ret;
    }

    /**
     * The {@code new_node} method from [1] implementing Table 3. 
     * It returns the index of the next sub-node intersected by a ray.
     * 
     */
    protected int nextSubNode(double tx1, int yzExitPlane, double ty1, int xzExitPlane, double tz1, int xyExitPlane) {
        double min = Math.min(Math.min(tx1, ty1), tz1);
        if (min == tx1) {
            return yzExitPlane;
        } else if (min == ty1) {
            return xzExitPlane;
        } else {
            return xyExitPlane;
        }
    }
    
    /**
     * A proxy class that overrides equation of triangles to consider their 
     * address (instead of geometric equation by vertices)
     * 
     * @author Radek Oslejsek
     */
    private class TriangleProxy {
        
        private final MeshTriangle tri;
        
        TriangleProxy(MeshTriangle tri) {
            this.tri = tri;
        }
        
        @Override
        public boolean equals(Object other) {
            if (!(other instanceof TriangleProxy)) {
                return false;
            }
            return this.tri == ((TriangleProxy) other).tri;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(tri);
        }
        
        
    }
}
