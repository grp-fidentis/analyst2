package cz.fidentis.analyst.engines.interactivemask;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.octree.OctreeVisitor;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask;
import cz.fidentis.analyst.data.surfacemask.SurfaceMask2D;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionServices;

import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.util.List;

/**
 * Visits meshes and returns intersections with projection of SurfaceMask2D
 * @author Mario Chromik
 */
public class MaskProjector implements OctreeVisitor {

    private final List<SurfaceMask2D.PointToProject> points;

    private SurfaceMask projectedMask = new SurfaceMask();

    private final MaskProjectorConfig config;

    /**
     * Constructor using configuration and surface mask
     * @param config configuration for visitor
     * @param points points to project
     */
    public MaskProjector(MaskProjectorConfig config, List<SurfaceMask2D.PointToProject> points) {
        this.config = config;
        this.points = points;
    }

    private Ray getRayFromPoint(Point point) {
        //Points are drawn in canvas which has the point (0, 0) in the top left, move points so the center is in the middle
        Point2d normalizedPoint = new Point2d(point.getX() - config.panelWidth()/2.0 ,point.getY() - config.panelHeight()/2.0);
        //scale point by the scale of panel to bbox, scale by a percentage and scale to canvas
        normalizedPoint.x = normalizedPoint.x * config.bboxScale() * config.scaleFactor() * config.canvasWidth() / config.panelWidth();
        normalizedPoint.y = normalizedPoint.y * config.bboxScale() * config.scaleFactor() * config.canvasHeight() / config.panelHeight();
        Ray ray = ScreenPointToRay.convert((int) (normalizedPoint.x   + config.canvasWidth() / 2),
                (int) (normalizedPoint.y + config.canvasHeight() / 2),
                config.canvasWidth(), config.canvasHeight(), config.fov(),
                config.cameraPosition(), config.cameraCenter(),
                config.cameraUpDirection());

        return ray;
    }

    /**
     * Gets the result from visitor
     * @return projected surface mask
     */
    public SurfaceMask getResult() {
        return projectedMask;
    }


    @Override
    public void visitOctree(Octree octree) {
        if (points == null) {
            return;
        }
        for (SurfaceMask2D.PointToProject point : points) {
            RayIntersection ri = RayIntersectionServices.computeClosest(octree,
                    new RayIntersectionConfig(getRayFromPoint(point.getPoint()), MeshTriangle.Smoothing.NORMAL, false));
            if (ri != null) {
                projectedMask.addPoint(
                        ri.getPosition(),
                        ri.getHitTriangle().getFacet(),
                        ri.getHitTriangle().computeOrientedNormal(),
                        point.isSelected(), point.isSampled());
            }
        }
    }

    /**
     * Utility class for converting screen position to ray
     *
     * @author Mario Chromik
     *
     * @deprecated The implementation is not correct and was replaced by
     * the {@link  cz.fidentis.analyst.canvas.Canvas#castRayThroughPixel(int, int)}
     * method, which is mased on the {@code gluUnProject} reverse mapping of the projection and modelview
     * OpenGL matrices.
     */
    @Deprecated(forRemoval = true)
    private class ScreenPointToRay {

        /**
         * converts screen point to ray by first calculating position on near plane,
         * constructing a coordinate frame and calculates direction from camera
         * position to point on near plane within this frame
         * @param screenX screen x coordinate
         * @param screenY screen y coordinate
         * @param width view port width
         * @param height view port height
         * @param camPos position of camera within scene
         * @param camCenter direction the camera is pointing
         * @param camUpDir camera up direction
         * @return ray
         */
        public static Ray convert(int screenX, int screenY, int width, int height, int fov, Vector3d camPos, Vector3d camCenter, Vector3d camUpDir) {
            Point3d nearPlanePos = calculatePositionOnNearPlane(screenX, screenY, width, height);

            double fovY = fov * Math.PI / 180.0;
            double fovX = fovY * width/height;

            Vector3d camDir = calculateCameraDirection(camPos, camCenter);
            Vector3d camRight = calculateOrthogonalCameraRight(camUpDir, camDir);
            Vector3d camUp = calculateOrthogonalCameraUp(camDir, camRight);

            double alpha = nearPlanePos.x * Math.tan(fovX / 2) * nearPlanePos.z;
            double beta = nearPlanePos.y * Math.tan(fovY / 2) * nearPlanePos.z;

            Vector3d upScaled = new Vector3d(beta * camUp.x, beta * camUp.y, beta * camUp.z);
            Vector3d rightScaled = new Vector3d(alpha * camRight.x, alpha * camRight.y, alpha * camRight.z);

            camDir.negate();
            upScaled.add(rightScaled);
            upScaled.add(camDir);

            Vector3d dir = new Vector3d();
            dir.normalize(upScaled);

            Point3d origin = new Point3d(camPos);
            return new Ray(origin, dir);
        }

        /**
         * Converts pixel coordinates to Normalized Device Coordinates
         *
         * @param screenX screen x coordinate
         * @param screenY screen y coordinate
         * @param width width of view port
         * @param height height of view port
         * @return Normalized Device Coordinates of the pixel's location in the range from -1 to 1 for both X and Y axes.
         */
        private static Point3d calculatePositionOnNearPlane(int screenX, int screenY, int width, int height) {
            double x = (2.0 * screenX) / width - 1.0;
            double y = 1.0 - (2.0 * screenY) / height;
            double z = 1.0;

            return new Point3d(x, y, z);
        }

        /**
         * calculates camera direction from camera position to center of scene
         * @param positionCam position of camera
         * @param centerCam  center of camera
         * @return camera direction
         */
        private static Vector3d calculateCameraDirection(Vector3d positionCam, Vector3d centerCam) {
            Vector3d camPos = new Vector3d(positionCam);
            Vector3d center = new Vector3d(centerCam);
            center.negate();
            camPos.add(center);
            Vector3d camDir = new Vector3d();
            camDir.normalize(camPos);
            camDir.scale(1);

            return camDir;
        }

        /**
         * calculates orthogonal camera right direction
         * @param up camera's up direction
         * @param camDir direction to the center of the scene
         * @return camera right
         */
        private static Vector3d calculateOrthogonalCameraRight(Vector3d up, Vector3d camDir) {
            Vector3d right = new Vector3d();
            right.cross(up, camDir);
            right.normalize();

            return right;
        }

        /**
         * calculates orthogonal camera up direction
         * @param camDir
         * @param camRight
         * @return orthogonal camera up direction
         */
        private static Vector3d calculateOrthogonalCameraUp(Vector3d camDir, Vector3d camRight) {
            Vector3d up = new Vector3d();
            up.cross(camDir, camRight);
            up.normalize();

            return up;
        }
    }
}
