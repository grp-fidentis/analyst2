/**
 * Glyphs to visually recognize distance of meshes.
 */
package cz.fidentis.analyst.glyphs;