package cz.fidentis.analyst.engines.avgmesh;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.avgmesh.impl.AvgMeshVisitorNN;
import cz.fidentis.analyst.engines.avgmesh.impl.AvgMeshVisitorRC;
import cz.fidentis.analyst.engines.avgmesh.impl.AvgMeshVisitorRCGPU;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * Strategies of the average mesh computation.
 *
 * @author Radek Oslejsek
 *
 * @param templateMesh Template mesh which is transformed to the averaged mesh.
 *                     The original mesh remains unchanged. New mesh is allocated instead.
 * @param glContext OpenGL context for GPU-accelerated computation. Can be {@code null} for CPU-based methods.
 */
public record AvgMeshConfig(Collection<MeshFacet> templateMesh, GLContext glContext) {

    /**
     * Constructor.
     * @param facet Template mesh facet which is transformed to the averaged mesh.
     *              The original mesh remains unchanged. New mesh is allocated instead.
     * @param glContext OpenGL context for GPU-accelerated computation. Can be {@code null} for CPU-based methods.
     */
    public AvgMeshConfig(MeshFacet facet, GLContext glContext) {
        this(Collections.singleton(facet), glContext);
    }

    /**
     * Constructor.
     * @param model Template mesh model which is transformed to the averaged mesh.
     *              The original mesh remains unchanged. New mesh is allocated instead.
     * @param glContext OpenGL context for GPU-accelerated computation. Can be {@code null} for CPU-based methods.
     */
    public AvgMeshConfig(MeshModel model, GLContext glContext) {
        this(model.getFacets(), glContext);
    }

    /**
     * Instantiates and returns a k-d tree visitor.
     * @return a visitor
     */
    public AvgMeshVisitor getNearestNeighborsVisitor() {
        return new AvgMeshVisitorNN(templateMesh);
    }

    /**
     * Instantiates and returns an octree visitor.
     * @return a visitor
     */
    public AvgMeshVisitor getRayCastingVisitor() {
        return new AvgMeshVisitorRC(templateMesh);
    }

    /**
     * Instantiates and returns a mesh visitor.
     * @return a visitor
     */
    public AvgMeshVisitor getRayCastingGpuVisitor() {
        return new AvgMeshVisitorRCGPU(templateMesh, Objects.requireNonNull(glContext));
    }
}
