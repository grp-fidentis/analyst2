package cz.fidentis.analyst.engines.point2surface;

import cz.fidentis.analyst.engines.point2surface.impl.ClosestMeshVerticesImpl;
import cz.fidentis.analyst.engines.point2surface.impl.ClosestSurfacePointsImpl;

import javax.vecmath.Point3d;

/**
 * Point sampling strategies and their configuration.
 *
 * @author Radek Oslejsek
 *
 * @param method Distance method
 * @param point The point whose distance is measured
 * @param checkOverlay If {@code true} and the closest point lies on the boundary of the mesh stored in the k-d tree,
 *                     then the reference point is considered "outside" of the mesh and the distance is set to infinity.
 */
public record PointToSurfaceDistanceConfig(Method method, Point3d point, boolean checkOverlay) {

    /**
     * Instantiates and returns a point sampling visitor.
     *
     * @return a point sampling visitor
     */
    public PointToSurfaceDistanceVisitor getVisitor() {
        return switch (method) {
            case POINT_TO_VERTICES -> new ClosestMeshVerticesImpl(point, checkOverlay);
            case POINT_TO_SURFACE -> new ClosestSurfacePointsImpl(point, checkOverlay);
        };
    }

    /**
     * Sampling method.
     *
     * @author Radek Oslejsek
     */
    public enum Method {
        /**
         * Computes the distance to the closest mesh vertices.
         */
        POINT_TO_VERTICES,

        /**
         * Computes the distance to the closest point on polygonal mesh surface (i.e., inside mesh triangles).
         * <strong>This computation is approximate</strong> and can produce wrong results.
         */
        POINT_TO_SURFACE
    }
}
