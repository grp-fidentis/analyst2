package cz.fidentis.analyst.engines.symmetry.impl;

import cz.fidentis.analyst.data.grid.UniformGrid3d;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.curvature.CurvatureConfig;
import cz.fidentis.analyst.engines.curvature.CurvatureServices;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A robust implementation of symmetry plane estimation for manifold triangle meshes.
 * The code is based on the
 * <a href="https://link.springer.com/article/10.1007/s00371-020-02034-w">...</a> paper.
 * This extension uses the similarity of Gaussian curvature values (the static weight)
 * and the symmetry of normal vectors (the dynamic weight) in the computation,
 * along with Wendland’s similarity function. The computation is a bit slower
 * in general (depends on parameters set, of course) that the super-class, 
 * but the weights should help the algorithm to <b>better deal with damaged faces,
 * i.e., when only a small fragment of a face is available</b>.
 * <p>
 * Although the pruning of candidate planes and the measurement of their quality
 * is similar to the {@link SymmetryVisitorMesh} implementation, this code
 * is much slower with similar quality of results :-( Probably the price for 
 * robustness (need to be further researched).
 * </p>
 *
 * @author Radek Oslejsek
 */
public class SymmetryVisitorMesh extends SymmetryVisitorPointCloud {

    /**
     * Two points whose Euclidean distance or curvature difference is less than this
     * value are considered the same and skipped from generating candidate planes
     */
    public static final double EQUALS_EPS = 0.001;
    
    /**
     * Constructor.
     * 
     * @param samplingStrategy Subsampling strategy for finding candidate planes. Must not be {@code null}
     *                         Use {@code NoSampling} strategy to avoid subsampling.
     * @param maxSamplesForCandidatePruning Secondary subsampling used for finding the best candidate
     *                                      (pruning of candidates) uses always the <b>Uniform 3D grid.</b>
     *                                      This parameter sets the strength of this subsampling.
     */
    public SymmetryVisitorMesh(PointSamplingConfig samplingStrategy, int maxSamplesForCandidatePruning) {
        super(samplingStrategy, maxSamplesForCandidatePruning);
    }
    
    @Override
    public void visitMeshFacet(MeshFacet facet) {
        // We need vertex normals
        synchronized (this) {
            if (!facet.hasVertexNormals()) {
                facet.calculateVertexNormals();
            }
        }

        // We need Gaussian curvatures
        synchronized (this) {
            if (facet.getVertex(0).getCurvature() == null) {
                CurvatureServices.computeAndSet(facet, new CurvatureConfig());
            }
        }

        super.visitMeshFacet(facet);
    }
    
    /**
     * Copies mesh samples, moves them to the space origin, and then computes candidate planes.
     * This implementation uses curvature and normal vectors to prune candidates.
     * 
     * @param meshSamples Subsampled mesh
     * @param centroid Centroid of the subsampled mesh
     * @return Candidate planes
     */
    protected Set<CandidatePlaneRobust> generateCandidates(List<MeshPoint> meshSamples, Point3d centroid) {
        ProcessedCloud cloud = new ProcessedCloud(meshSamples, centroid);
        Set<CandidatePlaneRobust> ret = new HashSet<>();

        for (int i = 0; i < cloud.points.size(); i++) {
            MeshPoint pi = cloud.points.get(i);
            for (int j = i; j < cloud.points.size(); j++) { // from i !!!
                if (i == j) {
                    continue;
                }
                MeshPoint pj = cloud.points.get(j);

                if (pi.getPosition().epsilonEquals(pj.getPosition(), EQUALS_EPS) ||
                        cloud.getCurvatureSimilarity(pi, pj) <= EQUALS_EPS) {
                    continue;
                }

                CandidatePlaneRobustMesh candPlane = new CandidatePlaneRobustMesh(
                        pi.getPosition(),
                        pj.getPosition(),
                        cloud.avgDistance
                );

                Vector3d nri = candPlane.getPlane().reflectUnitVectorOverPlane(pi.getNormal());
                if (CandidatePlaneRobustMesh.getNormalVectorsWeight(nri, pj.getNormal()) <= 0.25) {
                    continue;
                }

                ret.add(candPlane);
            }
        }
        
        return ret;
    }
    
    /**
     * Copies mesh samples, moves them to the space origin, and then measures the quality of 
     * candidate planes. The results are stored in the candidate planes.
     * 
     * @param meshSamples Downsampled mesh
     * @param centroid Centroid of the downsampled mesh
     * @param candidates Candidate planes
     */
    @Override
    protected void measureSymmetry(List<MeshPoint> meshSamples, Point3d centroid, Set<CandidatePlaneRobust> candidates) {
        ProcessedCloud cache = new ProcessedCloud(meshSamples, centroid);
        double alpha = 15.0 / cache.avgDistance;
        UniformGrid3d<MeshPoint> samplesGrid = UniformGrid3d.create(2.6 / alpha, cache.points, MeshPoint::getPosition);
        candidates.parallelStream().forEach(plane -> ((CandidatePlaneRobustMesh) plane).measureWeightedSymmetry(cache, samplesGrid, alpha));
    }
    
    /********************************************************************
     * A helper class that copies input mesh point and moves them 
     * so that the given centroid is in the space origin.
     * Also, it computes some additional data
     * 
     * @author Radek Oslejsek
     */
    public static class ProcessedCloud {
        
        public static final double AVG_CUR_MAGIC_MULTIPLIER = 0.01;

        private final List<MeshPoint> points;
        private double avgDistance;
        
        /**
         * The average of absolute values of Gaussian curvatures in all points
         */
        private double avgCurvature;

        /**
         * Moves orig points so that the centroid is in the space origin, copies
         * them into a new list. Also, computes the average distance of orig
         * points to the centroid (= average distance of shifted points into the
         * space origin).
         *
         * @param centroid Centroid.
         * @param points Original mesh point
         */
        public ProcessedCloud(List<MeshPoint> points, Point3d centroid) {
            int size = points.size();
            this.points = new ArrayList<>(size);
            
            int counter = 0;
            for (int i = 0; i < size; i++) {
                MeshPoint mp = points.get(i);
                Point3d p = new Point3d(mp.getPosition());
                p.sub(centroid);
                this.points.add(MeshFactory.createMeshPoint(p, mp.getNormal(), mp.getTexCoord(), mp.getCurvature()));
                avgDistance += Math.sqrt(p.x * p.x + p.y * p.y + p.z * p.z) / size;
                
                if (mp.getCurvature() != null && !Double.isNaN(mp.getCurvature().gaussian())) {
                    avgCurvature += Math.abs(mp.getCurvature().gaussian());
                    counter++;
                }
            }
            
            if (counter != 0) {
                avgCurvature /= counter;
            } else {
                avgCurvature = Double.NaN;
            }
        }
        
        /**
         * Returns similarity of Gaussian curvature.
         * 
         * @param p1 First point
         * @param p2 Second point
         * @return similarity of Gaussian curvature.
         */
        public double getCurvatureSimilarity(MeshPoint p1, MeshPoint p2) {
            double avgh = avgCurvature * AVG_CUR_MAGIC_MULTIPLIER;

            if (p1.getCurvature() == null || p2.getCurvature() == null) {
                return 0;
            }

            double g1 = p1.getCurvature().gaussian();
            double g2 = p2.getCurvature().gaussian();
            
            if (Double.isNaN(g1) || Double.isNaN(g2)) {
                return 0;
            }

            if (g1 * g1 <= 0) {
                return 0;
            }

            if (Math.abs(g1) < avgh) {
                return 0;
            }

            if (Math.abs(g2) < avgh) {
                return 0;
            }

            double weight = Math.min(Math.abs(g1), Math.abs(g2)) / Math.max(Math.abs(g1), Math.abs(g2));

            if (weight <= 0) {
                return 0;
            }

            return weight;
        }
        
        /**
         * Returns i-th transformed point
         * @param i index
         * @return i-th transformed point
         */
        public MeshPoint getPoint(int i) {
            return points.get(i);
        }
        
        /**
         * Returns the number of points.
         * @return the number of points.
         */
        public int getNumPoints() {
            return points.size();
        }
        
        public double getAvgCurvature() {
            return this.avgCurvature;
        }
        
        public double getAvgDistance() {
            return this.avgDistance;
        }
        
    }        
}
