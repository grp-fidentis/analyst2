package cz.fidentis.analyst.engines.glyphs.impl;

import cz.fidentis.analyst.data.mesh.Curvature;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Glyph;
import cz.fidentis.analyst.engines.curvature.CurvatureServices;
import cz.fidentis.analyst.engines.glyphs.GlyphsVisitor;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;
import cz.fidentis.analyst.engines.sampling.PointSamplingVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of glyphs visitor interface.
 *
 * @author Ondrej Simecek
 */
public class GlyphsVisitorImpl implements GlyphsVisitor {

    private final List<MeshFacet> facets = new ArrayList<>();
    private final PointSamplingVisitor samplingVisitor;

    /**
     * Constructor.
     * @param samplingConfig Sub-sampling configuration. Must not be {@code null}
     */
    public GlyphsVisitorImpl(PointSamplingConfig samplingConfig) {
        this.samplingVisitor = samplingConfig.getVisitor();
    }

    @Override
    public void visitMeshFacet(MeshFacet facet) {
        facets.add(facet);
        samplingVisitor.visitMeshFacet(facet);
    }

    @Override
    public List<Glyph> getGlyphs() {
        List<Glyph> glyphs = new ArrayList<>();
        for (MeshPoint sample : this.samplingVisitor.getSamples()) {
            Curvature curvature = CurvatureServices.compute(sample, facets);
            Glyph glyph = (curvature == null)
                    ? new Glyph(sample.getPosition(), sample.getNormal())
                    : new Glyph(sample.getPosition(), sample.getNormal(), curvature.maxCurvatureDir(), curvature.minCurvatureDir());
            glyphs.add(glyph);
        }
        return glyphs;
    }

    @Override
    public void dispose(){
        samplingVisitor.dispose();
    }
}
