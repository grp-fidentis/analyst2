package cz.fidentis.analyst.engines.icp;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.icp.impl.IcpVisitorImpl;
import cz.fidentis.analyst.engines.sampling.PointSamplingConfig;

import java.util.Collection;

/**
 * ICP strategies and their configuration.
 *
 * @author Radek Oslejsek
 *
 * @param kdTree The k-d tree of the primary mesh. Must not be {@code null}.
 *        Inspected facets are transformed toward this primary mesh.
 * @param maxIteration Maximal number of ICP iterations (it includes computing
 *        new transformation and applying it). A number bigger than zero.
 *        Reasonable number seems to be 10.
 * @param scale If {@code true}, then the scale factor is also computed.
 * @param error Acceptable error - a number bigger than or equal to zero.
 *        Mean distance of vertices is computed for each ICP iteration.
 *        If the difference between the previous and current mean distances is less than the error,
 *        then the ICP computation stops. Reasonable number seems to be 0.05.
 * @param strategy One of the reduction strategies. If {@code null}, then no sub-sampling is used.
 * @param crop The iteration number from which partially overlapped parts of surfaces are omitted from distance minimization
 *             (i.e. the faces are auto-cropped for the ICP step). Set the parameter to zero to turn the auto-cropping
 *             feature on for all ICP iterations. Set the parameter to any negative number to disable the auto-cropping
 *             feature. In general, use auto-cropping if the faces are pre-aligned. If unsure, use value 1.
 */
public record IcpConfig(KdTree kdTree, int maxIteration, boolean scale, double error, PointSamplingConfig strategy, int crop) {

    /**
     * Constructor that constructs k-D tree of th mesh model automatically.
     *
     * @param meshModel Mesh model towards which other meshes will be registered
     * @param maxIteration Max iterations
     * @param scale Whether to scale
     * @param error Error factor
     * @param strategy ICP strategy
     * @param crop Whether to crop
     */
    public IcpConfig(MeshModel meshModel, int maxIteration, boolean scale, double error, PointSamplingConfig strategy, int crop) {
        this(meshModel.getFacets(), maxIteration, scale, error, strategy, crop);
    }

    /**
     * Constructor that constructs k-D tree of th mesh model automatically.
     *
     * @param meshFacets Mesh facets towards which other meshes will be registered
     * @param maxIteration Max iterations
     * @param scale Whether to scale
     * @param error Error factor
     * @param strategy ICP strategy
     * @param crop Whether to crop
     */
    public IcpConfig(Collection<MeshFacet> meshFacets, int maxIteration, boolean scale, double error, PointSamplingConfig strategy, int crop) {
        this(KdTree.create(meshFacets), maxIteration, scale, error, strategy, crop);
    }

    /**
     * Instantiates and returns a point sampling visitor.
     *
     * @return a point sampling visitor
     */
    public IcpVisitor getVisitor() {
        return new IcpVisitorImpl(kdTree, maxIteration, scale, error, strategy, crop);
    }
}
