package cz.fidentis.analyst.engines.cut;

import cz.fidentis.analyst.data.mesh.MeshVisitor;
import cz.fidentis.analyst.data.shapes.CrossSection3D;

/**
 * Stateful computation of a cross-sections.
 * First, the visitor is created. Then, it is applied to mesh facets. Finally, the samples are returned.
 *
 * @author Radek Oslejsek
 */
public interface CrossSectionVisitor extends MeshVisitor {

    /**
     * Returns computed cross-section curve.
     *
     * @return cross-section curve
     */
    CrossSection3D getCrossSectionCurve();
}
