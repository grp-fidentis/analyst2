package cz.fidentis.analyst.engines.distance;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;

import java.util.Collection;
import java.util.Collections;

/**
 * Stateless services for mesh-to-mesh distance measurement.
 * It is a wrapper to the stateful implementation of {@link cz.fidentis.analyst.engines.distance.MeshDistanceVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface MeshDistanceServices {

    /**
     * Returns distances between the source mesh (stored in the configuration object) and meshes provided as
     * input parameter. <b>The measurement is one-directional.</b> It means that only the distance from the source
     * mesh to target mesh(es) is estimated. If the distance strategy is asymmetric, e.g., the distance based
     * on nearest neighbours or ray-casting, then the measurement should be called also in the opposite direction
     * and the results should be <a href="https://doi.org/10.1109/ICPR.1994.576361">combined to achieve better accuracy</a>.
     *
     * @param facets Meshes towards which the distance is measured
     * @param config Configuration of the service. It includes the source mesh.
     * @return Distance measurement
     */
    static MeshDistances measure(Collection<MeshFacet> facets, MeshDistanceConfig config) {
        var visitor = config.getVisitor();
        facets.forEach(facet -> facet.accept(visitor));
        visitor.dispose();
        return visitor.getDistancesOfVisitedFacets();
    }

    /**
     * Returns distances between the source mesh (stored in the configuration object) and the mesh provided as
     * input parameter. <b>The measurement is one-directional.</b> It means that only the distance from the source
     * mesh to target mesh(es) is estimated. If the distance strategy is asymmetric, e.g., the distance based
     * on nearest neighbours or ray-casting, then the measurement should be called also in the opposite direction
     * and the results should be <a href="https://doi.org/10.1109/ICPR.1994.576361">combined to achieve better accuracy</a>.
     *
     * @param facet Mesh towards which the distance is measured
     * @param config Configuration of the service. It includes the source mesh.
     * @return Distance measurement
     */
    static MeshDistances measure(MeshFacet facet, MeshDistanceConfig config) {
        return measure(Collections.singleton(facet), config);
    }

    /**
     * Returns distances between the source mesh (stored in the configuration object) and the mesh model provided as
     * input parameter. <b>The measurement is one-directional.</b> It means that only the distance from the source
     * mesh to target mesh(es) is estimated. If the distance strategy is asymmetric, e.g., the distance based
     * on nearest neighbours or ray-casting, then the measurement should be called also in the opposite direction
     * and the results should be <a href="https://doi.org/10.1109/ICPR.1994.576361">combined to achieve better accuracy</a>.
     *
     * @param meshModel Mesh model towards which the distance is measured
     * @param config Configuration of the service. It includes the source mesh.
     * @return Distance measurement
     */
    static MeshDistances measure(MeshModel meshModel, MeshDistanceConfig config) {
        return measure(meshModel.getFacets(), config);
    }
}
