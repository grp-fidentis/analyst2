package cz.fidentis.analyst.engines.distance;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.engines.distance.impl.MeshDistanceNN;
import cz.fidentis.analyst.engines.distance.impl.MeshDistanceRC;
import cz.fidentis.analyst.engines.distance.impl.MeshDistanceRCGPU;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * Mesh-to-mesh distance strategies and their configuration.
 *
 * @author Daniel Schramm
 * @author Radek Oslejsek
 *
 * @param method Distance calculation method
 * @param kdTree K-d tree of the mesh to which distance from other meshes is to be computed.
 *               Either this parameter or the {@code octree} must not be {@code null}, depending on the method used.
 * @param octree Octree of the mesh to which distance from other meshes is to be computed.
 *               Either this parameter or the {@code kdTree} must not be {@code null}, depending on the method used.
 * @param gpuData Wrapper with data for GPU computing, can be {@code null} if you do not want to use GPU-based method.
 * @param relativeDist If true, then the visitor calculates the relative distances with respect
 *                     to the normal vectors of source facets (normal vectors have to present)
 *                     i.e., we can get negative distances.
 * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
 *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
 *             (their distance is set to {@code NaN}).
 *             This feature makes the nearest-neighbor distance computation more symmetric.
 *             On the contrary, this parameter is ignored by ray-casting methods.
 */
public record MeshDistanceConfig(Method method, KdTree kdTree, Octree octree, GPUData gpuData, boolean relativeDist, boolean crop) {

    /**
     * Wrapper with data for GPU computing.
     *
     * @param facets Facets of primary face.
     * @param context Active OpenGL context on which makeCurrent() can be called.
     *
     * @author Pavol Kycina
     */
    public record GPUData(Collection<MeshFacet> facets, GLContext context) {}

    /**
     * Constructor for the nearest-neighbor distance methods only, when the k-D tree already exists.
     *
     * @param method Distance calculation method
     * @param kdTree K-d tree of the mesh to which distance from other meshes is to be computed.
     *               Must not be {@code null}
     * @param relativeDist If true, then the visitor calculates the relative distances with respect
     *                     to the normal vectors of source facets (normal vectors have to present)
     *                     i.e., we can get negative distances.
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the nearest-neighbor distance computation more symmetric.
     *             On the contrary, this parameter is ignored by ray-casting methods.
     */
    public MeshDistanceConfig(Method method, KdTree kdTree, boolean relativeDist, boolean crop) {
        this(method, kdTree, null, null, relativeDist, crop);
        if (!(method.equals(Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS) || method.equals(Method.POINT_TO_POINT_NEAREST_NEIGHBORS))) {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Constructor for the ray-casting distance methods only, when the octree already exists.
     *
     * @param method Distance calculation method
     * @param octree An octree of the mesh to which distance from other meshes is to be computed.
     *               Must not be {@code null}
     * @param relativeDist If true, then the visitor calculates the relative distances with respect
     *                     to the normal vectors of source facets (normal vectors have to present)
     *                     i.e., we can get negative distances.
     */
    public MeshDistanceConfig(Method method, Octree octree, boolean relativeDist) {
        this(method, null, octree, null, relativeDist, false /* ignored */);
        if (!method.equals(Method.RAY_CASTING)) {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Constructor that creates k-d tree or octree automatically based on required method.
     *
     * @param method Distance calculation method
     * @param mainFacets Facets to which distance from other facets is to be computed. Must not be {@code null}
     * @param context Active OpenGL context on which makeCurrent() can be called.
     * @param relativeDist If true, then the visitor calculates the relative distances with respect
     *                     to the normal vectors of source facets (normal vectors have to present)
     *                     i.e., we can get negative distances.
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the distance computation more symmetric.
     *             On the contrary, this parameter is ignored by ray-casting methods.
     */
    public MeshDistanceConfig(Method method, Collection<MeshFacet> mainFacets, GLContext context, boolean relativeDist, boolean crop) {
        this(method,
                method.equals(Method.RAY_CASTING) || method.equals(Method.RAY_CASTING_GPU) ? null : KdTree.create(mainFacets),
                method.equals(Method.RAY_CASTING) ? Octree.create(mainFacets) : null,
                method.equals(Method.RAY_CASTING_GPU) ? new GPUData(mainFacets, context) : null, relativeDist,
                crop);
    }

    /**
     * Constructor that creates k-d tree or octree automatically based on required method.
     *
     * @param method Distance calculation method
     * @param mainFacet Facet to which distance from other facets is to be computed. Must not be {@code null}
     * @param context Active OpenGL context on which makeCurrent() can be called.
     * @param relativeDist If true, then the visitor calculates the relative distances with respect
     *                     to the normal vectors of source facets (normal vectors have to present)
     *                     i.e., we can get negative distances.
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the distance computation more symmetric.
     *             On the contrary, this parameter is ignored by ray-casting methods.
     */
    public MeshDistanceConfig(Method method, MeshFacet mainFacet, GLContext context, boolean relativeDist, boolean crop) {
        this(method, Collections.singleton(mainFacet), context, relativeDist, crop);
    }

    /**
     * Constructor that creates k-d tree or octree automatically based on required method.
     *
     * @param method Distance calculation method
     * @param mainModel Mesh model to which distance from other facets is to be computed. Must not be {@code null}
     * @param context Active OpenGL context on which makeCurrent() can be called.
     * @param relativeDist If true, then the visitor calculates the relative distances with respect
     *                     to the normal vectors of source facets (normal vectors have to present)
     *                     i.e., we can get negative distances.
     * @param crop If {@code true}, then only parts of the visited secondary faces that overlay the primary face are
     *             taken into account. Parts (vertices) that are out of the surface of the primary face are ignored
     *             (their distance is set to {@code NaN}).
     *             This feature makes the distance computation more symmetric.
     *             On the contrary, this parameter is ignored by ray-casting methods.
     */
    public MeshDistanceConfig(Method method, MeshModel mainModel, GLContext context, boolean relativeDist, boolean crop) {
        this(method, new HashSet<>(mainModel.getFacets()), context, relativeDist, crop);
    }

    /**
     * Instantiates and returns a point sampling visitor.
     *
     * @return a point sampling visitor
     */
    public MeshDistanceVisitor getVisitor() {
        return switch (method) {
            case POINT_TO_TRIANGLE_NEAREST_NEIGHBORS -> new MeshDistanceNN(kdTree, false, relativeDist, true, crop);
            case POINT_TO_POINT_NEAREST_NEIGHBORS -> new MeshDistanceNN(kdTree, true, relativeDist, true, crop);
            case RAY_CASTING -> new MeshDistanceRC(octree, relativeDist, true);
            case RAY_CASTING_GPU -> new MeshDistanceRCGPU(gpuData.context, gpuData.facets, relativeDist);
        };
    }

    /**
     * Sampling method.
     *
     * @author Radek Oslejsek
     */
    public enum Method {
        /**
         * A point-to-point algorithm. It can produce "noise". If there are multiple closest points
         * at the primary mesh, then it is not possible to choose which of them is correct (better).
         * Therefore, the {@code Double.POSITIVE_INFINITY} value is set as the distance for this vertices.
         * If this situation happens with human faces, we would consider this part
         * of the face as noisy anyway, trying to avoid these parts from further processing.
         */
        POINT_TO_TRIANGLE_NEAREST_NEIGHBORS,

        /**
         * Fast point-to-triangle distance strategy. Produces more precise results
         * than the point-to-point strategy, but is not geometrically correct. Can produce "noise"
         * in the sense that it may not find the really closest triangle on "wrinkly" surfaces.
         * If this situation happens with human faces, we would consider this part
         * of the face as noisy anyway, trying to avoid these parts from further processing.
         * The algorithm supports both relative and absolute distances.
         */
        POINT_TO_POINT_NEAREST_NEIGHBORS,

        /**
         * Casting rays from visited meshes in the direction of their normal vectors.
         */
        RAY_CASTING,

        /**
         * Casting rays from visited meshes in the direction of their normal vectors.
         * Runs on GPU. Available only for systems with OpenGL >= 4.3
         */
        RAY_CASTING_GPU,
    }
}
