package cz.fidentis.analyst.engines.glyphs;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.shapes.Glyph;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Stateless services for glyphs.
 * It is a wrapper to the stateful implementation in {@link cz.fidentis.analyst.engines.glyphs.GlyphsVisitor}.
 *
 * @author Radek Oslejsek
 */
public interface GlyphServices {

    /**
     * Sub-samples the mesh and creates properly oriented glyphs on sample locations.
     *
     * @param facet Mesh surface
     * @param config Configuration of the service
     * @return list of {@code Glyph}s
     */
    static List<Glyph> calculateGlyphSamples(MeshFacet facet, GlyphsConfig config) {
        return calculateGlyphs(Collections.singleton(facet), config);
    }

    /**
     * Sub-samples the mesh and creates properly oriented glyphs on sample locations.
     *
     * @param mesh Mesh surface
     * @param config Configuration of the service
     * @return list of {@code Glyph}s
     */
    static List<Glyph> calculateGlyphs(MeshModel mesh, GlyphsConfig config) {
        return calculateGlyphs(mesh.getFacets(), config);
    }

    /**
     * Sub-samples the mesh and creates properly oriented glyphs on sample locations.
     *
     * @param facets Mesh surface
     * @param config Configuration of the service
     * @return list of {@code Glyph}s
     */
    static List<Glyph> calculateGlyphs(Collection<MeshFacet> facets, GlyphsConfig config) {
        var visitor = config.getVisitor();
        facets.forEach(facet -> visitor.visitMeshFacet(facet));
        List<Glyph> glyphs = visitor.getGlyphs();
        visitor.dispose();
        return glyphs;
    }
}
