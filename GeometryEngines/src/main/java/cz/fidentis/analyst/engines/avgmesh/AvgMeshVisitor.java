package cz.fidentis.analyst.engines.avgmesh;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.kdtree.KdTreeVisitor;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshVisitor;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.octree.OctreeVisitor;

/**
 * An interface declaring the ability to deform a template mesh into an average mesh based on visiting other meshes.
 * Specific implementations are usually able to visit only one polygonal structure, either k-d tree, octree, or mesh.
 * Therefore, check which of the visiting methods are supported by the respective implementing class.
 *
 * @author Radek Oslejsek
 */
public interface AvgMeshVisitor extends KdTreeVisitor, OctreeVisitor, MeshVisitor {

    /**
     * Returns the averaged mesh model
     *
     * @return averaged mesh model
     */
    MeshModel getAveragedMeshModel();

    @Override
    default boolean isThreadSafe() {
        return false;
    }

    @Override
    default void visitKdTree(KdTree kdTree) {
        throw new UnsupportedOperationException();
    }

    @Override
    default void visitOctree(Octree octree) {
        throw new UnsupportedOperationException();
    }

    @Override
    default void visitMeshFacet(MeshFacet facet) {
        throw new UnsupportedOperationException();
    }
}
