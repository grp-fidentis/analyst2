package cz.fidentis.analyst.engines.curvature;

import cz.fidentis.analyst.data.mesh.MeshVisitor;

/**
 * Stateful computation of curvature.
 * First, the visitor is created. Then, it is applied to mesh facets. Finally, the samples are returned.
 * <p>
 *  The visitor returns nothing. Therefore, there is no "get result" method available.
 *  It just sets the curvature values of inspected meshes.
 *  <b>All curvature algorithms suppose that the triangle vertices are oriented clockwise!</b>
 * </p>
 *
 * @author Radek Oslejsek
 */
public interface CurvatureVisitor extends MeshVisitor {
}
