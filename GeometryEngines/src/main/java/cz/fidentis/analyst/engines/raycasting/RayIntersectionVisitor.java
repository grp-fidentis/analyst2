package cz.fidentis.analyst.engines.raycasting;

import cz.fidentis.analyst.data.octree.OctreeVisitor;
import cz.fidentis.analyst.data.ray.RayIntersection;

import java.util.List;
import java.util.SortedSet;

/**
 * Stateful ray casting.
 * First, the visitor is created. Then, it is applied to mesh facets. Finally, the samples are returned.
 *
 * @author Radek Oslejsek
 */
public interface RayIntersectionVisitor extends OctreeVisitor {

    /**
     * Returns ray intersections <strong>in random order and without distances</strong>.
     *
     * @param filter If {@code true}, then only triangles with the same orientation are taken into account
     * @return ray intersections without distances.
     */
    List<RayIntersection> getUnsortedIntersections(boolean filter);

    /**
     * Returns found ray intersections <strong>sorted by distances</strong>.
     *
     * @param filter If {@code true}, then only triangles with the same orientation are taken into account
     * @return found ray intersections ordered by distances.
     */
    SortedSet<RayIntersection> getSortedIntersections(boolean filter);

    /**
     * Returns a closets intersection point, either positive or negative, or {@code null}.
     *
     * @param filter If {@code true}, then only triangles with the same orientation are taken into account
     * @return a closets intersection point, either positive or negative, or {@code null}.
     */
    RayIntersection getClosestIntersection(boolean filter);
}
