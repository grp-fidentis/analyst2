package cz.fidentis.analyst.engines.avgmesh.impl;

import com.jogamp.opengl.GLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.engines.distance.MeshDistanceConfig;
import cz.fidentis.analyst.engines.distance.MeshDistanceVisitor;
import cz.fidentis.analyst.data.mesh.measurement.FacetDistances;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * GPU accelerated {@link AvgMeshVisitorRC}. In contrast to its CPU-predecessor,
 * this implementation is capable of visiting mesh facets only (not octrees).
 *
 * @author Pavol Kycina
 */
public class AvgMeshVisitorRCGPU extends AvgMeshVisitorRC {

    private final GLContext glContext;

    /**
     * Constructor.
     *
     * @param templateFacet Mesh facet which is transformed to the averaged mesh.
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @param glContext OpenGL context. Must not be {@code null}
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorRCGPU(MeshFacet templateFacet, GLContext glContext) {
        super(templateFacet);
        this.glContext = Objects.requireNonNull(glContext);
    }

    /**
     * Constructor.
     *
     * @param templateFacets Mesh facets that are transformed to the averaged mesh
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @param glContext OpenGL context. Must not be {@code null}
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorRCGPU(Collection<MeshFacet> templateFacets, GLContext glContext) {
        super(templateFacets);
        this.glContext = Objects.requireNonNull(glContext);
    }

    /**
     * Constructor.
     *
     * @param templateModel Mesh model which is transformed to the averaged mesh
     *        The original mesh remains unchanged. New mesh is allocated instead.
     * @param glContext OpenGL context. Must not be {@code null}
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public AvgMeshVisitorRCGPU(MeshModel templateModel, GLContext glContext) {
        super(templateModel);
        this.glContext = Objects.requireNonNull(glContext);
    }

    @Override
    public void visitOctree(Octree octree) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void visitMeshFacet(MeshFacet facet) {
        //setAvgMeshModel(null);
        setNumInspectedFacets(getNumInspectedFacets()+1);

        MeshDistanceVisitor distVisitor = new MeshDistanceConfig(MeshDistanceConfig.Method.RAY_CASTING_GPU,
                facet, glContext, true, true).getVisitor();
        getTransformations().keySet().forEach(distVisitor::visitMeshFacet);
        distVisitor.dispose();

        for (MeshFacet mainFacet : getTransformations().keySet()) {
            FacetDistances distances = distVisitor.getDistancesOfVisitedFacets().getFacetMeasurement(mainFacet);
            List<Double> transformation = getTransformations().get(mainFacet);

            for (int i = 0; i < mainFacet.getNumberOfVertices(); i++) {
                double calculatedDist = distances.get(i).getDistance();
                if (!Double.valueOf(calculatedDist).isNaN() && !Double.valueOf(calculatedDist).isInfinite()) {
                    synchronized (this) {
                        transformation.set(i, transformation.get(i) + calculatedDist);
                    }
                }
            }
        }
    }


}
