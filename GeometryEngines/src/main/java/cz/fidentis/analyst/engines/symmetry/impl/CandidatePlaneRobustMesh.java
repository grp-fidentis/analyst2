package cz.fidentis.analyst.engines.symmetry.impl;

import cz.fidentis.analyst.data.grid.UniformGrid3d;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.symmetry.impl.SymmetryVisitorMesh.ProcessedCloud;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;

/**
 * This symmetry plane changes the behavior so that similarity of Gaussian
 * curvature values and the symmetry of normal vectors are used to check the
 * quality of the symmetry plane.
 *
 * @author Radek Oslejsek
 */
public class CandidatePlaneRobustMesh extends CandidatePlaneRobust {
    
    /**
     * New candidate symmetry plane constructed from two points.
     *
     * @param point1 point in space
     * @param point2 point in space
     * @param avgDist the average distance between vertices and their centroid
     * @throws IllegalArgumentException if the @code{plane} argument is null
     */
    public CandidatePlaneRobustMesh(Point3d point1, Point3d point2, double avgDist) {
        super(point1, point2, avgDist);
    }

    /**
     * Symmetry measurement based on the similarity of Gaussian curvatures and
     * the symmetry of normal vectors, in addition to Wendland’s similarity function.<b>The plane is normalized and {@code numAvg} set to zero!</b>
     *
     * @param cache Precomputed values, including downsampled points
     * @param grid The same cloud stored in the uniform grid
     * @param alpha the average distance between vertices and their centroid
     */
    public void measureWeightedSymmetry(ProcessedCloud cache, UniformGrid3d<MeshPoint> grid, double alpha) {
        normalizeIfNeeded();
        setSymmetryMeasure(0.0);

        for (int i = 0; i < cache.getNumPoints(); i++) {
            MeshPoint p1 = cache.getPoint(i); // original point
            Point3d rp1 = getPlane().reflectPointOverPlane(p1.getPosition()); // reflected point
            Vector3d rn1 = getPlane().reflectUnitVectorOverPlane(p1.getNormal()); // reflected normal
            List<MeshPoint> closest = grid.getClosest(rp1);

            for (MeshPoint p2 : closest) {
                if (p1 == p2) {
                    continue;
                }

                double ws = cache.getCurvatureSimilarity(p1, p2);
                if (ws == 0) {
                    continue;
                }

                double wd = getNormalVectorsWeight(rn1, p2.getNormal());
                if (wd == 0) {
                    continue;
                }

                double phi = similarityFunction(rp1.distance(p2.getPosition()), alpha);
                if (phi == 0) {
                    continue;
                }

                setSymmetryMeasure(getSymmetryMeasure() + ws * wd * phi); // increment the measure
            }
        }
    }
    
    /**
     * Returns the symmetry of normal vectors
     * 
     * @param rv1 A normal vector already reflected over this plane
     * @param v2 A second normal vector for comparison
     * @return the symmetry of the given normal vectors
     */
    public static double getNormalVectorsWeight(Vector3d rv1, Vector3d v2) {
        double cosN = rv1.dot(v2);
        cosN = (cosN >= 0) ? Math.min(1.0, cosN) : Math.max(-1.0, cosN);
        double acos = Math.acos(cosN);
        return similarityFunction(acos, 4.0);
    }
}
