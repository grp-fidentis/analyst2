package cz.fidentis.analyst.engines.distance;

import com.jogamp.opengl.*;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.mesh.measurement.FacetDistances;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIf;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests GPU distance measurement. Runs only when GL capable window manager is present.
 * Results are compared to reference CPU implementation with defined error - note floats vs doubles.
 *
 * @author Pavol Kycina
 */
public class MeshDistanceRCGPUTest {

    /** this tells how much the GPU calculated distance can be different from CPU distance (note GPU floats vs CPU doubles)*/
    private static final double ALLOWED_DISTANCE_ERROR = 0.01;

    protected static MeshFacet getTrivialTwoTriangleFacet(double offsetZ, double size) {
        MeshFacet facet = MeshFactory.createEmptyMeshFacet();

        // these 6 points create one square with two triangles, no vertex sharing
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-size, -size, offsetZ), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(size, -size, offsetZ), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-size, size, offsetZ), new Vector3d(0, 0, 1), new Vector3d(), null));

        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-size, size, offsetZ), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(size, -size, offsetZ), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-size, size, offsetZ), new Vector3d(0, 0, 1), new Vector3d(), null));

        facet.getCornerTable().addRow(new CornerTableRow(0, -1));
        facet.getCornerTable().addRow(new CornerTableRow(1, -1));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));

        facet.getCornerTable().addRow(new CornerTableRow(3, -1));
        facet.getCornerTable().addRow(new CornerTableRow(4, -1));
        facet.getCornerTable().addRow(new CornerTableRow(5, -1));

        return facet;
    }

    @Test
    @EnabledIf("isOpenGLAvailable")
    public void gpuDistanceTest() {
        GLAutoDrawable drawable = createDummyGLContext();
        GLContext context = drawable.getContext();

        // idea is that rays are shot from the smaller two triangles to the bigger two triangles ib the background
        // which should definitely result into intersections
        MeshFacet smallFacet = getTrivialTwoTriangleFacet(0, 1);
        MeshFacet normalFacet = getTrivialTwoTriangleFacet(1, 10);

        // CPU visit
        MeshDistanceVisitor cpuVisitor = new MeshDistanceConfig(MeshDistanceConfig.Method.RAY_CASTING, normalFacet, null, true, true).getVisitor();
        smallFacet.accept(cpuVisitor);
        FacetDistances referenceDistances = cpuVisitor.getDistancesOfVisitedFacets().getFacetMeasurement(smallFacet);

        // GPU visit
        MeshDistanceVisitor gpuVisitor = new MeshDistanceConfig(MeshDistanceConfig.Method.RAY_CASTING_GPU, normalFacet, context, true, true).getVisitor();
        smallFacet.accept(gpuVisitor);
        FacetDistances gpuDistances = gpuVisitor.getDistancesOfVisitedFacets().getFacetMeasurement(smallFacet);

        assertTrue(areEqual(referenceDistances, gpuDistances));

        gpuVisitor.dispose();
        drawable.destroy();
    }

    /**
     * Tests two distance measurements if they have the same values with defined error.
     * @param cpuDistances first values
     * @param gpuDistances second values
     * @return true if they are the same (with error), false otherwise
     */
    private static boolean areEqual(FacetDistances cpuDistances, FacetDistances gpuDistances) {
        if(cpuDistances.size() != gpuDistances.size()) {
            return false;
        }

        int matches = 0;
        for (int i = 0; i < cpuDistances.size(); i++) {
            double dGPU = cpuDistances.get(i).getDistance();
            double dCPU = gpuDistances.get(i).getDistance();

            // round the numbers because GPU uses floats
            if((Double.isInfinite(dGPU) && Double.isInfinite(dCPU)) || Math.abs(dGPU - dCPU) < ALLOWED_DISTANCE_ERROR) {
                matches++;
            }else {
                // uncomment for debugging
                //System.out.println("NOT MATCHING: (gpu) " + dGPU + " (cpu) " + dCPU + " i: " + i);
            }
        }
        return cpuDistances.size() == matches;
    }

    /**
     * Creates dummy GL drawable with its context. Available only on platforms with window manager.
     * @return initialized GL context
     */
    private static GLAutoDrawable createDummyGLContext() {
        GLProfile glProfile = GLProfile.getDefault();
        GLCapabilities glCapabilities = new GLCapabilities(glProfile);
        GLAutoDrawable drawable = GLDrawableFactory.getFactory(glProfile).createOffscreenAutoDrawable(null, glCapabilities, null, 1, 1);
        drawable.display(); // initialize context
        return drawable;
    }

    private static boolean isOpenGLAvailable() {
        try {
            GLProfile glProfile = GLProfile.getDefault();
            return glProfile != null;
        }catch (java.lang.UnsatisfiedLinkError ignored) {
            return false;
        }
    }
}
