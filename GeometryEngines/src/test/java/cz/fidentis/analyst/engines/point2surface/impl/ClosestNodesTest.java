package cz.fidentis.analyst.engines.point2surface.impl;

import cz.fidentis.analyst.data.kdtree.KdNode;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.kdtree.impl.KdNodeImpl;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author Daniel Schramm
 */
public class ClosestNodesTest {
    
    private final Point3d centerNode = new Point3d(0, 0, 0);
    
    private Set<MeshPoint> getTreeNodesSymmetric(double size) {
        final Set<MeshPoint> treeNodes = new HashSet<>();
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d( size,  size,  size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d(-size,  size,  size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d( size, -size,  size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d(-size, -size,  size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d( size,  size, -size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d(-size,  size, -size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d( size, -size, -size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        treeNodes.add(MeshFactory.createMeshPoint(new Point3d(-size, -size, -size), new Vector3d(0, 0, 0), new Vector3d(0, 0, 0), null));
        
        return treeNodes;
    }
    
    private void testContainsExactlyPositions(Set<MeshPoint> expected, Set<KdNode> actual) {
        final Set<KdNode> actualCopy = new HashSet<>(actual);
        for (final MeshPoint point: expected) {
            final Point3d position = point.getPosition();
            boolean found = false;
            for (final KdNode node: actual) {
                if (position.equals(node.getLocation())) {
                    found = true;
                    actualCopy.remove(node);
                }
            }
            assertTrue(found, position.toString());
        }
        assertTrue(actualCopy.isEmpty());
    }
    
    @Test
    public void allNodesClosestTest() {
        final Set<MeshPoint> closest = getTreeNodesSymmetric(1);
        final KdTree kdTree = KdTree.create(closest);
        
        final ClosestNodesImpl visitor = new ClosestNodesImpl(centerNode);
        kdTree.accept(visitor);
        
        final Set<KdNode> closestFound = visitor.getClosestNodes();
        assertEquals(8, closestFound.size());
        testContainsExactlyPositions(closest, closestFound);
    }
    
    @Test
    public void singleClosestNodeTest() {
        final MeshPoint closest = MeshFactory.createMeshPoint(new Point3d(1, 1, 0), new Vector3d(0, 0, 1), new Vector3d(), null);
        final Set<MeshPoint> treeNodes = getTreeNodesSymmetric(1);
        treeNodes.add(closest);
        final KdTree kdTree = KdTree.create(treeNodes);
        
        final ClosestNodesImpl visitor = new ClosestNodesImpl(centerNode);
        kdTree.accept(visitor);
        
        final Set<KdNode> closestFound = visitor.getClosestNodes();
        assertEquals(1, closestFound.size());
        assertEquals(closest.getPosition(), ((KdNodeImpl) closestFound.toArray()[0]).getLocation());
    }
    
    @Test
    public void closestInBothSubtreesTest() {
        final Set<MeshPoint> closest = getTreeNodesSymmetric(1);
        final Set<MeshPoint> treeNodes = new HashSet<>(closest);
        treeNodes.addAll(getTreeNodesSymmetric(2));
        assertEquals(16, treeNodes.size());
        final KdTree kdTree = KdTree.create(treeNodes);
        
        final ClosestNodesImpl visitor = new ClosestNodesImpl(centerNode);
        kdTree.accept(visitor);
        
        final Set<KdNode> closestFound = visitor.getClosestNodes();
        assertEquals(8, closestFound.size());
        testContainsExactlyPositions(closest, closestFound);
    }
    
    @Test
    public void manyPointsEightClosestTest() {
        final Set<MeshPoint> closest = getTreeNodesSymmetric(1);
        final Set<MeshPoint> treeNodes = new HashSet<>(closest);
        for (int i = 2; i < 100; i++) {
            treeNodes.addAll(getTreeNodesSymmetric(i));
        }
        assertEquals(8 * 99, treeNodes.size());
        final KdTree kdTree = KdTree.create(treeNodes);
        
        final ClosestNodesImpl visitor = new ClosestNodesImpl(centerNode);
        kdTree.accept(visitor);
        
        final Set<KdNode> closestFound = visitor.getClosestNodes();
        assertEquals(8, closestFound.size());
        testContainsExactlyPositions(closest, closestFound);
    }
    
    @Test
    public void threeTreesSingleClosestTest() {
        final Set<MeshPoint> closest = getTreeNodesSymmetric(1);
        final KdTree kdTreeClosest = KdTree.create(closest);
        
        final KdTree kdTree1 = KdTree.create(getTreeNodesSymmetric(2));
        final KdTree kdTree2 = KdTree.create(getTreeNodesSymmetric(3));
        
        final ClosestNodesImpl visitor = new ClosestNodesImpl(centerNode);
        kdTree1.accept(visitor);
        kdTreeClosest.accept(visitor);
        kdTree2.accept(visitor);
        
        final Set<KdNode> closestFound = visitor.getClosestNodes();
        assertEquals(8, closestFound.size());
        testContainsExactlyPositions(closest, closestFound);
    }
    
    @Test
    public void threeTreesTwoClosestTest() {
        final Set<MeshPoint> closest = getTreeNodesSymmetric(1);
        final Set<MeshPoint> closest1 = new HashSet<>();
        final Set<MeshPoint> closest2 = new HashSet<>();
        boolean odd = true;
        for (final MeshPoint point: closest) {
            if (odd) {
                closest1.add(point);
            } else {
                closest2.add(point);
            }
            odd = !odd;
        }
        assertEquals(8, closest.size());
        assertEquals(4, closest1.size());
        assertEquals(4, closest2.size());
        
        final KdTree kdTreeClosest1 = KdTree.create(closest1);
        final KdTree kdTreeClosest2 = KdTree.create(closest2);
        final KdTree kdTree = KdTree.create(getTreeNodesSymmetric(2));
        
        final ClosestNodesImpl visitor = new ClosestNodesImpl(centerNode);
        kdTreeClosest1.accept(visitor);
        kdTree.accept(visitor);
        kdTreeClosest2.accept(visitor);
        
        final Set<KdNode> closestFound = visitor.getClosestNodes();
        assertEquals(8, closestFound.size());
        testContainsExactlyPositions(closest, closestFound);
    }
    
}
