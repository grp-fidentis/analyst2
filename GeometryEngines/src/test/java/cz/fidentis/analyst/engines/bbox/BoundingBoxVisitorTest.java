package cz.fidentis.analyst.engines.bbox;

import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.engines.bbox.impl.BoundingBoxVisitorImpl;
import org.junit.jupiter.api.Test;

import javax.vecmath.Vector3d;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Radek Oslejsek
 */
public class BoundingBoxVisitorTest {
    
    Path testFileDirectory = Paths.get("src", "test", "resources", "cz", "fidentis", "analyst");
    
    @Test
    void icoSphereTest() throws IOException {
        MeshModel m = MeshIO.readMeshModel(new File(testFileDirectory.toFile(), "IcoSphere-20.obj"));
        assertNotNull(m);
        BoundingBoxVisitorImpl visitor = new BoundingBoxVisitorImpl();
        m.compute(visitor);
        assertEquals(new Vector3d(0.8944249749183655, 1.0, 0.8506399989128113), visitor.getBoundingBox().maxPoint());
        assertEquals(new Vector3d(-0.8944249749183655, -1.0, -0.8506399989128113), visitor.getBoundingBox().minPoint());
    }
    
    @Test
    void combinedTest() throws IOException {
        MeshModel m = MeshIO.readMeshModel(new File(testFileDirectory.toFile(), "IcoSphere-20.obj"));
        assertNotNull(m);
        BoundingBoxVisitorImpl visitor = new BoundingBoxVisitorImpl();
        m.compute(visitor);
        
        m = MeshIO.readMeshModel(new File(testFileDirectory.toFile(), "Tetrahedron.obj"));
        assertNotNull(m);
        m.compute(visitor);
        
        assertEquals(new Vector3d(1.0, 1.0, 1.0), visitor.getBoundingBox().maxPoint());
        assertEquals(new Vector3d(-1.0, -1.0, -1.0), visitor.getBoundingBox().minPoint());
    }
}
