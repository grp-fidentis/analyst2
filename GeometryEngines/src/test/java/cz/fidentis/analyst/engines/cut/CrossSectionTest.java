package cz.fidentis.analyst.engines.cut;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.shapes.CrossSection3D;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.cut.impl.CrossSectionVisitorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * @author Dominik Racek
 */
public class CrossSectionTest {

    private MeshModel createModel() {
        MeshModel model = MeshFactory.createEmptyMeshModel();

        MeshFacet facet = MeshFactory.createEmptyMeshFacet();
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0, 0, 0)));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(1, 0, 0)));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(1, 1, 0)));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0, 1, 0)));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(1, 2, 0)));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0, 2, 0)));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(1, 3, 0)));

        facet.getCornerTable().addRow(new CornerTableRow(0, -1));
        facet.getCornerTable().addRow(new CornerTableRow(1, 5));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));
        facet.getCornerTable().addRow(new CornerTableRow(0, 8));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));
        facet.getCornerTable().addRow(new CornerTableRow(3, 1));
        facet.getCornerTable().addRow(new CornerTableRow(3, -1));
        facet.getCornerTable().addRow(new CornerTableRow(2, 11));
        facet.getCornerTable().addRow(new CornerTableRow(4, 3));
        facet.getCornerTable().addRow(new CornerTableRow(3, 13));
        facet.getCornerTable().addRow(new CornerTableRow(4, -1));
        facet.getCornerTable().addRow(new CornerTableRow(5, 7));
        facet.getCornerTable().addRow(new CornerTableRow(4, -1));
        facet.getCornerTable().addRow(new CornerTableRow(6, 9));
        facet.getCornerTable().addRow(new CornerTableRow(5, -1));

        model.addFacet(facet);
        return model;
    }

    /**
     * Unit test for CrossSectionZigZag visitor
     */
    @Test
    public void CrossSectionZigZag() {
        Plane cuttingPlane = new Plane(new Vector3d(-1, 0, 0), -0.5);
        MeshModel model = createModel();

        CrossSectionVisitorImpl cs = new CrossSectionVisitorImpl(cuttingPlane);
        model.compute(cs);
        CrossSection3D curve = cs.getCrossSectionCurve();

        //They can be ordered two ways, check both
        Assertions.assertEquals(6, curve.getSegmentSize(0));
        if (curve.getSegment(0).get(0).equals(new Point3d(0.5, 0, 0))) {
            Assertions.assertEquals(curve.getSegment(0).get(0), new Point3d(0.5, 0, 0));
            Assertions.assertEquals(curve.getSegment(0).get(1), new Point3d(0.5, 0.5, 0));
            Assertions.assertEquals(curve.getSegment(0).get(2), new Point3d(0.5, 1, 0));
            Assertions.assertEquals(curve.getSegment(0).get(3), new Point3d(0.5, 1.5, 0));
            Assertions.assertEquals(curve.getSegment(0).get(4), new Point3d(0.5, 2, 0));
            Assertions.assertEquals(curve.getSegment(0).get(5), new Point3d(0.5, 2.5, 0));
        } else if (curve.getSegment(0).get(0).equals(new Point3d(0.5, 2.5, 0))) {
            Assertions.assertEquals(curve.getSegment(0).get(0), new Point3d(0.5, 2.5, 0));
            Assertions.assertEquals(curve.getSegment(0).get(1), new Point3d(0.5, 2, 0));
            Assertions.assertEquals(curve.getSegment(0).get(2), new Point3d(0.5, 1.5, 0));
            Assertions.assertEquals(curve.getSegment(0).get(3), new Point3d(0.5, 1, 0));
            Assertions.assertEquals(curve.getSegment(0).get(4), new Point3d(0.5, 0.5, 0));
            Assertions.assertEquals(curve.getSegment(0).get(5), new Point3d(0.5, 0, 0));
        } else {
            Assertions.fail();
        }
    }

}
