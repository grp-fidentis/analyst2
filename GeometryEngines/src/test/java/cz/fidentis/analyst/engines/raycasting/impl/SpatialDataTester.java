package cz.fidentis.analyst.engines.raycasting.impl;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.ray.RayIntersection;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Set;

/**
 * Class for convenient testing of large RayIntersection tests for equality accounting for CPU vs GPU precision differences
 *
 * @author Marek Horský
 */

public class SpatialDataTester {
    private final static float EPS = 0.001f;

    static boolean checkMeshPointForEquality(Set<MeshPoint> set1, Set<MeshPoint> set2, boolean text) {
        if (text) {
            System.out.println("Set1 size: " + set1.size() + " vs " + set2.size() + " Set2 size");
        }
        if (set1.size() != set2.size()) {
            return false;
        }
        return containsMeshPoint(set1, set2, text);
    }

    public static boolean checkIntersectionForEquality(Set<RayIntersection> set1, Set<RayIntersection> set2, boolean text) {
        if (text) {
            System.out.println("Set1 size: " + set1.size() + " vs " + set2.size() + " Set2 size");
        }
        if (set1.size() != set2.size()) {
            return false;
        }
        return contains(set1, set2, text);
    }

    static private boolean contains(Set<RayIntersection> set1, Set<RayIntersection> set2, boolean text) {
        return set1.parallelStream().allMatch(i -> floatContains(i, set2, text));
    }

    static private boolean containsMeshPoint(Set<MeshPoint> set1, Set<MeshPoint> set2, boolean text) {
        return set1.parallelStream().allMatch(i -> floatContainsMeshPoint(i, set2, text));
    }

    static private boolean floatContainsMeshPoint(MeshPoint intersection, Set<MeshPoint> intersections, boolean text) {
        return intersections.parallelStream().anyMatch(i -> floatEquals(i, intersection, text));
    }

    static private boolean floatContains(RayIntersection intersection, Set<RayIntersection> intersections, boolean text) {
        return intersections.parallelStream().anyMatch(i -> floatEquals(i, intersection, text));
    }

    static private boolean floatEquals(MeshPoint i1, MeshPoint i2, boolean text) {
        Point3d pos1 = i1.getPosition();
        Point3d pos2 = i2.getPosition();
        Vector3d vec1 = i1.getNormal();
        Vector3d vec2 = i2.getNormal();

        boolean result = pos1.epsilonEquals(pos2, EPS)
                && vec1.epsilonEquals(vec2, EPS);
        if (text && !result && pos1.epsilonEquals(pos2, EPS)) {
            System.out.println(i1 + " != " + i2);
        }
        return result;
    }

    static private boolean floatEquals(RayIntersection i1, RayIntersection i2, boolean text) {
        Point3d pos1 = i1.getPosition();
        Point3d pos2 = i2.getPosition();
        Vector3d vec1 = i1.getNormal();
        Vector3d vec2 = i2.getNormal();

        boolean result = pos1.epsilonEquals(pos2, EPS)
                && vec1.epsilonEquals(vec2, EPS)
                && (Math.abs(i1.getDistance()) - Math.abs(i2.getDistance())) < EPS
                && i1.isDirectHit() == i2.isDirectHit()
                && i1.getHitTriangle().equals(i2.getHitTriangle());
        if (text && !result && pos1.epsilonEquals(pos2, EPS)) {
            System.out.println(i1 + " != " + i2);
            System.out.println(i1.getDistance() + " != " + i2.getDistance());
            System.out.println(i1.isDirectHit() + " != " + i2.isDirectHit());
            System.out.println(i1.getHitTriangle() + " != " + i2.getHitTriangle());
        }
        return result;
    }
}
