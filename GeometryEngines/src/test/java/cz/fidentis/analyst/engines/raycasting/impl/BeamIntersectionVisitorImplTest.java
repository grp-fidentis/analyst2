package cz.fidentis.analyst.engines.raycasting.impl;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.ray.Ray;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionConfig;
import cz.fidentis.analyst.engines.raycasting.RayIntersectionServices;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.memory.BufferFactory;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLConfig;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLServices;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIf;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests GPU-based Beam Intersection Visitor equivalence with CPU-based Ray Intersection Visitor
 *
 * @author Marek Horský
 */
public class BeamIntersectionVisitorImplTest {
    private final static int SEED = 1;
    private static SpatialDataProvider testDataProvider;

    @BeforeAll
    public static void setUp() {
        testDataProvider = new SpatialDataProvider();
        testDataProvider.setSeed(SEED);
        if (!OpenCLServices.isOpenCLAvailable()) {
            System.err.println("Skipping OpenCl Ray Intersection tests. Reason: No supported OpenCL device");
        }
    }

    @Test
    @DisabledIf("OpenCLisNotAvailable")
    public void ParallelVisitorEqualityTest() {
        CLContext clContext = OpenCLServices.createContext();
        RayIntersectionOpenCLServices rayIntersectionServices = new RayIntersectionOpenCLServices(clContext,
                new RayIntersectionOpenCLConfig(MeshTriangle.Smoothing.NORMAL_AND_SHAPE, false));

        List<Point3d> originPoints = testDataProvider.getRandomPoints3D(100);
        WriteBufferGPU<Point3d> originPointsBuffer = BufferFactory.getVoxelPointBuffer(clContext);
        originPointsBuffer.putAll(originPoints);

        Vector3d direction = testDataProvider.getRandomVector3D();
        WriteBufferGPU<Vector3d> directionBuffer = BufferFactory.getVoxelVectorBuffer(clContext);
        directionBuffer.putAll(List.of(direction));

        List<MeshFacet> facets = List.of(testDataProvider.getRandomFacet(300, true));

        Octree octree = Octree.create(facets);
        OctreeOpenCL octreeCL = OctreeOpenCL.create(clContext);
        octreeCL.build(facets);

        Set<RayIntersection> resultGPU = rayIntersectionServices.computeClosest(octreeCL, originPointsBuffer, directionBuffer);
        Set<RayIntersection> resultCPU = computeSortedOnCPU(originPoints, octree, direction, MeshTriangle.Smoothing.NORMAL_AND_SHAPE);

        assertTrue(SpatialDataTester.checkIntersectionForEquality(resultCPU, resultGPU, false));
    }

    @Test
    @DisabledIf("OpenCLisNotAvailable")
    public void ParallelVisitorEqualityWithoutNormalsTest() {
        CLContext clContext = OpenCLServices.createContext();
        RayIntersectionOpenCLServices rayIntersectionServices = new RayIntersectionOpenCLServices(clContext,
                new RayIntersectionOpenCLConfig(MeshTriangle.Smoothing.NONE, false));

        List<Point3d> originPoints = testDataProvider.getRandomPoints3D(100);
        WriteBufferGPU<Point3d> originPointsBuffer = BufferFactory.getVoxelPointBuffer(clContext);
        originPointsBuffer.putAll(originPoints);

        Vector3d direction = testDataProvider.getRandomVector3D();
        WriteBufferGPU<Vector3d> directionBuffer = BufferFactory.getVoxelVectorBuffer(clContext);
        directionBuffer.putAll(List.of(direction));

        List<MeshFacet> facets = List.of(testDataProvider.getRandomFacet(300, true));

        Octree octree = Octree.create(facets);
        OctreeOpenCL octreeCL = OctreeOpenCL.create(clContext);
        octreeCL.build(facets);

        Set<RayIntersection> resultGPU = rayIntersectionServices.computeClosest(octreeCL, originPointsBuffer, directionBuffer);
        Set<RayIntersection> resultCPU = computeSortedOnCPU(originPoints, octree, direction, MeshTriangle.Smoothing.NONE);
        assertTrue(SpatialDataTester.checkIntersectionForEquality(resultCPU, resultGPU, false));
    }

    @Test
    @DisabledIf("OpenCLisNotAvailable")
    public void ParallelVisitorEqualityMeshPointTest() {
        CLContext clContext = OpenCLServices.createContext();
        RayIntersectionOpenCLServices rayIntersectionServices = new RayIntersectionOpenCLServices(clContext,
                new RayIntersectionOpenCLConfig(MeshTriangle.Smoothing.NONE, false));

        List<Point3d> originPoints = testDataProvider.getRandomPoints3D(100);
        WriteBufferGPU<Point3d> originPointsBuffer = BufferFactory.getVoxelPointBuffer(clContext);
        originPointsBuffer.putAll(originPoints);

        Vector3d direction = testDataProvider.getRandomVector3D();
        WriteBufferGPU<Vector3d> directionBuffer = BufferFactory.getVoxelVectorBuffer(clContext);
        directionBuffer.putAll(List.of(direction));

        List<MeshFacet> facets = List.of(testDataProvider.getRandomFacet(300, true));

        Octree octree = Octree.create(facets);
        OctreeOpenCL octreeCL = OctreeOpenCL.create(clContext);
        octreeCL.build(facets);

        Set<MeshPoint> resultGPU = rayIntersectionServices.computeClosestPoints(octreeCL, originPointsBuffer, directionBuffer);
        Set<MeshPoint> resultCPU = (Set) computeSortedOnCPU(originPoints, octree, direction, MeshTriangle.Smoothing.NONE);
        assertTrue(SpatialDataTester.checkMeshPointForEquality(resultCPU, resultGPU, false));

        originPointsBuffer.release();
        directionBuffer.release();
        octreeCL.release();
        rayIntersectionServices.release();
        OpenCLServices.release(clContext);
    }

    private Set<RayIntersection> computeSortedOnCPU(List<Point3d> originPoints, Octree octree, Vector3d direction, MeshTriangle.Smoothing smoothing) {
        return originPoints.parallelStream()
                .map(origin -> RayIntersectionServices.computeClosest(
                        List.of(octree),
                        new RayIntersectionConfig(new Ray(origin, direction), smoothing, false)))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    private boolean OpenCLisNotAvailable() {
        return !OpenCLServices.isOpenCLAvailable();
    }
}
