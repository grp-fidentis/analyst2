package cz.fidentis.analyst.engines.distance;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.mesh.measurement.FacetDistances;
import cz.fidentis.analyst.data.mesh.measurement.MeshDistances;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MeshDistanceTest {
    
    protected MeshFacet getTrivialFacet(double offset, double size) {
        MeshFacet facet = MeshFactory.createEmptyMeshFacet();
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0, 0, offset), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(size, 0, offset), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0, size, offset), new Vector3d(0, 0, 1), new Vector3d(), null));

        facet.getCornerTable().addRow(new CornerTableRow(0, -1));
        facet.getCornerTable().addRow(new CornerTableRow(1, -1));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));

        return facet;
    }
    
    protected MeshModel getTrivialModel(double offset, double size) {
        MeshModel model = MeshFactory.createEmptyMeshModel();
        model.addFacet(getTrivialFacet(offset, size));
        return model;
    }
    
    protected void testDist(MeshDistanceVisitor vis, MeshModel firstModel, MeshModel secondModel, double expectedDist) {
        MeshFacet secondFacet = secondModel.getFacets().get(0);
        
        secondModel.compute(vis);
        MeshDistances distances = vis.getDistancesOfVisitedFacets();
        
        assertNotNull(distances.getFacetMeasurement(secondFacet));
        FacetDistances facetDistances = distances.getFacetMeasurement(secondFacet);
        for (int i = 0; i < secondFacet.getNumberOfVertices(); i++) {
            assertEquals(expectedDist, facetDistances.get(i).getDistance());
        }
    }

    protected void testNearestPoints(MeshDistanceVisitor vis, MeshModel primaryModel, MeshModel measuredModel) {
        MeshFacet measuredFacet = measuredModel.getFacets().get(0);
        MeshFacet primaryFacet = primaryModel.getFacets().get(0);

        measuredModel.compute(vis);
        MeshDistances distances = vis.getDistancesOfVisitedFacets();

        assertNotNull(distances.getFacetMeasurement(measuredFacet));
        FacetDistances facetDistances = distances.getFacetMeasurement(measuredFacet);
        for (int i = 0; i < measuredFacet.getNumberOfVertices(); i++) {
            assertEquals(primaryFacet.getVertex(i).getPosition(), facetDistances.get(i).getNearestNeighbor().getPosition());
        }
    }
    
    
    @Test
    public void visitToVerticesTest() {
        MeshModel firstModel = getTrivialModel(1, 1);
        MeshModel secondModel = getTrivialModel(1.5, 1);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, secondModel, false, false), secondModel, firstModel, 0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, secondModel, false, false), secondModel, firstModel, 0.5);
    }

    @Test
    public void visitToVerticesBehindMeshTest() {
        MeshModel firstModel = getTrivialModel(1, 1);
        MeshModel secondModel = getTrivialModel(-1.5, 1);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 2.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, secondModel, false, false), secondModel, firstModel, 2.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 2.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, secondModel, false, false), secondModel, firstModel, 2.5);
    }

    @Test
    public void visitToVerticesRelativeDistanceTest() {
        MeshModel firstModel = getTrivialModel(1, 1);
        MeshModel secondModel = getTrivialModel(1.5, 1);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, firstModel, true, false), firstModel, secondModel, -0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, secondModel, true, false), secondModel, firstModel, 0.5);
    }

    @Test
    public void visitToVerticesBehindMeshRelativeDistanceTest() {
        MeshModel firstModel = getTrivialModel(1, 1);
        MeshModel secondModel = getTrivialModel(-1.5, 1);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, firstModel, true, false), firstModel, secondModel, 2.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, secondModel, true, false), secondModel, firstModel, -2.5);
    }
    
    @Test
    public void concurrencyTest() {
        MeshModel firstModel = getTrivialModel(1, 1);
        MeshModel secondModel = getTrivialModel(1.5, 1);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, secondModel, false, false), secondModel, firstModel, 0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, secondModel, false, false), secondModel, firstModel, 0.5);
    }
    
    @Test
    public void concurrencyThreeFacetsTest() {
        MeshModel firstModel = getTrivialModel(1, 1);
        firstModel.addFacet(getTrivialFacet(3, 1));
        firstModel.addFacet(getTrivialFacet(4, 1));
        MeshModel secondModel = getTrivialModel(1.5, 1);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 0.5);
        testDist(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel, 0.5);
    }


    @Test
    public void correspondentNearestPointTest() {
        MeshModel firstModel = getTrivialModel(1, 1);
        MeshModel secondModel = getTrivialModel(1, 1);
        MeshModel thirdModel = getTrivialModel(1.5, 1);
        testNearestPoints(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel);
        testNearestPoints(createVisitor(MeshDistanceConfig.Method.POINT_TO_TRIANGLE_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, thirdModel);
        testNearestPoints(createVisitor(MeshDistanceConfig.Method.POINT_TO_POINT_NEAREST_NEIGHBORS, firstModel, false, false), firstModel, secondModel);
    }

    private MeshDistanceVisitor createVisitor(MeshDistanceConfig.Method method, MeshModel model, boolean relativeDist, boolean crop) {
        return new MeshDistanceConfig(method, model, null, relativeDist, crop).getVisitor();
    }

}
