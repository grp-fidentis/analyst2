package cz.fidentis.analyst.engines.distance;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.engines.raycasting.impl.SpatialDataProvider;
import cz.fidentis.analyst.opencl.services.kdtree.KdTreeOpenCL;
import cz.fidentis.analyst.opencl.services.kdtree.impl.KdTreeOpenCLImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class KdTreeOpenCLBuildTest {
    private record KdTreePoint(int index, int depth) {}
    private static SpatialDataProvider testDataProvider;

    @BeforeAll
    public static void setUp() {
        testDataProvider = new SpatialDataProvider();
        testDataProvider.setSeed(1234);
    }


    @Test
    public void KdTreeBuildTest() {
        List<MeshPoint> meshVertices = testDataProvider.getRandomFacet(300, false).getVertices();
        KdTreeOpenCL kdTree = new KdTreeOpenCLImpl(meshVertices);

        List<Point3d> result = kdTree.getKdTreeArray();

        int index = 0;
        int depth = 0;
        Deque<KdTreePoint> deque;
        deque = new ArrayDeque<>();
        deque.push(new KdTreePoint(index, depth));

        while (!deque.isEmpty()) {
            KdTreePoint current = deque.removeFirst();
            index = current.index;
            depth = current.depth;

            Point3d currentPoint = result.get(index);

            if (index * 2 + 1 < result.size()) {
                switch (depth % 3) {
                    case 0:
                        assertTrue(currentPoint.x >= result.get(index * 2 + 1).x); break;
                    case 1:
                        assertTrue(currentPoint.y >= result.get(index * 2 + 1).y); break;
                    case 2:
                        assertTrue(currentPoint.z >= result.get(index * 2 + 1).z); break;
                    default: break;
                }
                deque.push(new KdTreePoint(index * 2 + 1, depth + 1));
            }
            if (index * 2 + 2 < result.size()) {
                switch (depth % 3) {
                    case 0:
                        assertTrue(currentPoint.x < result.get(index * 2 + 2).x); break;
                    case 1:
                        assertTrue(currentPoint.y < result.get(index * 2 + 2).y); break;
                    case 2:
                        assertTrue(currentPoint.z < result.get(index * 2 + 2).z); break;
                    default: break;
                }
                deque.push(new KdTreePoint(index * 2 + 2, depth + 1));
            }
        }
    }
}
