package cz.fidentis.analyst.engines.raycasting.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.octree.impl.OctNodeLeaf;
import cz.fidentis.analyst.data.ray.Ray;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Enkh-Undral EnkhBayar
 */
class RayImplIntersectionVisitorTest {

    final double EPS = 1e-10;
    
    @Test
    void isPointInCubeTest() {
        int halfSize = 1;
        List<Vector3d> multipliers = new ArrayList<>(27);
        for (int mx = -1; mx <= 1; mx++) {
            for (int my = -1; my <= 1; my++) {
                for (int mz = -1; mz <= 1; mz++) {
                    multipliers.add(new Vector3d(mx, my, mz));
                }
            }
        }

        var cube = new OctNodeLeaf(
                new Point3d(-halfSize, -halfSize, -halfSize), 
                new Point3d(halfSize, halfSize, halfSize),
                Collections.emptyList()
        );

        for (Vector3d m : multipliers) {
            var point = new Point3d(m.x * halfSize, m.y * halfSize, m.z * halfSize);
            assertTrue(cube.isPointInside(point));
        }

        int size = halfSize * 2;
        for (Vector3d m : multipliers) {
            if (m.equals(new Vector3d())) {
                continue;
            }
            var point = new Point3d(m.x * size, m.y * size, m.z * size);
            assertFalse(cube.isPointInside(point));
        }
    }
    
    protected MeshFacet getTrivialFacet() {
        Vector3d normal = new Vector3d(1,1,0);
        normal.normalize();

        MeshFacet facet = MeshFactory.createEmptyMeshFacet();
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-1,  1, -1), normal, null, null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-1,  1,  1), normal, null, null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d( 1, -1,  0), normal, null, null));

        facet.getCornerTable().addRow(new CornerTableRow(0, -1));
        facet.getCornerTable().addRow(new CornerTableRow(1, -1));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));

        return facet;
    }
    
    @Test
    void getSubNodeMaskTest() {
        MeshFacet facet = getTrivialFacet();
        Octree octree = Octree.create(List.of(facet));
        
        // positive coords in direction
        Ray ray = new Ray(new Point3d(0, -100, 0), new Vector3d(0, 1, 0));
        RayIntersectionVisitorImpl rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        int mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(new Vector3d(0, 1, 0), ray.direction());
        assertEquals(new Point3d(0, -100, 0), ray.origin());
        assertEquals(0, mask);
        
        // negative direction => insvert the ray and update the octants labeling mask
        ray = new Ray(new Point3d(0, 100, 0), new Vector3d(0, -1, 0));
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(new Vector3d(0, 1, 0), ray.direction());
        assertEquals(new Point3d(0, -100, 0), ray.origin());
        assertEquals(2, mask);
        
        var dir = new Vector3d(1,1,1);
        dir.normalize();
        ray = new Ray(new Point3d(-100, -100.5, -100.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(0, mask);
        
        dir = new Vector3d(-1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(100, 100.5, 100.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(7, mask);
    }
    
    @Test
    void getParamsTest() {
        MeshFacet facet = getTrivialFacet();
        Octree octree = Octree.create(List.of(facet));

        // a ray with no zero coord intersecting the bounding box
        var dir = new Vector3d(1,1,1);
        dir.normalize();
        Ray ray = new Ray(new Point3d(-100, -100, -100), dir);
        RayIntersectionVisitorImpl rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        var params = rtVisitor.getParams(octree.getRoot(), ray);
        Point3d t0 = params.get(0);
        Point3d t1 = params.get(1);
        assertTrue(Double.isFinite(t0.x) && Double.isFinite(t0.y) && Double.isFinite(t0.z));
        assertTrue(Double.isFinite(t1.x) && Double.isFinite(t1.y) && Double.isFinite(t1.z));
        assertTrue(Math.max(Math.max(t0.x, t0.y), t0.z) < Math.min(Math.min(t1.x, t1.y), t1.z));

        // a ray with no zero coord NOT intersecting the bounding box
        dir = new Vector3d(1,1,1);
        dir.normalize();
        ray = new Ray(new Point3d(-100, -110, -110), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        assertTrue(Double.isFinite(t0.x) && Double.isFinite(t0.y) && Double.isFinite(t0.z));
        assertTrue(Double.isFinite(t1.x) && Double.isFinite(t1.y) && Double.isFinite(t1.z));
        assertTrue(Math.max(Math.max(t0.x, t0.y), t0.z) >= Math.min(Math.min(t1.x, t1.y), t1.z));

        // a ray parallel to the Y coord intersecting the bounding box
        dir = new Vector3d(0,1, 0);
        dir.normalize();
        ray = new Ray(new Point3d(0.5, -100, 0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        assertTrue(Double.isInfinite(t0.x) && Double.isFinite(t0.y) && Double.isInfinite(t0.z));
        assertTrue(Double.isInfinite(t1.x) && Double.isFinite(t1.y) && Double.isInfinite(t1.z));
        assertTrue(Math.max(Math.max(t0.x, t0.y), t0.z) < Math.min(Math.min(t1.x, t1.y), t1.z));
        
        // a ray parallel to the Y coord intersecting the bounding box in different octants
        dir = new Vector3d(0,1, 0);
        dir.normalize();
        ray = new Ray(new Point3d(-0.5, -100, -0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        assertTrue(Double.isInfinite(t0.x) && Double.isFinite(t0.y) && Double.isInfinite(t0.z));
        assertTrue(Double.isInfinite(t1.x) && Double.isFinite(t1.y) && Double.isInfinite(t1.z));
        assertTrue(Math.max(Math.max(t0.x, t0.y), t0.z) < Math.min(Math.min(t1.x, t1.y), t1.z));
        
        // a ray parallel to the Y coord, which DOSN'T intersect the bounding box
        dir = new Vector3d(0,1, 0);
        dir.normalize();
        ray = new Ray(new Point3d(5, -100, 5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        assertTrue(Double.isInfinite(t0.x) && Double.isFinite(t0.y) && Double.isInfinite(t0.z));
        assertTrue(Double.isInfinite(t1.x) && Double.isFinite(t1.y) && Double.isInfinite(t1.z));
        assertTrue(Math.max(Math.max(t0.x, t0.y), t0.z) >= Math.min(Math.min(t1.x, t1.y), t1.z));
    }
    
    @Test
    void getNextCellPointTest() {
        MeshFacet facet = getTrivialFacet();
        Octree octree = Octree.create(List.of(facet));

        // a ray with positive coordinates, which intersects the bounding box
        var dir = new Vector3d(1,1,1);
        dir.normalize();
        Ray ray = new Ray(new Point3d(-100, -100.5, -100.5), dir);
        RayIntersectionVisitorImpl rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        var params = rtVisitor.getParams(octree.getRoot(), ray);
        var t0 = params.get(0);
        var t1 = params.get(1);
        Point3d pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        assertTrue(Double.isFinite(pm.x) && Double.isFinite(pm.y) && Double.isFinite(pm.z));
        assertTrue(pm.x >= t0.x && pm.y >= t0.y && pm.z >= t0.z);
        assertTrue(pm.x <= t1.x && pm.y <= t1.y && pm.z <= t1.z);
        
        // a ray with negative coordinates, which intersects the bounding box
        dir = new Vector3d(-1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(100, 100.5, 100.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        assertTrue(Double.isFinite(pm.x) && Double.isFinite(pm.y) && Double.isFinite(pm.z));
        assertTrue(pm.x >= t0.x && pm.y >= t0.y && pm.z >= t0.z);
        assertTrue(pm.x <= t1.x && pm.y <= t1.y && pm.z <= t1.z);

        // a ray with zero X and Z coordinates, which intersects the bounding box
        dir = new Vector3d(0,1,0);
        ray = new Ray(new Point3d(0, -100, 0), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        assertTrue(Double.isInfinite(pm.x) && Double.isFinite(pm.y) && Double.isInfinite(pm.z));
        assertTrue(pm.x >= t0.x && pm.y >= t0.y && pm.z >= t0.z);
        assertTrue(pm.x <= t1.x && pm.y <= t1.y && pm.z <= t1.z);
    }
    
    @Test
    void getFirstSubNodeTest() {
        MeshFacet facet = getTrivialFacet();
        Octree octree = Octree.create(List.of(facet));

        // a ray with positive coordinates, which intersects the bounding box
        var dir = new Vector3d(1,1,1);
        dir.normalize();
        Ray ray = new Ray(new Point3d(-100, -100.5, -100.5), dir);
        RayIntersectionVisitorImpl rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        var mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(0, mask);
        var params = rtVisitor.getParams(octree.getRoot(), ray);
        var t0 = params.get(0);
        var t1 = params.get(1);
        Point3d pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        var subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(0, subNode);
        
        dir = new Vector3d(0,1,0);
        dir.normalize();
        ray = new Ray(new Point3d(-0.5, -100, -0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(0, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(0, subNode);
        
        dir = new Vector3d(0,1,0);
        dir.normalize();
        ray = new Ray(new Point3d(0.5, -100, 0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(0, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(5, subNode);
        
        dir = new Vector3d(0,1,0);
        dir.normalize();
        ray = new Ray(new Point3d(0.5, -100, -0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(0, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(4, subNode);

        dir = new Vector3d(1,1,1);
        dir.normalize();
        ray = new Ray(new Point3d(-8.25, -10, -8.25), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(0, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(5, subNode);
        
        dir = new Vector3d(1,1,1);
        dir.normalize();
        ray = new Ray(new Point3d(-8.25, -10, -9.25), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(0, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(4, subNode);
        
        
        
        // negative direction:
        dir = new Vector3d(-1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(100, 100, 100), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(7, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(0, subNode);
        
        dir = new Vector3d(0,-1,0);
        dir.normalize();
        ray = new Ray(new Point3d(0.5, 100, 0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(2, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(5, subNode);
        
        dir = new Vector3d(0,-1,0);
        dir.normalize();
        ray = new Ray(new Point3d(-0.5, 100, -0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(2, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(0, subNode);
        
        dir = new Vector3d(0,-1,0);
        dir.normalize();
        ray = new Ray(new Point3d(0.5, 100, -0.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        mask = rtVisitor.getSubNodeMask(octree.getRoot(), ray);
        assertEquals(2, mask);
        params = rtVisitor.getParams(octree.getRoot(), ray);
        t0 = params.get(0);
        t1 = params.get(1);
        pm = rtVisitor.getNextCellPoint(t0, t1, octree.getRoot());
        subNode = rtVisitor.getFistSubNode(t0, pm);
        assertEquals(4, subNode);
    }
    
    @Test
    void traverseOctreeTest() {
        MeshFacet facet = getTrivialFacet();
        Octree octree = Octree.create(List.of(facet));
        Vector3d dir;
        Ray ray;
        RayIntersectionVisitorImpl rtVisitor;
        
        // a ray with positive coordinates, which intersects the bounding box
        dir = new Vector3d(1,1,1);
        dir.normalize();
        ray = new Ray(new Point3d(-100, -100, -100), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertEquals(new Point3d(0,0,0), rtVisitor.getClosestIntersection(false).getPosition()); 
        
        dir = new Vector3d(-1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(100, 100, 100), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertEquals(new Point3d(0,0,0), rtVisitor.getClosestIntersection(false).getPosition()); 
        
        dir = new Vector3d(1,1,1);
        dir.normalize();
        ray = new Ray(new Point3d(-100, -100.5, -100.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertEquals(new Point3d(0.25, -0.25, -0.25), rtVisitor.getClosestIntersection(false).getPosition()); 
        
        dir = new Vector3d(-1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(100, 100.5, 100.5), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertEquals(new Point3d(-0.25, 0.25, 0.25), rtVisitor.getClosestIntersection(false).getPosition()); // ???
        
        dir = new Vector3d(0,1,0);
        dir.normalize();
        ray = new Ray(new Point3d(0, -100, 0), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertTrue(rtVisitor.getClosestIntersection(false).getPosition().epsilonEquals(new Point3d(0, 0, 0), EPS));
        //assertEquals(new Point3d(0, 0, 0), rtVisitor.getIntersectionsData().getIntersection(0));

        dir = new Vector3d(0,-1,0);
        dir.normalize();
        ray = new Ray(new Point3d(0, 100, 0), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertTrue(rtVisitor.getClosestIntersection(false).getPosition().epsilonEquals(new Point3d(0, 0, 0), EPS));
        //assertEquals(new Point3d(0, 0, 0), rtVisitor.getIntersectionsData().getIntersection(0));
    }
}
