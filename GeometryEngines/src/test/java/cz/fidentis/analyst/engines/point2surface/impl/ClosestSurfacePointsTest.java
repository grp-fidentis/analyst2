package cz.fidentis.analyst.engines.point2surface.impl;

import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClosestSurfacePointsTest {
    
    protected MeshFacet getTrivialFacet(double offset, double size) {
        MeshFacet facet = MeshFactory.createEmptyMeshFacet();
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0, 0, offset), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(size, 0, offset), new Vector3d(0, 0, 1), new Vector3d(), null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0, size, offset), new Vector3d(0, 0, 1), new Vector3d(), null));

        facet.getCornerTable().addRow(new CornerTableRow(0, -1));
        facet.getCornerTable().addRow(new CornerTableRow(1, -1));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));

        return facet;
    }
    
    protected void distTest(double expectedDist, ClosestSurfacePointsImpl vis) {
        KdTree tree = KdTree.create(getTrivialFacet(1.5, 1));
        tree.accept(vis);
        assertEquals(expectedDist, vis.getDistance());
    }
    
    @Test
    public void distTest() {
        ClosestSurfacePointsImpl vis = new ClosestSurfacePointsImpl(new Point3d(0,0,0), false); // sequentially
        distTest(1.5, vis);
    }

    @Test
    public void exactMatchTest() {
        ClosestSurfacePointsImpl vis = new ClosestSurfacePointsImpl(new Point3d(0, 0, 1.5), false); // relative dist, sequentially
        distTest(0, vis);
    }
}
