package cz.fidentis.analyst.engines.raycasting.impl;


import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.ray.Ray;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author oslejsek
 */
class RayImplIntersectionVisitorComplexTest {
    
    protected MeshFacet getComplexFacet() {
        Vector3d normal = new Vector3d(-1,1,0);
        normal.normalize();
        
        MeshFacet facet = MeshFactory.createEmptyMeshFacet();
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-1,  -1, -1), normal, null, null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-1,  -1,  1), normal, null, null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(-0.2, -0.2,  0), normal, null, null));
        
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(1,  1, -1), normal, null, null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(1,  1,  1), normal, null, null));
        facet.addVertex(MeshFactory.createMeshPoint(new Point3d(0.2, 0.2,  0), normal, null, null));

        facet.getCornerTable().addRow(new CornerTableRow(0, -1));
        facet.getCornerTable().addRow(new CornerTableRow(1, -1));
        facet.getCornerTable().addRow(new CornerTableRow(2, -1));
        
        facet.getCornerTable().addRow(new CornerTableRow(3, -1));
        facet.getCornerTable().addRow(new CornerTableRow(4, -1));
        facet.getCornerTable().addRow(new CornerTableRow(5, -1));

        return facet;
    }
    
    @Test
    void traverseOctreeTest() {
        MeshFacet facet = getComplexFacet();
        Octree octree = Octree.create(List.of(facet));
        Vector3d dir;
        Ray ray;
        RayIntersectionVisitorImpl rtVisitor;

        dir = new Vector3d(1,-1,0);
        dir.normalize();
        ray = new Ray(new Point3d(-5.75, 5, 0), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertEquals(1, rtVisitor.getUnsortedIntersections(false).size());
        assertEquals(new Point3d(-0.375,-0.375,0), rtVisitor.getClosestIntersection(false).getPosition()); 

        dir = new Vector3d(1,-1,-1);
        dir.normalize();
        ray = new Ray(new Point3d(-10,10,10), dir);
        rtVisitor = new RayIntersectionVisitorImpl(ray, MeshTriangle.Smoothing.NONE);
        octree.accept(rtVisitor);
        assertEquals(0, rtVisitor.getUnsortedIntersections(false).size());
    }
}
