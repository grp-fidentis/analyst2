package cz.fidentis.analyst.engines.raycasting.impl;

import com.jogamp.opencl.CLContext;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.ray.RayIntersection;
import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.engines.bbox.BoundingBoxConfig;
import cz.fidentis.analyst.engines.bbox.BoundingBoxServices;
import cz.fidentis.analyst.opencl.OpenCLServices;
import cz.fidentis.analyst.opencl.memory.BufferFactory;
import cz.fidentis.analyst.opencl.memory.WriteBufferGPU;
import cz.fidentis.analyst.opencl.services.octree.OctreeOpenCL;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLConfig;
import cz.fidentis.analyst.opencl.services.raycasting.RayIntersectionOpenCLServices;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIf;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests GPU-based Octree Construction
 *
 * @author Marek Horský
 */
public class OctreeBuilderTest {
    private final static int SEED = 1;
    private static SpatialDataProvider testDataProvider;

    @BeforeAll
    public static void setUp() {
        testDataProvider = new SpatialDataProvider();
        testDataProvider.setSeed(SEED);
        if (!OpenCLServices.isOpenCLAvailable()) {
            System.err.println("Skipping OpenCl Ray Intersection tests. Reason: No supported OpenCL device");
        }
    }

    @Test
    @DisabledIf("OpenCLisNotAvailable")
    public void OctreeBuildBBoxTest() {
        CLContext clContext = OpenCLServices.createContext();
        MeshFacet facet = testDataProvider.getRandomFacet(300, true);
        OctreeOpenCL octree = OctreeOpenCL.create(clContext);
        octree.build(List.of(facet));

        Box cpuBBox = BoundingBoxServices.compute(facet, new BoundingBoxConfig());
        Box gpuBBox = octree.getBBox();
        assertTrue(gpuBBox.minPoint().epsilonEquals(cpuBBox.minPoint(), 0.01f));
        assertTrue(gpuBBox.maxPoint().epsilonEquals(cpuBBox.maxPoint(), 0.01f));

        octree.release();
        OpenCLServices.release(clContext);
    }

    @Test
    @DisabledIf("OpenCLisNotAvailable")
    public void OctreeBuildTest() {
        CLContext clContext = OpenCLServices.createContext();
        RayIntersectionOpenCLServices rayIntersectionServices = new RayIntersectionOpenCLServices(clContext,
                new RayIntersectionOpenCLConfig(MeshTriangle.Smoothing.NONE, false));

        WriteBufferGPU<Point3d> originPointsBuffer = BufferFactory.getVoxelPointBuffer(clContext);
        originPointsBuffer.putAll(testDataProvider.getRandomPoints3D(100));

        WriteBufferGPU<Vector3d> directionBuffer = BufferFactory.getVoxelVectorBuffer(clContext);
        directionBuffer.putAll(List.of(testDataProvider.getRandomVector3D()));

        List<MeshFacet> facets = List.of(testDataProvider.getRandomFacet(300, true));

        OctreeOpenCL octree = OctreeOpenCL.create(clContext);
        octree.build(facets);

        OctreeOpenCL octree2 = OctreeOpenCL.create(clContext);
        octree2.build(facets);

        assertEquals(octree.getMaxTrianglesPerLeaf(), octree2.getMaxTrianglesPerLeaf());
        assertEquals(octree.getBBox(), octree2.getBBox());

        Set<RayIntersection> result = rayIntersectionServices.computeClosest(octree, originPointsBuffer, directionBuffer);
        Set<RayIntersection> result2 = rayIntersectionServices.computeClosest(octree2, originPointsBuffer, directionBuffer);
        assertTrue(SpatialDataTester.checkIntersectionForEquality(result, result2, false));

        originPointsBuffer.release();
        directionBuffer.release();
        octree.release();
        octree2.release();
        rayIntersectionServices.release();
        OpenCLServices.release(clContext);
    }

    private boolean OpenCLisNotAvailable() {
        return !OpenCLServices.isOpenCLAvailable();
    }
}
