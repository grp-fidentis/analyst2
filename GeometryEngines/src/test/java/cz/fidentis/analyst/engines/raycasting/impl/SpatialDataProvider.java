package cz.fidentis.analyst.engines.raycasting.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.impl.cornertable.CornerTableRow;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

/**
 * Provides convenient creation of spatial data for test purposes
 *
 * @author Marek Horský
 */
public class SpatialDataProvider {
    private final Random random = new Random();
    private final int coordinateBound = 100;

    public void setSeed(long seed) {
        random.setSeed(seed);
    }

    public List<Point3d> getRandomPoints3D(int count) {
        return Stream.generate(this::getRandomPoint3D).limit(count).toList();
    }

    public MeshFacet getRandomFacet(int vertexCount, boolean withNormals) {

        MeshFacet facet = MeshFactory.createEmptyMeshFacet();
        for (int i = 0; i < vertexCount; i++) {
            Vector3d normal = null;
            if (withNormals) {
                normal = getRandomVector3D();
                normal.normalize();
            } else {
                getRandomVector3D(); //To keep the seed increments
            }
            facet.addVertex(MeshFactory.createMeshPoint(getRandomPoint3D(), normal, null, null));
            facet.getCornerTable().addRow(new CornerTableRow(i, -1));
        }

        return facet;
    }

    public Vector3d getRandomVector3D() {
        return new Vector3d(
                random.nextDouble(-coordinateBound, coordinateBound),
                random.nextDouble(-coordinateBound, coordinateBound),
                random.nextDouble(-coordinateBound, coordinateBound));
    }

    Point3d getRandomPoint3D() {
        return new Point3d(
                random.nextDouble(-coordinateBound, coordinateBound),
                random.nextDouble(-coordinateBound, coordinateBound),
                random.nextDouble(-coordinateBound, coordinateBound));
    }
}
