package cz.fidentis.analyst.rendering;

/**
 * Rendering modes.
 * 
 * @author Radek Oslejsek
 */
public enum RenderingMode {
    SMOOTH, 
    WIREFRAME, 
    POINT_CLOUD, 
    TEXTURE, 
    HIDE // hides the object(s)
}
