package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.shapes.HeatMap3D;
import cz.fidentis.analyst.data.face.HumanFace;

import java.awt.*;

/**
 * Drawable human face.
 * 
 * @author Radek Oslejsek
 * @author Daniel Schramm
 */
public class DrawableFace extends DrawableMesh {

    public static final Color SKIN_COLOR_PRIMARY = new Color(224, 172, 105);
    public static final Color SKIN_COLOR_SECONDARY = new Color(242, 214, 208); //new Color(236, 188, 180);
    public static final Color SKIN_COLOR_DEFAULT = new Color(255, 255, 255); //new Color(236, 188, 180);
    
    private final HumanFace humanFace;

    private HeatMap3D heatMap;

    /**
     * Constructor.
     * 
     * @param face Drawable human face
     * @throws IllegalArgumentException if the mesh model
     *         of the human face is {@code null}
     */
    public DrawableFace(HumanFace face) {
        super(face.getMeshModel());
        humanFace = face;
        setColor(SKIN_COLOR_DEFAULT);
    }
    
    /**
     * Returns the human face.
     * 
     * @return {@link HumanFace}
     */
    public HumanFace getHumanFace() {
        return humanFace;
    }

    public void setHeatMap(HeatMap3D heatMap) {
        this.heatMap = heatMap;
    }

    /**
     * Determines whether the heatmap is set to be rendered.
     * 
     * @return id the heatmap will be rendered
     */
    public boolean isHeatmapRendered() {
        return heatMap != null;
    }

    public HeatMap3D getHeatMap() {
        return heatMap;
    }
}
