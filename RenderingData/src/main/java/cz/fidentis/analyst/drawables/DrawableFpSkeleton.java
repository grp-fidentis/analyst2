package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.landmarks.FeaturePointSkeleton;
import cz.fidentis.analyst.data.landmarks.Landmark;

import javax.vecmath.Point3d;
import java.util.ArrayList;
import java.util.List;

/**
 * A drawable feature point skeleton consisting of drawable lines.
 *
 * @author Katerina Zarska
 */
public class DrawableFpSkeleton extends Drawable {
    
    private final List<DrawableLine> skeleton = new ArrayList<>();
    
    /**
     * Constructor
     * @param featurePoints A list of feature points
     */
    public DrawableFpSkeleton(List<Landmark> featurePoints) {
        loadSkeleton(featurePoints);
    }
    
        /**
     * Loads the correct lines into the skeleton based on available feature points
     * 
     * @param featurePoints feature points that are currently in the face
     */
    public void loadSkeleton(List<Landmark> featurePoints) {
        skeleton.clear();
        List<Landmark> activeFeaturePoints = new ArrayList<>();
        
        for (Landmark fp : featurePoints) {
            if (FeaturePointSkeleton.SKELETON_FEATURE_POINTS.contains(fp.getName())) {
                activeFeaturePoints.add(fp);
            }
        }
        
        Point3d pointA; Point3d pointB;
        for (var e: FeaturePointSkeleton.SKELETON_MAP.entrySet()) {
            pointA = positionOfFeaturePoint(activeFeaturePoints, e.getKey());
            
            for (String s : e.getValue()) {
                pointB = positionOfFeaturePoint(activeFeaturePoints, s);
                if (pointA != null && pointB != null) {
                    skeleton.add(new DrawableLine(pointA, pointB));
                }
            }
        }
    }
    
        /**
     * Gets the position of a feature point based on its name
     * 
     * @param featurePoints A list of feature points
     * @param name Name of the feature point
     * @return 3D position
     */
    private Point3d positionOfFeaturePoint(List<Landmark> featurePoints, String name) {
        Point3d point = null;
        for (Landmark fp : featurePoints) {
            if (fp.getName().equals(name)) {
                point = fp.getPosition();
            }
        }
        return point;
    }
    
    /**
     * Gets the list of lines in the skeleton
     * @return list of DrawableLine
     */
    public List<DrawableLine> getSkeleton(){
        return skeleton;
    }
}
