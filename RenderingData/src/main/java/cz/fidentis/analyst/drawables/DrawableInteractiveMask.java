package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.surfacemask.SurfaceMask;

/**
 * Drawable interactive mask drawn as points connected by lines.
 * 
 * @author Mario Chromik
 */
public class DrawableInteractiveMask extends Drawable {

    private final SurfaceMask mask;

    /**
     * Constructor.
     *
     * @param mask mask with points projected onto face and ready to be drawn
     * @throws IllegalArgumentException if the {@code mask} is {@code null}
     */
    public DrawableInteractiveMask(SurfaceMask mask) {
        if (mask == null) {
            throw new IllegalArgumentException("MaskPoints cannot be null");
        }
        this.mask = mask;
    }

    public SurfaceMask getMask() {
        return mask;
    }
}
