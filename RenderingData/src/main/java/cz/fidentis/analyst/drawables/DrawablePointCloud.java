package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.mesh.MeshPoint;
import java.awt.Color;
import java.util.List;

/**
 * Points rendered as small spheres.
 * 
 * @author Radek Oslejsek
 */
public class DrawablePointCloud extends Drawable {
    
    public static final Color DEFAULT_COLOR = Color.BLACK.darker();
    public static final float FP_DEFAULT_SIZE = 3.0f;
    
    private final List<MeshPoint> points;
    
    /**
     * Constructor.
     * 
     * @param points Feature points
     */
    public DrawablePointCloud(List<MeshPoint> points) {
        this.points = points;
        setColor(DEFAULT_COLOR);
    }

    public List<MeshPoint> getPoints() {
        return points;
    }
}
