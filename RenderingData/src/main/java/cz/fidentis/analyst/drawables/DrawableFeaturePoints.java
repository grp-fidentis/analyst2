package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.landmarks.MeshVicinity;
import cz.fidentis.analyst.rendering.RenderingMode;

import java.awt.*;
import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Drawable feature points.
 *
 * @author Radek Oslejsek
 * @author Daniel Schramm
 * @author Katerina Zarska
 */
public class DrawableFeaturePoints extends Drawable implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Color offTheMeshColor = Color.GRAY;
    private Color closeToMeshColor = Color.ORANGE;
    private Color onTheMeshColor = Color.WHITE;
    private Color customFpColor = Color.RED;
    
    private double distanceThreshold = Double.POSITIVE_INFINITY;
    
    public static final Color FP_DEFAULT_COLOR = Color.LIGHT_GRAY;
    public static final double FP_DEFAULT_SIZE = 3f;
    
    private final double defaultPerimeter;
    
    /* feature points */
    private final List<Landmark> featurePoints;
    
    /**
     * feature points with color different from the default color
     */
    private final Map<Integer, Color> specialColors = new HashMap<>();
    
    /**
     * feature points with size different from the default size
     */
    private final Map<Integer, Double> specialSizes = new HashMap<>();
    
    /**
     * feature points rendered differently than the default render mode
     */
    private final Map<Integer, RenderingMode> specialRenderModes = new HashMap<>();
    
    /**
     * Constructor.
     * 
     * @param featurePoints Feature points
     */
    public DrawableFeaturePoints(List<Landmark> featurePoints) {
        this(featurePoints, FP_DEFAULT_COLOR, FP_DEFAULT_SIZE);
    }
    
    /**
     * Constructor.
     * 
     * @param featurePoints Feature points
     * @param defaultColor Default color
     * @param defaultPerimeter Default perimeter
     */
    public DrawableFeaturePoints(List<Landmark> featurePoints, Color defaultColor, double defaultPerimeter) {
        this.featurePoints = new ArrayList<>(featurePoints);
        this.defaultPerimeter = defaultPerimeter;
        setColor(defaultColor);
    }
    
    /**
     * Adds a new feature point
     * 
     * @param fp featurePoint to be added
     */
    public void addFeaturePoint(Landmark fp) {
        featurePoints.add(fp);
        int i = featurePoints.indexOf(fp);
        setColor(i, getAssignedColor(i));
        setSize(i, getSize(0));
        setRenderMode(i, RenderingMode.SMOOTH);
    }
    
    /**
     * Removes a feature point
     * 
     * @param fp featurePoint to be removed
     */
    public void removeFeaturePoint(Landmark fp) {
        int i = featurePoints.indexOf(fp);
        removeFeaturePoint(i);
    }
    
    /**
     * Removes a feature point
     * 
     * @param i index
     */
    public void removeFeaturePoint(int i) {
        if (i >= 0 && i < featurePoints.size()){
            featurePoints.remove(i);
            specialColors.remove(i);
            specialSizes.remove(i);
            specialRenderModes.remove(i);
            remapKeys(i);
        }
    }
    
    /**
     * updates the keys in HashMap attributes after removing a feature point
     * @param removedIndex index
     */
    private void remapKeys(int removedIndex) {
        for (int i = removedIndex + 1; i < featurePoints.size(); i++ ) {
            if (specialColors.containsKey(i)) {
                specialColors.put(i - 1, specialColors.get(i));
                specialColors.remove(i);
            }
            if (specialSizes.containsKey(i)) {
                specialSizes.put(i - 1, specialSizes.get(i));
                specialSizes.remove(i);
            }
            if (specialRenderModes.containsKey(i)) {
                specialRenderModes.put(i - 1, specialRenderModes.get(i));
                specialRenderModes.remove(i);
            }
        }
    }
    
    /**
     * Returns color of the feature point.
     * 
     * @param index Index of the feature point
     * @return The color or {@code null}
     */
    public Color getColor(int index) {
        if (index < 0 || index >= featurePoints.size()) {
            return null;
        }
        return specialColors.getOrDefault(index, FP_DEFAULT_COLOR);
    }
    
    /**
     * Sets color of the feature point.
     * 
     * @param index Index of the feature point
     * @param color New color of the feature point
     */
    public void setColor(int index, Color color) {
        if (index < 0 || index >= featurePoints.size() || color == null) {
            return;
        }
        if (color.equals(getColor())) {
            specialColors.remove(index);
        } else {
            specialColors.put(index, color);
        }
    }
    
    /**
     * Gets the assigned color of a feature point based on its relation to mesh
     * 
     * @param index index of a chosen feature point
     * @return Color
     */
    public Color getAssignedColor(int index) {
        return switch (featurePoints.get(index).getMeshVicinity().getPositionType(distanceThreshold)) {
            case ON_THE_MESH -> onTheMeshColor;
            case CLOSE_TO_MESH -> closeToMeshColor;
            case OFF_THE_MESH -> offTheMeshColor;
        };
    }
    
    /**
     * Gets the maximum distance of all feature points from the mesh
     * 
     * @return double
     */
    public double getMaxDistance() {
        double maxDistance = Double.NEGATIVE_INFINITY;
        for(Landmark fp : featurePoints) {
            if ((fp.getMeshVicinity().getPositionType(distanceThreshold) != MeshVicinity.Location.OFF_THE_MESH) &&
                (fp.getMeshVicinity().distance() > maxDistance)) {
                maxDistance = fp.getMeshVicinity().distance();
            }
        }
        return maxDistance;
    }
    
    /**
     * Removes (possible) special color of the feature point.
     * 
     * @param index Index of the feature point
     */
    public void resetColorToDefault(int index) {
        setColor(index, getColor());
    }
    
    /**
     * Removes all individual colors of feature points.
     */
    public void resetAllColorsToDefault() {
        this.specialColors.clear();
    }
    
    /**
     * Returns size of the feature point.
     * 
     * @param index Index of the feature point
     * @return The size or {@code null}
     */
    public Double getSize(int index) {
        if (index < 0 || index >= featurePoints.size()) {
            return null;
        }
        return specialSizes.getOrDefault(index, this.defaultPerimeter);
    }
    
    /**
     * Sets size of the feature point. 
     * 
     * @param index Index of the feature point
     * @param size New size of the feature point.
     */
    public void setSize(int index, double size) {
        if (index < 0 || index >= featurePoints.size()) {
            return;
        }
        if (size == defaultPerimeter) {
            specialSizes.remove(index);
        } else {
            specialSizes.put(index, size);
        }
    }
    
    /**
     * Removes (possible) special size of the feature point.
     * 
     * @param index Index of the feature point
     */
    public void resetSizeToDefault(int index) {
        setSize(index, defaultPerimeter);
    }
    
    /**
     * Removes all individual sizes of feature points.
     */
    public void resetAllSizesToDefault() {
        specialSizes.clear();
    }
    
    /**
     * Returns render mode of the feature point.
     * 
     * @param index Index of the feature point
     * @return The render mode or {@code null}
     */
    public RenderingMode getRenderMode(int index) {
        if (index < 0 || index >= featurePoints.size()) {
            return null;
        }
        if (specialRenderModes.containsKey(index)) {
            return specialRenderModes.get(index);
        } else {
            return getRenderMode();
        }
    }
    
    /**
     * Sets render mode of the feature point. {@code RenderingMode.NOTHING} hides the point.
     * 
     * @param index Index of the feature point
     * @param renderMode New render mode of the feature point
     */
    public void setRenderMode(int index, RenderingMode renderMode) {
        if (index < 0 || index >= featurePoints.size()) {
            return;
        }
        if (renderMode == getRenderMode()) {
            specialRenderModes.remove(index);
        } else {
            specialRenderModes.put(index, renderMode);
        }
    }
    
    /**
     * Removes (possible) special render mode of the feature point.
     * 
     * @param index Index of the feature point
     */
    public void resetRenderModeToDefault(int index) {
        setRenderMode(index, getRenderMode());
    }
    
    /**
     * Removes all individual render modes of feature points.
     */
    public void resetAllRenderModesToDefault() {
        specialRenderModes.clear();
    }

    /**
     * @return {@link List} of {@link Landmark}
     */
    public List<Landmark> getFeaturePoints() {
        return featurePoints;
    }
    
    public double getDefaultPerimeter() {
        return defaultPerimeter;
    }

    public double getDistanceThreshold() {
        return distanceThreshold;
    }

    public Color getOffTheMeshColor() {
        return offTheMeshColor;
    }

    public Color getCloseToMeshColor() {
        return closeToMeshColor;
    }

    public Color getOnTheMeshColor() {
        return onTheMeshColor;
    }

    public Color getCustomFpColor() {
        return customFpColor;
    }

    public void setDistanceThreshold(double distanceThreshold) {
        this.distanceThreshold = distanceThreshold;
    }

    public void setOffTheMeshColor(Color offTheMeshColor) {
        this.offTheMeshColor = offTheMeshColor;
    }

    public void setCloseToMeshColor(Color closeToMeshColor) {
        this.closeToMeshColor = closeToMeshColor;
    }

    public void setOnTheMeshColor(Color onTheMeshColor) {
        this.onTheMeshColor = onTheMeshColor;
    }

    public void setCustomFpColor(Color customFpColor) {
        this.customFpColor = customFpColor;
    }

    public Map<Integer, Color> getSpecialColors() {
        return specialColors;
    }


    public Map<Integer, Double> getSpecialSizes() {
        return specialSizes;
    }

    public Map<Integer, RenderingMode> getSpecialRenderModes() {
        return specialRenderModes;
    }
}
