package cz.fidentis.analyst.drawables;

import javax.vecmath.Point3d;

/**
 * A line to be shown in the scene, e.g., to show a ray.
 * 
 * @author Radek Oslejsek
 */
public class DrawableLine extends Drawable {
    
    private final Point3d startPoint;
    private final Point3d endPoint;
    
    /**
     * Constructor.
     * 
     * @param start Start point
     * @param end  End point
     */
    public DrawableLine(Point3d start, Point3d end) {
        this.startPoint = start;
        this.endPoint = end;
    }

    public Point3d getStartPoint() {
        return startPoint;
    }

    public Point3d getEndPoint() {
        return endPoint;
    }
}
