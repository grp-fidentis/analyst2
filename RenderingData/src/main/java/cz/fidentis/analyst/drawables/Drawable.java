package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.rendering.RenderingMode;
import java.awt.Color;
import java.util.Comparator;

/**
 * A drawable object, i.e., an object with drawing state and capable to 
 * render itself into an OpenGL context.
 * 
 * @author Radek Oslejsek
 * @author Richard Pajersky
 */
public abstract class Drawable {

    private boolean display = true;
    
    /* material info */
    private Color color = Color.LIGHT_GRAY;
    private float transparency = 1; // 0 = off, 1 = max
    private Color highlights = new Color(0, 0, 0, 1);
    
    /**
     * Render mode to use, one from {@code GL_FILL}, {@code GL_LINE}, {@code GL_POINT}
     */
    private RenderingMode renderMode = RenderingMode.SMOOTH;
    
    /**
     * Default constructor.
     */
    public Drawable() {
        
    }
    
    /**
     * Copy constructor.
     * @param drawable Original drawable object
     * @throws NullPointerException if the input argument is {@code null}
     */
    public Drawable(Drawable drawable) {
        this.color = drawable.color;
        this.highlights = drawable.highlights;
        this.transparency = drawable.transparency;
    }

    /**
     * Comparator for Drawable objects based on transparency. Used for rendering.
     *
     * @author Dominik Racek
     */
    public static class TransparencyComparator implements Comparator<Drawable> {
        @Override
        public int compare(Drawable a, Drawable b) {
            if (a.transparency < b.transparency) {
                return 1;
            }
            if (a.transparency > b.transparency) {
                return -1;
            }
            return 0;
        }
    }

    /**
     * Shows or hides the drawable.
     * 
     * @param show If {@code true}, then the drawable is shown.
     */
    public void show(boolean show) {
        display = show;
    }
    
    /**
     * 
     * @return {@code true} if the object is included (rendered) in the scene.
     */
    public boolean isShown() {
        return display;
    }
    
    /**
     * Sets color
     * @param color Color
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @return {@link Color}
     */
    public Color getColor() {
        return color;
    }
    
    /**
     * @return Current value of transparency
     */
    public float getTransparency() {
        return transparency;
    }

    /**
     * Sets transparency
     * @param transparency Transparency
     */
    public void setTransparency(float transparency) {
        this.transparency = transparency;
    }
    
    /**
     * @return {@link Color} of highlights
     */
    public Color getHighlights() {
        return highlights;
    }

    /**
     * Sets {@link Color} of highlights
     * @param highlights 
     */
    public void setHighlights(Color highlights) {
        this.highlights = highlights;
    }
    
    /**
     * @return Value of {@link #renderMode}
     */
    public RenderingMode getRenderMode() {
        return renderMode;
    }

    /**
     * Sets rendering mode
     * @param renderMode Rendering mode
     */
    public void setRenderMode(RenderingMode renderMode) {
        this.renderMode = renderMode;
    }

}
