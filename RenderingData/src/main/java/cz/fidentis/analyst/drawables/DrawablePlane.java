package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.Plane;

/**
 * A plane to be shown as a rectangular mesh facet.
 * The rectangular shape is generated at run-time from the associated plane.
 *
 * @author Radek Oslejsek
 * @author Dominik Racek
 */
public class DrawablePlane extends Drawable {

    private Plane plane;
    private final Box bbox;
    private boolean showBoundingBox;


    /**
     * Constructor.
     *
     * @param plane    plane
     * @param bbox     bounding box used to estimate cutting pale shape (rectangle)
     * @param showBBox whether to render bounding box
     */
    public DrawablePlane(Plane plane, Box bbox, boolean showBBox) {
        this.plane = plane;
        this.bbox = bbox;
        this.showBoundingBox = showBBox;
    }

    /**
     * Copy constructor.
     *
     * @param drPlane Original plane
     * @throws NullPointerException if the input argument is {@code null}
     */
    public DrawablePlane(DrawablePlane drPlane) {
        super(drPlane);
        this.plane = new Plane(drPlane.getPlane());
        this.bbox = drPlane.bbox;
        this.showBoundingBox = drPlane.showBoundingBox;
    }

    public Plane getPlane() {
        return plane;
    }

    /**
     * Returns {@code true} if the bounding box is to be rendered.
     *
     * @return {@code true} if the bounding box is to be rendered.
     */
    public boolean isShownBBobx() {
        return this.showBoundingBox;
    }

    /**
     * Switches on/off bounding box rendering.
     *
     * @param show If {@code true}, then the bounding box is rendered as well
     */
    public void setShowBBox(boolean show) {
        this.showBoundingBox = show;
    }

    /**
     * Sets the plane
     *
     * @param plane new plane
     */
    public void setPlane(Plane plane) {
        this.plane = plane;
    }


    public Box getBBox() {
        return this.bbox;
    }
}
