package cz.fidentis.analyst.drawables;

import com.jogamp.opengl.util.texture.Texture;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshModel;

import java.util.List;

/**
 * A drawable triangular mesh, i.e., a mesh model with drawing information like
 * material, transparency, color, relative transformations in the scene etc.
 * This class encapsulates rendering state and parameters,
 *
 * @author Radek Oslejsek
 * @author Richard Pajersky
 * @author Katerina Zarska
 */
public class DrawableMesh extends Drawable {

    private final MeshModel model;
    private Texture texture = null;

    /**
     * Copy constructor.
     *
     * @param mesh Mesh to be copied
     * @throws NullPointerException if the input argument is {@code null}
     */
    public DrawableMesh(DrawableMesh mesh) {
        super(mesh);
        this.model = MeshFactory.cloneMeshModel(mesh.getModel());
        this.texture = mesh.texture;
    }

    /**
     * Constructor.
     *
     * @param model Drawable mesh model
     * @throws IllegalArgumentException if the model is {@code null}
     */
    public DrawableMesh(MeshModel model) {
        if (model == null) {
            throw new IllegalArgumentException("model is null");
        }
        this.model = model;
    }

    /**
     * Constructor.
     *
     * @param facet Mesh facet
     * @throws IllegalArgumentException if the model is {@code null}
     */
    public DrawableMesh(MeshFacet facet) {
        if (facet == null) {
            throw new IllegalArgumentException("facet is null");
        }
        this.model = MeshFactory.createEmptyMeshModel();
        this.model.addFacet(facet);
    }

    /**
     * Returns list of individual facets.
     *
     * @return list of individual facets.
     */
    public List<MeshFacet> getFacets() {
        return model.getFacets();
    }

    /**
     * @return {@link MeshModel}
     */
    public MeshModel getModel() {
        return this.model;
    }

    /**
     * @return {@link Texture}
     */
    public Texture getTexture() {
        return this.texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
}
