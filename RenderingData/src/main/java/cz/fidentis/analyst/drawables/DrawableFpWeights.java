package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.rendering.RenderingMode;

import java.awt.*;
import java.util.List;

/**
 * Spherical surrounding of feature points that represent their weights.
 * 
 * @author Radek Oslejsek
 */
public class DrawableFpWeights extends DrawableFeaturePoints {

    public static final Color FPW_DEFAULT_COLOR = Color.WHITE;
    public static final double FPW_DEFAULT_SIZE = 20f;
    public static final RenderingMode FPW_DEFAULT_RENDERING_MODE = RenderingMode.WIREFRAME;
    
    /**
     * Constructor.
     * 
     * @param featurePoints Feature points
     */
    public DrawableFpWeights(List<Landmark> featurePoints) {
        this(featurePoints, FPW_DEFAULT_COLOR, FPW_DEFAULT_SIZE);
    }
    
    /**
     * Constructor.
     * 
     * @param featurePoints Feature points
     * @param defaultColor Default color
     * @param defaultPerimeter Default perimeter
     */
    public DrawableFpWeights(List<Landmark> featurePoints, Color defaultColor, double defaultPerimeter) {
        super(featurePoints, FPW_DEFAULT_COLOR, FPW_DEFAULT_SIZE);
        setRenderMode(FPW_DEFAULT_RENDERING_MODE);
    }
}
