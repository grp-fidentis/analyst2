package cz.fidentis.analyst.drawables;

import cz.fidentis.analyst.data.shapes.Box;
import cz.fidentis.analyst.data.shapes.Plane;

/**
 * Drawable plane with the possibility to shift it along the normal and,
 * moreover, to show a "mirror" plane (a plane shifted in the opposite direction).
 * 
 * @author Radek Oslejsek
 * @author Dominik Racek
 */
public class DrawableCuttingPlane extends DrawablePlane {
    
    private double shift = 0.0;
    private boolean showMirror = false;
    
    /**
     * Constructor.
     * 
     * @param plane plane
     * @param bbox bounding box used to estimate cutting pale shape (rectangle)
     * @param showBBox whether to render bounding box
     */
    public DrawableCuttingPlane(Plane plane, Box bbox, boolean showBBox) {
        super(plane, bbox, showBBox);
    }
    
    /**
     * Copy constructor.
     * @param drPlane Original plane
     * @throws NullPointerException if the input argument is {@code null}
     */
    public DrawableCuttingPlane(DrawableCuttingPlane drPlane) {
        super(drPlane);
        this.shift = drPlane.shift;
    }
    
    /**
     * Moves the plane in the plane's normal direction relatively to the original position.
     * 
     * @param dist Distance. If positive, then the movements is in the direction of the normal vector.
     * Otherwise, the movement is against the normal direction.
     */
    public void shift(double dist) {
        this.shift = dist;
    }
    
    /**
     * Returns current shift value of plane
     * @return current shift value of plane
     */
    public double getShift() {
        return shift;
    }
    
    @Override
    public Plane getPlane() {
        return super.getPlane().shift(this.shift);
    }
    
    /**
     * Returns original non-shifted plane
     * @return original non-shifted plane
     */
    public Plane getNonShiftedPlane() {
        return super.getPlane();
    }
    
    /**
     * Returns the cutting plane shifted to opposite direction.
     * @return the cutting plane shifted to opposite direction
     */
    public Plane getMirrorPlane() {
        return super.getPlane().shift(-this.shift);
    }
    
    /**
     * Shows/hides the mirror plane (i.e., the cutting plane shifted in the opposite direction
     * @param show Shows if {@code true}, hides otherwise
     */
    public void showMirrorPlane(boolean show) {
        this.showMirror = show;
    }
    
    /**
     * Returns {@code true} if the mirror planes are set to be shown.
     * @return {@code true} if the mirror planes are set to be shown.
     */
    public boolean isMirrorPlaneShown() {
        return this.showMirror;
        //return getModel().getFacets().size() > 1;
    }
}
