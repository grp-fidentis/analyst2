package cz.fidentis.analyst.engines.landmarks;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.IPosition;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.shapes.Plane;
import cz.fidentis.analyst.engines.landmarks.impl.LandmarksSymmetryImpl;
import cz.fidentis.analyst.engines.landmarks.impl.ProcrustesImpl;

import java.util.Collection;
import java.util.List;

/**
 * Public stateless services dealing with landmarks.
 *
 * TO DO: Provide IcpTransformation instead of PrTransformation?
 *
 * @author Jakub Kolman
 * @author Ondřej Bazala
 * @author Radek Oslejsek
 */
public interface LandmarkServices {

    /**
     * Computes and return transformation that registers landmarks from the second face the first face.
     *
     * @param staticFaceFPs Feature points of a face towards which the superimposition is performed. Must not be {@code null}.
     * @param transformedFaceFPs Feature points of a face to be transformed (superimposed). Must not be {@code null}.
     * @param scale Whether to scale the face as well
     * @throws IllegalArgumentException if there are not at least three common feature points in the faces
     */
    static PrTransformation getProcrustesTransformation(
            Collection<Landmark> staticFaceFPs,
            Collection<Landmark> transformedFaceFPs,
            boolean scale) {

        return ProcrustesImpl.getTransformation(staticFaceFPs,transformedFaceFPs, scale);
    }

    /**
     * Applies transformation to given points.
     * @param points Points to be transformed
     * @param transformation Transformation
     * @param <T> Point type, e.g., {@link MeshPoint}, {@link Landmark}, etc.
     */
    static <T extends IPosition> void transform(List<T> points, PrTransformation transformation) {
        points.forEach(fp -> {
            ProcrustesImpl.movePoint(fp, transformation.getCentroidAdjustment());
            ProcrustesImpl.scalePointDistance(fp, transformation.getScale());
            ProcrustesImpl.rotateVertex(fp, transformation.getRotationMatrix());
            ProcrustesImpl.movePoint(fp, transformation.getSuperImpositionAdjustment());
        });
    }

    /**
     * Calculation of symmetry plane from landmarks. Only landmarks with "_L" and "_R" suffix
     * in their code are taken into account.
     *
     * @param landmarks Landmarks
     * @return Symmetry plane
     */
    static Plane computeSymmetryPlane(Collection<Landmark> landmarks) {
        return LandmarksSymmetryImpl.computeSymmetryPlane(landmarks);
    }
}
