package cz.fidentis.analyst.engines.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.mesh.IPosition;
import cz.fidentis.analyst.engines.landmarks.PrTransformation;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <a href="https://en.wikipedia.org/wiki/Procrustes_analysis">Procrustes superimposition</a> algorithm
 * that computes transformation of faces from their feature points.
 * This class doesn't transform any face. Only a transformation matrix is computed.
 *
 * @author Jakub Kolman
 * @author Radek Oslejsek
 */
public class ProcrustesImpl {

    /**
     * Computes and return transformation that registers feature points from the second face to
     * feature points of the first face.
     *
     * @param staticFaceFPs Feature points of a face towards which the superimposition is performed. Must not be {@code null}.
     * @param transformedFaceFPs Feature points of a face to be transformed (superimposed). Must not be {@code null}.
     * @param scale Whether to scale the face as well
     * @throws IllegalArgumentException if there are not at least three common feature points in the faces
     */
    public static PrTransformation getTransformation(
            Collection<Landmark> staticFaceFPs,
            Collection<Landmark> transformedFaceFPs,
            boolean scale) {

        Collection<Landmark> commonFeaturePointTypes = Landmark.landmarksOfTheSameType(staticFaceFPs, transformedFaceFPs);
        if (commonFeaturePointTypes.size() < 3) {
            return null;
        }

        Map<Integer, Landmark> faceModel1 = createFaceModel(
                staticFaceFPs, commonFeaturePointTypes.stream().map(Landmark::getType).toList());

        Map<Integer, Landmark> faceModel2 = createFaceModel(
                transformedFaceFPs, commonFeaturePointTypes.stream().map(Landmark::getType).toList());

        Point3d modelCentroid1 = findCentroidOfFeaturePoints(faceModel1.values());
        Point3d modelCentroid2 = findCentroidOfFeaturePoints(faceModel2.values());

        PrTransformation transformation = new PrTransformation();

        // adjustment is used for superimposition and return to the original place
        transformation.setCentroidAdjustment(new Vector3d(-modelCentroid2.x, -modelCentroid2.y, -modelCentroid2.z));
        transformation.setSuperImpositionAdjustment(new Vector3d(modelCentroid1.x, modelCentroid1.y, modelCentroid1.z));

        // sets both faces to the origin of plains
        faceModel2.values().forEach(fp -> movePoint(fp, new Vector3d(-modelCentroid2.x, -modelCentroid2.y, -modelCentroid2.z)));
        faceModel1.values().forEach(fp -> movePoint(fp, new Vector3d(-modelCentroid1.x, -modelCentroid1.y, -modelCentroid1.z)));

        // calculation of scaling vector
        if (scale) {
            double scaleFactor = calculateScalingValue(faceModel1, faceModel2);
            faceModel2.values().forEach(fp -> scalePointDistance(fp, scaleFactor));
            transformation.setScale(scaleFactor);
        }

        // calculation of rotation matrix
        transformation.setRotationMatrix(rotate(faceModel1, faceModel2));

        return transformation;
    }

    private static Map<Integer, Landmark> createFaceModel(
            Collection<Landmark> featurePoints,
            Collection<Integer> viablePoints) {

        return featurePoints.stream()
                .filter(fp -> viablePoints.contains(fp.getType()))
                .collect(Collectors.toMap(Landmark::getType, Landmark::clone));
    }

    private static Point3d findCentroidOfFeaturePoints(Collection<Landmark> featurePointList) {
        Point3d ret = new Point3d();
        featurePointList.stream().map(Landmark::getPosition).forEach(ret::add);
        ret.scale(1.0/featurePointList.size());
        return ret;
    }

    /**
     * Moves point by given vector value.
     *
     * @param <T> Type of 3D point
     * @param point A 3D point to be moved
     * @param vector A movement vector
     */
    public static <T extends IPosition> void movePoint(T point, Vector3d vector) {
        point.getPosition().x = point.getX() + vector.x;
        point.getPosition().y = point.getY() + vector.y;
        point.getPosition().z = point.getZ() + vector.z;
    }

    /**
     * Calculate scaling ratio of how much the appropriate object corresponding
     * to the second feature point list has to be scale up or shrunk.
     * <p>
     * If returned ratioValue is greater 1 then it means that the second object
     * should be scaled up ratioValue times. If returned ratioValue is smaller 1
     * than the second object should be shrunk.
     *
     * @return ratioValue
     */
    private static double calculateScalingValue(Map<Integer, Landmark> faceModel1, Map<Integer, Landmark> faceModel2) {
        List<Landmark> featurePointList1 = new ArrayList<>(faceModel1.values());
        List<Landmark> featurePointList2 = new ArrayList<>(faceModel2.values());

        double[] distancesOfList1 = new double[featurePointList1.size()];
        double[] distancesOfList2 = new double[featurePointList2.size()];

        Point3d featurePointCentroid1 = findCentroidOfFeaturePoints(featurePointList1);
        Point3d featurePointCentroid2 = findCentroidOfFeaturePoints(featurePointList2);

        for (int i = 0; i < featurePointList1.size(); i++) {
            distancesOfList1[i] = featurePointList1.get(i).getPosition().distance(featurePointCentroid1);
            distancesOfList2[i] = featurePointList2.get(i).getPosition().distance(featurePointCentroid2);
        }

        double[] ratioArray = new double[distancesOfList1.length];
        double ratioValue = 0;

        for (int i = 0; i < distancesOfList1.length; i++) {
            ratioArray[i] += distancesOfList1[i] / distancesOfList2[i];
        }
        for (double v : ratioArray) {
            ratioValue += v;
        }
        return ratioValue / distancesOfList1.length;
    }

    /**
     * Scales position of given point by multiplying its coordinates with given
     * scaleFactor.
     *
     * @param point A 3D point to be scaled
     * @param scaleFactor Scale factor
     * @param <T> Type of the 3D point
     * @return point with recalculated values
     */
    public static <T extends IPosition> T scalePointDistance(T point, double scaleFactor) {
        point.getPosition().x = point.getX() * scaleFactor;
        point.getPosition().y = point.getY() * scaleFactor;
        point.getPosition().z = point.getZ() * scaleFactor;
        return point;
    }

    /**
     * By rotation of matrices solves orthogonal procrustes problem.
     *
     * @return rotation matrix
     */
    private static SimpleMatrix rotate(Map<Integer, Landmark> faceModel1, Map<Integer, Landmark> faceModel2) {
        SimpleMatrix primaryMatrix = createMatrixFromList(new ArrayList<>(faceModel2.values()));
        SimpleMatrix transposedMatrix = createMatrixFromList(new ArrayList<>(faceModel1.values())).transpose();

        SimpleMatrix svdMatrix = transposedMatrix.mult(primaryMatrix);
        SimpleSVD<SimpleMatrix> singularValueDecomposition = svdMatrix.svd();
        SimpleMatrix transposedU = singularValueDecomposition.getU().transpose();
        return singularValueDecomposition.getV().mult(transposedU);
    }

    /**
     * Creates matrix from given feature point list
     *
     * @param list Input list of points
     * @return matrix
     */
    private static <T extends IPosition> SimpleMatrix createMatrixFromList(List<T> list) {
        SimpleMatrix matrix = new SimpleMatrix(list.size(), 3);
        for (int i = 0; i < list.size(); i++) {
            matrix.set(i, 0, list.get(i).getPosition().x);
            matrix.set(i, 1, list.get(i).getPosition().y);
            matrix.set(i, 2, list.get(i).getPosition().z);
        }
        return matrix;
    }

    /**
     * Rotates vertex v by simulating matrix multiplication with given matrix
     *
     * @param v is vertex than is going to be rotated
     * @param matrix is rotation matrix
     */
    public static <T extends IPosition> void rotateVertex(T v, SimpleMatrix matrix) {
        double x = ((v.getX() * matrix.get(0, 0))
                + (v.getY() * matrix.get(1, 0))
                + (v.getZ() * matrix.get(2, 0)));
        double y = ((v.getX() * matrix.get(0, 1))
                + (v.getY() * matrix.get(1, 1))
                + (v.getZ() * matrix.get(2, 1)));
        double z = ((v.getX() * matrix.get(0, 2))
                + (v.getY() * matrix.get(1, 2))
                + (v.getZ() * matrix.get(2, 2)));
        v.getPosition().x = x;
        v.getPosition().y = y;
        v.getPosition().z = z;
    }
}
