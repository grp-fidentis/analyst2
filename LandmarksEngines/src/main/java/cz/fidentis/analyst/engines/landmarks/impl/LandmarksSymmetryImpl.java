package cz.fidentis.analyst.engines.landmarks.impl;

import cz.fidentis.analyst.data.landmarks.Landmark;
import cz.fidentis.analyst.data.shapes.Plane;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.*;

/**
 * @author Ondřej Bazala
 * @author Radek Oslejsek
 */
public class LandmarksSymmetryImpl {

    /**
     * Calculation of symmetry plane from landmarks. Only landmarks with "_L" and "_R" suffix
     * in their code are taken into account.
     *
     * @param landmarks Landmarks
     * @return Symmetry plane
     */
    public static Plane computeSymmetryPlane(Collection<Landmark> landmarks) {
        Map<String, Point3d> toBeProcessedFeaturePoints = new HashMap<>();
        List<Plane> planes = new ArrayList<>();

        List<Landmark> leftOrRightFeaturePoints = landmarks.stream()
                .filter(f -> f.getCode().endsWith("_R") || f.getCode().endsWith("_L"))
                .toList();

        for (Landmark featurePoint: leftOrRightFeaturePoints) {
            String code = featurePoint.getCode();
            String codeEnding = code.endsWith("_R") ? "_R" : "_L";
            String replacement = code.endsWith("_R") ? "_L" : "_R";
            String pairCode = code.replace(codeEnding, replacement);

            if (toBeProcessedFeaturePoints.containsKey(pairCode)) {
                Vector3d normal = new Vector3d(toBeProcessedFeaturePoints.remove(pairCode));
                Vector3d center = new Vector3d(normal);

                normal.sub(featurePoint.getPosition());
                normal.normalize();

                center.add(featurePoint.getPosition());
                center.scale(0.5);

                planes.add(new Plane(normal, normal.dot(center)));
            } else {
                toBeProcessedFeaturePoints.put(code, featurePoint.getPosition());
            }
        }

        return new Plane(planes); // creates an average plane
    }
}
