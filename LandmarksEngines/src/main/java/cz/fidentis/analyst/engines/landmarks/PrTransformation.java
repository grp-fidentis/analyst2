package cz.fidentis.analyst.engines.landmarks;

import cz.fidentis.analyst.math.Quaternion;
import org.ejml.simple.SimpleMatrix;

import javax.vecmath.Vector3d;

/**
 * Class used to store information about transformation that needs to be applied
 * to a second face when computing Procrustes analysis.
 *
 * @author Jakub Kolman
 * @author Radek Oslejsek
 */
public class PrTransformation {

    private double scale;

    /**
     * Used for adjustment to move feature points (0,0,0) so the face is rotated around the centroid point
     */
    private Vector3d centroidAdjustment;

    /**
     * used to move face to superimposed position over the original human face
     */
    private Vector3d superImpositionAdjustment;

    private SimpleMatrix rotationMatrix;

    /**
     * constructor
     * 
     * @param scale determines by how much should be faced enlarged or minimized
     * @param centroidAdjustment is a vector determining how should be the face moved to make (0,0,0)
     * @param superImpositionAdjustment is a vector determining how should be the face
     *                                  moved to be superimposed over primary face
     * @param rotationMatrix determines how to rotate face
     */
    public PrTransformation(
            double scale,
            Vector3d centroidAdjustment,
            Vector3d superImpositionAdjustment,
            SimpleMatrix rotationMatrix) {
        this.scale = scale;
        this.centroidAdjustment = centroidAdjustment;                   
        this.superImpositionAdjustment = superImpositionAdjustment;
        this.rotationMatrix = rotationMatrix;
    }


    /**
     * Empty constructor with values that won't transform face. To set values later attribute setters can be used.
     * <pre>
     * rotation matrix is set to identity matrix
     * scale is 1
     * adjustment is none
     * </pre>
     */
    public PrTransformation() {
        this(1, new Vector3d(0,0,0), new Vector3d(0,0,0), new SimpleMatrix(3, 3));
        rotationMatrix.set(0, 0, 1);
        rotationMatrix.set(0, 1, 0);
        rotationMatrix.set(0, 2, 0);
        rotationMatrix.set(1, 0, 0);
        rotationMatrix.set(1, 1, 1);
        rotationMatrix.set(1, 2, 0);
        rotationMatrix.set(2, 0, 0);
        rotationMatrix.set(2, 1, 0);
        rotationMatrix.set(2, 2, 1);
    }

    public double getScale() {
        return this.scale;
    }

    public Vector3d getCentroidAdjustment() {
        return this.centroidAdjustment;
    }

    public Vector3d getSuperImpositionAdjustment() {
        return this.superImpositionAdjustment;
    }

    public SimpleMatrix getRotationMatrix() {
        return this.rotationMatrix;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    /**
     * Calculates quaternion from rotation matrix.
     * 
     * @param matrix Input matrix
     * @return quaternion with rotation values
     */
    public Quaternion getMatrixAsQuaternion(SimpleMatrix matrix) {
        if (matrix != null) {           
            double w = Math.sqrt(1.0 + matrix.get(0, 0) + matrix.get(1, 1) + matrix.get(2, 2)) / 2.0;
            double w4 = (4.0 * w);
            double x = (matrix.get(2, 1) - matrix.get(1, 2)) / w4;
            double y = (matrix.get(0, 2) - matrix.get(2, 0)) / w4;
            double z = (matrix.get(1, 0) - matrix.get(0, 1)) / w4;
            return new Quaternion(x, y, z, w);
        } else {
            return null;
        }
    }

    public void setCentroidAdjustment(Vector3d centroidAdjustment) {
        this.centroidAdjustment = centroidAdjustment;
    }

    public void setSuperImpositionAdjustment(Vector3d superImpositionAdjustment) {
        this.superImpositionAdjustment = superImpositionAdjustment;
    }

    public void setRotationMatrix(SimpleMatrix rotationMatrix) {
        this.rotationMatrix = rotationMatrix;
    }
    
    /**
     * Generates an identity matrix which doesn't cause any transformation and returns it.
     * 
     * @return identity matrix
     */
    public static SimpleMatrix getIdentityMatrix() {
        SimpleMatrix r = new SimpleMatrix(3, 3);
        r.set(0, 0, 1);
        r.set(0, 1, 0);
        r.set(0, 2, 0);
        r.set(1, 0, 0);
        r.set(1, 1, 1);
        r.set(1, 2, 0);
        r.set(2, 0, 0);
        r.set(2, 1, 0);
        r.set(2, 2, 1);
        return r;
   }

}
