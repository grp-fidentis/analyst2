package cz.fidentis.analyst.data.grid;

import javax.vecmath.Point3d;

import cz.fidentis.analyst.data.grid.impl.UniformGrid3dImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author Radek Oslejsek
 */
public class UniformGrid3DImplTest {
    
    @Test
    public void testStoreAndGet() {
        UniformGrid3dImpl<String> grid = getGrid();
        
        Assertions.assertEquals("[000]", grid.get(new Point3d(0.25, 0.25, 0.25)).toString());
        Assertions.assertEquals("[010]", grid.get(new Point3d(0.25, 0.75, 0.25)).toString());
        Assertions.assertEquals("[002]", grid.get(new Point3d(0.25, 0.25, 1.25)).toString());
        
        Assertions.assertEquals("[000]", grid.get(new Point3d(0.15, 0.15, 0.15)).toString());
        Assertions.assertEquals("[010]", grid.get(new Point3d(0.15, 0.65, 0.15)).toString());
        Assertions.assertEquals("[002]", grid.get(new Point3d(0.15, 0.15, 1.15)).toString());
        
        Assertions.assertEquals("[]", grid.get(new Point3d(-0.15, 0.15, 0.15)).toString());
        Assertions.assertEquals("[]", grid.get(new Point3d( 2.15, 0.15, 0.15)).toString());
        
        Assertions.assertEquals("[000]", grid.get(new Point3d(0.0, 0.0, 0.0)).toString());
        Assertions.assertEquals("[111]", grid.get(new Point3d(0.5, 0.5, 0.5)).toString());
        Assertions.assertEquals("[121]", grid.get(new Point3d(0.5, 1.0, 0.5)).toString());
    }
    
    @Test
    public void testClosest() {
        UniformGrid3dImpl<String> grid = getGrid();
        
        Assertions.assertEquals(27, grid.getClosest(new Point3d(0.75, 0.75, 0.75)).size()); // all cells
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("000"));
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("100"));
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("010"));
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("001"));
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("101"));
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("110"));
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("011"));
        Assertions.assertTrue(grid.getClosest(new Point3d(0.25, 0.25, 0.25)).contains("111"));
    }
    
    @Test
    public void testRemove() {
        UniformGrid3dImpl<String> grid = getGrid();
        grid.store(new Point3d(0.15, 0.15, 0.15), "aaa");
        grid.remove(new Point3d(0.15, 0.15, 0.15), "000");
        Assertions.assertEquals("[aaa]", grid.get(new Point3d(0.25, 0.25, 0.25)).toString());
        grid.remove(new Point3d(0.35, 0.35, 0.35), "aaa");
        Assertions.assertEquals("[]", grid.get(new Point3d(0.25, 0.25, 0.25)).toString());        
    }
     
    protected UniformGrid3dImpl<String> getGrid() {
        UniformGrid3dImpl<String> grid = new UniformGrid3dImpl<>(0.5);
        
        grid.store(new Point3d(0.25, 0.25, 0.25), "000");
        
        grid.store(new Point3d(0.75, 0.25, 0.25), "100");
        grid.store(new Point3d(0.25, 0.75, 0.25), "010");
        grid.store(new Point3d(0.25, 0.25, 0.75), "001");
        
        grid.store(new Point3d(0.75, 0.75, 0.25), "110");
        grid.store(new Point3d(0.25, 0.75, 0.75), "011");
        grid.store(new Point3d(0.75, 0.25, 0.75), "101");
        
        grid.store(new Point3d(0.75, 0.75, 0.75), "111");
        
        grid.store(new Point3d(1.25, 0.25, 0.25), "200");
        grid.store(new Point3d(0.25, 1.25, 0.25), "020");
        grid.store(new Point3d(0.25, 0.25, 1.25), "002");
        
        grid.store(new Point3d(1.25, 1.25, 0.25), "220");
        grid.store(new Point3d(0.25, 1.25, 1.25), "022");
        grid.store(new Point3d(1.25, 0.25, 1.25), "202");
        
        grid.store(new Point3d(1.25, 1.25, 1.25), "222");
        
        grid.store(new Point3d(1.25, 0.75, 0.75), "211");
        grid.store(new Point3d(0.75, 1.25, 0.75), "121");
        grid.store(new Point3d(0.75, 0.75, 1.25), "112");
        
        grid.store(new Point3d(1.25, 1.25, 0.75), "221");
        grid.store(new Point3d(0.75, 1.25, 1.25), "122");
        grid.store(new Point3d(1.25, 0.75, 1.25), "212");
        
        grid.store(new Point3d(0.25, 0.75, 1.25), "012");
        grid.store(new Point3d(0.25, 1.25, 0.75), "021");
        grid.store(new Point3d(0.75, 0.25, 1.25), "102");
        grid.store(new Point3d(1.25, 0.25, 0.75), "201");
        grid.store(new Point3d(0.75, 1.25, 0.25), "120");
        grid.store(new Point3d(1.25, 0.75, 0.25), "210");
        
        return grid;
    }
}
