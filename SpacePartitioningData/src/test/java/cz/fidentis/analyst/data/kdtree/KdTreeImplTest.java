package cz.fidentis.analyst.data.kdtree;

import cz.fidentis.analyst.data.kdtree.KdNode;
import cz.fidentis.analyst.data.kdtree.impl.KdTreeImpl;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.impl.MeshPointImpl;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * @author Maria Kocurekova
 */

public class KdTreeImplTest {

    private Point3d position = new Point3d(0.1f, 0.5f, 0.7f);
    private Vector3d normalAndTextCoord = new Vector3d(0,0,0);
    

    private void testCorrectlyBuiltTree(KdTreeImpl tree) {
        if (tree.getRoot() == null) {
            return;
        }
        
        final Queue<KdNode> queue = new LinkedList<>();
        queue.add(tree.getRoot());
        
        while (!queue.isEmpty()) {
            final KdNode node = queue.poll();
            
            final KdNode lesser = node.getLesser();
            if (lesser != null) {
                switch (node.getDepth() % 3) {
                    case 0:
                        assertTrue(node.getLocation().x > lesser.getLocation().x);
                        break;
                    case 1:
                        assertTrue(node.getLocation().y > lesser.getLocation().y);
                        break;
                    default:
                        assertTrue(node.getLocation().z > lesser.getLocation().z);
                }
                queue.add(lesser);
            }
            
            final KdNode greater = node.getGreater();
            if (greater != null) {
                switch (node.getDepth() % 3) {
                    case 0:
                        assertTrue(node.getLocation().x <= greater.getLocation().x);
                        break;
                    case 1:
                        assertTrue(node.getLocation().y <= greater.getLocation().y);
                        break;
                    default:
                        assertTrue(node.getLocation().z <= greater.getLocation().z);
                }
                queue.add(greater);
            }
        }
    }
    
    protected boolean containsPoint(KdNode node, MeshPoint p) {
        if (node == null) {
            return false;
        }
        if (node.getLocation().equals(p.getPosition())) {
            return true;
        }
        if (containsPoint(node.getLesser(), p)) {
            return true;
        }
        if (containsPoint(node.getGreater(), p)) {
            return true;
        }
        return false;
    }
    
    @Test
    public void testCorrectBuild() {
        final Set<MeshPoint> points = new HashSet<>();
        points.add(new MeshPointImpl(new Point3d(-331, 203, 320), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(-371, -222, -111), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(-223, 190, 113), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(275, -414, -378), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(-357, 98, -217), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(297, 403, 299), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(145, 252, -77), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(319, 13, 87), normalAndTextCoord, normalAndTextCoord));
        points.add(new MeshPointImpl(new Point3d(-284, 40, -231), normalAndTextCoord, normalAndTextCoord));
        
        final KdTreeImpl tree = new KdTreeImpl(points);
        testCorrectlyBuiltTree(tree);
    }
    
    @Test
    public void testCorrectBuildUnbalanced() {
        final Set<MeshPoint> treeNodes = new HashSet<>();
        treeNodes.add(new MeshPointImpl(new Point3d( 1,  1,  1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d(-1,  1,  1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d( 1, -1,  1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d(-1, -1,  1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d( 1,  1, -1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d(-1,  1, -1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d( 1, -1, -1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d(-1, -1, -1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d( 1,  2,  4), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d( 1,  -7,  4), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d( 1,  5,  -1), normalAndTextCoord, normalAndTextCoord));
        treeNodes.add(new MeshPointImpl(new Point3d( 1,  -2, -8), normalAndTextCoord, normalAndTextCoord));
        
        final KdTreeImpl kdTree = new KdTreeImpl(treeNodes);
        testCorrectlyBuiltTree(kdTree);
    }
    
    @Test
    public void testPut(){
        MeshPoint p = new MeshPointImpl(position,normalAndTextCoord,normalAndTextCoord);

        List<MeshFacet> facets = new LinkedList<>();
        MeshFacet facet = MeshFactory.createEmptyMeshFacet();
        facet.addVertex(p);
        facets.add(facet);
        KdTreeImpl tree = new KdTreeImpl(facets);
        testCorrectlyBuiltTree(tree);
        
        assertTrue(containsPoint(tree.getRoot(), p));
    }

    @Test
    public void testPutNothing(){
        MeshPoint p = new MeshPointImpl(position,normalAndTextCoord,normalAndTextCoord);

        List<MeshFacet> facets2 = new LinkedList<>();
        KdTreeImpl tree = new KdTreeImpl(facets2);
        testCorrectlyBuiltTree(tree);

        assertFalse(containsPoint(tree.getRoot(), p));
    }

    /**
     * Returns the closest point
     *
     * @param points list of points
     * @param p point to which the closest point is searched
     * @return the closest point to p
     */
    private MeshPoint findClosestSequential(List<MeshPoint> points, MeshPoint p){
        if(points.isEmpty()){
            return null;
        }

        MeshPoint closest = points.get(0);
        double dis = closest.getPosition().distance(p.getPosition());

        for(MeshPoint cl : points){
            if(cl.getPosition().distance(p.getPosition()) < dis){
                dis = cl.getPosition().distance(p.getPosition());
                closest = cl;
            }
        }

        return closest;
    }

    @Test
    public void testFindClosestAlreadyIn(){
        List<MeshFacet> facets = new LinkedList<>();

        MeshFacet facet = MeshFactory.createEmptyMeshFacet();

        List<MeshPoint> points = new LinkedList<>();
        Point3d positionOfPoints;

        for(int i = 0; i < 1000; i++){
            positionOfPoints = new Point3d(0.1f * i, 0.5f * i, 0.7f * i);
            points.add(new MeshPointImpl(positionOfPoints, normalAndTextCoord, normalAndTextCoord));
            facet.addVertex(new MeshPointImpl(positionOfPoints, normalAndTextCoord, normalAndTextCoord));
        }
        facets.add(facet);
        KdTreeImpl tree = new KdTreeImpl(facets);
        testCorrectlyBuiltTree(tree);

        Random r = new Random();

        MeshPoint p = points.get(r.nextInt(points.size()));
        assertTrue(containsPoint(tree.getRoot(), p));
    }

    @Test
    public void testFindClosestAlreadyInTwoFacets(){
        List<MeshFacet> facets = new LinkedList<>();

        MeshFacet facet1 = MeshFactory.createEmptyMeshFacet();
        MeshFacet facet2 = MeshFactory.createEmptyMeshFacet();

        List<MeshPoint> points = new LinkedList<>();
        Point3d positionOfPoints;

        for(int i = 0; i < 5; i++){
            positionOfPoints = new Point3d(0.1f * i, 0.5f * i, 0.7f * i);
            points.add(new MeshPointImpl(positionOfPoints, normalAndTextCoord, normalAndTextCoord));
            facet1.addVertex(new MeshPointImpl(positionOfPoints, normalAndTextCoord, normalAndTextCoord));
        }
        for(int i = 5; i < 10; i++){
            positionOfPoints = new Point3d(0.1f * i, 0.5f * i, 0.7f * i);
            points.add(new MeshPointImpl(positionOfPoints, normalAndTextCoord, normalAndTextCoord));
            facet2.addVertex(new MeshPointImpl(positionOfPoints, normalAndTextCoord, normalAndTextCoord));
        }
        facets.add(facet1);
        facets.add(facet2);
        KdTreeImpl tree = new KdTreeImpl(facets);
        testCorrectlyBuiltTree(tree);

        Random r = new Random();

        MeshPoint p = points.get(r.nextInt(points.size()));
        assertTrue(containsPoint(tree.getRoot(), p));
    }
}


