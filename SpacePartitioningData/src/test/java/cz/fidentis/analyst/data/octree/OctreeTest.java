package cz.fidentis.analyst.data.octree;

import cz.fidentis.analyst.data.mesh.MeshIO;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.OctNode;
import cz.fidentis.analyst.data.octree.impl.OctreeImpl;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Enkh-Undral EnkhBayar
 */
public class OctreeTest {
    
    private final Path testFileDirectory = Paths.get("src", "test", "resources", "cz", "fidentis", "analyst");
    
    private boolean isTriangleInCube(MeshTriangle triangle, OctNode node) {
        Point3d[] boundingBox = {
            new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY),
            new Point3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY)
        };
        for (MeshPoint vertex : triangle) {
            Point3d position = vertex.getPosition();
            boundingBox[0].x = Double.min(boundingBox[0].x, position.x);
            boundingBox[0].y = Double.min(boundingBox[0].y, position.y);
            boundingBox[0].z = Double.min(boundingBox[0].z, position.z);
            boundingBox[1].x = Double.max(boundingBox[1].x, position.x);
            boundingBox[1].y = Double.max(boundingBox[1].y, position.y);
            boundingBox[1].z = Double.max(boundingBox[1].z, position.z);
        }
        
        Point3d smallest = node.getLowerBound();
        Point3d largest = node.getUpperBound();
        if (!(boundingBox[1].x >= smallest.x && largest.x >= boundingBox[0].x)) {
            return false;
        }
        if (!(boundingBox[1].y >= smallest.y && largest.y >= boundingBox[0].y)) {
            return false;
        }
        return boundingBox[1].z >= smallest.z && largest.z >= boundingBox[0].z;
    }
    
    void checkOctree(OctreeImpl octree) {
        assertTrue(octree.getRoot() != null);
        checkOctSubTree(octree.getRoot());
    }
    
    void checkOctSubTree(OctNode node) {
        if (node.isLeafNode()) {
            for (int i = 0; i < 8; i++) {
                assertTrue(node.getOctant(i) == null);
            }
            assertTrue(node.getTriangles() != null);
            for (MeshTriangle triangle : node.getTriangles()) {
                assertTrue(isTriangleInCube(triangle, node));
            }
        } else {
            for (int i = 0; i < 8; i++) {
                assertTrue(node.getOctant(i) != null);
                checkOctSubTree(node.getOctant(i));
            }
            assertTrue(node.getTriangles() == null || node.getTriangles().isEmpty());
        }
    }
    
    @Test
    void icoSphereOctreeTest() throws IOException {
        File icoSphere = new File(testFileDirectory.toFile(), "IcoSphere-Triangles.obj");
        MeshModel m = MeshIO.readMeshModel(icoSphere);
        
        OctreeImpl octree = new OctreeImpl(m.getFacets());
        checkOctree(octree);
    }
}