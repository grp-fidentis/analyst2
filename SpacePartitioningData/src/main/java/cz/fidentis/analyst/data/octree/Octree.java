package cz.fidentis.analyst.data.octree;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.octree.impl.OctreeImpl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

/**
 * {@code Octree} for storing vertices ({@code MeshPoint}s) of triangular meshes
 * ({@code MeshFacet}s). Multiple mesh facets can be stored in a single
 * {@code Octree}. In this case, vertices that are shared across multiple facets
 * (have the same 3D location) are shared in the same node of the
 * {@code Octree}.
 * <p>
 * Triangles are stored in leaves. If a triangle intersects multiple leaves,
 * then it is stored in all of them. The intersection of bounding boxes is
 * used to detect whether a triangle intersects an octree node.
 * </p>
 *
 * @author Enkh-Undral EnkhBayar
 * @author Radek Oslejsek
 */
public interface Octree extends Serializable {

    /**
     * Creates a new OctTree.
     *
     * @param facet Mesh facet
     * @return a new OctTree
     */
    static Octree create(MeshFacet facet) {
        return create(Collections.singleton(facet));
    }

    /**
     * Creates a new OctTree.
     *
     * @param mesh Mesh model
     * @return a new OctTree
     */
    static Octree create(MeshModel mesh) {
        return create(mesh.getFacets());
    }

    /**
     * Creates a new OctTree.
     * If no mesh points (vertices) are provided, then an empty {@code Octree}
     * is constructed (with the root node set to null). If multiple mesh facets
     * share the same vertex, then they are stored efficiently in the same node
     * of the {@code Octree}.
     *
     * @param facets The list of mesh facets to be stored. Facets can share
     * @return a new OctTree
     */
    static Octree create(Collection<MeshFacet> facets) {
        return new OctreeImpl(facets);
    }

    /**
     * Tree traversal - go to the "root" of the tree.
     *
     * @return root node of the tree
     */
    OctNode getRoot();

    /**
     * @return distance of the smallest cell in {@code Octree}.
     */
    Double getMinLen();

    /**
     * Visits this tree.
     *
     * @param visitor Visitor
     */
    void accept(OctreeVisitor visitor);
}
