package cz.fidentis.analyst.data.kdtree.impl;

import cz.fidentis.analyst.data.kdtree.KdNode;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.io.Serial;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.vecmath.Point3d;

/**
 * Implementation of k-D node.
 *
 * @author Maria Kocurekova
 */
public class KdNodeImpl implements KdNode {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * Current depth in the kd-tree
     */
    private final int depth;
    
    /**
     * 3D location of the node
     */
    private final Point3d location;
    
    /**
     * Mesh facets sharing the stored vertex
     */
    private final Map<MeshFacet, Integer> facets = new HashMap<>();

    /**
     * KD-tree topology
     */
    private KdNode lesser = null;
    private KdNode greater = null;

    /**
     * Constructor for storing a vertex of a mesh facet.
     *
     * @param facet Mesh facet containing the mesh vertex. Must not be null
     * @param index The index under which the vertex is stored in the mesh facet.
     *              Must be &gt;= 0
     * @param depth Depth of the node in the kd-tree. Must be &gt;= 0
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public KdNodeImpl(MeshFacet facet, int index, int depth) {
        if (facet == null) {
            throw new IllegalArgumentException("facet");
        }
        if (index < 0) {
            throw new IllegalArgumentException("index");
        }
        if (depth < 0) {
            throw new IllegalArgumentException("depth");
        }
        this.facets.putIfAbsent(facet, index);
        this.depth = depth;
        this.location = new Point3d(facet.getVertex(index).getPosition());
    }
    
    /**
     * Constructor for storing a vertex belonging to multiple mesh facets.
     * 
     * @param facets Mesh facets containing the mesh vertex. Must not be null or empty
     * @param indices Indices under which the vertex is stored in the mesh facet.
     *              Must be &gt;= 0 and their unmber has to correspond to the number of facets.
     * @param depth Depth of the node in the kd-tree. Must be &gt;= 0
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public KdNodeImpl(List<MeshFacet> facets, List<Integer> indices, int depth) {
        if (facets == null || facets.isEmpty()) {
            throw new IllegalArgumentException("facets");
        }
        if (indices == null || indices.isEmpty()) {
            throw new IllegalArgumentException("indices");
        }
        if (depth < 0) {
            throw new IllegalArgumentException("depth");
        }
        if (facets.size() != indices.size()) {
            throw new IllegalArgumentException("The number of facets and indiecs mismatch");
        }
        for (int i = 0; i < facets.size(); i++) {
            this.facets.putIfAbsent(facets.get(i), indices.get(i));
        }
        this.depth = depth;
        this.location = new Point3d(facets.get(0).getVertex(indices.get(0)).getPosition());
    }

    /**
     * Constructor for storing an independent point.
     *
     * @param point A 3D point. Must not be null
     * @param depth Depth of the node in the kd-tree. Must be &gt;= 0
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public KdNodeImpl(Point3d point, int depth) {
        if (point == null) {
            throw new IllegalArgumentException("point");
        }
        if (depth < 0) {
            throw new IllegalArgumentException("depth");
        }
        this.depth = depth;
        this.location = point;
    }

    /**
     * Constructor for storing an independent mesh point.
     *
     * @param point A meshpoint. Must not be null
     * @param depth Depth of the node in the kd-tree. Must be &gt;= 0
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public KdNodeImpl(MeshPoint point, int depth) {
        if (point == null) {
            throw new IllegalArgumentException("point");
        }
        if (depth < 0) {
            throw new IllegalArgumentException("depth");
        }
        this.depth = depth;
        this.location = point.getPosition();
    }

    public int getDepth() {
        return depth;
    }

    @Override
    public Point3d getLocation() {
        return this.location;
    }

    @Override
    public KdNode getLesser() {
        return lesser;
    }

    @Override
    public KdNode getGreater() {
        return greater;
    }

    @Override
    public KdNode setLesser(KdNode lesser) {
        this.lesser = lesser;
        return this;
    }

    @Override
    public KdNode setGreater(KdNode greater) {
        this.greater = greater;
        return this;
    }

    @Override
    public Map<MeshFacet, Integer> getFacets() {
        return Collections.unmodifiableMap(facets);
    }

    @Override
    public String toString() {
        String ret = "";
        for (int i = 0; i < depth; i++) {
            ret += " ";
        }
        ret += "Node position: " + getLocation() + ", num. facets:  " + facets.size();
        ret += System.lineSeparator();
        return ret;
    }

}
