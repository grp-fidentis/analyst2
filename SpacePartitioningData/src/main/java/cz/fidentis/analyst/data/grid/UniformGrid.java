package cz.fidentis.analyst.data.grid;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * Infinite multidimensional uniform grid with the given cell size.
 *
 * @param <K> grid's dimension, i.e., {@code Point2d}, {@code Point3d}, {@code Point4d}, etc.
 * @param <V> the type of elements to be stored in the grid
 * @author Radek Oslejsek
 */
public interface UniformGrid<K,V> {

    /**
     * Returns cell size.
     * @return cell size
     */
    double getCellSize();

    /**
     * Returns number of non-empty cells.
     * @return number of non-empty cells.
     */
    int numOccupiedCells();

    /**
     * Returns non-empty cells (in random order)
     * @return non-empty cells
     */
    List<List<V>> getNonEmptyCells();

    /**
     * Clear the grid, i.e., removes all stored values.
     */
    void clear();

    /**
     * Stores an object located in given 3D position in the grid.
     *
     * @param loc 3D location of the object. Must not be {@code null}
     * @param object Object to store.
     */
    void store(K loc, V object);

    /**
     * Stores given objects into the grid.
     * As the mapping function, use {@code (MeshPoint mp) -> mp.getPosition()}
     * or {@code (Point3d p) -> p}, for instance.
     *
     * @param objects Objects to be stored
     * @param mapFunc A function that computes a space location from an object
     */
    void store(Collection<V> objects, Function<? super V, ? extends K> mapFunc);

    /**
     * Returns objects stored in the cell of given location in space.
     * @param loc location in space
     * @return objects stored in the cell or empty collection
     */
    List<V> get(K loc);

    /**
     * Returns all objects stored in the grid.
     * @return all objects stored in the grid
     */
    List<V> getAll();

    /**
     * Removes an object located in the space
     * @param loc Object's location in space
     * @param object Object to be removed
     * @return {@code true} if the objects was found and removed, {@code false} otherwise.
     */
    boolean remove(K loc, V object);

    /**
     * Returns all objects that can be closer that the cell size.
     *
     * @param loc location in space
     * @return objects that can be closer that the cell size or empty collection.
     */
    List<V> getClosest(K loc);
}
