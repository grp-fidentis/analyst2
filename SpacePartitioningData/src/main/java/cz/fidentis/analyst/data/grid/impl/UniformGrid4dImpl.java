package cz.fidentis.analyst.data.grid.impl;

import cz.fidentis.analyst.data.grid.UniformGrid4d;

import javax.vecmath.Point4i;
import javax.vecmath.Tuple4d;
import javax.vecmath.Tuple4i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * A 4D uniform grid. This grid is infinite with given cell size (only occupied cells are remembered)
 * 
 * @param <V> the type of elements to be stored in the grid
 * @author Radek Oslejsek
 */
public class UniformGrid4dImpl<V> extends UniformGridImpl<Tuple4i, Tuple4d, V> implements UniformGrid4d<V> {
    
    /**
     * Constructor.
     * 
     * @param cellSize Cell size. Must be bigger that one.
     */
    public UniformGrid4dImpl(double cellSize) {
        super(cellSize);
    }
    
    /**
     * Constructor.
     * As the mapping function, use {@code (Point4d p) -> p}, for instance.
     * 
     * @param cellSize Cell size. Must be bigger that one.
     * @param objects Objects to be stored
     * @param mapFunc A function that computes a 3D space location from an object
     */
    public UniformGrid4dImpl(double cellSize, Collection<V> objects, Function<? super V, Tuple4d> mapFunc) {
        this(cellSize);
        store(objects, mapFunc);
    }
    
    @Override
    public Tuple4i locationToCell(Tuple4d loc) {
        Point4i p = new Point4i(
                ((int) (loc.x / getCellSize())),
                ((int) (loc.y / getCellSize())),
                ((int) (loc.z / getCellSize())),
                ((int) (loc.w / getCellSize())));
        
        if (loc.x < 0) {
            p.x -= 1;
        }
        if (loc.y < 0) {
            p.y -= 1;
        }
        if (loc.z < 0) {
            p.z -= 1;
        }
        if (loc.w < 0) {
            p.w -= 1;
        }
        
        return p;
    }
    
    @Override
    public List<Tuple4i> getAdjacentCells(Tuple4d loc) {
        List<Tuple4i> ret = new ArrayList<>();
        Tuple4i cell = locationToCell(loc);
        ret.add(cell);
        addNeighbours(ret, cell);
        return ret;
    }
    
    @Override
    public String toString() {
        return "Keys: " + getGrid().keySet().size() + ", values: " + getAll().size();
    }
    
    protected void addNeighbours(Collection<Tuple4i> points, Tuple4i cell) {
        points.add(neighbour(cell,  1,  0,  0,  0));
        points.add(neighbour(cell,  0,  1,  0,  0));
        points.add(neighbour(cell,  0,  0,  1,  0));
        points.add(neighbour(cell,  0,  0,  0,  1));
        
        points.add(neighbour(cell, -1,  0,  0,  0));
        points.add(neighbour(cell,  0, -1,  0,  0));
        points.add(neighbour(cell,  0,  0, -1,  0));
        points.add(neighbour(cell,  0,  0,  0, -1));
        
        points.add(neighbour(cell,  1,  1,  0,  0));
        points.add(neighbour(cell,  0,  1,  1,  0));
        points.add(neighbour(cell,  0,  0,  1,  1));
        points.add(neighbour(cell,  1,  0,  1,  0));
        points.add(neighbour(cell,  0,  1,  0,  1));
        points.add(neighbour(cell,  1,  0,  0,  1));
        
        points.add(neighbour(cell, -1, -1,  0,  0));
        points.add(neighbour(cell,  0, -1, -1,  0));
        points.add(neighbour(cell,  0,  0, -1, -1));
        points.add(neighbour(cell, -1,  0, -1,  0));
        points.add(neighbour(cell,  0, -1,  0, -1));
        points.add(neighbour(cell, -1,  0,  0, -1));
        
        points.add(neighbour(cell,  1, -1,  0,  0));
        points.add(neighbour(cell, -1,  1,  0,  0));
        points.add(neighbour(cell,  0,  1, -1,  0));
        points.add(neighbour(cell,  0, -1,  1,  0));
        points.add(neighbour(cell,  0,  0,  1, -1));
        points.add(neighbour(cell,  0,  0, -1,  1));
        points.add(neighbour(cell,  1,  0, -1,  0));
        points.add(neighbour(cell, -1,  0,  1,  0));
        points.add(neighbour(cell,  0,  1,  0, -1));
        points.add(neighbour(cell,  0, -1,  0,  1));
        points.add(neighbour(cell,  1,  0,  0, -1));
        points.add(neighbour(cell, -1,  0,  0,  1));
        
        points.add(neighbour(cell,  1,  1,  1,  0));
        points.add(neighbour(cell,  1,  1,  0,  1));
        points.add(neighbour(cell,  1,  0,  1,  1));
        points.add(neighbour(cell,  0,  1,  1,  1));
        
        points.add(neighbour(cell, -1, -1, -1,  0));
        points.add(neighbour(cell, -1, -1,  0, -1));
        points.add(neighbour(cell, -1,  0, -1, -1));
        points.add(neighbour(cell,  0, -1, -1, -1));
        
        points.add(neighbour(cell, -1, -1,  1,  0));
        points.add(neighbour(cell,  1, -1, -1,  0));
        points.add(neighbour(cell, -1,  1, -1,  0));
        points.add(neighbour(cell,  1, -1,  1,  0));
        points.add(neighbour(cell,  1,  1, -1,  0));
        points.add(neighbour(cell, -1,  1,  1,  0));

        points.add(neighbour(cell, -1, -1,  0,  1));
        points.add(neighbour(cell, -1,  1,  0, -1));
        points.add(neighbour(cell,  1, -1,  0,  1));
        points.add(neighbour(cell,  1,  1,  0, -1));
        points.add(neighbour(cell, -1,  1,  0,  1));
        points.add(neighbour(cell,  1, -1,  0, -1));

        points.add(neighbour(cell, -1,  0, -1,  1));
        points.add(neighbour(cell,  1,  0, -1, -1));
        points.add(neighbour(cell, -1,  0,  1, -1));
        points.add(neighbour(cell,  1,  0, -1,  1));
        points.add(neighbour(cell,  1,  0,  1, -1));
        points.add(neighbour(cell, -1,  0,  1,  1));

        points.add(neighbour(cell,  0, -1, -1,  1));
        points.add(neighbour(cell,  0,  1, -1, -1));
        points.add(neighbour(cell,  0, -1,  1, -1));
        points.add(neighbour(cell,  0,  1, -1,  1));
        points.add(neighbour(cell,  0,  1,  1, -1));
        points.add(neighbour(cell,  0, -1,  1,  1));        
        
        points.add(neighbour(cell,  1,  1,  1,  1));
        points.add(neighbour(cell, -1, -1, -1, -1));
        points.add(neighbour(cell,  1, -1, -1, -1));
        points.add(neighbour(cell, -1,  1, -1, -1));
        points.add(neighbour(cell, -1, -1,  1, -1));
        points.add(neighbour(cell, -1, -1, -1,  1));
        
        points.add(neighbour(cell,  1,  1, -1, -1));
        points.add(neighbour(cell, -1, -1,  1,  1));
        points.add(neighbour(cell,  1, -1,  1, -1));
        points.add(neighbour(cell, -1,  1, -1,  1));
        points.add(neighbour(cell,  1, -1, -1,  1));
        points.add(neighbour(cell, -1,  1,  1, -1));
        
        points.add(neighbour(cell,  1,  1,  1, -1));
        points.add(neighbour(cell,  1,  1, -1,  1));
        points.add(neighbour(cell,  1, -1,  1,  1));
        points.add(neighbour(cell, -1,  1,  1,  1));

    }
    
    protected Tuple4i neighbour(Tuple4i p, int x, int y, int z, int w) {
        return new Point4i(p.x + x, p.y + y, p.z + z, p.w + w);
    }
}
