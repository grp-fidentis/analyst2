package cz.fidentis.analyst.data.octree;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.ray.Ray;

import javax.vecmath.Point3d;
import java.io.Serializable;
import java.util.List;

/**
 * A single node of an Octree. Sharing vertices across meshes is supported (the
 * node links all faces that share the same vertex).
 *
 * @author Enkh-Undral EnkhBayar
 * @author Radek Oslejsek
 */
public interface OctNode extends Serializable {

    /**
     * Returns {@code true} if the node is a leave node.
     * @return {@code true} if the node is a leave node.
     */
    boolean isLeafNode();

    /**
     * Returns a child node (octant) under specific index
     *
     * @param index index of the octant returned. Must be between 0 and 7 including.
     * @return Octant under specific index
     */
    OctNode getOctant(int index);

    /**
     * Returns a child node in which a given 3D point is located.
     * If the point lies on the dividing planes of the cube, then
     * the "lower" child (octant) is returned.
     * If this node is leaf, the {@code null} is returned.
     * It is supposed that the point is inside (or at the boundary) of the
     * node's bounding cube (see {@link #isPointInside(javax.vecmath.Point3d)}).
     *
     * @param point point, must not be {@code null}
     * @return Child node or {@code null}
     */
    OctNode getChild(Point3d point);

    /**
     * Returns triangles which this oct node holds
     *
     * @return triangles which this oct node holds
     */
    List<MeshTriangle> getTriangles();

    /**
     * returns boundary box - corner with the smallest coordinates.
     *
     * @return boundary box - corner with the smallest coordinates.
     */
    Point3d getLowerBound();

    /**
     * returns boundary box - corner with the largest coordinates.
     *
     * @return boundary box - corner with the largest coordinates.
     */
    Point3d getUpperBound();

    /**
     * Checks if a 3D point lies inside the cube
     *
     * @param point A 3D point to be checked. Must not be {@code null}.
     * @return {@code true} if the {@code point} is inside the node.
     */
    boolean isPointInside(Point3d point);

    /**
     * Calculates the intersection of a ray with this node.
     * If the ray's origin is inside the node's cube, then the second (leaving)
     * intersection is returned. Otherwise, the first (entering) intersection
     * is returned.
     *
     * @param ray Ray
     * @return returns the intersection in the direction of the ray
     * @throws NullPointerException if the {@code ray} is {@code null}
     */
    Point3d rayIntersection(Ray ray);

}
