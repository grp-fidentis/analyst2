package cz.fidentis.analyst.data.octree.impl;

import cz.fidentis.analyst.data.mesh.MeshTriangle;

import javax.vecmath.Point3d;
import java.util.List;

/**
 * A leaf node of an Octree.
 *
 * @author Enkh-Undral EnkhBayar
 */
public class OctNodeLeaf extends OctNodeImpl {
    
    /**
     * Constructor of a <b>leaf node</b>
     *
     * @param smallest boundary box - corner with the smallest coordinates. Must not
     * be null
     * @param largest boundary box - corner with the largest coordinates. Must not
     * be null
     * @param triangles Mesh triangles which this oct node holds, meaning a
     * bounding box of the triangle intersects with this oct node Must not be
     * null
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public OctNodeLeaf(Point3d smallest, Point3d largest, List<MeshTriangle> triangles) {
        super(smallest, largest, triangles, null);
        if (triangles == null) {
            throw new IllegalArgumentException("triangles");
        }
    }
}
