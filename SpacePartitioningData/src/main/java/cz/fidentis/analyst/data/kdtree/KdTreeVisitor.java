package cz.fidentis.analyst.data.kdtree;

/**
 * When instantiated, the object can be gradually applied to multiple k-d trees.
 * It inspects the state of the tree one by one, and (cumulatively) computes results.
 * <p>
 * Implement this interface whenever you want to define new algorithm over a k-d tree.
 * </p>
 * <p>
 * If the visitor is thread-safe, then a single instance of the visitor
 * can visit concurrently (and asynchronously) multiple k-d trees. Otherwise, 
 * the parallel inspection is still possible, but a new instance of the visitor 
 * has to be used for each k-d tree.
 * </p>
 *
 * @author Daniel Schramm
 */
@FunctionalInterface
public interface KdTreeVisitor {
    
    /**
     * Returns {@code true} if the implementation is thread-safe and then
     * <b>a single visitor instance</b> can be applied to multiple k-d trees simultaneously.
     * <p>
     * Thread-safe implementation means that any read or write from/to the visitor's 
     * state is protected by {@code synchronized}.
     * </p>
     * 
     * @return {@code true} if the implementation is thread-safe.
     */
    default boolean isThreadSafe() {
        return true;
    }
    
    /**
     * The main inspection method to be implemented by specific visitors.
     * 
     * @param kdTree K-d tree to be visited
     */
    void visitKdTree(KdTree kdTree);
}
