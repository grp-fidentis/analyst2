package cz.fidentis.analyst.data.grid;

import cz.fidentis.analyst.data.grid.impl.UniformGrid4dImpl;

import javax.vecmath.Tuple4d;
import java.util.Collection;
import java.util.function.Function;

/**
 * Infinite uniform 3D grid with the given cell size.
 *
 * @param <V> the type of elements to be stored in the grid
 * @author Radek Oslejsek
 */
public interface UniformGrid4d<V> extends UniformGrid<Tuple4d, V> {

    /**
     * Creates new 4D grid.
     *
     * @param cellSize Cell size. Must be bigger that one.
     * @return new 4D grid
     */
    static <V> UniformGrid4d<V> create(double cellSize) {
        return new UniformGrid4dImpl<>(cellSize);
    }

    /**
     * Creates new 4D grid.
     * As the mapping function, use {@code (Point4d p) -> p}, for instance.
     *
     * @param cellSize Cell size. Must be bigger that one.
     * @param objects Objects to be stored
     * @param mapFunc A function that computes a 4D space location from an object
     * @return new 4D grid
     */
    static <V> UniformGrid4d<V> create(double cellSize, Collection<V> objects, Function<? super V, Tuple4d> mapFunc) {
        return new UniformGrid4dImpl<>(cellSize, objects, mapFunc);
    }

}
