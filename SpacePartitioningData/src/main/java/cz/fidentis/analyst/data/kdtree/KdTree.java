package cz.fidentis.analyst.data.kdtree;

import cz.fidentis.analyst.data.kdtree.impl.KdTreeImpl;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshModel;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * k-D tree for storing vertices ({@code MeshPoint}s) of triangular ({@code MeshFacet}s).
 * Multiple mesh facets can be stored in a single kd-tree. In this case,
 * vertices that are shared across multiple facets (have the same 3D location)
 * are shared in the same node of the kd-tree.
 *
 * @author Maria Kocurekova
 * @author Radek Oslejsek
 * @author Karol Kovac
 */
public interface KdTree extends Serializable {

    /**
     * Creates a new k-D tree.
     *
     * @param points A set of individual mesh points.
     * If no mesh points are provided, then an empty
     * KD-tree is constructed (with the root node set to null).
     * @return a new k-D tree
     */
    static KdTree create(Set<MeshPoint> points) {
        return new KdTreeImpl(points);
    }

    /**
     * Creates a new k-D tree.
     * If no mesh points (vertices) are provided, then an empty
     * k-D tree is constructed (with the root node set to null).
     *
     * @param mesh Mesh model
     * @return a new k-D tree
     */
    static KdTree create(MeshModel mesh) {
        return create(mesh.getFacets());
    }

    /**
     * Creates a new k-D tree.
     * If no mesh points (vertices) are provided, then an empty
     * k-D tree is constructed (with the root node set to null).
     *
     * @param facet Mesh facet
     * @return a new k-D tree
     */
    static KdTree create(MeshFacet facet) {
        return create(Collections.singleton(facet));
    }

    /**
     * Creates a new k-D tree.
     * If no mesh points (vertices) are provided, then an empty
     * k-D tree is constructed (with the root node set to null).
     * If multiple mesh facets share the same vertex, then they are stored
     * efficiently in the same node of the k-D tree.
     *
     * @param facets The list of mesh facets to be stored. Facets can share vertices.
     * @return a new k-D tree
     */
    static KdTree create(Collection<MeshFacet> facets) {
        return new KdTreeImpl(facets);
    }

    /**
     * Tree traversal - go to the "root" of the tree.
     *
     * @return root node of the tree
     */
    KdNode getRoot();

    /**
     * Method for adding independent point to existing tree. The balance of the tree
     * is not guaranteed after adding elements, it depends on the distribution of the
     * added points. It can create long linear branches. In Poisson disk sub-sampling,
     * where this method is used, it isn't a problem.
     *
     * @param meshPoint point to add
     */
    void addNode(MeshPoint meshPoint);

    /**
     * Return number of nodes in the k-d tree.
     * @return number of nodes in the k-d tree
     */
    int getNumNodes();

    /**
     * Return the length of the longest path.
     * @return Return the length of the longest path.
     */
    int getDepth();

    /**
     * Visits this tree.
     *
     * @param visitor Visitor
     */
    void accept(KdTreeVisitor visitor);

}
