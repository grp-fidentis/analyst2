package cz.fidentis.analyst.data.kdtree.impl;

import cz.fidentis.analyst.data.kdtree.KdNode;
import cz.fidentis.analyst.data.kdtree.KdTree;
import cz.fidentis.analyst.data.kdtree.KdTreeVisitor;
import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshFactory;
import cz.fidentis.analyst.data.mesh.MeshPoint;

import javax.vecmath.Point3d;
import java.io.Serial;
import java.util.*;

/**
 * Implementation of k-D trees.
 *
 * @author Maria Kocurekova
 * @author Radek Oslejsek
 * @author Karol Kovac
 */
public class KdTreeImpl implements KdTree {

    @Serial
    private static final long serialVersionUID = 1L;

    private KdNode root;
    
    /**
     * Constructor.
     *
     * @param points A set of individual mesh points. 
     * If no mesh points are provided, then an empty 
     * KD-tree is constructed (with the root node set to null).
     */
    public KdTreeImpl(Set<MeshPoint> points) {
        if (points == null) {
            this.root = null;
            return;
        }
        MeshFacet newFacet = MeshFactory.createPointCloudFacet(points);
        buildTree(new LinkedList<>(Collections.singleton(newFacet)));
        //eventBus.post(new KdTreeCreated(""));
    }

    /**
     * Constructor. If no mesh points (vertices) are provided, then an empty 
     * KD-tree is constructed (with the root node set to null).
     * If multiple mesh facets share the same vertex, then they are stored 
     * efficiently in the same node of the KD-tree.
     *
     * @param facets The list of mesh facets to be stored. Facets can share vertices.
     */
    public KdTreeImpl(Collection<MeshFacet> facets) {
        if(facets == null ||  facets.isEmpty() || facets.stream().findAny().get().getVertices().isEmpty() ){
            this.root = null;
            return;
        }
        buildTree(facets);    
        //eventBus.post(new KdTreeCreated());
    }

    @Override
    public KdNode getRoot() {
        return root;
    }

    @Override
    public void addNode(MeshPoint meshPoint) {
        // check for empty tree
        if (this.root == null) {
            this.root = new KdNodeImpl(meshPoint, 0);
            return;
        }

        KdNode searched;
        KdNode next = this.root;
        do {
            searched = next;
            if (firstIsLessThanSecond(meshPoint.getPosition(), searched.getLocation(), searched.getDepth())) {
                next = searched.getLesser();
            } else {
                next = searched.getGreater();
            }
        } while (next != null);


        KdNode kdNode = new KdNodeImpl(meshPoint.getPosition(), searched.getDepth() + 1);
        if (firstIsLessThanSecond(meshPoint.getPosition(), searched.getLocation(), searched.getDepth())) {
            searched.setLesser(kdNode);
        } else {
            searched.setGreater(kdNode);
        }
    }

    @Override
    public String toString() {
        String ret = "";
        Queue<KdNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            KdNode node = queue.poll();
            if (node == null) {
                continue;
            }
            queue.add(node.getLesser());
            queue.add(node.getGreater());
            ret += node.toString();
        }
        
        return ret;
    }

    @Override
    public int getNumNodes() {
        int ret = 0;
        Queue<KdNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            KdNode node = queue.poll();
            if (node == null) {
                continue;
            }
            queue.add(node.getLesser());
            queue.add(node.getGreater());
            ret++;
        }
        
        return ret;
    }

    @Override
    public int getDepth() {
        int depth = 0;
        Queue<KdNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            KdNode node = queue.poll();
            if (node == null) {
                continue;
            }
            queue.add(node.getLesser());
            queue.add(node.getGreater());
            if (node.getDepth() > depth) {
                depth = node.getDepth();
            }
        }
        
        return depth;
    }

    @Override
    public void accept(KdTreeVisitor visitor) {
        visitor.visitKdTree(this);
    }
    

    /***********************************************************
     *  PRIVATE METHODS
     ***********************************************************/
    
    private void buildTree(Collection<MeshFacet> facets) {
        SortedMap<Point3d, AggregatedVertex> vertices = new TreeMap<>(new ComparatorX());
        
        /*
         * Sort all vertices according to the X coordinate, aggregate vertices
         * with the same 3D location.
         */
        for (MeshFacet facet: facets) {
            int index = 0;
            for (MeshPoint p: facet.getVertices()) {
                Point3d k = p.getPosition();
                if (vertices.containsKey(k)) {
                    vertices.get(k).facets.add(facet);
                    vertices.get(k).indices.add(index);
                } else {
                    vertices.put(k, new AggregatedVertex(facet, index));
                }
                index++;
            }
        }
        
        root = buildTree(null, vertices, 0);
    }
    
    /**
     * Builds kd-tree.
     *
     * @param parent Parent node
     * @param vertices List of aggregates sorted vertices
     * @param level Tree depth that affects the splitting direction (x, y, or z)
     * @return new node of the kd-tree or null
     */
    private KdNodeImpl buildTree(KdNodeImpl parent, SortedMap<Point3d, AggregatedVertex> vertices, int level) {
        
        if (vertices.isEmpty()) {
            return null;
        }
        
        Point3d pivot = findPivot(vertices, level);
        AggregatedVertex data = vertices.get(pivot);
        
        KdNodeImpl node = new KdNodeImpl(data.facets, data.indices, level);
            
        SortedMap<Point3d, AggregatedVertex> left = null;
        SortedMap<Point3d, AggregatedVertex> right = null;
            
        switch ((level + 1) % 3) {
            case 0:
                left = new TreeMap<>(new ComparatorX());
                right = new TreeMap<>(new ComparatorX());
                break;
            case 1:
                left = new TreeMap<>(new ComparatorY());
                right = new TreeMap<>(new ComparatorY());
                break;
            case 2:
                left = new TreeMap<>(new ComparatorZ());
                right = new TreeMap<>(new ComparatorZ());
                break;
            default:
                return null;
        }

        left.putAll(vertices.subMap(vertices.firstKey(), pivot));
        vertices.keySet().removeAll(left.keySet());
        vertices.remove(pivot);
        right.putAll(vertices);

        node.setLesser(buildTree(node, left, level + 1));
        node.setGreater(buildTree(node, right, level + 1));
        
        return node;
    }
    
    /**
     * Finds and returns the middle key or the first key with the same level-th coordinate.
     * 
     * @param map Map to be searched
     * @param level Tree depth that affects the choice of the coordinate (x, y, or z)
     * @return middle key or the first key with the same level-th coordinate
     */
    private Point3d findPivot(SortedMap<Point3d, AggregatedVertex> map, int level) {
        int mid = (map.size() / 2);
        int i = 0;
        
        Point3d fst = map.isEmpty() ? null : map.firstKey(); // First point with the same level-th coordinate as the 'key' point
        for (Point3d key: map.keySet()) {
            if (i++ == mid) {
                if (singleCoordinateEquals(key, fst, level)) {
                    return fst;
                }
                return key;
            }
            if (!singleCoordinateEquals(key, fst, level)) {
                fst = key;
            }
        }
        return null;
    }
    
    private boolean singleCoordinateEquals(Point3d v1, Point3d v2, int level) {
        switch (level % 3) {
            case 0:
                return v1.x == v2.x;
            case 1:
                return v1.y == v2.y;
            default:
                return v1.z == v2.z;
        }
    }

    protected boolean firstIsLessThanSecond(Point3d v1, Point3d v2, int level){
        switch (level % 3) {
            case 0:
                return v1.x <= v2.x;
            case 1:
                return v1.y <= v2.y;
            case 2:
                return v1.z <= v2.z;
            default:
                break;
        }
        return false;
    }

    protected KdNodeImpl rearrangeLeafNodes(Point3d point1, Point3d point2, Point3d point3, int level) {
        Point3d[] points = {point1, point2, point3};
        // choose comparator
        Comparator<Point3d> comparator;
        switch (level % 3) {
            case 0:
                comparator = new ComparatorX();
                break;
            case 1:
                comparator = new ComparatorY();
                break;
            default:
               comparator = new ComparatorZ();
        }
        // sort points
        Arrays.sort(points, comparator);
        // build subtree
        KdNodeImpl left = new KdNodeImpl(points[0], level+1);
        KdNodeImpl root = new KdNodeImpl(points[1], level);
        KdNodeImpl right = new KdNodeImpl(points[2], level+1);
        root.setLesser(left);
        root.setGreater(right);
        return root;
    }
   
    
    /***********************************************************
    *  EMBEDDED CLASSES
    ************************************************************/   

    /**
     * Helper class used during the kd-tree creation to store mesh vertices
     * with the same 3D location.
     * 
     * @author Radek Oslejsek
     */
    private class AggregatedVertex {
        public final List<MeshFacet> facets = new ArrayList<>();
        public final List<Integer> indices = new ArrayList<>();
        
        AggregatedVertex(MeshFacet f, int i) {
            facets.add(f);
            indices.add(i);
        }
    }
    
    /**
     * Comparator prioritizing the X coordinate.
     * @author Radek Oslejsek
     */
    private class ComparatorX implements Comparator<Point3d> {
        @Override
        public int compare(Point3d arg0, Point3d arg1) {
            int diff = Double.compare(arg0.x, arg1.x);
            if (diff != 0) { 
                return diff; 
            }
            diff = Double.compare(arg0.y, arg1.y);
            if (diff != 0) { 
                return diff; 
            }
            return Double.compare(arg0.z, arg1.z);
        }    
    }
    
    /**
     * Comparator prioritizing the X coordinate.
     * @author Radek Oslejsek
     */
    private class ComparatorY implements Comparator<Point3d> {
        @Override
        public int compare(Point3d arg0, Point3d arg1) {
            int diff = Double.compare(arg0.y, arg1.y);
            if (diff != 0) { 
                return diff; 
            }
            diff = Double.compare(arg0.x, arg1.x);
            if (diff != 0) { 
                return diff; 
            }
            return Double.compare(arg0.z, arg1.z);
        }    
    }
    
    /**
     * Comparator prioritizing the X coordinate.
     * @author Radek Oslejsek
     */
    private class ComparatorZ implements Comparator<Point3d> {
        @Override
        public int compare(Point3d arg0, Point3d arg1) {
            int diff = Double.compare(arg0.z, arg1.z);
            if (diff != 0) { 
                return diff; 
            }
            diff = Double.compare(arg0.y, arg1.y);
            if (diff != 0) { 
                return diff; 
            }
            return Double.compare(arg0.x, arg1.x);
        }    
    }
}
