package cz.fidentis.analyst.data.grid.impl;

import cz.fidentis.analyst.data.grid.UniformGrid3d;

import javax.vecmath.Point3i;
import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * A 3D uniform grid. This grid is infinite with given cell size (only occupied cells are remembered)
 * 
 * @param <V> the type of elements to be stored in the grid
 * @author Radek Oslejsek
 */
public class UniformGrid3dImpl<V> extends UniformGridImpl<Tuple3i, Tuple3d, V> implements UniformGrid3d<V> {
    
    /**
     * Constructor.
     * 
     * @param cellSize Cell size. Must be bigger that one.
     */
    public UniformGrid3dImpl(double cellSize) {
        super(cellSize);
    }
    
    /**
     * Constructor.
     * As the mapping function, use {@code (MeshPoint mp) -> mp.getPosition()} 
     * or {@code (Point3d p) -> p}, for instance.
     * 
     * @param cellSize Cell size. Must be bigger that one.
     * @param objects Objects to be stored
     * @param mapFunc A function that computes a 3D space location from an object
     */
    public UniformGrid3dImpl(double cellSize, Collection<V> objects, Function<? super V, Tuple3d> mapFunc) {
        this(cellSize);
        store(objects, mapFunc);
    }
    
    @Override
    public Tuple3i locationToCell(Tuple3d loc) {
        Point3i p = new Point3i(
                ((int) (loc.x / getCellSize())),
                ((int) (loc.y / getCellSize())),
                ((int) (loc.z / getCellSize())));
        
        if (loc.x < 0) {
            p.x -= 1;
        }
        if (loc.y < 0) {
            p.y -= 1;
        }
        if (loc.z < 0) {
            p.z -= 1;
        }
        
        return p;
    }
    
    @Override
    public List<Tuple3i> getAdjacentCells(Tuple3d loc) {
        List<Tuple3i> ret = new ArrayList<>();
        Tuple3i cell = locationToCell(loc);
        ret.add(cell);
        addNeighbours(ret, cell);
        return ret;
    }
    
    protected void addNeighbours(Collection<Tuple3i> points, Tuple3i cell) {
        points.add(neighbour(cell,  1,  0,  0));
        points.add(neighbour(cell,  0,  1,  0));
        points.add(neighbour(cell,  0,  0,  1));

        points.add(neighbour(cell, -1,  0,  0));
        points.add(neighbour(cell,  0, -1,  0));
        points.add(neighbour(cell,  0,  0, -1));

        points.add(neighbour(cell,  1,  1,  0));
        points.add(neighbour(cell,  0,  1,  1));
        points.add(neighbour(cell,  1,  0,  1));

        points.add(neighbour(cell, -1, -1,  0));
        points.add(neighbour(cell,  0, -1, -1));
        points.add(neighbour(cell, -1,  0, -1));

        points.add(neighbour(cell,  1, -1,  0));
        points.add(neighbour(cell, -1,  1,  0));
        points.add(neighbour(cell,  0, -1,  1));
        points.add(neighbour(cell,  0,  1, -1));
        points.add(neighbour(cell, -1,  0,  1));
        points.add(neighbour(cell,  1,  0, -1));
        
        points.add(neighbour(cell,  1,  1,  1));
        points.add(neighbour(cell, -1, -1,  1));
        points.add(neighbour(cell,  1, -1, -1));
        points.add(neighbour(cell, -1,  1, -1));
        points.add(neighbour(cell, -1, -1, -1));
        points.add(neighbour(cell,  1, -1,  1));
        points.add(neighbour(cell,  1,  1, -1));
        points.add(neighbour(cell, -1,  1,  1));
        
    }
    
    protected Tuple3i neighbour(Tuple3i p, int x, int y, int z) {
        return new Point3i(p.x + x, p.y + y, p.z + z);
    }
}
