package cz.fidentis.analyst.data.kdtree;

import cz.fidentis.analyst.data.mesh.MeshFacet;

import javax.vecmath.Point3d;
import java.io.Serializable;
import java.util.Map;

/**
 * A single node of a KD-tree. Sharing vertices across meshes is supported
 * (the node links all faces that share the same vertex).
 *
 * @author Maria Kocurekova
 * @author Radek Oslejsek
 */
public interface KdNode extends Serializable {

    /**
     * Returns depth of the node in the tree.
     * @return depth of the node in the tree.
     */
    int getDepth();

    /**
     * Returns 3D location of vertices stored in this node
     * @return 3D location of vertices stored in this node
     */
    Point3d getLocation();

    /**
     * Tree traversal - go to the "lesser" child.
     *
     * @return lesser node, null if current node is leaf
     */
    KdNode getLesser();

    /**
     * Tree traversal - go to the "grater" child.
     *
     * @return greater node, null if current node is leaf
     */
    KdNode getGreater();

    /**
     * Set lesser node.
     *
     * @param lesser Node to be set as lesser
     * @return current node
     */
    KdNode setLesser(KdNode lesser);

    /**
     * Set lesser node.
     *
     * @param greater Node to be set as greater
     * @return current node
     */
    KdNode setGreater(KdNode greater);

    /**
     * Returns a map of all mesh facets that share the stored vertex.
     * Value in the map contains the index which the vertex is stored in the mesh facet.
     *
     * @return Map of facets sharing the stored mesh vertex
     */
    Map<MeshFacet, Integer> getFacets();

}
