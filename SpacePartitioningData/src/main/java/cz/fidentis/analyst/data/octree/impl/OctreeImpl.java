package cz.fidentis.analyst.data.octree.impl;

import cz.fidentis.analyst.data.mesh.MeshFacet;
import cz.fidentis.analyst.data.mesh.MeshPoint;
import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.OctNode;
import cz.fidentis.analyst.data.octree.Octree;
import cz.fidentis.analyst.data.octree.OctreeVisitor;

import javax.vecmath.Point3d;
import java.io.Serial;
import java.util.*;

/**
 * OctTree implementation.
 *
 * @author Enkh-Undral EnkhBayar
 */
public class OctreeImpl implements Octree {

    @Serial
    private static final long serialVersionUID = 1L;

    private OctNodeImpl root;

    /**
     * distance of the smallest cell in {@code Octree}.
     */
    private Double minLen = Double.POSITIVE_INFINITY;

    /**
     * The octree is divided recursively until there is at most
     * {@code MAX_VERTICES_IN_LEAF} vertices in leaf nodes.
     */
    private static final int MAX_VERTICES_IN_LEAF = 1;

    /**
     * Constructor.
     * If no mesh points (vertices) are provided, then an empty {@code Octree}
     * is constructed (with the root node set to null). If multiple mesh facets
     * share the same vertex, then they are stored efficiently in the same node
     * of the {@code Octree}.
     *
     * @param facets The list of mesh facets to be stored. Facets can share
     * vertices.
     */
    public OctreeImpl(Collection<MeshFacet> facets) {
        if (facets == null || facets.isEmpty() || facets.stream().findAny().get().getVertices().isEmpty()) {
            this.root = null;
            return;
        }
        buildTree(facets);
    }

    @Override
    public OctNode getRoot() {
        return root;
    }

    @Override
    public Double getMinLen() {
        return minLen;
    }

    @Override
    public void accept(OctreeVisitor visitor) {
        visitor.visitOctree(this);
    }

    /**
     * Recursively display the contents of the tree in a verbose format.
     * Individual nodes are represented in format [small boundary, large
     * boundary] point and ends with skipped n empty node(-s) where n is the
     * number of empty leaf child nodes in the node float numbers have 2 decimal
     * places
     *
     * @return representation of the tree
     */
    @Override
    public String toString() {
        return toString(false, 2);
    }

    /**
     * Recursively display the contents of the tree in a verbose format.
     * Individual nodes are represented in format (if the node does not hold a
     * point and printEmpty is true) [small boundary, large boundary] and in
     * format if it does [small boundary, large boundary] point and if
     * printEmpty is set to false the node ends with skipped n empty node(-s)
     * where n is the number of empty leaf child nodes in the node float numbers
     * have 2 decimal places
     *
     * @param printEmpty if empty nodes should be printed or just skipped
     * @return representation of the tree
     */
    protected String toString(boolean printEmpty) {
        return toString(printEmpty, 2);
    }

    /**
     * Recursively display the contents of the tree in a verbose format.
     * Individual nodes are represented in format (if the node does not hold a
     * point and printEmpty is true) [small boundary, large boundary] and in
     * format if it does [small boundary, large boundary] point and if
     * printEmpty is set to false the node ends with skipped n empty node(-s)
     * where n is the number of empty leaf child nodes in the node float numbers
     * have decimalPlaces decimal places
     *
     * @param printEmpty if empty nodes should be printed or just skipped
     * @param decimalPlaces number of decimal places for float numbers
     * @return representation of the tree
     */
    protected String toString(boolean printEmpty, int decimalPlaces) {
        return root.toString("", printEmpty, decimalPlaces);
    }

    /**
     * Creates a cube out a bounding box which is larger than the bounding box
     * given
     *
     * @param small 3D location of the boundary box - corner with the smallest
     * coordinates
     * @param large 3D location of the boundary box - corner with the largest
     * coordinates
     * @return two points - new locations of boundaries - boundaries of the cube
     */
    private Point3d[] getBoundingCube(Point3d small, Point3d large) {
        double[] coorSmall = {small.x, small.y, small.z};
        double[] coorLarge = {large.x, large.y, large.z};
        int maxIndex = 0;
        for (int i = 1; i <= 2; i++) {
            if (coorLarge[i] - coorSmall[i] > coorLarge[maxIndex] - coorSmall[maxIndex]) {
                maxIndex = i;
            }
        }
        double halfMaxDiff = (coorLarge[maxIndex] - coorSmall[maxIndex]) / 2;
        for (int i = 0; i <= 2; i++) {
            if (i == maxIndex) {
                continue;
            }
            double average = (coorSmall[i] + coorLarge[i]) / 2;
            coorSmall[i] = average - halfMaxDiff;
            coorLarge[i] = average + halfMaxDiff;
        }
        return new Point3d[]{
            new Point3d(coorSmall[0], coorSmall[1], coorSmall[2]),
            new Point3d(coorLarge[0], coorLarge[1], coorLarge[2])
        };
    }

    /**
     * Updates minLen class property to match the minimum length of octNode
     *
     * @param smallestPoint of octNode
     * @param largestPoint of octNode
     */
    protected void updateMinLen(Point3d smallestPoint, Point3d largestPoint) {
        minLen = Double.min(minLen, largestPoint.x - smallestPoint.x);
        minLen = Double.min(minLen, largestPoint.y - smallestPoint.y);
        minLen = Double.min(minLen, largestPoint.z - smallestPoint.z);
    }

    /**
     * Builds octree
     *
     * @param facets facets to be held in octree
     */
    private void buildTree(Collection<MeshFacet> facets) {
        // We have to remove duplicities in vertices. 
        // Otherwise, more than 1 (same) vertex may remain in the bounding cube, 
        // violating the splitting stop-constraint that would never occur.
        // This is why the vertices are stored in a set.
        Set<Point3d> vertices = new HashSet<>();
        TriangleList triangles = new TriangleList(facets.stream().findAny().get().getNumTriangles());
        
        // Find bounding box, get all vertices, triangles and their bounding boxes
        Point3d smallestPoint = new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        Point3d largestPoint = new Point3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        for (MeshFacet facet : facets) {
            for (MeshPoint vertex : facet.getVertices()) {
                Point3d p = vertex.getPosition();
                vertices.add(p);
                smallestPoint = new Point3d(
                        Double.min(smallestPoint.x, p.x),
                        Double.min(smallestPoint.y, p.y),
                        Double.min(smallestPoint.z, p.z)
                );
                largestPoint = new Point3d(
                        Double.max(largestPoint.x, p.x),
                        Double.max(largestPoint.y, p.y),
                        Double.max(largestPoint.z, p.z)
                );
            }
            triangles.addMeshFacet(facet);
        }
        Point3d[] boundaries = getBoundingCube(smallestPoint, largestPoint);
        smallestPoint = boundaries[0];
        largestPoint = boundaries[1];
        updateMinLen(smallestPoint, largestPoint);
        root = buildTree(new ArrayList<>(vertices), triangles, smallestPoint, largestPoint);
    }

    /**
     * Builds Octree.
     *
     * @param vertices Collection of vertices in the boundary cube, must not be
     * {@code null}
     * @param triangles Collection of triangles in the boundary cube, must not be
     * {@code null}
     * @param smallestPoint boundary coordinate with x, y, z lower or equal than
     * any other point in created OctNode, must not be {@code null}
     * @param largestPoint boundary coordinate with x, y, z higher or equal than
     * any other point in created OctNode, must not be {@code null}
     * @return new node of the Octree
     */
    private OctNodeImpl buildTree(ArrayList<Point3d> vertices, TriangleList triangles, Point3d smallestPoint, Point3d largestPoint) {

        if (vertices.size() <= MAX_VERTICES_IN_LEAF) {
            updateMinLen(smallestPoint, largestPoint);
            return new OctNodeLeaf(smallestPoint, largestPoint, triangles.triangles);
        }

        Point3d middlePoint = new Point3d(
                (smallestPoint.x + largestPoint.x) / 2,
                (smallestPoint.y + largestPoint.y) / 2,
                (smallestPoint.z + largestPoint.z) / 2);
        List<ArrayList<Point3d>> octantsOfVertices = splitPoints(vertices, middlePoint);
        vertices.clear(); // save memory
        vertices.trimToSize();
        
        double[] xCoords = {smallestPoint.x, middlePoint.x, largestPoint.x};
        double[] yCoords = {smallestPoint.y, middlePoint.y, largestPoint.y};
        double[] zCoords = {smallestPoint.z, middlePoint.z, largestPoint.z};
        List<TriangleList> octantsOfTriangles = splitTriangles(xCoords, yCoords, zCoords, triangles);
        triangles.clearAndTrim(); // save memory
        
        List<OctNodeImpl> doneOctants = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            Point3d newSmallestPoint = new Point3d(xCoords[i >> 2], yCoords[i >> 1 & 1], zCoords[i & 1]);
            Point3d newLargestPoint = new Point3d(xCoords[(i >> 2) + 1], yCoords[(i >> 1 & 1) + 1], zCoords[(i & 1) + 1]);
            doneOctants.add(
                    buildTree(
                            octantsOfVertices.get(i),
                            octantsOfTriangles.get(i),
                            newSmallestPoint,
                            newLargestPoint
                    )
            );
        }
        return new OctNodeInner(smallestPoint, largestPoint, doneOctants);
    }
    
    private List<TriangleList> splitTriangles(double[] xCoords, double[] yCoords, double[] zCoords, TriangleList triangles) {
        List<TriangleList>  ret = new ArrayList<>(8);
        for (int i = 0; i < 8; i++) {
            ret.add(triangles.getTrianglesForOctant(
                    new Point3d(xCoords[i >> 2], yCoords[i >> 1 & 1], zCoords[i & 1]),
                    new Point3d(xCoords[(i >> 2) + 1], yCoords[(i >> 1 & 1) + 1], zCoords[(i & 1) + 1])
            ));
        }
        
        return ret;
    }

    /**
     * Splits all points into 8 octants separated by 3 planes which all
     * intersect in middlePoint
     *
     * @param vertices Set of vertices, must not be {@code null}
     * @param middlePoint middle point of space, must not be {@code null}
     * @return 8 octants containing all vertices in space
     */
    private List<ArrayList<Point3d>> splitPoints(List<Point3d> vertices, Point3d middlePoint) {
        List<ArrayList<Point3d>> octants = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            octants.add(new ArrayList<>());
        }
        for (Point3d p: vertices) {
            octants.get(getOctantIndex(p, middlePoint)).add(p);
        }
        return octants;
    }

    /**
     * Calculates which octant should vertex belong to based on middle point, if
     * vertex is outside of cube this function will not detect it and assign an
     * octant anyway
     *
     * @param point point to be assigned index
     * @param middlePoint dividing point which creates 3 dividing planes
     * @return index from 0 - 7 including
     */
    private int getOctantIndex(Point3d point, Point3d middlePoint) {
        int octantIndex = 0;
        if (point.x > middlePoint.x) {
            octantIndex += 4;
        }
        if (point.y > middlePoint.y) {
            octantIndex += 2;
        }
        if (point.z > middlePoint.z) {
            octantIndex += 1;
        }
        return octantIndex;
    }

    /**
     * Triangles with bounding boxes.
     *
     * @author Radek Oslejsek
     */
    private final class TriangleList {

        private final ArrayList<MeshTriangle> triangles;
        private final ArrayList<Point3d[]> bboxes;
        
        private TriangleList() {
            triangles = new ArrayList<>();
            bboxes = new ArrayList<>();
        }
        
        private TriangleList(int size) {
            triangles = new ArrayList<>(size);
            bboxes = new ArrayList<>(size);
        }

        private void addMeshFacet(MeshFacet facet) {
            for (MeshTriangle triangle : facet) {
                addTriangle(triangle, getBoundingBoxForTriangle(triangle));
            }
        }
        
        /**
         * Filters out all triangles which bounding box is within octant
         *
         * @param smallest smallest boundary of octant, must not be {@code null}
         * @param largest smallest boundary of octant, must not be {@code null}
         * @return triangles which are in octant
         */
        private TriangleList getTrianglesForOctant(Point3d smallest, Point3d largest) {
            TriangleList result = new TriangleList();
            for (int i = 0; i < triangles.size(); i++) {
                Point3d[] boundingBox = bboxes.get(i);
                if (isTriangleBoundingBoxInCube(smallest, largest, boundingBox)) {
                    result.addTriangle(triangles.get(i), boundingBox);
                }
            }
            return result;
        }
    
        private void addTriangle(MeshTriangle tri, Point3d[] bbox) {
            triangles.add(tri);
            bboxes.add(bbox);
        }

        /**
         * Calculates the bounding box of a triangle
         *
         * @param triangle triangle, Must not be {@code null}
         * @return bounding box of the triangle
         */
        private Point3d[] getBoundingBoxForTriangle(MeshTriangle triangle) {
            Point3d[] boundingBox = {
                new Point3d(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY),
                new Point3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY)
            };
            for (MeshPoint vertex : triangle) {
                Point3d position = vertex.getPosition();
                boundingBox[0].x = Double.min(boundingBox[0].x, position.x);
                boundingBox[0].y = Double.min(boundingBox[0].y, position.y);
                boundingBox[0].z = Double.min(boundingBox[0].z, position.z);
                boundingBox[1].x = Double.max(boundingBox[1].x, position.x);
                boundingBox[1].y = Double.max(boundingBox[1].y, position.y);
                boundingBox[1].z = Double.max(boundingBox[1].z, position.z);
            }
            return boundingBox;
        }
        
        /**
         * Check wheatear the bounding box of the triangle is within cube
         *
         * @param smallest smallest boundary of octant, must not be {@code null}
         * @param largest smallest boundary of octant, must not be {@code null}
         * @param boundingBox boundingBox of a triangle, must not be {@code null}
         * @return true if cube and bounding box overlap, false otherwise
         */
        private boolean isTriangleBoundingBoxInCube(Point3d smallest, Point3d largest, Point3d[] boundingBox) {
            if (!(boundingBox[1].x >= smallest.x && largest.x >= boundingBox[0].x)) {
                return false;
            }
            if (!(boundingBox[1].y >= smallest.y && largest.y >= boundingBox[0].y)) {
                return false;
            }
            return boundingBox[1].z >= smallest.z && largest.z >= boundingBox[0].z;
        }

        private void clearAndTrim() {
            this.triangles.clear();
            this.triangles.trimToSize();
            this.bboxes.clear();
            this.bboxes.trimToSize();
        }
    }
}
