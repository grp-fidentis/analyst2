package cz.fidentis.analyst.data.grid.impl;

import cz.fidentis.analyst.data.grid.UniformGrid;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * An abstract class for multidimensional uniform grids.
 * The grid is infinite with given cell size (only occupied cells are remembered in a map).
 *
 * @param <L> grid cells' multidimensional indices, e.g., {@code Tuple2i}, {@code Tuple3i}, {@code Tuple4i}, etc.
 * @param <K> grid's location in multidimensional space, i.e., {@code Tuple2d}, {@code Tuple3d}, {@code Tuple4d}, etc.
 * @param <V> the type of elements to be stored in the grid
 * @author Radek Oslejsek
 */
public abstract class UniformGridImpl<L,K,V> implements UniformGrid<K,V> {

    /**
     * Key = PointXi, value = any object
     */
    private final Map<L, List<V>> grid = new HashMap<>();
    private final double cellSize;
    
    /**
     * Constructor.
     * 
     * @param cellSize Cell size. Must be bigger that one.
     */
    public UniformGridImpl(double cellSize) {
        if (cellSize <= 0.0) {
            throw new IllegalArgumentException("cellSize");
        }
        this.cellSize = cellSize;
    }
    
    /**
     * Constructor. 
     * As the mapping function, use {@code (MeshPoint mp) -> mp.getPosition()} 
     * or {@code (Point3d p) -> p}, for instance.
     * 
     * @param cellSize Cell size. Must be bigger that one.
     * @param objects Objects to be stored
     * @param mapFunc A function that computes a space location from an object
     */
    public UniformGridImpl(double cellSize, Collection<V> objects, Function<? super V, ? extends K> mapFunc) {
        this(cellSize);
        store(objects, mapFunc);
    }

    @Override
    public double getCellSize() {
        return cellSize;
    }

    @Override
    public int numOccupiedCells() {
        return grid.keySet().size();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<List<V>> getNonEmptyCells() {
        return grid.values().stream().collect(Collectors.toList());
    }

    @Override
    public void clear() {
        grid.clear();
    }

    @Override
    public void store(K loc, V object) {
        L cell = locationToCell(loc);
        grid.computeIfAbsent(cell, f -> new ArrayList<>()).add(object);
    }

    @Override
    public final void store(Collection<V> objects, Function<? super V, ? extends K> mapFunc) {
        objects.forEach(obj -> store(mapFunc.apply(obj), obj));
    }

    @Override
    public List<V> get(K loc) {
        return grid.getOrDefault(locationToCell(loc), Collections.emptyList());
    }

    @Override
    public List<V> getAll() {
        return grid.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Override
    public boolean remove(K loc, V object) {
        L cell = locationToCell(loc);
        boolean ret = grid.containsKey(cell) && grid.get(cell).remove(object);
        if (grid.get(cell).isEmpty()) {
            grid.remove(cell);
        }
        return ret;
    }

    @Override
    public List<V> getClosest(K loc) {
        List<V> ret = new ArrayList<>();
        getAdjacentCells(loc).forEach(cell -> {
            List<V> cand = grid.get(cell);
            if (cand != null) {
                ret.addAll(cand);
            }
        });
        return ret;
    }

    /**
     * Takes a location in space and return coordinates of the corresponding cell.
     * 
     * @param loc Location in space
     * @return coordinates of the corresponding cell
     */
    protected abstract L locationToCell(K loc);
    
    /**
     * Computes the cell for given location and then all its neighbors.
     * 
     * @param loc Location in space
     * @return the cell for given location and then all its neighbors.
     */
    protected abstract List<L> getAdjacentCells(K loc);
    
    protected Map<L, List<V>> getGrid() {
        return grid;
    }
}
