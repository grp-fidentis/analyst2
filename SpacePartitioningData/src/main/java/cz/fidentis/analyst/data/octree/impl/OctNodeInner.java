package cz.fidentis.analyst.data.octree.impl;

import javax.vecmath.Point3d;
import java.util.Collections;
import java.util.List;

/**
 * An inner node of an Octree.
 *
 * @author Enkh-Undral EnkhBayar
 */
public class OctNodeInner extends OctNodeImpl {
    
    /**
     * Constructor of an <b>internal node</b>
     *
     * @param smallest boundary box - corner with the smallest coordinates. Must not
     * be null
     * @param largest boundary box - corner with the largest coordinates. Must not
     * be null
     * @param octants List of octNodes which are children for this node. Must
     * not be null and has to have size of 8.
     * @throws IllegalArgumentException if some parameter is wrong
     */
    public OctNodeInner(Point3d smallest, Point3d largest, List<OctNodeImpl> octants) {
        super(smallest, largest, Collections.emptyList(), octants);
        if (octants == null || octants.size() != 8) {
            throw new IllegalArgumentException("octants");
        }
    }
}
