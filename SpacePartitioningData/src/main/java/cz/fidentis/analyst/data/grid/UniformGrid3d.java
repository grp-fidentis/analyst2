package cz.fidentis.analyst.data.grid;

import cz.fidentis.analyst.data.grid.impl.UniformGrid3dImpl;

import javax.vecmath.Tuple3d;
import java.util.Collection;
import java.util.function.Function;

/**
 * Infinite uniform 3D grid with the given cell size.
 *
 * @param <V> the type of elements to be stored in the grid
 * @author Radek Oslejsek
 */
public interface UniformGrid3d<V> extends UniformGrid<Tuple3d, V> {

    /**
     * Creates new 3D grid.
     *
     * @param cellSize Cell size. Must be bigger that one.
     * @return new 3D grid
     */
    static <V> UniformGrid3d<V> create(double cellSize) {
        return new UniformGrid3dImpl<>(cellSize);
    }

    /**
     * Creates new 3D grid.
     * As the mapping function, use {@code (MeshPoint mp) -> mp.getPosition()}
     * or {@code (Point3d p) -> p}, for instance.
     *
     * @param cellSize Cell size. Must be bigger that one.
     * @param objects Objects to be stored
     * @param mapFunc A function that computes a 3D space location from an object
     * @return new 3D grid
     */
    static <V> UniformGrid3d<V> create(double cellSize, Collection<V> objects, Function<? super V, Tuple3d> mapFunc) {
        return new UniformGrid3dImpl<>(cellSize, objects, mapFunc);
    }

}
