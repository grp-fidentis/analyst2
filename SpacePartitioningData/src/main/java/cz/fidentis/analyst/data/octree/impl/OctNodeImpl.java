package cz.fidentis.analyst.data.octree.impl;

import cz.fidentis.analyst.data.mesh.MeshTriangle;
import cz.fidentis.analyst.data.octree.OctNode;
import cz.fidentis.analyst.data.ray.Ray;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.Serial;
import java.util.Collections;
import java.util.List;

/**
 * Implementation of an Octree node.
 *
 * @author Enkh-Undral EnkhBayar
 */
public abstract class OctNodeImpl implements OctNode {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * Mesh facets sharing the stored vertex
     */
    private final List<MeshTriangle> triangles;

    /**
     * Octree topology
     */
    private final List<OctNodeImpl> octants;

    /**
     * 3D location of the boundary box - corner with the smallest coordinates
     */
    private final Point3d smallestBoundary;

    /**
     * 3D location of the boundary box - corner with the largest coordinates
     */
    private final Point3d largestBoundary;

    protected OctNodeImpl(Point3d smallest, Point3d largest, List<MeshTriangle> triangles, List<OctNodeImpl> octants) {
        if (smallest == null) {
            throw new IllegalArgumentException("Smallest boundary in OctNode cannot be null");
        }
        if (largest == null) {
            throw new IllegalArgumentException("Largest boundary in OctNode cannot be null");
        }

        this.triangles = triangles;
        this.octants = octants;
        this.smallestBoundary = smallest;
        this.largestBoundary = largest;
    }

    @Override
    public boolean isLeafNode() {
        return octants == null || octants.isEmpty();
    }

    @Override
    public OctNode getOctant(int index) {
        if (index< 0 || index >= 8) {
            throw new IllegalArgumentException("getOctant passed with illegal index, can only be 0 to 7");
        }
        return isLeafNode() ? null : octants.get(index);
    }
    
    @Override
    public OctNode getChild(Point3d point) {
        Point3d middlePoint = new Point3d(
                (smallestBoundary.x + largestBoundary.x) / 2.0,
                (smallestBoundary.y + largestBoundary.y) / 2.0,
                (smallestBoundary.z + largestBoundary.z) / 2.0);
        
        int octantIndex = 0;
        if (point.x > middlePoint.x) {
            octantIndex += 4;
        }
        if (point.y > middlePoint.y) {
            octantIndex += 2;
        }
        if (point.z > middlePoint.z) {
            octantIndex += 1;
        }
        
        return getOctant(octantIndex);
    }
    
    @Override
    public List<MeshTriangle> getTriangles() {
        return Collections.unmodifiableList(triangles);
    }

    @Override
    public Point3d getLowerBound() {
        return smallestBoundary;
    }

    @Override
    public Point3d getUpperBound() {
        return largestBoundary;
    }
    
    @Override
    public boolean isPointInside(Point3d point) {
        Point3d smallBoundary = getLowerBound();
        Point3d largeBoundary = getUpperBound();
        if (point.x < smallBoundary.x) {
            return false;
        }
        if (point.y < smallBoundary.y) {
            return false;
        }
        if (point.z < smallBoundary.z) {
            return false;
        }
        if (point.x > largeBoundary.x) {
            return false;
        }
        if (point.y > largeBoundary.y) {
            return false;
        }
        return point.z <= largeBoundary.z;
    }

    @Override
    public Point3d rayIntersection(Ray ray) {
        Vector3d smallBoundary = new Vector3d(this.smallestBoundary);
        Vector3d largeBoundary = new Vector3d(this.largestBoundary);
        
        Vector3d[] planeNormals = {new Vector3d(1, 0, 0), new Vector3d(0, 1, 0), new Vector3d(0, 0, 1)};
        
        double tMin = Double.NEGATIVE_INFINITY;
        double tMax = Double.POSITIVE_INFINITY;
        
        for (Vector3d planeNormal : planeNormals) {
            // select required coordinate from p and d:
            double np = planeNormal.dot(new Vector3d(ray.origin()));
            double vp = planeNormal.dot(ray.direction());

            if (vp == 0) { // the ray is parallel with the cube's side
                continue;
            }

            double[] offset = {
                planeNormal.dot(smallBoundary),
                planeNormal.dot(largeBoundary)
            };

            double[] tTmp = {
                (offset[0] - np) / vp, 
                (offset[1] - np) / vp
            };

            // Eq. 9
            if (tTmp[0] > tTmp[1]) { // inverse order
                tMin = Double.max(tTmp[1], tMin);
                tMax = Double.min(tTmp[0], tMax);
            } else { // correct order
                tMin = Double.max(tTmp[0], tMin);
                tMax = Double.min(tTmp[1], tMax);
            }
        }
        
        // Almost Eq. (10) and ???
        if (tMin > tMax || (tMin < 0 && tMax < 0)) {
            return null;
        }
        
        Point3d point = new Point3d(ray.origin());
        Vector3d vector = new Vector3d(ray.direction());
        vector.scale(isPointInside(ray.origin()) ? tMax : tMin);
        point.add(vector);
        return point;
    }

    @Override
    public String toString() {
        return toString(2);
    }

    /**
     * toString override
     *
     * @param decimalPlaces number of decimal places to be used in printing for
     * double numbers
     * @return string representation of the current node
     */
    protected String toString(int decimalPlaces) {
        String formatString = "(";
        for (int i = 0; i < 2; i++) {
            formatString += "%." + decimalPlaces + "f, ";
        }
        formatString += "%." + decimalPlaces + "f)";
        String small = String.format(formatString, smallestBoundary.x, smallestBoundary.y, smallestBoundary.z);
        String large = String.format(formatString, largestBoundary.x, largestBoundary.y, largestBoundary.z);
        return  "[" + small + ", " + large + "]";
    }

    /**
     * toString helper function for tree like printing
     *
     * @param prefix prefix before representing current node
     * @return string representation of the current node and it's subtree
     */
    protected String toString(String prefix) {
        return toString(prefix, false, 2);
    }

    /**
     * toString helper function for tree like printing
     *
     * @param prefix prefix before representing current node
     * @param decimalPlaces number of decimal places to be used in printing for
     * double numbers
     * @return string representation of the current node and it's subtree
     */
    protected String toString(String prefix, int decimalPlaces) {
        return toString(prefix, false, decimalPlaces);
    }

    /**
     * toString helper function for tree like printing
     *
     * @param prefix prefix before representing current node
     * @param printEmpty whether empty nodes should be skipped or printed for
     * their boundaries
     * @param decimalPlaces number of decimal places to be used in printing for
     * double numbers
     * @return string representation of the current node and it's subtree
     */
    protected String toString(String prefix, boolean printEmpty, int decimalPlaces) {
        if (isLeafNode()) {
            if (triangles.isEmpty() && !printEmpty) {
                return null;
            }
            return adjustPrefix(prefix) + toString(decimalPlaces) + '\n';
        }
        StringBuilder result = new StringBuilder(adjustPrefix(prefix));
        result.append(toString(decimalPlaces)).append('\n');
        if (prefix.endsWith("\\")) {
            prefix = prefix.substring(0, prefix.length() - 1) + ' ';
        }
        int skippedCount = 0;
        for (int i = 0; i < 7; i++) {
            String childResult = ((OctNodeImpl) getOctant(i)).toString(prefix + '|', printEmpty, decimalPlaces);
            if (childResult == null) {
                skippedCount++;
                continue;
            }
            result.append(childResult);
        }
        char lastPrefix = '\\';
        if (skippedCount != 0) {
            lastPrefix = '|';
        }
        String childResult = ((OctNodeImpl) getOctant(7)).toString(prefix + lastPrefix, printEmpty, decimalPlaces);
        if (childResult == null) {
            skippedCount++;
        } else {
            result.append(childResult);
        }
        if (skippedCount != 0) {
            String skippedResult = adjustPrefix(prefix + '\\') + "skipped " + skippedCount + " empty nodes\n";
            result.append(skippedResult);
        }
        return result.toString();
    }

    /**
     * Adjusts prefix to include spaces and prettifies the prefix
     *
     * @param prefix prefix with literals
     * @return prefix with correct formatting that can be printed immediately
     */
    private String adjustPrefix(String prefix) {
        if (prefix.isEmpty()) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < prefix.length() - 1; i++) {
            result.append(prefix.charAt(i)).append("   ");
        }
        result.append(prefix.charAt(prefix.length() - 1)).append("-- ");
        return result.toString();
    }
}
