package cz.fidentis.analyst.data.octree;


/**
 * When instantiated, the object can be gradually applied to multiple octrees.
 * It inspects the state of the tree one by one, and (cumulatively) computes results.
 * <p>
 * Implement this interface whenever you want to define new algorithm over a octree.
 * </p>
 * <p>
 * If the visitor is thread-safe, then a single instance of the visitor
 * can visit concurrently (and asynchronously) multiple octrees. Otherwise, 
 * the parallel inspection is still possible, but a new instance of the visitor 
 * has to be used for each octree.
 * </p>
 *
 * @author Enkh-Undral EnkhBayar
 */
@FunctionalInterface
public interface OctreeVisitor {
    
    /**
     * Returns {@code true} if the implementation is thread-safe and then
     * <b>a single visitor instance</b> can be applied to multiple octrees simultaneously.
     * <p>
     * Thread-safe implementation means that any read or write from/to the visitor's 
     * state is protected by {@code synchronized}.
     * </p>
     * 
     * @return {@code true} if the implementation is thread-safe.
     */
    default boolean isThreadSafe() {
        return false;
    }
    
    /**
     * The main inspection method to be implemented by specific visitors.
     * 
     * @param octree octree to be visited
     */
    void visitOctree(Octree octree);
}
