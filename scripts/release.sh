#!/bin/bash
#
# Script to update version and 

BRANCH=`git branch | grep \* | cut -d ' ' -f2`
if [ "$BRANCH" != "master" ]; then
  echo "Switch to the master branch first."
  exit;
fi

DEPLOYMENT_INFO_VERSION_FILE="VERSION.txt"

OLD_VERSION=$(grep -oP '^([^\s]*)' $DEPLOYMENT_INFO_VERSION_FILE)

vim $DEPLOYMENT_INFO_VERSION_FILE

VERSION=$(grep -oP '^([^\s]*)' $DEPLOYMENT_INFO_VERSION_FILE)
TAG_MESSAGE=$(grep -oP '(?<=\s)[^\s].*' $DEPLOYMENT_INFO_VERSION_FILE)

if [ "$OLD_VERSION" == "$VERSION" ]; then
  echo "The version number did not change."
  exit;
fi

#
# Set release version and run the release CI/CD jobs 
#
mvn versions:set -DnewVersion=$VERSION -DgenerateBackupPoms=false
git commit -am "Update pom.xml version to $VERSION"

LAST_TAG=$(git tag | head -1)
git log  --pretty=format:'%h -- %s' $LAST_TAG..HEAD --graph > CHANGELOG.md
git add CHANGELOG.md
git commit -m "CHANGELOG.md file updated with commits between the current and previous tag."

git tag -a $VERSION -m "$TAG_MESSAGE"
git config --global push.followTags true
git push

#
# Set snapshot version for further development
#
mvn versions:set -DnewVersion=master-SNAPSHOT -DgenerateBackupPoms=false
git commit -am "Update pom.xml version to master-SNAPSHOT"
git push


