* 80a44a64 -- Update pom.xml version to 2024.08.1
*   430ae275 -- Merge branch '323-fix-bugs-found-by-testing-pre-release-2024-07-5' into 'master'
|\  
| * c67a1b7e -- Fix FPs hide-show functionality.
| * 469c56cb -- Fix AI-based registration.
|/  
* 2dc95ab1 -- Update .gitlab-ci.yml file
* 7b6a0fc1 -- Update pom.xml version to master-SNAPSHOT
* c361f63b -- CHANGELOG.md file updated with commits between the current and previous tag.
* 6044899e -- Update pom.xml version to 2024.07.5
* dea92a57 -- Update .gitlab-ci.yml file
* 8f1810de -- Update pom.xml version to master-SNAPSHOT
* 583ca690 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 73b8994b -- Update pom.xml version to 2024.07.4
*   aab309aa -- Merge branch '322-upgrade-to-java-21' into 'master'
|\  
| * b9d928ef -- Resolve "Upgrade to Java 21"
|/  
* cf653dfd -- Update .gitlab-ci.yml file
* 49f87de9 -- Update pom.xml version to master-SNAPSHOT
* ad271d17 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 60aa374c -- Update pom.xml version to 2024.07.3
*   1c3bccc3 -- Merge branch 'master' of gitlab.fi.muni.cz:grp-fidentis/analyst2
|\  
| * d592da15 -- Update .gitlab-ci.yml file
* | a215cfea -- Update pom.xml version to master-SNAPSHOT
* | 4b74233f -- CHANGELOG.md file updated with commits between the current and previous tag.
* | 0fee27a8 -- Update pom.xml version to 2024.07.2
|/  
* b9a984ff -- Update pom.xml version to master-SNAPSHOT
* ba9a79f4 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 44ce7a88 -- Update pom.xml version to 2024.07.1
*   e6a1d1a3 -- Merge branch '321-refactor-the-detection-module' into 'master'
|\  
| * 4e86d2e0 -- Resolve "Refactor the Detection module"
|/  
*   a213e90d -- Merge branch '110-mask-refactoring' into 'master'
|\  
| * 01d0eaa1 -- Resolve "2D masks their projections into the 3D scene"
|/  
*   38d98163 -- Merge branch '318-core-interctive-mask' into 'master'
|\  
| * 6473e1c4 -- Resolve "Core functionality of 3D mask"
* |   f50bbbb4 -- Merge branch 'missing-author-tags' into 'master'
|\ \  
| |/  
|/|   
| * 4602b73e -- Add forgotten @author tags before submitting
|/  
*   855c03a6 -- Merge branch '319-opencv-evaluate' into 'master'
|\  
| * 4bdc9373 -- Resolve "Fix found issues on detection-based superimposition"
|/  
*   4b5b2c64 -- Merge branch '317-gpu-distance-unit-test' into 'master'
|\  
| * f2fe6030 -- Added unit test for GPU distance measurement
|/  
*   d3afd5ea -- Merge branch '310-integrate-opencv' into 'master'
|\  
| * e5bd766b -- Implement OpenCV face detection (#310)
|/  
*   3d3de0f1 -- Merge branch '272-merge-branch' into 'master'
|\  
| * 4788c4a1 -- moving ScreenPointToRay inside MaskProjector, updating documentation
| * 05618231 -- Transform MaskProjector from MeshVisitor to OctreeVisitor
| * 5b6135ad -- adding legacy bounding box for clipping lines between points of mask
| * 368ee28f -- adding missed author tag
| * 0088d6f3 -- adding documentation
| * 78365960 -- integrating MaskProjector
| * 092f2280 -- implementing MaskProjector
| * c6b27995 -- introducing ScreenPointToray in engines package, adding MaskProjector config
| * 774790d7 -- reworking screenPointToRay, testing projection using screenpointtoray
| * 41e2322b -- adding call MaskProjector, updating GeometryEngines pom
| * d2cff488 -- Introducing action listener to SurfaceMaskPanel
| * 5a1db19f -- refactoring Drawable IM
| * 461a0d62 -- introducing MaskProjector
| * 15b62ab3 -- moving MaskPoint class to DrawableIM, preparing to switch from using SurfaceMask(3D)
| * d2204185 -- introducing SurfaceMask2D
| * 5362cd10 -- Refactoring, adding documentation
| * 828bb2a5 -- implementacia neparalelneho mask tasku
| * 43ea4ee2 -- pridanie author tagu do javadocu
| * 59f01a05 -- pokus opravenia vynimky
| * 01c920de -- reimplementacia vykreslovania
| * 2e87da11 -- pridanie normaly bodu
| * 4b54407c -- presun rendrovania
| * 45ac5040 -- presun interactive mask tasku
| * 042f1bfb -- integracia surface mask panel
* |   cc22ab83 -- Merge branch '315-poisson-polish' into 'master'
|\ \  
| * | 6ea4b83e -- GPU Poisson disk sub-sampling (finalization)
|/ /  
* |   24d0e424 -- Merge branch '314-reuse-visitor-in-symmetry' into 'master'
|\ \  
| * | 7aed8186 -- Reuse visitor in Symmetry (robust-point-cloud) algorithm
* | |   136979d6 -- Merge branch '297-gpu-ray-casting-optimizations' into 'master'
|\ \ \  
| * | | 3fc88541 -- Optimizations and better java docs for GPU ray casting
|/ / /  
* | |   4aee5d9f -- Merge branch '316-integrate-gpu-rc-registration' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | 9e5badcd -- Add GPU-based average mesh computation into the batch processing
|/ /  
* |   f0326c6a -- Merge branch '308-fix-opencl-clinvalidcommandqueueexception' into 'master'
|\ \  
| * | 21cdc3ed -- Resolve "Fix OpenCL CLInvalidCommandQueueException"
|/ /  
* |   54094c40 -- Merge branch '106-gpu-poisson-benchmarks' into 'master'
|\ \  
| * | c2c43066 -- Poisson sub-sampling with GPU-based projector rotation, re-using octrees, and asynchronous upload of mesh facets.
|/ /  
* |   72e13694 -- Merge branch '311-remove-dump-logs' into 'master'
|\ \  
| * | 459c2862 -- Resolve "Remove dump logs"
|/ /  
* |   628e79b5 -- Merge branch '309-integrate-glsl-visualizations-to-gpu-module-II' into 'master'
|\ \  
| |/  
|/|   
| * 4c034c6d -- Replace frameListBuffer and frameListTexture with...
|/  
*   92b87c40 -- Merge branch '297-gpu-module-buffers' into 'master'
|\  
| * aa4b68cf -- move SSBO management to the GPU module
|/  
*   a1eb2726 -- Merge branch '307-Fix-GPU-test' into 'master'
|\  
| * 28038bbe -- Fixed memory leaks in Poisson OpenCL
|/  
*   e5cb42d6 -- Merge branch '301-GPU-Based-Octree' into 'master'
|\  
| * bd406cce -- GPU-based Octree construction
|/  
*   71914f2a -- Merge branch '297-gpu-ray-casting-distance' into 'master'
|\  
| * 3ba80a15 -- Adding ray-casting for distance measuring for GPU
|/  
*   22ae060a -- Merge branch '304-closest-ray-intersection-opencl' into 'master'
|\  
| * df546691 -- Introduce the GPU module. Implement initial OpenCL-based Poisson disk sub-sampling.
|/  
* e62d52be -- Update pom.xml version to master-SNAPSHOT
* 4d17b808 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 101e61fe -- Update pom.xml version to 2024.03.1
*   ae5039e4 -- Merge branch 'master' of gitlab.fi.muni.cz:grp-fidentis/analyst2
|\  
| * bfec201c -- Update .gitlab-ci.yml
* | 1210abfa -- Fix generation of OS bundles
|/  
* d6e0151f -- Update pom.xml version to master-SNAPSHOT
* b3c88d42 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 8c242202 -- Update pom.xml version to 2024.03.0
*   d8a4be77 -- Merge branch '302-release-2024-02-00' into 'master'
|\  
| * 4a522c30 -- Resolve "Release 2024.02.00"
|/  
*   8aebf8a7 -- Merge branch '298-new-module-for-shader-creation' into 'master'
|\  
| * 64379b9f -- Refactore creating shaders.
|/  
*   6d68768a -- Merge branch '296-fix-icp' into 'master'
|\  
| * a1e0b63f -- Resolve "Fix ICP"
|/  
*   4b5a0ab6 -- Merge branch '293-refactor-humanface-module' into 'master'
|\  
| * a2fe8272 -- Resolve "Refactor HumanFace module", rename all modules
|/  
*   88f873d8 -- Merge branch '292-refactor-meshalgorithms' into 'master'
|\  
| * 8efeba7f -- Resolve "Refactor MeshAlgorithms"
|/  
*   578d4146 -- Merge branch 'revert-0c46a6884846a9870c8086bc69b2a2f144eb1a9b' into 'master'
|\  
| * 56e116d1 -- Distance span reflects reality even when faces are not registered.
|/  
*   496d02f7 -- Merge branch '291-fix-declaration-of-provided-interfaces-in-the-meshmodel' into 'master'
|\  
| * e0f42862 -- Resolve "Fix declaration of provided interfaces in the MeshModel"
|/  
*   f426db95 -- Merge branch '290-fix-meshmodel' into 'master'
|\  
| * a343bbde -- Resolve "Fix MeshModel"
|/  
*   d5823c82 -- Merge branch '288-restructure-the-spacepartitioning-osgi-bundle' into 'master'
|\  
| * 31fde74f -- Resolve "Restructure the SpacePartitioning and MeshModel OSGi bundles"
|/  
*   cdc7c796 -- Merge branch 'distance-span-using-relative-distance' into 'master'
|\  
| * 38a31814 -- Distance span using relative distances
|/  
*   3f98a8ae -- Merge branch '289-fix-style-checking-erros' into 'master'
|\  
| * 93f93a14 -- Resolve "Fix style checking errors"
|/  
*   7c233446 -- Merge branch '273-introduce-shadow-casting-glyphs' into 'master'
|\  
| * f5529511 -- Resolve "Introduce shadow-casting glyphs"
|/  
*   8410844e -- Merge branch '96-selection-of-faces-on-cutting-planes-in-batch-processing' into 'master'
|\  
| * ed4f1949 -- Feat: Enable selection of a face on cutting planes of batch processing; fix artifacts appearing in cutting curves.
|/  
*   25a5c8e3 -- Merge branch '71-Cutting-planes-for-batch-processing' into 'master'
|\  
| * ca8451a0 -- Introduce cutting planes with distance to batch processing.
|/  
*   e93925ab -- Merge branch '286-make-the-speed-of-the-movement-of-3d-models-adaptive' into 'master'
|\  
| * 26de4916 -- Resolve "Make the speed of the movement of 3D models adaptive; add undo of the movement"
|/  
*   541310f1 -- Merge branch '93-join-sampling-and-reduce-vertices-slider' into 'master'
|\  
| * a244eba8 -- Feat: Adaptable number of cutting planes & Join Sampling and Reduce vertices sliders
|/  
*   44ad7115 -- Merge branch '285-fix-camera-settings-for-body-scans' into 'master'
|\  
| * 633f5bd7 -- Resolve "Fix camera settings for body scans"
|/  
*   334c5c55 -- Merge branch '70-cutting-planes-vertex-reduction-RDP' into 'master'
|\  
| * 18245dd8 -- Ramer-Douglas-Peucker reduction of points of 2D cutting curves
|/  
*   66776010 -- Merge branch 'add-feature-point-type-equals-and-hashcode' into 'master'
|\  
| * ece92cc6 -- Add feature point type equals and hashcode
|/  
*   086168c8 -- Merge branch '282-fix-pom-xml' into 'master'
|\  
| * fe6b353c -- Fix pom.xml
|/  
*   c41bc6f0 -- Merge branch '281-adapt-ci-cd' into 'master'
|\  
| * 54376894 -- Resolve "Adapt CI/CD" - set the `master-SNAPSHOT` version to the master code under development.
* |   601fa410 -- Merge branch 'fix-for-index-error-in-cutting-planes-projection' into 'master'
|\ \  
| * | 4b30dc69 -- Fix cutting planes panel bugs, Allow reuse of logic
|/ /  
* |   b6919b99 -- Merge branch '70-cutting-plane-vertex-reduction' into 'master'
|\ \  
| |/  
|/|   
| * 87a91b98 -- Feat: Vertices reduction
|/  
*   809041ff -- Merge branch 'reuse-of-symmetry-plane-recalculatioon' into 'master'
|\  
| * 3bf1de26 -- Making RegistrationUtils::transformPlane public for reuse in web fidentis
* |   44bb2fbd -- Merge branch '280-remove-humanfacevisitor' into 'master'
|\ \  
| |/  
|/|   
| * 17cf18d3 -- Resolve "Remove HumanFaceVisitor, refactor Procrustes analysis"
|/  
*   9e5f2333 -- Merge branch 'refactoring-distanceAction' into 'master'
|\  
| * 6e086f32 -- Refactoring DistanceAction for reuse, fixing frequency of distances calculation, fixing weighted distances calculation
|/  
*   7aa2ef7c -- Merge branch 'refactor-symmetry-for-reuse' into 'master'
|\  
| * b8158b45 -- Introduce the SymmetryUtils utility class into the HumanFace module.
|/  
*   dd5bae54 -- Merge branch 'optimizing-distances' into 'master'
|\  
| * 41b684c1 -- Optimizing PrioritySpheresMask::getFeaturePointWeights
|/  
*   16b30b19 -- Merge branch '275-improve-heatmap3d' into 'master'
|\  
| * b66973cb -- Resolve "Improve HeatMap3D"
|/  
*   27144287 -- Merge branch '274-introduce-distancemeasurement' into 'master'
|\  
| * 6dfe5d35 -- Resolve "Introduce DistanceMeasurement and HeatMap3D"
|/  
*   3463c903 -- Merge branch '271-optimize-poisson-disk-sub-sampling' into 'master'
|\  
| * 4b0bbdf8 -- Resolve "Optimize sub-sampling strategies"
|/  
*   20b0a6f2 -- Merge branch 'serialize-curvature' into 'master'
|\  
| * 47b030da -- Make Curvature class serializable
|/  
*   ae4f701a -- Merge branch '270-zoom-to-fit' into 'master'
|\  
| * fd21f233 -- Resolve "Zoom to fit"
|/  
* 10b60083 -- Fix style-check warnings
*   536a04a3 -- Merge branch '269-add-export-import-buttons-whenever-possible' into 'master'
|\  
| * 5817242c -- Resolve "Add export buttons to the Distance tab"
|/  
* 1f84bd59 -- Update .gitlab-ci.yml file
* 17e52254 -- Update .gitlab-ci.yml file
* 7ec5d998 -- Update .gitlab-ci.yml file
* 9bb30a91 -- Update .gitlab-ci.yml file
* 81a5e11b -- Update .gitlab-ci.yml file
* 2af725b4 -- Update .gitlab-ci.yml file
* e3af8696 -- Update .gitlab-ci.yml file
* a5d46443 -- Update .gitlab-ci.yml file
* 13c1ed1d -- Update .gitlab-ci.yml file
* cdf94714 -- Update .gitlab-ci.yml file
* 7779c831 -- Update VERSION.txt
* 575b6c7b -- Update .gitlab-ci.yml file
* d53d8c2f -- Update VERSION.txt
* 721a5757 -- Update .gitlab-ci.yml file
* 0bc19b06 -- Update VERSION.txt
* 207a0d83 -- Update .gitlab-ci.yml file
* aeda5fd6 -- Update VERSION.txt
* b331a1db -- Update .gitlab-ci.yml file
* 5e81a33e -- Update VERSION.txt
* 59434799 -- Update .gitlab-ci.yml file
* b33603e5 -- Update .gitlab-ci.yml file
* 0d014ed0 -- Update .gitlab-ci.yml file
* 0b8cb7f4 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 4a0ae8ba -- Update pom.xml version.
*   ba144254 -- Merge branch '268-refactor-batch-processing-gui' into 'master'
|\  
| * dbd3dfe1 -- Resolve "Refactor batch processing GUI"
|/  
*   988778d7 -- Merge branch '266-fix-heatmap-rendering-without-shaders' into 'master'
|\  
| * 7dc13540 -- Resolve "Fix heatmap rendering without shaders"
|/  
*   bb75a1d0 -- Merge branch '265-experiments-for-the-paper' into 'master'
|\  
| * c9c51418 -- Resolve "Experiments for the paper"
|/  
* 4b2d8aa6 -- Update README.md
*   babf6106 -- Merge branch '245-introduce-order-dependent-transparency-and-fog' into 'master'
|\  
| * 1a3da39e -- Resolve "Introduce order dependent transparency and fog"
|/  
*   cedda738 -- Merge branch '264-add-the-user-guide-link-into-the-help-menu' into 'master'
|\  
| * 2609a267 -- Resolve "Add the User Guide link into the Help menu"
|/  
*   7c733897 -- Merge branch '263-create-installers-with-included-jre' into 'master'
|\  
| * 74b7b871 -- Resolve "Create installers with included JRE"
|/  
*   84a107ce -- Merge branch '262-introduce-mvnw-wrapper' into 'master'
|\  
| * 0d7d3533 -- Resolve "Introduce mvnw wrapper"
|/  
* 12bbdf7e -- Minor improvements
* d08a691d -- Add help link icons; adjust point cloud views
* 9b876a14 -- Initiate cotnrol pannel correctly
* 1ec96204 -- Simplify control panel's GUI
*   499c43c6 -- Merge branch '259-bug-saved-task-tabs-do-not-reopen' into 'master'
|\  
| * faf6a776 -- Resolve "BUG: Saved task tabs do not reopen"
|/  
*   898954d5 -- Merge branch '258-remove-panel-builder' into 'master'
|\  
| * 70a5dcf0 -- Resolve "Remove panel builder"
|/  
*   5f4ac5d7 -- Merge branch '251-move-the-web-module-into-a-separate-project' into 'master'
|\  
| * 0189fd74 -- Resolve "Move the Web module into a separate project"
|/  
*   0bcd3b2a -- Merge branch '256-update-libraries' into 'master'
|\  
| * c130f6ff -- Resolve "Update libraries"
|/  
* 163e83d1 -- Update staging.sh
* 0e838745 -- Update staging.sh
* 2f3f352a -- Update staging.sh
*   ad9e26bc -- Merge branch 'Webapp-integration-tests' into 'master'
|\  
| * 8c1d937e -- Webapp-integration-tests
|/  
*   531ac27d -- Merge branch 'poisson-disk-sub-sampling' into 'master'
|\  
| * 76a4bbbb -- Poisson disk sub sampling
|/  
*   033499a9 -- Merge branch '250-introduce-generic-list-of-feature-points' into 'master'
|\  
| * 22e861e6 -- Added GUI element for list of feature points.
|/  
*   bf08a3d1 -- Merge branch 'Webapp-refactoring' into 'master'
|\  
| * 0ce75948 -- Webapp refactoring
|/  
*   1399fe7d -- Merge branch '249-fix-an-error-in-javadoc' into 'master'
|\  
| * d1a6a86c -- Resolve "Fix an error in javadoc"
|/  
* 58085ab8 -- Update .gitlab-ci.yml
* 3fee106b -- Update .gitlab-ci.yml
* 67e985c6 -- Update .gitlab-ci.yml
*   99b1c847 -- Merge branch '244-introduce-gui-panel-builder' into 'master'
|\  
| * fcf86633 -- Resolve "Introduce GUI panel builder"
* |   4214800a -- Merge branch '248-bug-camera-rotation' into 'master'
|\ \  
| |/  
|/|   
| * e20ccca3 -- Resolve "Bug: Camera rotation"
|/  
*   d21525a3 -- Merge branch 'Webapp-Hotfixes' into 'master'
|\  
| * c37d838a -- Webapp - hotfixes
|/  
*   45b696b5 -- Merge branch '70-cross-cuttting-curves' into 'master'
|\  
| * 4c2191a6 -- Feat: Normal vector on cross cutting curves with size slider
|/  
*   80df32ef -- Merge branch '246-Webapp_-_Refactoring_three.js_to_react-three-fiber' into 'master'
|\  
| * ebb2d543 -- Resolve "Webapp - Refactoring three.js to react-three-fiber"
|/  
*   2318359d -- Merge branch '221-draw-3d-mask' into 'master'
|\  
| * f9b3eb60 -- Resolve "draw-3D-mask"
|/  
*   a0ed34c7 -- Merge branch '241-Webapp_-_File,_project,_task_details_on_main_page' into 'master'
|\  
| * 8c1111c2 -- Webapp - File, project, task details on main page
|/  
* 8b8812c8 -- Update .gitlab-ci.yml
*   209c7fe8 -- Merge branch '242-fix-ci-cd-javadoc' into 'master'
|\  
| * a586a230 -- Resolve "Fix CI/CD - javadoc"
|/  
* e734be37 -- Update .gitlab-ci.yml
*   8b05f6d7 -- Merge branch '239-Webapp_-_Users_managment,_admin_dashboard' into 'master'
|\  
| * e8e072e5 -- Preparing page for users administration
|/  
*   cafea93e -- Merge branch '240-adapt-k-d-trees-to-standalone-points' into 'master'
|\  
| * 4745f25e -- Resolve "Adapt k-d trees to standalone points"
|/  
*   bb73c4ed -- Merge branch '237-HumanFace_service' into 'master'
|\  
| * 05a508d9 -- 237-HumanFace service
|/  
*   9071ee5e -- Merge branch '233-SceneProvider,_jpg_files_and_material' into 'master'
|\  
| * a494d901 -- Creating SceneProvider
* | 7da3987b -- Update .gitlab-ci.yml
* | cae10b0d -- Update .gitlab-ci.yml
* | 4b8e97ba -- Update .gitlab-ci.yml
* | d293919b -- Update .gitlab-ci.yml
* | e094099a -- Update .gitlab-ci.yml
* |   e2d6a7de -- Merge branch 'master' of gitlab.fi.muni.cz:grp-fidentis/analyst2
|\ \  
| * | 29689431 -- Update .gitlab-ci.yml
| * | 3543e5cc -- Update .gitlab-ci.yml
| * | a2405842 -- Update .gitlab-ci.yml
| * | 7b5ec40c -- Update .gitlab-ci.yml
| * | 354381d5 -- Update .gitlab-ci.yml
| * | 97e7c8ca -- Update .gitlab-ci.yml
| * | 4a7ad16f -- Update staging.sh
| * | 6b589f52 -- Update staging.sh
| * | 3fd3816e -- Update staging.sh
| * | f8b1e9f1 -- Update .gitlab-ci.yml
| * | a6f164d0 -- Update staging.sh
| * | 3ac7fa11 -- Update .gitlab-ci.yml
| * | 14b1e7aa -- Update .gitlab-ci.yml
| * | f3f3f6ef -- Update .gitlab-ci.yml
| * | 66ca2402 -- Update .gitlab-ci.yml
| * | d795d64f -- Update .gitlab-ci.yml
* | | e35dfa6f -- Fix javadoc
|/ /  
* |   a32021f6 -- Merge branch '235-prepare-staging-server' into 'master'
|\ \  
| |/  
|/|   
| * acab4e8c -- Resolve "Prepare staging server"
|/  
* a45bd72f -- Update .gitlab-ci.yml file
* e2540303 -- Update .gitlab-ci.yml file
* 66df6a52 -- Update .gitlab-ci.yml file
* b1e1ed6f -- Update .gitlab-ci.yml file
* f682ca52 -- Update .gitlab-ci.yml file
* 1c8b298c -- Update .gitlab-ci.yml file
* cb18b61a -- Update .gitlab-ci.yml file
* 13fccc3d -- Update .gitlab-ci.yml file
* 5ac54cc4 -- Update .gitlab-ci.yml file
* bac7b1c9 -- Update .gitlab-ci.yml file
* 7899cf85 -- Update .gitlab-ci.yml file
* 9738ccba -- Update .gitlab-ci.yml file
* ab83dcdf -- Update .gitlab-ci.yml file
* 9984e867 -- Update .gitlab-ci.yml file
* d0cdf0f4 -- Update .gitlab-ci.yml file
* 67c2340f -- Update .gitlab-ci.yml file
* cfb2f3e2 -- Update .gitlab-ci.yml file
* 6d04690b -- Update .gitlab-ci.yml file
* 41fd32c2 -- Update .gitlab-ci.yml file
* a1ca1dbe -- Update .gitlab-ci.yml file
* 9957d06b -- Update .gitlab-ci.yml file
* 7b035a04 -- Update .gitlab-ci.yml file
* 70dd6068 -- Update .gitlab-ci.yml file
* 85724e5e -- Update .gitlab-ci.yml file
* 02caa28b -- Update .gitlab-ci.yml file
*   ea74496b -- Merge branch '231-Missing_FE_features' into 'master'
|\  
| * 24f9377c -- 231-Missing FE features
|/  
*   d90712f8 -- Merge branch '230-Global_errorhandling_class_on_BE,_notifications_FE' into 'master'
|\  
| * 9602d8bf -- 230 - Global errorhandling class on BE, notifications FE
|/  
*   0d170ef6 -- Merge branch '232_-_Changing_API_requests_to_have_relative_paths' into 'master'
|\  
| * 142f3d4b -- 232 - Changing API requests to have relative paths
|/  
*   c2664b7d -- Merge branch '229-Loaders_across_whole_application' into 'master'
|\  
| * 46a739d8 -- 229-Loaders across application
|/  
*   8b4f1d50 -- Merge branch '228-Automatic_login,_password_complexity,_enter_even_handler' into 'master'
|\  
| * 5931b201 -- 228-Password complexity, enter even handler, axios interceptor for tokens
|/  
*   029f267f -- Merge branch '227_-_Implementing_project_logic_and_basic_render' into 'master'
|\  
| * 91e02143 -- 227 - implementing project logic and basic render
|/  
*   0e7cbe2e -- Merge branch '224-fe-ui-layout' into 'master'
|\  
| * 40a37595 -- FE UI layout
|/  
* 654bb520 -- Skip Web tests
*   ba60526e -- Merge branch '220-prepare-database-login-register' into 'master'
|\  
| * d6eadabc -- 220 - prepare database, login, register forms
|/  
*   11a872f2 -- Merge branch 'manual-alignment-hotfix' into 'master'
|\  
| * e3464ba4 -- apply fixes from branch 222-fix-manual-alignment
|/  
*   b3d742b4 -- Merge branch 'bug-when-loading-basic-face' into 'master'
|\  
| * baeab67c -- fix bug when loading basic faces
|/  
*   dc91a7a9 -- Merge branch '226-fix-color-marks-in-the-feature-point-controls-panel' into 'master'
|\  
| * c881f76d -- Resolve "Fix color marks in the Feature Point controls panel"
|/  
*   21b76b0f -- Merge branch '181-edit-feature-points' into 'master'
|\  
| * 10eb2a5a -- Resolve "Edit feature points"
|/  
*   d247c66f -- Resolve conflicts
|\  
| * 037478b7 -- add javadoc
| * ad9868aa -- small refactoring
| * 1a97567e -- rename profile rendering panel to curve rendering panel, fix view export
| * 70472524 -- add text for sampling info button
| * 0915a6b3 -- create helper function for file creation
| * ce153db9 -- add configuration export to json
| * d17bb358 -- add dependency for json
| * c8fa36f2 -- add function for getting current plane type
| * 288d4cf3 -- remove debug prints
| * 233ae844 -- fix null feature points in curve2d
| * d96fc2bf -- use deterministic sampling instead of random
| * 1204f4e9 -- remove old classes
| * ca432f91 -- change name of ProfilesPanel to CuttingPlanesPanel
| * 4ec380a4 -- change spinner listener for sampling
| * c3044634 -- change layout of cutting plane panel
| * e0bdf543 -- change the sampling slider size
| * f2cc5b5e -- fix setting of default plane, add javadoc
| * fca1a678 -- recompute hausdorff when sampling changes
| * db4808f1 -- fix docs
| * 6de33f8a -- use sampling when computing hausdorff distance
| * 19f374e0 -- hide hausdorff dist panel in single face mode
| * 71d6d271 -- prepare GUI for curve subsampling
| * 3f63320a -- add javadoc
| * 1e0b2eb8 -- fix warnings
| * 419da4d8 -- add tests for hausdorff distance
| * d09fc9f0 -- fix typos in GUI
| * 6ffe89c2 -- use whole curve in hausdorff computation, use only meaningful points of curve
| * f224365b -- add constructor for unit testing
| * c142d3e6 -- remove unused function
| * 4b74c7c7 -- add function for checking endPoints of curve
| * 55b3b59c -- hide hausdorff labels when analysing one face
| * 2e51dd90 -- show hausdorff for all three planes
| * 9ae49106 -- compute hausdorff only when mouse released
| * 1b996502 -- fix snapping of feature points
| * fb5598af -- fix documentation
| * 9f9f7bc3 -- hide hausdorff when only one face present, add tooltip
| * 4cd875b5 -- use hausdorff dist in profiles panel
| * 9c8d1dda -- remove unused function
| * d3e5507e -- fix conversion from 3d curve to 2d
| * a37638ca -- add basic hausdorff distance calculator for 2d shapes
| * 5c2859f8 -- switch to using 2d curves in GUI
| * f5e74115 -- remove unused functionality from CrossSectionCurve
| * dfef534d -- add option for returning Curve2d as a result of CrossSectionCalculator
| * 84df2964 -- set feature points manually
| * 4c3e7267 -- create class Curve2d - CrossSectionCurve projected to 2d
* |   2cf29e9a -- Merge branch '223-fix-spinslider' into 'master'
|\ \  
| * | 547df506 -- Resolve "Fix SpinSlider"
|/ /  
* |   c93fb032 -- Merge branch '222-fix-manual-alignment-of-objects' into 'master'
|\ \  
| |/  
|/|   
| * 38c5ef72 -- Resolve "Fix manual alignment of objects"
|/  
*   0137ef97 -- Merge branch '195-force-initial-colors-of-fps-when-working-with-fp-tab' into 'master'
|\  
| * 3b630e31 -- Fixed bug, new functionality added
|/  
*   e64eb716 -- Merge branch '63-refactor-gui-for-cutting-planes' into 'master'
|\  
| * 0a75873c -- Resolve "Refactor GUI for cutting planes"
|/  
*   3db148dd -- Merge branch '218-screen-point-to-ray' into 'master'
|\  
| * b065871e -- Resolve "screen point to ray"
|/  
*   51443462 -- Merge branch '214-measure-batch-ground-truth-in-both-directions' into 'master'
|\  
| * 1be579ac -- Resolve "Multiples GUI fixes"
|/  
*   04b466ef -- Merge branch '216-prepare-spring-boot-application' into 'master'
|\  
| * 127de0c5 -- Resolve "Prepare spring boot application"
|/  
*   84437da1 -- Merge branch 'multiple-cutting-planes' into 'master'
|\  
| * ee78fce0 -- Multiple cutting planes
|/  
*   3d277dfc -- Merge branch '215-initialize-webapp-and-basic-setup' into 'master'
|\  
| * cb6e91e4 -- Resolve "Initialize webapp via Vite"
|/  
*   3487b386 -- Merge branch '213-add-a-pairwise-batch-measurement-with-auto-cropping-to-the-mean-face' into 'master'
|\  
| * 58ed3820 -- Resolve "Add a pairwise batch measurement with auto-cropping to the mean face"
|/  
*   8ebb0ee5 -- Merge branch '209-add-multiple-iterations-into-batch-processing' into 'master'
|\  
| * eb80751c -- Resolve "Add multiple iterations into batch processing"
|/  
*   e8fa2bed -- Merge branch '208-introduce-symmetric-icp' into 'master'
|\  
| * dd9b3b67 -- Resolve "Introduce symmetric ICP"
|/  
*   e1bcdd94 -- Merge branch '206-fix-and-refactor-ray-casting' into 'master'
|\  
| * c9646558 -- Resolve "Fix and refactor ray-casting"
|/  
* deff3d81 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 33422698 -- Update pom.xml version.
*   d32c20cd -- Merge branch '205-refactor-project-gui' into 'master'
|\  
| * 11b5d39c -- Resolve "Refactor project GUI"
|/  
*   a4febb9e -- Merge branch '204-refactor-gui-classes' into 'master'
|\  
| * d819d0ef -- Resolve "Refactor GUI classes"
|/  
*   e71cfc26 -- Merge branch '202-move-project-related-logic-into-the-project-module' into 'master'
|\  
| * a10d47f3 -- Resolve "Move project-related logic into the Project module"
|/  
*   3f5ff965 -- Merge branch '201-refactor-project-management' into 'master'
|\  
| * fa1cd230 -- Resolve "Refactor project management"
|/  
*   6909cabf -- Merge branch '200-move-project-management-into-a-toolbar' into 'master'
|\  
| * 185f8022 -- Resolve "Move project management into a toolbar"
|/  
* bc57dac5 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 4e90787a -- Update pom.xml version.
*   93eaf830 -- Merge branch '198-accelerate-octree-creation' into 'master'
|\  
| * 2e52b77d -- Resolve "Accelerate octree creation"
|/  
*   cf48d852 -- Merge branch '197-remove-dependency-of-gui-on-jogl' into 'master'
|\  
| * a07a91ca -- Resolve "Remove dependency of GUI on JOGL"
|/  
*   df507270 -- Merge branch '196-fix-readme' into 'master'
|\  
| * d4d31fbf -- Resolve "Fix README"
|/  
*   beb195cf -- Merge branch '108-upgrade-to-jdk-17-2' into 'master'
|\  
| * 7a771e38 -- Resolve "Upgrade to JDK 17"
|/  
*   6922bb8c -- Merge branch '191-fix-error-in-feature-points-rendering' into 'master'
|\  
| * 2883b90b -- Resolve "Fix error in feature points rendering"
|/  
*   1882da95 -- Merge branch '190-introduce-spacepartioning-module' into 'master'
|\  
| * 0c0b05c8 -- Resolve "Introduce SpacePartioning module"
|/  
*   fb401659 -- Merge branch '193-move-project-classes-to-project-package' into 'master'
|\  
| * 173c3b14 -- moved project files to separate package
|/  
*   60f0ac1d -- Merge branch 'issue-186/procrustes-documentation' into 'master'
|\  
| * 013ccf8f -- MR [#186] documentation
|/  
*   27c3d80f -- Merge branch '192-fix-rendering-of-fps-and-lights-when-texture-is-turned-on' into 'master'
|\  
| * 74f990e7 -- Resolve "Fix rendering of FPs and lights when texture is turned on"
|/  
* 5d6b4b8a -- Add LICENSE
*   90d401a7 -- Merge branch 'procrustes-refactor' into 'master'
|\  
| * 4a13944f -- refactor: Removing not need code from procrustes face model
|/  
*   48cd2542 -- Merge branch '181-edit-feature-points' into 'master'
|\  
| * 440e419d -- Resolve "Edit feature points"
|/  
*   e2b13218 -- Merge branch 'issue-123/procrustes-visitor' into 'master'
|\  
| * 4dbd9a33 -- Minor improvements before merge
| * 6f18f763 -- [#123] feat: rework of procrustes face model class and simplyfication of analysis
| * e33246e2 -- [#123] feat: Procrustes visitor has history of applied transformations
| * 63355ca1 -- [#123] feat: reworking procrustes visitor to extend HumanFaceVisitor
| *   796d7224 -- Merge branch 'issue-123/procrustes-visitor'
| |\  
| | * 4207346e -- [#123] rebase: rebase on master branch
| | * 58d6f682 -- [#123] test: adding tests and refactoring code
| | * a2616ddb -- [#123] feat: procrustes visitor superimposition of points
| | * c5d994f1 -- [123] feat: procrustes analysis with subset of feature points
| | * 2c5017f6 -- [#123] feat: visualisation of rotated featurepoints
| | * 9411d1ad -- [#123] feat: rotation issue
| | * e958e2df -- [#123] feat: procrustes visitor rework
| | * 1c2ba9ed -- [#123] feat: Procrustes Visitor
* | |   762be9ed -- Merge branch '184-move-useOctree-to-GUI' into 'master'
|\ \ \  
| * | | 7c26e69c -- Move useOctree variable to GUI and create it's own check box
|/ / /  
* | |   452054af -- Merge branch '182-indicate-faces-analyzed-in-multiple-tasks' into 'master'
|\ \ \  
| * | | a53cbdf4 -- Resolve "Indicate faces analyzed in multiple tasks"
|/ / /  
* | |   988dfb20 -- Merge branch '126-create-average-face-contructor-for-octrees' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | 8ef1afdd -- Compute Average Face
|/ /  
* |   5b39bd6c -- Merge branch '183-fix-material-serialization' into 'master'
|\ \  
| * | c1dd8f84 -- Resolve "Fix material serialization"
|/ /  
* |   16c9fcaa -- Merge branch '180-adaptive-downsampling-for-icp' into 'master'
|\ \  
| * | 5b791b26 -- Resolve "Adaptive downsampling for ICP"
|/ /  
* |   eacd7eef -- Merge branch '171-integrate-face-state-tab-into-task-views' into 'master'
|\ \  
| * | f1b9c1b6 -- Resolve "Integrate Face state tab into task views"
|/ /  
* |   f330a3c8 -- Merge branch 'ro-load-material-and-texture-together-with-face-geometry' into 'master'
|\ \  
| * | 989d8778 -- Ro load material and texture together with face geometry
* | |   457f5429 -- Merge branch '168-adapt-filter-tab' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | be46d007 -- Resolve "Adapt Filter tab"
|/ /  
* |   a1d037ac -- Merge branch '177-fix-error-in-cutting-plane-1-1' into 'master'
|\ \  
| |/  
|/|   
| * 64e2e116 -- Resolve "Fix error in cutting plane 1:1"
|/  
*   1d78362c -- Merge branch '178-fix-error-in-the-registration-gui' into 'master'
|\  
| * f40f7a6f -- Resolve "Fix error in the registration GUI"
|/  
*   00b599e2 -- Merge branch '174-introduce-a-uniform-space-sampling' into 'master'
|\  
| * 857af3dd -- Resolve "Introduce a uniform space sampling"
|/  
*   9713c5aa -- Merge branch '176-add-drawable-line' into 'master'
|\  
| * 94e0f2c8 -- Added drawable line
|/  
*   befdabbe -- Merge branch '167-adapt-face-info-tab' into 'master'
|\  
| * b381ba89 -- Resolve "Adapt Face info tab"
|/  
*   3a1a4371 -- Merge branch '161-165-fix-and-improve-project-workflow' into 'master'
|\  
| *   118ceedb -- Merge branch 'master' into 161-165-fix-and-improve-project-workflow
| |\  
| * | bf288f1a -- fixed scrolling of project tab
| * | 9a88a1aa -- fixed preview image loading
| * |   36bfff07 -- Merge branch 'master' into 161-165-fix-and-improve-project-workflow
| |\ \  
| * | | 3bf4e9be -- fixed saving project
| * | | 0aae829f -- fixed image preview, showing preview images
| * | | eeaacb1d -- changed the behavior of initial window
| * | | 4eb34529 -- fixed image preview
| * | | f70ed1cc -- added selection of the primary face in 1:1
| * | | 6183290e -- fix N:N tab opening
* | | |   2ff7d689 -- Merge branch '173-introduce-point-clouds' into 'master'
|\ \ \ \  
| * | | | b2969a87 -- Introduce point sampling strategies for ICP and symmetry"
|/ / / /  
* | | |   9d7cad76 -- Merge branch '172-visualize-bouding-box-for-cutting-plane' into 'master'
|\ \ \ \  
| |_|_|/  
|/| | |   
| * | | 68f736dd -- Resolve "Visualize bouding box for cutting plane"
|/ / /  
* | |   85a892f1 -- Merge branch '170-fix-showing-path-in-file-info-panel' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | 57093535 -- fixed showing path; changed font
|/ /  
* |   3f4435ae -- Merge branch '166-adapt-file-info-tab' into 'master'
|\ \  
| |/  
|/|   
| * b854fefa -- adapt file info tab
|/  
*   394cd0cd -- Merge branch '158-upgrade-project-tab-load-landmarks-open-close-tabs-from-menu-save-project-when-closing-app' into 'master'
|\  
| * f5315c9b -- Resolve "Upgrade project tab (opening and closing app changes; analyse models; opening preview image)"
|/  
*   4719829d -- Merge branch '160-accelerate-n-n' into 'master'
|\  
| * 578cc3bb -- Resolve "Accelerate N:N"
|/  
*   b57b6375 -- Merge branch '159-accelerate-swapping' into 'master'
|\  
| *   b3dc7133 -- Merge branch 'master' into '159-accelerate-swapping'
| |\  
| |/  
|/|   
* |   e24b2ea7 -- Merge branch '159-accelerate-swapping' into 'master'
|\ \  
| * | 0bc94b7a -- Resolve "Accelerate swapping"
|/ /  
| * 8d58415a -- N:N integrated into the project tab
| * 9267f7f2 -- Using Kryo with objenesis to accelerate serialization
|/  
*   d31bc855 -- Merge branch '157-fix-symmetry-plane-alignment' into 'master'
|\  
| * b35e2db9 -- Resolve "Fix symmetry plane alignment"
|/  
*   bd88d625 -- Merge branch '155-remove-eventbus-from-projectpanel' into 'master'
|\  
| * 7bedd802 -- Resolve "Remove EventBus from ProjectPanel"
|/  
*   028d627a -- Merge branch '156-fix-icp-scaling' into 'master'
|\  
| * 82026c27 -- Resolve "Fix ICP scaling"
|/  
*   0d5d4478 -- Merge branch '131-remove-crosssection-remain-crosssectionzigzag-only' into 'master'
|\  
| * 44f75a2a -- Resolve "Remove CrossSection, remain CrossSectionZigZag only"
|/  
*   d9f2884a -- Merge branch '154-introduce-humanfaceutils' into 'master'
|\  
| * 05bf4a11 -- Resolve "Introduce HumanFaceUtils"
|/  
*   86f9932c -- Merge branch '149-project-tab-improvements' into 'master'
|\  
| * 7a233e97 -- Resolve "Project tab improvements"
|/  
*   3793a05a -- Merge branch '152-improve-batch-processing' into 'master'
|\  
| * b63600f0 -- Resolve "Improve batch processing"
|/  
*   c1e4c798 -- Merge branch '151-compute-hd-batch-finalization-by-using-opencl' into 'master'
|\  
| * 3956ed7f -- Resolve "Compute HD batch finalization by using OpenCL"
|/  
*   759528af -- Merge branch '148-fix-batch-processing' into 'master'
|\  
| * 758bae20 -- Resolve "Fix batch processing"
|/  
*   a9500202 -- Merge branch '146-export-average-face' into 'master'
|\  
| * 4615002e -- Resolve "Export average face"
|/  
*   36c1cef3 -- Merge branch '127-add-basic-json-scheme-for-project' into 'master'
|\  
| * b3ad53f4 -- Resolve "Add basic JSON scheme for project"
|/  
*   6d5a67c6 -- Merge branch '145-fix-landmarks-initialization-from-file' into 'master'
|\  
| * ff5495ae -- Resolve "Fix landmarks initialization from file"
|/  
*   10d197ac -- Merge branch '144-fix-batch-mode-rendering-of-avg-face' into 'master'
|\  
| * 12b00022 -- Resolve "Fix batch mode rendering of AVG face"
|/  
*   b5cd9492 -- Merge branch '143-fix-the-scene' into 'master'
|\  
| * 1ea67138 -- Resolve "Fix the scene"
|/  
*   88316bf8 -- Merge branch '142-introduce-batch-processing-gui' into 'master'
|\  
| * d23bf126 -- Resolve "Introduce batch processing GUI"
|/  
*   53b5c786 -- Merge branch '111-create-octree' into 'master'
|\  
| * 20b29d73 -- Create Octree structure and create Visitor for Octree
|/  
*   411006ff -- Merge branch '141-create-batch-evaluation-tests' into 'master'
|\  
| * 47c5ce9e -- Resolve "Create batch evaluation tests"
|/  
*   6690a848 -- Merge branch '140-optimize-symmetry-plane-computation' into 'master'
|\  
| * 2cbe32cc -- Resolve "Optimize symmetry plane computation"
|/  
*   249c4bdd -- Merge branch '139-ground-truth-similarity-stats' into 'master'
|\  
| * da56c781 -- Resolve "Ground truth similarity stats"
|/  
*   d597fb18 -- Merge branch '138-optimize-weighted-hd' into 'master'
|\  
| * 40c9cea8 -- Resolve "Optimize weighted HD"
|/  
*   e7976cff -- Merge branch '137-symmetry-plane-precision-measurement' into 'master'
|\  
| * a6f30d31 -- Added symmetry plane precision measurment
|/  
*   e37267a9 -- Merge branch '136-compute-symmetry-plane-from-feature-points' into 'master'
|\  
| * 4cf89ef5 -- Resolve "Compute symmetry plane from feature points"
|/  
* 5e583ab5 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 023f290d -- Update pom.xml version.
* 3c1e0b70 -- CHANGELOG.md file updated with commits between the current and previous tag.
* ed1bad2b -- Update pom.xml version.
* 4cc13e1c -- Update .gitlab-ci.yml
* a1f16612 -- CHANGELOG.md file updated with commits between the current and previous tag.
* e061166e -- Update pom.xml version.
*   4ba0434e -- Merge branch 'master' of gitlab.fi.muni.cz:grp-fidentis/analyst2
|\  
| * afb2d982 -- Update .gitlab-ci.yml
* | dd5a5496 -- CHANGELOG.md file updated with commits between the current and previous tag.
* | 718dfc43 -- Update pom.xml version.
|/  
* 37c3f4b3 -- CHANGELOG.md file updated with commits between the current and previous tag.
* 9f5d6521 -- Update pom.xml version.
*   2ea14287 -- Merge branch 'master' of gitlab.fi.muni.cz:grp-fidentis/analyst2
|\  
| *   53be5bf6 -- Merge branch '134-update-ci-pipeline' into 'master'
| |\  
| | * 42d46ca2 -- Resolve "Update CI pipeline"
| |/  
* | 445febb1 -- CHANGELOG.md file updated with commits between the current and previous tag.
* | 216d3b07 -- Update pom.xml version.
|/  
*   ec13b772 -- Merge branch '130-split-a-profile-curve-with-respect-to-feature-points' into 'master'
|\  
| * 5c67269f -- Resolve "Split a profile curve with respect to feature points"
|/  
*   55e33200 -- Merge branch '128-support-segmentation-of-a-profile-curve' into 'master'
|\  
| * ede1e5c3 -- Resolve "Support segmentation of a profile curve"
|/  
*   06678872 -- Merge branch '133-fix-gui-changes' into 'master'
|\  
| * 3fb8e0c9 -- Fixed previous dirty merge
|/  
*   02943644 -- Merge branch '132-fix-transparency-of-cutting-planes' into 'master'
|\  
| * d9623f67 -- Fixed
|/  
*   1a6a0aac -- Merge branch '118-introduce-view-toolbox' into 'master'
|\  
| * f3f3b606 -- Resolve "Introduce view toolbox"
|/  
*   80f08293 -- Merge branch '122-add-unit-tests-for-crosssection' into 'master'
|\  
| * 517dccd2 -- Add unit tests for CrossSection
|/  
*   6412ac23 -- Merge branch 'issue121' into 'master'
|\  
| * 1160ce6f -- cross section visitor optimised
|/  
*   86d571d4 -- Merge branch '120-set-transparency-of-cutting-planes' into 'master'
|\  
| * 0f6a22e6 -- Transparency of cutting planes
|/  
*   b1583cce -- Merge branch 'mirror' into 'master'
|\  
| * 2d1f6280 -- mirror cuts in both single and 1:1 views
|/  
*   512a1dfc -- Merge branch 'issue-38/procrustes-superimposition' into 'master'
|\  
| * 3eff067a -- Removed 3point... file resiuum
| * 21cb1809 -- Minor javadoc fixies
| * daacd3e3 -- [#38] refactor: deleted unnecessary files
| * 82cf0ac8 -- [#38] feat: check if amount of fp is more than 3
| * ca38f5a4 -- [#38] refactor: removing procrustes analysis utils class
| * 33d17160 -- [#38] refactor: removing procrustes analysis utils class
| * 8b3f663f -- [#38] documentation
| * 4dd01a3f -- [#38] feat: procrustes scaling
| *   2ed5c65e -- Merge branch 'master' into 'issue-38/procrustes-superimposition'
| |\  
| * | 4123f32e -- [#38] documentation: adding java docs
| * |   83ec7a7c -- Merge branch 'master' into issue-38/procrustes-superimposition
| |\ \  
| * | | 7428bd66 -- [#38] documentation: IPosition documantation
| * | | cda9762b -- [#38] refactor: adding documantation and deleting dead code
| * | | 447b13c8 -- [#38] feat: procrustes analysis working with matrix
| * | | fa57f8e8 -- [#38] feat: procrustes visualisation
| * | | 8bd53bfa -- [#38] feat: rotation on procrustes analysis
| * | | 02ca049a -- [#38] feat: procrustes analysis (work in progress: wrong transformation)
| * | | f7586beb -- [#38] feat: calculation of vertices rotation
| * | | 104198c5 -- [#38] feat: centering models on feature points centroid
* | | |   74aa7589 -- Merge branch '116-show-changes-from-humanface-in-list-of-models-via-eventbus' into 'master'
|\ \ \ \  
| |_|_|/  
|/| | |   
| * | | 0b418e8c -- Resolve "Show changes from HumanFace in list of models (via EventBus)"
|/ / /  
* | |   f51302ba -- Merge branch '117-bug-the-application-freezes-in-1-1-analysis' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | a7b96146 -- Resolve "BUG: The application freezes in 1:1 analysis"
|/ /  
* |   a216b006 -- Merge branch 'offset' into 'master'
|\ \  
| * | 0664170d -- working offset and alligning of faces made optional
|/ /  
* |   ccb34fd8 -- Merge branch '115-make-logging-into-the-outputwindow-available-from-low-level-modules' into 'master'
|\ \  
| * | a721e90e -- Resolve "Make logging into the OutputWindow available from low-level modules"
|/ /  
* |   42761201 -- Merge branch '113-introduce-new-features-on-list-of-faces-in-a-project' into 'master'
|\ \  
| * \   4e7d7f99 -- Merge branch 'master' into 113-introduce-new-features-on-list-of-faces-in-a-project
| |\ \  
| * | | f216d538 -- added filter and face state panels (without functionality)
| * | | f168c590 -- removed commented code
| * | | ede73147 -- created new table model class which observes HumanFace
| * | | 6c8f10cd -- HumanFace now extends Observable
| * | | 0d12a329 -- created new class DefaultModelsTableModel
| * | |   e20670fa -- Merge branch 'master' into 113-introduce-new-features-on-list-of-faces-in-a-project
| |\ \ \  
| | | |/  
| | |/|   
| * | | 0f7f9e0c -- added "has KD-tree" column to table and multiple model selection
* | | |   972a5551 -- Merge branch 'export_csv' into 'master'
|\ \ \ \  
| |_|_|/  
|/| | |   
| * | | 46b8ca5e -- Exporting polyline profiles
|/ / /  
* | |   dc929d34 -- Merge branch 'issue10' into 'master'
|\ \ \  
| |_|/  
|/| |   
| * | 4224b404 -- Polyline profiles in separate tab
|/ /  
* |   c0329df4 -- Merge branch 'issue-81/fp-io-formats' into 'master'
|\ \  
| |/  
|/|   
| * 4213b08a -- Final minor improvements
| * 8de43d65 -- [#81] test: tests generate tmp files
| * 27c5c426 -- [#81] direcotry: moving files
| * f41c4a32 -- [#81] fix: removed comparison procrustesAnalysis files in development from incorrect branch
| * b77711c4 -- [#81] feat: added procrustesAnalysis exception
| * b0f0ba96 -- [#81] documentation: added java docs and resolved lint wanrings
| * 04d278fb -- [#81] documentation: adding java docs
| *   a88d1df4 -- Merge branch 'master' into issue-81/fp-io-formats
| |\  
| * | 470099d3 -- [#81] documantation: added author
| * | 5de17b10 -- [#81] documantation: added documantation to classes
| * | 9980bca9 -- [issue81]: refactoring export import services
| * | 90e0dbe8 -- [#81] feat: import/export of .fp format
|  /  
* | 85415dbd -- CHANGELOG.md file updated with commits between the current and previous tag.
* | 43727377 -- Update pom.xml version.
|/  
*   ad05bcb5 -- Merge branch '109-fix-gl-canvas-size' into 'master'
|\  
| * 1d737ddc -- Resolve "Fix GL Canvas size"
|/  
*   1c9a6f3d -- Merge branch '112-introduce-a-message-window' into 'master'
|\  
| * ec85edec -- Added OuputWindow and testing output meaasges
|/  
*   c2339a54 -- Merge branch '103-list-of-faces-in-project' into 'master'
|\  
| * 0bcb3c98 -- removed bug when getting name of file throws exception
| * 8179a447 -- updated message when model with same name is loaded
| * 50b0f598 -- removed warnings
| * 96078e98 -- added javadocs
| * 7b65ab15 -- updated attributes, added javadocs
| * b738128e -- pick model from list of models and select analysis
| * 54537962 -- loading models to the project class (not only to the list of models)
| * 3dcc80d3 -- added getShortName method (name of the file without path)
| * 9a374c6f -- merged primaryFace and secondaryFaces to just one attribute, models
| *   813aeb3e -- Merge branch 'master' into 103-list-of-faces-in-project
| |\  
| |/  
|/|   
* |   e6d35d70 -- Merge branch '105-adjust-hd-control-panel' into 'master'
|\ \  
| * | 14d7ce92 -- Removed unchecked and public access warnings
| * | b7637929 -- Misleading parameter replaced in the method signature
| * | 2d77d1ff -- Javadoc added
| * | a314b781 -- More suitable method called to omit unused method parameters
| * | a581e9fd -- Public and private constants reordered
| * | 7a436b3b -- Private and final modifiers added to class attribute
| * | d9a5ca5f -- Recompute Hausdorff distance only if the feature point is selected
| * | 6adb0c77 -- Buttons added to the feature point sliders
| * | 96eaaee7 -- Unused import removed
| * | d69360f1 -- Method for addition of slider option line with buttons implemented
| * | aecd2f51 -- Method for addition of slider with buttons implemented
| * | 23badd6c -- Method for addition of formatted text field with buttons implemented
| * | dd0d8e0c -- Gridwidth moved out of the addButtonCustom method
| * | 4dc74873 -- Methods for addition of slider with value refactored to use new methods for text field and slider addition
| * | 876869f2 -- Method for addition of slider implemented
| * | b084b098 -- Method for addition of formatted text field implemented
| * | b12f8fb6 -- Value-range class for integer implemented
|/ /  
* | 49b0fabb -- Update README.md
* | 919df176 -- Update README.md
* | 3fce6531 -- Update README.md
* |   7b57c496 -- Merge branch '104-better-color-mapping-for-the-whd-heatmap' into 'master'
|\ \  
| * | 682bcfd1 -- Useless class attribute eliminated
| * | f1ddef1d -- Heatmap of weighted HD replaced with a saturated heatmap of regular HD
| * | 7e37cf33 -- Minor refactoring
| * |   eb5313cb -- Merge branch 'master' into 104-better-color-mapping-for-the-whd-heatmap
| |\ \  
| |/ /  
|/| |   
| * | 8f5c6295 -- Variable color saturation added to heatmap renderer
| * | d3a24cda -- Map of heatmap colors saturation added to the drawable face
| * | fdba90b8 -- Double objects replaced with primitive data types
| | * 17bf3917 -- added remove button functionality
| | * c6faaa70 -- added selection buttons functionality
| | *   cbce008e -- Merge branch 'master' into 103-list-of-faces-in-project
| | |\  
| |_|/  
|/| |   
* | |   692a363c -- Merge branch '105-adjust-hd-control-panel' into 'master'
|\ \ \  
| * | | eb5fcf5f -- Methods of slider's mouse listener reduced to MouseListener-class methods only
| * | | 4e9da8b2 -- Hasdorff distance recalculated immediately when feature point highlighted
| * | | efded212 -- Buttons for (de)selection of all feature points added
| * | | 8ca5c086 -- Simple buttons are aligned to the start of the line
| * | | 4def42ab -- More suitable method called
| * | | 589a13ed -- Hausdorff distance recomputed when slider is released
| * | | eebac547 -- Slider mouse events redirected to the text field
| * | | 5d8fb6a8 -- Action listener replaced with key binding
| * | | 091d27c5 -- Identity return replaced with method call
| * | | c35c8a18 -- Typo fixed
| * | | 1146c286 -- Heatmap check-boxes replaced with dropdown menu
| |/ /  
* | |   e81a9fd1 -- Merge branch '106-replace-average-with-weighted-average-function-in-the-computation-of-feature-point-s-weight' into 'master'
|\ \ \  
| |/ /  
|/| |   
| * | 493c71a2 -- Variable renamed
| * | 29b865b5 -- Javadoc corrected
| * | cd6496c7 -- Javadoc beautified
| * | 12421b53 -- Computation of feature point weights corrected (weighted average used)
| * | 0d5d262c -- Collector for computation of the weighted average implemented
| * | f5ee16b8 -- Tests modified for correct calculation of feature point weights
|/ /  
| *   6c48d42b -- Merge branch 'master' into 103-list-of-faces-in-project
| |\  
| |/  
|/|   
* | af107f3e -- Removed -n (newver files only) parameter from the mirror command in the publish stage
 /  
* 9e090d11 -- added checkbox to table and reduced name of models
* f93e2b4e -- added table with models